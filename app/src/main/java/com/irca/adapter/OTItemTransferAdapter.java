package com.irca.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.irca.cosmo.R;
import com.irca.dto.OTItemDto;
import com.irca.dto.OTTransfer;
import com.irca.fields.OnRecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class OTItemTransferAdapter extends RecyclerView.Adapter<OTItemTransferAdapter.MultiSelectDialogViewHolder> {

    private List<OTItemDto> itemCartDtoList;
    private Context mContext;
    private OnRecyclerViewItemClickListener<OTItemTransferAdapter.MultiSelectDialogViewHolder, OTItemDto> onRecyclerViewItemClickListener;
    public static List<String> selectedList = new ArrayList<>();

    public OTItemTransferAdapter(List<OTItemDto> dataSet, Context context) {
        itemCartDtoList = dataSet;
        this.mContext = context;
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<OTItemTransferAdapter.MultiSelectDialogViewHolder, OTItemDto> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @NonNull
    @Override
    public OTItemTransferAdapter.MultiSelectDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_transfer, parent, false);
        return new OTItemTransferAdapter.MultiSelectDialogViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final OTItemTransferAdapter.MultiSelectDialogViewHolder holder, int position) {
        try {
//            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            holder.memberTextView.setText(itemCartDtoList.get(position).getAccountNumber() + "-" + itemCartDtoList.get(position).getMemberName());
            holder.otTextView.setText(itemCartDtoList.get(position).getOTNo());
            holder.qtyTextView.setText(itemCartDtoList.get(position).getQuantity());
            holder.itemTextView.setText(itemCartDtoList.get(position).getItemCode() + "-" + itemCartDtoList.get(position).getItemName());
            holder.tableTextView.setText(itemCartDtoList.get(position).getTableNo());

            if (selectedList.size() == 0) {
                holder.imageView.setVisibility(View.GONE);
                holder.relativeLayout.setBackground(mContext.getResources().getDrawable(R.drawable.bg_normal));

            } else {
                for (int i = 0; i < selectedList.size(); i++) {
                    if (selectedList.get(i).equalsIgnoreCase(itemCartDtoList.get(position).getItemCode())) {
                        holder.imageView.setVisibility(View.VISIBLE);
                        holder.relativeLayout.setBackground(mContext.getResources().getDrawable(R.drawable.bg_tab));
                        return;
                    } else {
                        holder.imageView.setVisibility(View.GONE);
                        holder.relativeLayout.setBackground(mContext.getResources().getDrawable(R.drawable.bg_normal));
                    }
                }
            }
//            if (itemCartDtoList.get(position).isClick()) {
//                holder.imageView.setVisibility(View.VISIBLE);
//                holder.relativeLayout.setBackground(mContext.getResources().getDrawable(R.drawable.bg_tab));
//            } else {
//                holder.imageView.setVisibility(View.GONE);
//                holder.relativeLayout.setBackground(mContext.getResources().getDrawable(R.drawable.bg_normal));
//
//            }


        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemCartDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MultiSelectDialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView memberTextView, otTextView, itemTextView, qtyTextView, tableTextView;
        private RelativeLayout relativeLayout;
        private ImageView imageView;

        MultiSelectDialogViewHolder(View view) {
            super(view);
            memberTextView = view.findViewById(R.id.text_viewMember);
            otTextView = view.findViewById(R.id.text_viewOT);
            tableTextView = view.findViewById(R.id.text_viewTable);
            itemTextView = view.findViewById(R.id.text_viewItem);
            qtyTextView = view.findViewById(R.id.text_viewQty);
            imageView = view.findViewById(R.id.image_view);
            relativeLayout = view.findViewById(R.id.relativeLayout);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            try {
                boolean isAdded = false;
                for (int i = 0; i < selectedList.size(); i++) {
                    if (selectedList.get(i).equals(itemCartDtoList.get(getAdapterPosition()).getItemCode())) {
                        isAdded = true;
                        selectedList.remove(i);
                    }
                }
                if (!isAdded) {
                    selectedList.add(itemCartDtoList.get(getAdapterPosition()).getItemCode());
                }
                notifyDataSetChanged();
//                if (onRecyclerViewItemClickListener != null)
//                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, itemCartDtoList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
}




