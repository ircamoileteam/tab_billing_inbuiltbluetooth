package com.irca.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.dto.ProvisionalDto2;
import com.irca.fields.OnRecyclerViewItemClickListener;

import java.text.DecimalFormat;
import java.util.List;

public class ProvisionalBillAdapter extends RecyclerView.Adapter<ProvisionalBillAdapter.MultiSelectDialogViewHolder> {

    private List<ProvisionalDto2> itemCartDtoList;
    private Context mContext;
    private OnRecyclerViewItemClickListener<ProvisionalBillAdapter.MultiSelectDialogViewHolder, ProvisionalDto2> onRecyclerViewItemClickListener;

    public ProvisionalBillAdapter(List<ProvisionalDto2> dataSet, Context context) {
        itemCartDtoList = dataSet;
        this.mContext = context;
    }

    public void filterList(List<ProvisionalDto2> dataList, Context context) {
        this.itemCartDtoList = dataList;
        this.mContext = context;
        notifyDataSetChanged();
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<ProvisionalBillAdapter.MultiSelectDialogViewHolder, ProvisionalDto2> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @NonNull
    @Override
    public ProvisionalBillAdapter.MultiSelectDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_pbill_item, parent, false);
        return new ProvisionalBillAdapter.MultiSelectDialogViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ProvisionalBillAdapter.MultiSelectDialogViewHolder holder, int position) {
        try {
//            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            holder.nameTextView.setText(itemCartDtoList.get(position).getItemName());
            holder.rateTextView.setText(itemCartDtoList.get(position).getTRate());
            holder.qtyTextView.setText(itemCartDtoList.get(position).getQuantity());
            DecimalFormat precision = new DecimalFormat("0.00");

            holder.taxTextView.setText("" + precision.format(Double.parseDouble(itemCartDtoList.get(position).getCgst())));
//            holder.sgstTextView.setText("" + precision.format(Double.parseDouble(itemCartDtoList.get(position).getSgst())));

            Double d = Double.parseDouble(itemCartDtoList.get(position).getTRate()) * Double.parseDouble(itemCartDtoList.get(position).getQuantity());
            Double d1 = Double.parseDouble(itemCartDtoList.get(position).getSgst()) +
                    Double.parseDouble(itemCartDtoList.get(position).getCgst()) + Double.parseDouble(itemCartDtoList.get(position).getVatAmount())
                    + Double.parseDouble(itemCartDtoList.get(position).getServiceChargeAmount())
                    + (Double.parseDouble(itemCartDtoList.get(position).getCessvalue())* Double.parseDouble(itemCartDtoList.get(position).getQuantity()))
                    + Double.parseDouble(itemCartDtoList.get(position).getCess())
                    ;
            Double d2 = d + d1;
            holder.taxTextView.setText("" + precision.format(d1));
            //holder.amtTextView.setText("" + precision.format(d2));
            holder.amtTextView.setText("" + precision.format(d));
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemCartDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MultiSelectDialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nameTextView, rateTextView, qtyTextView, taxTextView, sgstTextView, amtTextView;

        MultiSelectDialogViewHolder(View view) {
            super(view);
            nameTextView = view.findViewById(R.id.itmName);
            rateTextView = view.findViewById(R.id.rate);
            qtyTextView = view.findViewById(R.id.itmQty);
            taxTextView = view.findViewById(R.id.tax);
            sgstTextView = view.findViewById(R.id.sgst);
            amtTextView = view.findViewById(R.id.amt);
        }

        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, itemCartDtoList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }

    }
}

