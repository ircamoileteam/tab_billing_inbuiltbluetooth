package com.irca.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.dto.ClickListener;
import com.irca.fields.Card;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AccountlistAdapter extends RecyclerView.Adapter<AccountlistAdapter.MyviewHolder> {
    Context context;
    ArrayList<Card> masterStore;
    private ClickListener listener;

    public AccountlistAdapter(Context context, ArrayList<Card> masterStore, ClickListener listener)
    {
        this.context=context;
        this.masterStore=masterStore;
        this.listener = listener;

    }
    class MyviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView account,cardName,steward,memberName,tableNO,otdate;        private WeakReference<ClickListener> listenerRef;

        public MyviewHolder(@NonNull View itemView) {

            super(itemView);
            listenerRef = new WeakReference<>(listener);

            account = (TextView)itemView.findViewById(R.id.account);
            cardName = (TextView)itemView.findViewById(R.id.cardtype);
            steward=(TextView)itemView.findViewById(R.id.steward);
            memberName=(TextView)itemView.findViewById(R.id.MemberName);
            tableNO=(TextView)itemView.findViewById(R.id.tableno);
            otdate=(TextView)itemView.findViewById(R.id.otdate);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            //  Toast.makeText(v.getContext(), "ROW PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();

            listenerRef.get().onPositionClicked(getAdapterPosition());

        }
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View    convertView = inflater.inflate(R.layout.custom_accountlist, viewGroup,false);
        return new MyviewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder myviewHolder, int i) {

        String hifun=masterStore.get(i).getAccount();
        if(hifun.contains("#"))
        {
            String ss[]=masterStore.get(i).getAccount().split("#");
            myviewHolder.account.setText(ss[0]);
            myviewHolder.cardName.setText(masterStore.get(i).getCardtype());
            myviewHolder.steward.setText(masterStore.get(i).getFirstName());
            myviewHolder.memberName.setText(masterStore.get(i).getDummy_account());
            myviewHolder.tableNO.setText("Table No.  :  " + masterStore.get(i).getTableNO());
            Date dateIn = null;
            try {
                //   dateIn= new SimpleDateFormat("dd-MM-yyyy").parse(masterStore.get(position).getOtdate());
                dateIn= new SimpleDateFormat("yyyy-MM-dd").parse(masterStore.get(i).getOtdate());
                String formatteddateIn = new SimpleDateFormat("dd MMM yyyy").format(dateIn);
                myviewHolder.otdate.setText(formatteddateIn);
            } catch (ParseException e) {

            }
            //     otdate.setText( masterStore.get(position).getTableNO());
            //  steward.setText("");

            //   String st[]=waiterDetailses.get(position).getFirstName().toString().split("#");
            //   steward.setText(st[0]);
            //  steward.setText(waiterDetailses.get(position).getFirstName().toString());


        }
        else
        {
            myviewHolder.account.setText(masterStore.get(i).getAccount());
            myviewHolder.cardName.setText(masterStore.get(i).getCardtype());
            myviewHolder.steward.setText(masterStore.get(i).getFirstName());
            myviewHolder.steward.setText("");
            myviewHolder.memberName.setText(masterStore.get(i).getDummy_account());
            myviewHolder.tableNO.setText("Table No.  :" +masterStore.get(i).getTableNO());
        }


    }

    @Override
    public int getItemCount() {
        return masterStore.size();
    }
    public void updateList(ArrayList<Card> list){
        masterStore = list;
        Log.d("list:", list.size()+ "");
        Log.d("masterStore list:", masterStore.size()+ "");
        notifyDataSetChanged();
    }

}
