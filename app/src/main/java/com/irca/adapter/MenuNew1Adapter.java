package com.irca.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.irca.cosmo.R;
import com.irca.dto.MenuNewDto;
import com.irca.fields.OnRecyclerViewItemClickListener;

import java.util.List;

public class MenuNew1Adapter extends RecyclerView.Adapter<MenuNew1Adapter.MultiSelectDialogViewHolder> {

    private List<MenuNewDto> itemCartDtoList;
    private Context mContext;
    private OnRecyclerViewItemClickListener<MenuNew1Adapter.MultiSelectDialogViewHolder, MenuNewDto> onRecyclerViewItemClickListener;

    public MenuNew1Adapter(List<MenuNewDto> dataSet, Context context) {
        itemCartDtoList = dataSet;
        this.mContext = context;
    }


    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<MultiSelectDialogViewHolder, MenuNewDto> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @NonNull
    @Override
    public MenuNew1Adapter.MultiSelectDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_menu_new_item, parent, false);
        return new MenuNew1Adapter.MultiSelectDialogViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MenuNew1Adapter.MultiSelectDialogViewHolder holder, int position) {
        try {
            holder.menuTextView.setText(itemCartDtoList.get(position).getItemGroup());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            MenuNew2Adapter menuNew2Adapter = new MenuNew2Adapter(itemCartDtoList.get(position).getMenuNewDtoList(), mContext);
            holder.recyclerView.setLayoutManager(linearLayoutManager);
            holder.recyclerView.setAdapter(menuNew2Adapter);
            menuNew2Adapter.notifyDataSetChanged();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemCartDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MultiSelectDialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView menuTextView;
        private RecyclerView recyclerView;

        MultiSelectDialogViewHolder(View view) {
            super(view);
            menuTextView = view.findViewById(R.id.text_viewMenu);
            recyclerView = view.findViewById(R.id.recycler_view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            try {
//                boolean isAdded = false;
//                for (int i = 0; i < selectedList.size(); i++) {
//                    if (selectedList.get(i).equals(itemCartDtoList.get(getAdapterPosition()).getTableNumber())) {
//                        isAdded = true;
//                        selectedList.remove(i);
//                    }
//                }
//                if (!isAdded) {
//                    selectedList.add(itemCartDtoList.get(getAdapterPosition()).getTableNumber());
//                }
//                notifyDataSetChanged();
//                if (onRecyclerViewItemClickListener != null)
//                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, itemCartDtoList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
}




