package com.irca.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.dto.PendingBillsDto;
import com.irca.dto.PromotionsDto;
import com.irca.fields.OnRecyclerViewItemClickListener;

import java.util.List;
import java.util.Random;

public class PendingBillsAdapter extends RecyclerView.Adapter<PendingBillsAdapter.MultiSelectDialogViewHolder> {

    private List<PendingBillsDto> itemCartDtoList;
    private Context mContext;
    private OnRecyclerViewItemClickListener<PendingBillsAdapter.MultiSelectDialogViewHolder, PendingBillsDto> onRecyclerViewItemClickListener;

    public PendingBillsAdapter(List<PendingBillsDto> dataSet, Context context) {
        itemCartDtoList = dataSet;
        this.mContext = context;
    }


    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<PendingBillsAdapter.MultiSelectDialogViewHolder, PendingBillsDto> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @NonNull
    @Override
    public PendingBillsAdapter.MultiSelectDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_pending_bills, parent, false);
        return new PendingBillsAdapter.MultiSelectDialogViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final PendingBillsAdapter.MultiSelectDialogViewHolder holder, int position) {
        try {
//            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            holder.otTextView.setText(itemCartDtoList.get(position).getOTNo());
            holder.mQtyTextView.setText(itemCartDtoList.get(position).getQuantity());
            holder.mItemTextView.setText(itemCartDtoList.get(position).getItemCode() + " - " + itemCartDtoList.get(position).getItemName());
            holder.memberTextView.setText(itemCartDtoList.get(position).getAccountNumber() + " - " + itemCartDtoList.get(position).getMemberName());
            holder.noteTextView.setText(itemCartDtoList.get(position).getoTNote());
            if (itemCartDtoList.get(position).getBillDate().contains("T")) {
                holder.dateTextView.setText(itemCartDtoList.get(position).getBillDate().split("T")[0]);
            } else {
                holder.dateTextView.setText(itemCartDtoList.get(position).getBillDate());
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemCartDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MultiSelectDialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView otTextView, dateTextView,noteTextView, mQtyTextView, mItemTextView, memberTextView;

        MultiSelectDialogViewHolder(View view) {
            super(view);
            otTextView = view.findViewById(R.id.text_viewOT);
            mItemTextView = view.findViewById(R.id.text_viewItem);
            mQtyTextView = view.findViewById(R.id.text_viewMQty);
            memberTextView = view.findViewById(R.id.text_viewMember);
            dateTextView = view.findViewById(R.id.text_viewDate);
            noteTextView = view.findViewById(R.id.text_viewNote);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, itemCartDtoList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}


