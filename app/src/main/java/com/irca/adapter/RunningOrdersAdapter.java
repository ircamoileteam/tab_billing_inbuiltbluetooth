package com.irca.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.irca.ServerConnection.ConnectionDetector;
import com.irca.activity.ModsShowActivity;
import com.irca.activity.ProvisionalBillActivity;
import com.irca.activity.RunningOrdersActivity;
import com.irca.activity.TableTransferActivity;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.dto.TableTransferDetails;
import com.irca.fields.OnRecyclerViewItemClickListener;

import org.json.JSONObject;

import java.util.List;
import java.util.Objects;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class RunningOrdersAdapter extends RecyclerView.Adapter<RunningOrdersAdapter.MultiSelectDialogViewHolder> {

    private List<TableTransferDetails> itemCartDtoList;
    private Activity mContext;
    private String userId = "";
    private OnRecyclerViewItemClickListener<RunningOrdersAdapter.MultiSelectDialogViewHolder, TableTransferDetails> onRecyclerViewItemClickListener;

    public RunningOrdersAdapter(List<TableTransferDetails> dataSet, Activity context, String userId) {
        itemCartDtoList = dataSet;
        this.mContext = context;
        this.userId = userId;
    }

    public void filterList(List<TableTransferDetails> dataList, Activity context, String userId) {
        this.itemCartDtoList = dataList;
        this.mContext = context;
        this.userId = userId;
        notifyDataSetChanged();
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<RunningOrdersAdapter.MultiSelectDialogViewHolder, TableTransferDetails> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @NonNull
    @Override
    public RunningOrdersAdapter.MultiSelectDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_running_orders_item, parent, false);
        return new RunningOrdersAdapter.MultiSelectDialogViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RunningOrdersAdapter.MultiSelectDialogViewHolder holder, int position) {
        try {
//            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            holder.otTextView.setText(itemCartDtoList.get(position).getOTNo());
            holder.memberTextView.setText(itemCartDtoList.get(position).getAccountnumber() + "-" + itemCartDtoList.get(position).getMembername());
            holder.idTextView.setText(itemCartDtoList.get(position).getBillID());
            holder.tableTextView.setText(itemCartDtoList.get(position).getTableNumber());
            holder.paxTextView.setText(itemCartDtoList.get(position).getPaxCount());
            holder.noteTextView.setText(itemCartDtoList.get(position).getoTNote());
            if (itemCartDtoList.get(position).getBillDate().contains("T")) {
                holder.dateTextView.setText(itemCartDtoList.get(position).getBillDate().split("T")[0]);
            } else {
                holder.dateTextView.setText(itemCartDtoList.get(position).getBillDate());
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemCartDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MultiSelectDialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView otTextView, dateTextView, noteTextView, paxTextView, idTextView, tableTextView, memberTextView;
        private Button button, paxButton;

        MultiSelectDialogViewHolder(View view) {
            super(view);
            idTextView = view.findViewById(R.id.text_viewBillId);
            dateTextView = view.findViewById(R.id.text_viewDate);
            otTextView = view.findViewById(R.id.text_viewOT);
            memberTextView = view.findViewById(R.id.text_viewMember);
            tableTextView = view.findViewById(R.id.text_viewTable);
            paxTextView = view.findViewById(R.id.text_viewPax);
            noteTextView = view.findViewById(R.id.text_viewNote);
            button = view.findViewById(R.id.button);
            paxButton = view.findViewById(R.id.buttonPax);
            button.setOnClickListener(this);
            paxButton.setOnClickListener(this);
            view.setOnClickListener(this);
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void onClick(View view) {
            try {
                switch (view.getId()) {
                    case R.id.button:
                        mContext.startActivity(new Intent(mContext, ProvisionalBillActivity.class).putExtra("billId", itemCartDtoList.get(getAdapterPosition()).getBillID()));
                        break;
                    case R.id.buttonPax:
                        showDialog(itemCartDtoList.get(getAdapterPosition()));
                        break;
                    default:
                        mContext.startActivity(new Intent(mContext, ModsShowActivity.class).putExtra("billId", itemCartDtoList.get(getAdapterPosition()).getBillID()));
                        break;
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        private void showDialog(final TableTransferDetails itemsDto) {
            try {
                final Dialog dialog = new Dialog(mContext);
                dialog.getWindow();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_paxupdate);
                Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                Button button = dialog.findViewById(R.id.buttonOK);
                final Button cancelButton = dialog.findViewById(R.id.buttonCancel);
                final EditText qtyEditText = dialog.findViewById(R.id.edit_textQty);
                qtyEditText.setText(itemsDto.getPaxCount());
                dialog.show();

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                button.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(View v) {
                        if (qtyEditText.getText().toString().trim().isEmpty()) {
                            Toast.makeText(mContext, "Please enter Pax Count", Toast.LENGTH_SHORT).show();
                        } else {
                            hideKeyboardFrom(mContext, qtyEditText);
                            new CallApi().execute(qtyEditText.getText().toString().trim(), itemsDto.getBillID(), itemsDto.getMemberID(), userId);
                        }
                    }
                });
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(mContext);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                ConnectionDetector cd = new ConnectionDetector(mContext);
                boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
//                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.UpdatePaxCount(strings[0], strings[1], strings[2], strings[3]);
                    result = "" + getResult;
                    if (result.contains("true")) {
                        String jsonObject = getResult.getString("Value");
                        if (jsonObject.equalsIgnoreCase("success")) {
                            status = "success";
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(mContext);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        pd.dismiss();
                        Toast.makeText(mContext, "Pax Count updated successfully", Toast.LENGTH_SHORT).show();
                        mContext.finish();
                        break;
                    case "server":
                        Toast.makeText(mContext, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(mContext, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(mContext, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(mContext, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }

    private static void hideKeyboardFrom(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

}

