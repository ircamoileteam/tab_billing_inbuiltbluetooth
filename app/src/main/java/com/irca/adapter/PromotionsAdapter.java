package com.irca.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.dto.PromotionsDto;
import com.irca.dto.TableTransferDetails;
import com.irca.fields.OnRecyclerViewItemClickListener;

import java.util.List;
import java.util.Random;

public class PromotionsAdapter extends RecyclerView.Adapter<PromotionsAdapter.MultiSelectDialogViewHolder> {

    private List<PromotionsDto> itemCartDtoList;
    private Context mContext;
    private OnRecyclerViewItemClickListener<PromotionsAdapter.MultiSelectDialogViewHolder, PromotionsDto> onRecyclerViewItemClickListener;

    public PromotionsAdapter(List<PromotionsDto> dataSet, Context context) {
        itemCartDtoList = dataSet;
        this.mContext = context;
    }


    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<PromotionsAdapter.MultiSelectDialogViewHolder, PromotionsDto> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @NonNull
    @Override
    public PromotionsAdapter.MultiSelectDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_promotions_item, parent, false);
        return new PromotionsAdapter.MultiSelectDialogViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final PromotionsAdapter.MultiSelectDialogViewHolder holder, int position) {
        try {
//            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            holder.promotionTextView.setText(itemCartDtoList.get(position).getPromotionName());
            holder.qtyTextView.setText(itemCartDtoList.get(position).getFreeQty());
            holder.mQtyTextView.setText(itemCartDtoList.get(position).getQuantity());
            holder.validTextView.setText(itemCartDtoList.get(position).getFromDate() + " - " + itemCartDtoList.get(position).getToDate());
            holder.fItemTextView.setText(itemCartDtoList.get(position).getFreeItemCode() + " - " + itemCartDtoList.get(position).getFreeItemName());
            holder.mItemTextView.setText(itemCartDtoList.get(position).getItemCode() + " - " + itemCartDtoList.get(position).getMainItem());
            int[] androidColors = mContext.getResources().getIntArray(R.array.androidColors);
            int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
            holder.colorLinearLayout.setBackgroundColor(randomAndroidColor);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemCartDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MultiSelectDialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView fItemTextView, qtyTextView,mQtyTextView,mItemTextView, promotionTextView, validTextView;
        private LinearLayout colorLinearLayout;

        MultiSelectDialogViewHolder(View view) {
            super(view);
            promotionTextView = view.findViewById(R.id.text_viewPromotion);
            qtyTextView = view.findViewById(R.id.text_viewQty);
            fItemTextView = view.findViewById(R.id.text_viewFItem);
            mQtyTextView = view.findViewById(R.id.text_viewMQty);
            mItemTextView = view.findViewById(R.id.text_viewMItem);
            validTextView = view.findViewById(R.id.text_viewValid);
            colorLinearLayout = view.findViewById(R.id.linear_layoutColor);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, itemCartDtoList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}


