package com.irca.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.irca.cosmo.R;
import com.irca.dto.MenuNewDto;
import com.irca.fields.OnRecyclerViewItemClickListener;

import java.util.List;

public class MenuNew2Adapter extends RecyclerView.Adapter<MenuNew2Adapter.MultiSelectDialogViewHolder> {

    private List<MenuNewDto> itemCartDtoList;
    private Context mContext;
    private OnRecyclerViewItemClickListener<MenuNew2Adapter.MultiSelectDialogViewHolder, MenuNewDto> onRecyclerViewItemClickListener;

    public MenuNew2Adapter(List<MenuNewDto> dataSet, Context context) {
        itemCartDtoList = dataSet;
        this.mContext = context;
    }

    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<MenuNew2Adapter.MultiSelectDialogViewHolder, MenuNewDto> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @NonNull
    @Override
    public MenuNew2Adapter.MultiSelectDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_menu_new1_item, parent, false);
        return new MenuNew2Adapter.MultiSelectDialogViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final MenuNew2Adapter.MultiSelectDialogViewHolder holder, int position) {
        try {
            holder.menuTextView.setText(itemCartDtoList.get(position).getItemName());
            holder.rateTextView.setText("₹ " + itemCartDtoList.get(position).getRate());

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemCartDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MultiSelectDialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView menuTextView, rateTextView;

        MultiSelectDialogViewHolder(View view) {
            super(view);
            menuTextView = view.findViewById(R.id.text_viewMenu);
            rateTextView = view.findViewById(R.id.text_viewRate);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            try {
//                boolean isAdded = false;
//                for (int i = 0; i < selectedList.size(); i++) {
//                    if (selectedList.get(i).equals(itemCartDtoList.get(getAdapterPosition()).getTableNumber())) {
//                        isAdded = true;
//                        selectedList.remove(i);
//                    }
//                }
//                if (!isAdded) {
//                    selectedList.add(itemCartDtoList.get(getAdapterPosition()).getTableNumber());
//                }
//                notifyDataSetChanged();
//                if (onRecyclerViewItemClickListener != null)
//                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, itemCartDtoList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
}





