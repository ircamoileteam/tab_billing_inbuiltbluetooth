package com.irca.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.dto.TableTransferDetails;
import com.irca.fields.OnRecyclerViewItemClickListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TableTransferAdapter extends RecyclerView.Adapter<TableTransferAdapter.MultiSelectDialogViewHolder> {

    private List<TableTransferDetails> itemCartDtoList;
    private Context mContext;
    private OnRecyclerViewItemClickListener<MultiSelectDialogViewHolder, TableTransferDetails> onRecyclerViewItemClickListener;

    public TableTransferAdapter(List<TableTransferDetails> dataSet, Context context) {
        itemCartDtoList = dataSet;
        this.mContext = context;
    }



    public void setOnRecyclerViewItemClickListener(@NonNull OnRecyclerViewItemClickListener<TableTransferAdapter.MultiSelectDialogViewHolder, TableTransferDetails> onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }

    @NonNull
    @Override
    public TableTransferAdapter.MultiSelectDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_transfer_table_item, parent, false);
        return new TableTransferAdapter.MultiSelectDialogViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final TableTransferAdapter.MultiSelectDialogViewHolder holder, int position) {
        try {
//            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            holder.dateTextView.setText(itemCartDtoList.get(position).getBillDate());
            holder.otTextView.setText(itemCartDtoList.get(position).getOTNo());
            holder.memberTextView.setText(itemCartDtoList.get(position).getAccountnumber() + "-" + itemCartDtoList.get(position).getMembername());
            holder.idTextView.setText(itemCartDtoList.get(position).getBillID());
            holder.tableTextView.setText(itemCartDtoList.get(position).getTableNumber());
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemCartDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MultiSelectDialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView otTextView, dateTextView, idTextView, tableTextView, memberTextView;
        private Button button;

        MultiSelectDialogViewHolder(View view) {
            super(view);
            idTextView = view.findViewById(R.id.text_viewBillId);
            dateTextView = view.findViewById(R.id.text_viewDate);
            otTextView = view.findViewById(R.id.text_viewOT);
            memberTextView = view.findViewById(R.id.text_viewMember);
            tableTextView = view.findViewById(R.id.text_viewTable);
            button = view.findViewById(R.id.button);
            button.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            try {
                if (onRecyclerViewItemClickListener != null)
                    onRecyclerViewItemClickListener.onRecyclerViewItemClick(this, view, itemCartDtoList.get(getAdapterPosition()), getAdapterPosition());
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}

