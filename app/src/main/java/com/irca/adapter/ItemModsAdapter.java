package com.irca.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.irca.cosmo.R;
import com.irca.dto.ItemMods;

import java.util.List;

public class ItemModsAdapter extends RecyclerView.Adapter<ItemModsAdapter.MultiSelectDialogViewHolder> {

    private List<ItemMods> itemCartDtoList;
    private Activity mContext;

    public ItemModsAdapter(List<ItemMods> dataSet, Activity context) {
        itemCartDtoList = dataSet;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ItemModsAdapter.MultiSelectDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_mods, parent, false);
        return new ItemModsAdapter.MultiSelectDialogViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ItemModsAdapter.MultiSelectDialogViewHolder holder, int position) {
        try {
            holder.itemTextView.setText(itemCartDtoList.get(position).getItemName());
            holder.modsTextView.setText(itemCartDtoList.get(position).getModifier());


        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemCartDtoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MultiSelectDialogViewHolder extends RecyclerView.ViewHolder {

        private TextView modsTextView, itemTextView;

        MultiSelectDialogViewHolder(View view) {
            super(view);
            itemTextView = view.findViewById(R.id.text_viewItem);
            modsTextView = view.findViewById(R.id.text_viewMods);
        }


    }


}

