package com.irca.Printer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.epson.epos2.discovery.DeviceInfo;
import com.epson.epos2.discovery.Discovery;
import com.epson.epos2.discovery.DiscoveryListener;
import com.epson.epos2.discovery.FilterOption;
import com.irca.ImageCropping.CircularImage;
import com.irca.Utils.ShowMsg;
import com.irca.cosmo.R;

import java.util.ArrayList;
import java.util.HashMap;

public class DiscoveryActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private Context mContext = null;
    private ArrayList<HashMap<String, String>> mPrinterList = null;
    private SimpleAdapter mPrinterListAdapter = null;
    private FilterOption mFilterOption = null;
    SharedPreferences prefrence;
    SharedPreferences.Editor editor;
    ProgressDialog pd;
    ImageView logo;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discovery);

        logo = findViewById(R.id.imageView_logo);
        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.otyclub);
        logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));

        mContext = this;
        prefrence = getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);

        pd = new ProgressDialog(mContext);
        pd.setMessage("Loading");
        pd.setCancelable(true);


        ImageView button = findViewById(R.id.btnRestart);
        button.setOnClickListener(this);

        mPrinterList = new ArrayList<>();
    /* //boat
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("Target", "BT:00:01:90:C7:62:DB");
        hashMap.put("PrinterName", "TM-P20_003205");
        mPrinterList.add(hashMap);

        HashMap<String, String> hashMap1 = new HashMap<>();
        hashMap1.put("Target", "BT:00:01:90:AE:DC:A5");
        hashMap1.put("PrinterName", "TM-P20_013002");
        mPrinterList.add(hashMap1);
        //boat

        HashMap<String, String> hashMap2 = new HashMap<>();
        hashMap2.put("Target", "BT:00:01:90:C7:62:DB");
        hashMap2.put("PrinterName", "TM-P20_003205");
        mPrinterList.add(hashMap2);

        HashMap<String, String> hashMap3 = new HashMap<>();
        hashMap3.put("Target", "BT:00:01:90:AE:EA:73");
        hashMap3.put("PrinterName", "TM-P20_012010");
        mPrinterList.add(hashMap3);

        HashMap<String, String> hashMap4 = new HashMap<>();
        hashMap4.put("Target", "BT:00:01:90:AE:D2:5E");
        hashMap4.put("PrinterName", "TM-P20_012012");
        mPrinterList.add(hashMap4);

        HashMap<String, String> hashMap5 = new HashMap<>();
        hashMap5.put("Target", "BT:00:01:90:AE:ED:BA");
        hashMap5.put("PrinterName", "TM-P20_012822");
        mPrinterList.add(hashMap5);
        HashMap<String, String> hashMap6 = new HashMap<>();
        hashMap6.put("Target", "BT:00:01:90:AE:ED:66");
        hashMap6.put("PrinterName", "TM-P20_012816");
        mPrinterList.add(hashMap6);

        HashMap<String, String> hashMap7 = new HashMap<>();
        hashMap7.put("Target", "BT:00:01:90:B9:26:BD");
        hashMap7.put("PrinterName", "TM-P20_012348");
        mPrinterList.add(hashMap7);*/
//boat*/
       HashMap<String, String> hashMap3 = new HashMap<>();
        hashMap3.put("Target", "BT:00:01:90:88:DA:48");
        hashMap3.put("PrinterName", "TM-P80_002585");
        mPrinterList.add(hashMap3);


        mPrinterListAdapter = new SimpleAdapter(this, mPrinterList, R.layout.list_at, new String[]{"PrinterName", "Target"}, new int[]{R.id.PrinterName, R.id.Target});
        ListView list = findViewById(R.id.lstReceiveData);
        list.setAdapter(mPrinterListAdapter);
        list.setOnItemClickListener(this);

        //   Bundle b=getIntent().getExtras();
        //  positionn=b.getInt("position");

        mFilterOption = new FilterOption();
        mFilterOption.setDeviceType(Discovery.TYPE_PRINTER);

        mFilterOption.setEpsonFilter(Discovery.FILTER_NAME);
        mFilterOption.setEpsonFilter(Discovery.PORTTYPE_ALL);
        try {
            Discovery.start(this, mFilterOption, mDiscoveryListener);
            pd.show();
        } catch (Throwable e) {
            e.printStackTrace();
//            Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            try {
                Discovery.stop();
                if (pd != null)
                    pd.dismiss();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            mFilterOption = null;
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRestart:
                try {
                    restartDiscovery();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                break;

            default:
                // Do nothing
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent();

        HashMap<String, String> item = mPrinterList.get(position);
        intent.putExtra(getString(R.string.title_target), item.get("Target"));
        intent.putExtra(getString(R.string.title_Printer), item.get("PrinterName"));
        //  Bundle b=getIntent().getExtras();
        //   positionn=b.getInt("position");
        //   intent.putExtra("position",positionn);
        setResult(RESULT_OK, intent);
        editor = prefrence.edit();
        editor.putString("Target", item.get("Target"));
        editor.putString("Printer", item.get("PrinterName"));
        editor.apply();

        finish();
    }

    private void restartDiscovery() {
        try {
            Discovery.stop();
            pd.dismiss();
        } catch (Throwable e) {
            e.printStackTrace();
        }


        mPrinterList.clear();
        mPrinterListAdapter.notifyDataSetChanged();

        try {
            Discovery.start(this, mFilterOption, mDiscoveryListener);
            pd.show();
        } catch (Throwable e) {
            ShowMsg.showMsg(e.getMessage(), mContext);
        }
    }

    private DiscoveryListener mDiscoveryListener = new DiscoveryListener() {
        @Override
        public void onDiscovery(final DeviceInfo deviceInfo) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    HashMap<String, String> item = new HashMap<>();
                    item.put("PrinterName", deviceInfo.getDeviceName());
                    item.put("Target", deviceInfo.getTarget());
                    mPrinterList.add(item);
                    mPrinterListAdapter.notifyDataSetChanged();
                    pd.dismiss();
                }
            });
        }
    };
}
