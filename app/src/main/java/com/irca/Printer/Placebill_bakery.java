package com.irca.Printer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.epson.epos2.Epos2Exception;
import com.epson.epos2.Log;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.irca.Utils.ShowMsg;
import com.irca.cosmo.KotItemlist;
import com.irca.fields.ItemList;
import com.irca.cosmo.Bakery;
import com.irca.cosmo.R;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Placebill_bakery extends Activity implements View.OnClickListener, ReceiveListener {

    private Context mContext = null;
    private EditText mEditTarget = null;
    private Printer mPrinter = null;
    ArrayList<ItemList> list;
    ItemList _list;
    String print="";
    SharedPreferences pref1;
    String _target="",_printer="";
    LinearLayout dynamic;
    String mName="",mAcc="";
    String bill_id="";
    int tableNo=0,tableId=0;
    TextView _mName,mCode;
    SharedPreferences pref;
    String counter_name="";
    String waiter="";
    String billType="";
    String type="";
    double servicetax=0,vat=0,GrantTotal=0,billAmt=0,cessTax=0,temp_billamount=0,discount=0;
    TextView itemName, qty, amount, title, itemCount;
    // Button close;

    int count=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_placebill_bakery);
        mContext = this;
        //close=(Button)findViewById(R.id.btnclose);
        dynamic=(LinearLayout)findViewById(R.id.dynAddr_Id_order);
        pref=getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);
        pref1 = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        counter_name=pref1.getString("StoreName", "");
        waiter=pref1.getString("WaiterName", "");
        billType=pref1.getString("billType","");

        _mName=(TextView)findViewById(R.id.mName);
        mCode=(TextView)findViewById(R.id.mCode);


        _target=pref.getString("Target", "");
        _printer=pref.getString("Printer", "");


        Bundle b=getIntent().getExtras();
        mName=b.getString("mName");
        mAcc=b.getString("a/c");
        type=b.getString("Type");
        tableId=b.getInt("tableId");

        tableNo=b.getInt("tableNo");
        bill_id=b.getString("bill_id");
        _mName.setText(""+mName);
        mCode.setText("" + mAcc);

        //loadData(Bakery.placeOrder_list1);
        loadData(KotItemlist.placeOrder_list);


        int[] target = {
                R.id.btnDiscovery,
                R.id.btnSampleReceipt,
                R.id.btnclose
        };

        for (int i = 0; i < target.length; i++) {
            Button button = (Button)findViewById(target[i]);
            button.setOnClickListener(this);
        }

        try {
            Log.setLogSettings(mContext, Log.PERIOD_TEMPORARY, Log.OUTPUT_STORAGE, null, 0, 1, Log.LOGLEVEL_LOW);
        }
        catch (Exception e) {
            ShowMsg.showException(e, "setLogSettings", mContext);
        }
        mEditTarget = (EditText)findViewById(R.id.edtTarget1);
        mEditTarget.setText(_printer+"-"+_target);

    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        if (data != null && resultCode == RESULT_OK) {
            String target = data.getStringExtra(getString(R.string.title_target));
            String printer=data.getStringExtra(getString(R.string.title_Printer));
            if (printer != null) {
                EditText mEdtTarget = (EditText)findViewById(R.id.edtTarget1);
                mEdtTarget.setText(printer+"-"+target);

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        KotItemlist.placeOrder_list.clear();
        finish();
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.btnDiscovery:
                //  Find bluetooth
                intent = new Intent(this, DiscoveryActivity.class);
                startActivityForResult(intent, 0);
                break;

            case R.id.btnSampleReceipt:

//Main print
                print=mEditTarget.getText().toString();
                if(print.equals("")){

                    Toast.makeText(this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                }else
                {
                    updateButtonState(false);
                    if (!runPrintReceiptSequence()) {
                        updateButtonState(true);
                    }

                }

                break;
            case R.id.btnclose:
                Bakery.placeOrder_list1.clear();
                finish();
            default:
                // Do nothing
                break;
        }
    }

    private boolean runPrintReceiptSequence() {
        if (!initializeObject()) {
            return false;
        }


        if (!createReceiptData()) {
            finalizeObject();
            return false;
        }



        if (!printData()) {
            finalizeObject();
            return false;
        }
        count=count+1;
        return true;
    }

    private boolean createReceiptData() {
        String method = "";
        StringBuilder textData = new StringBuilder();
        servicetax=0;
        vat=0;
        billAmt=0;
        GrantTotal=0;
        cessTax=0;
        temp_billamount=0;
        discount=0;
        DecimalFormat format=new DecimalFormat("#.##");
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        Calendar cal =Calendar.getInstance();
        SimpleDateFormat format1=new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date=format1.format(cal.getTime());

        ArrayList<ItemList> _closeorderItemsArrayList =Bakery.placeOrder_list1;

        if (mPrinter == null) {
            return false;
        }
        try {
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("BANGALORE CITY INSTITUTE\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("No.8,Pampa Mahakavi Road," + "\n" + "Opp.Makkala Koota," + "\n" + "Basavanagudi,"+ "\n" + "Bangalore-560004" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());



            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            if(count == 0){
                textData.append("Bill No. :" + bill_id + "\n");
            }else{
                textData.append("RE-Print Bill No. :" + bill_id + "\n");
            }

            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("--------------------------------------\n");
            textData.append("Member  Id   " + ": " + mAcc+ "\n");
            textData.append("Member Name  " + ": " +  mName + "\n");

            textData.append("--------------------------------------\n");
            textData.append("Type       " + ":" + _closeorderItemsArrayList.get(0).cardType + "\n");
            // textData.append("Billed By  " + ":" + "           " + "\n");
            textData.append("Date Time  " + ":" + date+ "\n");
            textData.append("Waiter     " + ":" + waiter + "\n");
            textData.append("--------------------------------------\n");

            //

            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append(
                    "Item Name             "

                            + String .format("%4s","Qty")
                            //  + " "
                            + String.format("%8s","Rate")
                            //  + " "
                            + String.format("%8s","Amount")
                            + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());



            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);

            for(int j=0;j<_closeorderItemsArrayList.size();j++){
                textData.append(
                        String.format("%18s",(_closeorderItemsArrayList.get(j).itemName +"                                                ").substring(0,20))
                                // +" "
                                + String.format("%4s",(_closeorderItemsArrayList.get(j).quantity))

                                + String.format("%8s",format.format
                                (Double.parseDouble((_closeorderItemsArrayList.get(j).rate))))
                                // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                +String.format("%10s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).amount))))
                                // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                +"\n");


                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                //TODO include in manoch 25-4

                servicetax=servicetax+Double.parseDouble(_closeorderItemsArrayList.get(j).getTax());
                vat=vat+Double.parseDouble(_closeorderItemsArrayList.get(j).getVat());
                billAmt=billAmt+Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                cessTax=cessTax+Double.parseDouble(_closeorderItemsArrayList.get(j).getCesstax());
                temp_billamount=temp_billamount+Double.parseDouble(_closeorderItemsArrayList.get(j).getBillAmount());
                discount=discount+Double.parseDouble(_closeorderItemsArrayList.get(j).getDiscount());


            }

            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            if( _closeorderItemsArrayList.get(0).cardType.equals("CLUB CARD")){
                GrantTotal=0+0+billAmt;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("Bill Amount" + " :  " + format.format(temp_billamount) + "\n");
                textData.append("Service Tax" + " :  " +  "0.0" + "\n");
                textData.append("Vat        " + " :  " +  "0.0"+ "\n");
                //    textData.append("Cess Tax   " + " :  " +  "0.0"+ "\n");
                textData.append("Discount   " + " :  " +  format.format(discount)+ "\n");


                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }else{
                GrantTotal=servicetax+vat+billAmt;

                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("Bill Amount" + " :  " + format.format(temp_billamount) + "\n");
                textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                textData.append("Vat        " + " :  " +  format.format(vat)+ "\n");
                //   textData.append("Cess Tax   " + " :  " +  format.format(cessTax)+ "\n");
                textData.append("Discount   " + " :  " +  format.format(discount)+ "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }

            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            textData.append("\n");
            textData.append("\n");
            textData.append("(Signature)"+"\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
            textData.append("* Thank you ! We Wish To Serve You Again ");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);

        }
        catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }

    /*private boolean createReceiptData() {
        String method = "";
        StringBuilder textData = new StringBuilder();
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        if (mPrinter == null) {
            return false;
        }
        try {
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            textData.append("BANGALORE GOLF CLUB\n");
            textData.append("KOT /BOT\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            textData.append("--------------------------------------\n");
            textData.append("Table No.  " + tableNo + "\n");
            textData.append("--------------------------------------\n");
            if(billType.contains("KOT")){
                textData.append("KOT No.    "+": " +bill_id + "\n");
            }else{
                textData.append("BOT No.    "+": " +bill_id + "\n");
            }

            textData.append("Counter    "+": "+counter_name+"\n");
            textData.append("Date Time  "+": "+currentDateTimeString+"\n");
            textData.append("Waiter     "+": "+waiter+"\n");
            textData.append("--------------------------------------\n");
            textData.append("Member      "+":"+mName+"\n");
            textData.append("Member Code "+":"+mAcc+"\n");
            textData.append("--------------------------------------\n");
            textData.append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(0, 0, 0, Printer.COLOR_2);
           *//* textData.append("Code  "
                           +" "
                           +"Item Name             "
                           +" "
                           +"Qty "
                           +" "
                           +"Rate "
                           +"\n"); *//*

            textData.append("Code  "
                    +" "
                    +"Item Name                   "
                           *//*+" "
                           +"Qty "*//*
                    +" "
                    +"Qty "
                    +"\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());



            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B );

            for(int j=0;j< Bakery.placeOrder_list1.size();j++){
                textData.append(

                        (Bakery.placeOrder_list1.get(j).itemCode+"          ").substring(0,6)
                                +" "
                                +String .format("%28s",(Bakery.placeOrder_list1.get(j).itemName+"                                   " ).substring(0,28))
                              *//* + " "
                               +(Bakery.placeOrder_list1.get(j).quantity+"      ").substring(0,4)*//*
                                // +" "
                                +String .format("%4s",(Bakery.placeOrder_list1.get(j).quantity))
                                +"\n");

                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }

            textData.append("------------------------------------------\n");

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);

        }
        catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }
*/

    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            return false;
        }

        PrinterStatusInfo status = mPrinter.getStatus();

        dispPrinterWarnings(status);

        if (!isPrintable(status)) {
            ShowMsg.showMsg(makeErrorMessage(status), mContext);
            try {
                mPrinter.disconnect();
            }
            catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        }
        catch (Exception e) {
            ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            }
            catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    private boolean initializeObject() {
        try {
               /* mPrinter = new Printer(((SpnModelsItem) mSpnSeries.getSelectedItem()).getModelConstant(),
                        ((SpnModelsItem) mSpnLang.getSelectedItem()).getModelConstant(),
                        mContext);*/
            mPrinter= new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
            //    mPrinter= new Printer(Printer.TM_P80, Printer.MODEL_ANK, mContext);
        }
        catch (Exception e) {
            ShowMsg.showException(e, "Printer", mContext);
            return false;
        }

        mPrinter.setReceiveEventListener(this);

        return true;
    }

    private void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    private boolean connectPrinter() {
        boolean isBeginTransaction = false;
        String ll="";

        if (mPrinter == null) {
            return false;
        }

        try {
            ll=mEditTarget.getText().toString();
            if(!ll.equals("")){
                mPrinter.connect(mEditTarget.getText().toString(), Printer.PARAM_DEFAULT);
            }else{
                String[] l=ll.split("-");
                mPrinter.connect(l[1], Printer.PARAM_DEFAULT);
            }
        }
        catch (Exception e) {
            ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        try {
            mPrinter.beginTransaction();
            isBeginTransaction = true;
        }
        catch (Exception e) {
            ShowMsg.showException(e, "beginTransaction", mContext);
        }

        if (isBeginTransaction == false) {
            try {
                mPrinter.disconnect();
            }
            catch (Epos2Exception e) {
                // Do nothing
                return false;
            }
        }

        return true;
    }

    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        }
        catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "endTransaction", mContext);
                }
            });
        }

        try {
            mPrinter.disconnect();
        }
        catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "disconnect", mContext);
                }
            });
        }

        finalizeObject();
    }

    private boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        }
        else if (status.getOnline() == Printer.FALSE) {
            return false;
        }
        else {
            ;//print available
        }

        return true;
    }

    private String makeErrorMessage(PrinterStatusInfo status) {
        String msg = "";

        if (status.getOnline() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_offline);
        }
        if (status.getConnection() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_no_response);
        }
        if (status.getCoverOpen() == Printer.TRUE) {
            msg += getString(R.string.handlingmsg_err_cover_open);
        }
        if (status.getPaper() == Printer.PAPER_EMPTY) {
            msg += getString(R.string.handlingmsg_err_receipt_end);
        }
        if (status.getPaperFeed() == Printer.TRUE || status.getPanelSwitch() == Printer.SWITCH_ON) {
            msg += getString(R.string.handlingmsg_err_paper_feed);
        }
        if (status.getErrorStatus() == Printer.MECHANICAL_ERR || status.getErrorStatus() == Printer.AUTOCUTTER_ERR) {
            msg += getString(R.string.handlingmsg_err_autocutter);
            msg += getString(R.string.handlingmsg_err_need_recover);
        }
        if (status.getErrorStatus() == Printer.UNRECOVER_ERR) {
            msg += getString(R.string.handlingmsg_err_unrecover);
        }
        if (status.getErrorStatus() == Printer.AUTORECOVER_ERR) {
            if (status.getAutoRecoverError() == Printer.HEAD_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_head);
            }
            if (status.getAutoRecoverError() == Printer.MOTOR_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_motor);
            }
            if (status.getAutoRecoverError() == Printer.BATTERY_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_battery);
            }
            if (status.getAutoRecoverError() == Printer.WRONG_PAPER) {
                msg += getString(R.string.handlingmsg_err_wrong_paper);
            }
        }
        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_0) {
            msg += getString(R.string.handlingmsg_err_battery_real_end);
        }

        return msg;
    }

    private void dispPrinterWarnings(PrinterStatusInfo status) {
        EditText edtWarnings = (EditText)findViewById(R.id.edtWarnings);
        String warningsMsg = "";

        if (status == null) {
            return;
        }

        if (status.getPaper() == Printer.PAPER_NEAR_END) {
            warningsMsg += getString(R.string.handlingmsg_warn_receipt_near_end);
        }

        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_1) {
            warningsMsg += getString(R.string.handlingmsg_warn_battery_near_end);
        }

        edtWarnings.setText(warningsMsg);
    }

    private void updateButtonState(boolean state) {
        Button btnReceipt = (Button)findViewById(R.id.btnSampleReceipt);
        //Button btnCoupon = (Button)findViewById(R.id.btnSampleCoupon);

        if(state==true){
            if(count==0){
                btnReceipt.setEnabled(state);
                btnReceipt.setBackgroundColor(getResources().getColor(R.color.pi));
                btnReceipt.setText("Print");

            }

            else{
                btnReceipt.setEnabled(state);
                btnReceipt.setBackgroundColor(getResources().getColor(R.color.pi));
                btnReceipt.setText("Re-Print");

            }
        }else{
            btnReceipt.setEnabled(state);
            btnReceipt.setBackgroundColor(getResources().getColor(R.color.control_focused));
            btnReceipt.setText("Printing ...");
        }
        //  btnCoupon.setEnabled(state);
    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
                ShowMsg.showResult(code, makeErrorMessage(status), mContext);

                dispPrinterWarnings(status);

                updateButtonState(true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        disconnectPrinter();
                    }
                }).start();
            }
        });
    }


    private void loadData(final ArrayList<ItemList> list) {
        dynamic.removeAllViews();

        DecimalFormat format=new DecimalFormat("#.##");


        for( int j=0;j<list.size();j++)
        {
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.kotbill_bak, null);
            addView.setId(j);

            final TextView code=(TextView)addView.findViewById(R.id.t1);
            final TextView name=(TextView)addView.findViewById(R.id.t2);
            final TextView qty=(TextView)addView.findViewById(R.id.t3);
            final TextView amt=(TextView)addView.findViewById(R.id.t4);

            code.setText(list.get(j).itemName);
            name.setText(format.format(Double.parseDouble((list.get(j).rate))));
            qty.setText(list.get(j).quantity);
            amt.setText(list.get(j).amount);
            dynamic.addView(addView);
        }




    }


}
