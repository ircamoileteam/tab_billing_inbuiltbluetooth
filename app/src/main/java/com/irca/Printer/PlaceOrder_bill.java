package com.irca.Printer;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.google.gson.JsonObject;
import com.irca.Billing.MakeOrder;
import com.irca.Billing.PlaceOrder;
import com.irca.Utils.DialogUtils;
import com.irca.Utils.SDK_Result;
import com.irca.Utils.ShowMsg;
import com.irca.Utils.SystemInfoUtils;
import com.irca.cosmo.BuildConfig;
import com.irca.cosmo.RestAPI;
import com.irca.db.Dbase;
import com.irca.fields.CloseorderItems;
import com.irca.fields.ItemList;
import com.irca.Billing.Accountlist;
import com.irca.cosmo.R;
import com.irca.scan.PermissionsManager;
import com.zcs.sdk.ConnectTypeEnum;
import com.zcs.sdk.DriverManager;
import com.zcs.sdk.SdkResult;
import com.zcs.sdk.Sys;
import com.zcs.sdk.print.PrnStrFormat;
import com.zcs.sdk.print.PrnTextFont;
import com.zcs.sdk.print.PrnTextStyle;
import com.zcs.sdk.util.StringUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
/*

import io.fabric.sdk.android.services.concurrency.AsyncTask;
*/

public class PlaceOrder_bill extends AppCompatActivity implements ReceiveListener {
    private static final int REQ_CODE_READ_PHONE = 0x01;
    private static final int REQ_CODE_PERMISSIONS = 0x02;
    private final String[] mPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    private ProgressDialog mDialogInit;
    private Context mContext = null;
    private EditText mEditTarget = null;
    // private Printer mPrinter = null;
    int position;
    ArrayList<ItemList> list;
    ItemList _list;
    String print = "";
    SharedPreferences pref1;
    String _target = "", _printer = "";
    String waiternamecode;
    LinearLayout dynamic;
    String mName = "", mAcc = "";
    String bill_id = "";
    int tableId = 0;
    String tableNo = "";
    TextView _mName, mCode, waitername, otno;
    SharedPreferences pref;
    String counter_name = "";
    String waiter = "";
    String billType = "";
    String type = "";
    int count = 0;
    TextView itemName, qty, amount, title, itemCount;
    boolean allowprint = false;
    // Button close;
    Button b_close, b_print, b_discover, btnSampleReceiptnew;
    String clubName = "", clubaddress = "";
    ProgressDialog pd;
    Dbase dbase;
    String method = "";
    StringBuilder textData = new StringBuilder();
    String date = "";
    //new print
    private static final String TAG = "PrintFragment";
    private Printer mPrinter;

    private DriverManager mDriverManager = DriverManager.getInstance();
    private com.zcs.sdk.Printer mPrinter1;

    private ExecutorService mSingleThreadExecutor;

    private PermissionsManager mPermissionsManager;
    private Sys mSys = mDriverManager.getBaseSysDevice();
    public String modifier = "";
    String modifier1 = "";
    String modifier2 = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_order_bill);


        mContext = this;
        pd = new ProgressDialog(this);
        pd.setMessage("Printing Please Wait .......");
        pd.setCancelable(true);
        //pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        //close=(Button)findViewById(R.id.btnclose);
        dynamic = (LinearLayout) findViewById(R.id.dynAddr_Id_order);
        pref = getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);
        pref1 = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        counter_name = pref1.getString("StoreName", "");
        waiter = pref1.getString("WaiterName", "");
        billType = pref1.getString("billType", "");
        clubName = pref1.getString("clubName", "");
        clubaddress = pref1.getString("clubAddress", "");

        _mName = (TextView) findViewById(R.id.mName);
        mCode = (TextView) findViewById(R.id.mCode);
        waitername = (TextView) findViewById(R.id.strwardname);
        otno = (TextView) findViewById(R.id.otno);
        b_close = (Button) findViewById(R.id.btnclose);
        b_print = (Button) findViewById(R.id.btnSampleReceipt);
        btnSampleReceiptnew = (Button) findViewById(R.id.btnSampleReceiptnew);
        b_discover = (Button) findViewById(R.id.btnDiscovery);
        //   b_print.setVisibility(View.GONE);


        _target = pref.getString("Target", "");
        _printer = pref.getString("Printer", "");
        Bundle b = getIntent().getExtras();
        mName = b.getString("mName");
        mAcc = b.getString("a/c");
        type = b.getString("Type");
        position = b.getInt("position");
        waiternamecode = b.getString("waiternamecode");

        tableNo = b.getString("tableNo");
        tableId = b.getInt("tableId");
        bill_id = b.getString("bill_id");
        _mName.setText("" + mName);
        mCode.setText("" + mAcc);
        waitername.setText("Waiter Name: " + waiternamecode);
        loadData(MakeOrder.placeOrder_list);

     /*   for (int j = 0; j < MakeOrder.placeOrder_list.size(); j++) {
            //         getmodifier(MakeOrder.placeOrder_list.get(0).otNo, MakeOrder.placeOrder_list.get(j).itemCode, MakeOrder.placeOrder_list.get(j).quantity);
            new AsyncGetModifiers().execute(MakeOrder.placeOrder_list.get(0).otNo, MakeOrder.placeOrder_list.get(j).itemCode, MakeOrder.placeOrder_list.get(j).quantity, j + "");
        }*/
        new AsyncGetModifiers().execute( );

        btnSampleReceiptnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mSingleThreadExecutor = mDriverManager.getSingleThreadExecutor();
                    mDriverManager = DriverManager.getInstance();
                    mPrinter1 = mDriverManager.getPrinter();

                         /*  print=mEditTarget.getText().toString();
                if(print.equals("")){
                    Toast.makeText(PlaceOrder_bill.this,"Please choose the Bluetooth Device ",Toast.LENGTH_SHORT).show();
                }else */
                /* if(!allowprint) {
                    Toast.makeText(PlaceOrder_bill.this,"Items Not Available",Toast.LENGTH_SHORT).show();// pd.dismiss();
                }
                else
                {*/
                    mSingleThreadExecutor.execute(new Runnable() {
                        @Override
                        public void run() {
                            int printStatus = mPrinter1.getPrinterStatus();
                            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                                PlaceOrder_bill.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(PlaceOrder_bill.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                                    }
                                });
                            } else {
                                try {

                                    createReceiptDatapoona();


/*
                                    if (type.equals("k") || type.equals("kk")) {
                                        createReceiptDatapoona();
                                    } else {
                                        createReceiptData1poona();
                                    }*/
                                    //   createReceiptDatapoona();
                                } catch (Exception e) {
                                    Log.d("catch exp :", e + "");
                                }

                            }
                        }
                    });

                    // progress = ProgressDialog.show(PlaceOrder_bill.this, "Printing", "Printing Please Wait .......", true, false);
                    //  pd.show();
       /*          if(!pd.isShowing())
                    {
                        pd.show();
                        Toast.makeText(mContext, "started", Toast.LENGTH_SHORT).show();
                        b_print.setEnabled(false);
                        if (!runPrintReceiptSequence()) {
                            updateButtonState(true);  }
                            }
                            else
                         {
                        Toast.makeText(mContext, "No", Toast.LENGTH_SHORT).show();
                         }

            try
            {
                b_print.setEnabled(false);
                new AsyncPrinting().execute();
            }catch(Exception e){
                String rr=e.getMessage().toString();
            }



                    new Thread(new Runnable() {
                        public void run() {
                            //loadFeed(); //just load data and prepare the model
                            runOnUiThread(new Runnable() {
                                @Override public void run() {


                                    b_print.setEnabled(false);
                                    if (!runPrintReceiptSequence()) {
                                        updateButtonState(true);

                                    }
                                    //here you can modify UI here
                                    //modifiying UI element happen here and at the end you cancel the progress dialog
                                    //progress.cancel();
                                    pd.dismiss();
                                }
                            }); // runOnUIthread

                        }
                    }).start();//*//*

                     updateButtonState(false);
                    if (!runPrintReceiptSequence()) {
                        updateButtonState(true);

                    }else {
                       // pd.dismiss();
                    }*/


                } catch (Exception e) {
                    Toast.makeText(PlaceOrder_bill.this, "exception e : " + e, Toast.LENGTH_LONG).show();
                }

            }
        });


        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        date = format1.format(cal.getTime());
        b_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                print = mEditTarget.getText().toString();
                if (print.equals("")) {
                    Toast.makeText(PlaceOrder_bill.this, "Please choose the Bluetooth printer to print", Toast.LENGTH_SHORT).show();
                } else if (print.equals("-")) {
                    Toast.makeText(PlaceOrder_bill.this, "Please choose the Bluetooth printer to print  ", Toast.LENGTH_SHORT).show();
                } else if (!allowprint) {
                    Toast.makeText(PlaceOrder_bill.this, "Items Not Available", Toast.LENGTH_SHORT).show();// pd.dismiss();
                } else {

                    try {
                        b_print.setEnabled(false);
                        Activity mActivity = PlaceOrder_bill.this;

                        mActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                new AsyncPrinting().execute();
                            }
                        });
                        //  new AsyncPrinting().execute();
                    } catch (Exception e) {
                        ShowMsg.showException(e, "AsyncTask", mContext);
                    }

                    // pd.show();
//                    try {
//                        mPrinter= new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
//                        mPrinter.setReceiveEventListener(PlaceOrder_bill.this);
//                    }
//                    catch (Exception e) {
//                        ShowMsg.showException(e, "Printer", mContext);
//                    }
                }

            }
        });
        b_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pd.dismiss();
                //    KotItemlist.placeOrder_list.clear();
                //PlaceOrder_new.placeOrder_list.clear();
                MakeOrder.placeOrder_list.clear();
                Intent i = new Intent(PlaceOrder_bill.this, Accountlist.class);
                i.putExtra("tableNo", tableNo);
                i.putExtra("tableId", tableId);
                i.putExtra("position", position);
                startActivity(i);
                finish();


            }
        });

        b_discover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Find bluetooth
                Intent intent = new Intent(PlaceOrder_bill.this, DiscoveryActivity.class);
                startActivityForResult(intent, 0);

            }
        });

//        try {
//            //TODO remove commented
//            Log.setLogSettings(mContext, Log.PERIOD_TEMPORARY, Log.OUTPUT_STORAGE, null, 0, 1, Log.LOGLEVEL_LOW);
//        } catch (Exception e) {
//            ShowMsg.showException(e, "setLogSettings", mContext);
//        }
        mEditTarget = (EditText) findViewById(R.id.edtTarget1);
        mEditTarget.setText(_printer + "-" + _target);

        if (_printer.equalsIgnoreCase("") && _target.equalsIgnoreCase("")) {


//            mEditTarget.setText("TM-P20_011044" + "-" + "BT:00:01:90:AE:93:17");
            //    mEditTarget.setText("TM-P20_012348-BT:00:01:90:B9:26:DB");

//            mEditTarget.setText("TM-P20_000452" + "-" + "BT:00:01:90:C2:AE:77");
        } else {

            mEditTarget.setText(_printer + "-" + _target);
        }


    }


    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        if (data != null && resultCode == RESULT_OK) {
            String target = data.getStringExtra(getString(R.string.title_target));
            String printer = data.getStringExtra(getString(R.string.title_Printer));
            if (printer != null) {
                EditText mEdtTarget = (EditText) findViewById(R.id.edtTarget1);
                mEdtTarget.setText(printer + "-" + target);

            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("OT Print");
            ab.setElevation(0);
            ab.setDisplayHomeAsUpEnabled(true);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //  KotItemlist.placeOrder_list.clear();
        //PlaceOrder_new.placeOrder_list.clear();
        MakeOrder.placeOrder_list.clear();
        Intent i = new Intent(PlaceOrder_bill.this, Accountlist.class);
        i.putExtra("tableNo", tableNo);
        i.putExtra("tableId", tableId);
        i.putExtra("position", position);
        startActivity(i);
        finish();
    }

/*
    public boolean runPrintReceiptSequence() {
        //  pd.show();
        if (!initializeObject()) {
            return false;
        }

        if (type.equals("k") || type.equals("kk")) {
            if (!createReceiptData()) {
                finalizeObject();
                return false;
            }
        } else {
            if (!createReceiptData1()) {
                finalizeObject();
                return false;
            }
        }


        if (!printData()) {
            finalizeObject();
            return false;
        }
        count = count + 1;

        return true;
    }*/

    //******
    public boolean createReceiptData() {
        String method = "";
        StringBuilder textData = new StringBuilder();
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());

        if (mPrinter == null) {
            return false;
        }
        try {
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            //  textData.append("Cosmopolitan Club (Regd.)\n");
            textData.append(clubName + "\n");
            if (count == 0) {
                textData.append("KOT /BOT\n");
            } else {
                textData.append("KOT /BOT (RE-Print)\n");
            }

            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            textData.append("--------------------------------------\n");
            textData.append("Table No.  " + tableNo + "\n");
            textData.append("--------------------------------------\n");
         /*   if (billType.contains("KOT")) {
                textData.append("KOT No.    " + ": " + bill_id + "\n");
            } else {
                textData.append("BOT No.    " + ": " + bill_id + "\n");
            }*/
            if (billType.contains("KOT")) {
                textData.append("KOT No.    " + ": " + MakeOrder.placeOrder_list.get(0).otNo + "\n");
            } else {
                textData.append("BOT No.    " + ": " + MakeOrder.placeOrder_list.get(0).otNo + "\n");
            }

            textData.append("Counter    " + ": " + counter_name + "\n");
            textData.append("Date Time  " + ": " + date + "\n");
            //  textData.append("Bill Clerk " + ": " + waiter + "\n");
            textData.append("Steward " + ": " + waiter + "\n");
            textData.append("Waiter     " + ": " + waiternamecode + "\n");
            textData.append("--------------------------------------\n");
            textData.append("Member Name " + ":" + mName + "\n");
            textData.append("Member Code " + ":" + mAcc + "\n");
            textData.append("--------------------------------------\n");
            textData.append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(0, 0, 0, Printer.COLOR_2);
           /* textData.append("Code  "
                           +" "
                           +"Item Name             "
                           +" "
                           +"Qty "
                           +" "
                           +"Rate "
                           +"\n"); */

            textData.append("Code  "
                    + " "
                    + "Item Name                   "
                           /*+" "
                           +"Qty "*/
                    + " "
                    + "Qty "
                    + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);

            for (int j = 0; j < MakeOrder.placeOrder_list.size(); j++) {
                textData.append(

                        (MakeOrder.placeOrder_list.get(j).itemCode + "          ").substring(0, 6)
                                + " "
                                + String.format("%28s", (MakeOrder.placeOrder_list.get(j).itemName + "                                   ").substring(0, 28))
                              /* + " "
                               +(PlaceOrder_new.placeOrder_list.get(j).quantity+"      ").substring(0,4)*/
                                // +" "
                                + String.format("%4s", (MakeOrder.placeOrder_list.get(j).quantity))
                                + "\n");

                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }

            textData.append("------------------------------------------\n");
            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);

        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }

    public boolean createReceiptDatapoona() {
        String method = "";
        StringBuilder textData = new StringBuilder();
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());

        int printStatus = mPrinter1.getPrinterStatus();
        if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
            PlaceOrder_bill.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(PlaceOrder_bill.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                }
            });
        } else {
            PrnStrFormat format = new PrnStrFormat();
            format.setTextSize(30);
            format.setAli(Layout.Alignment.ALIGN_CENTER);
            format.setFont(PrnTextFont.DEFAULT);

            mPrinter1.setPrintAppendString(clubName, format);
            format.setTextSize(25);
            format.setStyle(PrnTextStyle.NORMAL);
            format.setAli(Layout.Alignment.ALIGN_NORMAL);

            if (count == 0) {
                //  textData.append("KOT /BOT\\n");
                mPrinter1.setPrintAppendString("KOT /BOT", format);

            } else {
                textData.append("KOT /BOT (RE-Print)\n");
                mPrinter1.setPrintAppendString(" ", format);

            }


            mPrinter1.setPrintAppendString("------------------------------------------------------", format);
            mPrinter1.setPrintAppendString("Table No.  " + MakeOrder.placeOrder_list.get(0).Tableno, format);
            mPrinter1.setPrintAppendString("------------------------------------------------------", format);
            format.setAli(Layout.Alignment.ALIGN_CENTER);
            format.setTextSize(30);
            format.setStyle(PrnTextStyle.BOLD);
     /*       if (billType.contains("KOT")) {
                mPrinter1.setPrintAppendString("KOT No.    " + ": " + bill_id, format);

            } else {
                textData.append("BOT No.    " + ": " + bill_id + "\n");
                mPrinter1.setPrintAppendString("BOT No.    " + ": " + bill_id, format);


            }*/

            if (billType.contains("KOT")) {
                mPrinter1.setPrintAppendString("KOT No.    " + ": " + MakeOrder.placeOrder_list.get(0).otNo, format);

            } else {
                textData.append("BOT No.    " + ": " + bill_id + "\n");
                mPrinter1.setPrintAppendString("BOT No.    " + ": " + MakeOrder.placeOrder_list.get(0).otNo, format);


            }

            mPrinter1.setPrintAppendString("--------------------------------------", format);


            format.setAli(Layout.Alignment.ALIGN_NORMAL);
            format.setStyle(PrnTextStyle.NORMAL);
            format.setTextSize(25);
            mPrinter1.setPrintAppendString("Counter    " + ": " + counter_name, format);
            mPrinter1.setPrintAppendString("Date Time  " + ": " + date, format);
            mPrinter1.setPrintAppendString("Steward " + ": " + waiter, format);
            mPrinter1.setPrintAppendString("Waiter     " + ": " + waiternamecode, format);
            mPrinter1.setPrintAppendString("------------------------------------------------------", format);
            format.setTextSize(30);
            format.setStyle(PrnTextStyle.BOLD);
            mPrinter1.setPrintAppendString("Member Name " + ":" + mName, format);
            mPrinter1.setPrintAppendString("Member Code " + ":" + mAcc, format);
            format.setAli(Layout.Alignment.ALIGN_NORMAL);
            format.setStyle(PrnTextStyle.NORMAL);
            format.setTextSize(25);

            mPrinter1.setPrintAppendString("Code  "
                    + " "
                    + "Item Name                               "
                           /*+" "
                           +"Qty "*/
                    + " "
                    + "Qty ", format);

            format.setAli(Layout.Alignment.ALIGN_NORMAL);
            format.setStyle(PrnTextStyle.NORMAL);
            format.setTextSize(25);
            for (int j = 0; j < MakeOrder.placeOrder_list.size(); j++) {

                if (MakeOrder.placeOrder_list.get(j).itemName.length() > 20) {
                    String itemstr1 = MakeOrder.placeOrder_list.get(j).itemName.substring(0, 20);
                    String itemstr2 = "..." + MakeOrder.placeOrder_list.get(j).itemName.substring(20, MakeOrder.placeOrder_list.get(j).itemName.length());

                    String code = (MakeOrder.placeOrder_list.get(j).itemCode + "********************").substring(0, 6);
                    code = code.replace("*", "   ");

                    mPrinter1.setPrintAppendString(

                            (code + " "
                                    + itemstr1 + " "
                                    + String.format("%4s", (MakeOrder.placeOrder_list.get(j).quantity)))
                            , format);
                    mPrinter1.setPrintAppendString(

                            ("          ")

                                    + itemstr2
                            , format);
                } else if (MakeOrder.placeOrder_list.get(j).itemName.length() == 19) {
                    //    String itemname= (MakeOrder.placeOrder_list.get(j).itemName+"----------------------------------------------------------------------------").substring(0,30);
                    String itemname = (MakeOrder.placeOrder_list.get(j).itemName + "********************").substring(0, 20);
                    itemname = itemname.replace("*", "   ");
                    String code = (MakeOrder.placeOrder_list.get(j).itemCode + "********************").substring(0, 6);
                    code = code.replace("*", "   ");

                    String qty = (MakeOrder.placeOrder_list.get(j).quantity + "                                                                           ").substring(0, 4);
                    mPrinter1.setPrintAppendString(


                            (code + " "
                                    + itemname
                                    + " "
                                    + qty), format);
                } else {
                    //    String itemname= (MakeOrder.placeOrder_list.get(j).itemName+"----------------------------------------------------------------------------").substring(0,30);
                    String itemname = (MakeOrder.placeOrder_list.get(j).itemName + "********************").substring(0, 19);
                    itemname = itemname.replace("*", "   ");
                    String code = (MakeOrder.placeOrder_list.get(j).itemCode + "********************").substring(0, 6);
                    code = code.replace("*", "   ");

                    String qty = (MakeOrder.placeOrder_list.get(j).quantity + "                                                                           ").substring(0, 4);
                    mPrinter1.setPrintAppendString(


                            (code + " "
                                    + itemname
                                    + " "
                                    + qty), format);
                }


                //  Toast.makeText(PlaceOrder_bill.this, "Modifier- " + modifier, Toast.LENGTH_LONG).show();
                if (MakeOrder.placeOrder_list.get(j).modifier != null) {
                    mPrinter1.setPrintAppendString("Modifier- " + MakeOrder.placeOrder_list.get(j).modifier, format);//7

                }

            }
            mPrinter1.setPrintAppendString("------------------------------------------------------", format);//7
            mPrinter1.setPrintAppendString(" ", format);
            mPrinter1.setPrintAppendString(" ", format);
            mPrinter1.setPrintAppendString(" ", format);
            mPrinter1.setPrintAppendString(" ", format);
         /*
         mPrinter1.setPrintAppendString("voucher_no" + " 000002 ", format);
         mPrinter1.setPrintAppendString("date"+ " 2018/05/28 ", format);
         mPrinter1.setPrintAppendString("time" + " 00:00:01 ", format);
         format.setTextSize(30);
         format.setStyle(PrnTextStyle.BOLD);
         mPrinter1.setPrintAppendString("amount" + "￥0.01", format);
         format.setStyle(PrnTextStyle.NORMAL);
         format.setTextSize(25);
         mPrinter1.setPrintAppendString("reference" + " ", format);
         mPrinter1.setPrintAppendString(" ", format);*/
/*
            mPrinter1.setPrintAppendString(" --------------------------------------", format);
            mPrinter1.setPrintAppendString(" ", format);
            mPrinter1.setPrintAppendString(" ", format);
            mPrinter1.setPrintAppendString(" ", format);
            mPrinter1.setPrintAppendString(" ", format);*/
            printStatus = mPrinter1.setPrintStart();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                PlaceOrder_bill.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(PlaceOrder_bill.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                    }
                });
            }
        }


        textData = null;

        return true;
    }

    public boolean createReceiptData1() {

        //StringBuilder textData = new StringBuilder();
        if (mPrinter == null) {
            return false;
        }

        try {
            dbase = null;
            dbase = new Dbase(PlaceOrder_bill.this);
            textData = new StringBuilder();
            ArrayList<String> otlist = dbase.getOt();
            for (int i = 0; i < otlist.size(); i++) {
                String itemcategoryid = otlist.get(i);
                printHeader();
                ArrayList<CloseorderItems> otdetsils = dbase.getotDetails(itemcategoryid);
                printBody(otdetsils);
            }
            count++;

        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }

    public boolean createReceiptData1poona() {


        try {
            dbase = null;
            dbase = new Dbase(PlaceOrder_bill.this);
            textData = new StringBuilder();
            ArrayList<String> otlist = dbase.getOt();
            for (int i = 0; i < otlist.size(); i++) {
                String itemcategoryid = otlist.get(i);
                printHeader();
                ArrayList<CloseorderItems> otdetsils = dbase.getotDetails(itemcategoryid);
                printBody(otdetsils);
            }
            count++;

        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }

    private void printHeader() {


        try {
            int printStatus = mPrinter1.getPrinterStatus();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                PlaceOrder_bill.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(PlaceOrder_bill.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                    }
                });
            } else {

                PrnStrFormat format = new PrnStrFormat();
                format.setTextSize(30);
                format.setAli(Layout.Alignment.ALIGN_CENTER);
                format.setFont(PrnTextFont.DEFAULT);

                mPrinter1.setPrintAppendString(clubName, format);

                format.setTextSize(25);
                format.setStyle(PrnTextStyle.NORMAL);
                format.setAli(Layout.Alignment.ALIGN_CENTER);


                if (count == 0) {
                    //  textData.append("KOT /BOT\\n");
                    mPrinter1.setPrintAppendString("KOT /BOT", format);

                } else {
                    mPrinter1.setPrintAppendString("KOT /BOT (RE-Print)", format);
                }


                mPrinter1.setPrintAppendString("--------------------------------------", format);
                mPrinter1.setPrintAppendString("Table No.  " + tableNo, format);
                mPrinter1.setPrintAppendString("--------------------------------------", format);
                format.setAli(Layout.Alignment.ALIGN_CENTER);
                format.setTextSize(30);
                format.setStyle(PrnTextStyle.BOLD);
         /*       if (billType.contains("KOT")) {
                    mPrinter1.setPrintAppendString("KOT No.    " + ": " + bill_id, format);

                } else {
                    textData.append("BOT No.    " + ": " + bill_id + "\n");
                    mPrinter1.setPrintAppendString("BOT No.    " + ": " + bill_id, format);


                }*/
                if (billType.contains("KOT")) {
                    mPrinter1.setPrintAppendString("KOT No.    " + ": " + MakeOrder.placeOrder_list.get(0).otNo, format);

                } else {
                    textData.append("BOT No.    " + ": " + bill_id + "\n");
                    mPrinter1.setPrintAppendString("BOT No.    " + ": " + MakeOrder.placeOrder_list.get(0).otNo, format);


                }

                mPrinter1.setPrintAppendString("--------------------------------------", format);


                format.setAli(Layout.Alignment.ALIGN_NORMAL);
                format.setStyle(PrnTextStyle.NORMAL);
                format.setTextSize(25);
                mPrinter1.setPrintAppendString("Counter    " + ": " + counter_name, format);
                mPrinter1.setPrintAppendString("Date Time  " + ": " + date, format);
                if(waiter.contains("null")){
                    mPrinter1.setPrintAppendString("Steward " + ": " + "", format);

                }
                else {
                    mPrinter1.setPrintAppendString("Steward " + ": " + waiter, format);

                }
                if(waiternamecode.contains("null")){
                    mPrinter1.setPrintAppendString("Waiter     " + ": " + "", format);

                }
                else {
                    mPrinter1.setPrintAppendString("Waiter     " + ": " + waiternamecode, format);

                }
                mPrinter1.setPrintAppendString("--------------------------------------", format);
                format.setTextSize(30);
                format.setStyle(PrnTextStyle.BOLD);
                mPrinter1.setPrintAppendString("Member Name " + ":" + mName, format);
                mPrinter1.setPrintAppendString("Member Code " + ":" + mAcc, format);
                mPrinter1.setPrintAppendString("--------------------------------------", format);

                format.setAli(Layout.Alignment.ALIGN_NORMAL);
                format.setStyle(PrnTextStyle.NORMAL);
                format.setTextSize(25);

                mPrinter1.setPrintAppendString("Code  "
                        + " "
                        + "Item Name                               "
                           /*+" "
                           +"Qty "*/
                        + " "
                        + "Qty ", format);
                printStatus = mPrinter1.setPrintStart();
                if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                    PlaceOrder_bill.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(PlaceOrder_bill.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                        }
                    });
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void printBody(ArrayList<CloseorderItems> otdetsils) {

        try {

            int printStatus = mPrinter1.getPrinterStatus();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                PlaceOrder_bill.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(PlaceOrder_bill.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                    }
                });
            } else {
                //111111111
                //mPrinter1.setPrintAppendBitmap(bitmap, Layout.Alignment.ALIGN_CENTER);
                PrnStrFormat format = new PrnStrFormat();
                format.setTextSize(30);
                format.setAli(Layout.Alignment.ALIGN_CENTER);
                format.setFont(PrnTextFont.DEFAULT);
                for (int j = 0; j < otdetsils.size(); j++) {


                    if (MakeOrder.placeOrder_list.get(j).itemName.length() > 20) {
                        String itemstr1 = MakeOrder.placeOrder_list.get(j).itemName.substring(0, 20);
                        String itemstr2 = "..." + MakeOrder.placeOrder_list.get(j).itemName.substring(20, MakeOrder.placeOrder_list.get(j).itemName.length());

                        String code = (MakeOrder.placeOrder_list.get(j).itemCode + "********************").substring(0, 6);
                        code = code.replace("*", "   ");

                        mPrinter1.setPrintAppendString(

                                (code + " "
                                        + itemstr1 + " "
                                        + String.format("%4s", (MakeOrder.placeOrder_list.get(j).quantity)))
                                , format);
                        mPrinter1.setPrintAppendString(

                                ("          ")

                                        + itemstr2
                                , format);
                    } else if (MakeOrder.placeOrder_list.get(j).itemName.length() == 19) {
                        //    String itemname= (MakeOrder.placeOrder_list.get(j).itemName+"----------------------------------------------------------------------------").substring(0,30);
                        String itemname = (MakeOrder.placeOrder_list.get(j).itemName + "********************").substring(0, 20);
                        itemname = itemname.replace("*", "   ");
                        String code = (MakeOrder.placeOrder_list.get(j).itemCode + "********************").substring(0, 6);
                        code = code.replace("*", "   ");

                        String qty = (MakeOrder.placeOrder_list.get(j).quantity + "                                                                           ").substring(0, 4);
                        mPrinter1.setPrintAppendString(


                                (code + " "
                                        + itemname
                                        + " "
                                        + qty), format);
                    } else {
                        //    String itemname= (MakeOrder.placeOrder_list.get(j).itemName+"----------------------------------------------------------------------------").substring(0,30);
                        String itemname = (MakeOrder.placeOrder_list.get(j).itemName + "********************").substring(0, 19);
                        itemname = itemname.replace("*", "   ");
                        String code = (MakeOrder.placeOrder_list.get(j).itemCode + "********************").substring(0, 6);
                        code = code.replace("*", "   ");

                        String qty = (MakeOrder.placeOrder_list.get(j).quantity + "                                                                           ").substring(0, 4);
                        mPrinter1.setPrintAppendString(


                                (code + " "
                                        + itemname
                                        + " "
                                        + qty), format);
                    }


                }
                printStatus = mPrinter1.setPrintStart();
                if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                    PlaceOrder_bill.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(PlaceOrder_bill.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                        }
                    });
                }

            }

        } catch (Exception e) {

        }
    }

    private void printBodyold(ArrayList<CloseorderItems> otdetsils) {

        try {
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);

            for (int j = 0; j < otdetsils.size(); j++) {

//                if (MakeOrder.placeOrder_list.get(j).status.equalsIgnoreCase("N") || MakeOrder.placeOrder_list.get(j).status.equalsIgnoreCase("CN")) {
//                    continue;
//                }
//                textData.append(
//
//                        //(otdetsils.get(j).getItemCode() + "          ").substring(0, 6)
//                        (otdetsils.get(j).getItemCode() + "          ")
//                                + " "
//                                + String.format("%28s", (otdetsils.get(j).getItemname() + "                                   ").substring(0, 28))
//                              /* + " "
//                               +(PlaceOrder_new.placeOrder_list.get(j).quantity+"      ").substring(0,4)*/
//                                // +" "
//                                + String.format("%4s", (otdetsils.get(j).getQuantity()))
//                                + "\n");

                textData.append(
                        String.format("%28s", (otdetsils.get(j).getItemCode() + "-" + otdetsils.get(j).getItemname() + "                                   ").substring(0, 28))
                                + String.format("%4s", (otdetsils.get(j).getQuantity()))
                                + "\n");

                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }

            //textData.append("------------------------------------------\n");

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);
        } catch (Exception e) {

        }
    }

    public boolean printData() {


        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            return false;
        }

        PrinterStatusInfo status = mPrinter.getStatus();

        dispPrinterWarnings(status);

        if (!isPrintable(status)) {
            ShowMsg.showMsg(makeErrorMessage(status), mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    public boolean initializeObject() {
        try {
               /* mPrinter = new Printer(((SpnModelsItem) mSpnSeries.getSelectedItem()).getModelConstant(),
                        ((SpnModelsItem) mSpnLang.getSelectedItem()).getModelConstant(),
                        mContext);*/

            mPrinter = new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
            //  mPrinter = new Printer(Printer.TM_P80, Printer.MODEL_ANK, mContext);

        } catch (Exception e) {
            ShowMsg.showException(e, "Printer", mContext);
            return false;
        }

        mPrinter.setReceiveEventListener(this);

        return true;
    }

    public void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    public boolean connectPrinter() {
        boolean isBeginTransaction = false;
        String ll = "";

        if (mPrinter == null) {
            return false;
        }

        try {
            ll = mEditTarget.getText().toString();
            if (!ll.equals("")) {
                mPrinter.connect(mEditTarget.getText().toString(), Printer.PARAM_DEFAULT);
            } else {
                String[] l = ll.split("-");
                mPrinter.connect(l[1], Printer.PARAM_DEFAULT);
            }
        } catch (Exception e) {
            ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        try {
            mPrinter.beginTransaction();
            isBeginTransaction = true;
        } catch (Exception e) {
            ShowMsg.showException(e, "beginTransaction", mContext);
        }

        if (!isBeginTransaction) {
            try {
                mPrinter.disconnect();
            } catch (Epos2Exception e) {
                // Do nothing
                return false;
            }
        }

        return true;
    }

    public void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "endTransaction", mContext);
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "disconnect", mContext);
                }
            });
        }

        finalizeObject();
    }

    public boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            //print available
        }

        return true;
    }

    public String makeErrorMessage(PrinterStatusInfo status) {
        String msg = "";

        if (status.getOnline() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_offline);
        }
        if (status.getConnection() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_no_response);
        }
        if (status.getCoverOpen() == Printer.TRUE) {
            msg += getString(R.string.handlingmsg_err_cover_open);
        }
        if (status.getPaper() == Printer.PAPER_EMPTY) {
            msg += getString(R.string.handlingmsg_err_receipt_end);
        }
        if (status.getPaperFeed() == Printer.TRUE || status.getPanelSwitch() == Printer.SWITCH_ON) {
            msg += getString(R.string.handlingmsg_err_paper_feed);
        }
        if (status.getErrorStatus() == Printer.MECHANICAL_ERR || status.getErrorStatus() == Printer.AUTOCUTTER_ERR) {
            msg += getString(R.string.handlingmsg_err_autocutter);
            msg += getString(R.string.handlingmsg_err_need_recover);
        }
        if (status.getErrorStatus() == Printer.UNRECOVER_ERR) {
            msg += getString(R.string.handlingmsg_err_unrecover);
        }
        if (status.getErrorStatus() == Printer.AUTORECOVER_ERR) {
            if (status.getAutoRecoverError() == Printer.HEAD_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_head);
            }
            if (status.getAutoRecoverError() == Printer.MOTOR_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_motor);
            }
            if (status.getAutoRecoverError() == Printer.BATTERY_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_battery);
            }
            if (status.getAutoRecoverError() == Printer.WRONG_PAPER) {
                msg += getString(R.string.handlingmsg_err_wrong_paper);
            }
        }
        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_0) {
            msg += getString(R.string.handlingmsg_err_battery_real_end);
        }

        return msg;
    }

    public void dispPrinterWarnings(PrinterStatusInfo status) {
        EditText edtWarnings = (EditText) findViewById(R.id.edtWarnings);
        String warningsMsg = "";

        if (status == null) {
            return;
        }

        if (status.getPaper() == Printer.PAPER_NEAR_END) {
            warningsMsg += getString(R.string.handlingmsg_warn_receipt_near_end);
        }

        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_1) {
            warningsMsg += getString(R.string.handlingmsg_warn_battery_near_end);
        }

        edtWarnings.setText(warningsMsg);
    }

    public void updateButtonState(boolean state) {


        b_print.setText("Printing ...");
        //b_print.setBackgroundColor(getResources().getColor(R.color.control_focused));
        if (state == true) {
            if (count == 0) {
                b_print.setEnabled(state);
                // b_print.setBackgroundColor(getResources().getColor(R.color.violet));
                b_print.setText("Print");
                // pd.dismiss();


            } else {
                b_print.setEnabled(state);
                b_print.setBackgroundColor(getResources().getColor(R.color.violet));
                b_print.setText("Re-Print");
                //    pd.dismiss();

            }
        } else {
            b_print.setEnabled(state);
            b_print.setBackgroundColor(getResources().getColor(R.color.pink));
            b_print.setText("Printing ...");
        }
        //  btnCoupon.setEnabled(state);
    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
                ShowMsg.showResult(code, makeErrorMessage(status), mContext);

                dispPrinterWarnings(status);

                updateButtonState(true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        disconnectPrinter();
                    }
                }).start();
            }
        });
    }

    public void loadData(final ArrayList<ItemList> list) {


        dynamic.removeAllViews();
        boolean ispresent = false;
        int cout = 0;

        if (type.equals("k")) {

            LayoutInflater layoutInflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView1 = layoutInflater1.inflate(R.layout.kitchen_header, null);
            // addView1.setId(k);
            final TextView header = addView1.findViewById(R.id.header);
            //  header.setText("Placed OT");
            dynamic.addView(addView1);
            for (int j = 0; j < list.size(); j++) {
                LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.kotbill, null);
                addView.setId(j);

                final TextView code = addView.findViewById(R.id.t1);
                final TextView name = addView.findViewById(R.id.t2);
                final TextView qty = addView.findViewById(R.id.t3);

                code.setText(list.get(j).itemCode);
                name.setText(list.get(j).itemName);
                qty.setText(list.get(j).quantity);

                dynamic.addView(addView);
                allowprint = true;
                cout = cout + 1;
            }
        } else if (type.equals("kk")) {
            LayoutInflater layoutInflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView1 = layoutInflater1.inflate(R.layout.kitchen_header, null);
            // addView1.setId(k);
            final TextView header = addView1.findViewById(R.id.header);
            //  header.setText("Placed OT");
            dynamic.addView(addView1);
            for (int j = 0; j < list.size(); j++) {
                LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.kotbill, null);
                addView.setId(j);

                final TextView code = addView.findViewById(R.id.t1);
                final TextView name = addView.findViewById(R.id.t2);
                final TextView qty = addView.findViewById(R.id.t3);

                code.setText(list.get(j).itemCode);
                name.setText(list.get(j).itemName);
                qty.setText(list.get(j).quantity);

                dynamic.addView(addView);
                allowprint = true;
                cout = cout + 1;
            }
            showAlert();
        } else {
            for (int k = 0; k < 3; k++) {
                if (k == 0) { //layout for yes
                    LayoutInflater layoutInflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView1 = layoutInflater1.inflate(R.layout.header, null);
                    addView1.setId(k);
                    final LinearLayout h_bg = addView1.findViewById(R.id.hbg);
                    h_bg.setBackgroundColor(getResources().getColor(R.color.gr));
                    final TextView header = addView1.findViewById(R.id.header);
                    header.setText("Placed OT");
                    dynamic.addView(addView1);
//                    for (int i = 0; i < list.size(); i++) {
//                        if (list.get(i).getItemCode().contains("Transaction error please verify")) {
//                            list.remove(i);
//                        }
//                    }
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).status.equals("N")) {
                            ispresent = true;
                            continue;
                        }
                        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View addView = layoutInflater.inflate(R.layout.bar_itemlist, null);
                        addView.setId(j);
                        final TextView code = addView.findViewById(R.id.kot_itemcode);
                        final TextView name = addView.findViewById(R.id.kot_itemname);
                        final TextView qty = addView.findViewById(R.id.bot_qty);
                        //final TextView stat = (TextView) addView.findViewById(R.id.bot_staus);
                        final TextView stk = addView.findViewById(R.id.bot_stock);
                        otno.setText(list.get(0).otNo);
                        code.setText(list.get(j).itemCode);
                        name.setText(list.get(j).itemName);
                        qty.setText(list.get(j).quantity);
                        //stat.setText(list.get(j).status);
                        stk.setText(list.get(j).stock);
                        dynamic.addView(addView);
                        allowprint = true;
                        cout = cout + 1;
                    }

                } else if (k == 1) {
                    if (ispresent) {
                        if (cout == 0) {
                            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View addView = layoutInflater.inflate(R.layout.bar_itemlist, null);
                            // addView.setId(j);

                            final TextView code = addView.findViewById(R.id.kot_itemcode);
                            final TextView name = addView.findViewById(R.id.kot_itemname);
                            final TextView qty = addView.findViewById(R.id.bot_qty);
                            final TextView stat = addView.findViewById(R.id.bot_staus);
                            final TextView stk = addView.findViewById(R.id.bot_stock);

                            code.setText("");
                            name.setText(" No Item Placed ");
                            qty.setText("");

                            stat.setText("");
                            stk.setText("");
                            dynamic.addView(addView);
                        }
                        LayoutInflater layoutInflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View addView1 = layoutInflater1.inflate(R.layout.header, null);
                        addView1.setId(k);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                        layoutParams.setMargins(0, 40, 0, 0);

                        final LinearLayout h_bg = addView1.findViewById(R.id.hbg);
                        final LinearLayout h_bg1 = addView1.findViewById(R.id.dummy);
                        h_bg.setBackgroundColor(getResources().getColor(R.color.pi));
                        h_bg1.setLayoutParams(layoutParams);
                        final TextView header = addView1.findViewById(R.id.header);
                        header.setText("Stock Not Available");
                        dynamic.addView(addView1);
                    }//layoot for no
                    for (int j = 0; j < list.size(); j++) {

                        if (list.get(j).status.equals("Y") || list.get(j).status.equals("S")) {
                            continue;
                        }
                        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View addView = layoutInflater.inflate(R.layout.bar_itemlist, null);
                        addView.setId(j);

                        final TextView code = addView.findViewById(R.id.kot_itemcode);
                        final TextView name = addView.findViewById(R.id.kot_itemname);
                        final TextView qty = addView.findViewById(R.id.bot_qty);
                        final TextView stat = addView.findViewById(R.id.bot_staus);
                        final TextView stk = addView.findViewById(R.id.bot_stock);

                        String _code = list.get(j).itemCode;
                        code.setText(_code);
                        name.setText(list.get(j).itemName);
                        qty.setText(list.get(j).quantity);
                        if (!_code.equals("")) {
                            stat.setText(list.get(j).status);
                            stat.setTextColor(getResources().getColor(R.color.android_red));
                        } else {
                            stat.setText("");
                            //stat.setTextColor(getResources().getColor(R.color.android_red));
                        }
                        stk.setText(list.get(j).stock);
                        dynamic.addView(addView);
                    }

                } else if (k == 2) {
                    for (int j = 0; j < list.size(); j++) {
                        if (!list.get(j).status.equals("S")) {
                            continue;
                        }
                        showAlert();
                    }
                }
            }


        }


    }

    public void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("POS Printer");
        builder.setMessage("Unable to connect to POS Printer");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // this.finish();
               /* b_print.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        print=mEditTarget.getText().toString();
                        if(print.equals("")){

                            Toast.makeText(getApplicationContext(),"Please choose the Bluetooth Device ",Toast.LENGTH_SHORT).show();
                        }
                        else if(!allowprint)
                        {
                            Toast.makeText(getApplicationContext(),"Items Not Available",Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            updateButtonState(false);
                            if (!runPrintReceiptSequence()) {
                                updateButtonState(true);

                            }
                        }
                    }
                });*/
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public class AsyncPrinting extends AsyncTask<Void, Void, String> {

        String status = "";
        ProgressDialog pd;

        @Override
        protected String doInBackground(Void... params) {

            try {
                mPrinter = new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
                //          mPrinter = new Printer(Printer.TM_P80, Printer.MODEL_ANK, mContext);
                mPrinter.setReceiveEventListener(PlaceOrder_bill.this);
                if (type.equals("k") || type.equals("kk")) {
                    if (!createReceiptData()) {
                        if (mPrinter == null) {
                            status = "false";
                        }
                        mPrinter.clearCommandBuffer();
                        mPrinter.setReceiveEventListener(null);
                        mPrinter = null;
                        status = "false";
                    } else {
                        status = "true";
                    }
                } else {
                    if (!createReceiptData1()) {
                        if (mPrinter == null) {
                            status = "false";
                        }
                        mPrinter.clearCommandBuffer();
                        mPrinter.setReceiveEventListener(null);
                        mPrinter = null;
                        status = "false";
                    } else {
                        status = "true";
                    }
                }

               /* boolean print =printData();

                if(print ==false){
                    if (mPrinter == null) {
                        status="false";
                    }

                    mPrinter.clearCommandBuffer();

                    mPrinter.setReceiveEventListener(null);

                    mPrinter = null;

                }
                else
                {
                    count=count+1;
                    status="true";
                }*/
            } catch (Throwable e) {
                e.printStackTrace();
                status = e.getMessage();
            }

            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                pd = new ProgressDialog(PlaceOrder_bill.this);
                pd.setMessage("Printing Please Wait .......");
                pd.setCancelable(false);
                pd.show();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);


            if (status.equals("true")) {
                try {
                    boolean print = printData();
                    if (!print) {
                        if (mPrinter == null) {
                            status = "false";
                        }

                        mPrinter.clearCommandBuffer();

                        mPrinter.setReceiveEventListener(null);

                        mPrinter = null;
                        b_print.setEnabled(true);

                    } else {
                        count = count + 1;
                        b_print.setEnabled(true);
                        status = "true";
                    }
                } catch (Exception e) {
                    ShowMsg.showException(e, "Post execute error", mContext);
                }
                // updateButtonState(true);
                Toast.makeText(PlaceOrder_bill.this, "Printing Success", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else if (status.equals("")) {
                Toast.makeText(PlaceOrder_bill.this, "Printing failed to enter", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else {
                Toast.makeText(PlaceOrder_bill.this, "Printing failed to enter" + status, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }


    private void checkPermission() {
        mPermissionsManager = new PermissionsManager(PlaceOrder_bill.this) {
            @Override
            public void authorized(int requestCode) {
                if (requestCode == REQ_CODE_READ_PHONE) {
                    initSdk();
                }
            }

            @Override
            public void noAuthorization(int requestCode, String[] lacksPermissions) {
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(PlaceOrder_bill.this);
                builder.setTitle("Warning");
                builder.setMessage("Please open permission");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PermissionsManager.startAppSettings(PlaceOrder_bill.this.getApplicationContext());
                    }
                });
                builder.create().show();
            }

            @Override
            public void ignore(int requestCode) {
                if (requestCode == REQ_CODE_READ_PHONE) {
                    initSdk();
                }
            }
        };
        mPermissionsManager.checkPermissions(PlaceOrder_bill.this, REQ_CODE_READ_PHONE, Manifest.permission.READ_PHONE_STATE);
        mPermissionsManager.checkPermissions(PlaceOrder_bill.this, REQ_CODE_PERMISSIONS, mPermissions);
    }

    void initSdk() {
        initSdk(true);
    }

    int speed = 460800;

    //int speed = 115200;
    private void initSdk(final boolean reset) {
        // Config the SDK base info
        mSys.showLog(true);
        // mSys.showLog( getPreferenceManager().getSharedPreferences().getBoolean(getString(R.string.key_show_log), true));

        if (mDialogInit == null) {
            mDialogInit = (ProgressDialog) DialogUtils.showProgress(PlaceOrder_bill.this, "Waiting", "Initializing");
        } else if (!mDialogInit.isShowing()) {
            mDialogInit.show();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                int statue = mSys.getFirmwareVer(new String[1]);
                if (statue != SdkResult.SDK_OK) {
                    int sysPowerOn = mSys.sysPowerOn();
                    Log.i(TAG, "sysPowerOn: " + sysPowerOn);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                mSys.setUartSpeed(speed);
                final int i = mSys.sdkInit();
                if (i == SdkResult.SDK_OK) {
                    setDeviceInfo();
                }
                if (reset && ++count < 2 && i == SdkResult.SDK_OK && mSys.getCurSpeed() != 460800) {
                    Log.d(TAG, "switch baud rate, cur speed = " + mSys.getCurSpeed());
                    int ret = mSys.setDeviceBaudRate();
                    if (ret != SdkResult.SDK_OK) {
                        DialogUtils.show(PlaceOrder_bill.this, "SwitchBaudRate error: " + ret);
                    }
                    mSys.sysPowerOff();
                    initSdk();
                    return;
                }
                if (PlaceOrder_bill.this != null) {
                    PlaceOrder_bill.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mDialogInit != null)
                                mDialogInit.dismiss();
                            Log.d(TAG, "Cur speed: " + mSys.getCurSpeed());
                            if (BuildConfig.DEBUG && mSys.getConnectType() == ConnectTypeEnum.COM) {
                                DialogUtils.show(PlaceOrder_bill.this, "Cur speed: " + mSys.getCurSpeed());
                            }
                            String initRes = (i == SdkResult.SDK_OK) ? "init_success" : SDK_Result.obtainMsg(PlaceOrder_bill.this, i);

                            Toast.makeText(PlaceOrder_bill.this, initRes, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }

    private void setDeviceInfo() {
        // 读取并判断, 不存在则存入
        byte[] info = new byte[1000];
        byte[] infoLen = new byte[2];
        int getInfo = mSys.getDeviceInfo(info, infoLen);
        if (getInfo == SdkResult.SDK_OK) {
            int len = infoLen[0] * 256 + infoLen[1];
            byte[] newInfo = new byte[len];
            System.arraycopy(info, 0, newInfo, 0, len);
            String infoStr = new String(newInfo);
            Log.i(TAG, "getDeviceInfo: " + getInfo + "\t" + len + "\t" + infoStr);
            if (!TextUtils.isEmpty(infoStr)) {
                String[] split = infoStr.split("\t");
                // 已存则返回
                try {
                    // 确保imei1和mac 存值正确
                    if (split.length >= 4) {
                        String val1 = split[0].split(":")[1];
                        String val4 = split[3].split(":")[1];
                        if (!TextUtils.isEmpty(val1) && !val1.equals("null") && val1.length() >= 15
                                && !TextUtils.isEmpty(val4) && !val4.equals("null") && val4.length() >= 12 && val4.contains(":")) {
                            Log.i(TAG, "Have saved, return");
                            return;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        Map<String, String> map = SystemInfoUtils.getImeiAndMeid(PlaceOrder_bill.this.getApplicationContext());
        String imei1 = map.get("imei1");
        String imei2 = map.get("imei2");
        String meid = map.get("meid");
        String mac = SystemInfoUtils.getMac();
        WifiManager wifi = (WifiManager) PlaceOrder_bill.this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        long start = System.currentTimeMillis();
        while (TextUtils.isEmpty(mac) && System.currentTimeMillis() - start < 5000) {
            if (!wifi.isWifiEnabled()) {
                wifi.setWifiEnabled(true);
            }
            mac = SystemInfoUtils.getMac();
        }
        Log.i(TAG, "mac = " + mac);
        if (TextUtils.isEmpty(mac)) {
            PlaceOrder_bill.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.show(PlaceOrder_bill.this, "Warning! No mac!!!");
                }
            });
            return;
        }
        String msg = "IMEI1:" + (imei1 == null ? "" : imei1) + "\t" + "IMEI2:" + (imei2 == null ? "" : imei2) + "\t" + "MEID:" + (meid == null ? "" : meid) + "\t" + "MAC:" + mac;
        Log.i(TAG, "readDeviceInfo: " + msg);
        byte[] bytes = msg.getBytes();
        int setInfo;
        int count = 0;
        do {
            setInfo = mSys.setDeviceInfo(bytes, bytes.length);
            Log.i(TAG, "setDeviceInfo: " + setInfo);
            if (setInfo == SdkResult.SDK_OK) {
                break;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (count++ < 5);
    }


    public class AsyncGetModifiers extends AsyncTask<String, Void, String> {
        String modifiernew = "";
        String error = "";


        ProgressDialog   progressDialog;

        @Override
        protected String doInBackground(String... strings) {
            modifier = "";
            RestAPI restAPI = new RestAPI(PlaceOrder_bill.this);

            for (int j = 0; j < MakeOrder.placeOrder_list.size(); j++) {
                //         getmodifier(MakeOrder.placeOrder_list.get(0).otNo, MakeOrder.placeOrder_list.get(j).itemCode, MakeOrder.placeOrder_list.get(j).quantity);


                try {
                JSONObject jsonObject = restAPI.getModifiersBluetooth(MakeOrder.placeOrder_list.get(0).otNo, MakeOrder.placeOrder_list.get(j).itemCode,  MakeOrder.placeOrder_list.get(j).quantity);
                JSONObject jsonObject1 = jsonObject.getJSONObject("Value");
                JSONArray jsonArray = jsonObject1.getJSONArray("Table");
                JSONObject jsonObject2 = (JSONObject) jsonArray.get(0);
               modifiernew="";
                modifiernew = jsonObject2.optString("Modifier");

                if (!modifiernew.equalsIgnoreCase("")) {
                    ItemList itemList = MakeOrder.placeOrder_list.get(j);
                    MakeOrder.placeOrder_list.remove(j);
                    itemList.setModifier(modifiernew);
                    MakeOrder.placeOrder_list.add(j,itemList);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            }


            return modifiernew;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(PlaceOrder_bill.this);
            if(!progressDialog.isShowing()){
                progressDialog.setMessage("Loading Please Wait!");
           progressDialog.show();
                progressDialog.setCancelable(false);
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if(!error.equalsIgnoreCase("")){
                Toast.makeText(PlaceOrder_bill.this,"Error : "+error,Toast.LENGTH_LONG).show();
            }
        }
    }


}
