package com.irca.table;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.irca.Billing.BillingProfile;
import com.irca.cosmo.R;
import com.irca.dto.DependentDetails;

import java.util.ArrayList;
import java.util.List;

public class DependentTable extends RelativeLayout {


    String headers1[] =
            {
                    "Dependant Name",
                    "Account Number",
                    "Mobile Number",
                    "Salutation",
                    "Marital Status",
                    "Relation",
                    "Gender",
                    "DateOfBirth",
                    "DateOfJoin",
                    "Member Type",
                    "Remarks",

            };
    float Total = 0;
    TableLayout tableA;
    TableLayout tableB;
    TableLayout tableC;
    TableLayout tableD;

    List<DependentDetails> sampleObjects = null;
    HorizontalScrollView horizontalScrollViewB;
    HorizontalScrollView horizontalScrollViewD;
    ScrollView scrollViewC;
    ScrollView scrollViewD;
    Context context;

    int headerCellsWidth[] = new int[headers1.length];
    private int firstColumnWidth;


    public DependentTable(Context context, List<DependentDetails> result) {

        super(context);
        this.context = context;
        this.sampleObjects = result;

        // initialize the main components (TableLayouts, HorizontalScrollView, ScrollView)
        this.initComponents();
        this.setComponentsId();
        this.setScrollViewAndHorizontalScrollViewTag();


        // no need to assemble component A, since it is just a table
        this.horizontalScrollViewB.addView(this.tableB);

        this.scrollViewC.addView(this.tableC);

        this.scrollViewD.addView(this.horizontalScrollViewD);
        this.horizontalScrollViewD.addView(this.tableD);

        // add the components to be part of the main layout
        this.addComponentToMainLayout();
        this.setBackgroundColor(Color.TRANSPARENT);


        // add some table rows
        this.addTableRowToTableA();
        this.addTableRowToTableB();

        this.resizeHeaderHeight();

        this.getTableRowHeaderCellWidth();

        this.generateTableC_AndTable_B();

        this.resizeBodyTableRowHeight();

        this.tableRowForTableC1();

        this.taleRowForTableD1();

    }

    private void initComponents() {

        this.tableA = new TableLayout(this.context);
        this.tableB = new TableLayout(this.context);
        this.tableC = new TableLayout(this.context);
        this.tableD = new TableLayout(this.context);

        this.horizontalScrollViewB = new MyHorizontalScrollView(this.context);
        this.horizontalScrollViewD = new MyHorizontalScrollView(this.context);

        this.scrollViewC = new MyScrollView(this.context);
        this.scrollViewD = new MyScrollView(this.context);

        this.tableA.setBackgroundColor(Color.BLACK);
        this.horizontalScrollViewB.setBackgroundColor(Color.BLACK);

//        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

    }

    // set essential component IDs
    private void setComponentsId() {
        this.tableA.setId(R.id.tableA);
        this.horizontalScrollViewB.setId(R.id.tableB);
        this.scrollViewC.setId(R.id.tableC);
        this.scrollViewD.setId(R.id.tableD);
    }

    // set tags for some horizontal and vertical scroll view
    private void setScrollViewAndHorizontalScrollViewTag() {

        this.horizontalScrollViewB.setTag("horizontal scroll view b");
        this.horizontalScrollViewD.setTag("horizontal scroll view d");

        this.scrollViewC.setTag("scroll view c");
        this.scrollViewD.setTag("scroll view d");
    }

    // we add the components here in our TableMainLayout
    private void addComponentToMainLayout() {

        // RelativeLayout params were very useful here
        // the addRule method is the key to arrange the components properly

        LayoutParams componentB_Params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        componentB_Params.addRule(RelativeLayout.RIGHT_OF, this.tableA.getId());

        LayoutParams componentC_Params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        componentC_Params.addRule(RelativeLayout.BELOW, this.tableA.getId());

        LayoutParams componentD_Params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        componentD_Params.addRule(RelativeLayout.RIGHT_OF, this.scrollViewC.getId());
        componentD_Params.addRule(RelativeLayout.BELOW, this.horizontalScrollViewB.getId());

        // 'this' is a relative layout,
        // we extend this table layout as relative layout as seen during the creation of this class
        this.addView(this.tableA);
        this.addView(this.horizontalScrollViewB, componentB_Params);
        this.addView(this.scrollViewC, componentC_Params);
        this.addView(this.scrollViewD, componentD_Params);

    }


    private void addTableRowToTableA() {
        this.tableA.addView(this.componentATableRow());
        firstColumnWidth = viewWidth(this.tableA);
    }

    private void addTableRowToTableB() {

        this.tableB.addView(this.componentBTableRow());
    }

    // generate table row of table A
    TableRow componentATableRow() {

        TableRow componentATableRow = new TableRow(this.context);
        TextView textView = this.headerTextView(this.headers1[0]);
        componentATableRow.addView(textView);

        return componentATableRow;
    }


    // generate table row of table B
    TableRow componentBTableRow() {

        TableRow componentBTableRow = new TableRow(this.context);
        int headerFieldCount = this.headers1.length;

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        width -= firstColumnWidth + 4;

        //TableRow.LayoutParams params = new TableRow.LayoutParams(width , LayoutParams.MATCH_PARENT);
        TableRow.LayoutParams params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        params.setMargins(2, 0, 0, 0);

        for (int x = 0; x < (headerFieldCount - 2); x++) {
            TextView textView = this.headerTextView(this.headers1[x + 1]);
            textView.setLayoutParams(params);
            componentBTableRow.addView(textView);
        }

        return componentBTableRow;
    }

    // generate table row of table C and table D
    private void generateTableC_AndTable_B() {
        int count = 1;
        for (DependentDetails sampleObject : this.sampleObjects) {

            TableRow tableRowForTableC = this.tableRowForTableC(sampleObject, count);
            TableRow taleRowForTableD = this.taleRowForTableD(sampleObject, count);

           /* tableRowForTableC.setBackgroundColor(Color.WHITE);
            taleRowForTableD.setBackgroundColor(Color.WHITE);*/

            tableRowForTableC.setBackgroundColor(Color.BLACK);
            taleRowForTableD.setBackgroundColor(Color.BLACK);

            this.tableC.addView(tableRowForTableC);
            this.tableD.addView(taleRowForTableD);
            count++;

        }

    }

    // a TableRow for table C
    TableRow tableRowForTableC(final DependentDetails sampleObject, final int count) {
        String dealer = "";
        dealer = sampleObject.getDependantName();

        TableRow.LayoutParams params = new TableRow.LayoutParams(this.headerCellsWidth[0], LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 2, 0, 0);
        TableRow tableRowForTableC = new TableRow(this.context);
        TextView textView = this.bodyTextView(dealer);
        textView.setBackgroundColor(Color.parseColor("#ffffff"));
//        textView.setTextColor(Color.parseColor("#f44242"));
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(5, 5, 5, 5);
        textView.setTextSize(14);
        textView.setTextColor(context.getResources().getColor(R.color.black));
        if (count % 2 == 0)
            textView.setBackgroundColor(getResources().getColor(R.color.white_pressed));
        else
            textView.setBackgroundColor(getResources().getColor(R.color.white));

        tableRowForTableC.addView(textView, params);
        return tableRowForTableC;
    }

    TableRow taleRowForTableD(final DependentDetails sampleObject, final int count) {

        TableRow taleRowForTableD = new TableRow(this.context);

        int loopCount = ((TableRow) this.tableB.getChildAt(0)).getChildCount();

        final String info[] = {
                sampleObject.getDependantAccountnumber(),
                sampleObject.getMobileNo(),
                sampleObject.getSalutation(),
                sampleObject.getMaritialStatus(),
                sampleObject.getRelation(),
                sampleObject.getSex(),
                sampleObject.getDateOfBirth(),
                sampleObject.getConfirmationDate(),
                sampleObject.getMemberType(),
                sampleObject.getRemarks(),

        };

        for (int x = 0; x < loopCount; x++) {
            TableRow.LayoutParams params = new TableRow.LayoutParams(headerCellsWidth[x + 1], LayoutParams.MATCH_PARENT);
            params.setMargins(2, 2, 0, 0);
            TextView textViewB = this.bodyTextView(info[x]);
            textViewB.setTextSize(14);
            if (count % 2 == 0)
                textViewB.setBackgroundColor(getResources().getColor(R.color.white_pressed));
            else
                textViewB.setBackgroundColor(getResources().getColor(R.color.white));

            textViewB.setId(x);
//            textViewB.setBackgroundColor(getResources().getColor(R.color.white));
            taleRowForTableD.addView(textViewB, params);
            textViewB.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {


                int vid = v.getId();
                    if (vid == 0) {
                      Intent i =new Intent(context, BillingProfile.class);
                        i.putExtra("accountType", sampleObject.getDependantAccountnumber()+"#"+sampleObject.getMemberID()
                                +"#"+sampleObject.getDependantAccountnumber()
                                +"#"+sampleObject.getMemberType()
                                +"#"+"NCM"
                                +"#"+sampleObject.getDependantID()
                                +"#"+"*"
                        );
                        i.putExtra("cardType","DEPENDENT CARD");
                        context.startActivity(i);

                    }

                }
            });

        }
        return taleRowForTableD;

    }

    //TOTAL
    TableRow tableRowForTableC1() {

        TableRow.LayoutParams params = new TableRow.LayoutParams(this.headerCellsWidth[0], LayoutParams.MATCH_PARENT);
        params.setMargins(0, 2, 0, 0);

        TableRow tableRowForTableC = new TableRow(this.context);
        TextView textView = this.bodyTextView("TOTAL");
        textView.setBackgroundColor(Color.parseColor("#DAD5BE"));
        textView.setTextColor(Color.BLACK);
        textView.setGravity(Gravity.LEFT);

        textView.setPadding(5, 5, 5, 5);
        tableRowForTableC.addView(textView, params);

        return tableRowForTableC;
    }

    TableRow taleRowForTableD1() {

        TableRow taleRowForTableD = new TableRow(this.context);

        int loopCount = ((TableRow) this.tableB.getChildAt(0)).getChildCount();
   /*     String info[] = {
    	    sampleObjects.,
            sampleObjects.reportdate,
            sampleObjects.iamount,
            sampleObjects.oamount

   };
   */

        for (int x = 0; x < loopCount; x++) {
            TableRow.LayoutParams params = new TableRow.LayoutParams(headerCellsWidth[x + 1], LayoutParams.MATCH_PARENT);

            params.setMargins(2, 2, 0, 0);
            if (x == 3) {
                TextView textViewB = this.bodyTextView("" + Total);
                taleRowForTableD.addView(textViewB, params);
            } else {
                TextView textViewB = this.bodyTextView("");
                taleRowForTableD.addView(textViewB, params);
            }


        }
        return taleRowForTableD;

    }


    // table cell standard TextView
    TextView bodyTextView(String label) {

        TextView bodyTextView = new TextView(this.context);
        //  bodyTextView.setBackgroundColor(Color.parseColor("#DAD5BE"));
//        bodyTextView.setBackgroundColor(Color.parseColor("#ffffff"));
        //bodyTextView.setBackgroundResource(R.drawable.bg);
        bodyTextView.setText(label);
        bodyTextView.setTextColor(Color.BLACK);
        bodyTextView.setGravity(Gravity.CENTER);
        bodyTextView.setPadding(10, 10, 10, 10);
        bodyTextView.setTextSize(14);
        //bodyTextView.setMargin(5,5,5,5);

        return bodyTextView;
    }

    // header standard TextView
    TextView headerTextView(String label) {

        TextView headerTextView = new TextView(this.context);
        //   headerTextView.setBackgroundColor(Color.parseColor("#c7c096"));
        headerTextView.setBackgroundColor(getResources().getColor(R.color.light_grey));
        headerTextView.setTextColor(context.getResources().getColor(R.color.white));
        headerTextView.setText(label);
        headerTextView.setTextSize(16);
        headerTextView.setGravity(Gravity.CENTER);
        headerTextView.setPadding(20, 20, 20, 20);

        return headerTextView;
    }

    // resizing TableRow height starts here
    void resizeHeaderHeight() {

        TableRow productNameHeaderTableRow = (TableRow) this.tableA.getChildAt(0);
        TableRow productInfoTableRow = (TableRow) this.tableB.getChildAt(0);

        int rowAHeight = this.viewHeight(productNameHeaderTableRow);
        int rowBHeight = this.viewHeight(productInfoTableRow);

        TableRow tableRow = rowAHeight < rowBHeight ? productNameHeaderTableRow : productInfoTableRow;
        int finalHeight = rowAHeight > rowBHeight ? rowAHeight : rowBHeight;

        this.matchLayoutHeight(tableRow, finalHeight);
    }

    void getTableRowHeaderCellWidth() {

        try {
            int tableAChildCount = ((TableRow) this.tableA.getChildAt(0)).getChildCount();
            int tableBChildCount = ((TableRow) this.tableB.getChildAt(0)).getChildCount();


            for (int x = 0; x < (tableAChildCount + tableBChildCount); x++) {

                if (x == 0) {
                    this.headerCellsWidth[x] = this.viewWidth(((TableRow) this.tableA.getChildAt(0)).getChildAt(x));
                } else {
                    this.headerCellsWidth[x] = this.viewWidth(((TableRow) this.tableB.getChildAt(0)).getChildAt(x - 1));
                    // this.headerCellsWidth[x] = ((this.viewWidth(((TableRow) this.tableB.getChildAt(0)))) / 4) - 4;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // resize body table row height
    void resizeBodyTableRowHeight() {

        int tableC_ChildCount = this.tableC.getChildCount();

        for (int x = 0; x < tableC_ChildCount; x++) {

            TableRow productNameHeaderTableRow = (TableRow) this.tableC.getChildAt(x);
            TableRow productInfoTableRow = (TableRow) this.tableD.getChildAt(x);

            int rowAHeight = this.viewHeight(productNameHeaderTableRow);
            int rowBHeight = this.viewHeight(productInfoTableRow);

            TableRow tableRow = rowAHeight < rowBHeight ? productNameHeaderTableRow : productInfoTableRow;
            int finalHeight = rowAHeight > rowBHeight ? rowAHeight : rowBHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }

    // match all height in a table row
    // to make a standard TableRow height
    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }

        return heighestViewPosition == layoutPosition;
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // read a view's width
    private int viewWidth(View view) {
        view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }

    // horizontal scroll view custom class
    class MyHorizontalScrollView extends HorizontalScrollView {

        public MyHorizontalScrollView(Context context) {
            super(context);
        }

        @Override
        protected void onScrollChanged(int l, int t, int oldl, int oldt) {
            String tag = (String) this.getTag();

            if (tag.equalsIgnoreCase("horizontal scroll view b")) {
                horizontalScrollViewD.scrollTo(l, 0);
            } else {
                horizontalScrollViewB.scrollTo(l, 0);
            }
        }

    }

    // scroll view custom class
    class MyScrollView extends ScrollView {

        public MyScrollView(Context context) {
            super(context);
        }

        @Override
        protected void onScrollChanged(int l, int t, int oldl, int oldt) {

            String tag = (String) this.getTag();

            if (tag.equalsIgnoreCase("scroll view c")) {
                scrollViewD.scrollTo(0, t);
            } else {
                scrollViewC.scrollTo(0, t);
            }
        }
    }

}

