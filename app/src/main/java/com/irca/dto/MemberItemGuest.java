package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MemberItemGuest implements Serializable {

    @SerializedName("ItemCode")
    @Expose
    private String ItemCode;

    @SerializedName("ItemName")
    @Expose
    private String ItemName;

    @SerializedName("GuestCount")
    @Expose
    private String GuestCount;


    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getGuestCount() {
        return GuestCount;
    }

    public void setGuestCount(String guestCount) {
        GuestCount = guestCount;
    }


















}
