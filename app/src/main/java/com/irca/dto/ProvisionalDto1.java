package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProvisionalDto1 implements Serializable {
        @SerializedName("BillID")
        @Expose
        private String billID;
        @SerializedName("BillNumber")
        @Expose
        private String billNumber;
        @SerializedName("BillDate")
        @Expose
        private String billDate;
        @SerializedName("Amount")
        @Expose
        private String amount;
        @SerializedName("StoreName")
        @Expose
        private String storeName;
        @SerializedName("StoreCode")
        @Expose
        private String storeCode;
        @SerializedName("barcoderequired")
        @Expose
        private Boolean barcoderequired;
        @SerializedName("MemberID")
        @Expose
        private String memberID;
        @SerializedName("AccountNumber")
        @Expose
        private String accountNumber;
        @SerializedName("MemberName")
        @Expose
        private String memberName;
        @SerializedName("OBalance")
        @Expose
        private String oBalance;
        @SerializedName("CBalance")
        @Expose
        private String cBalance;
        @SerializedName("PersonID")
        @Expose
        private String personID;
        @SerializedName("StewardCode")
        @Expose
        private String stewardCode;
        @SerializedName("StewardName")
        @Expose
        private String stewardName;
        @SerializedName("CreatedBy")
        @Expose
        private String createdBy;
        @SerializedName("BillMode")
        @Expose
        private String billMode;
        @SerializedName("BillType")
        @Expose
        private String billType;
        @SerializedName("Reason")
        @Expose
        private String reason;
        @SerializedName("DependantName")
        @Expose
        private String dependantName;
        @SerializedName("Relation")
        @Expose
        private String relation;
        @SerializedName("DependantAccountnumber")
        @Expose
        private String dependantAccountnumber;
        @SerializedName("Contractor")
        @Expose
        private String contractor;
        @SerializedName("TINNumber")
        @Expose
        private String tINNumber;
        @SerializedName("STNumber")
        @Expose
        private String sTNumber;
        @SerializedName("Address")
        @Expose
        private String address;
        @SerializedName("OTNos")
        @Expose
        private String oTNos;
        @SerializedName("BillPrintType")
        @Expose
        private String billPrintType;
        @SerializedName("SmartCardNo")
        @Expose
        private String smartCardNo;
        @SerializedName("SecurityDeposit")
        @Expose
        private String securityDeposit;

    @SerializedName("TableNumber")
    @Expose
    private String TableNumber;

    public String getTableNumber() {
        return TableNumber;
    }

    public void setTableNumber(String tableNumber) {
        TableNumber = tableNumber;
    }

        public String getBillID() {
            return billID;
        }

        public void setBillID(String billID) {
            this.billID = billID;
        }

        public String getBillNumber() {
            return billNumber;
        }

        public void setBillNumber(String billNumber) {
            this.billNumber = billNumber;
        }

        public String getBillDate() {
            return billDate;
        }

        public void setBillDate(String billDate) {
            this.billDate = billDate;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getStoreCode() {
            return storeCode;
        }

        public void setStoreCode(String storeCode) {
            this.storeCode = storeCode;
        }

        public Boolean getBarcoderequired() {
            return barcoderequired;
        }

        public void setBarcoderequired(Boolean barcoderequired) {
            this.barcoderequired = barcoderequired;
        }

        public String getMemberID() {
            return memberID;
        }

        public void setMemberID(String memberID) {
            this.memberID = memberID;
        }

        public String getAccountNumber() {
            return accountNumber;
        }

        public void setAccountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
        }

        public String getMemberName() {
            return memberName;
        }

        public void setMemberName(String memberName) {
            this.memberName = memberName;
        }

        public String getOBalance() {
            return oBalance;
        }

        public void setOBalance(String oBalance) {
            this.oBalance = oBalance;
        }

        public String getCBalance() {
            return cBalance;
        }

        public void setCBalance(String cBalance) {
            this.cBalance = cBalance;
        }

        public String getPersonID() {
            return personID;
        }

        public void setPersonID(String personID) {
            this.personID = personID;
        }

        public String getStewardCode() {
            return stewardCode;
        }

        public void setStewardCode(String stewardCode) {
            this.stewardCode = stewardCode;
        }

        public String getStewardName() {
            return stewardName;
        }

        public void setStewardName(String stewardName) {
            this.stewardName = stewardName;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getBillMode() {
            return billMode;
        }

        public void setBillMode(String billMode) {
            this.billMode = billMode;
        }

        public String getBillType() {
            return billType;
        }

        public void setBillType(String billType) {
            this.billType = billType;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getDependantName() {
            return dependantName;
        }

        public void setDependantName(String dependantName) {
            this.dependantName = dependantName;
        }

        public String getRelation() {
            return relation;
        }

        public void setRelation(String relation) {
            this.relation = relation;
        }

        public String getDependantAccountnumber() {
            return dependantAccountnumber;
        }

        public void setDependantAccountnumber(String dependantAccountnumber) {
            this.dependantAccountnumber = dependantAccountnumber;
        }

        public String getContractor() {
            return contractor;
        }

        public void setContractor(String contractor) {
            this.contractor = contractor;
        }

        public String getTINNumber() {
            return tINNumber;
        }

        public void setTINNumber(String tINNumber) {
            this.tINNumber = tINNumber;
        }

        public String getSTNumber() {
            return sTNumber;
        }

        public void setSTNumber(String sTNumber) {
            this.sTNumber = sTNumber;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getOTNos() {
            return oTNos;
        }

        public void setOTNos(String oTNos) {
            this.oTNos = oTNos;
        }

        public String getBillPrintType() {
            return billPrintType;
        }

        public void setBillPrintType(String billPrintType) {
            this.billPrintType = billPrintType;
        }

        public String getSmartCardNo() {
            return smartCardNo;
        }

        public void setSmartCardNo(String smartCardNo) {
            this.smartCardNo = smartCardNo;
        }

        public String getSecurityDeposit() {
            return securityDeposit;
        }

        public void setSecurityDeposit(String securityDeposit) {
            this.securityDeposit = securityDeposit;
        }

}
