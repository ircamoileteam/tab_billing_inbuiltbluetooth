package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProvisionalDto2 implements Serializable {


    @SerializedName("ItemID")
    @Expose
    private String itemID;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("ServiceChargeAmount")
    @Expose
    private String serviceChargeAmount;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("tRate")
    @Expose
    private String tRate;
    @SerializedName("OTNo")
    @Expose
    private String oTNo;
    @SerializedName("TaxID")
    @Expose
    private String taxID;
    @SerializedName("TDescription")
    @Expose
    private String tDescription;
    @SerializedName("TaxAmount")
    @Expose
    private String taxAmount;
    @SerializedName("VatAmount")
    @Expose
    private String vatAmount;
    @SerializedName("MCAmount")
    @Expose
    private String mCAmount;
    @SerializedName("HSNCode")
    @Expose
    private String hSNCode;
    @SerializedName("sgst")
    @Expose
    private String sgst;
    @SerializedName("cgst")
    @Expose
    private String cgst;
    @SerializedName("cess")
    @Expose
    private String cess;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("cessvalue")
    @Expose
    private String cessvalue;
    @SerializedName("ItemCategoryID")
    @Expose
    private String itemCategoryID;
    @SerializedName("gst")
    @Expose
    private String gst;

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }


    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getServiceChargeAmount() {
        return serviceChargeAmount;
    }

    public void setServiceChargeAmount(String serviceChargeAmount) {
        this.serviceChargeAmount = serviceChargeAmount;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getTRate() {
        return tRate;
    }

    public void setTRate(String tRate) {
        this.tRate = tRate;
    }

    public String getOTNo() {
        return oTNo;
    }

    public void setOTNo(String oTNo) {
        this.oTNo = oTNo;
    }

    public String getTaxID() {
        return taxID;
    }

    public void setTaxID(String taxID) {
        this.taxID = taxID;
    }

    public String getTDescription() {
        return tDescription;
    }

    public void setTDescription(String tDescription) {
        this.tDescription = tDescription;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(String vatAmount) {
        this.vatAmount = vatAmount;
    }

    public String getMCAmount() {
        return mCAmount;
    }

    public void setMCAmount(String mCAmount) {
        this.mCAmount = mCAmount;
    }

    public String getHSNCode() {
        return hSNCode;
    }

    public void setHSNCode(String hSNCode) {
        this.hSNCode = hSNCode;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getCess() {
        return cess;
    }

    public void setCess(String cess) {
        this.cess = cess;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCessvalue() {
        return cessvalue;
    }

    public void setCessvalue(String cessvalue) {
        this.cessvalue = cessvalue;
    }

    public String getItemCategoryID() {
        return itemCategoryID;
    }

    public void setItemCategoryID(String itemCategoryID) {
        this.itemCategoryID = itemCategoryID;
    }

    
}
