package com.irca.dto;

public interface ClickListener {
    void onPositionClicked(int position);
 }
