package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MenuNewDto implements Serializable {

    @SerializedName("StoreId")
    @Expose
    private String storeId;
    @SerializedName("POSName")
    @Expose
    private String pOSName;
    @SerializedName("ItemId")
    @Expose
    private String itemId;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("ItemGroupID")
    @Expose
    private String itemGroupID;
    @SerializedName("ItemGroup")
    @Expose
    private String itemGroup;
    @SerializedName("UOM")
    @Expose
    private String uOM;
    @SerializedName("Rate")
    @Expose
    private String rate;
    @SerializedName("ItemGroupImage")
    @Expose
    private String itemGroupImage;
    @SerializedName("ItemImage")
    @Expose
    private String itemImage;
    @SerializedName("IsStockCheckRequired")
    @Expose
    private Boolean isStockCheckRequired;
    @SerializedName("AvailableQty")
    @Expose
    private String availableQty;
    @SerializedName("ItemCategoryID")
    @Expose
    private String itemCategoryID;
    @SerializedName("SalesUnitId")
    @Expose
    private String salesUnitId;
    @SerializedName("list")
    @Expose
    private List<MenuNewDto> menuNewDtoList = new ArrayList<>();

    public List<MenuNewDto> getMenuNewDtoList() {
        return menuNewDtoList;
    }

    public void setMenuNewDtoList(List<MenuNewDto> menuNewDtoList) {
        this.menuNewDtoList = menuNewDtoList;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getPOSName() {
        return pOSName;
    }

    public void setPOSName(String pOSName) {
        this.pOSName = pOSName;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemGroupID() {
        return itemGroupID;
    }

    public void setItemGroupID(String itemGroupID) {
        this.itemGroupID = itemGroupID;
    }

    public String getItemGroup() {
        return itemGroup;
    }

    public void setItemGroup(String itemGroup) {
        this.itemGroup = itemGroup;
    }

    public String getUOM() {
        return uOM;
    }

    public void setUOM(String uOM) {
        this.uOM = uOM;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getItemGroupImage() {
        return itemGroupImage;
    }

    public void setItemGroupImage(String itemGroupImage) {
        this.itemGroupImage = itemGroupImage;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public Boolean getIsStockCheckRequired() {
        return isStockCheckRequired;
    }

    public void setIsStockCheckRequired(Boolean isStockCheckRequired) {
        this.isStockCheckRequired = isStockCheckRequired;
    }

    public String getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(String availableQty) {
        this.availableQty = availableQty;
    }

    public String getItemCategoryID() {
        return itemCategoryID;
    }

    public void setItemCategoryID(String itemCategoryID) {
        this.itemCategoryID = itemCategoryID;
    }

    public String getSalesUnitId() {
        return salesUnitId;
    }

    public void setSalesUnitId(String salesUnitId) {
        this.salesUnitId = salesUnitId;
    }


}
