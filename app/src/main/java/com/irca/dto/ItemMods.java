package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ItemMods implements Serializable {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("BillID")
    @Expose
    private String billID;
    @SerializedName("ItemID")
    @Expose
    private String itemID;
    @SerializedName("ItemSerialNo")
    @Expose
    private String itemSerialNo;
    @SerializedName("ModifierID")
    @Expose
    private String modifierID;
    @SerializedName("ModifierName")
    @Expose
    private String modifierName;
    @SerializedName("ModifierDetailID")
    @Expose
    private String modifierDetailID;
    @SerializedName("ModifierDetailName")
    @Expose
    private String modifierDetailName;
    @SerializedName("Modifier")
    @Expose
    private String modifier;
    @SerializedName("ItemID1")
    @Expose
    private String itemID1;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("ItemName")
    @Expose
    private String itemName;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getBillID() {
        return billID;
    }

    public void setBillID(String billID) {
        this.billID = billID;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getItemSerialNo() {
        return itemSerialNo;
    }

    public void setItemSerialNo(String itemSerialNo) {
        this.itemSerialNo = itemSerialNo;
    }

    public String getModifierID() {
        return modifierID;
    }

    public void setModifierID(String modifierID) {
        this.modifierID = modifierID;
    }

    public String getModifierName() {
        return modifierName;
    }

    public void setModifierName(String modifierName) {
        this.modifierName = modifierName;
    }

    public String getModifierDetailID() {
        return modifierDetailID;
    }

    public void setModifierDetailID(String modifierDetailID) {
        this.modifierDetailID = modifierDetailID;
    }

    public String getModifierDetailName() {
        return modifierDetailName;
    }

    public void setModifierDetailName(String modifierDetailName) {
        this.modifierDetailName = modifierDetailName;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getItemID1() {
        return itemID1;
    }

    public void setItemID1(String itemID1) {
        this.itemID1 = itemID1;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    
}
