package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ModifiersDto implements Serializable {
    @SerializedName("modifierName")
    @Expose
    private String modifierName;
    @SerializedName("modifierDetailName")
    @Expose
    private String modifierDetailName;
    @SerializedName("Id")
    @Expose
    private String modifierId;
    @SerializedName("Id1")
    @Expose
    private String modifierDetailId;
    @SerializedName("free")
    @Expose
    private String freeFlow;
    @SerializedName("selected")
    @Expose
    private boolean isSelected;

    public String getFreeFlow() {
        return freeFlow;
    }

    public void setFreeFlow(String freeFlow) {
        this.freeFlow = freeFlow;
    }

    public String getModifierDetailName() {
        return modifierDetailName;
    }

    public void setModifierDetailName(String modifierDetailName) {
        this.modifierDetailName = modifierDetailName;
    }

    public String getModifierDetailId() {
        return modifierDetailId;
    }

    public void setModifierDetailId(String modifierDetailId) {
        this.modifierDetailId = modifierDetailId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getModifierName() {
        return modifierName;
    }

    public void setModifierName(String modifierName) {
        this.modifierName = modifierName;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }
}
