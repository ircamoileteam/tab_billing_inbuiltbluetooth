package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PendingBillsDto implements Serializable {


    @SerializedName("BillMode")
    @Expose
    private String billMode;
    @SerializedName("BillID")
    @Expose
    private String billID;
    @SerializedName("BillNumber")
    @Expose
    private String billNumber;
    @SerializedName("BillType")
    @Expose
    private String billType;
    @SerializedName("BillDate")
    @Expose
    private String billDate;
    @SerializedName("OTNo")
    @Expose
    private String oTNo;
    @SerializedName("MemberID")
    @Expose
    private String memberID;
    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("MemberName")
    @Expose
    private String memberName;
    @SerializedName("PersonID")
    @Expose
    private String personID;
    @SerializedName("StoreID")
    @Expose
    private String storeID;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("ItemID")
    @Expose
    private String itemID;
    @SerializedName("ItemCode")
    @Expose
    private String itemCode;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("UnitCode")
    @Expose
    private String unitCode;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("PersonCode")
    @Expose
    private String personCode;
    @SerializedName("OTNote")
    @Expose
    private String oTNote;

    public String getoTNote() {
        return oTNote;
    }

    public void setoTNote(String oTNote) {
        this.oTNote = oTNote;
    }

    public String getBillMode() {
        return billMode;
    }

    public void setBillMode(String billMode) {
        this.billMode = billMode;
    }

    public String getBillID() {
        return billID;
    }

    public void setBillID(String billID) {
        this.billID = billID;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getOTNo() {
        return oTNo;
    }

    public void setOTNo(String oTNo) {
        this.oTNo = oTNo;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPersonCode() {
        return personCode;
    }

    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    
}
