package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MemberDetails implements Serializable {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("Company")
    @Expose
    private String company;
    @SerializedName("RenewalDate")
    @Expose
    private String renewalDate;
    @SerializedName("Types")
    @Expose
    private String types;
    @SerializedName("OwnerShipType")
    @Expose
    private String ownerShipType;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("MiddleName")
    @Expose
    private String middleName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("MemberType")
    @Expose
    private String memberType;
    @SerializedName("ConfirmationDate")
    @Expose
    private String confirmationDate;
    @SerializedName("DateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("DateOfExp")
    @Expose
    private String dateOfExp;
    @SerializedName("DateOfInActive")
    @Expose
    private String dateOfInActive;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("AnnivDate")
    @Expose
    private String annivDate;
    @SerializedName("Remarks")
    @Expose
    private String remarks;
    @SerializedName("Age")
    @Expose
    private String age;
    @SerializedName("ClubCode")
    @Expose
    private String clubCode;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(String renewalDate) {
        this.renewalDate = renewalDate;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getOwnerShipType() {
        return ownerShipType;
    }

    public void setOwnerShipType(String ownerShipType) {
        this.ownerShipType = ownerShipType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(String confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDateOfExp() {
        return dateOfExp;
    }

    public void setDateOfExp(String dateOfExp) {
        this.dateOfExp = dateOfExp;
    }

    public String getDateOfInActive() {
        return dateOfInActive;
    }

    public void setDateOfInActive(String dateOfInActive) {
        this.dateOfInActive = dateOfInActive;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAnnivDate() {
        return annivDate;
    }

    public void setAnnivDate(String annivDate) {
        this.annivDate = annivDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getClubCode() {
        return clubCode;
    }

    public void setClubCode(String clubCode) {
        this.clubCode = clubCode;
    }

}
