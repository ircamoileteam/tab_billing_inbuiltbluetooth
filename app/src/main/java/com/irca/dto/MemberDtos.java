package com.irca.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MemberDtos implements Serializable {


    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("MemberName")
    @Expose
    private String memberName;
    @SerializedName("MemberActive")
    @Expose
    private String memberActive;
    @SerializedName("SmartCardSerialNo")
    @Expose
    private String smartCardSerialNo;
    @SerializedName("IsCreditCheckRequired")
    @Expose
    private String isCreditCheckRequired;
    @SerializedName("CreditLimit")
    @Expose
    private String creditLimit;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("EffectiveDate")
    @Expose
    private String effectiveDate;
    @SerializedName("DependantName")
    @Expose
    private String dependantName;
    @SerializedName("Relation")
    @Expose
    private String relation;
    @SerializedName("OBalance")
    @Expose
    private String oBalance;
    @SerializedName("PrevBalAmt")
    @Expose
    private String prevBalAmt;
    @SerializedName("BillAmount")
    @Expose
    private String billAmount;
    @SerializedName("ItemSerialNo")
    @Expose
    private String itemSerialNo;
    @SerializedName("IsBillDebitAccNotRequired")
    @Expose
    private String isBillDebitAccNotRequired;
    @SerializedName("DependantID")
    @Expose
    private String dependantID;
    @SerializedName("IsComplimentaryBillApplicable")
    @Expose
    private String isComplimentaryBillApplicable;
    @SerializedName("IsBillDebitAccNotRequired1")
    @Expose
    private String isBillDebitAccNotRequired1;
    @SerializedName("IsCardActivated")
    @Expose
    private String isCardActivated;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberActive() {
        return memberActive;
    }

    public void setMemberActive(String memberActive) {
        this.memberActive = memberActive;
    }

    public String getSmartCardSerialNo() {
        return smartCardSerialNo;
    }

    public void setSmartCardSerialNo(String smartCardSerialNo) {
        this.smartCardSerialNo = smartCardSerialNo;
    }

    public String getIsCreditCheckRequired() {
        return isCreditCheckRequired;
    }

    public void setIsCreditCheckRequired(String isCreditCheckRequired) {
        this.isCreditCheckRequired = isCreditCheckRequired;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getDependantName() {
        return dependantName;
    }

    public void setDependantName(String dependantName) {
        this.dependantName = dependantName;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getOBalance() {
        return oBalance;
    }

    public void setOBalance(String oBalance) {
        this.oBalance = oBalance;
    }

    public String getPrevBalAmt() {
        return prevBalAmt;
    }

    public void setPrevBalAmt(String prevBalAmt) {
        this.prevBalAmt = prevBalAmt;
    }

    public String getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    public String getItemSerialNo() {
        return itemSerialNo;
    }

    public void setItemSerialNo(String itemSerialNo) {
        this.itemSerialNo = itemSerialNo;
    }

    public String getIsBillDebitAccNotRequired() {
        return isBillDebitAccNotRequired;
    }

    public void setIsBillDebitAccNotRequired(String isBillDebitAccNotRequired) {
        this.isBillDebitAccNotRequired = isBillDebitAccNotRequired;
    }

    public String getDependantID() {
        return dependantID;
    }

    public void setDependantID(String dependantID) {
        this.dependantID = dependantID;
    }

    public String getIsComplimentaryBillApplicable() {
        return isComplimentaryBillApplicable;
    }

    public void setIsComplimentaryBillApplicable(String isComplimentaryBillApplicable) {
        this.isComplimentaryBillApplicable = isComplimentaryBillApplicable;
    }

    public String getIsBillDebitAccNotRequired1() {
        return isBillDebitAccNotRequired1;
    }

    public void setIsBillDebitAccNotRequired1(String isBillDebitAccNotRequired1) {
        this.isBillDebitAccNotRequired1 = isBillDebitAccNotRequired1;
    }

    public String getIsCardActivated() {
        return isCardActivated;
    }

    public void setIsCardActivated(String isCardActivated) {
        this.isCardActivated = isCardActivated;
    }

}
