package com.irca.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irca.Printer.DiscoveryActivity;
import com.irca.Printer.PlaceOrder_bill;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.Utils.ShowMsg;
import com.irca.adapter.ProvisionalBillAdapter;
import com.irca.adapter.RunningOrdersAdapter;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.dto.ProvisionalDto1;
import com.irca.dto.ProvisionalDto2;
import com.irca.dto.TableTransferDetails;
import com.irca.scan.PermissionsManager;
import com.zcs.sdk.DriverManager;
import com.zcs.sdk.SdkResult;
import com.zcs.sdk.Sys;
import com.zcs.sdk.print.PrnStrFormat;
import com.zcs.sdk.print.PrnTextFont;
import com.zcs.sdk.print.PrnTextStyle;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutorService;

//import io.fabric.sdk.android.services.concurrency.AsyncTask;

public class ProvisionalBillActivity extends AppCompatActivity implements ReceiveListener {

    RecyclerView recyclerView;
    ConnectionDetector cd;
    boolean isInternetPresent;
    SharedPreferences sharedpreferences, pref;
    String posId, userId, _target, _printer, clubName, clubaddress, counter_name,printertype;
    ImageView backImageView;
    List<TableTransferDetails> tableTransferDetailsList = new ArrayList<>();
    RunningOrdersAdapter orderListAdapter;
    TextView mName, mCode, tamt, g_tot, g_sgst, g_vat, g_cgst,tcess,tservicechage,t_gst;
    LinearLayout cesslinear,linearservice;
    EditText edtTarget1;
    private Printer mPrinter = null;
    DecimalFormat format = new DecimalFormat("#.##");

    Button bluetooth, button_print,btnSampleReceiptnew;
    String billId;
    StringBuilder textData = new StringBuilder();
    Double billAmt = 0.0;
    String method = "";

    List<ProvisionalDto2> itemsList1 = new ArrayList<>();
    List<ProvisionalDto1> mainList1 = new ArrayList<>();
    private DriverManager mDriverManager = DriverManager.getInstance();
    private com.zcs.sdk.Printer mPrinter1;

    private ExecutorService mSingleThreadExecutor;

    private PermissionsManager mPermissionsManager;
    private Sys mSys = mDriverManager.getBaseSysDevice();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provisional_bill);
        try {
            mSingleThreadExecutor = mDriverManager.getSingleThreadExecutor();
            mDriverManager = DriverManager.getInstance();
            mPrinter1 = mDriverManager.getPrinter();

            try {
                Objects.requireNonNull(this.getSupportActionBar()).hide();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            mName = findViewById(R.id.mName);
            mCode = findViewById(R.id.mCode);
            tamt = findViewById(R.id.tamt);
            g_tot = findViewById(R.id.g_tot);
            g_sgst = findViewById(R.id.sgst);
            g_cgst = findViewById(R.id.cgst);
            t_gst = findViewById(R.id.gst);
            g_vat = findViewById(R.id.vat);
            tservicechage = findViewById(R.id.servicecharge);
            tcess = findViewById(R.id.cess);
            edtTarget1 = findViewById(R.id.edtTarget1);
            bluetooth = findViewById(R.id.bluetooth);
            button_print = findViewById(R.id.button_print);
            recyclerView = findViewById(R.id.recycler_view);
            backImageView = findViewById(R.id.image_viewBack);

            cesslinear = findViewById(R.id.cesslinear);
            linearservice = findViewById(R.id.linearservice);
            btnSampleReceiptnew = (Button) findViewById(R.id.btnSampleReceiptnew);

            sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            pref = getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);
            posId = sharedpreferences.getString("storeId", "");
            userId = sharedpreferences.getString("userId", "");
            clubName = sharedpreferences.getString("clubName", "");
            clubaddress = sharedpreferences.getString("clubAddress", "");
            counter_name = sharedpreferences.getString("StoreName", "");
            _target = pref.getString("Target", "");
            _printer = pref.getString("Printer", "");
            printertype = sharedpreferences.getString("PrinterType", "");

            billId = getIntent().getStringExtra("billId");
            backImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            bluetooth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  Find bluetooth
                    Intent intent = new Intent(getApplication(), DiscoveryActivity.class);
                    startActivityForResult(intent, 0);

                }
            });
            edtTarget1.setText(_printer + "-" + _target);

            if (_printer.equalsIgnoreCase("") && _target.equalsIgnoreCase("")) {
//                edtTarget1.setText("TM-P20_011044" + "-" + "BT:00:01:90:AE:93:17");
        //        edtTarget1.setText("TM-P80_002585" + "-" + "BT:00:01:90:88:DA:48");
            //    edtTarget1.setText("TM-P20_012348-BT:00:01:90:B9:26:DB");

//            mEdtTarget.setText("TM-P20_000452" + "-" + "BT:00:01:90:C2:AE:77");
            } else {

                edtTarget1.setText(_printer + "-" + _target);
            }


            new CallApi().execute();


            button_print.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String print = edtTarget1.getText().toString();
                    if (!(print.equals("") || print.equals("-"))) {
                        try {
                            Activity mActivity = ProvisionalBillActivity.this;
                            mActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    new AsyncPrinting().execute();
                                }
                            });
                        } catch (Throwable e) {
                            e.printStackTrace();
                            Toast.makeText(ProvisionalBillActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(ProvisionalBillActivity.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            btnSampleReceiptnew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                           try {
                            Activity mActivity = ProvisionalBillActivity.this;
                            mActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    new AsyncPrintingpoona().execute();
                                }
                            });
                        } catch (Throwable e) {
                            e.printStackTrace();
                            Toast.makeText(ProvisionalBillActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

            });

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void filter(String text) {
        try {
            List<TableTransferDetails> itemsArrayList = new ArrayList<>();
            for (int i = 0; i < tableTransferDetailsList.size(); i++) {
                if (tableTransferDetailsList.get(i).getOTNo().toLowerCase().contains(text.toLowerCase()) || tableTransferDetailsList.get(i).getTableNumber().toLowerCase().contains(text.toLowerCase())) {
                    itemsArrayList.add(tableTransferDetailsList.get(i));
                }
            }
//            orderListAdapter.filterList(itemsArrayList, this);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
    public class AsyncPrintingpoona extends AsyncTask<Void, Void, String> {

        String status = "";
        ProgressDialog pd;
        int printcode=2;
        @Override
        protected String doInBackground(Void... params) {


            try {
                mSingleThreadExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        int printStatus = mPrinter1.getPrinterStatus();
                        if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                            ProvisionalBillActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ProvisionalBillActivity.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                                }
                            });
                        } else {
                            try {
                                    createReceiptDatapoona();

                                //   createReceiptDatapoona();
                            } catch (Exception e) {
                                Log.d("catch exp :", e + "");
                            }

                        }
                    }
                });






            } catch (final Throwable e) {
                e.printStackTrace();
            }

            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                pd = new ProgressDialog(ProvisionalBillActivity.this);
                pd.setMessage("Printing Please Wait .......");
                pd.setCancelable(false);
                pd.show();
            } catch (Throwable e) {
                e.printStackTrace();
                e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {
                Toast.makeText(ProvisionalBillActivity.this,"int printer code : "+printcode,Toast.LENGTH_LONG).show();

                if (status.equals("true")) {
                    try {
                        boolean print = printData();
                        if (!print) {
                            if (mPrinter == null) {
                                status = "false";
                            }
                            mPrinter.clearCommandBuffer();

                            mPrinter.setReceiveEventListener(null);

                            mPrinter = null;

                        } else {
                            status = "true";
                        }
                    } catch (Exception e) {
                        ShowMsg.showException(e, "Post execute error", ProvisionalBillActivity.this);
                    }
                    // updateButtonState(true);
                    Toast.makeText(ProvisionalBillActivity.this, "Printing Success", Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                } else if (status.equals("")) {
                    Toast.makeText(ProvisionalBillActivity.this, "Printing failed to enter", Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                } else {
                    Toast.makeText(ProvisionalBillActivity.this, "Printing failed to enter" + status, Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                }
            } catch (final Exception e) {
                ProvisionalBillActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        ShowMsg.showException(e, "other", ProvisionalBillActivity.this);
                    }
                });
            }
        }

    }



    public class AsyncPrinting extends AsyncTask<Void, Void, String> {

        String status = "";
        ProgressDialog pd;
        int printcode=2;
        @Override
        protected String doInBackground(Void... params) {
            try {
                printcode=    getPrinterCode(printertype);
                // mPrinter = new Printer(Printer.TM_P20, Printer.MODEL_ANK, ProvisionalBillActivity.this);
              //   mPrinter = new Printer(Printer.TM_P80, Printer.MODEL_ANK, ProvisionalBillActivity.this);
                 mPrinter = new Printer(printcode, 0, ProvisionalBillActivity.this);
                mPrinter.setReceiveEventListener(ProvisionalBillActivity.this);
            } catch (Throwable e) {
                e.printStackTrace();
            }
            try {
                if (!createReceiptData()) {
                    if (mPrinter == null) {
                        status = "false";
                    }
                    mPrinter.clearCommandBuffer();
                    mPrinter.setReceiveEventListener(null);
                    mPrinter = null;
                    status = "false";
                } else {
                    status = "true";
                }
            } catch (final Throwable e) {
                e.printStackTrace();
            }

            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                pd = new ProgressDialog(ProvisionalBillActivity.this);
                pd.setMessage("Printing Please Wait .......");
                pd.setCancelable(false);
                pd.show();
            } catch (Throwable e) {
                e.printStackTrace();
                e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {
                Toast.makeText(ProvisionalBillActivity.this,"int printer code : "+printcode,Toast.LENGTH_LONG).show();

                if (status.equals("true")) {
                    try {
                        boolean print = printData();
                        if (!print) {
                            if (mPrinter == null) {
                                status = "false";
                            }
                            mPrinter.clearCommandBuffer();

                            mPrinter.setReceiveEventListener(null);

                            mPrinter = null;

                        } else {
                            status = "true";
                        }
                    } catch (Exception e) {
                        ShowMsg.showException(e, "Post execute error", ProvisionalBillActivity.this);
                    }
                    // updateButtonState(true);
                    Toast.makeText(ProvisionalBillActivity.this, "Printing Success", Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                } else if (status.equals("")) {
                    Toast.makeText(ProvisionalBillActivity.this, "Printing failed to enter", Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                } else {
                    Toast.makeText(ProvisionalBillActivity.this, "Printing failed to enter" + status, Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                }
            } catch (final Exception e) {
                ProvisionalBillActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        ShowMsg.showException(e, "other", ProvisionalBillActivity.this);
                    }
                });
            }
        }

    }

    public void dispPrinterWarnings(PrinterStatusInfo status) {
        //EditText edtWarnings = (EditText) findViewById(R.id.edtWarnings1);

        String warningsMsg = "";

        if (status == null) {
            return;
        }

        if (status.getPaper() == Printer.PAPER_NEAR_END) {
            warningsMsg += getString(R.string.handlingmsg_warn_receipt_near_end);
        }

        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_1) {
            warningsMsg += getString(R.string.handlingmsg_warn_battery_near_end);
        }

        // edtWarnings.setText(warningsMsg);
        Toast.makeText(getApplicationContext(), warningsMsg, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onPtrReceive(Printer printer, int i, final PrinterStatusInfo printerStatusInfo, String s) {
        runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
//                ShowMsg.showResult(code, makeErrorMessage(status), mContext);

                dispPrinterWarnings(printerStatusInfo);

                updateButtonState(true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        disconnectPrinter();
                    }
                }).start();
            }
        });
    }

    public void updateButtonState(boolean state) {
        //Button btnCoupon = (Button)findViewById(R.id.btnSampleCoupon);

        if (state) {
            button_print.setEnabled(state);
            button_print.setText("Print");
        } else {
            button_print.setBackgroundColor(getResources().getColor(R.color.black_semi_transparent));
            button_print.setEnabled(state);
            button_print.setText("Printing ...");
        }
        //  btnCoupon.setEnabled(state);
    }

    public void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "endTransaction", ProvisionalBillActivity.this);
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "disconnect", ProvisionalBillActivity.this);
                }
            });
        }

        finalizeObject();
    }

    public void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }


    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        try {

            if (data != null && resultCode == RESULT_OK) {
                String target = data.getStringExtra(getString(R.string.title_target));
                String printer = data.getStringExtra(getString(R.string.title_Printer));
                if (printer != null) {
                    edtTarget1.setText(printer + "-" + target);

                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public boolean connectPrinter() {
        boolean isBeginTransaction = false;
        String ll = "";

        if (mPrinter == null) {
            return false;
        }

        try {
            ll = edtTarget1.getText().toString();
            if (!ll.equals("")) {
                mPrinter.connect(edtTarget1.getText().toString(), Printer.PARAM_DEFAULT);
            } else {
                String[] l = ll.split("-");
                mPrinter.connect(l[1], Printer.PARAM_DEFAULT);
            }
        } catch (Exception e) {
            ShowMsg.showException(e, "connect", ProvisionalBillActivity.this);
            return false;
        }

        try {
            mPrinter.beginTransaction();
            isBeginTransaction = true;
        } catch (Exception e) {
            ShowMsg.showException(e, "beginTransaction", ProvisionalBillActivity.this);
        }

        if (isBeginTransaction == false) {
            try {
                mPrinter.disconnect();
            } catch (Epos2Exception e) {
                // Do nothing
                return false;
            }
        }

        return true;
    }


    public boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            ;//print available
        }

        return true;
    }

    public String makeErrorMessage(PrinterStatusInfo status) {
        String msg = "";

        if (status.getOnline() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_offline);
        }
        if (status.getConnection() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_no_response);
        }
        if (status.getCoverOpen() == Printer.TRUE) {
            msg += getString(R.string.handlingmsg_err_cover_open);
        }
        if (status.getPaper() == Printer.PAPER_EMPTY) {
            msg += getString(R.string.handlingmsg_err_receipt_end);
        }
        if (status.getPaperFeed() == Printer.TRUE || status.getPanelSwitch() == Printer.SWITCH_ON) {
            msg += getString(R.string.handlingmsg_err_paper_feed);
        }
        if (status.getErrorStatus() == Printer.MECHANICAL_ERR || status.getErrorStatus() == Printer.AUTOCUTTER_ERR) {
            msg += getString(R.string.handlingmsg_err_autocutter);
            msg += getString(R.string.handlingmsg_err_need_recover);
        }
        if (status.getErrorStatus() == Printer.UNRECOVER_ERR) {
            msg += getString(R.string.handlingmsg_err_unrecover);
        }
        if (status.getErrorStatus() == Printer.AUTORECOVER_ERR) {
            if (status.getAutoRecoverError() == Printer.HEAD_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_head);
            }
            if (status.getAutoRecoverError() == Printer.MOTOR_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_motor);
            }
            if (status.getAutoRecoverError() == Printer.BATTERY_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_battery);
            }
            if (status.getAutoRecoverError() == Printer.WRONG_PAPER) {
                msg += getString(R.string.handlingmsg_err_wrong_paper);
            }
        }
        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_0) {
            msg += getString(R.string.handlingmsg_err_battery_real_end);
        }

        return msg;
    }

    public boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            return false;
        }

        PrinterStatusInfo status = mPrinter.getStatus();

        dispPrinterWarnings(status);

        if (!isPrintable(status)) {
            ShowMsg.showMsg(makeErrorMessage(status), ProvisionalBillActivity.this);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }
        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            ShowMsg.showException(e, "sendData", ProvisionalBillActivity.this);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }
    private void printFoooterpoona(ProvisionalDto1 provisionalDto1) {
        try {  int printStatus = mPrinter1.getPrinterStatus();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                ProvisionalBillActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ProvisionalBillActivity.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                    }
                });
            } else {
                PrnStrFormat format1 = new PrnStrFormat();

                format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                format1.setStyle(PrnTextStyle.NORMAL);
                format1.setTextSize(25);
                mPrinter1.setPrintAppendString("------------------------------------------------------", format1);

                mPrinter1.setPrintAppendString("Bill Amount" + " :  " + format.format(billAmt), format1);

                Double cgst = 0.0, sgst = 0.0, vat = 0.0, cess = 0.0, servicecharge = 0.0;
                for (int t = 0; t < itemsList1.size(); t++) {
                    cgst = cgst + Double.parseDouble(itemsList1.get(t).getCgst());
                    sgst = sgst + Double.parseDouble(itemsList1.get(t).getSgst());
                    Double vat1 = Double.parseDouble(itemsList1.get(t).getVatAmount());
                    vat = vat + vat1;
                    Double cess1 = (Double.parseDouble(itemsList1.get(t).getCessvalue()) * Double.parseDouble(itemsList1.get(t).getQuantity())) + Double.parseDouble(itemsList1.get(t).getCess());
                    cess = cess + cess1;
                    Double servicecharge1 = Double.parseDouble(itemsList1.get(t).getServiceChargeAmount());
                    servicecharge = servicecharge + servicecharge1;
                }

                mPrinter1.setPrintAppendString("CGST" + " :  " + format.format(cgst), format1);
                mPrinter1.setPrintAppendString("SGST" + " :  " + format.format(sgst), format1);

                if (vat > 0) {
                    // textData.append("VAT" + " :  ").append(format.format(vat)).append("\n");
                    mPrinter1.setPrintAppendString("VAT" + " :  " + format.format(vat), format1);

                }
                if (cess > 0) {
                    //  textData.append("CESS" + " :  ").append(format.format(cess)).append("\n");
                    mPrinter1.setPrintAppendString("CESS" + " :  " + format.format(cess), format1);

                }
                if (servicecharge > 0) {
                    // textData.append("servicecharge" + " :  ").append(format.format(servicecharge)).append("\n");
                    mPrinter1.setPrintAppendString("servicecharge" + " :  " + format.format(servicecharge), format1);

                }
                mPrinter1.setPrintAppendString("------------------------------------------------------", format1);

                Double GrantTotal = billAmt + cgst + sgst + vat + cess + servicecharge;
                //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");
                // textData.append("GRAND TOTAL" + " :  " + String.format("%.2f", GrantTotal) + "\n\n");

                format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                format1.setStyle(PrnTextStyle.NORMAL);
                format1.setTextSize(30);
                mPrinter1.setPrintAppendString("GRAND TOTAL" + " :  " + String.format("%.2f", GrantTotal), format1);


                format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                format1.setStyle(PrnTextStyle.NORMAL);
                format1.setTextSize(25);
                mPrinter1.setPrintAppendString("------------------------------------------------------", format1);
                mPrinter1.setPrintAppendString(" ", format1);
                mPrinter1.setPrintAppendString(" ", format1);
                mPrinter1.setPrintAppendString("(Signature)" + "\n", format1);
                mPrinter1.setPrintAppendString("* Thank you ! We Wish To Serve You Again ", format1);
                mPrinter1.setPrintAppendString(" ", format1);
                mPrinter1.setPrintAppendString(" ", format1);
                mPrinter1.setPrintAppendString(" ", format1);
                mPrinter1.setPrintAppendString(" ", format1);
            }

            printStatus = mPrinter1.setPrintStart();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                ProvisionalBillActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ProvisionalBillActivity.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                    }
                });
            }
        } catch (Exception e) {
            ShowMsg.showException(e, method, ProvisionalBillActivity.this);
            //return false;
        }
    }

    private void printFoooter(ProvisionalDto1 provisionalDto1) {
        try {
            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            //   GrantTotal=servicetax+vat+billAmt+cessTax;
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("Bill Amount" + " :  ").append(format.format(billAmt)).append("\n");
            Double cgst = 0.0, sgst = 0.0, vat = 0.0, cess = 0.0, servicecharge = 0.0;
            for (int t = 0; t < itemsList1.size(); t++) {
                cgst = cgst + Double.parseDouble(itemsList1.get(t).getCgst());
                sgst = sgst + Double.parseDouble(itemsList1.get(t).getSgst());
                Double vat1 = Double.parseDouble(itemsList1.get(t).getVatAmount());
                vat = vat + vat1;
                Double cess1 =( Double.parseDouble(itemsList1.get(t).getCessvalue())* Double.parseDouble(itemsList1.get(t).getQuantity()))+Double.parseDouble(itemsList1.get(t).getCess());
                cess = cess + cess1;
                Double servicecharge1 = Double.parseDouble(itemsList1.get(t).getServiceChargeAmount());
                servicecharge = servicecharge + servicecharge1;
            }
            textData.append("CGST" + " :  ").append(format.format(cgst)).append("\n");
            textData.append("SGST" + " :  ").append(format.format(sgst)).append("\n");
           if(vat>0){
                textData.append("VAT" + " :  ").append(format.format(vat)).append("\n");

            }   if(cess>0){
                textData.append("CESS" + " :  ").append(format.format(cess)).append("\n");

            }   if(servicecharge>0){
                textData.append("servicecharge" + " :  ").append(format.format(servicecharge)).append("\n");

            }




            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            //}
            Double GrantTotal = billAmt + cgst + sgst+vat+cess+servicecharge;
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");
            textData.append("GRAND TOTAL" + " :  " + String.format("%.2f", GrantTotal) + "\n\n");


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            textData.append("\n");
            textData.append("\n");
            textData.append("(Signature)" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
            textData.append("* Thank you ! We Wish To Serve You Again ");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);


        } catch (Exception e) {
            ShowMsg.showException(e, method, ProvisionalBillActivity.this);
            //return false;
        }
    }

    public boolean createReceiptDatapoona() {
        String itemcategoryid = "";

            printHeaderpoona(mainList1.get(0));
            billAmt=0.00;
            for (int item = 0; item < itemsList1.size(); item++) {
                printContentpoona(item, itemsList1);
            }
            //monica

            printFoooterpoona(mainList1.get(0));

            textData = null;


        return true;
    }
    public boolean createReceiptData() {
        String itemcategoryid = "";
        if (mPrinter == null) {
            return false;
        } else {
            printHeader(mainList1.get(0));
            billAmt=0.00;
            for (int item = 0; item < itemsList1.size(); item++) {
                printContent(item, itemsList1);
            }
            //monica

            printFoooter(mainList1.get(0));

            textData = null;
        }

        return true;
    }

    private void printHeader(ProvisionalDto1 provisionalDto1) {
        try {
            if (textData == null) {
                textData = new StringBuilder();
            }
            DecimalFormat format = new DecimalFormat("#.##");
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
            String date = format1.format(cal.getTime());

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            //  textData.append("Cosmopolitan Club (Regd.)\n");
            textData.append(clubName).append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append(clubaddress).append("\n");

            textData.delete(0, textData.length());

            //    String mstoreGST = sharedpreferences.getString("mstoreGST", "");
//            textData.append("179,club road,Ooty,Tamil Nadu,643001" + "\n");
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("\n" + "GST NO." + "" + ": " + provisionalDto1.getSTNumber() + "\n\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(0);
            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("PROVISIONAL BILL" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            //   mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("Bill  No" + ": " + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            //   mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("Membership No" + ": " + provisionalDto1.getAccountNumber() + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            //   mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("Name " + ": " + provisionalDto1.getMemberName() + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("PAY MODE: " + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("Date: " + provisionalDto1.getBillDate() + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("Outlet: " + provisionalDto1.getStoreName() + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("Billed By: " + provisionalDto1.getStewardName() + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("OT Number: ").append(provisionalDto1.getOTNos()).append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append(
                    "Item Name             "

                            + String.format("%4s", "Qty")
                            //  + " "
                            + String.format("%8s", "Rate")
                            //  + " "
                            + String.format("%8s", "Amount")
                            + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Header part issue: " + e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }
    private void printHeaderpoona(ProvisionalDto1 provisionalDto1) {
        try {

         //   DecimalFormat format = new DecimalFormat("#.##");
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
            String date = format1.format(cal.getTime());
            int printStatus = mPrinter1.getPrinterStatus();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                ProvisionalBillActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ProvisionalBillActivity.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                    }
                });
            } else {
                PrnStrFormat format = new PrnStrFormat();
               format.setTextSize(30);
                format.setAli(Layout.Alignment.ALIGN_CENTER);
                format.setFont(PrnTextFont.DEFAULT);
                mPrinter1.setPrintAppendString(clubName, format);
                format.setTextSize(25);
                format.setAli(Layout.Alignment.ALIGN_CENTER);
                format.setFont(PrnTextFont.DEFAULT);
                mPrinter1.setPrintAppendString(clubaddress, format);
                mPrinter1.setPrintAppendString("GST NO." + "" + ": " + provisionalDto1.getSTNumber(), format);
                mPrinter1.setPrintAppendString(" ", format);
                mPrinter1.setPrintAppendString("PROVISIONAL BILL", format);

                mPrinter1.setPrintAppendString("------------------------------------------------------", format);
                mPrinter1.setPrintAppendString("Bill  No" + ": ", format);
                format.setTextSize(30);
                format.setStyle(PrnTextStyle.BOLD);
                mPrinter1.setPrintAppendString("Membership No" + ": " + provisionalDto1.getAccountNumber(), format);
                mPrinter1.setPrintAppendString("Name " + ": " + provisionalDto1.getMemberName(), format);
                mPrinter1.setPrintAppendString("--------------------------------------", format);
                format.setAli(Layout.Alignment.ALIGN_NORMAL);
                format.setStyle(PrnTextStyle.NORMAL);
                format.setTextSize(25);
                mPrinter1.setPrintAppendString("PAY MODE: ", format);
                mPrinter1.setPrintAppendString("------------------------------------------------------", format);
                mPrinter1.setPrintAppendString(("Date: " + provisionalDto1.getBillDate()), format);
                mPrinter1.setPrintAppendString(("Outlet: " + provisionalDto1.getStoreName()), format);
                mPrinter1.setPrintAppendString(("Billed By: " + provisionalDto1.getStewardName()), format);
                mPrinter1.setPrintAppendString("------------------------------------------------------", format);
                mPrinter1.setPrintAppendString("OT Number: " + (provisionalDto1.getOTNos()), format);
                mPrinter1.setPrintAppendString("Table Number: " + (provisionalDto1.getTableNumber()), format);

               // textData.append("OT Number: ").append(provisionalDto1.getOTNos()).append("\n");
                //textData.append("Table Number: ").append(provisionalDto1.getTableNumber()).append("\n");
                mPrinter1.setPrintAppendString("------------------------------------------------------", format);

                format.setAli(Layout.Alignment.ALIGN_NORMAL);
                format.setStyle(PrnTextStyle.NORMAL);
                format.setTextSize(23);
                mPrinter1.setPrintAppendString(
                        String.format("%30s", ( "Item Name             "+ "                                                ").substring(0, 30))

                        + String.format("%6s", "Qty")
                        //  + " "
                    //    + String.format("%8s", "Rate")
                        //  + " "
                        + String.format("%10s", "Amount"), format);
                mPrinter1.setPrintAppendString("----------------------------------------------------------------", format);
            }
            printStatus = mPrinter1.setPrintStart();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                ProvisionalBillActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ProvisionalBillActivity.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Header part issue: " + e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void printContentpoona(int m, List<ProvisionalDto2> _closeorderbillList) {
        try {
            int printStatus = mPrinter1.getPrinterStatus();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                ProvisionalBillActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ProvisionalBillActivity.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                    }
                });
            } else {
                PrnStrFormat format1 = new PrnStrFormat();
                format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                format1.setStyle(PrnTextStyle.NORMAL);
                format1.setTextSize(23);
                if(_closeorderbillList.get(m).getItemName().length()>20){
                    String itemstr1=_closeorderbillList.get(m).getItemName().substring(0,20);
                    String itemstr2="..."+_closeorderbillList.get(m).getItemName().substring(20,_closeorderbillList.get(m).getItemName().length());
                    String itemline =
                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                            String.format("%15s", (itemstr1 + "                                                ").substring(0, 20))

                                    + String.format("%4s", (_closeorderbillList.get(m).getQuantity()))

                                    + String.format("%8s", format.format
                                    //     (Double.parseDouble((_closeorderbillList.get(m).getTRate())))) + String.format("%5s", format.format(Double.parseDouble((_closeorderbillList.get(m).getAmount()))))
                                            (Double.parseDouble((_closeorderbillList.get(m).getAmount()))))

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline, format1);

                    String itemline1 =
                             String.format("%15s", (itemstr2 + "                                                ").substring(0, 20))

                                    + String.format("%4s", (""))

                                    + String.format("%8s","")

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline1, format1);
                }
                else{
                    String itemline =
                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                            String.format("%15s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))

                                    + String.format("%4s", (_closeorderbillList.get(m).getQuantity()))

                                    + String.format("%8s", format.format
                                    //     (Double.parseDouble((_closeorderbillList.get(m).getTRate())))) + String.format("%5s", format.format(Double.parseDouble((_closeorderbillList.get(m).getAmount()))))
                                            (Double.parseDouble((_closeorderbillList.get(m).getAmount()))))

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline, format1);

                }


                billAmt = billAmt + Double.parseDouble(_closeorderbillList.get(m).getAmount());
                //08-04-02     billAmt =   Double.parseDouble(_closeorderbillList.get(m).getAmount());
            }
            printStatus = mPrinter1.setPrintStart();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                ProvisionalBillActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ProvisionalBillActivity.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                    }
                });
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
            billAmt = billAmt + Double.parseDouble(_closeorderbillList.get(m).getAmount());
            //08-04-02     billAmt =   Double.parseDouble(_closeorderbillList.get(m).getAmount());

        } } catch (Throwable e) {
        e.printStackTrace();
    }
}

    private void printContent(int m, List<ProvisionalDto2> _closeorderbillList) {
        try {
            textData.append(
                    String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                            // +" "
                            + String.format("%4s", (_closeorderbillList.get(m).getQuantity()))

                            // + String.format("%8s", format.format(Double.parseDouble((_closeorderbillList.get(m).getTRate()))))
                            // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                            // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                            + String.format("%5s", format.format(Double.parseDouble((_closeorderbillList.get(m).getAmount()))))
                            // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                            // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                            + "\n");


            method = "addText";
            mPrinter.addTextSize(1, 1);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(ProvisionalBillActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;
        List<ProvisionalDto2> itemsList = new ArrayList<>();
        List<ProvisionalDto1> mainList = new ArrayList<>();

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(ProvisionalBillActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
//                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.GetProvisionalBill(billId);
                    result = "" + getResult;
                    if (result.contains("true")) {
                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        JSONArray jsonArray = jsonObject.getJSONArray("Table");
                        JSONArray jsonArray1 = jsonObject.getJSONArray("Table1");
                        try {
                            Gson gson = new Gson();
                            mainList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<ProvisionalDto1>>() {
                            }.getType());
                            itemsList = gson.fromJson(String.valueOf(jsonArray1), new TypeToken<List<ProvisionalDto2>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ProvisionalBillActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        try {
                            pd.dismiss();
                            if (mainList.size() > 0) {
                                mainList1.clear();
                                mainList1.addAll(mainList);
                                mName.setText(mainList.get(0).getMemberName());
                                mCode.setText(mainList.get(0).getAccountNumber());
                            } else {
                                Toast.makeText(ProvisionalBillActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                            }
                            if (itemsList.size() > 0) {
                                itemsList1.clear();
                                itemsList1.addAll(itemsList);
                                showData(itemsList);
                                Double amt = 0.0, cgst = 0.0, sgst = 0.0, vat = 0.0, total = 0.0, cess = 0.0, servicecharge = 0.0, gst = 0.0;
                                for (int i = 0; i < itemsList.size(); i++) {
                                    Double d = Double.parseDouble(itemsList.get(i).getTRate()) * Double.parseDouble(itemsList.get(i).getQuantity());
                                    amt = amt + d;
                                    Double cgst1 = Double.parseDouble(itemsList.get(i).getCgst());
                                    cgst = cgst + cgst1;
                                    Double  gst1 = Double.parseDouble(itemsList.get(i).getGst());
                                    gst =  gst + gst1;
                                    Double sgst1 = Double.parseDouble(itemsList.get(i).getSgst());
                                    sgst = sgst + sgst1;
                                    Double vat1 = Double.parseDouble(itemsList.get(i).getVatAmount());
                                    vat = vat + vat1;
                                    Double cess1 =( Double.parseDouble(itemsList.get(i).getCessvalue())* Double.parseDouble(itemsList.get(i).getQuantity()))+Double.parseDouble(itemsList.get(i).getCess());
                                    cess = cess + cess1;
                                    Double servicecharge1 = Double.parseDouble(itemsList.get(i).getServiceChargeAmount());
                                    servicecharge = servicecharge + servicecharge1;
                                    total = total + (d + cgst1 + sgst1 +gst1+ vat1+cess1+servicecharge1);
                                }
                                DecimalFormat precision = new DecimalFormat("0.00");

                                tamt.setText(precision.format(amt) + "");
                                g_tot.setText(precision.format(total) + "");
                                g_vat.setText(precision.format(vat) + "");
                                g_cgst.setText(precision.format(cgst) + "");
                                g_sgst.setText(precision.format(sgst) + "");
                                t_gst.setText(precision.format(gst) + "");
                                if(cess>0){
                                    tcess.setText(precision.format(cess) + "");

                                }
                                else {
                                    cesslinear.setVisibility(View.GONE);

                                }
                                if(servicecharge>0){
                                    tservicechage.setText(precision.format(servicecharge) + "");
                                }
                                else {
                                    linearservice.setVisibility(View.GONE);

                                }
                            } else {
                                Toast.makeText(ProvisionalBillActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                        break;
                    case "server":
                        Toast.makeText(ProvisionalBillActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(ProvisionalBillActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(ProvisionalBillActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(ProvisionalBillActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }

    }

    public void showData(List<ProvisionalDto2> itemsList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProvisionalBillActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        ProvisionalBillAdapter orderListAdapter = new ProvisionalBillAdapter(itemsList, ProvisionalBillActivity.this);
        recyclerView.setAdapter(orderListAdapter);
        orderListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    private int getPrinterCode(String printername) {
        int printerCode = 2;
        switch (printername) {
            case "TM_P20":
                printerCode = 2;
                break;
            case "TM_P80":
                printerCode = 5;
                break;
         /*   case 3:
                noOfDays = 31;
                break;
            case 4:
                noOfDays = 30;
                break;
            case 5:
                noOfDays = 31;
                break;
            case 6:
                noOfDays = 30;
                break;
            case 7:
                noOfDays = 31;
                break;
            case 8:
                noOfDays = 31;
                break;
            case 9:
                noOfDays = 30;
                break;
            case 10:
                noOfDays = 31;
                break;
            case 11:
                noOfDays = 30;
                break;
            case 12:
                noOfDays = 31;
                break;*/

        }
        return printerCode;

    }
}
