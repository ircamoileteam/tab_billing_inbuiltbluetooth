package com.irca.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.irca.Billing.MainActivity;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.Utils.Constants;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;

import org.json.JSONObject;

import java.util.Objects;

public class ConfigurationActivity extends AppCompatActivity {

    Button setButton, getButton;
    EditText codeEditText;
    TextView urlTextView;
    LinearLayout linearLayout;
    SharedPreferences sharedpreferences;
    String url;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        try {
            try {
                Objects.requireNonNull(this.getSupportActionBar()).hide();
            } catch (Throwable e) {
                e.printStackTrace();
            }

            getButton = findViewById(R.id.buttonOK);
            setButton = findViewById(R.id.buttonSet);
            codeEditText = findViewById(R.id.edit_textCode);
            urlTextView = findViewById(R.id.text_viewURL);
            linearLayout = findViewById(R.id.lin);
//            linearLayout.setVisibility(View.GONE);
            sharedpreferences = getSharedPreferences("URLS", Context.MODE_PRIVATE);
            url = sharedpreferences.getString("url", "");
            codeEditText.setText(url);


            getButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (codeEditText.getText().toString().trim().isEmpty()) {
                        Toast.makeText(ConfigurationActivity.this, "Please enter Club Code", Toast.LENGTH_SHORT).show();
                    } else {
                        new CallApi().execute(codeEditText.getText().toString().trim());
                    }
                }
            });

            setButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (codeEditText.getText().toString().trim().isEmpty()) {
                        Toast.makeText(ConfigurationActivity.this, "Please enter URL", Toast.LENGTH_SHORT).show();
                    } else {
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("url", codeEditText.getText().toString().trim());
                        editor.apply();
                        Toast.makeText(ConfigurationActivity.this, "URL updated successfully", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ConfigurationActivity.this, MainActivity.class));
                    }
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
            Toast.makeText(ConfigurationActivity.this,"ERROR-- : "+e,Toast.LENGTH_LONG).show();

        }
    }

    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(ConfigurationActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                ConnectionDetector cd = new ConnectionDetector(ConfigurationActivity.this);
                boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    getResult = api.GetTabBillingApplicationURL(strings[0]);
                    result = "" + getResult;
                    if (result.contains("true")) {
                        String jsonObject = getResult.getString("Value");
                        if (jsonObject.equalsIgnoreCase("success")) {
                            status = "success";
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ConfigurationActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        linearLayout.setVisibility(View.VISIBLE);
                        pd.dismiss();

                        break;
                    case "server":
                        Toast.makeText(ConfigurationActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(ConfigurationActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(ConfigurationActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(ConfigurationActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }

}
