package com.irca.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.adapter.ItemModsAdapter;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.dto.ItemMods;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class ModsShowActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SharedPreferences sharedpreferences;
    String posId, userId, billId;
    ImageView backImageView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_mods_show);
            try {
                Objects.requireNonNull(this.getSupportActionBar()).hide();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            recyclerView = findViewById(R.id.recycler_view);
            backImageView = findViewById(R.id.image_viewBack);

            sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            posId = sharedpreferences.getString("storeId", "");
            userId = sharedpreferences.getString("userId", "");
            billId = getIntent().getStringExtra("billId");

            backImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            if (posId.isEmpty()) {
                Toast.makeText(this, "Please select POS", Toast.LENGTH_SHORT).show();
                finish();
            } else if (userId.isEmpty()) {
                Toast.makeText(this, "Authentication failed, Please login again", Toast.LENGTH_SHORT).show();
            } else {
                new CallApi().execute();
            }


        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("StaticFieldLeak")
    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(ModsShowActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;
        List<ItemMods> itemsList = new ArrayList<>();

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                ConnectionDetector cd = new ConnectionDetector(ModsShowActivity.this);

                boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    getResult = api.GetTempBillItemModifierdetails(posId, billId);
                    result = "" + getResult;
                    if (result.contains("true")) {
//                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        JSONArray jsonArray = getResult.getJSONArray("Value");
//                        JSONArray jsonArray = jsonObject.getJSONArray("Table");
                        try {
                            Gson gson = new Gson();
                            itemsList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<ItemMods>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ModsShowActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        if (itemsList.size() > 0) {
                            showData(itemsList);
                        } else {
                            Toast.makeText(ModsShowActivity.this, "Details not found", Toast.LENGTH_SHORT).show();
                        }
                        pd.dismiss();
                        break;
                    case "server":
                        Toast.makeText(ModsShowActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(ModsShowActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(ModsShowActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(ModsShowActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }

    }

    public void showData(List<ItemMods> itemsList) {
        try {
            List<String> itemCodes = new ArrayList<>();
            List<ItemMods> itemModsList = new ArrayList<>();
            for (int i = 0; i < itemsList.size(); i++) {
                itemCodes.add(itemsList.get(i).getItemCode());
            }
            HashSet<String> hashSet = new HashSet<>(itemCodes);
            itemCodes.clear();
            itemCodes.addAll(hashSet);
            for (int i = 0; i < itemCodes.size(); i++) {
                ItemMods itemMods = new ItemMods();
                String name = "";
                StringBuilder mod = new StringBuilder();
                for (int j = 0; j < itemsList.size(); j++) {
                    if (itemCodes.get(i).equals(itemsList.get(j).getItemCode())) {
                        name = itemsList.get(j).getItemCode() + "-" + itemsList.get(j).getItemName();
                        if (itemsList.get(j).getModifier().isEmpty()) {
                            mod.append(itemsList.get(j).getModifierName()).append("-").append(itemsList.get(j).getModifierDetailName()).append("\n");
                        } else {
                            mod.append(itemsList.get(j).getModifier()).append("\n");
                        }
                    }
                }
                itemMods.setItemName(name);
                itemMods.setModifier(mod.toString());
                itemModsList.add(itemMods);

            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ModsShowActivity.this);
            recyclerView.setLayoutManager(linearLayoutManager);
            ItemModsAdapter orderListAdapter = new ItemModsAdapter(itemModsList, ModsShowActivity.this);
            recyclerView.setAdapter(orderListAdapter);
            orderListAdapter.notifyDataSetChanged();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }

}
