package com.irca.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;
import com.irca.cosmo.R;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;

public class QRCodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    public static String isScan = "";
    public static String Page = "";
    public static String QrCode = "";
    String barCode;
    int key = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("onCreate", "onCreate");
        mScannerView = new ZXingScannerView(QRCodeActivity.this);
        setContentView(mScannerView);
        key = getIntent().getIntExtra("key", 0);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                //  Toast.makeText(QrCodeScannerActivity.this, "Permission already granted", Toast.LENGTH_LONG).show();
            } else {
                requestPermission();
            }
        }
    }


    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access camera", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA},
                                                            REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(QRCodeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    @Override
    public void onResume() {
        super.onResume();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            if (checkPermission()) {
                if (mScannerView == null) {
                    mScannerView = new ZXingScannerView(QRCodeActivity.this);
                    setContentView(mScannerView);
                }
                mScannerView.setResultHandler(this);
                mScannerView.startCamera();
            } else {
                requestPermission();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {

        final String result = rawResult.getText();
        mScannerView.playSoundEffect(1);


        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    if (!result.isEmpty()) {
                        barCode = result;
                        try {
                            Log.e("handler", barCode); // Prints scan results
                            // show the scanner result into dialog box.
                            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(QRCodeActivity.this);
                            builder.setTitle("Scan Result");
                            builder.setMessage(barCode);
                            android.support.v7.app.AlertDialog alert1 = builder.create();
                            //alert1.show();
                            //Intent i = new Intent(Scancode.this, QrcodeDetails.class);
           /* Intent i = new Intent(Scancode.this, Qrcode_Scanning.class);
            i.putExtra("QrCode",rawResult.getText());
            i.putExtra("isScan","true");
            i.putExtra("Page","1");
            startActivity(i);*/
                            isScan = "true";
                            Page = "1";
                            QrCode = barCode;

//                            ScanQrcodeActivity.scanned = "true";
//                            ScanQrcodeActivity.qrCode = barCode;

                            mScannerView.stopCamera();
//                            finish();
                            startActivity(new Intent(QRCodeActivity.this, MenuNewListActivity.class).putExtra("qrcode", barCode).putExtra("key", 2));
                        } catch (Exception e) {
                            String mm = e.getMessage().toString();
                            String whatr = "";
                        }
                    } else {
                        Toast.makeText(QRCodeActivity.this, "Please scan again", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(QRCodeActivity.this, "Please scan again", Toast.LENGTH_LONG).show();
                }
            }
        });


//        Log.d("QRCodeScanner", rawResult.getText());
//        Log.d("QRCodeScanner", rawResult.getBarcodeFormat().toString());
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Scan Result");
//        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                mScannerView.resumeCameraPreview(QrCodeScannerActivity.this);
//            }
//        });
//        builder.setNeutralButton("Visit", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(result));
//                startActivity(browserIntent);
//            }
//        });
//        builder.setMessage(rawResult.getText());
//        AlertDialog alert1 = builder.create();
//        alert1.show();


    }

    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView mScannerView;

}
