package com.irca.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.adapter.TableTransferAdapter;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.dto.TableList;
import com.irca.dto.TableTransferDetails;
import com.irca.fields.OnRecyclerViewItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class TableTransferActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ConnectionDetector cd;
    boolean isInternetPresent;
    SharedPreferences sharedpreferences;
    String posId, userId;
    ImageView backImageView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_transfer);
        try {
            try {
                Objects.requireNonNull(this.getSupportActionBar()).hide();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            recyclerView = findViewById(R.id.recycler_view);
            backImageView = findViewById(R.id.image_viewBack);


            sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            posId = sharedpreferences.getString("storeId", "");
            userId = sharedpreferences.getString("userId", "");

            backImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            if (posId.isEmpty()) {
                Toast.makeText(this, "Please select POS", Toast.LENGTH_SHORT).show();
                finish();
            } else if (userId.isEmpty()) {
                Toast.makeText(this, "Authentication failed, Please login again", Toast.LENGTH_SHORT).show();
            } else {
                new CallApi().execute();
            }


        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(TableTransferActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;
        List<TableTransferDetails> itemsList = new ArrayList<>();

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(TableTransferActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.CmsGetRunningOrders(posId, userId);
                    result = "" + getResult;
                    if (result.contains("true")) {
                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        JSONArray jsonArray = jsonObject.getJSONArray("Table");
                        try {
                            Gson gson = new Gson();
                            itemsList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<TableTransferDetails>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(TableTransferActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        if (itemsList.size() > 0) {
                            showData(itemsList);
                        } else {
                            Toast.makeText(TableTransferActivity.this, "Orders not found", Toast.LENGTH_SHORT).show();
                        }
                        pd.dismiss();
                        break;
                    case "server":
                        Toast.makeText(TableTransferActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(TableTransferActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(TableTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(TableTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }

    public void showData(List<TableTransferDetails> itemsList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TableTransferActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        TableTransferAdapter orderListAdapter = new TableTransferAdapter(itemsList, TableTransferActivity.this);
        recyclerView.setAdapter(orderListAdapter);
        orderListAdapter.notifyDataSetChanged();
        orderListAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener<TableTransferAdapter.MultiSelectDialogViewHolder, TableTransferDetails>() {
            @Override
            public void onRecyclerViewItemClick(@NonNull TableTransferAdapter.MultiSelectDialogViewHolder multiSelectDialogViewHolder, @NonNull View view, @NonNull TableTransferDetails tableTransferDetails, int position) {
                try {
                    if (tableTransferDetails.getOTNo().isEmpty()) {
                        Toast.makeText(TableTransferActivity.this, "OT Number not found", Toast.LENGTH_SHORT).show();
                    } else {
                        new CallTablesApi().execute(tableTransferDetails.getOTNo());
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showTableData(final List<TableList> itemsList, final String otNo) {
        try {
//            final CharSequence[] choice = {"Choose from Gallery", "Capture a photo"};
            final CharSequence[] choice = new CharSequence[itemsList.size()];

            for (int i = 0; i < itemsList.size(); i++) {
                choice[i] = itemsList.get(i).getTableNumber();
            }

            final int[] from = new int[1]; //This must be declared as global !
            from[0] = -1;
            final AlertDialog.Builder alert = new AlertDialog.Builder(TableTransferActivity.this);
            alert.setTitle("Select Table for Transfer");
            alert.setSingleChoiceItems(choice, -1, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    from[0] = which;
                }
            });
            alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {

                        if (from[0] >= 0) {
                            String id = "";
                            for (int i = 0; i < itemsList.size(); i++) {
                                if (choice[from[0]].equals(itemsList.get(i).getTableNumber())) {
                                    id = itemsList.get(i).getID();
                                }
                            }
                            if (id.isEmpty()) {
                                Toast.makeText(TableTransferActivity.this, "Please select Table", Toast.LENGTH_SHORT).show();
                            } else {
                                new CallUpdateTableApi().execute(id, otNo);
                            }
                        } else {
                            Toast.makeText(TableTransferActivity.this, "Please select Table", Toast.LENGTH_SHORT).show();
                            alert.show();
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                        Toast.makeText(TableTransferActivity.this, "Please select Table", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            alert.show();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public class CallTablesApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(TableTransferActivity.this);
        String status = "";
        String result = "";
        String error = "";
        String OT = "";
        ProgressDialog pd;
        List<TableList> itemsList = new ArrayList<>();

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(TableTransferActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    OT = strings[0];
                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.CmsGetTablesForTransfer(posId, strings[0]);
                    result = "" + getResult;
                    if (result.contains("true")) {
                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        JSONArray jsonArray = jsonObject.getJSONArray("Table");
                        try {
                            Gson gson = new Gson();
                            itemsList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<TableList>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(TableTransferActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        if (itemsList.size() > 0) {
                            showTableData(itemsList, OT);
                        } else {
                            Toast.makeText(TableTransferActivity.this, "Table List not available", Toast.LENGTH_SHORT).show();
                        }
                        pd.dismiss();
                        break;
                    case "server":
                        Toast.makeText(TableTransferActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(TableTransferActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(TableTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(TableTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }

    public class CallUpdateTableApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(TableTransferActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(TableTransferActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.CmsUpdateTableTransfer(strings[0], strings[1]);
                    result = "" + getResult;
                    if (result.contains("true")) {
                        try {
                            JSONObject jsonObject = getResult.getJSONObject("Value");
                            JSONArray jsonArray = jsonObject.getJSONArray("Table");
                            String billId = jsonArray.getJSONObject(0).getString("billid");
                            if (!billId.isEmpty()) {
                                status = "success";
                            }
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(TableTransferActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        pd.dismiss();
                        Toast.makeText(TableTransferActivity.this, "updated", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case "server":
                        Toast.makeText(TableTransferActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(TableTransferActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(TableTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(TableTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }

}
