package com.irca.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.adapter.MenuNew1Adapter;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.dto.MenuNewDto;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class MenuNewListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ConnectionDetector cd;
    boolean isInternetPresent;
    SharedPreferences sharedpreferences;
    String posId, userId;
    int key = 0;

    //    ImageView backImageView;
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_new_list);
        try {
            try {
                Objects.requireNonNull(this.getSupportActionBar()).hide();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            recyclerView = findViewById(R.id.recycler_view);
//            backImageView = findViewById(R.id.image_viewBack);


            sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            posId = sharedpreferences.getString("storeId", "");
            userId = sharedpreferences.getString("userId", "");
            key = getIntent().getIntExtra("key", 0);

//            backImageView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    finish();
//                }
//            });

            if (key == 2) {
                String qrCode = getIntent().getStringExtra("qrcode");
                new CallApi().execute(qrCode, "");

            } else {
                if (posId.isEmpty()) {
                    Toast.makeText(this, "Please select POS", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    new CallApi().execute("", posId);
                }
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(MenuNewListActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;
        List<MenuNewDto> itemsList = new ArrayList<>();

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(MenuNewListActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
//                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.GetMenuListForMenuCard(strings[0], strings[1]);
                    result = "" + getResult;
                    if (result.contains("true")) {
                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        JSONArray jsonArray = jsonObject.getJSONArray("Table");
                        try {
                            Gson gson = new Gson();
                            itemsList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<MenuNewDto>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MenuNewListActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        if (itemsList.size() > 0) {
                            showData(itemsList);
                        } else {
                            Toast.makeText(MenuNewListActivity.this, "Menu List not available", Toast.LENGTH_SHORT).show();
                        }
                        pd.dismiss();
                        break;
                    case "server":
                        Toast.makeText(MenuNewListActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(MenuNewListActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(MenuNewListActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(MenuNewListActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }

    public void showData(List<MenuNewDto> itemsList) {
//        List<MenuNewDto> itemsList1 = new ArrayList<>();
//
//        MenuNewDto menuNewDto;
//        MenuNewDto menuNewDto1;
//        List<MenuNewDto> itemsList2;
//
//        itemsList2 = new ArrayList<>();
//        menuNewDto1 = new MenuNewDto();
//        menuNewDto1.setMenuName("Eggs");
//        menuNewDto1.setRate("₹ 15");
//        itemsList2.add(menuNewDto1);
//
//        menuNewDto1 = new MenuNewDto();
//        menuNewDto1.setMenuName("Idli");
//        menuNewDto1.setRate("₹ 20");
//        itemsList2.add(menuNewDto1);
//
//        menuNewDto1 = new MenuNewDto();
//        menuNewDto1.setMenuName("Dosa");
//        menuNewDto1.setRate("₹ 35");
//        itemsList2.add(menuNewDto1);
//
//        menuNewDto = new MenuNewDto();
//        menuNewDto.setMenuName("Breakfast");
//        menuNewDto.setMenuNewDtoList(itemsList2);
//        itemsList1.add(menuNewDto);
//
//
//        itemsList2 = new ArrayList<>();
//        menuNewDto1 = new MenuNewDto();
//        menuNewDto1.setMenuName("Ice Tea");
//        menuNewDto1.setRate("₹ 45");
//        itemsList2.add(menuNewDto1);
//
//        menuNewDto1 = new MenuNewDto();
//        menuNewDto1.setMenuName("Mango Juice");
//        menuNewDto1.setRate("₹ 50");
//        itemsList2.add(menuNewDto1);
//
//        menuNewDto1 = new MenuNewDto();
//        menuNewDto1.setMenuName("Milk Tea");
//        menuNewDto1.setRate("₹ 25");
//        itemsList2.add(menuNewDto1);
//
//        menuNewDto = new MenuNewDto();
//        menuNewDto.setMenuName("Beverages");
//        menuNewDto.setMenuNewDtoList(itemsList2);
//        itemsList1.add(menuNewDto);
//
//
//
//        itemsList2 = new ArrayList<>();
//        menuNewDto1 = new MenuNewDto();
//        menuNewDto1.setMenuName("Baked Potatoes");
//        menuNewDto1.setRate("₹ 65");
//        itemsList2.add(menuNewDto1);
//
//        menuNewDto1 = new MenuNewDto();
//        menuNewDto1.setMenuName("Salads");
//        menuNewDto1.setRate("₹ 50");
//        itemsList2.add(menuNewDto1);
//
//        menuNewDto1 = new MenuNewDto();
//        menuNewDto1.setMenuName("French Fries");
//        menuNewDto1.setRate("₹ 40");
//        itemsList2.add(menuNewDto1);
//
//        menuNewDto = new MenuNewDto();
//        menuNewDto.setMenuName("Side Orders");
//        menuNewDto.setMenuNewDtoList(itemsList2);
//        itemsList1.add(menuNewDto);

        ArrayList<String> stringList = new ArrayList<>();
        for (int i = 0; i < itemsList.size(); i++) {
            stringList.add(itemsList.get(i).getItemGroup());
        }
        HashSet<String> hashSet = new HashSet<>(stringList);
        stringList.clear();
        stringList.addAll(hashSet);

        List<MenuNewDto> itemsList1 = new ArrayList<>();

        for (int i = 0; i < stringList.size(); i++) {
            MenuNewDto menuNewDto = new MenuNewDto();
            menuNewDto.setItemGroup(stringList.get(i));
            List<MenuNewDto> itemsList2 = new ArrayList<>();
            for (int j = 0; j < itemsList.size(); j++) {
                if (stringList.get(i).equals(itemsList.get(j).getItemGroup())) {
                    itemsList2.add(itemsList.get(j));
                }
            }
            menuNewDto.setMenuNewDtoList(itemsList2);
            itemsList1.add(menuNewDto);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MenuNewListActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        MenuNew1Adapter orderListAdapter = new MenuNew1Adapter(itemsList1, MenuNewListActivity.this);
        recyclerView.setAdapter(orderListAdapter);
        orderListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(MenuNewListActivity.this, NewMenuActivity.class));
    }

}

