package com.irca.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.adapter.OTItemTransferAdapter;
import com.irca.adapter.OTTransferAdapter;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.dto.MemberDtos;
import com.irca.dto.OTItemDto;
import com.irca.dto.OTTransfer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class ItemOTTransferActivity extends AppCompatActivity {

    RecyclerView recyclerView, recyclerView1;
    ConnectionDetector cd;
    boolean isInternetPresent;
    SharedPreferences sharedpreferences;
    String posId, userId;
    ImageView backImageView;
    List<OTTransfer> tableTransferDetailsList = new ArrayList<>();
    List<OTItemDto> otTransferItemList = new ArrayList<>();
    OTTransferAdapter otTransferAdapter;
    OTItemTransferAdapter otItemTransferAdapter;
    Button searchButton;
    EditText idEditText, otEditText, tableEditText;
    RadioGroup radioGroup;
    TextView placeTextView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_ottransfer);
        try {
            try {
                Objects.requireNonNull(this.getSupportActionBar()).hide();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            recyclerView = findViewById(R.id.recycler_view);
            recyclerView1 = findViewById(R.id.recycler_view1);
            backImageView = findViewById(R.id.image_viewBack);
            searchButton = findViewById(R.id.buttonSearch);
            idEditText = findViewById(R.id.edit_textId);
            otEditText = findViewById(R.id.edit_textOT);
            tableEditText = findViewById(R.id.edit_textTable);
            radioGroup = findViewById(R.id.radio_group);
            placeTextView = findViewById(R.id.tv_place);


            sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            posId = sharedpreferences.getString("storeId", "");
            userId = sharedpreferences.getString("userId", "");

            backImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
            placeTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int radioButtonID = radioGroup.getCheckedRadioButtonId();
                    RadioButton r = radioGroup.findViewById(radioButtonID);
                    String string = r.getText().toString();
                    if (string.equalsIgnoreCase("OT Wise")) {
                        if (OTTransferAdapter.selectedList.size() > 0) {
                            OTTransfer(tableTransferDetailsList.get(0).getMemberID(), tableTransferDetailsList.get(0).getBillID());
                        } else {
                            Toast.makeText(ItemOTTransferActivity.this, "Please select an OT", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (OTItemTransferAdapter.selectedList.size() > 0) {
                            ItemTransfer(otTransferItemList.get(0).getMemberID(), otTransferItemList.get(0).getBillID(), otTransferItemList.get(0).getMemberName(), otTransferItemList.get(0).getOTNo());
                        } else {
                            Toast.makeText(ItemOTTransferActivity.this, "Please select an Item", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            if (posId.isEmpty()) {
                Toast.makeText(this, "Please select POS", Toast.LENGTH_SHORT).show();
                finish();
            } else if (userId.isEmpty()) {
                Toast.makeText(this, "Authentication failed, Please login again", Toast.LENGTH_SHORT).show();
                finish();
            }

            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                    int radioButtonID = radioGroup.getCheckedRadioButtonId();
//                    RadioButton r = radioGroup.findViewById(radioButtonID);
//                    String string = r.getText().toString();
//                    if (string.equalsIgnoreCase("OT Wise")) {
//
//                    } else {
//
//                    }
                    try {

                        OTTransferAdapter.selectedList.clear();
                        OTItemTransferAdapter.selectedList.clear();
                        otTransferItemList.clear();
                        showItemData(otTransferItemList);
                        tableTransferDetailsList.clear();
                        showData(tableTransferDetailsList);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int radioButtonID = radioGroup.getCheckedRadioButtonId();
                    RadioButton r = radioGroup.findViewById(radioButtonID);
                    String string = r.getText().toString();
                    if (string.equalsIgnoreCase("OT Wise")) {
                        if (idEditText.getText().toString().trim().isEmpty()) {
                            Toast.makeText(ItemOTTransferActivity.this, "Please enter MemberId", Toast.LENGTH_SHORT).show();
                        } else {
                            OTTransferAdapter.selectedList.clear();
                            OTItemTransferAdapter.selectedList.clear();
                            tableTransferDetailsList.clear();
                            showData(tableTransferDetailsList);
                            new CallApi().execute(posId, idEditText.getText().toString().trim(), otEditText.getText().toString().trim(), tableEditText.getText().toString().trim());
                        }
                    } else {
                        if (otEditText.getText().toString().trim().isEmpty()) {
                            Toast.makeText(ItemOTTransferActivity.this, "Please enter OT Number", Toast.LENGTH_SHORT).show();
                        } else {
                            OTTransferAdapter.selectedList.clear();
                            OTItemTransferAdapter.selectedList.clear();
                            otTransferItemList.clear();
                            showItemData(otTransferItemList);
                            new CallItemApi().execute(posId, idEditText.getText().toString().trim(), otEditText.getText().toString().trim(), tableEditText.getText().toString().trim());
                        }

                    }
                }
            });
//            searchButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (OTTransferAdapter.selectedList.size() > 0) {
//                        for (int i = 0; i < OTTransferAdapter.selectedList.size(); i++) {
//                            Toast.makeText(ItemOTTransferActivity.this, "" + OTTransferAdapter.selectedList.get(i), Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//            });

//            TableTransferDetails tableTransferDetails;
//
//            tableTransferDetails = new TableTransferDetails();
//            tableTransferDetails.setMembername("10001-Akash Roy");
//            tableTransferDetails.setOTNo("2020OT04");
//            tableTransferDetails.setTableNumber("T5");
//            tableTransferDetails.setClick(false);
//            tableTransferDetailsList.add(tableTransferDetails);
//
//            tableTransferDetails = new TableTransferDetails();
//            tableTransferDetails.setMembername("10003-Virat Sing");
//            tableTransferDetails.setOTNo("2020OT04");
//            tableTransferDetails.setTableNumber("T3");
//            tableTransferDetails.setClick(false);
//            tableTransferDetailsList.add(tableTransferDetails);
//
//            tableTransferDetails = new TableTransferDetails();
//            tableTransferDetails.setMembername("10002-Suresh Naidu");
//            tableTransferDetails.setOTNo("2020OT04");
//            tableTransferDetails.setTableNumber("T1");
//            tableTransferDetails.setClick(false);
//            tableTransferDetailsList.add(tableTransferDetails);
//
//            tableTransferDetails = new TableTransferDetails();
//            tableTransferDetails.setMembername("10012-Sia Jain");
//            tableTransferDetails.setOTNo("2020OT04");
//            tableTransferDetails.setTableNumber("T8");
//            tableTransferDetails.setClick(false);
//            tableTransferDetailsList.add(tableTransferDetails);
//
//            tableTransferDetails = new TableTransferDetails();
//            tableTransferDetails.setMembername("10065-Aron Dubey");
//            tableTransferDetails.setOTNo("2020OT04");
//            tableTransferDetails.setTableNumber("T9");
//            tableTransferDetails.setClick(false);
//            tableTransferDetailsList.add(tableTransferDetails);
//
//            tableTransferDetails = new TableTransferDetails();
//            tableTransferDetails.setMembername("100022-J King");
//            tableTransferDetails.setOTNo("2020OT04");
//            tableTransferDetails.setTableNumber("T12");
//            tableTransferDetails.setClick(false);
//            tableTransferDetailsList.add(tableTransferDetails);
//
//            tableTransferDetails = new TableTransferDetails();
//            tableTransferDetails.setMembername("10001-mathews");
//            tableTransferDetails.setOTNo("2020OT04");
//            tableTransferDetails.setTableNumber("T14");
//            tableTransferDetails.setClick(false);
//            tableTransferDetailsList.add(tableTransferDetails);


//            showData(tableTransferDetailsList);

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

//    private void filter(String text) {
//        try {
//            List<OTTransfer> itemsArrayList = new ArrayList<>();
//            for (int i = 0; i < tableTransferDetailsList.size(); i++) {
//                if (tableTransferDetailsList.get(i).getOTNo().toLowerCase().contains(text.toLowerCase()) || tableTransferDetailsList.get(i).getTableNumber().toLowerCase().contains(text.toLowerCase())) {
//                    itemsArrayList.add(tableTransferDetailsList.get(i));
//                }
//            }
////            otTransferAdapter.filterList(itemsArrayList, this);
//        } catch (Throwable e) {
//            e.printStackTrace();
//        }
//    }


    @SuppressLint("StaticFieldLeak")
    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(ItemOTTransferActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;
        List<OTTransfer> itemsList = new ArrayList<>();

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(ItemOTTransferActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
//                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.GetOTSummaryForTransfer(strings[0], strings[1], strings[2], strings[3]);
                    result = "" + getResult;
                    if (result.contains("true")) {
//                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        JSONArray jsonArray = getResult.getJSONArray("Value");
                        try {
                            Gson gson = new Gson();
                            itemsList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<OTTransfer>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ItemOTTransferActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        if (itemsList.size() > 0) {
                            tableTransferDetailsList.addAll(itemsList);
                            showData(itemsList);
                        } else {
                            Toast.makeText(ItemOTTransferActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                        }
                        pd.dismiss();
                        break;
                    case "server":
                        Toast.makeText(ItemOTTransferActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(ItemOTTransferActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(ItemOTTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(ItemOTTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void OTTransfer(final String memberId, final String billId) {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            Objects.requireNonNull(dialog.getWindow()).setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            dialog.setContentView(R.layout.dialog_ot_transfer);
            dialog.setCancelable(false);
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final EditText fromEditText = dialog.findViewById(R.id.edit_textFrom);
            final EditText otEditText = dialog.findViewById(R.id.edit_textOT);
            final EditText toEditText = dialog.findViewById(R.id.edit_textTo);
            final TextView getTextView = dialog.findViewById(R.id.text_viewGet);
            final EditText mEditText = dialog.findViewById(R.id.edit_textM);
            final EditText iEditText = dialog.findViewById(R.id.edit_textI);
            final LinearLayout mLinearLayout = dialog.findViewById(R.id.linearLayoutM);
            final Button cancelButton = dialog.findViewById(R.id.buttonCancel);
            final Button transferButton = dialog.findViewById(R.id.buttonTransfer);
//            fromEditText.setText(tableTransferDetailsList.get(0).getItemCode() + " - " + itemCartDtoList.get(position).getItemName());

            StringBuilder otList = new StringBuilder();
            for (int i = 0; i < OTTransferAdapter.selectedList.size(); i++) {
                if (otList.length() == 0) {
                    otList = new StringBuilder(OTTransferAdapter.selectedList.get(i));
                } else {
                    otList.append(", ").append(OTTransferAdapter.selectedList.get(i));
                }
            }
            otEditText.setText(otList);

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            toEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    mLinearLayout.setVisibility(View.GONE);
                    iEditText.setText("");
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            final StringBuilder finalOtList = otList;
            transferButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (toEditText.getText().toString().trim().isEmpty()) {
                        Toast.makeText(ItemOTTransferActivity.this, "Please enter AccNo", Toast.LENGTH_SHORT).show();
                    } else if (iEditText.getText().toString().trim().isEmpty()) {
                        Toast.makeText(ItemOTTransferActivity.this, "Unable to get Member ID", Toast.LENGTH_SHORT).show();
                    } else if (OTTransferAdapter.selectedList.size() <= 0) {
                        Toast.makeText(ItemOTTransferActivity.this, "Please select OTs", Toast.LENGTH_SHORT).show();
                    } else {
                        new CallOTSubmitApi().execute(memberId, iEditText.getText().toString().trim(), billId, finalOtList.toString(), posId, userId);
                    }
                }
            });
            getTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (toEditText.getText().toString().trim().isEmpty()) {
                        mLinearLayout.setVisibility(View.GONE);
                        iEditText.setText("");
                        Toast.makeText(ItemOTTransferActivity.this, "Please enter AccNo", Toast.LENGTH_SHORT).show();
                    } else {
                        mLinearLayout.setVisibility(View.GONE);
                        iEditText.setText("");
                        CallMemberApi callMemberDetailsApi = new CallMemberApi(mEditText, mLinearLayout, iEditText);
                        callMemberDetailsApi.execute(toEditText.getText().toString().trim());
                    }
                }
            });
            dialog.show();


        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void ItemTransfer(final String memberId, final String billId, final String memberName, final String otNo) {
        try {
            final Dialog dialog = new Dialog(this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            Objects.requireNonNull(dialog.getWindow()).setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            dialog.setContentView(R.layout.dialog_item_trasnfer);
            dialog.setCancelable(false);
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final EditText fromEditText = dialog.findViewById(R.id.edit_textFrom);
            final EditText otEditText = dialog.findViewById(R.id.edit_textOT);
            final EditText toEditText = dialog.findViewById(R.id.edit_textTo);
            final TextView getTextView = dialog.findViewById(R.id.text_viewGet);
            final EditText mEditText = dialog.findViewById(R.id.edit_textM);
            final EditText iEditText = dialog.findViewById(R.id.edit_textI);
            final LinearLayout mLinearLayout = dialog.findViewById(R.id.linearLayoutM);
            final Button cancelButton = dialog.findViewById(R.id.buttonCancel);
            final Button transferButton = dialog.findViewById(R.id.buttonTransfer);
//            fromEditText.setText(tableTransferDetailsList.get(0).getItemCode() + " - " + itemCartDtoList.get(position).getItemName());

            StringBuilder otList = new StringBuilder();
            for (int i = 0; i < OTItemTransferAdapter.selectedList.size(); i++) {
                if (otList.length() == 0) {
                    otList = new StringBuilder(OTItemTransferAdapter.selectedList.get(i));
                } else {
                    otList.append(", ").append(OTItemTransferAdapter.selectedList.get(i));
                }
            }
            otEditText.setText(otList);

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            final StringBuilder finalOtList = otList;
            transferButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (toEditText.getText().toString().trim().isEmpty()) {
                        Toast.makeText(ItemOTTransferActivity.this, "Please enter AccNo", Toast.LENGTH_SHORT).show();
                    } else if (iEditText.getText().toString().trim().isEmpty()) {
                        Toast.makeText(ItemOTTransferActivity.this, "Unable to get Member ID", Toast.LENGTH_SHORT).show();
                    } else if (OTItemTransferAdapter.selectedList.size() <= 0) {
                        Toast.makeText(ItemOTTransferActivity.this, "Please select Items", Toast.LENGTH_SHORT).show();
                    } else {
                        new CallItemsSubmitApi().execute(memberId, iEditText.getText().toString().trim(), billId, otNo, finalOtList.toString(), posId, userId);
                    }
                }
            });
            toEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    mLinearLayout.setVisibility(View.GONE);
                    iEditText.setText("");
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            getTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (toEditText.getText().toString().trim().isEmpty()) {
                        mLinearLayout.setVisibility(View.GONE);
                        iEditText.setText("");
                        Toast.makeText(ItemOTTransferActivity.this, "Please enter AccNo", Toast.LENGTH_SHORT).show();
                    } else {
                        mLinearLayout.setVisibility(View.GONE);
                        iEditText.setText("");
                        CallMemberApi callMemberDetailsApi = new CallMemberApi(mEditText, mLinearLayout, iEditText);
                        callMemberDetailsApi.execute(toEditText.getText().toString().trim());
                    }
                }
            });
            dialog.show();


        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class CallItemApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(ItemOTTransferActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;
        List<OTItemDto> itemsList = new ArrayList<>();

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(ItemOTTransferActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
//                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.GetOTSummaryForItemsTransfer(strings[0], strings[1], strings[2], strings[3]);
                    result = "" + getResult;
                    if (result.contains("true")) {
//                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        JSONArray jsonArray = getResult.getJSONArray("Value");
                        try {
                            Gson gson = new Gson();
                            itemsList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<OTItemDto>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ItemOTTransferActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        if (itemsList.size() > 0) {
                            otTransferItemList.addAll(itemsList);
                            showItemData(itemsList);
                        } else {
                            Toast.makeText(ItemOTTransferActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                        }
                        pd.dismiss();
                        break;
                    case "server":
                        Toast.makeText(ItemOTTransferActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(ItemOTTransferActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(ItemOTTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(ItemOTTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    public class CallMemberApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(ItemOTTransferActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;
        EditText nameEditText, idEditText;
        LinearLayout mLinearLayout;
        List<MemberDtos> itemsList = new ArrayList<>();


        public CallMemberApi(EditText one, LinearLayout two, EditText three) {
            nameEditText = one;
            mLinearLayout = two;
            idEditText = three;

        }

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(ItemOTTransferActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.GetMemberDetailsForOTTransfer(strings[0], date);
                    result = "" + getResult;
                    if (result.contains("true")) {
//                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        JSONArray jsonArray = getResult.getJSONArray("Value");
                        try {
                            Gson gson = new Gson();
                            itemsList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<MemberDtos>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ItemOTTransferActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        pd.dismiss();
                        if (itemsList.size() > 0) {
                            if (itemsList.get(0).getID() != null) {
                                mLinearLayout.setVisibility(View.VISIBLE);
                                nameEditText.setText(itemsList.get(0).getMemberName());
                                idEditText.setText(itemsList.get(0).getID());
                            } else {
                                Toast.makeText(ItemOTTransferActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(ItemOTTransferActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case "server":
                        Toast.makeText(ItemOTTransferActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(ItemOTTransferActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(ItemOTTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(ItemOTTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    public class CallOTSubmitApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(ItemOTTransferActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;

        List<MemberDtos> itemsList = new ArrayList<>();


        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(ItemOTTransferActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.InsertOTTransferDetails(strings[0], strings[1], strings[2], strings[3], strings[4], strings[5]);
                    result = "" + getResult;
                    if (result.contains("true")) {
//                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        String s = getResult.getString("Value");
                        try {
                            if (s.equalsIgnoreCase("success")) {
                                status = "success";
                            }
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ItemOTTransferActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        pd.dismiss();
                        Toast.makeText(ItemOTTransferActivity.this, "Transferred Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case "server":
                        Toast.makeText(ItemOTTransferActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(ItemOTTransferActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(ItemOTTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(ItemOTTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    public class CallItemsSubmitApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(ItemOTTransferActivity.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;


        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(ItemOTTransferActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
//                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.InsertOTItemTransferDetails(strings[0], strings[1], strings[2], strings[3], strings[4], strings[5], strings[6]);
                    result = "" + getResult;
                    if (result.contains("true")) {
//                        JSONObject jsonObject = getResult.getJSONObject("Value");
                        String s = getResult.getString("Value");
                        try {
                            if (s.equalsIgnoreCase("success")) {
                                status = "success";
                            }
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ItemOTTransferActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        pd.dismiss();
                        Toast.makeText(ItemOTTransferActivity.this, "Transferred Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case "server":
                        Toast.makeText(ItemOTTransferActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(ItemOTTransferActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(ItemOTTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(ItemOTTransferActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }

    }

    public void showData(List<OTTransfer> itemsList) {
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView1.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ItemOTTransferActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        otTransferAdapter = new OTTransferAdapter(itemsList, ItemOTTransferActivity.this);
        recyclerView.setAdapter(otTransferAdapter);
        otTransferAdapter.notifyDataSetChanged();
    }

    public void showItemData(List<OTItemDto> itemsList) {
        recyclerView.setVisibility(View.GONE);
        recyclerView1.setVisibility(View.VISIBLE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ItemOTTransferActivity.this);
        recyclerView1.setLayoutManager(linearLayoutManager);
        otItemTransferAdapter = new OTItemTransferAdapter(itemsList, ItemOTTransferActivity.this);
        recyclerView1.setAdapter(otItemTransferAdapter);
        otItemTransferAdapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
        finish();
    }

}
