package com.irca.Billing;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.MaterialFloatLabel.SearchableSpinner;
import com.irca.Printer.PlaceOrder_bill;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.cosmo.Alert_ItemDescription;
import com.irca.cosmo.CloseOrderdetails;
import com.irca.cosmo.OT_Reprint;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.cosmo.Search_Item;
import com.irca.db.Dbase;
import com.irca.dto.ModifiersDto;
import com.irca.dto.SendModifiers;
import com.irca.fields.AndroidTempbilldetails;
import com.irca.fields.AndroidTempbilling;
import com.irca.fields.CloseorderItems;
import com.irca.fields.ItemList;
import com.irca.fields.Table;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
/*

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
*/


public class MakeOrder extends AppCompatActivity {
    Bundle bundle;
    int tableId;
    String table = "", accesstype = "", paxcount = "0";
    String creditLimit = "", TempBillAmount = "0", debitbal = "", creditAccountno = "", creditName = "", memberId = "", cardType = "", _params = "", waiterid = "", waiternamecode = "", ct = "";
    int position;
    TextView openBalance, debitBalance, steward, tableNo;
    EditText editText;
    LinearLayout itemView;
    TelephonyManager tel;
    String serialNo;
    DecimalFormat format = new DecimalFormat("#.##");
    double noOfCounts = 0;
    TextView txt_itemcount;
    View view = null;
    TextView itemName, itemCount, memberName, memAccountNo, rate, rateT, tax;
    ImageView add, info, subtract, search;
    Dbase db;
    String groupId = "";
    //CheckBox happyHour;
    FloatingActionButton fab;
    Button placeOrder;

    float Totalamount = 0;
    float TotalAmountWithTax = 0;
    ProgressDialog pd;
    public static ArrayList<ItemList> placeOrder_list;
    SharedPreferences sharedpreferences;
    String pos = "";
    String newTableNo = "0", newTableNo1 = "0";
    String C = "0";
    String referenceNo = "";
    String Bill = "";
    String idob = "";
    String idoa = "";
    String otType = "";
    String finalBillMode = "";
    String bill = "";
    String storeId = "";
    //close order
    ArrayList<String> paymentMode;
    public static ArrayList<CloseorderItems> closeorderItemsArrayList = new ArrayList<>();
    String mode = "";
    String[] payments;
    String IsOTRequired = "", waiterNameFromLogin = "";
    String OTCreditLimitNotRequired = "";
    String isCashCardFilled = "";
    String contractorsId = "";
    EditText misguest_etxt, guest_etxt1;
    String mguest = "";
    Boolean isInternetPresent = false;
    public static ArrayList<CloseorderItems> closeorderItemsArrayList1 = new ArrayList<>();
    ConnectionDetector cd;
  //  Spinner ;
    ArrayAdapter arrayAdapter;
    String isTable, isPax, stewardBilling, isSteward;
    Spinner tableSpinner;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_place_order_new2);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        pos = sharedpreferences.getString("StoreName", "");
        Bill = sharedpreferences.getString("BillMode", "");
        otType = sharedpreferences.getString("billType", "");
        bill = sharedpreferences.getString("Bill", "");
        storeId = sharedpreferences.getString("storeId", "");
        isTable = sharedpreferences.getString("isTable", "");
        isPax = sharedpreferences.getString("isPax", "");
        stewardBilling = sharedpreferences.getString("StewardBilling", "");
        isSteward = sharedpreferences.getString("isSteward", "");
        IsOTRequired = sharedpreferences.getString("IsOTRequired", "");
        waiterNameFromLogin = sharedpreferences.getString("WaiterName", "");
        OTCreditLimitNotRequired = sharedpreferences.getString("OTCreditLimitNotRequired", "");
        actionBarSetup();
        bundle = getIntent().getExtras();
        table = bundle.getString("tableNo");
        creditLimit = bundle.getString("creditLimit");
        TempBillAmount = bundle.getString("TempBillAmount");
        if (bundle.getString("TempBillAmount") != null) {
            TempBillAmount = bundle.getString("TempBillAmount");
        }
        TempBillAmount=""+0;

        debitbal = bundle.getString("debitbal");
        creditAccountno = bundle.getString("creditAno");
        creditName = bundle.getString("creditName");
        memberId = bundle.getString("memberId");
        tableId = bundle.getInt("tableId");
        cardType = bundle.getString("cardType");
        referenceNo = bundle.getString("referenceNo");
        waiterid = bundle.getString("waiterid");
        position = bundle.getInt("position");
        waiternamecode = bundle.getString("waiternamecode");
        finalBillMode = bundle.getString("billmode");
        idob = bundle.getString("dob");
        idoa = bundle.getString("doa");
        paxcount = bundle.getString("paxcnt");
        String access = bundle.getString("accessType");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (Objects.requireNonNull(access).equalsIgnoreCase("nfc")) {
                accesstype = "1";
            } else {
                accesstype = "0";
            }
        }
        tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        serialNo = Build.SERIAL;

        C = creditLimit;
        tableSpinner = findViewById(R.id.spinnerTable);

        misguest_etxt = (EditText) findViewById(R.id.guest_etxt);
        guest_etxt1 = (EditText) findViewById(R.id.guest_etxt1);
        openBalance = (TextView) findViewById(R.id.op_bal);
        debitBalance = (TextView) findViewById(R.id.debit_bal);
        steward = (TextView) findViewById(R.id.steward);
        tableNo = (TextView) findViewById(R.id.table);
        memberName = (TextView) findViewById(R.id.memName);
        memAccountNo = (TextView) findViewById(R.id.accNo);
        itemView = (LinearLayout) findViewById(R.id.itemView);
        placeOrder = (Button) findViewById(R.id.button_placeOrder);
        placeOrder.setVisibility(View.GONE);
        // txt_itemcount = (TextView) findViewById(R.id.txtCount);
        db = new Dbase(MakeOrder.this);
        debitBalance.setText("Debit Balance :" + debitbal + "Rs.");
        if (bill.equals("Party Billing")) {
            openBalance.setText("Booking Number  :" + creditLimit + "");
            cardType = "PARTY CARD";
        } else {
            openBalance.setText("Opening Balance :" + creditLimit + "Rs.");
        }
        if (waiternamecode != null) {
            steward.setText("Waiter         :" + waiternamecode);

        } else {
            steward.setText("Waiter         Select");
        }
        // memberName.setText(""+creditName + " ( " + creditAccountno +" )");
        memberName.setText("" + creditName);
        memAccountNo.setText("Account No.    :" + creditAccountno);
        //     tableNo.setText("Pax Count : " + table);
        if (paxcount != null) {
            tableNo.setText("Pax Count : " + paxcount);
        } else {
            tableNo.setText("Pax Count ");
        }
//        if (isTable.equalsIgnoreCase("true")) {
//            tableSpinner.setVisibility(View.VISIBLE);
//        } else {
//            tableSpinner.setVisibility(View.GONE);
//        }
//        if (isPax.equalsIgnoreCase("true")) {
//            tableNo.setVisibility(View.VISIBLE);
//        } else {
//            tableNo.setVisibility(View.GONE);
//        }
        tableNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertTableNO();
            }
        });
        steward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MakeOrder.this, WaiterList.class);
                startActivityForResult(intent, 2);
            }
        });

        List<String> stringList = new ArrayList<>();
        final ArrayList<Table> getTablenamelidt = db.getTablename();

        stringList.add("Select Table");
        for (int i = 0; i < getTablenamelidt.size(); i++) {
            stringList.add(getTablenamelidt.get(i).getTableNmae());
        }
        if (table != null & !table.equals("") & !table.equals("0")) {
            tableSpinner.setEnabled(false);
            String compareValue = table;
            //  ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.select_state, android.R.layout.simple_spinner_item);
            ArrayAdapter<CharSequence> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, stringList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            tableSpinner.setAdapter(adapter);
            if (compareValue != null) {
                int spinnerPosition = adapter.getPosition(compareValue);
                tableSpinner.setSelection(spinnerPosition);
            }

        } else {
            arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, stringList);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            tableSpinner.setAdapter(arrayAdapter);
        }


        tableSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    table = tableSpinner.getSelectedItem().toString();
                    newTableNo = "0";
                    newTableNo1 = "";
                    if (table.equals("Select Table")) {
                        table = "";
                    } else {
                        for (int j = 0; j < getTablenamelidt.size(); j++) {
                            if (table.equalsIgnoreCase(getTablenamelidt.get(j).getTableNmae())) {
                                newTableNo = getTablenamelidt.get(j).getTableId() + "";
                                newTableNo1 = getTablenamelidt.get(j).getTableNmae() + "";
                            }
                        }
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        if (isSteward.equalsIgnoreCase("true")) {
            List<String> list = db.getWaiterList1();
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    String array1[] = list.get(i).split("_");
                    String waiterName = array1[1].trim();
                    if (waiterName.equalsIgnoreCase(waiterNameFromLogin)) {
                        BillingProfile.waiternamecode = list.get(i);
                        String array[] = list.get(i).split("_");
                        waiterid = db.getWaiterId(array[0].trim());
                        steward.setText("Waiter         :" + list.get(i));
                        String mid = BillingProfile.member;
                        String storeId = sharedpreferences.getString("storeId", "");
                        db.updateWaiterId(mid, storeId, list.get(i));
                        waiternamecode = list.get(i);
                        if (stewardBilling.equalsIgnoreCase("true")) {
                            steward.setOnClickListener(null);
                        }
                    }
                }
            } else {
                Toast.makeText(this, "steward list not available", Toast.LENGTH_SHORT).show();
                finish();
            }
        }

        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String IsOTRequired = sharedpreferences.getString("IsOTRequired", "");
                isCashCardFilled = sharedpreferences.getString("isCashCardFilled", "");
                String ot = sharedpreferences.getString("ot_close", "");
                String ct = sharedpreferences.getString("ct", "");
                String[] ss1 = ot.split("#");

                contractorsId = (ct.equals("DEPENDENT CARD")) ? ss1[2] : "0";
                mguest = misguest_etxt.getText().toString();
                if (isPax.equalsIgnoreCase("true")) {
                    if (paxcount != null) {
                        if (paxcount.isEmpty()) {
                            Toast.makeText(MakeOrder.this, "Please enter Pax Count", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else {
                        Toast.makeText(MakeOrder.this, "Please enter Pax Count", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    if (paxcount != null) {
                        if (paxcount.isEmpty()) {
                            paxcount = "0";
                        }
                    } else {
                        paxcount = "0";
                    }
                }
                if (isTable.equalsIgnoreCase("true")) {
                    if (!newTableNo.isEmpty()) {
                        if (newTableNo.equals("0")) {
                            Toast.makeText(MakeOrder.this, "Please select table", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else {
                        Toast.makeText(MakeOrder.this, "Please select table", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    if (newTableNo.isEmpty()) {
                        newTableNo = "0";
                    }
                }
                if (waiternamecode == null) {
                    //   Toast.makeText(getApplicationContext(), "Select Steward", Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "Select Waiter", Toast.LENGTH_LONG).show();
                } else if (IsOTRequired.equals("True")) {
                    new AsyncPlaceOrder2().execute();
                } else if (!mguest.equals("")) {
                    String mstoreid = sharedpreferences.getString("mstoreId", "");
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("vocationalmember", "Occ.Member:" + mguest);
                    editor.apply();
//                    AlertPayment(ss1[1], "0", ct, isCashCardFilled);
                    mode = "Account";
                    new AsyncPlaceOrder2().execute();
                    //new PlaceOrder_new.AsyncPlaceOrder2().execute();
                } else {
                    String mstoreid = sharedpreferences.getString("mstoreId", "");
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("vocationalmember", "");
                    editor.apply();
                    mode = "Account";
                    new AsyncPlaceOrder2().execute();
//                    AlertPayment(ss1[1], "0", ct, isCashCardFilled);
                }


            }
        });

        fab = (FloatingActionButton)

                findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/

                //  Intent i = new Intent(PlaceOrder_new.this, Itemview.class);
                Intent i = new Intent(MakeOrder.this, Search_Item.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("tableNo", table);
                i.putExtra("paxcnt", paxcount);
                i.putExtra("creditLimit", creditLimit);
                i.putExtra("debitbal", debitbal);
                i.putExtra("creditAno", creditAccountno);
                i.putExtra("creditName", creditName);
                i.putExtra("memberId", memberId);
                i.putExtra("tableId", tableId);
                i.putExtra("cardType", cardType);
                i.putExtra("referenceNo", referenceNo);
                i.putExtra("position", position);
                i.putExtra("waiternamecode", waiternamecode);
                i.putExtra("waiterid", waiterid);
                i.putExtra("accessType", accesstype);
                i.putExtra("billmode", finalBillMode);
                i.putExtra("TempBillAmount", TempBillAmount);
                i.putExtra("dob", "0");
                i.putExtra("doa", "0");
                startActivity(i);
                finish();
            }
        });
        itemView.removeAllViews();

        additems(2);

        String misguest = sharedpreferences.getString("misguest", "");
        if (misguest.equals("False")) {
            // misguest_etxt.setVisibility(View.INVISIBLE);
        }

    }

    //*****
    protected class AsyncPlaceOrder2 extends AsyncTask<String, Void, String> {
        ArrayList<String> credit = null;
        String status = "";
        String otdate = "";
        // String creditAccountno="";
        // String creditName="";
        String storeId = sharedpreferences.getString("storeId", "");
        String loginname = sharedpreferences.getString("loginName", "");
        String userId = sharedpreferences.getString("userId", "");
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        String result = "";
        String narration = "", narationid = "";
        JSONObject jsonObject;
        String mstoreid = sharedpreferences.getString("mstoreId", "");

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(MakeOrder.this);
            try {
                Totalamount = 0;
                TotalAmountWithTax = 0;
                AndroidTempbilling obj = new AndroidTempbilling();
                ArrayList<Object> itemList_main = new ArrayList<>();
                ArrayList<Object> modifierList = new ArrayList<>();
                placeOrder_list = new ArrayList<>();
                AndroidTempbilldetails ob = null;
                ItemList li = null;
                if (itemView != null) {
                    //itemView.bringChildToFront(v);
                    int childRow = itemView.getChildCount();
                    for (int i = 0; i < childRow; i++) {
                        View ItemRow = itemView.getChildAt(i);
                        TextView t = ItemRow.findViewById(R.id.kot_itemCount);
                        TextView t1 = ItemRow.findViewById(R.id.kot_itemname);
                        TextView t2 = ItemRow.findViewById(R.id.kot_rate);
                        TextView t2T = ItemRow.findViewById(R.id.kot_rateT);
                        String Itemcount = t.getText().toString();
                        String isoffer = t1.getTag(R.string.isoffer).toString();
                        String parentItemId = t1.getTag(R.string.parentItemId).toString();
                        String offerQty = t1.getTag(R.string.offerQty).toString();
                        String FreeQty = t1.getTag(R.string.FreeQty).toString();
                        Log.d("parentItemId : ", parentItemId);

                        if (!((Itemcount.equals("")) || (Itemcount.equals("0")))) {
                            String itemname = t1.getText().toString();
                            String[] spl = itemname.split("#");
                            String _itemCode = spl[0];
                            String _itemRate = t2.getText().toString().replace("₹", "");
                            ArrayList<String> dummy = new ArrayList<String>();
                            List<String> itemId = db.getItemid(_itemCode);
                            narration = db.getNarration(_itemCode);
                            narationid = db.getNarrationId(_itemCode);
                            float salesTax = 0;
                            float serviceTax = 0;
                            float cessTax = 0;
                            ob = new AndroidTempbilldetails();
                            li = new ItemList();
                            String _itemName = itemId.get(3);

                            ob.setSalestax(0);  // unwanted
                            ob.setIsFreeitem(isoffer);  // unwanted
                            ob.setParentItemId(parentItemId);  // unwanted
                            ob.setOfferQty(offerQty);  // unwanted
                            ob.setFreeQty(FreeQty);  // unwanted
                            ob.setServicetax(0);
                            ob.setAllergen(db.getAllen(_itemCode));
                            ob.setCesstax(0);
                            ob.setOtno("");
                            ob.setTaxId(0);
                            ob.setTaxRate(0);
                            ob.setAlltax(dummy);

                            ob.setSalesUintid(Integer.parseInt(itemId.get(2)));
                            if (Itemcount.contains(".")) {
                                ob.setQuantity(Double.parseDouble(Itemcount));
                            } else {
                                ob.setQuantity(Double.parseDouble(Itemcount + ".0"));
                            }

                            //   ob.setRate(itemId.get(1));
                            ob.setRate(_itemRate);
                            ob.setItemcode(_itemCode);
                            ob.setItemname(_itemName);
                            ob.setItemId(itemId.get(0));
                            ob.setItemCategoryid(itemId.get(4));
//                            ob.setNarration(narration.replace("]", ""));
//                            ob.setNarattionid(narationid.replace("]", ""));
                            ob.setNarration("");
                            ob.setNarattionid("");


                            double v1 = Double.parseDouble(Itemcount);
                            //   float v2 = Float.parseFloat(itemId.get(1));
                            float v2 = Float.parseFloat(_itemRate);
                            double v3 = v1 * v2;
                            //  ob.setAmount(Float.parseFloat(itemId.get(1)));
                            ob.setAmount((float) v3);

                           /* ArrayList<String> dummy=new ArrayList<String>();
                            if (!cardType.equals("CLUB CARD"))
                            {
                                if(itemId.size()>7)
                                {
                                    int k=5;
                                    int l=2;
                                    for(int j=0;j<(itemId.size()/7);j++)
                                    {
                                        dummy.add(itemId.get(k)+"-"+itemId.get(l));
                                        k=k+7;
                                        l=l+7;
                                    }
                                    ob.setAlltax(dummy);
                                    ob.setTaxId(Integer.parseInt(itemId.get(2)));
                                }
                                else
                                {
                                    dummy.add(itemId.get(5)+"-"+itemId.get(2));
                                    ob.setAlltax(dummy);
                                }
                            }
                            else
                            {
                                dummy.add("0");
                                ob.setAlltax(dummy);
                                ob.setTaxId(Integer.parseInt(itemId.get(2)));
                            }*/
                            Totalamount = Totalamount + (float) v3 + salesTax + serviceTax + cessTax;
                            TotalAmountWithTax = TotalAmountWithTax + Float.parseFloat(t2T.getText().toString().trim()) + salesTax + serviceTax + cessTax;
                            itemList_main.add(ob);
                            obj.setMemberId(memberId);
                            obj.setStoreId(storeId);
                            obj.setTableId(newTableNo + "");
                            Object object = paxcount;
                            if (object instanceof Integer) {
                                Integer integer = (Integer) object;
                                obj.setPaxcount(paxcount + "");

                            } else {
                                paxcount = "0";
                                obj.setPaxcount("0");
                            }
                            obj.setTamount(Totalamount);
                            obj.setNarration(narration);
                            String id = "";
                            //    if(accesstype.equalsIgnoreCase("1") && cardType.equals("DEPENDENT CARD")){
                            if (cardType.equals("DEPENDENT CARD")) {
                                id = referenceNo;
                            } else {
                                id = (cardType.equals("DEPENDENT CARD")) ? memberId : "0";
                            }
                            obj.setDependantid(id);

                            List<SendModifiers> modDetails = new ArrayList<>();
                            modDetails = db.getModDetails(_itemCode);
                            modifierList.addAll(modDetails);
                        }

                    }
                }
                if (itemList_main.size() != 0) {
                    //String  limit = (cLimit.equals(""))?"0":cLimit;
                    String limit = (BillingProfile.c_Limit.equals("")) ? "0" : BillingProfile.c_Limit;

                    Log.d("TAGGGGG", (Double.parseDouble(limit) - Float.parseFloat(C)) + "");
                    Log.d("TAGGGGG", TotalAmountWithTax + "");
                    db.clearBills();
                    //1 if not cash/smart card
                    if (!cardType.equals("CASH CARD") && !cardType.equals("SMART CARD")) {
                        //1 if  PARTY CARD
                        if (cardType.equals("PARTY CARD")) {
                            Log.d("order : ","1");
                            jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo, waiterid, Bill, finalBillMode, otType, mstoreid, mguest, guest_etxt1.getText().toString().trim(), accesstype, modifierList);
                            status = jsonObject.toString();
                            result = jsonObject.optString("Value");

                            JSONArray array = jsonObject.getJSONArray("Value");
                            ItemList it = null;
                            for (int i = 0; i < array.length(); i++) {
                                it = new ItemList();
                                JSONObject object = array.getJSONObject(i);
                                String status = object.getString("status");
                                String Quantity = object.getString("qty");
                                String stock = object.getString("stock");
                                String rate = object.optString("rate");
                                String itemcode = object.optString("itemcode");
                                String itemName = object.optString("itemname");
                                String billno = object.optString("billid");
                                String otno = object.optString("otno");
                                String itemcategoryid = object.optString("itemcategoryid");
                                String narration = object.optString("narration");

                                db.insertBills("", itemcode, Quantity, "", billno, "", "", "", rate, itemName, "", "", "", "", status, otno, itemcategoryid, "", narration, "0", "");
                                it.setItemCode(itemcode);
                                it.setItemName(itemName);
                                it.setStatus(status);
                                it.setStock(stock);
                                it.setQuantity(Quantity);
                                it.setRate(rate);
                                it.setBillno(billno);
                                it.setOtNo(otno);
                                it.setItemcategory(itemcategoryid);
                                it.setNarration(narration);
                                it.setTableno(table);
                                placeOrder_list.add(it);
                            }
                        }
                        //dont knw when this hits
                        else if (C.equals("-0") || BillingProfile.check.equalsIgnoreCase("False") || C.equals("-0.0")) {
                            Log.d("order : ","2");
                            //  JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                            jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo, waiterid, Bill, finalBillMode, otType, mstoreid, mguest, guest_etxt1.getText().toString().trim(), accesstype, modifierList);
                            status = jsonObject.toString();
                            result = jsonObject.optString("Value");

                            JSONArray array = jsonObject.getJSONArray("Value");
                            ItemList it = null;

                            for (int i = 0; i < array.length(); i++) {
                                it = new ItemList();
                                JSONObject object = array.getJSONObject(i);
                                String status = object.getString("status");
                                String Quantity = object.getString("qty");
                                String stock = object.getString("stock");
                                String rate = object.optString("rate");
                                String itemcode = object.optString("itemcode");
                                String itemName = object.optString("itemname");
                                String billno = object.optString("billid");
                                String otno = object.optString("otno");
                                String itemcategoryid = object.optString("itemcategoryid");
                                String narration = object.optString("narration");
                                otdate = object.optString("otdate");
                                db.insertBills("", itemcode, Quantity, "", billno, "", "", "", rate, itemName, "", "", "", "", status, otno, itemcategoryid, "", narration, "0", "");
                                it.setItemCode(itemcode);
                                it.setItemName(itemName);
                                it.setStatus(status);
                                it.setStock(stock);
                                it.setQuantity(Quantity);
                                it.setRate(rate);
                                it.setBillno(billno);
                                it.setOtNo(otno);
                                it.setItemcategory(itemcategoryid);
                                it.setNarration(narration);
                                it.setTableno(table);
                                placeOrder_list.add(it);
                            }
                        } else if (OTCreditLimitNotRequired.equalsIgnoreCase("true")) {
                            Log.d("order : ","3");


                            //   Log.d("TempBillAmount", Double.parseDouble(TempBillAmount)+"");
                            //   Log.d("minus amt", (Double.parseDouble(limit) - Float.parseFloat(C)- Float.parseFloat(TempBillAmount))+"");

                            //  JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                            //   JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                            jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo, waiterid, Bill, finalBillMode, otType, mstoreid, mguest, guest_etxt1.getText().toString().trim(), accesstype, modifierList);
                            status = jsonObject.toString();
                            result = jsonObject.optString("Value");
                            JSONArray array = jsonObject.getJSONArray("Value");
                            ItemList it = null;

                            for (int i = 0; i < array.length(); i++) {
                                it = new ItemList();
                                JSONObject object = array.getJSONObject(i);
                                String status = object.getString("status");
                                String Quantity = object.getString("qty");
                                String stock = object.getString("stock");
                                String rate = object.optString("rate");
                                String itemcode = object.optString("itemcode");
                                String itemName = object.optString("itemname");
                                String billno = object.optString("billid");

                                String otno = object.optString("otno");
                                String itemcategoryid = object.optString("itemcategoryid");
                                String narration = object.optString("narration");
                                otdate = object.optString("otdate");
                                db.insertBills("", itemcode, Quantity, "", billno, "", "", "", rate, itemName, "", "", "", "", status, otno, itemcategoryid, "", narration, "0", "");
                                it.setItemCode(itemcode);
                                it.setItemName(itemName);
                                it.setStatus(status);
                                it.setStock(stock);
                                it.setQuantity(Quantity);
                                it.setRate(rate);
                                it.setBillno(billno);
                                it.setOtNo(otno);
                                it.setItemcategory(itemcategoryid);
                                it.setNarration(narration);
                                it.setTableno(table);
                                placeOrder_list.add(it);
                            }

                        }
                        else if (TotalAmountWithTax <= (Double.parseDouble(limit) - Float.parseFloat(C) - Float.parseFloat(TempBillAmount))) {
                            Log.d("order : ","4");

                            Log.d("TotalAmountWithTax", TotalAmountWithTax + "");
                            Log.d("limit", Double.parseDouble(limit) + "");
                            Log.d("C", Float.parseFloat(C) + "");

                            Log.d("TempBillAmount", Double.parseDouble(TempBillAmount) + "");
                            Log.d("minus amt", (Double.parseDouble(limit) - Float.parseFloat(C) - Float.parseFloat(TempBillAmount)) + "");

                            //  JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                            //   JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                            jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo, waiterid, Bill, finalBillMode, otType, mstoreid, mguest, guest_etxt1.getText().toString().trim(), accesstype, modifierList);
                            status = jsonObject.toString();
                            result = jsonObject.optString("Value");
                            JSONArray array = jsonObject.getJSONArray("Value");
                            ItemList it = null;

                            for (int i = 0; i < array.length(); i++) {
                                it = new ItemList();
                                JSONObject object = array.getJSONObject(i);
                                String status = object.getString("status");
                                String Quantity = object.getString("qty");
                                String stock = object.getString("stock");
                                String rate = object.optString("rate");
                                String itemcode = object.optString("itemcode");
                                String itemName = object.optString("itemname");
                                String billno = object.optString("billid");

                                String otno = object.optString("otno");
                                String itemcategoryid = object.optString("itemcategoryid");
                                String narration = object.optString("narration");
                                otdate = object.optString("otdate");
                                db.insertBills("", itemcode, Quantity, "", billno, "", "", "", rate, itemName, "", "", "", "", status, otno, itemcategoryid, "", narration, "0", "");
                                it.setItemCode(itemcode);
                                it.setItemName(itemName);
                                it.setStatus(status);
                                it.setStock(stock);
                                it.setQuantity(Quantity);
                                it.setRate(rate);
                                it.setBillno(billno);
                                it.setOtNo(otno);
                                it.setItemcategory(itemcategoryid);
                                it.setNarration(narration);
                                it.setTableno(table);
                                placeOrder_list.add(it);
                            }

                        } else {
                            status = "Credit exceeds";
                        }

                    } else {

                        if (cardType.equals("SMART CARD")) {


                            if (OTCreditLimitNotRequired.equalsIgnoreCase("true")) {
                                Log.d("order : ","5");
                                // JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                                // JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo, waiterid, Bill, finalBillMode, otType, mstoreid, mguest, guest_etxt1.getText().toString().trim(), accesstype, modifierList);
                                status = jsonObject.toString();
                                result = jsonObject.optString("Value");
                                JSONArray array = jsonObject.getJSONArray("Value");
                                ItemList it = null;

                                for (int i = 0; i < array.length(); i++) {
                                    it = new ItemList();
                                    JSONObject object = array.getJSONObject(i);

                                    String status = object.getString("status");

                                    String Quantity = object.getString("qty");
                                    String stock = object.getString("stock");
                                    String rate = object.optString("rate");
                                    String itemcode = object.optString("itemcode");
                                    String itemName = object.optString("itemname");
                                    String billno = object.optString("billid");
                                    String otno = object.optString("otno");
                                    String itemcategoryid = object.optString("itemcategoryid");
                                    String narration = object.optString("narration");
                                    otdate = object.optString("otdate");
                                    db.insertBills("", itemcode, Quantity, "", billno, "", "", "", rate, itemName, "", "", "", "", status, otno, itemcategoryid, "", narration, "0", "");
                                    it.setItemCode(itemcode);
                                    it.setItemName(itemName);
                                    it.setStatus(status);
                                    it.setStock(stock);
                                    it.setQuantity(Quantity);
                                    it.setRate(rate);
                                    it.setBillno(billno);
                                    it.setOtNo(otno);
                                    it.setItemcategory(itemcategoryid);
                                    it.setNarration(narration);
                                    it.setTableno(table);
                                    placeOrder_list.add(it);
                                }

                            } else if (Float.parseFloat(C) - Float.parseFloat(TempBillAmount) - TotalAmountWithTax > 0) {
                                Log.d("order : ","6");
                                // JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                                // JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo, waiterid, Bill, finalBillMode, otType, mstoreid, mguest, guest_etxt1.getText().toString().trim(), accesstype, modifierList);
                                status = jsonObject.toString();
                                result = jsonObject.optString("Value");
                                JSONArray array = jsonObject.getJSONArray("Value");
                                ItemList it = null;

                                for (int i = 0; i < array.length(); i++) {
                                    it = new ItemList();
                                    JSONObject object = array.getJSONObject(i);

                                    String status = object.getString("status");

                                    String Quantity = object.getString("qty");
                                    String stock = object.getString("stock");
                                    String rate = object.optString("rate");
                                    String itemcode = object.optString("itemcode");
                                    String itemName = object.optString("itemname");
                                    String billno = object.optString("billid");
                                    String otno = object.optString("otno");
                                    String itemcategoryid = object.optString("itemcategoryid");
                                    String narration = object.optString("narration");
                                    otdate = object.optString("otdate");
                                    db.insertBills("", itemcode, Quantity, "", billno, "", "", "", rate, itemName, "", "", "", "", status, otno, itemcategoryid, "", narration, "0", "");
                                    it.setItemCode(itemcode);
                                    it.setItemName(itemName);
                                    it.setStatus(status);
                                    it.setStock(stock);
                                    it.setQuantity(Quantity);
                                    it.setRate(rate);
                                    it.setBillno(billno);
                                    it.setOtNo(otno);
                                    it.setItemcategory(itemcategoryid);
                                    it.setNarration(narration);
                                    it.setTableno(table);
                                    placeOrder_list.add(it);
                                }

                            } else {
                                status = "Credit exceeds";
                            }

                        } else {
                            // TODO need to validate for thr fillied cards ------

                            if (isCashCardFilled.equalsIgnoreCase("true")) {
                                Log.d("order : ","7");

                                if (Float.parseFloat(C) - Float.parseFloat(TempBillAmount) - TotalAmountWithTax > 0) {
                                    // JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                                    // JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                    jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo, waiterid, Bill, finalBillMode, otType, mstoreid, mguest, guest_etxt1.getText().toString().trim(), accesstype, modifierList);
                                    status = jsonObject.toString();
                                    result = jsonObject.optString("Value");
                                    JSONArray array = jsonObject.getJSONArray("Value");
                                    ItemList it = null;

                                    for (int i = 0; i < array.length(); i++) {
                                        it = new ItemList();
                                        JSONObject object = array.getJSONObject(i);

                                        String status = object.getString("status");
                                        String Quantity = object.getString("qty");
                                        String stock = object.getString("stock");
                                        String rate = object.optString("rate");
                                        String itemcode = object.optString("itemcode");
                                        String itemName = object.optString("itemname");
                                        String billno = object.optString("billid");
                                        String otno = object.optString("otno");
                                        String itemcategoryid = object.optString("itemcategoryid");
                                        String narration = object.optString("narration");
                                        otdate = object.optString("otdate");
                                        db.insertBills("", itemcode, Quantity, "", billno, "", "", "", rate, itemName, "", "", "", "", status, otno, itemcategoryid, "", narration, "0", "");
                                        it.setItemCode(itemcode);
                                        it.setItemName(itemName);
                                        it.setStatus(status);
                                        it.setStock(stock);
                                        it.setQuantity(Quantity);
                                        it.setRate(rate);
                                        it.setBillno(billno);

                                        it.setOtNo(otno);
                                        it.setItemcategory(itemcategoryid);
                                        it.setNarration(narration);
                                        it.setTableno(table);
                                        placeOrder_list.add(it);
                                    }
                                } else {
                                    status = "Credit exceeds";
                                }
                            } else if (isCashCardFilled.equalsIgnoreCase("false")) {
                                Log.d("order : ","8");

                                //   JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo, waiterid, Bill, finalBillMode, otType, mstoreid, mguest, guest_etxt1.getText().toString().trim(), accesstype, modifierList);
                                status = jsonObject.toString();
                                result = jsonObject.optString("Value");
                                JSONArray array = jsonObject.getJSONArray("Value");
                                ItemList it = null;

                                for (int i = 0; i < array.length(); i++) {
                                    it = new ItemList();
                                    JSONObject object = array.getJSONObject(i);

                                    String status = object.getString("status");

                                    String Quantity = object.getString("qty");
                                    String stock = object.getString("stock");
                                    String rate = object.optString("rate");
                                    String itemcode = object.optString("itemcode");
                                    String itemName = object.optString("itemname");

                                    String billno = object.optString("billid");
                                    String otno = object.optString("otno");
                                    String itemcategoryid = object.optString("itemcategoryid");
                                    String narration = object.optString("narration");
                                    otdate = object.optString("otdate");
                                    db.insertBills("", itemcode, Quantity, "", billno, "", "", "", rate, itemName, "", "", "", "", status, otno, itemcategoryid, "", narration, "0", "");
                                    it.setItemCode(itemcode);
                                    it.setItemName(itemName);
                                    it.setStatus(status);
                                    it.setStock(stock);
                                    it.setQuantity(Quantity);
                                    it.setRate(rate);
                                    it.setBillno(billno);

                                    it.setOtNo(otno);
                                    it.setItemcategory(itemcategoryid);
                                    it.setNarration(narration);
                                    it.setTableno(table);
                                    placeOrder_list.add(it);
                                }

                            } else {

                                status = "Check Card";
                            }

                        }


                    }

                } else {
                    status = "No item";
                }

            } catch (Exception e) {
                e.printStackTrace();
                status = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MakeOrder.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            Date dateIn = null;
            try {
                Toast.makeText(MakeOrder.this, "Total Amount: " + TotalAmountWithTax, Toast.LENGTH_SHORT).show();
                if (otdate.contains("/")) {
                    dateIn = new SimpleDateFormat("MM/dd/yyyy").parse(otdate);
                } else {
                    dateIn = new SimpleDateFormat("dd-MM-yyyy").parse(otdate);
                }
                String formatteddateIn = new SimpleDateFormat("yyyy-MM-dd").format(dateIn);
                otdate = formatteddateIn;
            } catch (Exception e) {
                e.printStackTrace();
            }
            String IsOTRequired = sharedpreferences.getString("IsOTRequired", "");
            if (IsOTRequired.equalsIgnoreCase("True")) {
                if (s.equals("Credit exceeds")) {
                    if (cardType.equals("CASH CARD") && isCashCardFilled.equalsIgnoreCase("True")) {
                        showPopUp();
                    } else if (cardType.equals("SMART CARD")) {
                        showPopUp();
                    } else {
                        Toast.makeText(getApplicationContext(), "Credit limit exceeds", Toast.LENGTH_LONG).show();
                        showPopUp();
                    }

                } else if (s.contains("true")) {
//                    Toast.makeText(getApplicationContext(), "Order placed", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(MakeOrder.this, PlaceOrder_bill.class);
                    i.putExtra("mName", creditName);
                    i.putExtra("a/c", creditAccountno);
                    i.putExtra("tableNo", newTableNo1);
                    i.putExtra("tableId", tableId);
                    i.putExtra("paxcnt", paxcount);

                    i.putExtra("bill_id", "null");
                    i.putExtra("Type", "b");
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    startActivity(i);
                    //    int cc = db.updateTableNo(creditAccountno, newTableNo, storeId, userId, otdate, paxcount);
                    int cc = db.updateTableNo(creditAccountno, newTableNo1, storeId, userId, otdate, paxcount, waiternamecode);
                    finish();
                } else if (s.contains("Unable to connect to the remote server")) {
                    Toast.makeText(getApplicationContext(), "Order placed -- > POS system is off", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(MakeOrder.this, PlaceOrder_bill.class);
                    i.putExtra("mName", creditName);
                    i.putExtra("a/c", creditAccountno);
                    i.putExtra("tableNo", newTableNo1);
                    i.putExtra("tableId", tableId);
                    i.putExtra("paxcnt", paxcount);
                    i.putExtra("bill_id", "null");
                    i.putExtra("Type", "b");
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    startActivity(i);

                    //int cc = db.updateTableNo(creditAccountno, newTableNo, storeId, userId, otdate, paxcount);
                    int cc = db.updateTableNo(creditAccountno, newTableNo1, storeId, userId, otdate, paxcount, waiternamecode);
                    finish();
                } else if (s.contains("No item")) {
                    Toast.makeText(getApplicationContext(), "No item is ordered-->" + s, Toast.LENGTH_LONG).show();
                } else {
                    //  Toast.makeText(getApplicationContext(), "Order not placed-->" + s + "  " + jsonObject, Toast.LENGTH_LONG).show();

                    if (jsonObject != null && jsonObject.has("NetworkErr")) {

                        AlertError("", "", "");
                        Toast.makeText(getApplicationContext(), "Order not placed-->" + s + "  " + jsonObject, Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getApplicationContext(), "Order not placed-->" + s + "  " + jsonObject, Toast.LENGTH_LONG).show();
                    }

                }
            } else if (IsOTRequired.equalsIgnoreCase("False")) {
                if (s.equals("Credit exceeds")) {
                    if (cardType.equals("CASH CARD") && isCashCardFilled.equalsIgnoreCase("True")) {
                        showPopUp();

                    } else if (cardType.equals("SMART CARD")) {
                        showPopUp();
                    } else {
                        Toast.makeText(getApplicationContext(), "Credit limit exceeds", Toast.LENGTH_LONG).show();
                    }

                } else if (s.contains("true")) {
                    String close = sharedpreferences.getString("ot_close", "");
                    String[] ss = close.split("#");
                    new AsyncCloseOrder().execute(ss[1], "0", mode);
                    Toast.makeText(getApplicationContext(), "Order placed", Toast.LENGTH_SHORT).show();
                    //Intent i = new Intent(PlaceOrder_new.this, PlaceOrder_bill.class);
                    Intent i = new Intent(MakeOrder.this, Accountlist.class);
                    i.putExtra("mName", creditName);
                    i.putExtra("a/c", creditAccountno);
                    i.putExtra("tableNo", newTableNo);
                    i.putExtra("tableId", tableId);
                    i.putExtra("paxcnt", paxcount);

                    i.putExtra("bill_id", "null");
                    i.putExtra("Type", "b");
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    //startActivity(i);
                    //   int cc = db.updateTableNo(creditAccountno, newTableNo, storeId, userId, otdate, paxcount);
                    int cc = db.updateTableNo(creditAccountno, newTableNo1, storeId, userId, otdate, paxcount, waiternamecode);

                    //finish();
                } else if (s.contains("Unable to connect to the remote server")) {
                    Toast.makeText(getApplicationContext(), "Order placed -- > POS system is off", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(MakeOrder.this, PlaceOrder_bill.class);
                    i.putExtra("mName", creditName);
                    i.putExtra("a/c", creditAccountno);
                    i.putExtra("tableNo", newTableNo);
                    i.putExtra("tableId", tableId);
                    i.putExtra("paxcnt", paxcount);

                    i.putExtra("bill_id", "null");
                    i.putExtra("Type", "b");
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    startActivity(i);
                    //   int cc = db.updateTableNo(creditAccountno, newTableNo, storeId, userId, otdate, paxcount);
                    int cc = db.updateTableNo(creditAccountno, newTableNo1, storeId, userId, otdate, paxcount, waiternamecode);
                    finish();
                } else if (s.contains("No item")) {
                    Toast.makeText(getApplicationContext(), "No item is ordered-->" + s, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Order not placed-->" + s + "  " + jsonObject, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void showPopUp() {

       /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();*/
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MakeOrder.this);
        alertDialog.setMessage("Credit Limit Exceeds" + "\n Total amount :" + TotalAmountWithTax + "\n Previous OT amount :" + TempBillAmount + "\n Credit Limit :" + C);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                return;
            }
        });
        alertDialog.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        ArrayList<String> list = null;
        list = db.getOTItemList();
        if (!list.isEmpty()) {
            try {
                AlertDialog.Builder alertBulider = new AlertDialog.Builder(this);
                alertBulider.setMessage("Do you Want to Cancel the Order ?");
                alertBulider.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (Itemview.itemViewInstance != null) {
                            try {
                                Itemview.itemViewInstance.finish();
                            } catch (Exception e) {
                            }
                        }

                        Intent i = new Intent(MakeOrder.this, Accountlist.class);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        startActivity(i);
                        finish();
                    }
                });
                alertBulider.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog = alertBulider.create();
                //  alertBulider.create();
                dialog.show();
            } catch (Exception e) {
                String r = e.getMessage();
            }
        } else {
            Intent i = new Intent(MakeOrder.this, Accountlist.class);
            i.putExtra("tableNo", table);
            i.putExtra("tableId", tableId);
            i.putExtra("waiterid", waiterid);
            i.putExtra("paxcnt", paxcount);
            i.putExtra("position", position);
            i.putExtra("waiternamecode", waiternamecode);
            startActivity(i);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                ArrayList<String> list = null;
                list = db.getOTItemList();
                if (!list.isEmpty()) {
                    try {
                        AlertDialog.Builder alertBulider = new AlertDialog.Builder(this);
                        alertBulider.setMessage("Do you Want to Cancel the Order ?");
                        alertBulider.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (Itemview.itemViewInstance != null) {
                                    try {
                                        Itemview.itemViewInstance.finish();
                                    } catch (Exception e) {
                                    }
                                }

                                Intent i = new Intent(MakeOrder.this, Accountlist.class);
                                i.putExtra("waiterid", waiterid);
                                i.putExtra("position", position);
                                startActivity(i);
                                finish();
                            }
                        });
                        alertBulider.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog dialog = alertBulider.create();
                        //  alertBulider.create();
                        dialog.show();
                    } catch (Exception e) {
                        String r = e.getMessage();
                    }
                } else {
                    Intent i = new Intent(MakeOrder.this, Accountlist.class);
                    i.putExtra("tableNo", table);
                    i.putExtra("tableId", tableId);
                    i.putExtra("paxcnt", paxcount);

                    i.putExtra("waiterid", waiterid);
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    startActivity(i);
                    finish();
                    // finish();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void alertTableNO() {

        AlertDialog.Builder alertBulider = new AlertDialog.Builder(this);
        alertBulider.setMessage("Pax Count ");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_table_no, null);
        alertBulider.setView(dialogView);

        editText = dialogView.findViewById(R.id.tableno);

        alertBulider.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // finish();
                tableNo.setText("Pax Count " + editText.getText().toString());
                //  newTableNo = editText.getText().toString();
                paxcount = editText.getText().toString();
            }
        });
        alertBulider.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = alertBulider.create();
        dialog.show();

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("" + pos + "  - " + bill);
            ab.setElevation(0);
            ab.setDisplayHomeAsUpEnabled(true);

        }
    }


    public void addAllen(final String code) {
        try {
            final Dialog dialog = new Dialog(MakeOrder.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_allen);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
            Button button = dialog.findViewById(R.id.buttonOK);
            final EditText qtyEditText = dialog.findViewById(R.id.edit_textQty);

            button.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View v) {
                    if (qtyEditText.getText().toString().trim().isEmpty()) {
                        Toast.makeText(MakeOrder.this, "Please enter Allergen", Toast.LENGTH_SHORT).show();
                    } else {
                        db.deleteAllenId(code);
                        db.insertAllen(code, qtyEditText.getText().toString().trim());
                        Toast.makeText(MakeOrder.this, "Allergen added", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                }
            });
            dialog.show();

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("SetTextI18n")
    public void additems(int value) {
        ArrayList<String> list = null;
        noOfCounts = 0;
        String isoffer = "0";
        if (value == 1) {
            list = db.getItemList(groupId);
        } else {
            // itemView.removeAllViews();
            itemView = null;
            view = null;
            itemName = null;
            itemCount = null;
            add = null;
            subtract = null;
            rate = null;
            // happyHour=null;
            list = db.getOTItemListNew();
            // TODO reo=move the comments
            if (!list.isEmpty()) {
                placeOrder.setVisibility(View.VISIBLE);
            } else {
                placeOrder.setVisibility(View.GONE);
            }
        }
        String isOfferItem = "";
        for (int i = 0; i < list.size(); i++) {
            noOfCounts = 0;
            if (itemView == null) {
                itemView = (LinearLayout) findViewById(R.id.itemView);
                view = getLayoutInflater().inflate(R.layout.itemlist_kot, itemView, false);
                itemName = view.findViewById(R.id.kot_itemname);
                itemCount = view.findViewById(R.id.kot_itemCount);
                // happyHour=(CheckBox)view.findViewById(R.id.happyhour_checbox);
                // happyHour.setVisibility(View.GONE);
                add = view.findViewById(R.id.kot_add);
                subtract = view.findViewById(R.id.kot_minus);
                rate = view.findViewById(R.id.kot_rate);
                rateT = view.findViewById(R.id.kot_rateT);
                tax = view.findViewById(R.id.kot_tax);
                info = view.findViewById(R.id.info);
            } else {
                view = getLayoutInflater().inflate(R.layout.itemlist_kot, itemView, false);
                itemName = view.findViewById(R.id.kot_itemname);
                itemCount = view.findViewById(R.id.kot_itemCount);
                //happyHour=(CheckBox)view.findViewById(R.id.happyhour_checbox);
                //happyHour.setVisibility(View.GONE);
                add = view.findViewById(R.id.kot_add);
                subtract = view.findViewById(R.id.kot_minus);
                itemView = (LinearLayout) findViewById(R.id.itemView);
                rate = view.findViewById(R.id.kot_rate);
                rateT = view.findViewById(R.id.kot_rateT);
                tax = view.findViewById(R.id.kot_tax);
                info = view.findViewById(R.id.info);
            }
            String ll[] = list.get(i).split("\\$");
            if (ll.length > 1) {
           /*     if(ll[2].equalsIgnoreCase("Modifier")){
                    itemName.setText(ll[0] );
                    Log.d("Mod 1 :",ll[0]);
                    add.setVisibility(View.INVISIBLE);
                    subtract.setVisibility(View.INVISIBLE);
                }
                else {*/
                itemName.setText(ll[0].replace("F*", ""));
                itemCount.setText(ll[1]);
                noOfCounts = noOfCounts + Double.parseDouble(ll[1]);
                rate.setText(ll[2] + "");
                isOfferItem = ll[4];

                //  add.setTag(0,isOfferItem);
                //   add.setTag(0,rate);
                //added by monica
                add.setTag(R.string.firstvalue, isOfferItem);
                //added by monica
                //  add.setTag(R.string.secondvalue,rate);
                add.setTag(R.string.secondvalue, ll[2]);
                subtract.setTag(R.string.firstvalue, isOfferItem);
                //added by monica
                //  subtract.setTag(R.string.secondvalue,rate);
                subtract.setTag(R.string.secondvalue, ll[2]);
                List<String> itemId = db.getItemid(list.get(i).split("#")[0]);
                if (itemId.size() == 0) {
                    tax.setText("0");
                    rateT.setText("0");
                } else {
                    Float tax1 = db.getItemTax(itemId.get(0), rate.getText().toString().trim(), itemCount.getText().toString().trim(), "0", false);
                    Float subTax = db.getItemSubTax(itemId.get(0), rate.getText().toString().trim(), itemCount.getText().toString().trim(), "0", false);
                    Float finalTax = tax1 + subTax;
                    tax.setText(new DecimalFormat("##.##").format(finalTax) + "");
                    Double amount = Double.parseDouble(ll[2]) * Double.parseDouble(itemCount.getText().toString().trim()) + finalTax;
                    rateT.setText(new DecimalFormat("##.##").format(amount) + "");
                }
                //        }


            } else {
                itemName.setText(ll[0]);


            }

            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewGroup parent = (ViewGroup) v.getParent();
                    TextView t1 = parent.findViewById(R.id.kot_itemCount);
                    TextView t2 = parent.findViewById(R.id.kot_itemname);
                    final String _itemname = t2.getText().toString();
                    if (!_itemname.equals("") || _itemname != null) {
                        String[] ss = _itemname.split("#");
                        final String ii = ss[0];
                        String _itemcode = t1.getText().toString();

                        AlertDialog.Builder builder = new AlertDialog.Builder(MakeOrder.this);
                        builder.setTitle("Select");
                        String[] choice = {"Add Modifiers", "Add Allergen"};
                        // add a list
                        builder.setItems(choice, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        Intent i = new Intent(MakeOrder.this, Alert_ItemDescription.class);
                                        i.putExtra("itemName", _itemname);
                                        i.putExtra("itemCode", ii);
                                        //        startActivity(i);
                                        startActivityForResult(i, 3);
                                        break;
                                    case 1:
                                        addAllen(ii);
                                        break;

                                }
                            }
                        });

                        // create and show the alert dialog
                        AlertDialog dialog = builder.create();
                        dialog.show();

                    } else {
                        Toast.makeText(MakeOrder.this, "No Item", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            if (!isOfferItem.equals("CO")) {
                add.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {

                                               ViewGroup parent = (ViewGroup) v.getParent();
                                               TextView t1 = parent.findViewById(R.id.kot_itemCount);
                                               TextView t2 = parent.findViewById(R.id.kot_itemname);
                                               String _itemname = t2.getText().toString();
                                               String _itemcode = t1.getText().toString();
                                               String count = t1.getText().toString();
                                               //   String itemTag=v.getTag(0).toString();
                                               // String ratetag=v.getTag(0).toString();
                                               //added by monica
                                               String itemTag = v.getTag(R.string.firstvalue).toString();
                                               String ratetag = v.getTag(R.string.secondvalue).toString();
                                               if (!(count.contains(".") || count.equals(""))) {
                                                   int c = Integer.parseInt(count);
                                                   t1.setText(c + 1 + "");
                                                   int co = c + 1;
                                                   noOfCounts = noOfCounts + 1;
                                                   if (_itemname.contains("#") && itemTag.equals("NO")) {
                                                       String array[] = _itemname.split("#");
                                                       Long rvalur = db.insertOTtrail(array[0], array[1], Integer.toString(co), ratetag);
                                                       if (rvalur < 0) {
                                                           Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                       }
                                                       itemView.removeAllViews();
                                                       additems(2);
                                                   } else if (_itemname.contains("#") && itemTag.equals("PO")) {
                                                       //CHECK OFFER
                                                       String rate = v.getTag(R.string.secondvalue).toString();
                                                       String array[] = _itemname.split("#");
                                                       //      List<String> itemId = db.getItemid(array[0]);

                                                       Long rvalur = db.insertOTTest(array[0], array[1], Integer.toString(co), ratetag);
                                                       if (rvalur < 0) {
                                                           Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                       }
                                                       itemView.removeAllViews();
                                                       additems(2);
                                                   }
                                               } else if (count.contains(".")) {
                                                   double c = Double.parseDouble(count);

                                                   t1.setText(format.format(c + 0.5) + "");
                                                   double co = c + 1;
                                                   noOfCounts = noOfCounts + 0.5;
                                                   if (_itemname.contains("#")) {
                                                       String array[] = _itemname.split("#");
                                                       Long rvalur = db.insertOTtrail(array[0], array[1], Double.toString(co), ratetag);
                                                       if (rvalur < 0) {
                                                           Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                       } else {

                                                       }
                                                   }
                                               } else if (count.equals("")) {
                                                   t1.setText("1");
                                                   noOfCounts = noOfCounts + 1;
                                                   // txt_itemcount.setText(noOfCounts + "");
                                                   if (_itemname.contains("#")) {
                                                       String array[] = _itemname.split("#");
                                                       Long rvalur = db.insertOTtrail(array[0], array[1], "1", ratetag);
                                                       if (rvalur < 0) {
                                                           Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                       } else {

                                                       }

                                                   }
                                               }
                                           }
                                       }

                );
                subtract.setOnClickListener(new
                                                    View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            try {
                                                                ViewGroup parent = (ViewGroup) v.getParent();
                                                                TextView t1 = parent.findViewById(R.id.kot_itemCount);
                                                                TextView t2 = parent.findViewById(R.id.kot_itemname);

                                                                String _itemname = t2.getText().toString();
                                                                String _itemcode = t1.getText().toString();
                                                                String count = t1.getText().toString();
                                                                //added by monica
                                                                String itemTag = v.getTag(R.string.firstvalue).toString();
                                                                String ratetag = v.getTag(R.string.secondvalue).toString();

                                                                if (!(count.contains(".") || count.equals(""))) {
                                                                    int c = Integer.parseInt(count);
                                                                    if (c != 0) {
                                                                        t1.setText(format.format(c - 1) + "");
                                                                        noOfCounts = noOfCounts - 1;
                                                                        int co = c - 1;


                                                                        if (_itemname.contains("#") && itemTag.equals("NO")) {
                                                                            String array[] = _itemname.split("#");
                                                                            Long rvalur = db.insertOTtrail(array[0], array[1], Integer.toString(co), ratetag);
                                                                            if (rvalur < 0) {
                                                                                Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                                            }
                                                                            itemView.removeAllViews();
                                                                            additems(2);

                                                                        }

                                                              /*  if (_itemname.contains("-")&& itemTag.equals("NO"))
                                                                {
                                                                    String array[] = _itemname.split("-");
                                                                    Long rvalur = db.insertOTtrail(array[0], array[1], Integer.toString(co),ratetag);
                                                                    if (rvalur < 0)
                                                                    {
                                                                        Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                                    }

                                                                }*/
                                                                        else if (_itemname.contains("#") && itemTag.equals("PO")) {
                                                                            //CHECK OFFER substraction
                                                                            String rate = v.getTag(R.string.secondvalue).toString();
                                                                            String array[] = _itemname.split("#");
                                                                            //      List<String> itemId = db.getItemid(array[0]);

                                                                            Long rvalur = db.insertOTTest(array[0], array[1], Integer.toString(co), ratetag);
                                                                            if (rvalur < 0) {
                                                                                Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                                            }
                                                                            itemView.removeAllViews();
                                                                            additems(2);
                                                                        }


                                                                        // txt_itemcount.setText(noOfCounts + "");
                                                                    }
                                                                } else if (count.contains(".")) {
                                                                    double c = Double.parseDouble(count);
                                                                    if (c != 0) {
                                                                        if (c - 0.5 > 0.1) {
                                                                            //monica
                                                                            t1.setText(format.format(c - 0.5) + "");
                                                                            noOfCounts = noOfCounts - 0.5;
                                                                            double co = c - 0.5;
                                                                            if (_itemname.contains("#")) {
                                                                                String array[] = _itemname.split("#");
                                                                                Long rvalur = db.insertOTtrail(array[0], array[1], Double.toString(co), ratetag);
                                                                                if (rvalur < 0) {
                                                                                    Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                                                } else {

                                                                                }

                                                                            }

                                                                        }

                                                                        // txt_itemcount.setText(noOfCounts + "");
                                                                    }
                                                                } else if (count.equals("")) {
                                                                    Toast.makeText(getApplicationContext(), "Add item first", Toast.LENGTH_LONG).show();
                                                                }


                                                            } catch (Exception e) {
                                                                String hh = e.getMessage();
                                                            }

                                                        }
                                                    }

                );
                isoffer = "0";
                itemName.setTag(R.string.isoffer, isoffer + "");
                itemName.setTag(R.string.parentItemId,  "");
                itemName.setTag(R.string.offerQty,  "");
                itemName.setTag(R.string.FreeQty,  "");

                itemView.addView(view);

            } else {
                isoffer = "1";
                String[] l2 = list.get(i).split("\\$");
                String parentitemid1 = l2[5] != null ? l2[5] : "";
                String offerQty = l2[6] != null ? l2[6] : "";
                String FreeQty = l2[7] != null ? l2[7] : "";
                itemName.setTag(R.string.isoffer, isoffer + "");
                itemName.setTag(R.string.parentItemId, parentitemid1 + "");
                itemName.setTag(R.string.offerQty,  offerQty+"");
                itemName.setTag(R.string.FreeQty,  FreeQty+"");

                Log.d("parentitemid1: ", parentitemid1);
                add.setImageResource(R.drawable.star);
                subtract.setVisibility(View.INVISIBLE);
                itemView.addView(view);
            }
        }


    }

    private void AlertPayment(final String s, String s1, String cardType, String isCashCardFilled) {
        paymentMode = new ArrayList<>();
        if (bill.equals("Regular")) {
            switch (cardType) {

                case "MEMBER CARD":
                    paymentMode.add("Account");
                    paymentMode.add("Cash");
                    paymentMode.add("Credit");
                    break;

                case "CASH CARD":
                    if (MainActivity.cashCard.equals("true")) {
                        paymentMode.add("Account");
                    } else {
                        paymentMode.add("Cash");
                        paymentMode.add("Credit");
                    }
                    break;

                case "CLUB CARD":
                    paymentMode.add("Account");
                    break;


                case "ROOM CARD":
                    paymentMode.add("Account");
                    //   paymentMode.add("Complimentary");
                    break;

                case "DEPENDENT CARD":
                    paymentMode.add("Account");
                    paymentMode.add("Cash");
                    paymentMode.add("Credit");
                    break;
                case "MEMBER DUPL CARD":
                    paymentMode.add("Account");
                    paymentMode.add("Cash");
                    paymentMode.add("Credit");
                    break;

                case "SMART CARD":
                    paymentMode.add("Account");
                    break;
            }


            if (!paymentMode.isEmpty()) {
                payments = new String[paymentMode.size()];
                payments = paymentMode.toArray(payments);
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Select the Payment Mode");
            builder.setItems(payments, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int _paymentMode) {
                    // mode=payments[paymentMode];
                    mode = paymentMode.get(_paymentMode);
                    //new AsyncCloseOrder().execute(s, "0",mode);
                    new AsyncPlaceOrder2().execute();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

//        else if(bill.equals("Party Billing"))
//        {
//            if(cardType.equals("PARTY CARD"))
//            {
//                new AsyncCloseOrder().execute(s,"0","Account");
//            }
//            else
//            {
//                Toast.makeText(PlaceOrder_new.this, "This card is Not applicable for Party Billing ", Toast.LENGTH_SHORT).show();
//            }
//        }
//        else if(bill.equals("Direct Party Billing"))
//        {
//            if(cardType.equals("DIRECTPARTY CARD"))
//            {
//                new AsyncCloseOrder().execute(s,"0","Account");
//            }
//            else
//            {
//                Toast.makeText(PlaceOrder_new.this, "This card is Not applicable for Direct Party Billing ", Toast.LENGTH_SHORT).show();
//            }
//        }
//        else if(bill.equals("Compliment"))
//        {
//            if(cardType.equals("MEMBER CARD") ||cardType.equals("ROOM CARD"))
//            {
//                new AsyncCloseOrder().execute(s, "0","Complimentary");
//            }
//            else
//            {
//                Toast.makeText(PlaceOrder_new.this, "This card is Not applicable for Compliment Billing ", Toast.LENGTH_SHORT).show();
//            }
//
//        }
//        else if(bill.equals("coupon"))
//        {
//            if(cardType.equals("MEMBER CARD"))
//            {
//                new AsyncCloseOrder().execute(s, "0","Coupon");
//
//            }
//            else
//            {
//                Toast.makeText(this, "This card is not applicable for coupon ", Toast.LENGTH_SHORT).show();
//            }
//
//
//        }
        else {
            Toast.makeText(this, "This card is not applicable ", Toast.LENGTH_SHORT).show();
        }
    }

    protected class AsyncCloseOrder extends AsyncTask<String, Void, String> {
        String status = "";
        String storeId = sharedpreferences.getString("storeId", "");
        String deviceid = sharedpreferences.getString("deviceId", "");
        String userId = sharedpreferences.getString("userId", "");
        String pageType = "";
        String mstoreid = sharedpreferences.getString("mstoreId", "");
        String billtype = sharedpreferences.getString("BillMode", "");
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(MakeOrder.this);
            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                closeorderItemsArrayList.clear();
                if (bill.equals("coupon")) {
                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, "COUPON", pos, params[2], contractorsId, mstoreid, billtype, 0, "P",waiterid);
                } else if (bill.equals("Party Billing")) {
                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, "PARTY CARD", pos, params[2], contractorsId, mstoreid, billtype, 0, "P",waiterid);
                } else if (bill.equals("Direct Party Billing")) {
                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, "DIRECTPARTY CARD", pos, params[2], contractorsId, mstoreid, billtype, 0, "P",waiterid);
                } else {
                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype, 0, "P",waiterid);
                }

                JSONArray array = jsonObject.getJSONArray("Value");
                CloseorderItems cl = null;
                pageType = params[1];
                for (int i = 0; i < array.length(); i++) {
                    cl = new CloseorderItems();
                    JSONObject object = array.getJSONObject(i);
                    String ItemID = object.getString("ItemID");
                    String ItemCode = object.getString("ItemCode");
                    String Quantity = object.getString("Quantity");
                    String Amount = object.getString("Amount");
                    String BillID = object.getString("BillID");
                    String taxDescription = object.optString("taxDescription");
                    String mAcc = object.getString("memberAcc");
                    String taxValue = object.getString("taxValue");
                    String ava_balance = object.optString("avBalance");
                    String billNo = object.optString("billnumber");
                    String taxAmount = object.optString("taxAmount");
                    String Rate = object.getString("rate");
                    String itemname = object.getString("itemname");
                    String mName = object.getString("membername");
                    String memberId = object.getString("memberId");
                    String BillDate = object.getString("BillDate");
                    String ACCharge = object.getString("ACCharge");
                    String BillDetNumber = object.optString("BillDetNumber");
                    String otNo = object.getString("otno");
                    String opBalance = db.getOpeningBalance(mAcc);

                    cl.setItemID(ItemID);
                    cl.setItemCode(ItemCode);
                    cl.setQuantity(Quantity);
                    cl.setAmount(Amount);
                    cl.setBillID(BillID);
                    cl.setTaxDescription(taxDescription);
                    cl.setmAcc(mAcc);
                    cl.setTaxValue(taxValue);
                    cl.setAva_balance(ava_balance);
                    cl.setBillno(billNo);
                    cl.setTaxAmount(taxAmount);
                    cl.setmName(mName);
                    cl.setMemberID(memberId);
                    cl.setBillDate(BillDate);
                    cl.setRate(Rate);
                    cl.setItemname(itemname);
                    cl.setOtno(otNo);
                    cl.setCardType(params[2]);
                    cl.setACCharge(ACCharge);
                    cl.setBillDetNumber(BillDetNumber);
                    cl.setOpeningBalance(opBalance);
                    closeorderItemsArrayList.add(cl);
                }
                status = jsonObject.toString();

            } catch (Exception e) {
                e.printStackTrace();
                status = e.toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MakeOrder.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (pageType.equals("0"))  //close order
            {
                if (s.contains("true")) {
                    if (closeorderItemsArrayList.size() == 0 || closeorderItemsArrayList == null) {
                        // db.deleteAccount(member,posid,userId);
                        String ot = sharedpreferences.getString("ot_close", "");
                        db.deleteAccount_new(ot, storeId, userId, bill);
                        //readCard();
                        Toast.makeText(getApplicationContext(), "Order not taken for the member", Toast.LENGTH_LONG).show();
                    } else {
                        //db.deleteAccount(member,posid,userId);
                        String ot = sharedpreferences.getString("ot_close", "");
                        db.deleteAccount_new(ot, storeId, userId, bill);
                        Toast.makeText(getApplicationContext(), "Order closed ", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(MakeOrder.this, CloseOrderdetails.class);

                        i.putExtra("pageValue", 11);
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("creditName", creditName);
                        i.putExtra("tableNo", table);
                        i.putExtra("tableId", tableId);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("cardType", cardType);
                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("waiternamecode", waiternamecode);
                        startActivity(i);
                        finish();
                    }

                } else {

                    Toast.makeText(getApplicationContext(), "Order not closed-->" + s, Toast.LENGTH_LONG).show();
                }
            }
//            else if (pageType.equals("1"))  //cancel order
//            {
//                if (s.contains("true")) {
//                    if (closeorderItemsArrayList.size() == 0 || closeorderItemsArrayList == null) {
//                        Toast.makeText(getApplicationContext(), "Order not taken for the member", Toast.LENGTH_LONG).show();
//                    }
//                    else {
//                       // String memeberAnumber = memberAn.getText().toString();
//                        Intent i = new Intent(PlaceOrder_new.this, CloseOrderdetails.class);
//                        i.putExtra("pageValue", 1);
//                        i.putExtra("memeberAnumber", creditAccountno);
//                        i.putExtra("creditAno", creditAccountno);
//                        i.putExtra("creditName", creditName);
//                        i.putExtra("tableNo", table);
//                        i.putExtra("cardType", cardType);
//                        i.putExtra("tableId", tableId);
//                        i.putExtra("waiterid",waiterid);
//                        i.putExtra("waiternamecode",waiternamecode);
//                        //   i.putExtra("position",position);
//                        //  i.putExtra("waiternamecode",waiternamecode);
//                        startActivity(i);
//                        finish();
//                    }
//
//
//                } else {
//
//                    Toast.makeText(getApplicationContext(), "No Item to Cancel-->" + s, Toast.LENGTH_LONG).show();
//
//                }
//            }


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2 && data != null) {

            String _waiterid = data.getStringExtra("waiterid");
            String waitername = data.getStringExtra("waiternamecode");
            steward.setText("Waiter         :" + waitername);
            waiterid = _waiterid;
            String mid = BillingProfile.member;
            String storeId = sharedpreferences.getString("storeId", "");
            db.updateWaiterId(mid, storeId, waitername);
            waiternamecode = waitername;
        } else if (requestCode == 3) {
            Toast.makeText(MakeOrder.this, "reached", Toast.LENGTH_LONG).show();
            itemView.removeAllViews();
            additems(2);
        }

    }

    private void AlertError(String card, String ano, String error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Network Error:");
        builder.setIcon(R.drawable.wifi);
        builder.setCancelable(false);
        builder.setMessage("Please verify and Try again");
        builder.setNegativeButton("Verify", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    //     new AsyncCloseOrder().execute(ss1[1], "1", "");

                    new AsyncCloseOrder1().execute(memberId, "1", "");
                } else {
                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                    AlertError("", "", "");
                }

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    protected class AsyncCloseOrder1 extends AsyncTask<String, Void, String> {
        String status = "";
        String storeId = sharedpreferences.getString("storeId", "");
        String deviceid = sharedpreferences.getString("deviceId", "");
        String userId = sharedpreferences.getString("userId", "");
        String pageType = "";
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(MakeOrder.this);
            try {
                Calendar c = Calendar.getInstance();
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                closeorderItemsArrayList1.clear();

                //JSONObject jsonObject=api.cms_closeOrder(params[0], deviceid, storeId, formattedDate,Integer.parseInt(params[1]),userId,"","","");
                //JSONObject jsonObject=api.cms_getOTReprint(params[0], deviceid, storeId, formattedDate, userId, cardType, "",contractorsId);

                //if(bill.equals("coupon")){
                //     jsonObject=api.cms_getOTReprint(params[0], storeId, formattedDate, userId);
                //  }else {
                // jsonObject=api.cms_getOTReprint(params[0], storeId, formattedDate, userId);
                //  }

                if (bill.equals("coupon")) {
                    //jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "COUPON", billMode);
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "MEMBER CARD", Bill);
                } else if (bill.equals("Party Billing")) {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "PARTY CARD", Bill);
                } else if (bill.equals("Direct Party Billing")) {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "DIRECTPARTY CARD", Bill);
                } else {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, cardType, Bill);
                }

                JSONArray array = jsonObject.getJSONArray("Value");
                CloseorderItems cl = null;
                pageType = params[1];

                db.clearotreprint();
                for (int i = 0; i < array.length(); i++) {
                    cl = new CloseorderItems();
                    JSONObject object = array.getJSONObject(i);
                    String ItemID = object.optString("ItemID");
                    String ItemCode = object.optString("ItemCode");
                    String Quantity = object.optString("Quantity");
                    String Amount = object.optString("Amount");
                    String BillID = object.optString("BillID");
                    String mAcc = object.getString("memberAcc");
                    String billNo = object.optString("billnumber");
                    String Rate = object.getString("rate");
                    String itemname = object.getString("itemname");
                    String mName = object.getString("membername");
                    String memberId = object.getString("memberId");
                    String BillDate = object.getString("BillDate");
                    String ItemCategoryID = object.getString("ItemCategoryID");
                    String otNo = object.getString("otno");
                    db.insertotreprint(ItemID, ItemCode, Quantity, Amount, BillID, mAcc, "", billNo, Rate, itemname, mName, memberId, BillDate, otNo, ItemCategoryID);
                    cl.setItemID(ItemID);
                    cl.setItemCode(ItemCode);
                    cl.setQuantity(Quantity);
                    cl.setAmount(Amount);
                    cl.setBillID(BillID);
                    cl.setmAcc(mAcc);
                    cl.setBillno(billNo);
                    cl.setmName(mName);
                    cl.setMemberID(memberId);
                    cl.setBillDate(BillDate);
                    cl.setRate(Rate);
                    cl.setItemname(itemname);
                    cl.setOtno(otNo);
                    cl.setCardType(cardType);
                    cl.setItemCategoryID(ItemCategoryID);

                    closeorderItemsArrayList1.add(cl);
                }
                status = jsonObject.toString();

            } catch (Exception e) {
                e.printStackTrace();
                status = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MakeOrder.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (closeorderItemsArrayList1.size() > 0) {
                Intent i = new Intent(MakeOrder.this, OT_Reprint.class);
                i.putExtra("type", "makeorder");
                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(), "Please Place OT for the Member", Toast.LENGTH_LONG).show();
            }


        }
    }

}
