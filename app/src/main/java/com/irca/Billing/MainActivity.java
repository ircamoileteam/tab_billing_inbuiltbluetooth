package com.irca.Billing;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.irca.ImageCropping.CircularImage;
 import com.irca.ServerConnection.ConnectionDetector;
import com.irca.Utils.CardTest;
import com.irca.Utils.Constants;
import com.irca.Utils.DialogUtils;
import com.irca.Utils.SDK_Result;
import com.irca.Utils.SystemInfoUtils;
import com.irca.activity.ConfigurationActivity;
import com.irca.activity.MenuNewListActivity;
import com.irca.cosmo.BuildConfig;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.db.Dbase;
import com.irca.dto.MemberDtos;
import com.irca.dto.MenuNewDto;
import com.irca.dto.Version;
import com.irca.scan.PermissionsManager;
import com.zcs.sdk.ConnectTypeEnum;
import com.zcs.sdk.DriverManager;
import com.zcs.sdk.SdkResult;
import com.zcs.sdk.Sys;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/*
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
*/


public class MainActivity extends AppCompatActivity {
    Button login;
    EditText userName, passWord;
    CheckBox remember;
    TextView forgetPassword;
    SharedPreferences sharedpreferences;
    SharedPreferences urlSharedpreferences;
    String sharedU = "";
    String sharedP = "";
    ProgressDialog pd;
    Dbase db;
    int storeCount = 0;
    TelephonyManager tel;
    String imei = "";
    String serialNo = "";
    BluetoothAdapter mBluetoothAdapter;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    ImageView logo;
    Bitmap bitmap;
    TextView version;
    LinearLayout configLinearLayout;
    public static Boolean isSmartCard = false;
    public static String cashCard = "";
    public static ArrayList<String> addressList = null;

    Dialog dialog;
    TextView percentageTextView;
    TextView downloadingTextView;
    TextView progressTextView;
    ProgressBar progressBar;
    String PATH;

    private DriverManager mDriverManager = DriverManager.getInstance();
    private com.zcs.sdk.Printer mPrinter1;

    private ExecutorService mSingleThreadExecutor;
    private static final String TAG = "PrintFragment";
    private PermissionsManager mPermissionsManager;
    private Sys mSys = mDriverManager.getBaseSysDevice();
    private static final int REQ_CODE_READ_PHONE = 0x01;
    private static final int REQ_CODE_PERMISSIONS = 0x02;
    private final String[] mPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    private ProgressDialog mDialogInit;
    int count = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
        //    Fabric.with(this, new Crashlytics());
            setContentView(R.layout.activity_main);
            login = findViewById(R.id.button_login);
            logo = findViewById(R.id.imageView_logo);
            userName = findViewById(R.id.editText_user);
            passWord = findViewById(R.id.editText_pass);
            remember = findViewById(R.id.checkBox);
            version = findViewById(R.id.version);
            configLinearLayout = findViewById(R.id.linear_layoutConfig);
            tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            cd = new ConnectionDetector(getApplicationContext());
            isInternetPresent = cd.isConnectingToInternet();
            forgetPassword = findViewById(R.id.login_settings);
            try {
                // imei=tel.getDeviceId();  // todo remove commented line
                //imei= Settings.Secure.getString(getApplicationContext().getContentResolver(),   Settings.Secure.ANDROID_ID);
                serialNo = "";
            //    version.setText("Version  " + BuildConfig.VERSION_NAME);
                version.setText("Version  " + "1.2.4");

            } catch (Exception e) {
                String rr = e.getMessage();
            }


            try {
                //    bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
                bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.kot);
                logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));
            } catch (Exception e) {

            }
            try {
                checkPermission();
                
            } catch (Exception e) {
                String exr = e.toString();
            }
            db = new Dbase(MainActivity.this);
            storeCount = db.getStoreCount();//to check pos
            sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            sharedU = sharedpreferences.getString("appUser", "");
            sharedP = sharedpreferences.getString("appPass", "");
            String isChecked = sharedpreferences.getString("isChecked", "");
            // remove while live
            String ReportClassError = sharedpreferences.getString("ReportClassError", "");

            urlSharedpreferences = getSharedPreferences("URLS", Context.MODE_PRIVATE);
            String url = urlSharedpreferences.getString("url", "");
            if (url.isEmpty()) {
                Toast.makeText(this, "Please configure URL in Configuration Module", Toast.LENGTH_SHORT).show();
            } else {
                new Update().execute();
                Constants.MAINURL = url;
            }

            //
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = cd.isConnectingToInternet();
                    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    //17-11-2020        String address = sharedpreferences.getString("clubAddress", "");
                    String username = userName.getText().toString();
                    String password = passWord.getText().toString();
                    if (username.equals("")) {
                        Toast.makeText(getApplicationContext(), "Enter username ", Toast.LENGTH_SHORT).show();
                    }
               /* else if (password.equals("")) {
                   Toast.makeText(getApplicationContext(), "Enter password ", Toast.LENGTH_SHORT).show();
                }*/
                    //TODO remove the commmented line before generating apk
              /*  else if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBluetooth, 0);
                }*/
                    else if (username.equals("irca") && password.equals("irca")) {
                        Intent i = new Intent(MainActivity.this, CardTest.class);
                        startActivity(i);
                    } else {
                     //   String manufacturer = Build.MANUFACTURER;
                        String manufacturer ="";
                      //  String model = Build.MODEL;
                        String model = "";
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new AsyncLogin().execute(username, password, serialNo, manufacturer + "" + model);
                        } else {
                            Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                        }

                    }

                }
            });
            forgetPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "Contact your club", Toast.LENGTH_SHORT).show();
                }
            });

            if (isChecked.equals("true")) {
                userName.setText(sharedU);
                passWord.setText(sharedP);
                remember.setChecked(true);
            }
            configLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, ConfigurationActivity.class));
                }
            });
            remember.setOnClickListener(new View.OnClickListener() {
                String isChecked = sharedpreferences.getString("isChecked", "");

                @Override
                public void onClick(View v) {

                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    boolean dd = true;
                    String dummyUser = userName.getText().toString();
                    String dummyPass = passWord.getText().toString();
                    if (dd) {
                        if (isChecked.equals("true")) {
                            editor.putString("isChecked", "false");
                            editor.commit();
                            //  userName.setText("");
                            //  passWord.setText("");
                            remember.setChecked(false);
                            isChecked = sharedpreferences.getString("isChecked", "");
                        } else if (isChecked.equals("")) {
                            editor.putString("isChecked", "true");
                            editor.commit();
                            remember.setChecked(true);
                            isChecked = sharedpreferences.getString("isChecked", "");
                        } else {
                            editor.putString("isChecked", "true");
                            editor.commit();
                            userName.setText(dummyUser);
                            passWord.setText(dummyPass);
                            remember.setChecked(true);
                            isChecked = sharedpreferences.getString("isChecked", "");
                        }

                    } else {
                        editor.putString("isChecked", "false");
                        editor.commit();
                    }
                }
            });
            db.deleteInvalidOffer("");
        } catch (Throwable e) {
            e.printStackTrace();
            Toast.makeText(MainActivity.this,"ERROR** : "+e,Toast.LENGTH_LONG).show();
        }
    }

    private void callUpdateApi(String url) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        new DownloadNewVersion().execute(url);
    }

    private void appUpdate(final String url) {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update);
        TextView okTextView = dialog.findViewById(R.id.text_viewOk);
        TextView laterTextView = dialog.findViewById(R.id.text_viewLater);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        laterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        okTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog.dismiss();
                    if (Build.VERSION.SDK_INT >= 23) {
                        requestPermissions(1, url);
                    } else {
                        callUpdateApi(url);
                    }
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "Unable to Connect Try Again...", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
    }


    private void requestPermissions(int key, String url) {
        try {
            int permissionCheck3 = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int permissionCheck7 = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);


            if (permissionCheck3 == -1 || permissionCheck7 == -1) {
                requestPermissionsAlert();
            } else if (key == 1) {
                callUpdateApi(url);

            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    private void requestPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.need_storage_permission));
        builder.setMessage(getString(R.string.app_needs_permission));
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.grant), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, 225);
                dialog.cancel();
            }
        });
        builder.show();
    }


    @SuppressLint("StaticFieldLeak")
    public class DownloadNewVersion extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(MainActivity.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_progress_bar);
            percentageTextView = dialog.findViewById(R.id.text_viewPercentage);
            downloadingTextView = dialog.findViewById(R.id.text_viewDownloading);
            progressTextView = dialog.findViewById(R.id.text_viewProgress);
            progressBar = dialog.findViewById(R.id.progress_bar);
            Drawable progressDrawable = progressBar.getProgressDrawable().mutate();
            progressDrawable.setColorFilter(getResources().getColor(R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
            progressBar.setProgressDrawable(progressDrawable);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }


        @Override
        protected Boolean doInBackground(String... arg0) {
            Boolean flag;
            try {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                builder.detectFileUriExposure();
                URL url = new URL(arg0[0]);
                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                try {
                    path.mkdirs();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                PATH = path.getAbsolutePath();
                File file = new File(PATH);
                file.mkdirs();
                File outputFile = new File(file, "TabBilling.apk");
                try {
                    if (outputFile.exists()) {
                        outputFile.delete();
                    }
                    URLConnection connection = url.openConnection();
                    connection.connect();

                    // this will be useful so that you can show a tipical 0-100%
                    // progress bar
                    int lengthOfFile = connection.getContentLength();
                    int count;

                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream());
                    FileOutputStream output = new FileOutputStream(outputFile.getAbsoluteFile());

                    byte data[] = new byte[1024]; //anybody know what 1024 means ?
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
                        publishProgress((int) ((total * 100) / lengthOfFile));

                        // writing data to file
                        output.write(data, 0, count);


                    }

                    // flushing output
                    output.flush();

                    // closing streams
                    output.close();
                    input.close();


                } catch (Throwable e) {
                    e.printStackTrace();
                }

                flag = true;
            } catch (Exception e) {
                Log.e("TAG", "Update Error: " + e.getMessage());
                flag = false;
            }
            return flag;
        }

        @SuppressLint("SetTextI18n")
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            String msg;
            if (progress[0] > 99) {
                msg = "Finishing...";
                downloadingTextView.setText(msg);
                percentageTextView.setText("100%");
                progressBar.setProgress(100);
                progressTextView.setText("Wait...");
            } else {
                msg = progress[0] + "%";
                percentageTextView.setText(msg);
                progressBar.setProgress(progress[0]);
                progressTextView.setText(progress[0] + "/100");
                if ((progress[0] % 2) == 1) {
                    downloadingTextView.setText("Downloading..");
                } else {
                    downloadingTextView.setText("Downloading...");
                }
            }
        }


        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if (result) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                builder.detectFileUriExposure();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uriFromFile(MainActivity.this, new File(PATH + "/TabBilling.apk")), "application/vnd.android.package-archive");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
//                Toast.makeText(MainActivity.this, "Please goto downloads and click on TabBilling App to install latest app", Toast.LENGTH_LONG).show();
                Toast.makeText(MainActivity.this, "Click on Install", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Error: Try Again", Toast.LENGTH_SHORT).show();
            }
        }

    }


    Uri uriFromFile(Context context, File file) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
        } else {
            return Uri.fromFile(file);
        }
    }

    protected class AsyncLogin extends AsyncTask<String, Void, String> {
        boolean authentication = false;
        boolean isacrive = false;
        String connectionStatus = "";
        String u, p;
        String status = "";

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(MainActivity.this);
            try {
                addressList = new ArrayList<>();
                JSONObject jsonObject = api.getCommonUser_bgc(params[0], params[1], params[2], params[3]);
                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                u = params[0];
                p = params[1];
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    String authtentication = jsonObject1.optString("Auth");
                    String device = jsonObject1.optString("device");
                    if (authtentication.equals("True") && device.equals("second")) {
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        authentication = true;
                        String loginname = jsonObject1.getString("LoginName");
                        String userId = jsonObject1.getString("UserID");
                        String IsActive = jsonObject1.getString("IsActive");
                        String waiterName = jsonObject1.optString("Name");
                        String clubName = jsonObject1.optString("clubName");
                        String clubAddress = jsonObject1.optString("clubaddress");
                        String creditLimit = jsonObject1.optString("clubCreditlimit");
                        String smartCard = jsonObject1.optString("isSCA");
                        String isSteward = jsonObject1.optString("IsSteward");
                        cashCard = jsonObject1.optString("isCashCardFilled");

                   isSmartCard = !smartCard.contains("false");

                     //   isSmartCard=false;
                        if (clubAddress.contains("$")) {

                            String dummy[] = clubAddress.split("\\$");
                            //   String dummy[]= Pattern.quote("\\$");
                            for (int j = 0; j < dummy.length; j++) {
                                addressList.add(dummy[j]);
                            }
                        } else {
                            addressList.add(clubAddress);
                        }
                        editor.putString("loginName", loginname);
                        editor.putString("userId", userId);
                        editor.putString("deviceId", serialNo);
                        editor.putString("WaiterName", waiterName);
                        editor.putString("clubName", clubName);
                        editor.putString("clubAddress", clubAddress);
                        editor.putString("creditLimit", creditLimit);
                        editor.putString("isSteward", isSteward);
                        editor.putBoolean("CardRead", isSmartCard);
                        editor.commit();
                        if (IsActive.equals("True")) {
                            isacrive = true;
                        }
                    } else {
                        status = "false";

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                connectionStatus = e.toString();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MainActivity.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                pd.dismiss();
                if (authentication && isacrive) {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("appUser", u);
                    editor.putString("appPass", p);

                    editor.apply();
                    String Dashboard = sharedpreferences.getString("Dashboard", "");
                    Class c = null;
                    try {
                        c = Class.forName("com.irca.Billing." + Dashboard);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    if (storeCount != 0) {
                        //Intent i = new Intent(MainActivity.this, Dashboard.class);
                        Intent i = null;
                        i = new Intent(MainActivity.this, c);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    } else {
                        Intent i = new Intent(MainActivity.this, StoreList.class);
                        // Intent i = new Intent(MainActivity.this, Dashboard_new.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }

                } else if (!connectionStatus.equals("")) {
                    Toast.makeText(MainActivity.this, "Not connected to server(or)No internet connection" + connectionStatus, Toast.LENGTH_LONG).show();
                } else if (authentication == false) {
                    Toast.makeText(MainActivity.this, "Invalid username/password", Toast.LENGTH_LONG).show();
                } else if (status.equals("false")) {
                    Toast.makeText(MainActivity.this, "License error ,Please Contact Club", Toast.LENGTH_LONG).show();
                    // Toast.makeText(MainActivity.this,"Invalid Username and Password ",Toast.LENGTH_LONG) .show();
                } else if (isacrive == false) {
                    Toast.makeText(MainActivity.this, "Inactive login", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MainActivity.this, "Not connected to server(or)No internet connection", Toast.LENGTH_LONG).show();
                }

            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public class Update extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(MainActivity.this);
        String status = "";
        String result = "";
        String error = "";
        String url = "";
        String versionCode = "";
        ProgressDialog pd;
        Version version;

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(MainActivity.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
//                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.GetLatestApkReleaseVersion();
                    result = "" + getResult;
                    if (result.contains("true")) {
                        JSONArray jsonArray = getResult.getJSONArray("Value");
                        try {
                            Gson gson = new Gson();
                            version = gson.fromJson(String.valueOf(jsonArray.get(0)), new TypeToken<Version>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }

                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(MainActivity.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        if (version.getApkURL() != null) {
                            if (!version.getApkURL().isEmpty()) {
                                if (version.getVersion() != null) {
                                    if (!version.getVersion().isEmpty()) {
                                        int serverCode = 0;
                                        try {
                                            serverCode = Integer.parseInt(version.getVersion());
                                        } catch (Throwable e) {
                                            e.printStackTrace();
                                        }
                                        int versionCodee = BuildConfig.VERSION_CODE;
                                        if (serverCode > versionCodee) {
                                            appUpdate(version.getApkURL());
                                        }
                                    } else {
                                        Toast.makeText(MainActivity.this, "unable to get version details", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(MainActivity.this, "unable to get version details", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(MainActivity.this, "unable to get url to download APK", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "unable to get url to download APK", Toast.LENGTH_SHORT).show();
                        }

                        pd.dismiss();
                        break;
                    case "server":
                        Toast.makeText(MainActivity.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(MainActivity.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(MainActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(MainActivity.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }

    private String getTodayDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    private void checkPermission() {
        mPermissionsManager = new PermissionsManager(MainActivity.this) {
            @Override
            public void authorized(int requestCode) {
                if (requestCode == REQ_CODE_READ_PHONE) {
                    initSdk();
                }
            }

            @Override
            public void noAuthorization(int requestCode, String[] lacksPermissions) {
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Warning");
                builder.setMessage("Please open permission");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PermissionsManager.startAppSettings(MainActivity.this.getApplicationContext());
                    }
                });
                builder.create().show();
            }

            @Override
            public void ignore(int requestCode) {
                if (requestCode == REQ_CODE_READ_PHONE) {
                    initSdk();
                }
            }
        };
        mPermissionsManager.checkPermissions(MainActivity.this, REQ_CODE_READ_PHONE, Manifest.permission.READ_PHONE_STATE);
        mPermissionsManager.checkPermissions(MainActivity.this, REQ_CODE_PERMISSIONS, mPermissions);
    }

 /*   void initSdk() {
        initSdk();
    }
*/
    int speed = 460800;

    //int speed = 115200;
    private void initSdk() {
        int status = mSys.sdkInit();
        if(status != SdkResult.SDK_OK) {
            mSys.sysPowerOn();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        status = mSys.sdkInit();
        if(status != SdkResult.SDK_OK) {
            //init failed.
        }
    }
/*
    private void initSdk(final boolean reset) {
        // Config the SDK base info
        mSys.showLog(true);
        // mSys.showLog( getPreferenceManager().getSharedPreferences().getBoolean(getString(R.string.key_show_log), true));

        if (mDialogInit == null) {
            mDialogInit = (ProgressDialog) DialogUtils.showProgress(MainActivity.this, "Waiting", "Initializing");
        } else if (!mDialogInit.isShowing()) {
            mDialogInit.show();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                int statue = mSys.getFirmwareVer(new String[1]);
                if (statue != SdkResult.SDK_OK) {
                    int sysPowerOn = mSys.sysPowerOn();
                    Log.i(TAG, "sysPowerOn: " + sysPowerOn);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                mSys.setUartSpeed(speed);
                final int i = mSys.sdkInit();
                if (i == SdkResult.SDK_OK) {
                    setDeviceInfo();
                }
                if (reset && ++count < 2 && i == SdkResult.SDK_OK && mSys.getCurSpeed() != 460800) {
                    Log.d(TAG, "switch baud rate, cur speed = " + mSys.getCurSpeed());
                    int ret = mSys.setDeviceBaudRate();
                    if (ret != SdkResult.SDK_OK) {
                        DialogUtils.show(MainActivity.this, "SwitchBaudRate error: " + ret);
                    }
                    mSys.sysPowerOff();
                    initSdk();
                    return;
                }
                if (MainActivity.this != null) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mDialogInit != null)
                                mDialogInit.dismiss();
                            Log.d(TAG, "Cur speed: " + mSys.getCurSpeed());
                            if (BuildConfig.DEBUG && mSys.getConnectType() == ConnectTypeEnum.COM) {
                                DialogUtils.show(MainActivity.this, "Cur speed: " + mSys.getCurSpeed());
                            }
                            String initRes = (i == SdkResult.SDK_OK) ? "init_success" : SDK_Result.obtainMsg(MainActivity.this, i);

                            Toast.makeText(MainActivity.this, initRes, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }
*/


    private void setDeviceInfo() {
        // 读取并判断, 不存在则存入
        byte[] info = new byte[1000];
        byte[] infoLen = new byte[2];
        int getInfo = mSys.getDeviceInfo(info, infoLen);
        if (getInfo == SdkResult.SDK_OK) {
            int len = infoLen[0] * 256 + infoLen[1];
            byte[] newInfo = new byte[len];
            System.arraycopy(info, 0, newInfo, 0, len);
            String infoStr = new String(newInfo);
            Log.i(TAG, "getDeviceInfo: " + getInfo + "\t" + len + "\t" + infoStr);
            if (!TextUtils.isEmpty(infoStr)) {
                String[] split = infoStr.split("\t");
                // 已存则返回
                try {
                    // 确保imei1和mac 存值正确
                    if (split.length >= 4) {
                        String val1 = split[0].split(":")[1];
                        String val4 = split[3].split(":")[1];
                        if (!TextUtils.isEmpty(val1) && !val1.equals("null") && val1.length() >= 15
                                && !TextUtils.isEmpty(val4) && !val4.equals("null") && val4.length() >= 12 && val4.contains(":")) {
                            Log.i(TAG, "Have saved, return");
                            return;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        Map<String, String> map = SystemInfoUtils.getImeiAndMeid(MainActivity.this.getApplicationContext());
        String imei1 = map.get("imei1");
        String imei2 = map.get("imei2");
        String meid = map.get("meid");
        String mac = SystemInfoUtils.getMac();
        WifiManager wifi = (WifiManager) MainActivity.this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        long start = System.currentTimeMillis();
        while (TextUtils.isEmpty(mac) && System.currentTimeMillis() - start < 5000) {
            if (!wifi.isWifiEnabled()) {
                wifi.setWifiEnabled(true);
            }
            mac = SystemInfoUtils.getMac();
        }
        Log.i(TAG, "mac = " + mac);
        if (TextUtils.isEmpty(mac)) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.show(MainActivity.this, "Warning! No mac!!!");
                }
            });
            return;
        }
        String msg = "IMEI1:" + (imei1 == null ? "" : imei1) + "\t" + "IMEI2:" + (imei2 == null ? "" : imei2) + "\t" + "MEID:" + (meid == null ? "" : meid) + "\t" + "MAC:" + mac;
        Log.i(TAG, "readDeviceInfo: " + msg);
        byte[] bytes = msg.getBytes();
        int setInfo;
        int count = 0;
        do {
            setInfo = mSys.setDeviceInfo(bytes, bytes.length);
            Log.i(TAG, "setDeviceInfo: " + setInfo);
            if (setInfo == SdkResult.SDK_OK) {
                break;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (count++ < 5);
    }

}
