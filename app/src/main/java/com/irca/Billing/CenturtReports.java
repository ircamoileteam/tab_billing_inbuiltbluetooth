package com.irca.Billing;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.activity.TableTransferActivity;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by Manoch Richard on 06-Jul-17.
 */

public class CenturtReports extends Activity {
    LinearLayout reports;
    ProgressDialog pd;
    SharedPreferences sharedpreferences;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.century_utilityreports);
        reports = (LinearLayout) findViewById(R.id.reports);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        new AsyncReceiptInfo().execute();
    }

    protected class AsyncReceiptInfo extends AsyncTask<String, JSONObject, ArrayList<String>> {
        JSONObject jsonObj = null;
        String server = "";
        String storeid=sharedpreferences.getString("storeId","");
        String mstoreid=sharedpreferences.getString("mstoreId","");
        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> value = null;
            RestAPI api = new RestAPI(CenturtReports.this);
            try {
                jsonObj = api.cms_getUitilityBills(storeid,mstoreid);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.d("AsyncLogin", e.getMessage());
                server = e.getMessage().toString();

            }
            return value;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(CenturtReports.this);
            pd.setMessage("Loading... please wait");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);

        }

        @Override
        protected void onPostExecute(ArrayList<String> result) {

            try {
                reports.removeAllViews();
                if (server.equals("")) {
                    JSONArray jsonArray = jsonObj.getJSONArray("Value");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String accno = jsonObject1.getString("accno");
                        String name = jsonObject1.getString("name");
                        String amount = jsonObject1.getString("amount");
                        String receiptno = jsonObject1.getString("receiptno");


                        View inflater_view = getLayoutInflater().inflate(R.layout.receiptno_items, reports, false);
                        TextView txt_receiptno = (TextView) inflater_view.findViewById(R.id.txt_receiptno);
                        txt_receiptno.setText(accno);

                        TextView txt_date = (TextView) inflater_view.findViewById(R.id.txt_date);
                        txt_date.setText(name);

                        TextView txt_amount = (TextView) inflater_view.findViewById(R.id.txt_amount);
                        txt_amount.setText(amount);

                        TextView txt_receipt = (TextView) inflater_view.findViewById(R.id.txt_receipt);
                        txt_receipt.setText(receiptno);

//                        TextView txt_header1 = (TextView) inflater_view.findViewById(R.id.header1);
//                        txt_header1.setText("COUNTER:");
//                        TextView txt_header2 = (TextView) inflater_view.findViewById(R.id.header2);
//                        txt_header2.setText("SLIP COUNT:");
//                        TextView txt_header3 = (TextView) inflater_view.findViewById(R.id.header3);
//                        txt_header3.setText(receiptno);


                        reports.addView(inflater_view);

                    }
                } else {
                    Toast.makeText(CenturtReports.this, "Error" + server, Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            pd.dismiss();

        }

    }

}
