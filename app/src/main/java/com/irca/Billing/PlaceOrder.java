package com.irca.Billing;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.cosmo.Alert_ItemDescription;
import com.irca.cosmo.R;
import com.irca.db.Dbase;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;

import java.util.ArrayList;

//import io.fabric.sdk.android.Fabric;

public class PlaceOrder extends AppCompatActivity {
    Bundle bundle;
    int table,tableId;
    String creditLimit="" ,creditAccountno="",creditName="",memberId="",cardType="",_params="",waiterid="",waiternamecode="";
    int position;
    TextView openBalance ,steward,tableNo;
    EditText editText;
    LinearLayout itemView;
    TelephonyManager tel;
    String serialNo;

    int noOfCounts = 0;
    TextView txt_itemcount;
    View view = null;
    TextView itemName, itemCount,memberName;
    ImageView add, subtract,search;
    Dbase db;
    String groupId = "";
    CheckBox happyHour;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_place_order);
       // actionBarSetup();
        bundle= getIntent().getExtras();
        table= bundle.getInt("tableNo");
        creditLimit= bundle.getString("creditLimit", creditLimit);
        creditAccountno= bundle.getString("creditAno", creditAccountno);
        creditName=  bundle.getString("creditName", creditName);
        memberId=bundle.getString("memberId", memberId);
        tableId= bundle.getInt("tableId", tableId);
        cardType= bundle.getString("cardType", cardType);
        _params=bundle.getString("referenceNo", _params);
        waiterid=bundle.getString("waiterid",waiterid);
        position=bundle.getInt("position",position);
        waiternamecode=bundle.getString("waiternamecode",waiternamecode);
        tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        serialNo= Build.SERIAL;


        openBalance=(TextView)findViewById(R.id.op_bal);
        steward=(TextView)findViewById(R.id.steward);
        tableNo=(TextView)findViewById(R.id.table);
        memberName=(TextView)findViewById(R.id.memName);
        itemView= (LinearLayout) findViewById(R.id.itemView);
       // txt_itemcount = (TextView) findViewById(R.id.txtCount);
        db=new Dbase(PlaceOrder.this);

        openBalance.setText("Opening Balance :" + creditLimit + "Rs.");
        steward.setText    ("Steward         :"+ waiternamecode );
        memberName.setText(""+creditName + "\n ( " + creditAccountno +" )");

        tableNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertTableNO();
            }
        });

         //toolbar = (Toolbar) findViewById(R.id.toolbar);
         //setSupportActionBar(toolbar);
         //getSupportActionBar().setTitle(""+creditName + "\n ( " + creditAccountno +" )" );

       // getSupportActionBar().setSubtitle(""+creditAccountno);
      //  getSupportActionBar().setSubtitle(""+"archanna");

        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/


              /*  Intent i = new Intent(, Itemview.class);
                i.putExtra("tableNo", tableNo);
                i.putExtra("creditLimit", creditLimit);
                i.putExtra("creditAno", acc);
                i.putExtra("creditName", Nmae);
                i.putExtra("memberId", memberId);
                i.putExtra("tableId", tableId);
                i.putExtra("cardType", cardType);
                i.putExtra("referenceNo", referenceNo);
                i.putExtra("position",position);
                i.putExtra("waiternamecode",waiternamecode);
                i.putExtra("waiterid",waiterid);
                startActivity(i);
*/

                finish();
            }
        });

        itemView.removeAllViews();
        additems(2);
    }

    //*****
    private void alertTableNO() {

        AlertDialog.Builder alertBulider=new AlertDialog.Builder(this);
        alertBulider.setMessage("Table Number  ");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_table_no, null);
        alertBulider.setView(dialogView);

        editText = (EditText) dialogView.findViewById(R.id.tableno);

        alertBulider.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // finish();
                tableNo.setText("Table "+editText.getText().toString());
               // newTableNo=editText.getText().toString();
            }
        });
        alertBulider.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog=alertBulider.create();
        dialog.show();

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("");
            ab.setElevation(0);
            ab.setDisplayHomeAsUpEnabled(true);

        }
    }

    public void additems(int value) {
        ArrayList<String> list=null;
        noOfCounts=0;

        if(value==1)
        {
            list = db.getItemList(groupId);
        }
        else
        {
            // itemView.removeAllViews();
            itemView=null;
            view=null;
            itemName=null;
            itemCount=null;
            add=null;
            subtract=null;
            happyHour=null;
            list = db.getOTItemList();
            // TODO reo=move the comments
           /* if(!list.isEmpty()){
                placeOrder.setVisibility(View.VISIBLE);
            }else{
                placeOrder.setVisibility(View.GONE);
            }*/
        }

        for (int i = 0; i < list.size(); i++)
        {
            noOfCounts=0;
            if (itemView == null)
            {
                itemView = (LinearLayout) findViewById(R.id.itemView);
                view = getLayoutInflater().inflate(R.layout.itemlist_kot, itemView, false);
                itemName = (TextView) view.findViewById(R.id.kot_itemname);
                itemCount = (TextView) view.findViewById(R.id.kot_itemCount);
                //happyHour=(CheckBox)view.findViewById(R.id.happyhour_checbox);
               // happyHour.setVisibility(View.GONE);
                add = (ImageView) view.findViewById(R.id.kot_add);
                subtract = (ImageView) view.findViewById(R.id.kot_minus);

               /* if(!list.isEmpty()){
                    txt_itemcount.setText(list.size()+"");
                }else{
                    txt_itemcount.setText("0");
                }*/



            } else {
                view = getLayoutInflater().inflate(R.layout.itemlist_kot, itemView, false);
                itemName = (TextView) view.findViewById(R.id.kot_itemname);
                itemCount = (TextView) view.findViewById(R.id.kot_itemCount);
                //happyHour=(CheckBox)view.findViewById(R.id.happyhour_checbox);
                //happyHour.setVisibility(View.GONE);
                add = (ImageView) view.findViewById(R.id.kot_add);
                subtract = (ImageView) view.findViewById(R.id.kot_minus);
                itemView = (LinearLayout) findViewById(R.id.itemView);

              /*  if(!list.isEmpty() )
                {
                    txt_itemcount.setText(list.size()+"");
                }else{
                    txt_itemcount.setText("0");
                }*/



            }

//            happyHour.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    ViewGroup parent = (ViewGroup) v.getParent();
//                    TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
//                    TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);
//                   // CheckBox happy=(CheckBox)parent.findViewById(R.id.happyhour_checbox);
//
//                    String _itemname = t2.getText().toString();
//                    String count = t1.getText().toString();
//
//
//                    if (_itemname.contains("-"))
//                    {
//                        String array[] = _itemname.split("-");
//                        if(happy.isChecked()){
//                            Long rvalur = db.insertOT_tempHappyHour(array[0], array[1], "1");
//                            if (rvalur < 0) {
//                                Toast.makeText(getApplicationContext(), "happyHour not set", Toast.LENGTH_LONG).show();
//                            }
//                        }else {
//                            Long rvalur = db.insertOT_tempHappyHour(array[0], array[1], "0");
//                            if (rvalur < 0) {
//                                Toast.makeText(getApplicationContext(), "happyHour not set", Toast.LENGTH_LONG).show();
//                            }
//                        }
//
//                    }
//                }
//            });



            itemName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int id = v.getId();
                    ViewGroup parent = (ViewGroup) v.getParent();
                    TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                    TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);

                    String _itemname = t2.getText().toString();
                    if (!_itemname.equals("")|| _itemname != null ){
                        String[] ss=_itemname.split("-");
                        String ii=ss[0];

                        String _itemcode = t1.getText().toString();
                        Intent i=new Intent(PlaceOrder.this,Alert_ItemDescription.class);
                        i.putExtra("itemName",_itemname);
                        i.putExtra("itemCode",ii);
                        startActivity(i);
                    }else {
                        Toast.makeText(PlaceOrder.this, "No Item", Toast.LENGTH_SHORT).show();
                    }

                }
            });



            add.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {

                                           int id = v.getId();
                                           ViewGroup parent = (ViewGroup) v.getParent();
                                           TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                                           TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);

                                           String _itemname = t2.getText().toString();
                                           String _itemcode = t1.getText().toString();
                                           String count = t1.getText().toString();

                                           if (count.equals("")) {
                                               t1.setText("1");
                                               noOfCounts = noOfCounts + 1;
                                               // txt_itemcount.setText(noOfCounts + "");

                                               if (_itemname.contains("-"))
                                               {
                                                   String array[] = _itemname.split("-");
                                                   Long rvalur = db.insertOT(array[0], array[1], "1");
                                                   if (rvalur < 0) {
                                                       Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                   }else{

                                                   }

                                               }

                                           } else {
                                               int c = Integer.parseInt(count);
                                               t1.setText(c + 1 + "");
                                               int co=c+1;
                                               noOfCounts = noOfCounts + 1;

                                               if (_itemname.contains("-")) {
                                                   String array[] = _itemname.split("-");
                                                   Long rvalur = db.insertOT(array[0], array[1], Integer.toString(co));
                                                   if (rvalur < 0) {
                                                       Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                   }else{

                                                   }

                                               }
                                               //  txt_itemcount.setText(noOfCounts + "");
                                           }


                                       }
                                   }

            );
            subtract.setOnClickListener(new View.OnClickListener()

                                        {
                                            @Override
                                            public void onClick (View v)
                                            {
                                                try {
                                                    ViewGroup parent = (ViewGroup) v.getParent();
                                                    TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                                                    TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);

                                                    String _itemname = t2.getText().toString();
                                                    String _itemcode = t1.getText().toString();

                                                    String count = t1.getText().toString();
                                                    if (count.equals("")) {
                                                        Toast.makeText(getApplicationContext(), "Add item first", Toast.LENGTH_LONG).show();
                                                    } else {
                                                        int c = Integer.parseInt(count);
                                                        if (c != 0) {
                                                            t1.setText(c - 1 + "");
                                                            noOfCounts = noOfCounts - 1;
                                                            int co=c - 1;

                                                            if (_itemname.contains("-")) {
                                                                String array[] = _itemname.split("-");
                                                                Long rvalur = db.insertOT(array[0], array[1], Integer.toString(co));
                                                                if (rvalur < 0) {
                                                                    Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                                }else{

                                                                }

                                                            }

                                                            // txt_itemcount.setText(noOfCounts + "");
                                                        }
                                                    }



                                                } catch (Exception e) {
                                                    String hh = e.getMessage().toString();
                                                }

                                            }
                                        }

            );
            //itemName.setId(i);
            // itemCount.setId(i);
            //add.setId(i);
            //subtract.setId(i);
            String ll[] = list.get(i).toString().split("\\$");
            if(ll.length>1)

            {
                itemName.setText(ll[0]);
                itemCount.setText(ll[1]);
                noOfCounts = noOfCounts + Integer.parseInt(ll[1]);

                /*if (!list.isEmpty()) {
                    txt_itemcount.setText(list.size() + "");
                } else {
                    txt_itemcount.setText("0");
                }*/

            }

            else

            {
                itemName.setText(ll[0]);
            }

            itemView.addView(view);
        }

    }
}
