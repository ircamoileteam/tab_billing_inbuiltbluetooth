package com.irca.Billing;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.content.res.AppCompatResources;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.db.Dbase;
import com.irca.fields.Item;
import com.irca.fields.ItemGroup;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

//import io.fabric.sdk.android.Fabric;


public class ItemList extends AppCompatActivity {
    Dbase db;
    ListView listView;
    SharedPreferences sharedpreferences;
    String posId = "";
    ArrayList<Item> list;
    String base = "";
    String StoreName = "";
    Bundle b;
    String grpName = "", GrpId = "";
    ItemGroup itemGroup = null;
    public static ArrayList<ItemGroup> itemGrouplist = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_item_list);
        db = new Dbase(ItemList.this);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        posId = sharedpreferences.getString("storeId", "");
        StoreName = sharedpreferences.getString("StoreName", "");

        actionBarSetup();

        b = getIntent().getExtras();
        grpName = b.getString("GroupName");
        GrpId = b.getString("groupId");
        db.deleteitemgrp(GrpId, posId);
        new AsyncitemstockMaster().execute();

        listView = (ListView) findViewById(R.id.listHome);


    }

    protected class AsyncitemstockMaster extends AsyncTask<String, Void, Void> {

        ArrayList<Item> itemList = null;
        Item item = null;
        boolean itembool = false;
        boolean itemgroupbool = false;
        int position = 0;
        String itemCategoryId = sharedpreferences.getString("itemCategoryId", "");
        ProgressDialog pd;

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(ItemList.this);
            try {
                //item list
                int storeid = Integer.parseInt(posId);
                int groupid = Integer.parseInt(GrpId);
                JSONObject jsonObject1 = api.cms_getItemStockbyGroup(storeid, groupid);
                JSONArray jsonArray1 = jsonObject1.getJSONArray("Value");
                itemList = new ArrayList<Item>();
                for (int i = 0; i < jsonArray1.length(); i++) {
                    item = new Item();
                    JSONObject jsonObjectchild = jsonArray1.getJSONObject(i);
                    if (jsonObjectchild.has("ItemGroupID")) {

                        String ItemCategoryID = jsonObjectchild.optString("ItemCategoryID");
                        String ItemCode = jsonObjectchild.optString("ItemCode");
                        //  String ItemIdentityID = jsonObjectchild.optString("ItemIdentityID");
                        String ItemIdentityID = jsonObjectchild.optString("ID");
                        String ItemGroupID = jsonObjectchild.optString("ItemGroupID");
                        String ItemName = jsonObjectchild.optString("ItemName");
                        String StoreID = jsonObjectchild.optString("StoreID");
                        String TaxID = jsonObjectchild.optString("TaxID");
                        String Tax = jsonObjectchild.optString("Tax");
                        String unitId = jsonObjectchild.optString("UnitID");
                        String Rate = jsonObjectchild.optString("Rate");
                        String description = jsonObjectchild.optString("description");
                        String itemImage = jsonObjectchild.optString("ItemImage");
                        String Stock = jsonObjectchild.optString("Stock");


                        item.setItemCategoryID(ItemCategoryID);
                        item.setItemcode(ItemCode);
                        item.setItemIdentityID(ItemIdentityID);
                        item.setItemGroupID(ItemGroupID);
                        item.setItemName(ItemName);
                        item.setStoreID(StoreID);
                        item.setTaxID(TaxID);
                        item.setTax(Tax);
                        item.setUnitID(unitId);
                        item.setRate(Rate);
                        item.setDescription(description);
                        item.setItemImage(itemImage);
                        item.setStock(Stock);
                        itemList.add(item);
                    } else {
                        int id = i;
                    }

                }
                if (itemList != null) {
                    // db.deleteItem();
                    Long mm = db.insertItemstock(itemList);
                    if (mm > 0) {
                        itembool = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ItemList.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            pd.dismiss();

            if (itembool == true) {
           /*     Intent i1 = new Intent(ItemList.this, GroupView.class);
                startActivity(i1);
*/
                ArrayList<Item> list = db.getSearchItemlistAllFilter(GrpId);
                listView.setAdapter(new ItemListAdapter(ItemList.this, list));

            } else {
                Toast.makeText(ItemList.this, "Error", Toast.LENGTH_LONG).show();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("" + StoreName);
            ab.setElevation(0);
            ab.setDisplayHomeAsUpEnabled(true);

        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public class ItemListAdapter extends BaseAdapter {


        ArrayList<Item> masterStore;

        Context context;
        int pageNo;

        public ItemListAdapter(Context c, ArrayList<Item> masterStoreArrayAdapter)

        {
            this.masterStore = masterStoreArrayAdapter;

            context = c;

        }

        @Override
        public int getCount() {
            return masterStore.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.itemview_menu, parent, false);
            }

            TextView itemName = (TextView) convertView.findViewById(R.id.title);
            TextView itemPrice = (TextView) convertView.findViewById(R.id.sdetails);
            TextView itemCode = (TextView) convertView.findViewById(R.id.scode);
            TextView stockdetails = (TextView) convertView.findViewById(R.id.stockdetails);
            ImageView image = (ImageView) convertView.findViewById(R.id.list_image);
            TextView text_viewName1 = convertView.findViewById(R.id.text_viewName1);

            itemName.setText("" + masterStore.get(position).getItemName());
            itemPrice.setText("Rate : " + masterStore.get(position).getRate());
            stockdetails.setText("Available Stock : " + masterStore.get(position).getStock());
            itemCode.setText("" + masterStore.get(position).getItemcode());
            base = masterStore.get(position).getItemImage();

            try {
                text_viewName1.setText(getChars(masterStore.get(position).getItemName().toUpperCase(), 2));
            } catch (Throwable e) {
                e.printStackTrace();
            }

            if (base.isEmpty()) {
                text_viewName1.setVisibility(View.VISIBLE);
                image.setVisibility(View.GONE);
                Drawable unwrappedDrawable = AppCompatResources.getDrawable(context, R.drawable.bg_dynamic_circle);
                assert unwrappedDrawable != null;
                Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                int[] androidColors1 = context.getResources().getIntArray(R.array.androidColors);
                int randomAndroidColor1 = androidColors1[new Random().nextInt(androidColors1.length)];
                DrawableCompat.setTint(wrappedDrawable, randomAndroidColor1);
            } else {
                text_viewName1.setVisibility(View.GONE);
                image.setVisibility(View.VISIBLE);
                Picasso.get().load(base).into(image);
//                byte[] imageAsBytes = Base64.decode(base.getBytes(), Base64.DEFAULT);
//                image.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));

            }

            return convertView;
        }
        private String getChars(String str, int number) {
            try {

                if (str.length() < number) {
                    return str;
                } else {
                    return str.substring(0, number);
                }

            } catch (Throwable e) {
                e.printStackTrace();
                return str;
            }
        }
    }


}
