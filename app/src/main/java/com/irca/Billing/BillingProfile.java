package com.irca.Billing;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.acs.audiojack.AudioJackReader;
import com.acs.audiojack.Result;
//import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.cosmo.Bakery;
import com.irca.cosmo.CloseOrderdetails;
import com.irca.cosmo.MultiPrintView;
import com.irca.cosmo.OT_Reprint;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.cosmo.Search_Item;
import com.irca.db.Dbase;
import com.irca.dto.MemberDetails;
import com.irca.dto.MemberMainDto;
import com.irca.fields.CloseorderItems;
import com.irca.fields.MemberDob;
import com.irca.fields.WaiterDetails;
import com.irca.table.DependentTable;
import com.irca.table.MemberGuestTable;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;
import com.irca.widgets.FloatingActionButton.FloatingActionsMenu;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Pattern;
/*

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
*/


public class BillingProfile extends AppCompatActivity {

    String bookingId = "";
    public static String isCashCardFilled = "";
    // TextView tableNO;
    FloatingActionButton bot;
    EditText memberAn;
    ImageView takeOrder;
    Button corder;
    FloatingActionButton cancelOrder;
    Button order;
    //ImageView backround;
    ArrayAdapter arrayAdapterr;
    SharedPreferences sharedpreferences;
    public static String c_Limit = "";
    List<String> Blist;
    // int table = 0;
    String table = "0";
    ProgressDialog pd;
    int tableId = 0;
    FloatingActionsMenu fabmenu;
    // private boolean mPiccAtrReady;//poer on method
    private boolean mResultReady;//power
    private AudioManager mAudioManager;
    private AudioJackReader mReader;
    private Context mContext = this;
    private ProgressDialog mProgress;
    private boolean mPiccResponseApduReady;// transmit to check apdu return values
    private Object mResponseEvent = null;
    public String finalValue = "";
    public String waiterid = "";
    public static String waiternamecode;
    public int manoch = 0;
    public int overAll = 0;
    public int k = 0;
    public int position;
    public int positionb;
    public String itemValue;
    public String Discountpercent = "";
    public String daniel = "FF 82 00 00 06 FF FF FF FF FF FF," +
            "FF 86 00 00 05 01 00 04 60 00," +
            "FF B0 00 04 10," +
            "FF 86 00 00 05 01 00 05 60 00," +
            "FF B0 00 05 10," +
            "FF 86 00 00 05 01 00 06 60 00," +
            "FF B0 00 06 10 ";
    //public  String daniel1="FF 86 00 00 05 01 00 05 60 00,FF B0 00 05 10 ";

    private int mPiccTimeout = 1;
    private int mPiccCardType = 143;
    private byte[] mPiccCommandApdu;// transmit

    private byte[] mPiccResponseApdu;
    private byte[] mPiccRfConfig = new byte[19];
    private Result mResult;
    public static String cardType = "", member = "", Steward = "";
    public static ArrayList<CloseorderItems> closeorderItemsArrayList = new ArrayList<CloseorderItems>();
    public static ArrayList<WaiterDetails> waiterlist1 = new ArrayList<WaiterDetails>();
    public static ArrayList<CloseorderItems> closeorderItemsArrayList1 = new ArrayList<CloseorderItems>();
    Dbase db;
    String creditAccountno = "";
    String creditName = "";
    String TempBillAmount = "0";
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    //TextView title;
    String storeName;
    String pos;
    String cLimit = "";
    String mode = "";
    String bMode = "";
    String[] payments;
    String[] _BillMode;
    ArrayList<String> paymentMode;
    ArrayList<String> _billMode;


    Spinner sp_cardType;
    TextView colon;
    LinearLayout linear;
    EditText noEditText, otpEditText;
    Button otpButton, verifyButton;
    ArrayAdapter<String> spinnerAdapter;
    // String[] _cards =new String[ ]{"Select card Type","MEMBER CARD","MEMBER DUPL CARD","TEMP CARD","ROOM CARD","NEW ROOM CARD","CASH CARD","CLUB CARD","DEPENDENT CARD"};
    // String[] _cards = new String[]{"MEMBER CARD"};
    String[] _cards = new String[]{"MEMBER CARD", "ROOM CARD", "DEPENDENT CARD"};
    Boolean isReadCard = false;
    String selectedCard = "";
    String posid = "";
    String billType = "";
    RelativeLayout obstrucuterView;
    AudioManager audio;
    String contractorsId = "";
    String bill = "";
    String billMode = "";
    String userId = "", voidOT = "", isCashC, isCreditC, isDebitC, isChequeC;
    String _new = "";
    String bMod = "";
    String billdate = "";
    String paxcount = "0";
    public static String check = "";
    EditText etxt_discount;
    String discntpercent = "";
    String type = "";

    ArrayAdapter<String> spinnerDiscountAdapter;
    // String[] _cards =new String[ ]{"Select card Type","MEMBER CARD","MEMBER DUPL CARD","TEMP CARD","ROOM CARD","NEW ROOM CARD","CASH CARD","CLUB CARD","DEPENDENT CARD"};
    // String[] _cards = new String[]{"MEMBER CARD"};
    String[] _discount = new String[]{"Select Discount", "Discount %", "Discount value"};
    ArrayAdapter<String> spinnerPaymrntAdapter;
    // String[] _cards =new String[ ]{"Select car

    String OTCreditLimitNotRequired = "";

    //public  String pakistan="30 24 31 36 31 36 24 31 30 33 31 33 31 34 32 36 90 00 39 24 41 2D 30 30 30 33 24 68 48 51 47 2B 7A 46 90 00";
    private final BroadcastReceiver mHeadsetPlugReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                boolean plugged = (intent.getIntExtra("state", 0) == 1);
                /* Mute the audio output if the reader is unplugged. */
                mReader.setMute(!plugged);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.take_order_new);
        initPrefrence();
        actionBarSetup();
        // object creation
        db = new Dbase(BillingProfile.this);
        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audio.setStreamVolume(AudioManager.STREAM_MUSIC, audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        obstrucuterView = (RelativeLayout) findViewById(R.id.obstructor);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mReader = new AudioJackReader(mAudioManager, true);
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        fabmenu = (FloatingActionsMenu) findViewById(R.id.fab_menu);
        colon = (TextView) findViewById(R.id.colon);
        noEditText = findViewById(R.id.et_num);
        otpButton = findViewById(R.id.buttonOTP);
        otpEditText = findViewById(R.id.et_otp);
        linear = findViewById(R.id.linear);
        verifyButton = findViewById(R.id.buttonVerify);


        sp_cardType = (Spinner) findViewById(R.id.cardtype);
        //searchItemname = (SearchableSpinner) findViewById(R.id.spinnerfiler);
        //backround = (ImageView) findViewById(R.id.bg_image);
        corder = (Button) findViewById(R.id.corder);    // Button to close the order
        memberAn = (EditText) findViewById(R.id.editText_m1);   // to display the reader card
        order = (Button) findViewById(R.id.order);   // button lead to take order
        // tableNO = (TextView) findViewById(R.id.tableNo);   //display table No
        bot = (FloatingActionButton) findViewById(R.id.bot);
        bot.setEnabled(true);
        memberAn.setEnabled(false);
        sp_cardType.setVisibility(View.GONE);
        colon.setVisibility(View.GONE);

        try {
            if (db.getAllenCount() > 0) {
                db.clearAllen();
            }
            if (db.getModCount() > 0) {
                db.clearMods();
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }

        spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, _cards);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_cardType.setAdapter(spinnerAdapter);
        etxt_discount = (EditText) findViewById(R.id.etxt_discount);
        //Blist = db.getWaiterList();
        // arrayAdapterr = new ArrayAdapter<String>(BillingProfile.this, R.layout.support_simple_spinner_dropdown_item, Blist);
        //searchItemname.setAdapter(arrayAdapterr);
        //searchItemname.setLongClickable(false);
        // searchItemname.setOnSearchTextChangedListener();
        // searchItemname.setEnabled(true);
//        try {
//            db.deleteOT();
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }

//        try {
//            searchItemname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                    //  searchItemname.setEnabled(false);
//                    position = i;
//                    waiternamecode = adapterView.getItemAtPosition(i).toString();
//                    String array[] = waiternamecode.split("_");
//                    waiterid = db.getWaiterId(array[0].trim());
//                    View v = BillingProfile.this.getCurrentFocus();
//                    if (view != null && position != 0) {
//                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                    }
//
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> adapterView) {
//                    searchItemname.setEnabled(true);
//                }
//            });
//        } catch (Exception e) {
//            ShowMsg.showException(e, "setLogSettings", getApplicationContext());
//        }



        /* Register the headset plug receiver. */
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(mHeadsetPlugReceiver, filter);

        mProgress = new ProgressDialog(mContext);
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);


        String piccRfConfigString = "";
        if ((piccRfConfigString == null) || piccRfConfigString.equals("")
                || (toByteArray(piccRfConfigString, mPiccRfConfig) != 19)) {

            piccRfConfigString = "07 85 85 85 85 85 85 85 85 69 69 69 69 69 69 69 69 3F 3F";
            toByteArray(piccRfConfigString, mPiccRfConfig);
        }
        piccRfConfigString = toHexString(mPiccRfConfig);

        /* Set the PICC response APDU callback. */
        mReader.setOnPiccResponseApduAvailableListener(new OnPiccResponseApduAvailableListener());
        Bundle b = getIntent().getExtras();
        table = b.getString("tableNo");
        //  tableNO.setText("Table  " + table);
        tableId = b.getInt("tableId");
        cardType = b.getString("cardType");
        member = b.getString("accountType");
        Steward = b.getString("Steward");
        positionb = b.getInt("position");
        bMod = b.getString("billMode");
        billdate = b.getString("billdate");
        paxcount = b.getString("paxcnt");

        Log.d("memr1: ",member);
        if (Steward != null) {
            String array[] = Steward.split("_");
            waiterid = db.getWaiterId(array[0].trim());
        }

        Toast.makeText(BillingProfile.this, "waiterid : " + waiterid, Toast.LENGTH_SHORT).show();
        if (member.contains("#")) {
            _new = "old";
            String[] mm = member.split("#");

            if (cardType.equalsIgnoreCase("DEPENDENT CARD") || cardType.equalsIgnoreCase("DEPENDANT CARD")) {
                contractorsId = mm[5];

            } else {
                contractorsId = "0";
            }
            if(member.contains("*")){
                Log.d("ACdit : ",mm[0]);
              new  AsyncDependentCreditLimit().execute(mm[0]);
            }

            memberAn.setText(mm[0] + ":" + cardType);

            order.setVisibility(View.VISIBLE);
            corder.setVisibility(View.VISIBLE);  //TODO newly added
            etxt_discount.setVisibility(View.INVISIBLE);  //TODO newly added enable for discounts
            sp_cardType.setVisibility(View.GONE);
            colon.setVisibility(View.GONE);
            //searchItemname.setSelection(positionb);
            //searchItemname.setSelection(arrayAdapterr.getPosition(Steward));
            //searchItemname.setTitle(Steward);
            if (Steward != null) {
                String array[] = Steward.split("_");
                waiterid = db.getWaiterId(array[0].trim());
            }
            memberAn.setEnabled(false);
            memberAn.setFocusable(false);
            memberAn.setInputType(InputType.TYPE_NULL);

            //isReadCard=MainActivity.isSmartCard;
            isReadCard = sharedpreferences.getBoolean("CardRead", false);
            String isreader = sharedpreferences.getString("misReader", "");
            fabmenu.setVisibility(View.VISIBLE);
            if (isReadCard) {
                bot.setEnabled(true);
            } else {
                bot.setEnabled(false);
            }
            bot.setEnabled(false);
            //searchItemname.setVisibility(View.GONE);
            //Toast.makeText(TakeOrder.this,"member:#",Toast.LENGTH_LONG).show();

        } else if (member.equals("NCM")) {
            _new = "new";
            fabmenu.setVisibility(View.GONE);
            if (memberAn.getText().toString().equals("")) {
                order.setVisibility(View.GONE);
            }
            isReadCard = MainActivity.isSmartCard;
            if (isReadCard) {
                String isreader = sharedpreferences.getString("misReader", "");
                if (isreader.equals("False")) {
                    noCardReader();
                    isReadCard = false;
                } else {
                    readCard();
                }

            } else {
                noCardReader();
            }
        } else {
            // Toast.makeText(TakeOrder.this,"member:nohash",Toast.LENGTH_LONG).show();
            _new = "new";
            String memberid = b.getString("memberid");
            memberAn.setText(memberid);
            fabmenu.setVisibility(View.GONE);
            if (memberAn.getText().toString().equals("")) {
                order.setVisibility(View.GONE);
            }

            isReadCard = MainActivity.isSmartCard;
            if (isReadCard) {
                String isreader = sharedpreferences.getString("misReader", "");
                if (isreader.equals("False")) {
                    noCardReader();
                    isReadCard = false;
                } else {
                    readCard();
                }

            } else {
                noCardReader();
            }
            new AsyncCreditLimit().execute(memberid);
        }


        obstrucuterView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (obstrucuterView.getVisibility() == View.VISIBLE) {
                    fabmenu.collapse();
                    return true;
                }
                return false;
            }
        });

        otpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (noEditText.getText().toString().trim().isEmpty()) {
                    Toast.makeText(BillingProfile.this, "Please enter mobile number", Toast.LENGTH_SHORT).show();
                } else {
                    new CallApi().execute(noEditText.getText().toString().trim());
                }

            }
        });
        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (noEditText.getText().toString().trim().isEmpty()) {
                    Toast.makeText(BillingProfile.this, "Please enter mobile number", Toast.LENGTH_SHORT).show();
                } else if (otpEditText.getText().toString().trim().isEmpty()) {
                    Toast.makeText(BillingProfile.this, "Please enter OTP", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ct", selectedCard);
                    editor.apply();
                    cardType = selectedCard;
                    new CallVerifyAPi().execute(noEditText.getText().toString().trim(), otpEditText.getText().toString().trim());
                }

            }
        });
        fabmenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
          /*  if (obstrucuterView.getVisibility() == View.GONE)
                    obstrucuterView.setVisibility(View.VISIBLE);*/
            }

            @Override
            public void onMenuCollapsed() {
                if (obstrucuterView.getVisibility() == View.VISIBLE)
                    obstrucuterView.setVisibility(View.GONE);
            }
        });


        // *****************************  TAKE ORDER  BUTTON ******************************//
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String memeberAnumber = memberAn.getText().toString();
                isInternetPresent = cd.isConnectingToInternet();
                String waiter = "";
                SharedPreferences.Editor editor = sharedpreferences.edit();
                if (_new.equals("new")) {
                    // new card need to choose the billmode
                    if (isReadCard) {
                        if (memeberAnumber.equals("")) {
                            Toast.makeText(BillingProfile.this, "Read Member Card", Toast.LENGTH_LONG).show();
                        } else {
                            String ss[] = memeberAnumber.split(":");
                            editor.putString("ct", ss[1]);
                            editor.apply();
                            if (member.contains("#")) {
                                String ss1[] = member.split("#");
                                if (isInternetPresent) {
                                    // new AsyncCreditLimit().execute(ss1[2]);
                                    AlertBillmode(ss1[2], ss[1]);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                if (isInternetPresent) {
                                    //  new AsyncCreditLimit().execute(ss[0]);
                                    AlertBillmode(ss[0], ss[1]);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }

                            }

                        }
                    } else {
                        if (memeberAnumber.equals("")) {
                            Toast.makeText(BillingProfile.this, "Enter Member account Number !", Toast.LENGTH_LONG).show();
                        } else {
                            new CallProfileAPI().execute(memeberAnumber, "2", "", "", "", "", "", "");

                        }
                    }

                } else {
                    if (isReadCard) {
                        waiter = "";
                        if (memeberAnumber.equals("")) {
                            Toast.makeText(BillingProfile.this, "Read Member Card", Toast.LENGTH_LONG).show();
                        } else {
                            String ss[] = memeberAnumber.split(":");
                            editor.putString("ct", ss[1]);
                            editor.commit();
                            if (member.contains("#")) {
                                String ss1[] = member.split("#");
                                if (isInternetPresent) {
                                    new AsyncCreditLimit().execute(ss1[2], bMod);
                                    //  AlertBillmode(ss1[2],ss[1]);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                if (isInternetPresent) {
                                    new AsyncCreditLimit().execute(ss[0], bMod);
                                    // AlertBillmode(ss[0],ss[1]);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    } else {
                        if (memeberAnumber.equals("")) {
                            Toast.makeText(BillingProfile.this, "Enter Member account Number !", Toast.LENGTH_LONG).show();
                        } else if (selectedCard.contains("Select")) {
                            Toast.makeText(BillingProfile.this, "Please choose the cardtype !", Toast.LENGTH_LONG).show();
                        } else if (selectedCard.equals("ROOM CARD") && !(memeberAnumber.contains("RM") || memeberAnumber.contains("rm"))) {
                            Toast.makeText(BillingProfile.this, "Enter valid Room account Number !", Toast.LENGTH_LONG).show();
                        } else {
                            third();
//                            new CallProfileAPI().execute(memeberAnumber, "3", "", "", "", "", "", "");
                        }
                    }
                }
            }
        });

        //**************************** CARD READER BUTTON  **********************************//
        bot.setVisibility(View.GONE);
        //  takeOrder.setOnClickListener(new View.OnClickListener() {
        bot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audio.setStreamVolume(AudioManager.STREAM_MUSIC, audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
                mReader.piccPowerOff();
                mReader.sleep();
                finalValue = "";
                manoch = 0;
                k = 0;
                memberAn.setText("");
                mPiccResponseApdu = null;
                mResponseEvent = null;
                mReader.start();
                mResponseEvent = new Object();
                overAll = 0;
                if (!checkResetVolume()) {
                } else {
                    mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    mProgress.setMessage("Reading Card please wait.. ");
                    mProgress.show();
                    new Thread(new Transmit()).start();
                }
            }
        });


        //********************************      CLOSE ORDER BUTTON **********************************//


        corder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    final Dialog dialog = new Dialog(BillingProfile.this);
                    dialog.getWindow();
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dailog_diss);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    }
                    Button button = dialog.findViewById(R.id.buttonOK);
                    final EditText qtyEditText = dialog.findViewById(R.id.edit_textQty);
                    qtyEditText.setVisibility(View.GONE);

                    final RadioGroup radioSexGroup = dialog.findViewById(R.id.radioGroup);
                    Button button1 = dialog.findViewById(R.id.buttonSkip);
                    final Spinner spinnerDiscount = dialog.findViewById(R.id.spinnerDiscount);
                    final Spinner spinnerPayment = dialog.findViewById(R.id.spinnerPayment);
                    spinnerDiscountAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, _discount);
                    spinnerDiscountAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerDiscount.setAdapter(spinnerDiscountAdapter);
                    spinnerDiscount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            qtyEditText.setVisibility(View.VISIBLE);

                            String selectedDiscount = spinnerDiscount.getItemAtPosition(position).toString();
                            if (!selectedDiscount.equalsIgnoreCase("Select Discount")) {
                                if (selectedDiscount.contains("%")) {
                                    type = "P";

                                } else {
                                    type = "V";

                                }
                            } else {
                                qtyEditText.setVisibility(View.GONE);

                                type = "";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            type = "";
                            qtyEditText.setVisibility(View.GONE);
                        }
                    });

                    paymentMode = new ArrayList<>();
                    paymentMode.add("Select Payment Mode");
                    if (bill.equals("Regular")) {
                        if (isCashC.equals("true") || isCashC.equals("1")) {
                            paymentMode.add("Cash");
                        }
                     /*   if(!OTCreditLimitNotRequired.equalsIgnoreCase("true") && !cardType.equals("MEMBER CARD")) {
                            if (isDebitC.equals("true") || isDebitC.equals("1")) {
                                paymentMode.add("Account");
                            }
                        } */

                        //if(!OTCreditLimitNotRequired.equalsIgnoreCase("true") && !cardType.equals("MEMBER CARD")) {
                            if (isDebitC.equals("true") || isDebitC.equals("1")) {
                                paymentMode.add("Account");
                            }
                       // }


                        if (isChequeC.equals("true") || isChequeC.equals("1")) {
                            paymentMode.add("Cheque");
                        }
                        if (isCreditC.equals("true") || isCreditC.equals("1")) {
                            paymentMode.add("Credit");
                        }

                        if (!paymentMode.isEmpty()) {
                            payments = new String[paymentMode.size()];
                            payments = paymentMode.toArray(payments);
                        }
                    }

                    spinnerPaymrntAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, paymentMode);
                    spinnerPaymrntAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerPayment.setAdapter(spinnerPaymrntAdapter);
                    spinnerPayment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mode = spinnerPayment.getItemAtPosition(position).toString();
                            if (mode.equalsIgnoreCase("Select Payment Mode")) {
                                Toast.makeText(BillingProfile.this, "Select Payment Mode", Toast.LENGTH_LONG).show();

                            } else {
                                if (type.equalsIgnoreCase("")) {
                                    try {

                                        String memeberAnumber = memberAn.getText().toString();
                                        Discountpercent = etxt_discount.getText().toString().trim();
                                        isInternetPresent = cd.isConnectingToInternet();
                                        if (memeberAnumber.equals("")) {
                                            Toast.makeText(BillingProfile.this, "Enter member account Number", Toast.LENGTH_LONG).show();
                                        } else {
                                            String ss[] = memeberAnumber.split(":");
                                            if (member.contains("#")) {
                                                String ss1[] = member.split("#");
                                                if (isInternetPresent) {
                                                    String d = ss1.length == 5 ? ss1[3] : "NMC";
                                                    //          AlertPayment(ss1[1], "0", ss[1], isCashCardFilled, d, Discountpercent, type);
                                                    new AsyncCloseOrder().execute(ss1[1], "0", mode, discntpercent, type);
                                                    //  new AsyncCloseOrder().execute(ss1[1], "0");
                                                } else {
                                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                // Toast.makeText(TakeOrder.this,""+ss[0],Toast.LENGTH_LONG).show();
                                                if (isInternetPresent) {
                                                    //   AlertPayment(ss[0], "0", ss[1], isCashCardFilled, "", Discountpercent, type);

                                                    new AsyncCloseOrder().execute(ss[0], "0", mode, discntpercent, type);


                                                    //new AsyncCloseOrder().execute(ss[0], "0");
                                                } else {
                                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    //todo else condition
                                    try {

                                        if (qtyEditText.getText().toString().trim().isEmpty()) {
                                            Toast.makeText(BillingProfile.this, "Please enter Discount percentage", Toast.LENGTH_SHORT).show();

                                            spinnerPayment.setSelection(0);

                                        } else {

                                            String memeberAnumber = memberAn.getText().toString();
                                            Discountpercent = qtyEditText.getText().toString();
                                            isInternetPresent = cd.isConnectingToInternet();
                                            if (memeberAnumber.equals("")) {
                                                Toast.makeText(BillingProfile.this, "Enter member account Number", Toast.LENGTH_LONG).show();
                                            } else if (!Discountpercent.equals("")) {
                                                try {
                                                    int discnt = Integer.parseInt(Discountpercent);
                                                    if (discnt < 0 || discnt > 100) {
                                                        Toast.makeText(BillingProfile.this, "Enter Valid Discount Percentage ", Toast.LENGTH_LONG).show();
                                                    } else {
                                                        String ss[] = memeberAnumber.split(":");
                                                        if (member.contains("#")) {
                                                            String ss1[] = member.split("#");
                                                            // Toast.makeText(TakeOrder.this,""+ss1[1],Toast.LENGTH_LONG).show();
                                                            if (isInternetPresent) {
                                                                String d = ss1.length == 5 ? ss1[3] : "NMC";


                                                                new AsyncCloseOrder().execute(ss1[1], "0", mode, Discountpercent, type);


                                                                //    AlertPayment(ss1[1], "0", ss[1], isCashCardFilled, d, Discountpercent, type);
                                                                //  new AsyncCloseOrder().execute(ss1[1], "0");
                                                            } else {
                                                                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                                            }

                                                        } else {
                                                            // Toast.makeText(TakeOrder.this,""+ss[0],Toast.LENGTH_LONG).show();
                                                            if (isInternetPresent) {
                                                                //       AlertPayment(ss[0], "0", ss[1], isCashCardFilled, "", Discountpercent, type);
                                                                new AsyncCloseOrder().execute(ss[0], "0", mode, Discountpercent, type);
                                                                //new AsyncCloseOrder().execute(ss[0], "0");
                                                            } else {

                                                                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                                            }

                                                        }

                                                    }
                                                } catch (Exception e) {
                                                    Toast.makeText(BillingProfile.this, e + "", Toast.LENGTH_LONG).show();
                                                }

                                            } else {
                                                String ss[] = memeberAnumber.split(":");
                                                if (member.contains("#")) {
                                                    String ss1[] = member.split("#");
                                                    // Toast.makeText(TakeOrder.this,""+ss1[1],Toast.LENGTH_LONG).show();
                                                    if (isInternetPresent) {
                                                        String d = ss1.length == 5 ? ss1[3] : "NMC";
                                                        //     AlertPayment(ss1[1], "0", ss[1], isCashCardFilled, d, Discountpercent, type);
                                                        new AsyncCloseOrder().execute(ss1[1], "0", mode, Discountpercent, type);

                                                        //  new AsyncCloseOrder().execute(ss1[1], "0");
                                                    } else {
                                                        Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                                    }

                                                } else {
                                                    // Toast.makeText(TakeOrder.this,""+ss[0],Toast.LENGTH_LONG).show();
                                                    if (isInternetPresent) {
                                                        //        AlertPayment(ss[0], "0", ss[1], isCashCardFilled, "", Discountpercent, type);
                                                        new AsyncCloseOrder().execute(ss[0], "0", mode, Discountpercent, type);


                                                        //new AsyncCloseOrder().execute(ss[0], "0");
                                                    } else {
                                                        Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                                    }

                                                }

                                            }

                                        }
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    button1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                int selectedId = radioSexGroup.getCheckedRadioButtonId();
                                RadioButton radioSexButton = dialog.findViewById(selectedId);
                                String t = radioSexButton.getText().toString();
                                String type;
                                if (t.contains("%")) {
                                    type = "P";
                                } else {
                                    type = "V";
                                }
                                String memeberAnumber = memberAn.getText().toString();
                                Discountpercent = etxt_discount.getText().toString().trim();
                                isInternetPresent = cd.isConnectingToInternet();
                                dialog.dismiss();
                                if (memeberAnumber.equals("")) {
                                    Toast.makeText(BillingProfile.this, "Enter member account Number", Toast.LENGTH_LONG).show();
                                } else {
                                    String ss[] = memeberAnumber.split(":");
                                    if (member.contains("#")) {
                                        String ss1[] = member.split("#");
                                        // Toast.makeText(TakeOrder.this,""+ss1[1],Toast.LENGTH_LONG).show();
                                        if (isInternetPresent) {
                                            String d = ss1.length == 5 ? ss1[3] : "NMC";
                                            AlertPayment(ss1[1], "0", ss[1], isCashCardFilled, d, Discountpercent, type);
                                            //  new AsyncCloseOrder().execute(ss1[1], "0");
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        // Toast.makeText(TakeOrder.this,""+ss[0],Toast.LENGTH_LONG).show();
                                        if (isInternetPresent) {
                                            AlertPayment(ss[0], "0", ss[1], isCashCardFilled, "", Discountpercent, type);
                                            //new AsyncCloseOrder().execute(ss[0], "0");
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                }
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    button.setOnClickListener(new View.OnClickListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onClick(View v) {
                            try {

                                if (qtyEditText.getText().toString().trim().isEmpty()) {
                                    Toast.makeText(BillingProfile.this, "Please enter Discount percentage", Toast.LENGTH_SHORT).show();
                                } else {
                                    int selectedId = radioSexGroup.getCheckedRadioButtonId();
                                    RadioButton radioSexButton = dialog.findViewById(selectedId);
                                    String t = radioSexButton.getText().toString();
                                    String type;
                                    if (t.contains("%")) {
                                        type = "P";
                                    } else {
                                        type = "V";
                                    }
                                    String memeberAnumber = memberAn.getText().toString();
                                    Discountpercent = qtyEditText.getText().toString();
                                    isInternetPresent = cd.isConnectingToInternet();
                                    dialog.dismiss();
                                    if (memeberAnumber.equals("")) {
                                        Toast.makeText(BillingProfile.this, "Enter member account Number", Toast.LENGTH_LONG).show();
                                    } else if (!Discountpercent.equals("")) {
                                        try {
                                            int discnt = Integer.parseInt(Discountpercent);
                                            if (discnt < 0 || discnt > 100) {
                                                Toast.makeText(BillingProfile.this, "Enter Valid Discount Percentage ", Toast.LENGTH_LONG).show();
                                            } else {
                                                String ss[] = memeberAnumber.split(":");
                                                if (member.contains("#")) {
                                                    String ss1[] = member.split("#");
                                                    // Toast.makeText(TakeOrder.this,""+ss1[1],Toast.LENGTH_LONG).show();
                                                    if (isInternetPresent) {
                                                        String d = ss1.length == 5 ? ss1[3] : "NMC";
                                                        AlertPayment(ss1[1], "0", ss[1], isCashCardFilled, d, Discountpercent, type);
                                                        //  new AsyncCloseOrder().execute(ss1[1], "0");
                                                    } else {
                                                        Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                                    }

                                                } else {
                                                    // Toast.makeText(TakeOrder.this,""+ss[0],Toast.LENGTH_LONG).show();
                                                    if (isInternetPresent) {
                                                        AlertPayment(ss[0], "0", ss[1], isCashCardFilled, "", Discountpercent, type);
                                                        //new AsyncCloseOrder().execute(ss[0], "0");
                                                    } else {
                                                        Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                                    }

                                                }

                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(BillingProfile.this, e + "", Toast.LENGTH_LONG).show();
                                        }

                                    } else {
                                        String ss[] = memeberAnumber.split(":");
                                        if (member.contains("#")) {
                                            String ss1[] = member.split("#");
                                            // Toast.makeText(TakeOrder.this,""+ss1[1],Toast.LENGTH_LONG).show();
                                            if (isInternetPresent) {
                                                String d = ss1.length == 5 ? ss1[3] : "NMC";
                                                AlertPayment(ss1[1], "0", ss[1], isCashCardFilled, d, Discountpercent, type);
                                                //  new AsyncCloseOrder().execute(ss1[1], "0");
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                            }

                                        } else {
                                            // Toast.makeText(TakeOrder.this,""+ss[0],Toast.LENGTH_LONG).show();
                                            if (isInternetPresent) {
                                                AlertPayment(ss[0], "0", ss[1], isCashCardFilled, "", Discountpercent, type);
                                                //new AsyncCloseOrder().execute(ss[0], "0");
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                            }

                                        }

                                    }

                                }
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    dialog.show();

                } catch (Throwable e) {
                    e.printStackTrace();
                }

                //bottomSheet.setVisibility(View.VISIBLE);
                // bottomSheet.setMinimumHeight(100);
                // bottomSheet.showWithSheetView(LayoutInflater.from(TakeOrder.this).inflate(R.layout.sheet, bottomSheet, false));
            }
        });

        // *******************************  CANCEL ORDRE (FAB BUTTON )***************************//

      /*  FloatingActionButton otReprint = new FloatingActionButton(this);
        otReprint.setIcon(R.drawable.report2);
        otReprint.setTitle("OT Reprint");

        otReprint.setColorNormal(Color.parseColor("#e75b1e"));
        fabmenu.addButton(otReprint);

        otReprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                fabmenu.collapse();
                Intent i = new Intent(TakeOrder.this, Ot_Acc_List.class);
                i.putExtra("mpage","1");
                startActivity(i);
            }
        });
*/
        if (!billType.equals("BAK") || billType.equals("")) {
            FloatingActionButton otReprint = new FloatingActionButton(this);
            otReprint.setIcon(R.drawable.report2);
            otReprint.setTitle("OT Reprint");
            otReprint.setColorNormal(Color.parseColor("#e75b1e"));
            fabmenu.addButton(otReprint);
            otReprint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fabmenu.collapse();
                   /* Intent i = new Intent(TakeOrder.this, Ot_Acc_List.class);
                    i.putExtra("mpage","1");
                    startActivity(i);*/
                    String memeberAnumber = memberAn.getText().toString();
                    isInternetPresent = cd.isConnectingToInternet();
                    if (memeberAnumber.equals("")) {
                        Toast.makeText(BillingProfile.this, "Enter member account Number", Toast.LENGTH_LONG).show();
                    } else {
                        String ss[] = memeberAnumber.split(":");
                        if (member.contains("#")) {
                            String ss1[] = member.split("#");
                            if (isInternetPresent) {
                                //     new AsyncCloseOrder().execute(ss1[1], "1", "");

                                new AsyncCloseOrder1().execute(ss1[1], "1", "");
                            } else {
                                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            // Toast.makeText(TakeOrder.this,""+ss[0],Toast.LENGTH_LONG).show();
                            if (isInternetPresent) {
                                //     new AsyncCloseOrder().execute(ss[0], "1", "");
                                new AsyncCloseOrder1().execute(ss[0], "1", "");
                            } else {
                                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
        }

//        if (cardType.equals("")) {
//
//        }
//        else if (billType.equals("BOT")){
        FloatingActionButton transferOrder = new FloatingActionButton(this);
        transferOrder.setIcon(R.drawable.return_purchase);
        transferOrder.setTitle("OT Transfer");
        transferOrder.setColorNormal(Color.parseColor("#e75b1e"));
//        fabmenu.addButton(transferOrder);
        transferOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*if (member.contains("#")) {
                    String[] mebid = member.split("#");

                    if(mebid[0].contains("RM")||mebid[0].contains("rm")){
                        Toast.makeText(BillingProfile.this, "OT Transfer Not Allowed Room card ", Toast.LENGTH_SHORT).show();
                    }else {
                        Intent intent = new Intent(BillingProfile.this, Ottransfer.class);
                        intent.putExtra("billmode", billMode);
                        intent.putExtra("bill", bill);
                        intent.putExtra("memberid", mebid[1]);
                        startActivity(intent);
                    }
                }*/

                String memeberAnumber = memberAn.getText().toString();
                if (memeberAnumber.contains(":")) {
                    String ss[] = memeberAnumber.split(":");
                    if (ss[1].contains("ROOM CARD")) {
                        Toast.makeText(BillingProfile.this, "OT Transfer Not Allowed Room card ", Toast.LENGTH_SHORT).show();
                    } else {
                        if (member.contains("#")) {
                            String[] mebid = member.split("#");
                            Intent intent = new Intent(BillingProfile.this, Ottransfer.class);
                            intent.putExtra("billmode", billMode);
                            intent.putExtra("bill", bill);
                            intent.putExtra("memberid", mebid[1]);
                            startActivity(intent);

                        }
                    }
                } else {
                    if (member.contains("#")) {
                        String[] mebid = member.split("#");


                        Intent intent = new Intent(BillingProfile.this, Ottransfer.class);
                        intent.putExtra("billmode", billMode);
                        intent.putExtra("bill", bill);
                        intent.putExtra("memberid", mebid[1]);
                        startActivity(intent);

                    }
                }
            }
        });

        //  todo enable for ooty club
        FloatingActionButton cancelOrder = new FloatingActionButton(this);
        cancelOrder.setIcon(R.drawable.return_purchase);
        cancelOrder.setTitle("Cancel Order");
        cancelOrder.setColorNormal(Color.parseColor("#e75b1e"));
        if (voidOT.equalsIgnoreCase("true")) {
            fabmenu.addButton(cancelOrder);
        }
        cancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String memeberAnumber = memberAn.getText().toString();
                isInternetPresent = cd.isConnectingToInternet();
                if (memeberAnumber.equals("")) {
                    Toast.makeText(BillingProfile.this, "Enter member account Number", Toast.LENGTH_LONG).show();
                } else {
                    String ss[] = memeberAnumber.split(":");
                    if (member.contains("#")) {
                        String ss1[] = member.split("#");
                        //  Toast.makeText(TakeOrder.this,""+ss1[1],Toast.LENGTH_LONG).show();
                        if (isInternetPresent) {
                            new AsyncCloseOrder().execute(ss1[1], "1", bMod);
                        } else {
                            Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        // Toast.makeText(TakeOrder.this,""+ss[0],Toast.LENGTH_LONG).show();
                        if (isInternetPresent) {
                            new AsyncCloseOrder().execute(ss[0], "1", bMod);
                        } else {
                            Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

    private void third() {
        String memeberAnumber = memberAn.getText().toString();
        String mem = memeberAnumber + ":" + selectedCard;
        String ss[] = mem.split(":");
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("ct", ss[1]);
        editor.commit();
        String ss2[];
        cardType = ss[1];
        if (member.contains("#")) {
            String ss1[] = member.split("#");
            if (isInternetPresent) {
                //27-03-2021
                new AsyncCreditLimit().execute(ss1[2], bMod);
                // AlertBillmode(ss1[2],selectedCard);
            } else {
                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (isInternetPresent) {
                new AsyncCreditLimit().execute(ss[0], bMod);
                // AlertBillmode(ss[0],selectedCard);
            } else {
                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initPrefrence() {
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        pos = sharedpreferences.getString("StoreName", "");
        billType = sharedpreferences.getString("billType", "");
        posid = sharedpreferences.getString("storeId", "");
        cLimit = sharedpreferences.getString("creditLimit", "");
        bill = sharedpreferences.getString("Bill", "");
        billMode = sharedpreferences.getString("BillMode", "");

        userId = sharedpreferences.getString("userId", "");
        isCashC = sharedpreferences.getString("IsCashC", "");
        isCreditC = sharedpreferences.getString("IsCreditC", "");
        isDebitC = sharedpreferences.getString("IsDebitC", "");
        isChequeC = sharedpreferences.getString("IsChequeC", "");
        voidOT = sharedpreferences.getString("voidOT", "");



        OTCreditLimitNotRequired = sharedpreferences.getString("OTCreditLimitNotRequired", "");

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("" + pos + "  -  " + bill);
            ab.setElevation(0);
            ab.setDisplayHomeAsUpEnabled(true);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        String Dashboard = sharedpreferences.getString("Dashboard", "");
        if (Dashboard.equals("")) {
            Dashboard = "Dashboard_new";
        }
        Intent i = new Intent(BillingProfile.this, Accountlist.class);
        i.putExtra("waiterid", waiterid);
        i.putExtra("position", position);
        // i.putExtra("from","Dashboard_new");
        i.putExtra("from", Dashboard);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                String Dashboard = sharedpreferences.getString("Dashboard", "");
                if (Dashboard.equals("")) {
                    Dashboard = "Dashboard_new";
                }
                Intent i = new Intent(BillingProfile.this, Accountlist.class);
                i.putExtra("waiterid", waiterid);
                i.putExtra("position", position);
                //i.putExtra("from","Dashboard_new");
                i.putExtra("from", Dashboard);
                startActivity(i);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void noCardReader() {
        sp_cardType.setVisibility(View.VISIBLE);
        colon.setVisibility(View.VISIBLE);
        bot.setEnabled(false);
        memberAn.setEnabled(true);
        // memberAn.setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL);
        order.setVisibility(View.VISIBLE);
        corder.setVisibility(View.GONE);
        etxt_discount.setVisibility(View.GONE);

        sp_cardType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCard = spinnerAdapter.getItem(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void readCard() {
        sp_cardType.setVisibility(View.GONE);
        colon.setVisibility(View.GONE);
        bot.setEnabled(true);
        memberAn.setEnabled(false);
        memberAn.setInputType(InputType.TYPE_NULL);
        corder.setVisibility(View.GONE);
        etxt_discount.setVisibility(View.GONE);

    }

    private String AlertBillmode(final String Account, String cardType) {
        _billMode = new ArrayList<String>();
        if (bill.equals("Regular")) {
            if (cardType.equals("MEMBER CARD") || cardType.equals("CLUB CARD") || cardType.equals("ROOM CARD") || cardType.equals("DEPENDENT CARD") || cardType.equals("MEMBER DUPL CARD") || cardType.equals("SMART CARD")) {

                //new AsyncCreditLimit().execute(Account,"Account");
                return "Account";

            } else if (cardType.equals("CASH CARD")) {

                if (MainActivity.cashCard.equals("true")) {
                    // new AsyncCreditLimit().execute(Account,"Account");
                    return "Account";
                } else {
                    //new AsyncCreditLimit().execute(Account,"Cash");
                    return "Cash";
                }
            }
        } else if (bill.equals("Party Billing")) {
            if (cardType.equals("MEMBER CARD")) {
                //new AsyncCreditLimit().execute(Account,"Account");
                return "Account";
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Party Billing ", Toast.LENGTH_SHORT).show();
            }
        } else if (bill.equals("Direct Party Billing")) {
            if (cardType.equals("MEMBER CARD")) {
                //new AsyncCreditLimit().execute(Account,"Account");
                return "Account";
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Direct Party Billing ", Toast.LENGTH_SHORT).show();
            }
        } else if (bill.equals("Compliment")) {
            if (cardType.equals("MEMBER CARD") || cardType.equals("ROOM CARD")) {
                //new AsyncCreditLimit().execute(Account,"Complimentary");
                return "Complimentary";
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Compliment Billing ", Toast.LENGTH_SHORT).show();
            }

        } else if (bill.equals("coupon")) {
            if (cardType.equals("MEMBER CARD")) {
                //new AsyncCreditLimit().execute(Account,"Coupon");
                return "Coupon";
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Coupon Billing ", Toast.LENGTH_SHORT).show();
            }
        }

        return "";

    }

    // payment mode for close order
    private void AlertPayment(final String s, String s1, String cardType, String isCashCardFilled, String membertype, String discntper, final String type) {
        discntpercent = discntper;
        paymentMode = new ArrayList<>();
        if (bill.equals("Regular")) {
            if (isCashC.equals("true") || isCashC.equals("1")) {
                paymentMode.add("Cash");
            }
            if (isDebitC.equals("true") || isDebitC.equals("1")) {
                paymentMode.add("Account");
            }
            if (isChequeC.equals("true") || isChequeC.equals("1")) {
                paymentMode.add("Cheque");
            }
            if (isCreditC.equals("true") || isCreditC.equals("1")) {
                paymentMode.add("Credit");
            }

            if (!paymentMode.isEmpty()) {
                payments = new String[paymentMode.size()];
                payments = paymentMode.toArray(payments);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Select the Payment Mode");
            builder.setItems(payments, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int _paymentMode) {
                    mode = paymentMode.get(_paymentMode);
                    new AsyncCloseOrder().execute(s, "0", mode, discntpercent, type);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();

        } else if (bill.equals("Party Billing")) {
            if (cardType.equals("PARTY CARD")) {
                new AsyncCloseOrder().execute(s, "0", "Account");
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Party Billing ", Toast.LENGTH_SHORT).show();
            }
        } else if (bill.equals("Direct Party Billing")) {
            if (cardType.equals("DIRECTPARTY CARD")) {
                new AsyncCloseOrder().execute(s, "0", "Account", discntpercent, type);
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Direct Party Billing ", Toast.LENGTH_SHORT).show();
            }
        } else if (bill.equals("Compliment")) {
            if (cardType.equals("MEMBER CARD") || cardType.equals("ROOM CARD")) {
                new AsyncCloseOrder().execute(s, "0", "Complimentary", discntpercent, type);
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Compliment Billing ", Toast.LENGTH_SHORT).show();
            }

        } else if (bill.equals("coupon")) {
            if (cardType.equals("MEMBER CARD")) {
                new AsyncCloseOrder().execute(s, "0", "Coupon", discntpercent, type);

            } else {
                Toast.makeText(this, "This card is not applicable for coupon ", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void second() {
        //   String mem = memeberAnumber + ":" + selectedCard;
        String memeberAnumber = memberAn.getText().toString();
        String mem = memeberAnumber + ":" + selectedCard;
        String ss[] = mem.split(":");
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("ct", ss[1]);
        editor.commit();
        String ss2[];
        cardType = ss[1];
        if (member.contains("#")) {
            String ss1[] = member.split("#");
            if (isInternetPresent) {
                // new AsyncCreditLimit().execute(ss1[2]);
                AlertBillmode(ss1[2], selectedCard);
            } else {
                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
            }

        } else if (member.equals("NCM")) {
            //   new AsyncCreditLimit().execute(ss[0]);//with out card first time
            if (cardType.equals("ROOM CARD") && (memeberAnumber.contains("RM") || memeberAnumber.contains("rm"))) {
                new AsyncCreditLimit().execute(ss[0]);//with out card first time
            } else if (cardType.equals("MEMBER CARD")) {
                new AsyncCreditLimit().execute(ss[0]);//with out card first time
            } else if (cardType.equals("DEPENDENT CARD")) {
                new AsyncCreditLimit().execute(ss[0]);//with out card first time
            } else {
                Toast.makeText(mContext, "Please Enter Valid Room Number ", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (isInternetPresent) {
                //new AsyncCreditLimit().execute(ss[0]);
                AlertBillmode(ss[0], selectedCard);
            } else {
                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @SuppressLint("SetTextI18n")
    public void showProfile(MemberMainDto memberMainDto, final String typess, final String accesstype, final String bookingNumber, final String debitbal, final String memberId, final String bookingId, final String bmod) {
        try {
            final Dialog dialog = new Dialog(BillingProfile.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_member_display);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
            TextView name = dialog.findViewById(R.id.name);
            ImageView image = dialog.findViewById(R.id.image);
            ImageView image_view = dialog.findViewById(R.id.image_view);
            TextView type = dialog.findViewById(R.id.type);
            TextView doj = dialog.findViewById(R.id.doj);
            TextView dob = dialog.findViewById(R.id.dob);
            TextView owner = dialog.findViewById(R.id.owner);
            TextView types = dialog.findViewById(R.id.types);
            TextView status = dialog.findViewById(R.id.status);
            TextView remarks = dialog.findViewById(R.id.remarks);
            RelativeLayout relativeLayout = dialog.findViewById(R.id.table_scemelist);
            RelativeLayout memberguestcnt = dialog.findViewById(R.id.memberguestcnt);
            if (memberMainDto.getMemberDetailsList().size() > 0) {
                try {
                    MemberDetails memberDetails = memberMainDto.getMemberDetailsList().get(0);
                    name.setText(memberDetails.getAccountNumber() + "-" + memberDetails.getFirstName() + " " + memberDetails.getMiddleName() + " " + memberDetails.getLastName());
                    type.setText(memberDetails.getMemberType());
                    types.setText(memberDetails.getTypes());
                    dob.setText(memberDetails.getDateOfBirth());
                    doj.setText(memberDetails.getConfirmationDate());
                    owner.setText(memberDetails.getOwnerShipType());
                    status.setText(memberDetails.getStatus());
                    remarks.setText(memberDetails.getRemarks());

                    if (!memberDetails.getPhoto().isEmpty()) {
                        byte[] imageAsBytes = Base64.decode(memberDetails.getPhoto().getBytes(), Base64.DEFAULT);
                        image.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
            image_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (typess.equals("2")) {
                        dialog.dismiss();
                        second();
                    } else if (typess.equals("3")) {
                        dialog.dismiss();
                        third();
                    } else {
                        dialog.dismiss();
                        Intents(accesstype, bookingNumber, debitbal, memberId, bookingId, bmod);
                    }
                }
            });
            relativeLayout.removeAllViews();
            relativeLayout.addView(new DependentTable(BillingProfile.this, memberMainDto.getDependentDetailsList()));
            memberguestcnt.removeAllViews();
            memberguestcnt.addView(new MemberGuestTable(BillingProfile.this, memberMainDto.getMemberItemGuestList()));

            dialog.show();

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    // *****************  FOR CLOSE AND CANCEL ORDER ***************************//

    protected class AsyncCloseOrder extends AsyncTask<String, Void, String> {
        String status = "";
        String storeId = sharedpreferences.getString("storeId", "");
        String deviceid = sharedpreferences.getString("deviceId", "");
        String userId = sharedpreferences.getString("userId", "");
        String pageType = "";
        float discntper = 0f;
        String mstoreid = sharedpreferences.getString("mstoreId", "");
        JSONObject jsonObject;
        String billtype = sharedpreferences.getString("BillMode", "");
        String Error = "";
        String modepay = "";
        String type = "";

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(BillingProfile.this);
            try {
                modepay = params[2];
                try {
                    discntper = Float.parseFloat(params[3]);
                } catch (Exception e) {
                    discntper = 0f;
                }
                try {
                    type = params[4];
                } catch (Exception e) {
                    type = "P";
                }

                Calendar c = Calendar.getInstance();
                // SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());

//TODO monica
                  /*  if (!billdate.equals(""))
                    {
                        Date dateIn = null;
                        dateIn= new SimpleDateFormat("dd-MM-yyyy").parse(billdate);
                        String formatteddateIn = new SimpleDateFormat("yyyy-MM-dd ").format(dateIn);
                        formattedDate=  formatteddateIn;
                    }
                    else {
                        formattedDate=formattedDate;
                    }*/
                if (billdate.contains(" ")) {
                    billdate = billdate.split(" ")[0];
                }
                if (!billdate.equals("")) {
                    Date dateIn = null;
                    if (billdate.contains("/")) {
                        dateIn = new SimpleDateFormat("MM/dd/yyyy").parse(billdate);
                    } else {
                        dateIn = new SimpleDateFormat("yyyy-MM-dd").parse(billdate);
                    }
                    String formatteddateIn = new SimpleDateFormat("yyyy-MM-dd").format(dateIn);
                    formattedDate = formatteddateIn;
                }
                closeorderItemsArrayList.clear();
                if (bill.equals("coupon")) {
                    //   jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype);
                    if (discntper == 0f) {
                        jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype, 0, type, waiterid);

                    } else {
                        jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype, discntper, type, waiterid);
                    }

                } else if (bill.equals("Party Billing")) {
                    //  jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, "PARTY CARD", pos, params[2], contractorsId, mstoreid, billtype);
                    if (discntper == 0f) {
                        jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype, 0, type, waiterid);

                    } else {
                        jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype, discntper, type, waiterid);

                    }
                } else if (bill.equals("Direct Party Billing")) {
                    //    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, "DIRECTPARTY CARD", pos, params[2], contractorsId, mstoreid, billtype);
                    if (discntper == 0f) {
                        jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype, 0, type, waiterid);

                    } else {
                        jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype, discntper, type, waiterid);

                    }
                } else {
                    //     jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype);
                    if (discntper == 0f) {
                        jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype, 0, type, waiterid);

                    } else {
                        jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype, discntper, type, waiterid);

                    }
                }

                JSONArray array = jsonObject.getJSONArray("Value");
                CloseorderItems cl = null;
                pageType = params[1];
                db.clearBills();
                String WaiterName = "";
                if (array.length() > 0) {
                    for (int i = 0; i < array.length(); i++) {

                        cl = new CloseorderItems();
                        JSONObject object = array.getJSONObject(i);

                        Error = object.optString("Error");
                        if (Error.equals("")) {
                            status = jsonObject.toString();

                            if (i == 0) {
                                WaiterName = object.optString("WaiterName");

                            }

                            String ItemID = object.getString("ItemID");
                            String ItemCode = object.getString("ItemCode");
                            String Quantity = object.getString("Quantity");
                            String Amount = object.getString("Amount");
                            String BillID = object.getString("BillID");
                            String taxDescription = object.optString("taxDescription");
                            String mAcc = object.getString("memberAcc");
                            String taxValue = object.getString("taxValue");
                            String ava_balance = object.optString("avBalance");
                            String billNo = object.optString("billnumber");
                            String taxAmount = object.optString("taxAmount");
                            String Rate = object.getString("rate");
                            String itemname = object.getString("itemname");
                            String mName = object.getString("membername");
                            String memberId = object.getString("memberId");
                            String BillDate = object.getString("BillDate");
                            String ACCharge = object.getString("ACCharge");
                            String BillDetNumber = object.optString("BillDetNumber");
                            String otNo = object.getString("otno");
                            String ItemCategoryID = object.optString("ItemCategoryID");
                            String DiscountAmt = object.optString("DiscountAmt", "0");
                            String GSTNO = object.optString("GSTNO");
                            String opBalance = db.getOpeningBalance(mAcc);
                            String isFreeitem = object.optString("isFreeitem", "0");
                            String BuyItemId = object.optString("BuyItemId", "0");
                            String ItemSerialNo = object.optString("ItemSerialNo", "");
                            String ParentItemSerialNo = object.optString("ParentItemSerialNo", "0");
                            if (!taxAmount.equals("")) {
                                String[] arr1 = taxAmount.split("-");
                                for (int m = 0; m < arr1.length; m++) {
                                    String[] arr2 = arr1[m].split(",");
                                    try {
                                        db.insertBillTax(arr2[2], arr2[1], arr2[0], params[2], DiscountAmt);
                                    } catch (Exception e) {
                                        db.insertBillTax("", arr2[1], arr2[0], "", DiscountAmt);
                                    }
                                }
                            } else {
                                db.insertBills(ItemID, ItemCode, Quantity, Amount, BillID, mAcc, ava_balance, billNo, Rate, itemname, mName, memberId, BillDate, ACCharge, BillDetNumber, otNo, ItemCategoryID, opBalance, params[2], DiscountAmt, GSTNO);
                            }

                            cl.setItemID(ItemID);
                            cl.setItemCode(ItemCode);
                            cl.setQuantity(Quantity);
                            cl.setAmount(Amount);
                            cl.setBillID(BillID);
                            cl.setTaxDescription(taxDescription);
                            cl.setmAcc(mAcc);
                            cl.setTaxValue(taxValue);
                            cl.setAva_balance(ava_balance);
                            cl.setBillno(billNo);
                            cl.setTaxAmount(taxAmount);
                            cl.setmName(mName);
                            cl.setMemberID(memberId);
                            cl.setBillDate(BillDate);
                            cl.setRate(Rate);
                            cl.setItemname(itemname);
                            cl.setOtno(otNo);
                            cl.setCardType(params[2]);
                            cl.setACCharge(ACCharge);
                            cl.setOpeningBalance(opBalance);
                            cl.setBillDetNumber(BillDetNumber);
                            cl.setItemCategoryID(ItemCategoryID);
                            cl.setDiscountAmt(DiscountAmt);
                            cl.setGSTNO(GSTNO);
                            cl.setWaitername(WaiterName);
                            cl.setIsFreeitem(isFreeitem);
                            cl.setBuyItemId(BuyItemId);
                            cl.setItemSerialNo(ItemSerialNo);
                            cl.setParentItemSerialNo(ParentItemSerialNo);
                            closeorderItemsArrayList.add(cl);
                        } else {
                            status = Error;
                        }
                    }
                } else {
                    status = jsonObject.toString();
                }
                // status = jsonObject.toString();

            } catch (Exception e) {
                e.printStackTrace();
                status = e.toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(BillingProfile.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (pageType.equals("0")) {
                if (s.contains("true")) {
                    if (closeorderItemsArrayList.size() == 0 || closeorderItemsArrayList == null) {
                        // db.deleteAccount(member,posid,userId);
                        db.deleteAccount_new(member, posid, userId, bill);
                        readCard();
                        Toast.makeText(getApplicationContext(), "Order not taken for the member", Toast.LENGTH_LONG).show();
                    } else {
                        //db.deleteAccount(member,posid,userId);
                        db.deleteAccount_new(member, posid, userId, bill);
                        Toast.makeText(getApplicationContext(), "Order closed ", Toast.LENGTH_LONG).show();
                        //    Intent i = new Intent(BillingProfile.this, CloseOrderdetails.class);  -- JP nagar  (bill numberwise)
                        Intent i = new Intent(BillingProfile.this, MultiPrintView.class); // for pos wise bill -- ooty club (bill detail number wise)
                        i.putExtra("pageValue", 0);
                        i.putExtra("isdebitcard", modepay);
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("creditName", creditName);
                        i.putExtra("tableNo", table);
                        i.putExtra("tableId", tableId);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("cardType", cardType);
                        i.putExtra("waiternamecode", waiternamecode);
                        startActivity(i);
                        finish();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Order not closed-->" + s, Toast.LENGTH_LONG).show();
                }
            } else if (pageType.equals("1"))  //cancel order
            {
                if (s.contains("true")) {
                    if (closeorderItemsArrayList.size() == 0 || closeorderItemsArrayList == null) {
                        Toast.makeText(getApplicationContext(), "Order not taken for the member", Toast.LENGTH_LONG).show();
                    } else {
                        String memeberAnumber = memberAn.getText().toString();
                        Intent i = new Intent(BillingProfile.this, CloseOrderdetails.class);
                        i.putExtra("pageValue", 1);
                        i.putExtra("memeberAnumber", memeberAnumber);
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("creditName", creditName);
                        i.putExtra("tableNo", table);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableId", tableId);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("waiternamecode", waiternamecode);
                        //   i.putExtra("position",position);
                        //  i.putExtra("waiternamecode",waiternamecode);
                        startActivity(i);
                        finish();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No Item to Cancel-->" + s, Toast.LENGTH_LONG).show();

                }
            } else {
                Toast.makeText(getApplicationContext(), "Error-->" + jsonObject, Toast.LENGTH_LONG).show();
            }


        }
    }

    //    private void refreshViewofOt() {
//        searchItemname.setAdapter(arrayAdapterr);
//    }
    //**************************** for OT reprint *************************//
    protected class AsyncCloseOrder1 extends AsyncTask<String, Void, String> {
        String status = "";
        String storeId = sharedpreferences.getString("storeId", "");
        String deviceid = sharedpreferences.getString("deviceId", "");
        String userId = sharedpreferences.getString("userId", "");
        String pageType = "";
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(BillingProfile.this);
            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                closeorderItemsArrayList1.clear();
                //JSONObject jsonObject=api.cms_closeOrder(params[0], deviceid, storeId, formattedDate,Integer.parseInt(params[1]),userId,"","","");
                //JSONObject jsonObject=api.cms_getOTReprint(params[0], deviceid, storeId, formattedDate, userId, cardType, "",contractorsId);

                //if(bill.equals("coupon")){
                //     jsonObject=api.cms_getOTReprint(params[0], storeId, formattedDate, userId);
                //  }else {
                // jsonObject=api.cms_getOTReprint(params[0], storeId, formattedDate, userId);
                //  }
                if (bill.equals("coupon")) {
                    //jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "COUPON", billMode);
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "MEMBER CARD", billMode);

                } else if (bill.equals("Party Billing")) {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "PARTY CARD", billMode);

                } else if (bill.equals("Direct Party Billing")) {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "DIRECTPARTY CARD", billMode);

                } else {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, cardType, billMode);
                }

                JSONArray array = jsonObject.getJSONArray("Value");
                CloseorderItems cl = null;
                pageType = params[1];

                db.clearotreprint();
                for (int i = 0; i < array.length(); i++) {
                    cl = new CloseorderItems();
                    JSONObject object = array.getJSONObject(i);
                    String ItemID = object.optString("ItemID");
                    String ItemCode = object.optString("ItemCode");
                    String Quantity = object.optString("Quantity");
                    String Amount = object.optString("Amount");
                    String BillID = object.optString("BillID");
                    String mAcc = object.getString("memberAcc");
                    String billNo = object.optString("billnumber");
                    String Rate = object.getString("rate");
                    String itemname = object.getString("itemname");
                    String mName = object.getString("membername");
                    String memberId = object.getString("memberId");
                    String BillDate = object.getString("BillDate");
                    String ItemCategoryID = object.getString("ItemCategoryID");
                    String otNo = object.getString("otno");
                    db.insertotreprint(ItemID, ItemCode, Quantity, Amount, BillID, mAcc, "", billNo, Rate, itemname, mName, memberId, BillDate, otNo, ItemCategoryID);
                    cl.setItemID(ItemID);
                    cl.setItemCode(ItemCode);
                    cl.setQuantity(Quantity);
                    cl.setAmount(Amount);
                    cl.setBillID(BillID);
                    cl.setmAcc(mAcc);
                    cl.setBillno(billNo);
                    cl.setmName(mName);
                    cl.setMemberID(memberId);
                    cl.setBillDate(BillDate);
                    cl.setRate(Rate);
                    cl.setItemname(itemname);
                    cl.setOtno(otNo);
                    cl.setCardType(cardType);
                    cl.setItemCategoryID(ItemCategoryID);

                    closeorderItemsArrayList1.add(cl);
                }
                status = jsonObject.toString();

            } catch (Exception e) {
                e.printStackTrace();
                status = e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(BillingProfile.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (closeorderItemsArrayList1.size() > 0) {
                Intent i = new Intent(BillingProfile.this, OT_Reprint.class);
                i.putExtra("type", "billingprofile");
                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(), "Please Place OT for the Member", Toast.LENGTH_LONG).show();
            }


        }
    }


    // ******************************* TAKE ORDER ***************************//
    protected class AsyncCreditLimit extends AsyncTask<String, Void, String> {

        ArrayList<String> credit = null;
        String creditLimit = "";
        String debitbalance = "";
        String error = "";
        String memberId = "";
        String previousbal = "";

        String _params = "";
        String formattedDate = "";
        String userId = sharedpreferences.getString("userId", "");
        String storeId = sharedpreferences.getString("storeId", "");
        JSONObject jsonObject;

        String bmod = "";
        String bookingNumber = "";
        double ava_balance = 0;
        double debit_balance = 0;
        String cl = "0";
        String debitbal = "0";
        String membertype = "";
        String date_server = "";
        String date_expiry = "";
        String accesstype = "";

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(BillingProfile.this);
            try {

                // _params=(cardType.equals("SMART CARD")) ? "0":params[0];
                _params = params[0];
                //bmod=params[1];


                //monica
                //  member="NFC";
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                formattedDate = df.format(c.getTime());
                if (bill.equals("coupon")) {
                    if (member.equals("NCM")) {
                        jsonObject = api.cms_creditCheck(params[0], "", "COUPON", posid);
                        accesstype = "NCM";
                    } else if (member.contains("#") && member.contains("NCM")) {
                        //jsonObject = api.cms_creditCheckV2(params[0], "COUPON");
                        jsonObject = api.cms_creditCheck(params[0], "", "COUPON", posid);
                        accesstype = "NCM";
                    } else if (member.equals("NFC")) {
                        jsonObject = api.cms_creditCheckV2(params[0], "COUPON", posid);
                        cardType = "MEMBER CARD";
                        accesstype = "NFC";
                    } else if (member.contains("#") && member.contains("NFC")) {
                        jsonObject = api.cms_creditCheckV2(params[0], "COUPON", posid);
                        cardType = "MEMBER CARD";
                        accesstype = "NFC";
                    }

                } else if (bill.equals("Party Billing")) {
                    //jsonObject = api.cms_creditCheck(params[0], formattedDate, "PARTY CARD");
                    jsonObject = api.cms_creditCheckV2(_params, bill, posid);
                    cardType = "PARTY CARD";
                } else if (bill.equals("Direct Party Billing")) {
                    jsonObject = api.cms_creditCheck(params[0], formattedDate, "DIRECTPARTY CARD", posid);
                    cardType = "DIRECTPARTY CARD";
                }
//                else if(!MainActivity.isSmartCard)
//                {
//                    jsonObject = api.cms_creditCheck(params[0], formattedDate, cardType);
//                }
                else {
                    //jsonObject = api.cms_creditCheck(params[0], formattedDate, cardType);
                    //jsonObject = api.cms_creditCheckV2(_params, bill);
                    if (member.equals("NCM")) {
                        jsonObject = api.cms_creditCheck(params[0], formattedDate, cardType, posid);
                        accesstype = "NCM";

                    } else if (member.contains("#") && member.contains("NCM")) {

                        jsonObject = api.cms_creditCheck(params[0], formattedDate, cardType, posid);
                        accesstype = "NCM";
                    } else if (member.equals("NFC")) {
                        jsonObject = api.cms_creditCheckV2(params[0], bill, posid);
                        cardType = "MEMBER CARD";
                        accesstype = "NFC";
                    } else if (member.contains("#") && member.contains("NFC")) {
                        //  jsonObject = api.cms_creditCheckV2(params[0], bill);
                        jsonObject = api.cms_creditCheckV2(params[0], bill, posid);
                        cardType = "MEMBER CARD";
                        accesstype = "NFC";
                    }
                }


                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                credit = new ArrayList<>();
                check = "";
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    cl = jsonObject1.optString("availablebalance").toString();
                    debitbal = jsonObject1.optString("MemberDebitCardBalalnce").toString();
                    c_Limit = jsonObject1.optString("creditLimit");
                    creditAccountno = jsonObject1.optString("AccountNumber");
                    creditName = jsonObject1.optString("MemberName");
                    memberId = jsonObject1.optString("memberID");
                    previousbal = jsonObject1.optString("previousCreditLimit");
                    check = jsonObject1.optString("isCLRFTM");
                    isCashCardFilled = jsonObject1.optString("isCashCardFilled");
                    bookingNumber = jsonObject1.optString("BookingNumber");
                    bookingId = jsonObject1.optString("BookingID");
                    ava_balance = Double.parseDouble((cl.equals("") ? "0" : cl));
                    debit_balance = Double.parseDouble((debitbal.equals("") ? "0" : debitbal));
                    cardType = jsonObject1.optString("cardtype");
                    membertype = jsonObject1.optString("MemberType");
                    date_server = jsonObject1.optString("ServerDate");
                    date_server = jsonObject1.optString("ServerDate");
                    TempBillAmount = jsonObject1.optString("TempBillAmount");

                    if (cardType.equalsIgnoreCase("DEPENDENT CARD")) {
                        bookingId = jsonObject1.optString("DependantID");

                    }

                    if (cardType.equalsIgnoreCase("CASH CARD")) {
                        date_expiry = jsonObject1.optString("ExpiryDate");
                    }
                    //AlertBillmode (creditAccountno,cardType);
                    //   String   usedbalance=jsonObject1.get("usedbalance").toString();
                    //  double usedB=Double.parseDouble(usedbalance);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ct", cardType);
                    editor.apply();

                    String limit = (c_Limit.equals("")) ? "0" : c_Limit;
                    //String limit = (cLimit.equals("")) ? "0" : cLimit;
                    debitbalance = debit_balance + "";
                    if (!previousbal.equals("")) {
                        creditLimit = "0";
                    } else if (memberId.equals("AA")) {
                        creditLimit = "";
                    } else if (cardType.equals("CLUB CARD")) {
                        creditLimit = ava_balance + "";
                    } else if (bill.equals("coupon")) {
                        creditLimit = "coupon";
                    } else {
                        if (check.equals("")) {

                            if (cardType.equals("CASH CARD")) {
                                String date = date_server;
                                String dateafter = date_expiry;
                                SimpleDateFormat dateFormat = new SimpleDateFormat(
                                        "dd-mm-yyyy hh:mm:ss");
                                Date convertedDate = new Date();
                                Date convertedDate2 = new Date();
                                try {
                                    convertedDate = dateFormat.parse(date);
                                    convertedDate2 = dateFormat.parse(dateafter);
                                    if (convertedDate2.after(convertedDate)) {
                                        date_expiry = date_expiry;
                                    } else {
                                        date_expiry = "expired";
                                    }
                                } catch (ParseException e) {

                                    e.printStackTrace();
                                }
                                if (date_expiry.equalsIgnoreCase("expired")) {
                                    creditLimit = "";
                                } else {
                                    String datec = date_server;
                                    String dateafterc = date_expiry;
                                    SimpleDateFormat dateFormatc = new SimpleDateFormat(
                                            "dd-mm-yyyy hh:mm:ss");
                                    Date convertedDatec = new Date();
                                    Date convertedDate2c = new Date();
                                    try {
                                        convertedDatec = dateFormat.parse(datec);
                                        convertedDate2c = dateFormat.parse(dateafterc);
                                        if (convertedDate2c.after(convertedDatec)) {
                                            date_expiry = date_expiry;
                                        } else {
                                            date_expiry = "expired";
                                        }
                                    } catch (ParseException e) {

                                        e.printStackTrace();
                                    }
                                    if (date_expiry.equalsIgnoreCase("expired")) {
                                        creditLimit = "";
                                    } else {
                                        //  TODo need to validate for the cash card filled  (if  filled allow below line to validate or allow directly  inside to take order )
                                        if (isCashCardFilled.equalsIgnoreCase("false")) {
                                            creditLimit = ava_balance + "";
                                        } else {
                                            if (ava_balance > 0) {
                                                creditLimit = ava_balance + "";
                                            } else {
                                                creditLimit = "0";
                                            }
                                        }
                                    }

                                }

                            } else if (cardType.equals("SMART CARD")) {
                                if (ava_balance > 0) {
                                    creditLimit = ava_balance + "";
                                } else {
                                    creditLimit = "0";
                                }
                            } else     //if (!cardType.equals("CASH CARD") || !cardType.equals("SMART CARD"))
                            {
                                // if (ava_balance == -0 || (25000 - ava_balance) > 0) {
                                if (ava_balance == -0 || (Double.parseDouble(limit) - ava_balance) > 0) {
                                    creditLimit = ava_balance + "";
                                } else {
                                    creditLimit = "0";
                                }
                            }

                        } else {
                            if (check.equalsIgnoreCase("False")) {
                                creditLimit = ava_balance + "";
                            } else {
                                if (cardType.equals("CASH CARD")) {

                                    //  TODo need to validate for the cash card filled  (if  filled allow below line to validate or allow directly  inside to take order )
                                    if (isCashCardFilled.equalsIgnoreCase("false")) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        if (ava_balance > 0) {
                                            creditLimit = ava_balance + "";
                                        } else {
                                            creditLimit = "0";
                                        }
                                    }
                                } else if (cardType.equals("SMART CARD")) {
                                    if (ava_balance > 0) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        creditLimit = "0";
                                    }
                                }
                                // else  if (!cardType.equals("CASH CARD") || !cardType.equals("SMART CARD"))
                                else if(OTCreditLimitNotRequired.equalsIgnoreCase("true") && cardType.equals("MEMBER CARD")) {
                                    // if (ava_balance == -0 || (25000 - ava_balance) > 0) {
                                //    if (ava_balance == -0 || (Double.parseDouble(limit) - ava_balance) > 0) {
                                        creditLimit = ava_balance + "";

                                }
                                else if(OTCreditLimitNotRequired.equalsIgnoreCase("true") && cardType.equals("DEPENDENT CARD")) {
                                    // if (ava_balance == -0 || (25000 - ava_balance) > 0) {
                                    //    if (ava_balance == -0 || (Double.parseDouble(limit) - ava_balance) > 0) {
                                    creditLimit = ava_balance + "";

                                }  else {
                                    // if (ava_balance == -0 || (25000 - ava_balance) > 0) {
                                    if (ava_balance == -0 || (Double.parseDouble(limit) - ava_balance) > 0) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        creditLimit = "0";
                                    }
                                }
                            }
                        }

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                error = e.toString();
            }
            return creditLimit;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(BillingProfile.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (!s.equals("")) {
                waiternamecode = Steward;
                if (s.equals("0")) {
                    if (cardType.equals("CASH CARD") && isCashCardFilled.equals("true")) {
                        //Toast.makeText(getApplicationContext(), "  cash card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        //showMessage("  cash card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "cash card limit exceeded ,Can't do billing");
                    } else if (cardType.equals("MEMBER CARD") && !OTCreditLimitNotRequired.equalsIgnoreCase("true") ) {
                        // Toast.makeText(getApplicationContext(), "  Member card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        //showMessage(" Member card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "Member card limit exceeded ,Can't do billing");
                    } else if (cardType.equals("SMART CARD")) {
                        //Toast.makeText(getApplicationContext(), "  SMART card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        //showMessage("  SMART card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "SMART card limit exceeded ,Can't do billing");
                    } else if (cardType.equals("DEPENDENT CARD")&& !OTCreditLimitNotRequired.equalsIgnoreCase("true") ) {
                        // showMessage("DEPENDENT card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "DEPENDENT card limit exceeded ,Can't do billing");
                    } else {
                        //Toast.makeText(getApplicationContext(), "No account found  >" + s + "  " + _params +jsonObject, Toast.LENGTH_LONG).show();
                        //showMessage(" No account found  >" + s + "  " + _params + jsonObject, getApplicationContext());
                        //   AlertError(cardType, creditAccountno, "No account found  >" + s + "  " + _params);
                        AlertError(cardType, creditAccountno, "No account found  >" + s + "  ");
                    }

                } else if (bill.equals("Party Billing")) {
                    bmod = AlertBillmode(creditAccountno, "MEMBER CARD");
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");
                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.commit();
                    Intent i = new Intent(BillingProfile.this, Search_Item.class);
                    //Intent i = new Intent(TakeOrder.this, Itemview.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("tableNo", table);
                    i.putExtra("accessType", accesstype);
                    i.putExtra("creditLimit", bookingNumber);           // coupon no credit check in place order  so given -0to place order without validating for credit limit
                    i.putExtra("debitbal", debitbal);           // coupon no credit check in place order  so given -0to place order without validating for credit limit
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("memberId", memberId);
                    i.putExtra("tableId", tableId);
                    i.putExtra("cardType", cardType);
                    i.putExtra("referenceNo", bookingId);
                    Log.d("contractor1",bookingId);
                    i.putExtra("paxcnt", paxcount);
                    i.putExtra("waiterid", waiterid);
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    i.putExtra("billmode", bmod);
                    i.putExtra("dob", "0");
                    i.putExtra("doa", "0");


                    i.putExtra("paxcnt", paxcount);
                    i.putExtra("TempBillAmount", TempBillAmount);
                    startActivity(i);
                    db.deleteOT();
                    finish();

                } else if (cardType.equals("ROOM CARD")) {

                    String billType = sharedpreferences.getString("billType", "");
                    bmod = AlertBillmode(creditAccountno, cardType);
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");
                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.commit();

                    Intent i = new Intent(BillingProfile.this, Search_Item.class);
                    //Intent i = new Intent(TakeOrder.this, Itemview.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("tableNo", table);
                    i.putExtra("accessType", accesstype);
                    i.putExtra("creditLimit", creditLimit);
                    i.putExtra("debitbal", debitbal);
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("memberId", memberId);
                    i.putExtra("tableId", tableId);
                    i.putExtra("paxcnt", paxcount);
                    i.putExtra("cardType", cardType);
                    i.putExtra("referenceNo", bookingId);
                    Log.d("contractor2",bookingId);

                    i.putExtra("waiterid", waiterid);
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    i.putExtra("billmode", bmod);
                    i.putExtra("doa", "0");
                    i.putExtra("dob", "0");

                    i.putExtra("paxcnt", paxcount);
                    i.putExtra("TempBillAmount", TempBillAmount);
                    startActivity(i);
                    db.deleteOT();
                    finish();


                } else if (s.equals("coupon")) {  //coupon billing no credit check required

                    String billType = sharedpreferences.getString("billType", "");
                    bmod = AlertBillmode(creditAccountno, cardType);
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");

                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.commit();
                    if (billType.equals("BAK")) {
                        Intent i = new Intent(BillingProfile.this, Bakery.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("GroupName", "");
                        i.putExtra("name", creditName);
                        i.putExtra("accountNo", creditAccountno);
                        i.putExtra("credit", creditLimit);
                        i.putExtra("groupId", "");
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableNo", table);
                        i.putExtra("accessType", accesstype);
                        i.putExtra("referenceNo", _params);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);

                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("TempBillAmount", TempBillAmount);
                        startActivity(i);
                        finish();
                    } else {

                        Intent i = new Intent(BillingProfile.this, Search_Item.class);
                        //Intent i = new Intent(TakeOrder.this, Itemview.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("tableNo", table);
                        i.putExtra("accessType", accesstype);
                        i.putExtra("creditLimit", "-0");           // coupon no credit check in place order  so given -0to place order without validating for credit limit
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("debitbal", debitbal);
                        i.putExtra("creditName", creditName);
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("referenceNo", _params);
                        Log.d("contractor3",_params);

                        i.putExtra("waiterid", waiterid);
                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);
                        i.putExtra("doa", "0");
                        i.putExtra("dob", "0");

                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("TempBillAmount", TempBillAmount);
                        startActivity(i);
                        db.deleteOT();
                        finish();

                    }

                } else {

                    String billType = sharedpreferences.getString("billType", "");
                    bmod = AlertBillmode(creditAccountno, cardType);


                    Date dateIn = null;
                    try {
                        dateIn = new SimpleDateFormat("dd-MM-yyyy").parse(date_server);
                        String formatteddateIn = new SimpleDateFormat("yyyy-MM-dd").format(dateIn);
                        date_server = formatteddateIn;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype + "#" + date_server, date_server, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");
                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.commit();
                    if (billType.equals("BAK")) {
                        Intent i = new Intent(BillingProfile.this, Bakery.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("GroupName", "");
                        i.putExtra("name", creditName);
                        i.putExtra("accountNo", creditAccountno);
                        i.putExtra("credit", creditLimit);
                        i.putExtra("groupId", "");
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableNo", table);
                        i.putExtra("accessType", accesstype);
                        i.putExtra("referenceNo", _params);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);

                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("TempBillAmount", TempBillAmount);
                        startActivity(i);
                        finish();
                    } else {

                        //monica
                        new AsyncmemberDob().execute(memberId, creditLimit, _params, bmod, debitbal, accesstype);


                        //no dob
                         /*   Intent i = new Intent(BillingProfile.this, Search_Item.class);
                            //Intent i = new Intent(TakeOrder.this, Itemview.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtra("tableNo", table);
                            i.putExtra("creditLimit", creditLimit);
                            i.putExtra("creditAno", creditAccountno);
                            i.putExtra("creditName", creditName);
                            i.putExtra("memberId", memberId);
                            i.putExtra("tableId", tableId);
                            i.putExtra("cardType", cardType);
                            i.putExtra("referenceNo", _params);
                            i.putExtra("waiterid", waiterid);
                            i.putExtra("position", position);
                            i.putExtra("waiternamecode", waiternamecode);
                            i.putExtra("billmode", bmod);
                            startActivity(i);
                            db.deleteOT();
                            finish();*/
                        //no dob

                         /*     Intent i = new Intent(TakeOrder.this, KotItemlist.class);
                              i.putExtra("tableNo", table);
                              i.putExtra("creditLimit", creditLimit);
                              i.putExtra("creditAno", creditAccountno);
                              i.putExtra("creditName", creditName);
                              i.putExtra("memberId", memberId);
                              i.putExtra("tableId", tableId);
                              i.putExtra("cardType", cardType);
                              i.putExtra("referenceNo", _params);
                              startActivity(i);
                              finish();*/
                    }
                }
            } else {
                if (memberId.equals("AA")) {
                    //Toast.makeText(getApplicationContext(), " Sorry Member is already in another POS ", Toast.LENGTH_LONG).show();
                    // showMessage("Sorry Member is already in another POS" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "Sorry Member is already in another POS");
                } else if (cardType.equals("ROOM CARD")) {
                    //Toast.makeText(getApplicationContext(), " Room card is checked out ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("Room card is checked out ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "Room card is checked out ,Can't do billing");
                } else if (cardType.equals("NEW ROOM CARD")) {
                    //Toast.makeText(getApplicationContext(), "New Room card is checked out ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("New Room card is checked out ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "New Room card is checked out ,Can't do billing");
                } else if (cardType.equals("CASH CARD")) {
                    //Toast.makeText(getApplicationContext(), "cash card expired ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("cash card expired ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "cash card expired ,Can't do billing");

                } else if (cardType.equals("SMART CARD")) {
                    //Toast.makeText(getApplicationContext(), "Smart card expired ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("Smart card expired ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "Smart card expired ,Can't do billing");
                } else if (error.contains("No value for Value")) {
                    //Toast.makeText(getApplicationContext(), "No account found-->" + s + _params +jsonObject, Toast.LENGTH_LONG).show();
                    //showMessage("No account found-->" + s + _params + jsonObject, getApplicationContext());
                    //  AlertError(cardType, creditAccountno, "No account found-->" + error + _params);
                    AlertError(cardType, creditAccountno, "INVALID ACCOUNT");
                    //TODO remove
                    /* Intent i = new Intent(TakeOrder.this, Search_Item.class);
                    i.putExtra("tableNo", table);
                    i.putExtra("creditLimit", creditLimit);
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("memberId", memberId);
                    i.putExtra("tableId", tableId);
                    i.putExtra("cardType", cardType);
                    i.putExtra("referenceNo", _params);
                    startActivity(i);
                    db.deleteOT();
                    finish();*/
                } else if (error.contains("java.net.ConnectionException")) {
                    AlertError(cardType, creditAccountno, "SERVER ERROR " + "Check Your Wifi and Retry");
                } else {
                    AlertError(cardType, creditAccountno, " ERROR:NO ACCOUNT FOUND-->" + error);
                }
            }
        }
    }
    protected class AsyncDependentCreditLimit extends AsyncTask<String, Void, String> {

        ArrayList<String> credit = null;
        String creditLimit = "";
        String debitbalance = "";
        String error = "";
        String memberId = "";
        String previousbal = "";

        String _params = "";
        String formattedDate = "";
        String userId = sharedpreferences.getString("userId", "");
        String storeId = sharedpreferences.getString("storeId", "");
        JSONObject jsonObject;

        String bmod = "";
        String bookingNumber = "";
        double ava_balance = 0;
        double debit_balance = 0;
        String cl = "0";
        String debitbal = "0";
        String membertype = "";
        String date_server = "";
        String date_expiry = "";
        String accesstype = "";

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(BillingProfile.this);
            try {

                // _params=(cardType.equals("SMART CARD")) ? "0":params[0];
                _params = params[0];
                //bmod=params[1];


                //monica
                //  member="NFC";
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                formattedDate = df.format(c.getTime());
              cardType=  "DEPENDENT CARD";
                        jsonObject = api.cms_creditCheck(params[0], formattedDate, cardType, posid);
                        accesstype = "NCM";


                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                credit = new ArrayList<>();
                check = "";
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    cl = jsonObject1.optString("availablebalance").toString();
                    debitbal = jsonObject1.optString("MemberDebitCardBalalnce").toString();
                    c_Limit = jsonObject1.optString("creditLimit");
                    creditAccountno = jsonObject1.optString("AccountNumber");
                    creditName = jsonObject1.optString("MemberName");
                    memberId = jsonObject1.optString("memberID");
                    previousbal = jsonObject1.optString("previousCreditLimit");
                    check = jsonObject1.optString("isCLRFTM");
                    isCashCardFilled = jsonObject1.optString("isCashCardFilled");
                    bookingNumber = jsonObject1.optString("BookingNumber");
                    bookingId = jsonObject1.optString("BookingID");
                    ava_balance = Double.parseDouble((cl.equals("") ? "0" : cl));
                    debit_balance = Double.parseDouble((debitbal.equals("") ? "0" : debitbal));
                    cardType = jsonObject1.optString("cardtype");
                    membertype = jsonObject1.optString("MemberType");
                    date_server = jsonObject1.optString("ServerDate");
                    date_server = jsonObject1.optString("ServerDate");
                    TempBillAmount = jsonObject1.optString("TempBillAmount");

                    if (cardType.equalsIgnoreCase("DEPENDENT CARD")) {
                        bookingId = jsonObject1.optString("DependantID");

                    }

                    if (cardType.equalsIgnoreCase("CASH CARD")) {
                        date_expiry = jsonObject1.optString("ExpiryDate");


                    }
                    //AlertBillmode (creditAccountno,cardType);
                    //   String   usedbalance=jsonObject1.get("usedbalance").toString();
                    //  double usedB=Double.parseDouble(usedbalance);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ct", cardType);
                    editor.apply();

                    String limit = (c_Limit.equals("")) ? "0" : c_Limit;
                    //String limit = (cLimit.equals("")) ? "0" : cLimit;
                    debitbalance = debit_balance + "";
                    if (!previousbal.equals("")) {
                        creditLimit = "0";
                    } else if (memberId.equals("AA")) {
                        creditLimit = "";
                    } else if (cardType.equals("CLUB CARD")) {
                        creditLimit = ava_balance + "";
                    } else if (bill.equals("coupon")) {
                        creditLimit = "coupon";
                    } else {
                        if (check.equals("")) {

                            if (cardType.equals("CASH CARD")) {
                                String date = date_server;
                                String dateafter = date_expiry;
                                SimpleDateFormat dateFormat = new SimpleDateFormat(
                                        "dd-mm-yyyy hh:mm:ss");
                                Date convertedDate = new Date();
                                Date convertedDate2 = new Date();
                                try {
                                    convertedDate = dateFormat.parse(date);
                                    convertedDate2 = dateFormat.parse(dateafter);
                                    if (convertedDate2.after(convertedDate)) {
                                        date_expiry = date_expiry;
                                    } else {
                                        date_expiry = "expired";
                                    }
                                } catch (ParseException e) {

                                    e.printStackTrace();
                                }
                                if (date_expiry.equalsIgnoreCase("expired")) {
                                    creditLimit = "";
                                } else {
                                    String datec = date_server;
                                    String dateafterc = date_expiry;
                                    SimpleDateFormat dateFormatc = new SimpleDateFormat(
                                            "dd-mm-yyyy hh:mm:ss");
                                    Date convertedDatec = new Date();
                                    Date convertedDate2c = new Date();
                                    try {
                                        convertedDatec = dateFormat.parse(datec);
                                        convertedDate2c = dateFormat.parse(dateafterc);
                                        if (convertedDate2c.after(convertedDatec)) {
                                            date_expiry = date_expiry;
                                        } else {
                                            date_expiry = "expired";
                                        }
                                    } catch (ParseException e) {

                                        e.printStackTrace();
                                    }
                                    if (date_expiry.equalsIgnoreCase("expired")) {
                                        creditLimit = "";
                                    } else {
                                        //  TODo need to validate for the cash card filled  (if  filled allow below line to validate or allow directly  inside to take order )
                                        if (isCashCardFilled.equalsIgnoreCase("false")) {
                                            creditLimit = ava_balance + "";
                                        } else {
                                            if (ava_balance > 0) {
                                                creditLimit = ava_balance + "";
                                            } else {
                                                creditLimit = "0";
                                            }
                                        }
                                    }

                                }

                            } else if (cardType.equals("SMART CARD")) {
                                if (ava_balance > 0) {
                                    creditLimit = ava_balance + "";
                                } else {
                                    creditLimit = "0";
                                }
                            } else     //if (!cardType.equals("CASH CARD") || !cardType.equals("SMART CARD"))
                            {
                                // if (ava_balance == -0 || (25000 - ava_balance) > 0) {
                                if (ava_balance == -0 || (Double.parseDouble(limit) - ava_balance) > 0) {
                                    creditLimit = ava_balance + "";
                                } else {
                                    creditLimit = "0";
                                }
                            }

                        } else {
                            if (check.equalsIgnoreCase("False")) {
                                creditLimit = ava_balance + "";
                            } else {
                                if (cardType.equals("CASH CARD")) {

                                    //  TODo need to validate for the cash card filled  (if  filled allow below line to validate or allow directly  inside to take order )
                                    if (isCashCardFilled.equalsIgnoreCase("false")) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        if (ava_balance > 0) {
                                            creditLimit = ava_balance + "";
                                        } else {
                                            creditLimit = "0";
                                        }
                                    }
                                } else if (cardType.equals("SMART CARD")) {
                                    if (ava_balance > 0) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        creditLimit = "0";
                                    }
                                }
                                // else  if (!cardType.equals("CASH CARD") || !cardType.equals("SMART CARD"))
                                else if(OTCreditLimitNotRequired.equalsIgnoreCase("true") && cardType.equals("MEMBER CARD")) {
                                    // if (ava_balance == -0 || (25000 - ava_balance) > 0) {
                                //    if (ava_balance == -0 || (Double.parseDouble(limit) - ava_balance) > 0) {
                                        creditLimit = ava_balance + "";

                                }   else {
                                    // if (ava_balance == -0 || (25000 - ava_balance) > 0) {
                                    if (ava_balance == -0 || (Double.parseDouble(limit) - ava_balance) > 0) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        creditLimit = "0";
                                    }
                                }
                            }
                        }

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                error = e.toString();
                Log.d("DEPT error",error);
            }
            return creditLimit;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(BillingProfile.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (!s.equals("")) {
                waiternamecode = Steward;
                if (s.equals("0")) {
                    if (cardType.equals("CASH CARD") && isCashCardFilled.equals("true")) {
                        //Toast.makeText(getApplicationContext(), "  cash card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        //showMessage("  cash card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "cash card limit exceeded ,Can't do billing");
                    } else if (cardType.equals("MEMBER CARD") && !OTCreditLimitNotRequired.equalsIgnoreCase("true") ) {
                        // Toast.makeText(getApplicationContext(), "  Member card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        //showMessage(" Member card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "Member card limit exceeded ,Can't do billing");
                    } else if (cardType.equals("SMART CARD")) {
                        //Toast.makeText(getApplicationContext(), "  SMART card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        //showMessage("  SMART card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "SMART card limit exceeded ,Can't do billing");
                    } else if (cardType.equals("DEPENDENT CARD")&& !OTCreditLimitNotRequired.equalsIgnoreCase("true") ) {
                        // showMessage("DEPENDENT card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "DEPENDENT card limit exceeded ,Can't do billing");
                    } else {
                        //Toast.makeText(getApplicationContext(), "No account found  >" + s + "  " + _params +jsonObject, Toast.LENGTH_LONG).show();
                        //showMessage(" No account found  >" + s + "  " + _params + jsonObject, getApplicationContext());
                        //   AlertError(cardType, creditAccountno, "No account found  >" + s + "  " + _params);
                        AlertError(cardType, creditAccountno, "No account found  >" + s + "  ");
                    }

                } else if (bill.equals("Party Billing")) {
                    bmod = AlertBillmode(creditAccountno, "MEMBER CARD");
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");
                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.commit();
                    Intent i = new Intent(BillingProfile.this, Search_Item.class);
                    //Intent i = new Intent(TakeOrder.this, Itemview.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("tableNo", table);
                    i.putExtra("accessType", accesstype);
                    i.putExtra("creditLimit", bookingNumber);           // coupon no credit check in place order  so given -0to place order without validating for credit limit
                    i.putExtra("debitbal", debitbal);           // coupon no credit check in place order  so given -0to place order without validating for credit limit
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("memberId", memberId);
                    i.putExtra("tableId", tableId);
                    i.putExtra("cardType", cardType);
                    i.putExtra("referenceNo", bookingId);
                    Log.d("contractor4",bookingId);

                    i.putExtra("paxcnt", paxcount);
                    i.putExtra("waiterid", waiterid);
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    i.putExtra("billmode", bmod);
                    i.putExtra("dob", "0");
                    i.putExtra("doa", "0");


                    i.putExtra("paxcnt", paxcount);
                    i.putExtra("TempBillAmount", TempBillAmount);
                    startActivity(i);
                    db.deleteOT();
                    finish();

                } else if (cardType.equals("ROOM CARD")) {

                    String billType = sharedpreferences.getString("billType", "");
                    bmod = AlertBillmode(creditAccountno, cardType);
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");
                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.commit();

                    Intent i = new Intent(BillingProfile.this, Search_Item.class);
                    //Intent i = new Intent(TakeOrder.this, Itemview.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("tableNo", table);
                    i.putExtra("accessType", accesstype);
                    i.putExtra("creditLimit", creditLimit);
                    i.putExtra("debitbal", debitbal);
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("memberId", memberId);
                    i.putExtra("tableId", tableId);
                    i.putExtra("paxcnt", paxcount);
                    i.putExtra("cardType", cardType);
                    i.putExtra("referenceNo", bookingId);
                    Log.d("contractor5",bookingId);
                    i.putExtra("waiterid", waiterid);
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    i.putExtra("billmode", bmod);
                    i.putExtra("doa", "0");
                    i.putExtra("dob", "0");

                    i.putExtra("paxcnt", paxcount);
                    i.putExtra("TempBillAmount", TempBillAmount);
                    startActivity(i);
                    db.deleteOT();
                    finish();


                } else if (s.equals("coupon")) {  //coupon billing no credit check required

                    String billType = sharedpreferences.getString("billType", "");
                    bmod = AlertBillmode(creditAccountno, cardType);
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");

                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.commit();
                    if (billType.equals("BAK")) {
                        Intent i = new Intent(BillingProfile.this, Bakery.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("GroupName", "");
                        i.putExtra("name", creditName);
                        i.putExtra("accountNo", creditAccountno);
                        i.putExtra("credit", creditLimit);
                        i.putExtra("groupId", "");
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableNo", table);
                        i.putExtra("accessType", accesstype);
                        i.putExtra("referenceNo", _params);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);

                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("TempBillAmount", TempBillAmount);
                        startActivity(i);
                        finish();
                    } else {

                        Intent i = new Intent(BillingProfile.this, Search_Item.class);
                        //Intent i = new Intent(TakeOrder.this, Itemview.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("tableNo", table);
                        i.putExtra("accessType", accesstype);
                        i.putExtra("creditLimit", "-0");           // coupon no credit check in place order  so given -0to place order without validating for credit limit
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("debitbal", debitbal);
                        i.putExtra("creditName", creditName);
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("referenceNo", _params);
                        Log.d("contractor6",_params);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);
                        i.putExtra("doa", "0");
                        i.putExtra("dob", "0");

                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("TempBillAmount", TempBillAmount);
                        startActivity(i);
                        db.deleteOT();
                        finish();
                    }

                } else {

                    String billType = sharedpreferences.getString("billType", "");
                    bmod = AlertBillmode(creditAccountno, cardType);


                    Date dateIn = null;
                    try {
                        dateIn = new SimpleDateFormat("dd-MM-yyyy").parse(date_server);
                        String formatteddateIn = new SimpleDateFormat("yyyy-MM-dd").format(dateIn);
                        date_server = formatteddateIn;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype + "#" + date_server, date_server, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");
                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.commit();
                    if (billType.equals("BAK")) {
                        Intent i = new Intent(BillingProfile.this, Bakery.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("GroupName", "");
                        i.putExtra("name", creditName);
                        i.putExtra("accountNo", creditAccountno);
                        i.putExtra("credit", creditLimit);
                        i.putExtra("groupId", "");
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableNo", table);
                        i.putExtra("accessType", accesstype);
                        i.putExtra("referenceNo", _params);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);

                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("TempBillAmount", TempBillAmount);
                        startActivity(i);
                        finish();
                    } else {

                        //monica
                    //    new AsyncmemberDob().execute(memberId, creditLimit, _params, bmod, debitbal, accesstype);

                        Intent i = new Intent(BillingProfile.this, Search_Item.class);
                        //Intent i = new Intent(TakeOrder.this, Itemview.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("tableNo", "0");
                        i.putExtra("accessType", accesstype);
                        i.putExtra("creditLimit", creditLimit);
                        i.putExtra("debitbal", debitbal);
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("creditName", creditName);
                        i.putExtra("memberId", memberId);
                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("tableId", "0");
                        i.putExtra("cardType", cardType);
                        i.putExtra("referenceNo", contractorsId);
                        Log.d("contractor7",contractorsId);

                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);
                        i.putExtra("dob", "0");
                        i.putExtra("doa", "0");

                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("TempBillAmount", TempBillAmount);
                        startActivity(i);
                        db.deleteOT();
                        finish();

                        //no dob
                         /*   Intent i = new Intent(BillingProfile.this, Search_Item.class);
                            //Intent i = new Intent(TakeOrder.this, Itemview.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtra("tableNo", table);
                            i.putExtra("creditLimit", creditLimit);
                            i.putExtra("creditAno", creditAccountno);
                            i.putExtra("creditName", creditName);
                            i.putExtra("memberId", memberId);
                            i.putExtra("tableId", tableId);
                            i.putExtra("cardType", cardType);
                            i.putExtra("referenceNo", _params);
                            i.putExtra("waiterid", waiterid);
                            i.putExtra("position", position);
                            i.putExtra("waiternamecode", waiternamecode);
                            i.putExtra("billmode", bmod);
                            startActivity(i);
                            db.deleteOT();
                            finish();*/
                        //no dob

                         /*     Intent i = new Intent(TakeOrder.this, KotItemlist.class);
                              i.putExtra("tableNo", table);
                              i.putExtra("creditLimit", creditLimit);
                              i.putExtra("creditAno", creditAccountno);
                              i.putExtra("creditName", creditName);
                              i.putExtra("memberId", memberId);
                              i.putExtra("tableId", tableId);
                              i.putExtra("cardType", cardType);
                              i.putExtra("referenceNo", _params);
                              startActivity(i);
                              finish();*/
                    }
                }
            } else {
                if (memberId.equals("AA")) {
                    //Toast.makeText(getApplicationContext(), " Sorry Member is already in another POS ", Toast.LENGTH_LONG).show();
                    // showMessage("Sorry Member is already in another POS" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "Sorry Member is already in another POS");
                } else if (cardType.equals("ROOM CARD")) {
                    //Toast.makeText(getApplicationContext(), " Room card is checked out ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("Room card is checked out ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "Room card is checked out ,Can't do billing");
                } else if (cardType.equals("NEW ROOM CARD")) {
                    //Toast.makeText(getApplicationContext(), "New Room card is checked out ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("New Room card is checked out ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "New Room card is checked out ,Can't do billing");
                } else if (cardType.equals("CASH CARD")) {
                    //Toast.makeText(getApplicationContext(), "cash card expired ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("cash card expired ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "cash card expired ,Can't do billing");

                } else if (cardType.equals("SMART CARD")) {
                    //Toast.makeText(getApplicationContext(), "Smart card expired ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("Smart card expired ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "Smart card expired ,Can't do billing");
                } else if (error.contains("No value for Value")) {
                    //Toast.makeText(getApplicationContext(), "No account found-->" + s + _params +jsonObject, Toast.LENGTH_LONG).show();
                    //showMessage("No account found-->" + s + _params + jsonObject, getApplicationContext());
                    //  AlertError(cardType, creditAccountno, "No account found-->" + error + _params);
                    AlertError(cardType, creditAccountno, "INVALID ACCOUNT");
                    //TODO remove
                    /* Intent i = new Intent(TakeOrder.this, Search_Item.class);
                    i.putExtra("tableNo", table);
                    i.putExtra("creditLimit", creditLimit);
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("memberId", memberId);
                    i.putExtra("tableId", tableId);
                    i.putExtra("cardType", cardType);
                    i.putExtra("referenceNo", _params);
                    startActivity(i);
                    db.deleteOT();
                    finish();*/
                } else if (error.contains("java.net.ConnectionException")) {
                    AlertError(cardType, creditAccountno, "SERVER ERROR " + "Check Your Wifi and Retry");
                } else {
                    AlertError(cardType, creditAccountno, " ERROR:NO ACCOUNT FOUND-->" + error);
                }
            }
        }
    }

    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(BillingProfile.this);
        String status = "";
        String message = "";
        String result = "";
        String error = "";
        ProgressDialog pd;

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                ConnectionDetector cd = new ConnectionDetector(mContext);
                boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
//                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    getResult = api.OTPGeneration(strings[0], userId);
                    result = "" + getResult;
                    if (result.contains("true")) {
                        String jsonObject = getResult.getString("Value");
                        if (!jsonObject.isEmpty()) {
                            status = "success";
                            message = jsonObject;
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(mContext);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        pd.dismiss();
                        Toast.makeText(BillingProfile.this, "" + message, Toast.LENGTH_SHORT).show();
                        linear.setVisibility(View.VISIBLE);
                        break;
                    case "server":
                        Toast.makeText(mContext, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(mContext, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(mContext, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(mContext, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }

    protected class CallVerifyAPi extends AsyncTask<String, Void, String> {

        ArrayList<String> credit = null;
        String creditLimit = "";
        String debitbalance = "";
        String error = "";
        String memberId = "";
        String previousbal = "";

        String _params = "";
        String formattedDate = "";
        String userId = sharedpreferences.getString("userId", "");
        String storeId = sharedpreferences.getString("storeId", "");
        JSONObject jsonObject;

        String bmod = "";
        String bookingNumber = "";
        String bookingId = "";
        double ava_balance = 0;
        double debit_balance = 0;
        String cl = "0";
        String debitbal = "0";
        String membertype = "";
        String date_server = "";
        String accesstype = "";

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(BillingProfile.this);
            try {

                // _params=(cardType.equals("SMART CARD")) ? "0":params[0];
                _params = params[0];
                //bmod=params[1];


                //monica
                //  member="NFC";
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                formattedDate = df.format(c.getTime());

                jsonObject = api.ValidateOTP(params[0], params[1]);
                accesstype = "NCM";

                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                credit = new ArrayList<>();
                check = "";
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    cl = jsonObject1.optString("availablebalance").toString();
                    debitbal = jsonObject1.optString("MemberDebitCardBalalnce").toString();
                    c_Limit = jsonObject1.optString("creditLimit");
                    creditAccountno = jsonObject1.optString("AccountNumber");
                    creditName = jsonObject1.optString("MemberName");
                    memberId = jsonObject1.optString("memberID");
                    previousbal = jsonObject1.optString("previousCreditLimit");
                    check = jsonObject1.optString("isCLRFTM");
                    isCashCardFilled = jsonObject1.optString("isCashCardFilled");
                    bookingNumber = jsonObject1.optString("BookingNumber");
                    bookingId = jsonObject1.optString("BookingID");
                    ava_balance = Double.parseDouble((cl.equals("") ? "0" : cl));
                    debit_balance = Double.parseDouble((debitbal.equals("") ? "0" : debitbal));
                    cardType = jsonObject1.optString("cardtype");
                    membertype = jsonObject1.optString("MemberType");
                    date_server = jsonObject1.optString("ServerDate");
                    //AlertBillmode (creditAccountno,cardType);
                    //   String   usedbalance=jsonObject1.get("usedbalance").toString();
                    //  double usedB=Double.parseDouble(usedbalance);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ct", cardType);
                    editor.apply();

                    String limit = (c_Limit.equals("")) ? "0" : c_Limit;
                    //String limit = (cLimit.equals("")) ? "0" : cLimit;
                    debitbalance = debit_balance + "";
                    if (!previousbal.equals("")) {
                        creditLimit = "0";
                    } else if (memberId.equals("AA")) {
                        creditLimit = "";
                    } else if (cardType.equals("CLUB CARD")) {
                        creditLimit = ava_balance + "";
                    } else if (bill.equals("coupon")) {
                        creditLimit = "coupon";
                    } else {
                        if (check.equals("")) {


                            if (cardType.equals("CASH CARD")) {

                                //  TODo need to validate for the cash card filled  (if  filled allow below line to validate or allow directly  inside to take order )
                                if (isCashCardFilled.equalsIgnoreCase("false")) {
                                    creditLimit = ava_balance + "";
                                } else {
                                    if (ava_balance > 0) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        creditLimit = "0";
                                    }
                                }

                            } else if (cardType.equals("SMART CARD")) {
                                if (ava_balance > 0) {
                                    creditLimit = ava_balance + "";
                                } else {
                                    creditLimit = "0";
                                }
                            } else     //if (!cardType.equals("CASH CARD") || !cardType.equals("SMART CARD"))
                            {
                                // if (ava_balance == -0 || (25000 - ava_balance) > 0) {
                                if (ava_balance == -0 || (Double.parseDouble(limit) - ava_balance) > 0) {
                                    creditLimit = ava_balance + "";
                                } else {
                                    creditLimit = "0";
                                }
                            }

                        } else {
                            if (check.equalsIgnoreCase("False")) {
                                creditLimit = ava_balance + "";
                            } else {
                                if (cardType.equals("CASH CARD")) {

                                    //  TODo need to validate for the cash card filled  (if  filled allow below line to validate or allow directly  inside to take order )
                                    if (isCashCardFilled.equalsIgnoreCase("false")) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        if (ava_balance > 0) {
                                            creditLimit = ava_balance + "";
                                        } else {
                                            creditLimit = "0";
                                        }
                                    }
                                } else if (cardType.equals("SMART CARD")) {
                                    if (ava_balance > 0) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        creditLimit = "0";
                                    }
                                }
                                // else  if (!cardType.equals("CASH CARD") || !cardType.equals("SMART CARD"))
                                else {
                                    // if (ava_balance == -0 || (25000 - ava_balance) > 0) {
                                    if (ava_balance == -0 || (Double.parseDouble(limit) - ava_balance) > 0) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        creditLimit = "0";
                                    }
                                }
                            }
                        }

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                error = e.toString();
            }
            return creditLimit;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(BillingProfile.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (!s.equals("")) {
                waiternamecode = Steward;
                if (s.equals("0")) {
                    if (cardType.equals("CASH CARD") && isCashCardFilled.equals("true")) {
                        //Toast.makeText(getApplicationContext(), "  cash card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        //showMessage("  cash card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "cash card limit exceeded ,Can't do billing");
                    } else if (cardType.equals("MEMBER CARD")) {
                        // Toast.makeText(getApplicationContext(), "  Member card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        //showMessage(" Member card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "Member card limit exceeded ,Can't do billing");
                    } else if (cardType.equals("SMART CARD")) {
                        //Toast.makeText(getApplicationContext(), "  SMART card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        //showMessage("  SMART card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "SMART card limit exceeded ,Can't do billing");
                    } else if (cardType.equals("DEPENDENT CARD")) {
                        // showMessage("DEPENDENT card limit exceeded ,Can't do billing", getApplicationContext());
                        AlertError(cardType, creditAccountno, "DEPENDENT card limit exceeded ,Can't do billing");
                    } else {
                        //Toast.makeText(getApplicationContext(), "No account found  >" + s + "  " + _params +jsonObject, Toast.LENGTH_LONG).show();
                        //showMessage(" No account found  >" + s + "  " + _params + jsonObject, getApplicationContext());
                        //   AlertError(cardType, creditAccountno, "No account found  >" + s + "  " + _params);
                        AlertError(cardType, creditAccountno, "No account found  >" + s + "  ");
                    }
                } else if (bill.equals("Party Billing")) {
                    bmod = AlertBillmode(creditAccountno, "MEMBER CARD");
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");
                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.apply();
                    new CallProfileAPI().execute(creditAccountno, "4", accesstype, creditLimit, debitbal, memberId, bookingId, bmod);
                } else if (cardType.equals("ROOM CARD")) {
                    String billType = sharedpreferences.getString("billType", "");
                    bmod = AlertBillmode(creditAccountno, cardType);
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");
                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.apply();
                    new CallProfileAPI().execute(creditAccountno, "4", accesstype, creditLimit, debitbal, memberId, bookingId, bmod);

                } else if (s.equals("coupon")) {  //coupon billing no credit check required

                    String billType = sharedpreferences.getString("billType", "");
                    bmod = AlertBillmode(creditAccountno, cardType);
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");

                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.apply();
                    if (billType.equals("BAK")) {
                        Intent i = new Intent(BillingProfile.this, Bakery.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("GroupName", "");
                        i.putExtra("name", creditName);
                        i.putExtra("accountNo", creditAccountno);
                        i.putExtra("credit", creditLimit);
                        i.putExtra("groupId", "");
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableNo", table);
                        i.putExtra("accessType", accesstype);
                        i.putExtra("referenceNo", _params);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);
                        i.putExtra("paxcnt", paxcount);
                        startActivity(i);
                        finish();
                    } else {
                        new CallProfileAPI().execute(creditAccountno, "4", accesstype, "-0", debitbal, memberId, _params, bmod);
                    }

                } else {

                    String billType = sharedpreferences.getString("billType", "");
                    bmod = AlertBillmode(creditAccountno, cardType);


                    Date dateIn = null;
                    try {
                        dateIn = new SimpleDateFormat("dd-MM-yyyy").parse(date_server);
                        String formatteddateIn = new SimpleDateFormat("yyyy-MM-dd").format(dateIn);
                        date_server = formatteddateIn;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype + "#" + date_server, date_server, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno, debitbal, "");
                    member = creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.apply();
                    if (billType.equals("BAK")) {
                        Intent i = new Intent(BillingProfile.this, Bakery.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("GroupName", "");
                        i.putExtra("name", creditName);
                        i.putExtra("accountNo", creditAccountno);
                        i.putExtra("credit", creditLimit);
                        i.putExtra("groupId", "");
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableNo", table);
                        i.putExtra("accessType", accesstype);
                        i.putExtra("referenceNo", _params);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);

                        i.putExtra("paxcnt", paxcount);
                        startActivity(i);
                        finish();
                    } else {

                        //monica
                        new AsyncmemberDob().execute(memberId, creditLimit, _params, bmod, debitbal, accesstype);


                        //no dob
                         /*   Intent i = new Intent(BillingProfile.this, Search_Item.class);
                            //Intent i = new Intent(TakeOrder.this, Itemview.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtra("tableNo", table);
                            i.putExtra("creditLimit", creditLimit);
                            i.putExtra("creditAno", creditAccountno);
                            i.putExtra("creditName", creditName);
                            i.putExtra("memberId", memberId);
                            i.putExtra("tableId", tableId);
                            i.putExtra("cardType", cardType);
                            i.putExtra("referenceNo", _params);
                            i.putExtra("waiterid", waiterid);
                            i.putExtra("position", position);
                            i.putExtra("waiternamecode", waiternamecode);
                            i.putExtra("billmode", bmod);
                            startActivity(i);
                            db.deleteOT();
                            finish();*/
                        //no dob

                         /*     Intent i = new Intent(TakeOrder.this, KotItemlist.class);
                              i.putExtra("tableNo", table);
                              i.putExtra("creditLimit", creditLimit);
                              i.putExtra("creditAno", creditAccountno);
                              i.putExtra("creditName", creditName);
                              i.putExtra("memberId", memberId);
                              i.putExtra("tableId", tableId);
                              i.putExtra("cardType", cardType);
                              i.putExtra("referenceNo", _params);
                              startActivity(i);
                              finish();*/
                    }
                }
            } else {
                if (memberId.equals("AA")) {
                    //Toast.makeText(getApplicationContext(), " Sorry Member is already in another POS ", Toast.LENGTH_LONG).show();
                    // showMessage("Sorry Member is already in another POS" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "Sorry Member is already in another POS");
                } else if (cardType.equals("ROOM CARD")) {
                    //Toast.makeText(getApplicationContext(), " Room card is checked out ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("Room card is checked out ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "Room card is checked out ,Can't do billing");
                } else if (cardType.equals("NEW ROOM CARD")) {
                    //Toast.makeText(getApplicationContext(), "New Room card is checked out ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("New Room card is checked out ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "New Room card is checked out ,Can't do billing");
                } else if (cardType.equals("CASH CARD")) {
                    //Toast.makeText(getApplicationContext(), "cash card expired ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("cash card expired ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "cash card expired ,Can't do billing");

                } else if (cardType.equals("SMART CARD")) {
                    //Toast.makeText(getApplicationContext(), "Smart card expired ,Can't do billing", Toast.LENGTH_LONG).show();
                    //showMessage("Smart card expired ,Can't do billing" + jsonObject, getApplicationContext());
                    AlertError(cardType, creditAccountno, "Smart card expired ,Can't do billing");
                } else if (error.contains("No value for Value")) {
                    //Toast.makeText(getApplicationContext(), "No account found-->" + s + _params +jsonObject, Toast.LENGTH_LONG).show();
                    //showMessage("No account found-->" + s + _params + jsonObject, getApplicationContext());
                    //  AlertError(cardType, creditAccountno, "No account found-->" + error + _params);
                    AlertError(cardType, creditAccountno, "INVALID ACCOUNT");
                    //TODO remove
                    /* Intent i = new Intent(TakeOrder.this, Search_Item.class);
                    i.putExtra("tableNo", table);
                    i.putExtra("creditLimit", creditLimit);
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("memberId", memberId);
                    i.putExtra("tableId", tableId);
                    i.putExtra("cardType", cardType);
                    i.putExtra("referenceNo", _params);
                    startActivity(i);
                    db.deleteOT();
                    finish();*/
                } else if (error.contains("java.net.ConnectionException")) {
                    AlertError(cardType, creditAccountno, "SERVER ERROR " + "Check Your Wifi and Retry");
                } else {
                    AlertError(cardType, creditAccountno, " ERROR:NO ACCOUNT FOUND-->" + error);
                }
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class CallProfileAPI extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(BillingProfile.this);
        String status = "";
        String result = "";
        String error = "", items = "", taxes = "";
        ProgressDialog pd;
        MemberMainDto memberMainDto;
        String type;
        String accesstype, creditLimit, debitbal, memberId, bookingId, bmod;


        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                cd = new ConnectionDetector(BillingProfile.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    type = strings[1];
                    accesstype = strings[2];
                    creditLimit = strings[3];
                    debitbal = strings[4];
                    memberId = strings[5];
                    bookingId = strings[6];
                    bmod = strings[7];
                    getResult = api.GetMemberInformationforPOSonAccKeyIn(strings[0], "");
                    result = "" + getResult;
                    if (result.contains("true")) {
                        JSONObject jsonArray = getResult.getJSONObject("Value");
                        try {
                            Gson gson = new Gson();
                            try {
                                memberMainDto = gson.fromJson(String.valueOf(jsonArray), new TypeToken<MemberMainDto>() {
                                }.getType());
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(BillingProfile.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        try {
                            pd.dismiss();
                            if (memberMainDto != null) {
                                if (memberMainDto.getMemberDetailsList().size() > 0) {
                                    showProfile(memberMainDto, type, accesstype, creditLimit, debitbal, memberId, bookingId, bmod);
                                } else {
                                    if (type.equals("2")) {
                                        second();
                                    } else if (type.equals("3")) {
                                        third();
                                    } else {
                                        Intents(accesstype, creditLimit, debitbal, memberId, bookingId, bmod);
                                    }
                                }
                            } else {
                                Toast.makeText(BillingProfile.this, "failed: " + result, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                        break;
                    case "server":
                        Toast.makeText(BillingProfile.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(BillingProfile.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(BillingProfile.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(BillingProfile.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }


    public class Transmit implements Runnable {
        @Override
        public void run() {
            mReader.reset();
            if (!mReader.piccPowerOn(5, 143)) {
                //showRequestQueueError();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "Reader is not available", Toast.LENGTH_LONG).show();
                        mProgress.dismiss();
                    }
                });


            } else {
                /* Transmit the command APDU. */
                // while (manoch <= 4) {
                while (manoch <= 6) {
                    mPiccResponseApduReady = false;
                    mResultReady = false;
                    String india[] = null;
                    india = daniel.split(",");
                    mPiccCommandApdu = toByteArray(india[k]);
                    if (!mReader.piccTransmit(mPiccTimeout, mPiccCommandApdu)) {
                        /* Show the request queue error. */
                        //showRequestQueueError();
                        Toast.makeText(mContext, "Read again ", Toast.LENGTH_LONG).show();

                    } else {

                        /* Show the PICC response APDU. */
                        //showPiccResponseApdu();
                        synchronized (mResponseEvent) {
                            /* Wait for the PICC response APDU. */

                            while (!mPiccResponseApduReady && !mResultReady) {

                                try {
                                    mResponseEvent.wait(1000);
                                } catch (InterruptedException e) {
                                }
                                break;
                            }

                            if (mPiccResponseApduReady) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        /* Show the PICC response APDU. */
                                        String current = toHexString(mPiccResponseApdu);
                                        if (current.contains("90 00")) {

                                            if (manoch == 3) {
                                                finalValue = finalValue + current;
                                            } else if (manoch == 5) {
                                                try {

                                                    finalValue = finalValue + current;
                                                  /*  finalValue = finalValue.replaceAll("\\s+", "");
                                                    finalValue = hexToString(finalValue);
                                                    if (finalValue.contains("$"))
                                                    {
                                                        String ss[] = finalValue.split(Pattern.quote("$"));
                                                       // if (finalValue.contains("CLUB"))

                                                        if (finalValue.contains("DP")){


                                                            memberAn.setText(ss[3] + ":DEPENDENT CARD");
                                                            member = ss[3] + ":DEPENDENT CARD";
                                                            cardType = "DEPENDENT CARD";
                                                            order.setVisibility(View.VISIBLE);

                                                        }

                                                        else if (finalValue.contains("ICLB"))
                                                        {
                                                            memberAn.setText(ss[2] + ":CLUB CARD");
                                                            member = ss[2] + ":CLUB CARD";
                                                            cardType = "CLUB CARD";
                                                            order.setVisibility(View.VISIBLE);

                                                        }
                                                        else if (finalValue.contains("ICSH"))
                                                        {
                                                            memberAn.setText(ss[1] + ":CASH CARD");
                                                            member = ss[1] + ":CASH CARD";
                                                            cardType = "CASH CARD";
                                                            order.setVisibility(View.VISIBLE);

                                                        }
//                                                        else if (finalValue.contains("noom"))
//                                                        {
//                                                            memberAn.setText(ss[1] + ":NEW ROOM CARD");
//                                                            member = ss[1] + ":NEW ROOM CARD";
//                                                            cardType = "NEW ROOM CARD";
//                                                            order.setVisibility(View.VISIBLE);
//                                                        }
                                                        else if (finalValue.contains("room"))
                                                        {
                                                            memberAn.setText(ss[1] + ":ROOM CARD");
                                                            member = ss[1] + ":ROOM CARD";
                                                            cardType = "ROOM CARD";
                                                            order.setVisibility(View.VISIBLE);

                                                      //  } else if (finalValue.contains("BGCCC"))
                                                        }
//                                                        else if (finalValue.contains("TMP"))
//                                                        {
//                                                            memberAn.setText(ss[2] + ":TEMP CARD");
//                                                            member = ss[2] + ":TEMP CARD";
//                                                            cardType = "TEMP CARD";
//                                                            order.setVisibility(View.VISIBLE);
//
//                                                        }
                                                        else if (finalValue.contains("DUPLC"))
                                                        {
                                                            memberAn.setText(ss[3] + ":MEMBER DUPL CARD");
                                                            member = ss[3] + ":MEMBER DUPL CARD";
                                                            cardType = "MEMBER DUPL CARD";
                                                            order.setVisibility(View.VISIBLE);

                                                        }else  if (finalValue.contains("0$"))
                                                        {
                                                            memberAn.setText(ss[3] + ":MEMBER CARD");
                                                            member = ss[3] + ":MEMBER CARD";
                                                            cardType = "MEMBER CARD";
                                                            order.setVisibility(View.VISIBLE);
                                                        }  else
                                                        {
                                                            memberAn.setText("Error:" + finalValue);
                                                            member = "";
                                                            order.setVisibility(View.INVISIBLE);
                                                        }
                                                        mReader.piccPowerOff();
                                                        mReader.sleep();
                                                    }*/
                                                } catch (Exception e) {
                                                    Toast.makeText(mContext, "Error" + e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                }

                                            } else if (manoch == 7) {
                                                try {

                                                    finalValue = finalValue + current;
                                                    finalValue = finalValue.replaceAll("\\s+", "");
                                                    finalValue = hexToString(finalValue);
                                                    if (finalValue.contains("$")) {
                                                        String ss[] = finalValue.split(Pattern.quote("$"));
                                                        // if (finalValue.contains("CLUB"))

                                                        if (ss[0].contains("DP")) {


                                                            //memberAn.setText(ss[1] + ":DEPENDENT CARD");
                                                            memberAn.setText(ss[3] + ":DEPENDENT CARD");
                                                            member = ss[1] + ":DEPENDENT CARD";
                                                            cardType = "DEPENDENT CARD";
                                                            order.setVisibility(View.VISIBLE);

                                                        } else if (finalValue.contains("ICLB")) {
                                                            memberAn.setText(ss[2] + ":CLUB CARD");
                                                            member = ss[2] + ":CLUB CARD";
                                                            cardType = "CLUB CARD";
                                                            order.setVisibility(View.VISIBLE);

                                                        } else if (finalValue.contains("ICSH")) {
                                                            memberAn.setText(ss[1] + ":CASH CARD");
                                                            member = ss[1] + ":CASH CARD";
                                                            cardType = "CASH CARD";
                                                            order.setVisibility(View.VISIBLE);

                                                        } else if (finalValue.contains("SMRT")) {
                                                            memberAn.setText(ss[1] + ":SMART CARD");
                                                            member = ss[1] + ":SMART CARD";
                                                            cardType = "SMART CARD";
                                                            order.setVisibility(View.VISIBLE);
                                                        }
//                                                        else if (finalValue.contains("noom"))
//                                                        {
//                                                            memberAn.setText(ss[1] + ":NEW ROOM CARD");
//                                                            member = ss[1] + ":NEW ROOM CARD";
//                                                            cardType = "NEW ROOM CARD";
//                                                            order.setVisibility(View.VISIBLE);
//                                                        }
                                                        else if (finalValue.contains("room")) {
                                                            memberAn.setText(ss[1] + ":ROOM CARD");
                                                            member = ss[1] + ":ROOM CARD";
                                                            cardType = "ROOM CARD";
                                                            order.setVisibility(View.VISIBLE);

                                                            //  } else if (finalValue.contains("BGCCC"))
                                                        }
//                                                        else if (finalValue.contains("TMP"))
//                                                        {
//                                                            memberAn.setText(ss[2] + ":TEMP CARD");
//                                                            member = ss[2] + ":TEMP CARD";
//                                                            cardType = "TEMP CARD";
//                                                            order.setVisibility(View.VISIBLE);
//
//                                                        }
                                                        else if (finalValue.contains("DUPLC")) {
                                                            memberAn.setText(ss[3] + ":MEMBER DUPL CARD");
                                                            member = ss[3] + ":MEMBER DUPL CARD";
                                                            cardType = "MEMBER DUPL CARD";
                                                            order.setVisibility(View.VISIBLE);

                                                        } else if (finalValue.contains("0$")) {

                                                            if (ss[3].equals("CLB")) {
                                                                memberAn.setText(ss[3] + ":CLUB CARD");
                                                                member = ss[3] + ":CLUB CARD";
                                                                cardType = "CLUB CARD";
                                                                order.setVisibility(View.VISIBLE);

                                                            } else {
                                                                memberAn.setText(ss[3] + ":MEMBER CARD");
                                                                member = ss[3] + ":MEMBER CARD";
                                                                cardType = "MEMBER CARD";
                                                                order.setVisibility(View.VISIBLE);
                                                            }

                                                        } else {
                                                            memberAn.setText("Error:" + finalValue);
                                                            member = "";
                                                            order.setVisibility(View.INVISIBLE);
                                                        }
                                                        mReader.piccPowerOff();
                                                        mReader.sleep();
                                                    }
                                                } catch (Exception e) {
                                                    Toast.makeText(mContext, "Error  1" + e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                            manoch++;
                                            k++;
                                            overAll++;
                                        } else {
                                            mReader.piccPowerOn(5, 143);
                                            overAll++;

                                        }
                                    }
                                });

                            } else if (mResultReady) {

                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        /* Show the result. */
                                        Toast.makeText(mContext,
                                                toErrorCodeString(mResult.getErrorCode()),
                                                Toast.LENGTH_LONG).show();
                                        mProgress.dismiss();
                                    }
                                });

                            } else {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        // finalValue=finalValue+"-11";
                                        /* Show the timeout. */
                                        overAll++;
                                        mReader.piccPowerOn(5, 143);
                                        if (overAll > 8) {
                                            Toast.makeText(mContext, "The operation timed out Read again ", Toast.LENGTH_LONG).show();
                                            mReader.piccPowerOff();
                                            mReader.sleep();
                                        }
                                    }
                                });
                            }

                            mPiccResponseApduReady = false;
                            mResultReady = false;
                        }
                    }

                    if (overAll > 8) {
                        mReader.piccPowerOff();
                        mReader.sleep();
                        mProgress.dismiss();
                        break;
                    }
                }
                /* Hide the progress. */
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mProgress.dismiss();
                    }

                });
            }
        }

    }

    private class OnPiccResponseApduAvailableListener implements AudioJackReader.OnPiccResponseApduAvailableListener {

        @Override
        public void onPiccResponseApduAvailable(AudioJackReader reader,
                                                byte[] responseApdu) {

            synchronized (mResponseEvent) {
                /* Store the PICC response APDU. */
                mPiccResponseApdu = new byte[responseApdu.length];
                System.arraycopy(responseApdu, 0, mPiccResponseApdu, 0,
                        responseApdu.length);
                /* Trigger the response event. */
                mPiccResponseApduReady = true;
                mResponseEvent.notifyAll();
            }
        }
    }

    @Override
    protected void onDestroy() {
        /* Unregister the headset plug receiver. */
        unregisterReceiver(mHeadsetPlugReceiver);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mReader.start();
        } catch (Exception e) {
//            String ss = e.getMessage().toString();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            mReader.start();
        } catch (Exception e) {

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            mReader.stop();
            mProgress.dismiss();
        } catch (Exception e) {

        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            mProgress.dismiss();
            mReader.stop();
        } catch (Exception e) {

        }

    }

    private String toHexString(byte[] buffer) {

        String bufferString = "";

        if (buffer != null) {

            for (int i = 0; i < buffer.length; i++) {

                String hexChar = Integer.toHexString(buffer[i] & 0xFF);
                if (hexChar.length() == 1) {
                    hexChar = "0" + hexChar;
                }

                bufferString += hexChar.toUpperCase(Locale.US) + " ";
            }
        }

        return bufferString;
    }

    private boolean checkResetVolume() {

        boolean ret = true;

        int currentVolume = mAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);

        int maxVolume = mAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        if (currentVolume < maxVolume) {

            showMessageDialog(R.string.info, R.string.message_reset_info_volume);
            ret = false;
        }

        return ret;
    }

    private void showMessageDialog(int titleId, int messageId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setMessage(messageId)
                .setTitle(titleId)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });

        builder.show();
    }

    private String toErrorCodeString(int errorCode) {

        String errorCodeString = null;

        switch (errorCode) {
            case Result.ERROR_SUCCESS:
                errorCodeString = "The operation completed successfully.";
                break;
            case Result.ERROR_INVALID_COMMAND:
                errorCodeString = "The command is invalid.";
                break;
            case Result.ERROR_INVALID_PARAMETER:
                errorCodeString = "The parameter is invalid.";
                break;
            case Result.ERROR_INVALID_CHECKSUM:
                errorCodeString = "The checksum is invalid.";
                break;
            case Result.ERROR_INVALID_START_BYTE:
                errorCodeString = "The start byte is invalid.";
                break;
            case Result.ERROR_UNKNOWN:
                errorCodeString = "The error is unknown.";
                break;
            case Result.ERROR_DUKPT_OPERATION_CEASED:
                errorCodeString = "The DUKPT operation is ceased.";
                break;
            case Result.ERROR_DUKPT_DATA_CORRUPTED:
                errorCodeString = "The DUKPT data is corrupted.";
                break;
            case Result.ERROR_FLASH_DATA_CORRUPTED:
                errorCodeString = "The flash data is corrupted.";
                break;
            case Result.ERROR_VERIFICATION_FAILED:
                errorCodeString = "The verification is failed.";
                break;
            case Result.ERROR_PICC_NO_CARD:
                errorCodeString = "No card in PICC slot.";
                break;
            default:
                errorCodeString = "Error communicating with reader.";
                break;
        }

        return errorCodeString;
    }

    private int toByteArray(String hexString, byte[] byteArray) {

        char c = 0;
        boolean first = true;
        int length = 0;
        int value = 0;
        int i = 0;

        for (i = 0; i < hexString.length(); i++) {

            c = hexString.charAt(i);
            if ((c >= '0') && (c <= '9')) {
                value = c - '0';
            } else if ((c >= 'A') && (c <= 'F')) {
                value = c - 'A' + 10;
            } else if ((c >= 'a') && (c <= 'f')) {
                value = c - 'a' + 10;
            } else {
                value = -1;
            }

            if (value >= 0) {

                if (first) {

                    byteArray[length] = (byte) (value << 4);

                } else {

                    byteArray[length] |= value;
                    length++;
                }

                first = !first;
            }

            if (length >= byteArray.length) {
                break;
            }
        }

        return length;
    }

    private void Intents(String accesstype, String bookingNumber, String debitbal, String memberId, String bookingId, String bmod) {

        Intent i = new Intent(BillingProfile.this, Search_Item.class);
        //Intent i = new Intent(TakeOrder.this, Itemview.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("tableNo", table);
        i.putExtra("accessType", accesstype);
        i.putExtra("creditLimit", bookingNumber);
        i.putExtra("debitbal", debitbal);
        i.putExtra("creditAno", creditAccountno);
        i.putExtra("creditName", creditName);
        i.putExtra("memberId", memberId);
        i.putExtra("tableId", tableId);
        i.putExtra("paxcnt", paxcount);
        i.putExtra("cardType", cardType);
        i.putExtra("referenceNo", bookingId);
        Log.d("contractor8",bookingId);

        i.putExtra("waiterid", waiterid);
        i.putExtra("position", position);
        i.putExtra("waiternamecode", waiternamecode);
        i.putExtra("billmode", bmod);
        i.putExtra("doa", "0");
        i.putExtra("dob", "0");

        i.putExtra("paxcnt", paxcount);
        startActivity(i);
        db.deleteOT();
        finish();

    }

    private byte[] toByteArray(String hexString) {

        byte[] byteArray = null;
        int count = 0;
        char c = 0;
        int i = 0;

        boolean first = true;
        int length = 0;
        int value = 0;

        // Count number of hex characters
        for (i = 0; i < hexString.length(); i++) {

            c = hexString.charAt(i);
            if (c >= '0' && c <= '9' || c >= 'A' && c <= 'F' || c >= 'a'
                    && c <= 'f') {
                count++;
            }
        }

        byteArray = new byte[(count + 1) / 2];
        for (i = 0; i < hexString.length(); i++) {

            c = hexString.charAt(i);
            if (c >= '0' && c <= '9') {
                value = c - '0';
            } else if (c >= 'A' && c <= 'F') {
                value = c - 'A' + 10;
            } else if (c >= 'a' && c <= 'f') {
                value = c - 'a' + 10;
            } else {
                value = -1;
            }

            if (value >= 0) {

                if (first) {

                    byteArray[length] = (byte) (value << 4);

                } else {

                    byteArray[length] |= value;
                    length++;
                }

                first = !first;
            }
        }

        return byteArray;
    }

    public String hexToString(String hex1) {
        StringBuilder sb = new StringBuilder();
        String hex2 = hex1.replaceAll("00", "");
        String hex = hex2.replaceAll("90", "");
        char[] hexData = hex.toCharArray();
        for (int count = 0; count < hexData.length - 1; count += 2) {
            int firstDigit = Character.digit(hexData[count], 16);
            int lastDigit = Character.digit(hexData[count + 1], 16);
            int decimal = firstDigit * 16 + lastDigit;
            sb.append((char) decimal);
        }
        return sb.toString();
    }

    private void showMessage(String text, Context c) {
        Toast toast = Toast.makeText(c, "message", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 10, 50);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setText(text);
        toast.show();
    }

    private void AlertError(String card, String ano, String error) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(card);
        builder.setIcon(R.drawable.card);
        builder.setCancelable(false);
        builder.setMessage("Account:" + ano + "\nError:" + error);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                memberAn.setText("");
                finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }


    protected class AsyncmemberDob extends AsyncTask<String, Void, ArrayList<MemberDob>> {
        ArrayList<MemberDob> arrayList = null;
        String dob = "", doa = "";
        String creditLimit = "", debitbal = "", memberId = "", _params = "", bmod = "", accesstype = "";


        @Override
        protected ArrayList<MemberDob> doInBackground(String... params) {
            RestAPI restAPI = new RestAPI(BillingProfile.this);
            creditLimit = params[1];
            debitbal = params[4];
            memberId = params[0];
            _params = params[2];
            bmod = params[3];
            accesstype = params[5];
            try {
                JSONObject jsonObject = restAPI.cms_getdateDetails(params[0]);
                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    String DOB = jsonObject1.optString("DOB");
                    String DOA = jsonObject1.optString("DOA");
                    MemberDob memberDob = new MemberDob();
                    if (DOA.equals("1900/01/01")) {
                        memberDob.setMemdoa("");
                    } else {
                        memberDob.setMemdoa(DOA);
                    }

                    if (DOB.equals("1900/01/01")) {
                        memberDob.setMemdob("");
                    } else {
                        memberDob.setMemdob(DOB);
                    }

                    arrayList = new ArrayList<>();
                    arrayList.add(memberDob);
                }

            } catch (Exception e) {

            }

            return arrayList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<MemberDob> array) {
            super.onPostExecute(array);
            if (array != null) {
                if (array.size() > 0) {
                    long millis = System.currentTimeMillis();
                    java.sql.Date date = new java.sql.Date(millis);
                    String datenow = date.toString();
                    String[] ary = datenow.split("-");
                    String monthnow = ary[1];
                    String datnow = ary[2];

                    String DOB = array.get(0).getMemdob();
                    String DOA = array.get(0).getMemdoa();
                    // DOB
                    if (!DOB.equals("")) {
                        String[] datearr = DOB.split("/");
                        if (datearr[1].equals(monthnow) && datearr[2].equals(datnow)) {
                            dob = "bday";
                        }
                        //     System.out.println(date);
                    }
                    if (!DOA.equals("")) {
                        String[] datearr = DOA.split("/");
                        if (datearr[1].equals(monthnow) && datearr[2].equals(datnow)) {
                            doa = "aday";
                        }
                    }

                    if (dob.equals("bday") || doa.equals("aday")) {

                        AlertDialog alertDialog = new AlertDialog.Builder(BillingProfile.this).create();

                        // Setting Dialog Title

                        if (dob.equals("bday")) {
                            alertDialog.setTitle("BirthDay");
                        } else if (doa.equals("aday")) {
                            alertDialog.setTitle("Anniversary");
                        }

                        if (dob.equals("bday")) {
                            alertDialog.setMessage("Today is " + creditName.toLowerCase() + " BirthDay");

                        } else if (doa.equals("aday")) {
                            alertDialog.setMessage("Today is " + creditName.toLowerCase() + " Anniversary");

                        }

                        // Setting Dialog Message

                        // Setting Icon to Dialog
                        alertDialog.setIcon(R.drawable.birthd);

                        // Setting OK Button
                        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                new CallProfileAPI().execute(creditAccountno, "4", accesstype, creditLimit, debitbal, memberId, _params, bmod);
                            }
                        });

                        // Showing Alert Message
                        alertDialog.show();
                    }//monica 31-03-2021
                    else {
                        Intent i = new Intent(BillingProfile.this, Search_Item.class);
                        //Intent i = new Intent(TakeOrder.this, Itemview.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("tableNo", table);
                        i.putExtra("accessType", accesstype);
                        i.putExtra("creditLimit", creditLimit);
                        i.putExtra("debitbal", debitbal);
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("creditName", creditName);
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("paxcnt", paxcount);
                        //TODO 31-03-2021         i.putExtra("referenceNo", _params);

                        i.putExtra("referenceNo", bookingId);
                        Log.d("contractor9",bookingId);

                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);
                        i.putExtra("dob", "0");
                        i.putExtra("doa", "0");

                        i.putExtra("paxcnt", paxcount);
                        i.putExtra("TempBillAmount", TempBillAmount);
                        startActivity(i);
                        db.deleteOT();
                        finish();
                    }

                }
            } else {

                Intent i = new Intent(BillingProfile.this, Search_Item.class);
                //Intent i = new Intent(TakeOrder.this, Itemview.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("tableNo", table);
                i.putExtra("accessType", accesstype);
                i.putExtra("creditLimit", creditLimit);
                i.putExtra("debitbal", debitbal);
                i.putExtra("creditAno", creditAccountno);
                i.putExtra("creditName", creditName);
                i.putExtra("memberId", memberId);
                i.putExtra("paxcnt", paxcount);
                i.putExtra("tableId", tableId);
                i.putExtra("cardType", cardType);
                i.putExtra("referenceNo", _params);
                Log.d("contractor10",_params);

                i.putExtra("waiterid", waiterid);
                i.putExtra("position", position);
                i.putExtra("waiternamecode", waiternamecode);
                i.putExtra("billmode", bmod);
                i.putExtra("dob", "0");
                i.putExtra("doa", "0");

                i.putExtra("paxcnt", paxcount);
                i.putExtra("TempBillAmount", TempBillAmount);
                startActivity(i);
                db.deleteOT();
                finish();
            }


        }
    }
}
