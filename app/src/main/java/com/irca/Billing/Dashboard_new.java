package com.irca.Billing;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.cosmo.TakeOrder;
import com.irca.cosmo.TodaysAccount;
import com.irca.db.Dbase;
import com.irca.fields.Item;
import com.irca.fields.ItemGroup;
import com.irca.fields.Table;
import com.irca.fields.WaiterDetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

//import io.fabric.sdk.android.Fabric;

public class Dashboard_new extends AppCompatActivity {

    Dbase db;
    int tablecount = 0;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    SharedPreferences sharedpreferences;
    ProgressDialog pd;
    String posId = "";
    String serialNo = "";
    SharedPreferences bluetooth_prefrence;

    String target = "", Printername = "", billType = "";

    String[] NameList = new String[]{};
    int[] Images = new int[]{};
    int[] colours = new int[]{};
    int[] colours2 = new int[]{};
    GridView gridView;
    CustomGridAdapter gridAdapter;
    String StoreName = "";
    Typeface tf;

    public static ArrayList<WaiterDetails> waiterlist = null;
    WaiterDetails waiterDetails = null;
    public static String pid, pcode, pname;
    public static String ItemCategoryID = "";

    String[] party;
    String _party = "";
    public static ArrayList<ItemGroup> itemGrouplist = null;
    ItemGroup itemGroup = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
   //     Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_dashboard_new);
        initView();
        initGridView();
        actionBarSetup();
        loadTable();

    }

    private void initView() {
        bluetooth_prefrence = getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        target = bluetooth_prefrence.getString("Target", "");
        Printername = bluetooth_prefrence.getString("Printer", "");
        String name = sharedpreferences.getString("loginName", "");
        billType = sharedpreferences.getString("billType", "");
        StoreName = sharedpreferences.getString("StoreName", "");
        posId = sharedpreferences.getString("storeId", "");
    }

    private void initGridView() {
        serialNo = Build.SERIAL;
        NameList = new String[]
                {"Regular",//1
                        "Party Billing",//2
                        "Compliment",//3
                        "coupon",//4
                        "My Order",//5
                        "Menu"//6
                };
        colours = new int[]{
                R.color.g_mint1,
                R.color.g_pink1,
                R.color.g_or1,
                R.color.g_gr1,
                R.color.g_lav1,
                R.color.g_yel1
        };

        colours2 = new int[]
                {
                        R.color.g_mint2,
                        R.color.g_pink2,
                        R.color.g_or2,
                        R.color.g_gr2,

                        R.color.g_lav2,
                        R.color.g_yel12

                };


        Images = new int[]{
                R.drawable.grid_kot_bot, //regular
                R.drawable.grid_partyhall_booking,//party hall
                R.drawable.grid_room_booking,// compliment
                R.drawable.grid_coupon,// coupon
                R.drawable.grid_kitchen,// my order
                R.drawable.grid_kitchen// my order
        };// bluish green


        gridView = (GridView) findViewById(R.id.homepage_gridlayout);
        gridAdapter = new CustomGridAdapter(this, NameList, Images, colours, colours2);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();

                switch (position) {

                    case 0:  //regular

                        loadItem("RR", NameList[position]);
                        break;

                    case 1:  // partyBilling

                        AlertParty();
                        // Toast.makeText(Dashboard_new.this, "Under construction ", Toast.LENGTH_SHORT).show();
                        break;

                    case 2: //compliment
                        loadItem("RR", NameList[position]);
                        //  Toast.makeText(Dashboard_new.this, "Under construction ", Toast.LENGTH_SHORT).show();
                        break;

                    case 3: //coupon
                        loadItem("RR", NameList[position]);
                        // Toast.makeText(Dashboard_new.this, "Under construction ", Toast.LENGTH_SHORT).show();
                        break;

                    case 4:  // my order
                        Intent i = new Intent(Dashboard_new.this, TodaysAccount.class);
                        i.putExtra("mpage", "0");
                        startActivity(i);

                        break;
                    case 5: // menu
                        // Toast.makeText(Dashboard_new.this, "Under construction ", Toast.LENGTH_SHORT).show();
                        final ArrayList<Table> tablename = db.getTablename();
                        int itemcount = db.getItemCount(posId);

                        if (itemcount == 0) {
                            new Dashboard_new.AsyncMenuMaster().execute();
                        } else {
                            Intent i1 = new Intent(Dashboard_new.this, GroupView.class);
                            startActivity(i1);
                        }//

                        break;

                }
            }


        });
    }


    private void AlertParty() {


        party = new String[]{"Party Billing", "Direct Party Billing"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select the Payment Mode");
        builder.setItems(party, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int _paymentMode) {

                _party = party[_paymentMode];
                if (_party.equals("Party Billing")) {
                    loadItem("PR", _party);
                } else {
                    loadItem("BR", _party);
                }

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void loadItem(String billmode, String billing) {

        final ArrayList<Table> tablename = db.getTablename();
        int itemGroupCount = db.getItemGroupCount();
        int waiterCount = db.getWaiterCount();
        int itemcount = db.getItemCount(posId);

        if (waiterCount == 0) {
            new Dashboard_new.AsyncMaster().execute(0 + 1 + "", tablename.get(0).getTableId() + "", billmode, billing);
        } else if (itemcount == 0) {
            new Dashboard_new.AsyncItemMaster().execute(0 + 1 + "", tablename.get(0).getTableId() + "", "0", billmode, billing);
        } else {

            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("BillMode", billmode);
            editor.putString("Bill", billing);
            editor.apply();

            Intent i = new Intent(Dashboard_new.this, Accountlist.class);
            i.putExtra("tableNo", 0 + 1);
            i.putExtra("tableId", tablename.get(0).getTableId() + "");
            i.putExtra("from", TakeOrder.class.getName());
            startActivity(i);
            finish();
        }

    }

    protected class AsyncMaster extends AsyncTask<String, Void, Void> {

        ArrayList<Item> itemList = null;
        Item item = null;
        boolean itembool = false;
        boolean itemgroupbool = false;
        int position = 0;
        int tableId;
        String billMode = "";
        String bill = "";
        String itemCategoryId = sharedpreferences.getString("itemCategoryId", "");

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard_new.this);
            try {

                String gid = "0";
                pid = "0";
                position = Integer.parseInt(params[0]);
                tableId = Integer.parseInt(params[1]);
                billMode = params[2];
                bill = params[3];


                int _itemGroupCount = db.getItemGroupCount(posId);
                int _waiterCount = db.getWaiterCount();
                int _itemcount = db.getItemCount(posId);


                //-------waiter-----------------------

                if (_waiterCount == 0) {

                    JSONObject jsonObject = api.cms_getSteward("","");
                    JSONArray jsonArray = jsonObject.getJSONArray("Value");
                    waiterlist = new ArrayList<WaiterDetails>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        waiterDetails = new WaiterDetails();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        pid = jsonObject1.get("personid").toString();
                        pcode = jsonObject1.get("personcode").toString();
                        pname = jsonObject1.get("FirstName").toString();
                        waiterDetails.setpersonid(pid);
                        waiterDetails.setpersoncode(pcode);
                        waiterDetails.setFirstName(pname);
                        waiterlist.add(waiterDetails);

                    }
                    if (waiterlist != null) {
                        //  db.deleteItemGroup();
                        Long mm = db.insertWaiterGroup(waiterlist);
                        if (mm > 0) {
                            itemgroupbool = true;
                        }

                    }
                }

                //item group

                if (_itemGroupCount == 0) {

                    JSONObject jsonObjectg = api.cms_getItemGroup(itemCategoryId);
                    JSONArray jsonArrayg = jsonObjectg.getJSONArray("Value");
                    itemGrouplist = new ArrayList<ItemGroup>();
                    for (int i = 0; i < jsonArrayg.length(); i++) {
                        itemGroup = new ItemGroup();
                        JSONObject jsonObject1 = jsonArrayg.getJSONObject(i);
                        gid = jsonObject1.get("ID").toString();
                        String gcode = jsonObject1.get("GroupCode").toString();
                        String gname = jsonObject1.get("GroupName").toString();
                        itemGroup.setGroupid(gid);
                        itemGroup.setGroupcode(gcode);
                        itemGroup.setGroupname(gname);
                        itemGrouplist.add(itemGroup);

                    }
                    if (itemGrouplist != null) {
                        db.deleteItemGroup(posId);
                        Long mm = db.insertItemGroup(itemGrouplist, posId);
                        if (mm > 0) {
                            itemgroupbool = true;
                        }

                    }
                }


                if (_itemcount == 0) {


                    JSONObject jsonObject1 = api.cms_getItemlist(itemCategoryId, posId);
                    JSONArray jsonArray1 = jsonObject1.getJSONArray("Value");
                    itemList = new ArrayList<Item>();
                    for (int i = 0; i < jsonArray1.length(); i++) {
                        item = new Item();
                        JSONObject jsonObjectchild = jsonArray1.getJSONObject(i);
                        if (jsonObjectchild.has("ItemGroupID")) {

                            String ItemCategoryID = jsonObjectchild.optString("ItemCategoryID");
                            String ItemCode = jsonObjectchild.optString("ItemCode");
                            // String ItemIdentityID = jsonObjectchild.optString("ItemIdentityID");
                            String ItemIdentityID = jsonObjectchild.optString("ID");
                            String ItemGroupID = jsonObjectchild.optString("ItemGroupID");
                            String ItemName = jsonObjectchild.optString("ItemName");
                            String StoreID = jsonObjectchild.optString("StoreID");
                            String TaxID = jsonObjectchild.optString("TaxID");
                            String Tax = jsonObjectchild.optString("Tax");
                            String unitId = jsonObjectchild.optString("UnitID");
                            String Rate = jsonObjectchild.optString("Rate");
                            String description = jsonObjectchild.optString("description");
                            String itemImage = jsonObjectchild.optString("ItemImage");


                            item.setItemCategoryID(ItemCategoryID);
                            item.setItemcode(ItemCode);
                            item.setItemIdentityID(ItemIdentityID);
                            item.setItemGroupID(ItemGroupID);
                            item.setItemName(ItemName);
                            item.setStoreID(StoreID);
                            item.setTaxID(TaxID);
                            item.setTax(Tax);
                            item.setUnitID(unitId);
                            item.setRate(Rate);
                            item.setDescription(description);
                            item.setItemImage(itemImage);
                            itemList.add(item);
                        } else {
                            int id = i;
                        }

                    }
                    if (itemList != null) {
                        //   db.deleteItem();
                        Long mm = db.insertItem(itemList);
                        if (mm > 0) {
                            itembool = true;
                        }
                    }
                } else {
                    itembool = true;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_new.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            pd.dismiss();

            if (itembool) {

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("BillMode", billMode);
                editor.putString("Bill", bill);
                editor.apply();

                Intent i = new Intent(Dashboard_new.this, Accountlist.class);
                i.putExtra("tableNo", position);
                i.putExtra("tableId", tableId);
                i.putExtra("from", TakeOrder.class.getName());
                //   i.putExtra("position",positionn);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(Dashboard_new.this, "Error", Toast.LENGTH_LONG).show();
            }
        }
    }

    protected class AsyncMenuMaster extends AsyncTask<String, Void, Void> {

        ArrayList<Item> itemList = null;
        Item item = null;
        boolean itembool = false;
        boolean itemgroupbool = false;
        int position = 0;
        String itemCategoryId = sharedpreferences.getString("itemCategoryId", "");


        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard_new.this);
            try {


                String gid = "0";


                //item group
                JSONObject jsonObjectg = api.cms_getItemGroup(itemCategoryId);
                JSONArray jsonArrayg = jsonObjectg.getJSONArray("Value");
                itemGrouplist = new ArrayList<ItemGroup>();
                for (int i = 0; i < jsonArrayg.length(); i++) {
                    itemGroup = new ItemGroup();
                    JSONObject jsonObject1 = jsonArrayg.getJSONObject(i);
                    gid = jsonObject1.get("ID").toString();
                    String gcode = jsonObject1.get("GroupCode").toString();
                    String gname = jsonObject1.get("GroupName").toString();
                    itemGroup.setGroupid(gid);
                    itemGroup.setGroupcode(gcode);
                    itemGroup.setGroupname(gname);
                    itemGrouplist.add(itemGroup);

                }
                if (itemGrouplist != null) {
                    db.deleteItemGroup(posId);
                    Long mm = db.insertItemGroup(itemGrouplist, posId);
                    if (mm > 0) {
                        itemgroupbool = true;
                    }

                }


                //item list
                JSONObject jsonObject1 = api.cms_getItemlist(itemCategoryId, posId);
                JSONArray jsonArray1 = jsonObject1.getJSONArray("Value");
                itemList = new ArrayList<Item>();
                for (int i = 0; i < jsonArray1.length(); i++) {
                    item = new Item();
                    JSONObject jsonObjectchild = jsonArray1.getJSONObject(i);
                    if (jsonObjectchild.has("ItemGroupID")) {

                        String ItemCategoryID = jsonObjectchild.optString("ItemCategoryID");
                        String ItemCode = jsonObjectchild.optString("ItemCode");
                        //  String ItemIdentityID = jsonObjectchild.optString("ItemIdentityID");
                        String ItemIdentityID = jsonObjectchild.optString("ID");
                        String ItemGroupID = jsonObjectchild.optString("ItemGroupID");
                        String ItemName = jsonObjectchild.optString("ItemName");
                        String StoreID = jsonObjectchild.optString("StoreID");
                        String TaxID = jsonObjectchild.optString("TaxID");
                        String Tax = jsonObjectchild.optString("Tax");
                        String unitId = jsonObjectchild.optString("UnitID");
                        String Rate = jsonObjectchild.optString("Rate");
                        String description = jsonObjectchild.optString("description");
                        String itemImage = jsonObjectchild.optString("ItemImage");


                        item.setItemCategoryID(ItemCategoryID);
                        item.setItemcode(ItemCode);
                        item.setItemIdentityID(ItemIdentityID);
                        item.setItemGroupID(ItemGroupID);
                        item.setItemName(ItemName);
                        item.setStoreID(StoreID);
                        item.setTaxID(TaxID);
                        item.setTax(Tax);
                        item.setUnitID(unitId);
                        item.setRate(Rate);
                        item.setDescription(description);
                        item.setItemImage(itemImage);
                        itemList.add(item);
                    } else {
                        int id = i;
                    }

                }
                if (itemList != null) {
                    // db.deleteItem();
                    Long mm = db.insertItem(itemList);
                    if (mm > 0) {
                        itembool = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_new.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            pd.dismiss();

            if (itembool == true) {
                Intent i1 = new Intent(Dashboard_new.this, GroupView.class);
                startActivity(i1);
            } else {
                Toast.makeText(Dashboard_new.this, "Error", Toast.LENGTH_LONG).show();
            }
        }
    }

    protected class AsyncItemMaster extends AsyncTask<String, Void, Void> {

        ArrayList<Item> itemList = null;
        Item item = null;
        boolean itembool = false;
        boolean itemgroupbool = false;
        int position = 0;
        int tableId;
        String billMode = "";
        String bill = "";
        String itemCategoryId = sharedpreferences.getString("itemCategoryId", "");

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard_new.this);
            try {
                position = Integer.parseInt(params[0]);
                tableId = Integer.parseInt(params[1]);

                String gid = "0";

                billMode = params[3];
                bill = params[4];

                int _itemGroupCount = db.getItemGroupCount(posId);

                if (_itemGroupCount == 0) {

                    JSONObject jsonObjectg = api.cms_getItemGroup(itemCategoryId);
                    JSONArray jsonArrayg = jsonObjectg.getJSONArray("Value");
                    itemGrouplist = new ArrayList<ItemGroup>();
                    for (int i = 0; i < jsonArrayg.length(); i++) {
                        itemGroup = new ItemGroup();
                        JSONObject jsonObject1 = jsonArrayg.getJSONObject(i);
                        gid = jsonObject1.get("ID").toString();
                        String gcode = jsonObject1.get("GroupCode").toString();
                        String gname = jsonObject1.get("GroupName").toString();
                        itemGroup.setGroupid(gid);
                        itemGroup.setGroupcode(gcode);
                        itemGroup.setGroupname(gname);
                        itemGrouplist.add(itemGroup);

                    }
                    if (itemGrouplist != null) {
                        db.deleteItemGroup(posId);
                        Long mm = db.insertItemGroup(itemGrouplist, posId);
                        if (mm > 0) {
                            itemgroupbool = true;
                        }

                    }
                }


                //item list
                JSONObject jsonObject1 = api.cms_getItemlist(itemCategoryId, posId);
                JSONArray jsonArray1 = jsonObject1.getJSONArray("Value");
                itemList = new ArrayList<Item>();
                for (int i = 0; i < jsonArray1.length(); i++) {
                    item = new Item();
                    JSONObject jsonObjectchild = jsonArray1.getJSONObject(i);
                    if (jsonObjectchild.has("ItemGroupID")) {

                        String ItemCategoryID = jsonObjectchild.optString("ItemCategoryID");
                        String ItemCode = jsonObjectchild.optString("ItemCode");
                        //  String ItemIdentityID = jsonObjectchild.optString("ItemIdentityID");
                        String ItemIdentityID = jsonObjectchild.optString("ID");
                        String ItemGroupID = jsonObjectchild.optString("ItemGroupID");
                        String ItemName = jsonObjectchild.optString("ItemName");
                        String StoreID = jsonObjectchild.optString("StoreID");
                        String TaxID = jsonObjectchild.optString("TaxID");
                        String Tax = jsonObjectchild.optString("Tax");
                        String unitId = jsonObjectchild.optString("UnitID");
                        String Rate = jsonObjectchild.optString("Rate");
                        String description = jsonObjectchild.optString("description");
                        String itemImage = jsonObjectchild.optString("ItemImage");


                        item.setItemCategoryID(ItemCategoryID);
                        item.setItemcode(ItemCode);
                        item.setItemIdentityID(ItemIdentityID);
                        item.setItemGroupID(ItemGroupID);
                        item.setItemName(ItemName);
                        item.setStoreID(StoreID);
                        item.setTaxID(TaxID);
                        item.setTax(Tax);
                        item.setUnitID(unitId);
                        item.setRate(Rate);
                        item.setDescription(description);
                        item.setItemImage(itemImage);
                        itemList.add(item);
                    } else {
                        int id = i;
                    }

                }
                if (itemList != null) {
                    // db.deleteItem();
                    Long mm = db.insertItem(itemList);
                    if (mm > 0) {
                        itembool = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_new.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            pd.dismiss();

            if (itembool == true) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("BillMode", billMode);
                editor.putString("Bill", bill);
                editor.apply();

                Intent i = new Intent(Dashboard_new.this, Accountlist.class);
                i.putExtra("tableNo", position);
                i.putExtra("tableId", tableId);
                i.putExtra("from", TakeOrder.class.getName());
                startActivity(i);
                finish();
            } else {
                Toast.makeText(Dashboard_new.this, "Error", Toast.LENGTH_LONG).show();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("" + StoreName);
            ab.setElevation(0);

        }
    }

    private void loadTable() {
        db = new Dbase(Dashboard_new.this);
        cd = new ConnectionDetector(Dashboard_new.this);
        tablecount = db.getTableCount();
        if (tablecount == 0) {
            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent || !isInternetPresent) {
                new Dashboard_new.AsyncTablename().execute();

            } else {
                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
            }

        }

    }

    protected class AsyncTablename extends AsyncTask<String, Void, ArrayList<Table>> {

        ArrayList<Table> tablename = null;
        String storeId = sharedpreferences.getString("storeId", "");

        @Override
        protected ArrayList<Table> doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard_new.this);
            try {

                String[] narr_kitchen = new String[]{"Salt", "Sugar", "More Salt", "Less Salt", "More Sugar", "Less Sugar", "More Spice", "Medium Spice", "Less Spice"};
                //   String[] narr_bar=new String[]{"Salt","Sugar","More Salt","Less Salt","More Sugar","Less Sugar","More Spice","Medium Spice","Less Spice"};
                //  Bundle b=getIntent().getExtras();
                //   positionn=b.getInt("position");
                //int storeId=b.getInt("Storeid");
                // JSONObject jsonObject=api.cms_getTable(storeId);//1150511156
                // JSONArray jsonArray=jsonObject.getJSONArray("Value");
                tablename = new ArrayList<>();
                Table table = null;
                for (int i = 1; i < 21; i++) {
                    table = new Table();
                    // JSONObject jsonObject1=jsonArray.getJSONObject(i);
                    // String tableN=jsonObject1.get("TableNumber").toString();
                    //  String tableid=jsonObject1.get("TableId").toString();
                    table.setTableId(i);
                    table.setTableNmae("" + i);
                    tablename.add(table);
                }


                for (int j = 0; j < narr_kitchen.length; j++) {
                    long i = db.insertNarration(narr_kitchen[j], "KOT");
                }

                if (tablename != null) {
                    db.deleteTable();
                    db.insertTables(tablename);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return tablename;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_new.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(final ArrayList<Table> s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s != null) {
                Toast.makeText(Dashboard_new.this, "table loaded ", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class AsyncPrinterTest extends AsyncTask<String, Void, Void> {
        String status = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_new.this);
            pd.show();
            pd.setMessage("Please wait loading....");

        }

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard_new.this);
            try {

                JSONObject jsonObject1 = api.printTest(params[0], params[1], params[2]);
                // JSONArray jsonArray1=jsonObject1.getJSONArray("Value");
                status = jsonObject1.toString();

            } catch (Exception e) {
                String r = e.getMessage().toString();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            if (status.contains("true")) {
                Toast.makeText(getApplication(), "Printer connected ", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplication(), "Printer  not  connected ", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void Refresh() {
        new AlertDialog.Builder(Dashboard_new.this)
                .setTitle("Refresh POS")
                .setMessage("Do you want to Refresh?")
                .setPositiveButton("Refresh",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                int count = db.getAccountCount(posId);
                                if (count > 0) {
                                    Toast.makeText(getApplicationContext(), "Please close the Member order in POS", Toast.LENGTH_LONG).show();
                                } else {
                                    db.deleteTable();
                                    db.deleteItem(posId);
                                    db.deleteItemGroup(posId);

                                    Intent back = new Intent(Dashboard_new.this, MainActivity.class);
                                    back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(back);
                                    finish();

                                }

                            }
                        })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    private void StewardRefresh() {
        new AlertDialog.Builder(Dashboard_new.this)
                .setTitle("Steward Refresh")
                //  .setMessage("Do you want to Logout?")
                .setMessage("Do you want to Refresh?")
                //  .setIcon(R.drawable.ninja)
                .setPositiveButton("Steward Refresh",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                int count = db.getAccountCount(posId);
                                if (count > 0) {
                                    Toast.makeText(getApplicationContext(), "Please close the Member order in POS", Toast.LENGTH_LONG).show();
                                } else {

                                    db.deleteSteward();

                                    Intent back = new Intent(Dashboard_new.this, MainActivity.class);
                                    back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(back);
                                    finish();

                                }

                            }
                        })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        Logout();
    }

    private void Logout() {
        new AlertDialog.Builder(Dashboard_new.this)
                .setMessage("Do you want to Logout?")
                .setPositiveButton("Logout",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                Intent back = new Intent(Dashboard_new.this, MainActivity.class);
                                back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(back);
                                finish();
                            }
                        })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menu_settings:

                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {

                    db.deleteStore();

                    Intent i = new Intent(Dashboard_new.this, StoreList.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();

                } else {
                    Intent i = new Intent(Dashboard_new.this, StoreList.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }

                return true;

            case R.id.menu_printer:

                new Dashboard_new.AsyncPrinterTest().execute(posId, "", serialNo);
                return true;

            case R.id.menu_refresh:
                Refresh();
                return true;
            case R.id.menu_refresh_steward:
                StewardRefresh();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
