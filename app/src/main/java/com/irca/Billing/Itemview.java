package com.irca.Billing;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.CustomAdapters.ItemGridViewAdapter;
import com.irca.Utils.ShowMsg;
import com.irca.cosmo.R;
import com.irca.cosmo.Search_Item;
import com.irca.db.Dbase;
import com.irca.fields.Item;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

//import io.fabric.sdk.android.Fabric;

public class Itemview extends AppCompatActivity {
    Toolbar toolbar;
    private GridView gridView;
    private ArrayList<String> stringArrayList;
    private ItemGridViewAdapter adapter;
    private ItemGridViewAdapter_new adapter_new;

    SharedPreferences sharedPreferences;
    String itemCategoryId = "", storeName = "";
    Dbase db;
    List<String> itemList;
    ArrayList<Item> itemList_new;
    TextView items, confirmOrder;

    Bundle bundle;
    int tableId;

    String table = "0";
    String paxcnt = "0";
    String creditLimit = "",debitbal = "", creditAccountno = "", accesstype = "",creditName = "", memberId = "", cardType = "", _params = "", waiterid = "", waiternamecode = "";
    int position;
    Spinner spinner;
    ArrayList<String> list;
    ArrayAdapter arrayAdapter;
    String posId = "";
    String bill = "";
    ArrayList<Item> ItemgroupId;
    List<String> _ItemgroupId;
    Context c;
    String Add = "";
    String itemName = "";
    String price = "";
    Double value4;
    public static double amt = 0.00;
    long rvalur = 0;
    String groupId = "";
    String billmode = "";
    LinearLayout nameSearch;

    public static Itemview itemViewInstance = null;

    @Override
    public void finish() {
        super.finish();
        itemViewInstance = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_itemview);
        c = this;
        itemViewInstance = this;
        bundle = getIntent().getExtras();
        if (bundle != null) {
            table = bundle.getString("tableNo");
            creditLimit = bundle.getString("creditLimit");
            debitbal = bundle.getString("debitbal");
            creditAccountno = bundle.getString("creditAno");
            creditName = bundle.getString("creditName");
            memberId = bundle.getString("memberId");
            paxcnt = bundle.getString("paxcnt");
            accesstype = bundle.getString("accessType");
            tableId = bundle.getInt("tableId");
            cardType = bundle.getString("cardType");
            _params = bundle.getString("referenceNo");
            waiterid = bundle.getString("waiterid");
            position = bundle.getInt("position");
            waiternamecode = bundle.getString("waiternamecode");
            billmode = bundle.getString("billmode");
        }
        db = new Dbase(Itemview.this);
        gridView = (GridView) findViewById(R.id.list_item);
        spinner = (Spinner) findViewById(R.id.itemcategory);
        items = (TextView) findViewById(R.id.items_count);
        confirmOrder = (TextView) findViewById(R.id.confirmorder);
        sharedPreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        nameSearch = (LinearLayout) findViewById(R.id.name);

        storeName = sharedPreferences.getString("StoreName", "");
        itemCategoryId = sharedPreferences.getString("itemCategoryId", "");
        posId = sharedPreferences.getString("storeId", "");
        bill = sharedPreferences.getString("Bill", "");


        list = db.getGroupList(posId);
        arrayAdapter = new ArrayAdapter(Itemview.this, android.R.layout.simple_list_item_1, list);
        spinner.setAdapter(arrayAdapter);

        try {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("" + storeName + " \n " + bill);
            getSupportActionBar().setElevation(0);
        } catch (Exception e) {
            ShowMsg.showException(e, "toolbar", Itemview.this);
        }


        // itemList=db.getSearchItemlist("", itemCategoryId);
        itemList_new = db.getSearchItemlist_new("", itemCategoryId, "ALL");
        setData();

        // adapter = new ItemGridViewAdapter(Itemview.this, R.layout.grid_itemview, itemList);
        adapter_new = new ItemGridViewAdapter_new(this, R.layout.grid_itemview, itemList_new);
        gridView.setAdapter(adapter_new);


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                {
                    String itemValue = (String) spinner.getItemAtPosition(position);
                    db = new Dbase(Itemview.this);
                    groupId = (db.getGroupId(itemValue).equals("")) ? "ALL" : db.getGroupId(itemValue);

                    //ItemgroupId=new ArrayList<Item>();
                    //   _ItemgroupId=db.getSearchItemlist("",itemCategoryId,groupId);
                    ItemgroupId = db.getSearchItemlist_new("", itemCategoryId, groupId);

                    //adapter = new ItemGridViewAdapter(Itemview.this, R.layout.grid_itemview, _ItemgroupId);
                    adapter_new = new ItemGridViewAdapter_new(Itemview.this, R.layout.grid_itemview, ItemgroupId);
                    gridView.setAdapter(adapter_new);


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        nameSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Itemview.this, Search_Item.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("tableNo", table);
                i.putExtra("creditLimit", "-0");           // coupon no credit check in place order  so given -0to place order without validating for credit limit
                i.putExtra("debitbal", debitbal);           // coupon no credit check in place order  so given -0to place order without validating for credit limit
                i.putExtra("creditAno", creditAccountno);
                i.putExtra("creditName", creditName);
                i.putExtra("memberId", memberId);
                i.putExtra("tableId", tableId);
                i.putExtra("cardType", cardType);
                i.putExtra("referenceNo", _params);
                i.putExtra("waiterid", waiterid);
                i.putExtra("position", position);
                i.putExtra("waiternamecode", waiternamecode);
                i.putExtra("billmode", billmode);
                i.putExtra("dob", "0");
                i.putExtra("doa", "0");
                i.putExtra("paxcnt", "paxcnt");
                i.putExtra("accessType", accesstype);

                startActivity(i);
                finish();
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {

                // Itemnamecode =ItemgroupId.get(position).getItemName()+"_"+ItemgroupId.get(position).getItemcode();
                // itemName =ItemgroupId.get(position).toString();
                itemName = ItemgroupId.get(position).getItemName();
                if (itemName.contains("_")) {
                    String[] it = itemName.split("_");
                    price = db.getItemPrice(it[1], "R");
                } else {
                    price = "0";

                }

                amt = Double.parseDouble(price);
                value4 = Double.parseDouble(new DecimalFormat("#.##").format(amt));

                AlertDialog.Builder alert = new AlertDialog.Builder(Itemview.this);
                final EditText edittext = new EditText(Itemview.this);
                alert.setMessage("Enter Quantity");
                alert.setTitle("Item Name : " + itemName + "   (Rs." + value4 + ")");
                alert.setView(edittext);
            //    edittext.setInputType(InputType.TYPE_CLASS_NUMBER);
                edittext.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //What ever you want to do with the value
                        //  Editable YouEditTextValue = edittext.getText();
                        //OR
                        Add = edittext.getText().toString();

                        String _itemName = itemName;
                        //String itemName = "";
                        String count = Add;

                        if (!(_itemName.equals("") || count.equals("") || count.equals("0")|| count.equals("\\.")|| count.contains("+"))) {
                            if (_itemName.contains("_")) {
                                String array[] = _itemName.split("_");
                                //  Long rvalur = db.insertOT(array[1], array[0], count);

                                try {
                                    String itemCategoryId = sharedPreferences.getString("itemCategoryId", "");
                                    String storeId = sharedPreferences.getString("storeId", "");
                                    String rate = db.getRate(array[1], storeId);
                                    rvalur = db.insertOT1(array[1], array[0], count, rate);

                                    if (rvalur < 0) {

                                        Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                    } else {
                                        items.setText("" + rvalur + "Items  \n \n View Order ......");
                                        Toast.makeText(Itemview.this, "Item  added", Toast.LENGTH_SHORT).show();

                                    }

                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), "" + e.toString(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(Itemview.this, "Give Valid Item Name", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "Dont give Invalid values", Toast.LENGTH_LONG).show();
                        }


                        //Toast.makeText(Itemview.this, "Item Added", Toast.LENGTH_SHORT).show();
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // what ever you want to do with No option.
                    }
                });

                alert.show();


            }
        });


        items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Itemview.this, CustomAddedItems.class);
                startActivity(intent);
            }
        });
        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Intent i=new Intent(Itemview.this,PlaceOrder.class);
                //Intent i=new Intent(Itemview.this,PlaceOrder_new.class);
                Intent i = new Intent(Itemview.this, MakeOrder.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("tableNo", table);
                i.putExtra("creditLimit", creditLimit);
                i.putExtra("debitbal", debitbal);
                i.putExtra("creditAno", creditAccountno);
                i.putExtra("creditName", creditName);
                i.putExtra("memberId", memberId);
                i.putExtra("tableId", tableId);
                i.putExtra("cardType", cardType);
                i.putExtra("referenceNo", _params);
                i.putExtra("waiterid", waiterid);
                i.putExtra("position", position);
                i.putExtra("waiternamecode", waiternamecode);
                i.putExtra("billmode", billmode);
                i.putExtra("paxcnt", paxcnt);
                i.putExtra("accessType", accesstype);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Itemview.this, MakeOrder.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("tableNo", table);
        i.putExtra("creditLimit", creditLimit);
        i.putExtra("creditAno", creditAccountno);
        i.putExtra("creditName", creditName);
        i.putExtra("memberId", memberId);
        i.putExtra("tableId", tableId);
        i.putExtra("cardType", cardType);
        i.putExtra("referenceNo", _params);
        i.putExtra("waiterid", waiterid);
        i.putExtra("position", position);
        i.putExtra("waiternamecode", waiternamecode);
        i.putExtra("billmode", billmode);
        i.putExtra("paxcnt", paxcnt);
        i.putExtra("accessType", accesstype);
        startActivity(i);
        finish();
        /*Intent i=new Intent(Itemview.this, Accountlist.class);
        i.putExtra("tableNo",table);
        i.putExtra("tableId",tableId);
        i.putExtra("waiterid",waiterid);
        i.putExtra("position",position);
        i.putExtra("waiternamecode",waiternamecode);
        startActivity(i);
        finish();*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                Intent i = new Intent(Itemview.this, MakeOrder.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("tableNo", table);
                i.putExtra("creditLimit", creditLimit);
                i.putExtra("creditAno", creditAccountno);
                i.putExtra("creditName", creditName);
                i.putExtra("memberId", memberId);
                i.putExtra("tableId", tableId);
                i.putExtra("cardType", cardType);
                i.putExtra("referenceNo", _params);
                i.putExtra("waiterid", waiterid);
                i.putExtra("position", position);
                i.putExtra("waiternamecode", waiternamecode);
                i.putExtra("billmode", billmode);
                i.putExtra("paxcnt", paxcnt);
                i.putExtra("accessType", accesstype);
                startActivity(i);
                finish();
               /* Intent i=new Intent(Itemview.this, Accountlist.class);
                i.putExtra("tableNo",table);
                i.putExtra("tableId",tableId);
                i.putExtra("waiterid",waiterid);
                i.putExtra("position",position);
                i.putExtra("waiternamecode",waiternamecode);
                startActivity(i);
                finish();*/
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void setData() {
        stringArrayList = new ArrayList<>();
        stringArrayList.add("Quynh Trang");
        stringArrayList.add("Hoang Bien");
        stringArrayList.add("Duc Tuan");
        stringArrayList.add("Dang Thanh");
        stringArrayList.add("Xuan Luu");
        stringArrayList.add("Phan Thanh");
        stringArrayList.add("Kim Kien");
        stringArrayList.add("Ngo Trang");
        stringArrayList.add("Thanh Ngan");
        stringArrayList.add("Nguyen Duong");
        stringArrayList.add("Quoc Cuong");
        stringArrayList.add("Tran Ha");
        stringArrayList.add("Vu Danh");
        stringArrayList.add("Minh Meo");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_itemview, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    adapter_new.filter("");
                    gridView.clearTextFilter();
                } else {
                    adapter_new.filter(newText);
                }
                return true;
            }
        });

        return true;

    }
}
