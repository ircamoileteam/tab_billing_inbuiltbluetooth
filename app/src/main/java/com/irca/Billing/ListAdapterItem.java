package com.irca.Billing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.fields.Itemadapter;

import java.util.ArrayList;

/**
 * Created by Shwetha on 24-Nov-16.
 */
public class ListAdapterItem extends BaseAdapter {

    ArrayList<Itemadapter> masterStore;
    Context context;
    ListAdapterItem(Context c, ArrayList<Itemadapter> masterStoreArrayAdapter)
    {
        this.masterStore=masterStoreArrayAdapter;
        context=c;
    }
    @Override
    public int getCount()
    {
        return masterStore.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.customitemlist, parent,false);

        }


        TextView itemName = (TextView)convertView.findViewById(R.id.itemname);
        itemName.setText(masterStore.get(position).getItemName());
        TextView quantity = (TextView)convertView.findViewById(R.id.codeitem);
        quantity.setText(masterStore.get(position).getQuantity());


      //  itemName.setText(masterStore.get(position).getItemName());

      //  quantity.setText(masterStore.get(position).getQuantity());



        return convertView;
    }
}
