package com.irca.Billing;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.content.res.AppCompatResources;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.fields.Item;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Archanna on 2/3/2017.
 */

public class ItemGridViewAdapter_new extends ArrayAdapter<Item> {

    private Context activity;
    private final int resource;
    // private List<String> friendList;
    private ArrayList<Item> friendList;
    final int Dummy = R.mipmap.ic_launcher;

    //  private List<String> searchList;
    private ArrayList<Item> searchList;
    ArrayList<Item> items;
    ArrayList<Item> lists = new ArrayList<>();
    String base = "";


    public ItemGridViewAdapter_new(Context context, int resource, ArrayList<Item> list) {
        super(context, resource, list);
        // super(context, resource, lists);

        this.activity = context;
        this.resource = resource;
        this.friendList = list;
        this.items = list;
        this.searchList = new ArrayList<>();
        this.searchList.addAll(friendList);
    }

    @Override
    public int getCount() {
        return friendList.size();
    }

    @Nullable
    @Override
    public Item getItem(int position) {

        return friendList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemGridViewAdapter_new.ViewHolder holder;

        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        // If holder not exist then locate all view from UI file.

        if (convertView == null) {

            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.grid_itemview, parent, false);
            // get all UI view
            holder = new ItemGridViewAdapter_new.ViewHolder(convertView);
            // set tag for holder
            convertView.setTag(holder);
        } else {
            // if holder created, get tag from view
            holder = (ItemGridViewAdapter_new.ViewHolder) convertView.getTag();
        }


        try {


            // holder.friendName.setText(getItem(position));
            holder.friendName.setText(friendList.get(position).getItemName());

            base = friendList.get(position).getItemImage();
            try {
                holder.text_viewName1.setText(getChars(friendList.get(position).getItemName().toUpperCase(), 2));
            } catch (Throwable e) {
                e.printStackTrace();
            }

            if (base.isEmpty()) {
                holder.text_viewName1.setVisibility(View.VISIBLE);
                holder.imageView.setVisibility(View.GONE);
                Drawable unwrappedDrawable = AppCompatResources.getDrawable(activity, R.drawable.bg_dynamic_circle);
                assert unwrappedDrawable != null;
                Drawable wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable);
                int[] androidColors1 = activity.getResources().getIntArray(R.array.androidColors);
                int randomAndroidColor1 = androidColors1[new Random().nextInt(androidColors1.length)];
                DrawableCompat.setTint(wrappedDrawable, randomAndroidColor1);
            } else {
                holder.text_viewName1.setVisibility(View.GONE);
                holder.imageView.setVisibility(View.VISIBLE);
                Picasso.get().load(base).into(holder.imageView);
//                byte[] imageAsBytes = Base64.decode(base.getBytes(), Base64.DEFAULT);
//                image.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
            }

        } catch (Exception e) {
            String r = e.getMessage().toString();
        }


        //get first letter of each String item
        //  String firstLetter = String.valueOf(getItem(position).charAt(0));

       /* ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color = generator.getColor(getItem(position));

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(firstLetter, color); // radius in px*/

        //holder.imageView.setImageDrawable();

        return convertView;
    }

    private String getChars(String str, int number) {
        try {

            if (str.length() < number) {
                return str;
            } else {
                return str.substring(0, number);
            }

        } catch (Throwable e) {
            e.printStackTrace();
            return str;
        }
    }

    // Filter method
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());

        friendList.clear();

        if (charText.length() == 0) {
            friendList.addAll(searchList);
        } else {
           /* for (String s : searchList)
            {
                if (s.toLowerCase(Locale.getDefault()).contains(charText))
                {
                    friendList.add(s);
                }
            }*/

            for (Item s : searchList) {
                if (s.getItemName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    friendList.add(s);
                }
            }
        }
        notifyDataSetChanged();
    }

    private class ViewHolder {
        private ImageView imageView;
        private TextView friendName;
        private TextView text_viewName1;

        public ViewHolder(View v) {
            imageView = v.findViewById(R.id.image_view);
            friendName = v.findViewById(R.id.text);
            text_viewName1 = v.findViewById(R.id.text_viewName1);
        }
    }
}