package com.irca.Billing;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.db.Dbase;
import com.irca.fields.CloseorderItems;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
/*

import io.fabric.sdk.android.Fabric;
*/

import static com.irca.Billing.BillingProfile.cardType;
import static com.irca.Billing.BillingProfile.waiternamecode;

/**
 * Created by Manoch Richard on 17-Mar-18.
 */

public class Ottransfer extends Activity {
    LinearLayout ll_ottransafer;
    FrameLayout frameLayout;
    ArrayList<String> al = new ArrayList<String>();
    ProgressDialog pd;
    SharedPreferences sharedpreferences;
    String bill = "", billMode = "", memberid;
    RestAPI api = new RestAPI(Ottransfer.this);
    ArrayList<String> otlist = new ArrayList<String>();
    ImageView img_transfer;
    EditText etxt_eccno;
    Dbase dbase =null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.ot_transafer);
        ll_ottransafer = (LinearLayout) findViewById(R.id.ll_chbox);
        frameLayout = (FrameLayout) findViewById(R.id.ll_accno);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        Bundle bundle = getIntent().getExtras();
        bill = bundle.getString("bill");
        billMode = bundle.getString("billmode");
        memberid = bundle.getString("memberid");
        frameLayout.setVisibility(View.INVISIBLE);
        img_transfer= (ImageView) findViewById(R.id.img_transfer);
        etxt_eccno= (EditText) findViewById(R.id.etxt_ottransfer_accno);
        if (bill != null && billMode != null && memberid != null) {

            new AsyncOt().execute(memberid);
        }
        img_transfer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                if (otlist.size() > 0)
                {
                    String accno=etxt_eccno.getText().toString();
                    new AsyncOttransafer().execute(accno);

                } else {
                    Toast.makeText(getApplicationContext(), "No ot checked", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    protected class AsyncOt extends AsyncTask<String, Void, String> {
        String status = "";
        String storeId = sharedpreferences.getString("storeId", "");
        String deviceid = sharedpreferences.getString("deviceId", "");
        String userId = sharedpreferences.getString("userId", "");
        String pageType = "";
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... params) {

            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                al.clear();
                if (bill.equals("coupon")) {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "MEMBER CARD", billMode);
                } else if (bill.equals("Party Billing")) {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "PARTY CARD", billMode);

                } else if (bill.equals("Direct Party Billing")) {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "DIRECTPARTY CARD", billMode);

                } else {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, cardType, billMode);
                }
                JSONArray array = jsonObject.getJSONArray("Value");
                CloseorderItems cl = null;
                //pageType = params[1];
                for (int i = 0; i < array.length(); i++) {
                    cl = new CloseorderItems();
                    JSONObject object = array.getJSONObject(i);
                    // String ItemID = object.optString("ItemID");
                    //String ItemCode = object.optString("ItemCode");
                    // String Quantity = object.optString("Quantity");
                    // String Amount = object.optString("Amount");
                    // String BillID = object.optString("BillID");
                    //String mAcc = object.getString("memberAcc");
                    // String billNo = object.optString("billnumber");
                    // String Rate = object.getString("rate");
                    // String itemname = object.getString("itemname");
                    // String mName = object.getString("membername");
                    // String memberId = object.getString("memberId");
                    // String BillDate = object.getString("BillDate");
                    String otNo = object.getString("otno");
                    //db.insertOtTransfer(BillID  ,ItemID ,otNo ,Quantity ,Amount ,Rate);
                    al.remove(otNo);
                    al.add(otNo);
                }
                status = jsonObject.toString();

            } catch (Exception e) {
                e.printStackTrace();
                status = e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Ottransfer.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            for (int i = 0; i < al.size(); i++) {
                CheckBox checkBox = new CheckBox(Ottransfer.this);
                checkBox.setId(i);
                checkBox.setText(al.get(i));
                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //call server andupdate ot
                        //String otno=view.getTeview();
                        CheckBox CheckBox = (CheckBox) view;
                        if (CheckBox.isChecked()) {

                            otlist.add(CheckBox.getText().toString());
                        } else {
                            otlist.remove(CheckBox.getText().toString());
                        }

                    }
                });
                ll_ottransafer.addView(checkBox);
            }
            if (al.size() > 0)
            {
//                Button button = new Button(Ottransfer.this);
//                button.setText("OT Transfer");
//                button.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view)
//                    {
//                        if (otlist.size() > 0)
//                        {
//                            new AsyncOttransafer().execute();
//
//                        } else {
//                            Toast.makeText(getApplicationContext(), "No ot checked", Toast.LENGTH_LONG).show();
//                        }
//
//                    }
//                });
//                ll_ottransafer.addView(button);
                frameLayout.setVisibility(View.VISIBLE);
            }
            pd.dismiss();

        }
    }

    private class AsyncOttransafer extends AsyncTask<String, Void, String> {
        JSONObject jsonObject = null;
        long up=0;
        String userId = sharedpreferences.getString("userId", "");
        @Override
        protected String doInBackground(String... strings) {
            try {
                jsonObject = api.cms_updateOTtransfer(otlist, memberid, strings[0]);
                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                if(jsonArray.length()>0)
                {
                    dbase = new Dbase(Ottransfer.this);
                    for (int i = 0; i < 1; i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String Memberid = jsonObject.optString("Memberid");
                        String Accno = jsonObject.optString("Accno");
                        String Billid = jsonObject.optString("Billid");
                        String loginId=jsonObject.optString("loginId");
                        String posId=jsonObject.optString("posId");
                        String accountNum=jsonObject.optString("accountNum");
                        String tdate=jsonObject.optString("tdate");
                        String ctype=jsonObject.optString("ctype");
                        String opBalance=jsonObject.optString("opBalance");
                        String accountName=jsonObject.optString("accountName");
                        String bill=jsonObject.optString("bill");
                        String billModeType=jsonObject.optString("billModeType");
                        String Membacc=jsonObject.optString("Membacc");
                        up=dbase.insertAccount(userId, posId, accountNum, tdate, ctype, opBalance, waiternamecode, accountName, bill, billModeType, Membacc,"0","");
                        dbase.updateIsot(Membacc,"0",posId);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if(up>0)
            {
                Toast.makeText(getApplicationContext(), "Transferred Successfully", Toast.LENGTH_LONG).show();
                finish();
            }

        }
    }

}


