package com.irca.Billing;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.irca.ServerConnection.ConnectionDetector;
import com.irca.activity.TableTransferActivity;
import com.irca.adapter.AccountlistAdapter;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.db.Dbase;
import com.irca.dto.AccountNew;
import com.irca.dto.ClickListener;
import com.irca.dto.MemberDetails;
import com.irca.dto.MemberMainDto;
import com.irca.fields.AccountDto;
import com.irca.fields.Card;
import com.irca.table.DependentTable;
import com.irca.table.MemberGuestTable;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/*
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
*/

/**
 * Created by Manoch Richard on 25-Apr-16.
 */
public class Accountlist extends AppCompatActivity {
    ListView listView;
    Dbase db;  ConnectionDetector cd;
    Boolean isInternetPresent = false;

    // TextView title,newCard;
    SharedPreferences sharedpreferences;
    //ImageView logo;
    String waiterid;
    int positionn;
    FloatingActionButton addButton;
    String bill;
    String billMode = "";
  //  Button smart_cartbtn;
    //NFC
    boolean isNFCenabled = false;
    NfcAdapter nfcAdapter = null;

    EditText MemberShipNo,TableNo;
    // list of NFC technologies detected:
    private final String[][] techList = new String[][]{
            new String[]{
                    NfcA.class.getName(),
                    NfcB.class.getName(),
                    NfcF.class.getName(),
                    NfcV.class.getName(),
                    IsoDep.class.getName(),
                    MifareClassic.class.getName(),
                    MifareUltralight.class.getName(), Ndef.class.getName()
            }
    };
    ArrayList<Card> list = new ArrayList<>();
    EditText inputSearch;
    String storeid = "";
    String userId = "";
  //  Button btn_click;
    ArrayList<AccountDto> accountDtoList = new ArrayList<>();
    RecyclerView recycler_view;
    String stewardBilling="";
    LinearLayout filterAccountlist;

    AccountlistAdapter accountlistAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Fabric.with(this, new Crashlytics());
        setContentView(R.layout.store_list);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        storeid = sharedpreferences.getString("storeId", "");
        userId = sharedpreferences.getString("userId", "");
        bill = sharedpreferences.getString("Bill", "");
        billMode = sharedpreferences.getString("BillMode", "");
        stewardBilling = sharedpreferences.getString("StewardBilling", "");

        actionBarSetup();
        listView = findViewById(R.id.list_store);
        recycler_view = findViewById(R.id.recycler_view);

        addButton = findViewById(R.id.add);
        inputSearch = findViewById(R.id.waiterSearch);
    //  smart_cartbtn=findViewById(R.id.smart_cartbtn);
        inputSearch.setVisibility(View.GONE);
        db = new Dbase(Accountlist.this);
        recycler_view.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
 /* addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String memberid =    "E74A97C1";
                Bundle b = getIntent().getExtras();
                int tableNo = b.getInt("tableNo");
                int tableId = b.getInt("tableId");
                String from = b.getString("from");
                Intent i = new Intent(Accountlist.this, BillingProfile.class);
                i.putExtra("tableNo", "0");
                i.putExtra("tableId", tableId);
                i.putExtra("accountType", "NFC");
                i.putExtra("cardType", "");
                i.putExtra("position", positionn);
                i.putExtra("billMode", "");
                i.putExtra("memberid", memberid);
                i.putExtra("billdate", "");
                startActivity(i);
            }
        });*/
    //    btn_click = (Button) findViewById(R.id.bnt_tesxt);
        //  final ArrayList<Card> list=db.getAccountList(storeid,userId);
        new CallApi().execute();


        //  title.setText("Member Account");

        Bundle b = getIntent().getExtras();
        //TODO Uncomment
   addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = getIntent().getExtras();
                int tableNo = b.getInt("tableNo");
                int tableId = b.getInt("tableId");
                //Intent i = new Intent(Accountlist.this, TakeOrder.class);
                Intent i = new Intent(Accountlist.this, BillingProfile.class);
                i.putExtra("tableNo", "0");
                i.putExtra("tableId", tableId);
                i.putExtra("accountType", "NCM");
                i.putExtra("cardType", "");
                i.putExtra("position", positionn);
                i.putExtra("billMode", "");
                startActivity(i);
                //finish();
            }
        });
        //TODO Uncomment


//        addButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               // String memberid = ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));
//                    String memberid ="B575E928";
//                Bundle b = getIntent().getExtras();
//                int tableNo = b.getInt("tableNo");
//                int tableId = b.getInt("tableId");
//                String from = b.getString("from");
//                Intent i = new Intent(Accountlist.this, BillingProfile.class);
//                i.putExtra("tableNo", "0");
//                i.putExtra("tableId", tableId);
//                i.putExtra("accountType", "NFC");
//                i.putExtra("cardType", "");
//                i.putExtra("position", positionn);
//                i.putExtra("billMode", "");
//                i.putExtra("memberid", memberid);
//                i.putExtra("billdate", "");
//                startActivity(i);
//            }
//        });


//        if (list.size() == 0) {
//            int tableNo = b.getInt("tableNo");
//            int tableId = b.getInt("tableId");
//            String from = b.getString("from");
//            Intent i = null;
//            if (from != null) {
//                if (from.equals("Dashboard_new")) {
//                    i = new Intent(Accountlist.this, Dashboard_new.class);
//                } else if (from.equals("CloseOrderdetails")) {
//                    i = new Intent(Accountlist.this, TakeOrder.class);
//                } else if (from.equals("Dashboard_century")) {
//                    i = new Intent(Accountlist.this, Dashboard_century.class);
//                } else {
//                    //i = new Intent(Accountlist.this, TakeOrder.class);
//                    //i = new Intent(Accountlist.this, BillingProfile.class);
//                }
////            i.putExtra("tableNo", "0");
////            i.putExtra("tableId", tableId);
////            i.putExtra("accountType", "New Card");
////            i.putExtra("cardType", "");
////            i.putExtra("position", positionn);
////            i.putExtra("billMode", "");
////            startActivity(i);
////            finish();
//            }
//        }
        Boolean isReadCard = MainActivity.isSmartCard;
        if (isReadCard) {
            String isreader = sharedpreferences.getString("misReader", "");
            if (isreader.equalsIgnoreCase("False")) {
                isReadCard = false;
            } else {
                addButton.setEnabled(false);
            }

        }

    }


    @SuppressLint("RestrictedApi")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("" + "Account List - " + bill);
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDefaultDisplayHomeAsUpEnabled(true);
            ab.setElevation(0);

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent i = new Intent(Accountlist.this, Dashboard_NSCI.class);
                Bundle b = getIntent().getExtras();
                positionn = b.getInt("position");
                i.putExtra("waiterid", waiterid);
                i.putExtra("position", positionn);
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(Accountlist.this, Dashboard_NSCI.class);
        Bundle b = getIntent().getExtras();
        positionn = b.getInt("position");
        // i.putExtra("tableNo",table);
        // i.putExtra("tableId",tableId);
        i.putExtra("waiterid", waiterid);
        i.putExtra("position", positionn);
        startActivity(i);
        finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)) {
          String memberid = ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));
           //    String memberid ="B9D15E4B";

            new CallProfileAPI().execute("", "2", "", "", "", "", "", "",memberid);

        }
    }

/*   @Override
    protected void onNewIntent(Intent intent) {
        if (intent.getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)) {
         //   String memberid = ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));
                String memberid ="79D2A94C";
            Bundle b = getIntent().getExtras();
            int tableNo = b.getInt("tableNo");
            int tableId = b.getInt("tableId");
            String from = b.getString("from");
            Intent i = new Intent(Accountlist.this, BillingProfile.class);
            i.putExtra("tableNo", "0");
            i.putExtra("tableId", tableId);
            i.putExtra("accountType", "NFC");
            i.putExtra("cardType", "");
            i.putExtra("position", positionn);
            i.putExtra("billMode", "");
            i.putExtra("memberid", memberid);
            i.putExtra("billdate", "");
            startActivity(i);
        }
    }*/
   @SuppressLint("StaticFieldLeak")
   public class CallProfileAPI extends AsyncTask<String, Void, String> {
       JSONObject getResult = null;
       RestAPI api = new RestAPI(Accountlist.this);
       String status = "";
       String result = "";
       String error = "", items = "", taxes = "";
       ProgressDialog pd;
       MemberMainDto memberMainDto;
       String type;
       String accesstype, creditLimit, debitbal, memberId, bookingId, bmod;


       @SuppressLint("SimpleDateFormat")
       @Override
       protected String doInBackground(String... strings) {
           try {
               cd = new ConnectionDetector(Accountlist.this);
               isInternetPresent = cd.isConnectingToInternet();
               if (isInternetPresent) {
                   type = strings[1];
                   accesstype = strings[2];
                   creditLimit = strings[3];
                   debitbal = strings[4];
                   memberId = strings[8];
                   bookingId = strings[6];
                   bmod = strings[7];
                   getResult = api.GetMemberInformationforPOSonAccKeyIn("",strings[8]);
                   result = "" + getResult;
                   if (result.contains("true")) {
                       JSONObject jsonArray = getResult.getJSONObject("Value");
                       try {
                           Gson gson = new Gson();
                           try {
                               memberMainDto = gson.fromJson(String.valueOf(jsonArray), new TypeToken<MemberMainDto>() {
                               }.getType());
                           } catch (Throwable e) {
                               e.printStackTrace();
                           }
                           status = "success";
                       } catch (Throwable e) {
                           e.getMessage();
                           status = e.getMessage();
                       }
                   } else {
                       status = result;
                   }
               } else {
                   status = "internet";
               }
           } catch (Exception e) {
               status = "server";
               error = e.getMessage();
           }
           return status;
       }

       @Override
       protected void onPreExecute() {
           super.onPreExecute();
           pd = new ProgressDialog(Accountlist.this);
           pd.setMessage("Loading...");
           pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
           pd.show();
           pd.setCancelable(false);

       }

       @Override
       protected void onPostExecute(String s) {
           super.onPostExecute(s);
           if (!s.equals("")) {
               switch (status) {
                   case "success":
                       try {
                           pd.dismiss();
                           if (memberMainDto != null) {
                               if (memberMainDto.getMemberDetailsList().size() > 0) {
                                   showProfile(memberMainDto, type, accesstype, creditLimit, debitbal, memberId, bookingId, bmod);
                               } else {

                               }
                           } else {
                               Toast.makeText(Accountlist.this, "failed: " + result, Toast.LENGTH_SHORT).show();
                           }



                       } catch (Throwable e) {
                           e.printStackTrace();
                       }
                       break;
                   case "server":
                       Toast.makeText(Accountlist.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                       pd.dismiss();
                       break;
                   case "internet":
                       Toast.makeText(Accountlist.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                       pd.dismiss();
                       break;
                   default:
                       Toast.makeText(Accountlist.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                       pd.dismiss();
                       break;
               }
           } else {
               Toast.makeText(Accountlist.this, "Error: " + result, Toast.LENGTH_SHORT).show();
               pd.dismiss();
           }
       }
   }
    @SuppressLint("SetTextI18n")
    public void showProfile(MemberMainDto memberMainDto, final String typess, final String accesstype, final String bookingNumber, final String debitbal, final String memberId, final String bookingId, final String bmod) {
        try {
            final Dialog dialog = new Dialog(Accountlist.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_member_display);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
            TextView name = dialog.findViewById(R.id.name);
            ImageView image = dialog.findViewById(R.id.image);
            ImageView image_view = dialog.findViewById(R.id.image_view);
            TextView type = dialog.findViewById(R.id.type);
            TextView doj = dialog.findViewById(R.id.doj);
            TextView dob = dialog.findViewById(R.id.dob);
            TextView owner = dialog.findViewById(R.id.owner);
            TextView types = dialog.findViewById(R.id.types);
            TextView status = dialog.findViewById(R.id.status);
            TextView remarks = dialog.findViewById(R.id.remarks);
            RelativeLayout relativeLayout = dialog.findViewById(R.id.table_scemelist);
            RelativeLayout memberguestcnt = dialog.findViewById(R.id.memberguestcnt);
            if (memberMainDto.getMemberDetailsList().size() > 0) {
                try {
                    MemberDetails memberDetails = memberMainDto.getMemberDetailsList().get(0);
                    name.setText(memberDetails.getAccountNumber() + "-" + memberDetails.getFirstName() + " " + memberDetails.getMiddleName() + " " + memberDetails.getLastName());
                    type.setText(memberDetails.getMemberType());
                    types.setText(memberDetails.getTypes());
                    dob.setText(memberDetails.getDateOfBirth());
                    doj.setText(memberDetails.getConfirmationDate());
                    owner.setText(memberDetails.getOwnerShipType());
                    status.setText(memberDetails.getStatus());
                    remarks.setText(memberDetails.getRemarks());

                    if (!memberDetails.getPhoto().isEmpty()) {
                        byte[] imageAsBytes = Base64.decode(memberDetails.getPhoto().getBytes(), Base64.DEFAULT);
                        image.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
            image_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (typess.equals("2")) {
                        dialog.dismiss();
                      //  second();
                    } else if (typess.equals("3")) {
                        dialog.dismiss();
                     //   third();
                    } else {
                        dialog.dismiss();
                     //   Intents(accesstype, bookingNumber, debitbal, memberId, bookingId, bmod);
                    }


                    Bundle b = getIntent().getExtras();
                    int tableNo = b.getInt("tableNo");
                    int tableId = b.getInt("tableId");
                    String from = b.getString("from");
                    Intent i = new Intent(Accountlist.this, BillingProfile.class);
                    i.putExtra("tableNo", "0");
                    i.putExtra("tableId", tableId);
                    i.putExtra("accountType", "NFC");
                    i.putExtra("cardType", "");
                    i.putExtra("position", positionn);
                    i.putExtra("billMode", "");
                    //send member hex serial number
                    i.putExtra("memberid", memberId);
                    i.putExtra("billdate", "");
                    startActivity(i);


                }
            });
            relativeLayout.removeAllViews();
            relativeLayout.addView(new DependentTable(Accountlist.this, memberMainDto.getDependentDetailsList()));
            memberguestcnt.removeAllViews();
            memberguestcnt.addView(new MemberGuestTable(Accountlist.this, memberMainDto.getMemberItemGuestList()));

            dialog.show();

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    private String ByteArrayToHexString(byte[] inarray) {
        int i, j, in;
        String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
        String out = "";

        for (j = 0; j < inarray.length; ++j) {
            in = (int) inarray[j] & 0xff;
            i = (in >> 4) & 0x0f;
            out += hex[i];
            i = in & 0x0f;
            out += hex[i];
        }
        return out;
    }

    @Override
    protected void onResume() {
        super.onResume();
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null) {
            // creating pending intent:
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            // creating intent receiver for NFC events:
            IntentFilter filter = new IntentFilter();
            filter.addAction(NfcAdapter.ACTION_TAG_DISCOVERED);
            filter.addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
            filter.addAction(NfcAdapter.ACTION_TECH_DISCOVERED);
            // enabling foreground dispatch for getting intent from NFC event:

            nfcAdapter.enableForegroundDispatch(this, pendingIntent, new IntentFilter[]{filter}, this.techList);
            isNFCenabled = nfcAdapter.isEnabled();
        } else {
            if (list.size() == 0) {
//                //Intent i = new Intent(Accountlist.this, TakeOrder.class); / NEED TO REMOVE FOR LIVE
//                Intent i = new Intent(Accountlist.this, BillingProfile.class);
//                i.putExtra("tableNo", "0");
//                i.putExtra("tableId", "0");
//                i.putExtra("accountType", "New Card");
//                i.putExtra("cardType", "");
//                i.putExtra("position", positionn);
//                i.putExtra("billMode", "");
//                i.putExtra("memberid", "67681000");// NEED TO REMOVE FOR LIVE
//                startActivity(i);
//                finish();


            }


        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        // disabling foreground dispatch:
        if (nfcAdapter != null) {
            nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            nfcAdapter.disableForegroundDispatch(this);
        }

    }

    public class GetData extends AsyncTask<Void, JSONObject, ArrayList<AccountDto>> {
        RestAPI api = new RestAPI(Accountlist.this);
        String stauts = "";
        String error = "";
        ProgressDialog pd;

        @Override
        protected ArrayList<AccountDto> doInBackground(Void... params) {
            try {

                sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String toDate = df.format(c.getTime());


                final Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -3);
                SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd");
                String fDate = dff.format(cal.getTime());

                //ot(String FromDate,String ToDate,String StoreID,String UserCode) t
                JSONObject jsonObj = api.cms_getaccountlistot(toDate, toDate, storeid, userId);
                JSONObject jsonObject = jsonObj.getJSONObject("Value");
                JSONArray jsonArray = jsonObject.getJSONArray("Table");
                try {
                    Gson gson = new Gson();
                    accountDtoList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<AccountDto>>() {
                    }.getType());
                } catch (Throwable e) {
                    e.getMessage();
                    error = e.getMessage();
                }
            } catch (Throwable e) {
                error = e.getMessage();
            }
            return accountDtoList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Accountlist.this);
            pd.setMessage("Loading Data...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(true);
        }

        @Override
        protected void onPostExecute(ArrayList<AccountDto> result) {
            //super.onPostExecute(result);
            //Dashboard.session=1;
            if (stauts.equals("No internet connection")) {
                Toast.makeText(getApplicationContext(), "No internet connection-Dealer not loaded please referesh ", Toast.LENGTH_LONG).show();
                // pd.dismiss();
                if (result != null) {
                    Toast.makeText(getApplicationContext(), "Data not Loaded", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            } else if (!error.equals("")) {
                Toast.makeText(Accountlist.this, "failed -- >" + error, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else {
                if (result.size() != 0) {
                    list = new ArrayList<>();
                    for (int k = 0; k < result.size(); k++) {
                        String accountno = result.get(k).getAccountNumber();
                        Card n = db.getAccountList_newcheck(storeid, userId, bill, accountno);
                        if (n.account != null) {
                            list.add(n);
                        }
                    }

//                    BarChart(result);
                } else {
                    //   list = db.getAccountList(storeid, userId, bill);
                    db.deleteAccountlist(storeid, userId);
                    list = new ArrayList<>();
                    Toast.makeText(Accountlist.this, "Data not found", Toast.LENGTH_SHORT).show();
                }
                CustomAccountAdapter adapter = new CustomAccountAdapter(Accountlist.this, list);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Bundle b = getIntent().getExtras();
                        int tableNo = b.getInt("tableNo");
                        int tableId = b.getInt("tableId");
                        waiterid = b.getString("waiterid");
                        positionn = b.getInt("position");
                        //Intent i = new Intent(Accountlist.this, TakeOrder.class);
                        Intent i = new Intent(Accountlist.this, BillingProfile.class);
                        i.putExtra("tableNo", list.get(position).getTableNO());
                        Log.e("Table No", list.get(position).getTableNO());
                        i.putExtra("tableId", tableId);
                        i.putExtra("accountType", list.get(position).getAccount());
                        i.putExtra("cardType", list.get(position).getCardtype());
                        i.putExtra("Steward", list.get(position).getFirstName());
                        i.putExtra("billMode", list.get(position).getBillmode());
                        i.putExtra("billdate", list.get(position).getOtdate());
                        i.putExtra("paxcnt", list.get(position).getPaxcount());
                        i.putExtra("Steward", list.get(position).getSteward());
                        //i.putExtra("Steward","");
                        i.putExtra("position", positionn);
                        startActivity(i);
                        //finish();
                    }

                });
                pd.dismiss();
            }


        }
    }

    public class CallApi extends AsyncTask<String, Void, String> {
        JSONObject getResult = null;
        RestAPI api = new RestAPI(Accountlist.this);
        String status = "";
        String result = "";
        String error = "";
        ProgressDialog pd;
        List<AccountNew> itemsList = new ArrayList<>();

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... strings) {
            try {
                ConnectionDetector cd = new ConnectionDetector(Accountlist.this);
                boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    getResult = api.GetAccountListData(storeid, userId);
                    result = "" + getResult;
                    if (result.contains("true")) {
                        JSONArray jsonArray = getResult.getJSONArray("Value");
                        try {
                            Gson gson = new Gson();
                            itemsList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<AccountNew>>() {
                            }.getType());
                            status = "success";
                        } catch (Throwable e) {
                            e.getMessage();
                            status = e.getMessage();
                        }
                    } else {
                        status = result;
                    }
                } else {
                    status = "internet";
                }
            } catch (Exception e) {
                status = "server";
                error = e.getMessage();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Accountlist.this);
            pd.setMessage("Loading...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!s.equals("")) {
                switch (status) {
                    case "success":
                        long time1 = System.currentTimeMillis();

                        //   showData(itemsList);
                       showDatanew(itemsList);
                        long time2 = System.currentTimeMillis();
                        Log.d("TIME GAP", "onCreate: " + (time2 - time1));

                        pd.dismiss();
                        break;
                    case "server":
                        Toast.makeText(Accountlist.this, "failed: " + error, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    case "internet":
                        Toast.makeText(Accountlist.this, "Opps... You lost the internet connection", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                    default:
                        Toast.makeText(Accountlist.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        break;
                }
            } else {
                Toast.makeText(Accountlist.this, "Error: " + result, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }
    private void showDatanew(List<AccountNew> accountNewList) {
        try {

            if (accountNewList.size() > 0) {
                db.deleteAccountlist(storeid, userId);
                for (int i = 0; i < accountNewList.size(); i++) {
                    String creditAccountno = accountNewList.get(i).getACCNUM();
                    String _params = accountNewList.get(i).getACCNUM();
                    String memberId = accountNewList.get(i).getMemberID();
                    String membertype = accountNewList.get(i).getMemberType();
                    String accesstype = accountNewList.get(i).getACCESSTYPE();
//                    String accesstype = "NCM";
                    String formattedDate = accountNewList.get(i).getTODAYSDATE().split("T")[0];
                    String cardType = accountNewList.get(i).getCARDTYPE();
                    String ava_balance = accountNewList.get(i).getOPENINGBALANCE();
                    String waiternamecode = accountNewList.get(i).getSTEWARD();
                    String creditName = accountNewList.get(i).getACCNAME();
                    String debitbal = accountNewList.get(i).getDEBITBALANCE();
                    String billMode = accountNewList.get(i).getBILLMODE();
                    String bmod = accountNewList.get(i).getBILLMODETYPE();
                    String tableNO = accountNewList.get(i).getTABLENO();
                    String paxcount = accountNewList.get(i).getPAXCOUNT();
                    String isOT = accountNewList.get(i).getISOTD();
                    String aCCESSTYPE = accountNewList.get(i).getACCESSTYPE();
                    String SmartCardSerialNo = accountNewList.get(i).getSmartCardSerialNo();
                    String ContractorsID = accountNewList.get(i).getContractorsID();
                    if (isOT.equalsIgnoreCase("true")) {
                        isOT = "1";
                    } else {
                        isOT = "0";
                    }
                    String billModes = "";
                    if (billMode.equalsIgnoreCase("R")) {
                        billModes = "Regular";
                    } else {
                        billModes = billMode;
                    }

                    if (aCCESSTYPE.equalsIgnoreCase("NFC")) {
                        _params = SmartCardSerialNo;


                        db.insertAccount(userId, storeid, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype + "#" + ContractorsID, formattedDate, cardType, ava_balance, waiternamecode, creditName, billModes, bmod, creditAccountno, debitbal,stewardBilling);
                     /*   if (stewardBilling.equalsIgnoreCase("true")) {
                            db.updateTableNoWaiterWise(creditAccountno, tableNO, storeid, userId, formattedDate, paxcount, waiternamecode, isOT);

                        }*/

                            db.updateTableNo(creditAccountno, tableNO, storeid, userId, formattedDate, paxcount, waiternamecode, isOT);


                    } else {

                        db.insertAccount(userId, storeid, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype + "#" + ContractorsID, formattedDate, cardType, ava_balance, waiternamecode, creditName, billModes, bmod, creditAccountno, debitbal,stewardBilling);
                 //       db.updateTableNo(creditAccountno, tableNO, storeid, userId, formattedDate, paxcount, waiternamecode, isOT);

                            db.updateTableNo(creditAccountno, tableNO, storeid, userId, formattedDate, paxcount, waiternamecode, isOT);


                    }
                }
                list = new ArrayList<>();
                for (int k = 0; k < accountNewList.size(); k++) {
                    String accountno = accountNewList.get(k).getACCNUM();
                    String billMode = accountNewList.get(k).getBILLMODE();
                    String steward = accountNewList.get(k).getSTEWARD();
                    String billModes = "";
                    if (billMode.equalsIgnoreCase("R")) {
                        billModes = "Regular";
                    } else {
                        billModes = billMode;
                    }
                    Card n;
                 if (stewardBilling.equalsIgnoreCase("true")){
                  n = db.getAccountList_newcheckwaiterwise(storeid, userId, billModes, accountno,steward);

                 }
                 else {
                       n = db.getAccountList_newcheck(storeid, userId, billModes, accountno);

                 }
                    if (n.account != null) {
                        list.add(n);
                    }
                }
                //TODO accountlist filter
                filterAccountlist = findViewById(R.id.filterAccountlist);
                filterAccountlist.setVisibility(View.VISIBLE);
                MemberShipNo = findViewById(R.id.MemberShipNo);
                TableNo = findViewById(R.id.TableNo);



                MemberShipNo.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        // filter your list from your input
                        filterMemberNo(s.toString());
                        //you can use runnable postDelayed like 500 ms to delay search text
                    }
                });
                TableNo.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        // filter your list from your input
                        filtertableNo(s.toString());
                        //you can use runnable postDelayed like 500 ms to delay search text
                    }
                });

            } else {
                db.deleteAccountlist(storeid, userId);
                list = new ArrayList<>();
            }

            recycler_view.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
              accountlistAdapter = new AccountlistAdapter(this, list, new ClickListener() {
                @Override
                public void onPositionClicked(int position) {
                    // callback performed on click
                    Bundle b = getIntent().getExtras();
                    int tableNo = b.getInt("tableNo");
                    int tableId = b.getInt("tableId");
                    waiterid = b.getString("waiterid");
                    positionn = b.getInt("position");
                    //Intent i = new Intent(Accountlist.this, TakeOrder.class);
                    Intent i = new Intent(Accountlist.this, BillingProfile.class);
                    i.putExtra("tableNo", list.get(position).getTableNO());
                    Log.e("Table No", list.get(position).getTableNO());
                    i.putExtra("tableId", tableId);
                    i.putExtra("accountType", list.get(position).getAccount());
                    i.putExtra("cardType", list.get(position).getCardtype());
                    i.putExtra("Steward", list.get(position).getFirstName());
                    i.putExtra("billMode", list.get(position).getBillmode());
                    i.putExtra("billdate", list.get(position).getOtdate());
                    i.putExtra("paxcnt", list.get(position).getPaxcount());
                    i.putExtra("Steward", list.get(position).getFirstName());
                    //i.putExtra("Steward","");
                    i.putExtra("position", positionn);
                    startActivity(i);
                }
            });
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            recycler_view.setLayoutManager(layoutManager);
            recycler_view.setAdapter(accountlistAdapter);

       /*     CustomAccountAdapter adapter = new CustomAccountAdapter(Accountlist.this, list);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Bundle b = getIntent().getExtras();
                    int tableNo = b.getInt("tableNo");
                    int tableId = b.getInt("tableId");
                    waiterid = b.getString("waiterid");
                    positionn = b.getInt("position");
                    //Intent i = new Intent(Accountlist.this, TakeOrder.class);
                    Intent i = new Intent(Accountlist.this, BillingProfile.class);
                    i.putExtra("tableNo", list.get(position).getTableNO());
                    Log.e("Table No", list.get(position).getTableNO());
                    i.putExtra("tableId", tableId);
                    i.putExtra("accountType", list.get(position).getAccount());
                    i.putExtra("cardType", list.get(position).getCardtype());
                    i.putExtra("Steward", list.get(position).getFirstName());
                    i.putExtra("billMode", list.get(position).getBillmode());
                    i.putExtra("billdate", list.get(position).getOtdate());
                    i.putExtra("paxcnt", list.get(position).getPaxcount());
                    i.putExtra("Steward", list.get(position).getSteward());
                    //i.putExtra("Steward","");
                    i.putExtra("position", positionn);
                    startActivity(i);
                }
            });*/


        } catch (Throwable e) {
            e.printStackTrace();
        }

    }
    void filterMemberNo(String text){
        ArrayList<Card> temp = new ArrayList();
        for(Card d: list){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getAccount().toLowerCase(Locale.ROOT).contains(text.toLowerCase(Locale.ROOT))){
                temp.add(d);
            }
        }
        //update recyclerview
        list=new ArrayList<>();
        list=temp;
        accountlistAdapter.updateList(temp);
    }
    void filtertableNo(String text){
        ArrayList<Card> temp = new ArrayList();
        for(Card d: list){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getTableNO().contains(text)){
                temp.add(d);
            }
        }
        //update recyclerview
        list=new ArrayList<>();
        list=temp;
        accountlistAdapter.updateList(temp);
    }


    private void showData(List<AccountNew> accountNewList) {
        try {
listView.setVisibility(View.VISIBLE);
            if (accountNewList.size() > 0) {
                db.deleteAccountlist(storeid, userId);
                for (int i = 0; i < accountNewList.size(); i++) {
                    String creditAccountno = accountNewList.get(i).getACCNUM();
                    String _params = accountNewList.get(i).getACCNUM();
                    String memberId = accountNewList.get(i).getMemberID();
                    String membertype = accountNewList.get(i).getMemberType();
                    String accesstype = accountNewList.get(i).getACCESSTYPE();
//                    String accesstype = "NCM";
                    String formattedDate = accountNewList.get(i).getTODAYSDATE().split("T")[0];
                    String cardType = accountNewList.get(i).getCARDTYPE();
                    String ava_balance = accountNewList.get(i).getOPENINGBALANCE();
                    String waiternamecode = accountNewList.get(i).getSTEWARD();
                    String creditName = accountNewList.get(i).getACCNAME();
                    String debitbal = accountNewList.get(i).getDEBITBALANCE();
                    String billMode = accountNewList.get(i).getBILLMODE();
                    String bmod = accountNewList.get(i).getBILLMODETYPE();
                    String tableNO = accountNewList.get(i).getTABLENO();
                    String paxcount = accountNewList.get(i).getPAXCOUNT();
                    String isOT = accountNewList.get(i).getISOTD();
                    String aCCESSTYPE = accountNewList.get(i).getACCESSTYPE();
                    String SmartCardSerialNo = accountNewList.get(i).getSmartCardSerialNo();
                    String ContractorsID = accountNewList.get(i).getContractorsID();
                    if (isOT.equalsIgnoreCase("true")) {
                        isOT = "1";
                    } else {
                        isOT = "0";
                    }
                    String billModes = "";
                    if (billMode.equalsIgnoreCase("R")) {
                        billModes = "Regular";
                    } else {
                        billModes = billMode;
                    }

                    if (aCCESSTYPE.equalsIgnoreCase("NFC")) {
                        _params=SmartCardSerialNo;


                        db.insertAccount(userId, storeid, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype + "#" + ContractorsID, formattedDate, cardType, ava_balance, waiternamecode, creditName, billModes, bmod, creditAccountno, debitbal,stewardBilling);
                        if (stewardBilling.equalsIgnoreCase("true")) {
                            db.updateTableNo(creditAccountno, tableNO, storeid, userId, formattedDate, paxcount, waiternamecode, isOT);
                        }
                        else {
                            db.updateTableNo(creditAccountno, tableNO, storeid, userId, formattedDate, paxcount, waiternamecode, isOT);

                        }
                    } else {

                        db.insertAccount(userId, storeid, creditAccountno + "#" + memberId + "#" + _params + "#" + membertype + "#" + accesstype+ "#" + ContractorsID, formattedDate, cardType, ava_balance, waiternamecode, creditName, billModes, bmod, creditAccountno, debitbal,stewardBilling);
                        db.updateTableNo(creditAccountno, tableNO, storeid, userId, formattedDate, paxcount, waiternamecode, isOT);
                        if (stewardBilling.equalsIgnoreCase("true")) {
                            db.updateTableNo(creditAccountno, tableNO, storeid, userId, formattedDate, paxcount, waiternamecode, isOT);
                        }
                        else {
                            db.updateTableNo(creditAccountno, tableNO, storeid, userId, formattedDate, paxcount, waiternamecode, isOT);

                        }
                    }
                }
                list = new ArrayList<>();
                for (int k = 0; k < accountNewList.size(); k++) {
                    String accountno = accountNewList.get(k).getACCNUM();
                    String billMode = accountNewList.get(k).getBILLMODE();
                    String billModes = "";
                    if (billMode.equalsIgnoreCase("R")) {
                        billModes = "Regular";
                    } else {
                        billModes = billMode;
                    }
                    Card n = db.getAccountList_newcheck(storeid, userId, billModes, accountno);
                    if (n.account != null) {
                        list.add(n);
                    }
                }
            } else {
                db.deleteAccountlist(storeid, userId);
                list = new ArrayList<>();
            }

            CustomAccountAdapter adapter = new CustomAccountAdapter(Accountlist.this, list);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Bundle b = getIntent().getExtras();
                    int tableNo = b.getInt("tableNo");
                    int tableId = b.getInt("tableId");
                    waiterid = b.getString("waiterid");
                    positionn = b.getInt("position");
                    //Intent i = new Intent(Accountlist.this, TakeOrder.class);
                    Intent i = new Intent(Accountlist.this, BillingProfile.class);
                    i.putExtra("tableNo", list.get(position).getTableNO());
                    Log.e("Table No", list.get(position).getTableNO());
                    i.putExtra("tableId", tableId);
                    i.putExtra("accountType", list.get(position).getAccount());
                    i.putExtra("cardType", list.get(position).getCardtype());
                    i.putExtra("Steward", list.get(position).getFirstName());
                    i.putExtra("billMode", list.get(position).getBillmode());
                    i.putExtra("billdate", list.get(position).getOtdate());
                    i.putExtra("paxcnt", list.get(position).getPaxcount());
                    i.putExtra("Steward", list.get(position).getSteward());
                    //i.putExtra("Steward","");
                    i.putExtra("position", positionn);
                    startActivity(i);
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }
}