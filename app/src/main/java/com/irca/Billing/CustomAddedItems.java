package com.irca.Billing;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

//import com.crashlytics.android.Crashlytics;
import com.irca.cosmo.R;
import com.irca.db.Dbase;
import com.irca.fields.Itemadapter;

import java.util.ArrayList;

//import io.fabric.sdk.android.Fabric;

public class CustomAddedItems extends Activity {
    ListView listitems;
    Dbase db;
   public static ArrayList<Itemadapter> list=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_custom_added_items);
        listitems= (ListView) findViewById(R.id.listitems);

        db = new Dbase(CustomAddedItems.this);
        list = db.getOTItem();

        if(list.size()>0)
        {
            ListAdapterItem adapter = new ListAdapterItem(CustomAddedItems.this,list);
            listitems.setAdapter(adapter);
        }
        else
        {
            finish();
        }
    }
}
