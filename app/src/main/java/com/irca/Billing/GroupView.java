package com.irca.Billing;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

//import com.crashlytics.android.Crashlytics;
import com.irca.cosmo.R;
import com.irca.db.Dbase;

import java.util.ArrayList;

//import io.fabric.sdk.android.Fabric;

public class GroupView extends AppCompatActivity {
    Dbase db;
    ListView listView;
    SharedPreferences sharedpreferences;
    String posId = "";
    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_group_view);
        actionBarSetup();
        db = new Dbase(GroupView.this);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        posId = sharedpreferences.getString("storeId", "");

       list = db.getGroupList_new(posId);

        listView = (ListView) findViewById(R.id.list_store);
        listView.setAdapter(new GroupView.CustomAccountAdapter_new(this, list));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // ListView Clicked item index
                int itemPosition = position;
                // ListView Clicked item value

                //String itemValue = listView.getItemAtPosition(position).toString();
                String itemValue = list.get(position).toString();

                String groupId = db.getGroupId(itemValue);

                if (!groupId.equals("0")) {
                    Intent i = new Intent(GroupView.this, ItemList.class);
                    i.putExtra("GroupName", itemValue);
                    i.putExtra("groupId", groupId);
                    startActivity(i);
                }
            }

        });

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("Group List");
            ab.setElevation(0);
            ab.setDisplayHomeAsUpEnabled(true);

            // ab.setElevation(0);

        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {

            case android.R.id.home:

                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
    public class CustomAccountAdapter_new extends BaseAdapter {


        ArrayList<String> masterStore;

        Context context;
        int pageNo;

        public CustomAccountAdapter_new(Context c, ArrayList<String> masterStoreArrayAdapter)

        {
            this.masterStore = masterStoreArrayAdapter;

            context = c;

        }

        @Override
        public int getCount() {
            return masterStore.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.custom_storelist1, parent, false);
            }

            TextView grp = (TextView) convertView.findViewById(R.id.storeText);
            grp.setText(""+masterStore.get(position).toString());
            return convertView;
        }

    }
}
