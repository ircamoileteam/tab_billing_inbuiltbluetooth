package com.irca.Billing;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

//import com.crashlytics.android.Crashlytics;
import com.irca.CustomAdapters.DividerItemDecoration;
import com.irca.CustomAdapters.ItemPromotionAdapter;
import com.irca.CustomAdapters.RecyclerTouchListener;
import com.irca.cosmo.R;
import com.irca.db.Dbase;
import com.irca.fields.ItemPromo;
import java.util.ArrayList;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by Manoch Richard on 26-Mar-18.
 */

public class ItemPromotion extends AppCompatActivity
{
    private RecyclerView recyclerView;
    private ItemPromotionAdapter m1Adapter;
    Dbase dbase = null;
    ArrayList<ItemPromo> promolist = new ArrayList<ItemPromo>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.item_promotion);
        actionBarSetup();
        prepareItemGroup();
        m1Adapter = new ItemPromotionAdapter(promolist);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(m1Adapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener()
        {
            @Override
            public void onClick(View view, int position)
            {
                Intent intent=new Intent(ItemPromotion.this,ItempromoDetails.class);
                String promoname= promolist.get(position).getPromotionName();
                String promoid=    dbase.getItemPromoid(promoname);
                intent.putExtra("Promoid",promoid);
                startActivity(intent);
            }
            @Override
            public void onLongClick(View view, int position)
            {
                Intent intent=new Intent(ItemPromotion.this,ItempromoDetails.class);
                String promoname= promolist.get(position).getPromotionName();
                String promoid=    dbase.getItemPromoid(promoname);
                intent.putExtra("Promoid",promoid);
                startActivity(intent);
            }
        }));

    }

    private void prepareItemGroup()
    {
        dbase = new Dbase(ItemPromotion.this);
        promolist = dbase.getItemPromotion();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup()
    {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("" + "Today's Offer");
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDefaultDisplayHomeAsUpEnabled(true);
            ab.setElevation(0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                Intent i = new Intent(ItemPromotion.this, Dashboard_NSCI.class);
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}

