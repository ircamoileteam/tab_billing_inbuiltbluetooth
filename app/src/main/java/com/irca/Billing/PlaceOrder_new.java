package com.irca.Billing;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.Printer.PlaceOrder_bill;
import com.irca.cosmo.Alert_ItemDescription;
import com.irca.cosmo.CloseOrderdetails;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.cosmo.Search_Item;
import com.irca.cosmo.TakeOrder;
import com.irca.db.Dbase;
import com.irca.fields.AndroidTempbilldetails;
import com.irca.fields.AndroidTempbilling;
import com.irca.fields.CloseorderItems;
import com.irca.fields.ItemList;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

//import io.fabric.sdk.android.Fabric;

public class PlaceOrder_new extends AppCompatActivity {
    Bundle bundle;
    int tableId;
    String table="0";
    String creditLimit="" ,creditAccountno="",creditName="",memberId="",cardType="",_params="",waiterid="",waiternamecode="",ct="";
    int position;
    TextView openBalance ,steward,tableNo;
    EditText editText;
    LinearLayout itemView;
    TelephonyManager tel;
    String serialNo;

    double noOfCounts = 0;
    TextView txt_itemcount;
    View view = null;
    TextView itemName, itemCount,memberName,memAccountNo,rate;
    ImageView add, subtract,search;
    Dbase db;
    String groupId = "";
    //CheckBox happyHour;
    FloatingActionButton fab;
    Button placeOrder ;

    float Totalamount = 0;
    ProgressDialog pd;
    public static ArrayList<ItemList>  placeOrder_list;
    SharedPreferences sharedpreferences;
    String pos="";
    String newTableNo="0";
    String C = "0";
    String referenceNo="";
    String Bill="";
    String otType="";
    String finalBillMode="";
    String bill="";
    String storeId="";
    //close order
    ArrayList<String> paymentMode;
    public static ArrayList<CloseorderItems> closeorderItemsArrayList = new ArrayList<CloseorderItems>();
    String mode="";
    String[] payments;
    String isCashCardFilled="";
    String contractorsId="";
    EditText misguest_etxt;
    String mguest="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_place_order_new2);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        pos=sharedpreferences.getString("StoreName", "");
        Bill=sharedpreferences.getString("BillMode", "");
        otType=sharedpreferences.getString("billType", "");
        bill=sharedpreferences.getString("Bill", "");
        storeId = sharedpreferences.getString("storeId", "");
        actionBarSetup();
        bundle= getIntent().getExtras();
        table= bundle.getString("tableNo");
        creditLimit= bundle.getString("creditLimit");
        creditAccountno= bundle.getString("creditAno");
        creditName=  bundle.getString("creditName");
        memberId=bundle.getString("memberId");
        tableId= bundle.getInt("tableId");
        cardType= bundle.getString("cardType");
        referenceNo=bundle.getString("referenceNo");
        waiterid=bundle.getString("waiterid");
        position=bundle.getInt("position");
        waiternamecode=bundle.getString("waiternamecode");
        finalBillMode=bundle.getString("billmode");
        tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        serialNo= Build.SERIAL;
        C = creditLimit;
        misguest_etxt= (EditText) findViewById(R.id.guest_etxt);
        openBalance=(TextView)findViewById(R.id.op_bal);
        steward=(TextView)findViewById(R.id.steward);
        tableNo=(TextView)findViewById(R.id.table);
        memberName=(TextView)findViewById(R.id.memName);
        memAccountNo=(TextView)findViewById(R.id.accNo);
        itemView= (LinearLayout) findViewById(R.id.itemView);
        placeOrder = (Button) findViewById(R.id.button_placeOrder);
        placeOrder.setVisibility(View.GONE);
        // txt_itemcount = (TextView) findViewById(R.id.txtCount);
        db=new Dbase(PlaceOrder_new.this);

        if(bill.equals("Party Billing")){
            openBalance.setText("Booking Number  :" + creditLimit + "");
        }else {
            openBalance.setText("Opening Balance :" + creditLimit + "Rs.");
        }
        steward.setText    ("Steward         :"+ waiternamecode);
      // memberName.setText(""+creditName + " ( " + creditAccountno +" )");
        memberName.setText(""+creditName );
        memAccountNo.setText("Account No.    :"+creditAccountno );


        tableNo.setText("Table No. : " + table);

        tableNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertTableNO();
            }
        });

        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String IsOTRequired=sharedpreferences.getString("IsOTRequired","");
                isCashCardFilled=sharedpreferences.getString("isCashCardFilled","");
                String ot=sharedpreferences.getString("ot_close","");
                String ct=sharedpreferences.getString("ct","");
                String [] ss1=ot.split("#");
                contractorsId=(ct.equals("DEPENDENT CARD"))?ss1[2]:"";
                mguest=misguest_etxt.getText().toString();
                if(IsOTRequired.equals("True"))
                {

                    new PlaceOrder_new.AsyncPlaceOrder2().execute();
                }
                else if(!mguest.equals(""))
                {

                    String mstoreid=sharedpreferences.getString("mstoreId","");
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("vocationalmember","Occ.Member:"+mguest);
                    editor.commit();
                    AlertPayment(ss1[1], "0",ct,isCashCardFilled);
                    //new PlaceOrder_new.AsyncPlaceOrder2().execute();
                }
                else
                {
                    String mstoreid=sharedpreferences.getString("mstoreId","");
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("vocationalmember","");
                    editor.commit();
                    AlertPayment(ss1[1], "0",ct,isCashCardFilled);
                }


            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/

              //  Intent i = new Intent(PlaceOrder_new.this, Itemview.class);
                Intent i = new Intent(PlaceOrder_new.this, Search_Item.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("tableNo", table);
                i.putExtra("creditLimit", creditLimit);
                i.putExtra("creditAno", creditAccountno);
                i.putExtra("creditName", creditName);
                i.putExtra("memberId", memberId);
                i.putExtra("tableId", tableId);
                i.putExtra("cardType", cardType);
                i.putExtra("referenceNo", referenceNo);
                i.putExtra("position",position);
                i.putExtra("waiternamecode",waiternamecode);
                i.putExtra("waiterid",waiterid);
                i.putExtra("billmode",finalBillMode);
                i.putExtra("doa", "0");
                i.putExtra("dob", "0");
                startActivity(i);
                finish();
            }
        });
        itemView.removeAllViews();
        additems(2);
        String misguest=sharedpreferences.getString("misguest","");
        if(misguest.equals("False"))
        {
            misguest_etxt.setVisibility(View.INVISIBLE);
        }

    }

    //*****


    protected class AsyncPlaceOrder2 extends AsyncTask<String,Void,String> {
        ArrayList<String> credit=null;
        String status="";
       // String creditAccountno="";
       // String creditName="";
        String storeId = sharedpreferences.getString("storeId", "");
        String loginname=sharedpreferences.getString("loginName","");
        String userId=sharedpreferences.getString("userId","");
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        String result="";
        String narration="";
        JSONObject jsonObject;
        String mstoreid=sharedpreferences.getString("mstoreId","");
        @Override
        protected String doInBackground(String... params)
        {
            RestAPI api=new RestAPI(PlaceOrder_new.this);
            try
            {
                Totalamount=0;
                AndroidTempbilling obj=new AndroidTempbilling();
                ArrayList<Object> itemList_main=new ArrayList<>();
                placeOrder_list=new ArrayList<>();
                AndroidTempbilldetails ob=null;
                ItemList li=null;
                if(itemView!=null)
                {
                    //itemView.bringChildToFront(v);
                    int childRow= itemView.getChildCount();
                    for(int i=0;i<childRow;i++)
                    {
                        View ItemRow=itemView.getChildAt(i);
                        TextView t= (TextView) ItemRow.findViewById(R.id.kot_itemCount);
                        TextView t1= (TextView) ItemRow.findViewById(R.id.kot_itemname);
                        String Itemcount=t.getText().toString();
                        if(!((Itemcount.equals("")) ||  (Itemcount.equals("0"))))
                        {
                            String itemname=t1.getText().toString();
                            String [] spl=itemname.split("-");
                            String _itemCode=spl[0];
                            ArrayList<String> dummy=new ArrayList<String>();
                            List<String> itemId=db.getItemid(_itemCode);

                            narration=db.getNarration(_itemCode);

                            float salesTax = 0;
                            float serviceTax = 0;
                            float cessTax = 0;

                            ob=new AndroidTempbilldetails();
                            li=new ItemList();
                            String _itemName = itemId.get(3);

                            ob.setSalestax(0);  // unwanted
                            ob.setServicetax(0);
                            ob.setCesstax(0);
                            ob.setOtno("");
                            ob.setTaxId(0);
                            ob.setTaxRate(0);
                            ob.setAlltax(dummy);

                            ob.setSalesUintid(Integer.parseInt(itemId.get(2)));
                            if(Itemcount.contains("."))
                            {
                                ob.setQuantity(Double.parseDouble(Itemcount));
                            }
                            else
                            {
                                ob.setQuantity(Double.parseDouble(Itemcount+".0"));
                            }

                            ob.setRate(itemId.get(1));
                            ob.setItemcode(_itemCode);
                            ob.setItemname(_itemName);
                            ob.setItemId(itemId.get(0));
                            ob.setItemCategoryid(itemId.get(4));
                            ob.setNarration(narration);


                            double v1=Double.parseDouble(Itemcount);
                            float v2=Float.parseFloat(itemId.get(1));
                            double  v3=v1 * v2;
                            //  ob.setAmount(Float.parseFloat(itemId.get(1)));
                            ob.setAmount((float) v3);

                           /* ArrayList<String> dummy=new ArrayList<String>();
                            if (!cardType.equals("CLUB CARD"))
                            {
                                if(itemId.size()>7)
                                {
                                    int k=5;
                                    int l=2;
                                    for(int j=0;j<(itemId.size()/7);j++)
                                    {
                                        dummy.add(itemId.get(k)+"-"+itemId.get(l));
                                        k=k+7;
                                        l=l+7;
                                    }
                                    ob.setAlltax(dummy);
                                    ob.setTaxId(Integer.parseInt(itemId.get(2)));
                                }
                                else
                                {
                                    dummy.add(itemId.get(5)+"-"+itemId.get(2));
                                    ob.setAlltax(dummy);
                                }
                            }
                            else
                            {
                                dummy.add("0");
                                ob.setAlltax(dummy);
                                ob.setTaxId(Integer.parseInt(itemId.get(2)));
                            }*/
                            Totalamount=Totalamount+(float)v3+salesTax+serviceTax+cessTax;
                            itemList_main.add(ob);

                            obj.setMemberId(memberId);
                            obj.setStoreId(storeId);
                            obj.setTableId(newTableNo+"");
                            obj.setTamount(Totalamount);
                            obj.setNarration(narration);
                            String id=(cardType.equals("DEPENDENT CARD"))?memberId:"";
                            obj.setDependantid(id);
                        }

                    }
                }

                if(itemList_main.size()!=0)
                {
                    //String  limit = (cLimit.equals(""))?"0":cLimit;
                    String  limit = (TakeOrder.c_Limit.equals(""))?"0":TakeOrder.c_Limit;

                    if(!cardType.equals("CASH CARD") && !cardType.equals("SMART CARD"))
                    {

                        if(cardType.equals("PARTY CARD"))
                        {
                            jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid,Bill,finalBillMode,otType,mstoreid,mguest);
                            status=jsonObject.toString();
                            result=jsonObject.optString("Value");

                            JSONArray array=jsonObject.getJSONArray("Value");
                            ItemList it=null;

                            for(int i=0;i<array.length();i++)
                            {
                                it=new ItemList();
                                JSONObject object=array.getJSONObject(i);

                                String status=object.getString("status");
                                String Quantity=object.getString("qty");
                                String stock=object.getString("stock");
                                String rate=object.optString("rate");
                                String itemcode=object.optString("itemcode");
                                String itemName=object.optString("itemname");
                                String billno=object.optString("billid");

                                it.setItemCode(itemcode);
                                it.setItemName(itemName);
                                it.setStatus(status);
                                it.setStock(stock);
                                it.setQuantity(Quantity);
                                it.setRate(rate);
                                it.setBillno(billno);
                                placeOrder_list.add(it);
                            }
                        }
                        else if(C.equals("-0") || TakeOrder.check.equals("False")||C.equals("-0.0"))
                        {
                            //  JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                            jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid,Bill,finalBillMode,otType,mstoreid,mguest);
                            status=jsonObject.toString();
                            result=jsonObject.optString("Value");

                            JSONArray array=jsonObject.getJSONArray("Value");
                            ItemList it=null;

                            for(int i=0;i<array.length();i++)
                            {
                                it=new ItemList();
                                JSONObject object=array.getJSONObject(i);

                                String status=object.getString("status");
                                String Quantity=object.getString("qty");
                                String stock=object.getString("stock");
                                String rate=object.optString("rate");
                                String itemcode=object.optString("itemcode");
                                String itemName=object.optString("itemname");
                                String billno=object.optString("billid");

                                it.setItemCode(itemcode);
                                it.setItemName(itemName);
                                it.setStatus(status);
                                it.setStock(stock);
                                it.setQuantity(Quantity);
                                it.setRate(rate);
                                it.setBillno(billno);
                                placeOrder_list.add(it);
                            }
                        }
                        else if(Totalamount<=(Double.parseDouble(limit)-Float.parseFloat(C)))
                        {
                            //  JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                         //   JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                            jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid,Bill,finalBillMode,otType,mstoreid,mguest);
                            status=jsonObject.toString();
                            result=jsonObject.optString("Value");
                            JSONArray array=jsonObject.getJSONArray("Value");
                            ItemList it=null;

                            for(int i=0;i<array.length();i++)
                            {
                                it=new ItemList();
                                JSONObject object=array.getJSONObject(i);

                                String status=object.getString("status");
                                String Quantity=object.getString("qty");
                                String stock=object.getString("stock");
                                String rate=object.optString("rate");
                                String itemcode=object.optString("itemcode");
                                String itemName=object.optString("itemname");
                                String billno=object.optString("billid");

                                it.setItemCode(itemcode);
                                it.setItemName(itemName);
                                it.setStatus(status);
                                it.setStock(stock);
                                it.setQuantity(Quantity);
                                it.setRate(rate);
                                it.setBillno(billno);
                                placeOrder_list.add(it);
                            }

                        }
                        else
                        {
                            status="Credit exceeds";
                        }

                    }
                    else
                    {

                        if(cardType.equals("SMART CARD"))
                        {
                            if(Float.parseFloat(C)-Totalamount >0 )
                            {
                                // JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                               // JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid,Bill,finalBillMode,otType,mstoreid,mguest);
                                status=jsonObject.toString();
                                result=jsonObject.optString("Value");
                                JSONArray array=jsonObject.getJSONArray("Value");
                                ItemList it=null;

                                for(int i=0;i<array.length();i++)
                                {
                                    it=new ItemList();
                                    JSONObject object=array.getJSONObject(i);

                                    String status=object.getString("status");

                                    String Quantity=object.getString("qty");
                                    String stock=object.getString("stock");
                                    String rate=object.optString("rate");
                                    String itemcode=object.optString("itemcode");
                                    String itemName=object.optString("itemname");

                                    String billno=object.optString("billid");

                                    it.setItemCode(itemcode);
                                    it.setItemName(itemName);
                                    it.setStatus(status);
                                    it.setStock(stock);
                                    it.setQuantity(Quantity);
                                    it.setRate(rate);
                                    it.setBillno(billno);
                                    placeOrder_list.add(it);
                                }

                            }else{
                                status="Credit exceeds";
                            }

                        }
                        else
                        {
                            // TODO need to validate for thr fillied cards ------

                            if(isCashCardFilled.equalsIgnoreCase("true"))
                            {


                                if(Float.parseFloat(C)-Totalamount >0 )
                                {
                                    // JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                                   // JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                    jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid,Bill,finalBillMode,otType,mstoreid,mguest);
                                    status=jsonObject.toString();
                                    result=jsonObject.optString("Value");
                                    JSONArray array=jsonObject.getJSONArray("Value");
                                    ItemList it=null;

                                    for(int i=0;i<array.length();i++)
                                    {
                                        it=new ItemList();
                                        JSONObject object=array.getJSONObject(i);

                                        String status=object.getString("status");

                                        String Quantity=object.getString("qty");
                                        String stock=object.getString("stock");
                                        String rate=object.optString("rate");
                                        String itemcode=object.optString("itemcode");
                                        String itemName=object.optString("itemname");

                                        String billno=object.optString("billid");

                                        it.setItemCode(itemcode);
                                        it.setItemName(itemName);
                                        it.setStatus(status);
                                        it.setStock(stock);
                                        it.setQuantity(Quantity);
                                        it.setRate(rate);
                                        it.setBillno(billno);
                                        placeOrder_list.add(it);
                                    }

                                }else{
                                    status="Credit exceeds";
                                }
                            }else if (isCashCardFilled.equalsIgnoreCase("false"))
                            {

                             //   JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                jsonObject = api.cms_WebplaceOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid,Bill,finalBillMode,otType,mstoreid,mguest);
                                status=jsonObject.toString();
                                result=jsonObject.optString("Value");
                                JSONArray array=jsonObject.getJSONArray("Value");
                                ItemList it=null;

                                for(int i=0;i<array.length();i++)
                                {
                                    it=new ItemList();
                                    JSONObject object=array.getJSONObject(i);

                                    String status=object.getString("status");

                                    String Quantity=object.getString("qty");
                                    String stock=object.getString("stock");
                                    String rate=object.optString("rate");
                                    String itemcode=object.optString("itemcode");
                                    String itemName=object.optString("itemname");

                                    String billno=object.optString("billid");

                                    it.setItemCode(itemcode);
                                    it.setItemName(itemName);
                                    it.setStatus(status);
                                    it.setStock(stock);
                                    it.setQuantity(Quantity);
                                    it.setRate(rate);
                                    it.setBillno(billno);
                                    placeOrder_list.add(it);
                                }

                            }else {

                                status = "Check Card";
                            }

                        }


                    }

                }
                else
                {
                    status="No item";
                }

            } catch (Exception e)
            {
                e.printStackTrace();
                status=e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pd=new ProgressDialog(PlaceOrder_new.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            pd.dismiss();
            String IsOTRequired=sharedpreferences.getString("IsOTRequired","");
            if(IsOTRequired.equals("True")) {
                if (s.equals("Credit exceeds")) {
                    if (cardType.equals("CASH CARD") && isCashCardFilled.equalsIgnoreCase("True")) {
                        showPopUp();

                    } else if (cardType.equals("SMART CARD")) {
                        showPopUp();
                    } else {
                        Toast.makeText(getApplicationContext(), "Credit limit exceeds", Toast.LENGTH_LONG).show();
                    }

                } else if (s.contains("true")) {
                    Toast.makeText(getApplicationContext(), "Order placed", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(PlaceOrder_new.this, PlaceOrder_bill.class);
                    i.putExtra("mName", creditName);
                    i.putExtra("a/c", creditAccountno);
                    i.putExtra("tableNo", newTableNo);
                    i.putExtra("tableId", tableId);
                    i.putExtra("bill_id", "null");
                    i.putExtra("Type", "b");
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    startActivity(i);

                    int cc = db.updateTableNo(creditAccountno, newTableNo, storeId,userId,"","",waiternamecode);
                    finish();
                } else if (s.contains("Unable to connect to the remote server")) {
                    Toast.makeText(getApplicationContext(), "Order placed -- > POS system is off", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(PlaceOrder_new.this, PlaceOrder_bill.class);
                    i.putExtra("mName", creditName);
                    i.putExtra("a/c", creditAccountno);
                    i.putExtra("tableNo", newTableNo);
                    i.putExtra("tableId", tableId);
                    i.putExtra("bill_id", "null");
                    i.putExtra("Type", "b");
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    startActivity(i);

                    int cc = db.updateTableNo(creditAccountno, newTableNo, storeId,userId,"","",waiternamecode);
                    finish();
                } else if (s.contains("No item")) {
                    Toast.makeText(getApplicationContext(), "No item is ordered-->" + s, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Order not placed-->" + s + "  " + jsonObject, Toast.LENGTH_LONG).show();
                }
            }
            else if(IsOTRequired.equals("False"))
            {
                if (s.equals("Credit exceeds")) {
                    if (cardType.equals("CASH CARD") && isCashCardFilled.equalsIgnoreCase("True")) {
                        showPopUp();

                    } else if (cardType.equals("SMART CARD")) {
                        showPopUp();
                    } else {
                        Toast.makeText(getApplicationContext(), "Credit limit exceeds", Toast.LENGTH_LONG).show();
                    }

                } else if (s.contains("true")) {

                    String close=sharedpreferences.getString("ot_close","");
                    String [] ss=close.split("#");
                    new AsyncCloseOrder().execute(ss[1], "0",mode);
                    Toast.makeText(getApplicationContext(), "Order placed", Toast.LENGTH_SHORT).show();
                    //Intent i = new Intent(PlaceOrder_new.this, PlaceOrder_bill.class);
                    Intent i = new Intent(PlaceOrder_new.this, Accountlist.class);
                    i.putExtra("mName", creditName);
                    i.putExtra("a/c", creditAccountno);
                    i.putExtra("tableNo", newTableNo);
                    i.putExtra("tableId", tableId);
                    i.putExtra("bill_id", "null");
                    i.putExtra("Type", "b");
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    //startActivity(i);
                    int cc = db.updateTableNo(creditAccountno, newTableNo, storeId,userId,"","",waiternamecode);
                    //finish();
                } else if (s.contains("Unable to connect to the remote server")) {
                    Toast.makeText(getApplicationContext(), "Order placed -- > POS system is off", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(PlaceOrder_new.this, PlaceOrder_bill.class);
                    i.putExtra("mName", creditName);
                    i.putExtra("a/c", creditAccountno);
                    i.putExtra("tableNo", newTableNo);
                    i.putExtra("tableId", tableId);
                    i.putExtra("bill_id", "null");
                    i.putExtra("Type", "b");
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    startActivity(i);
                    int cc = db.updateTableNo(creditAccountno, newTableNo, storeId,userId,"","",waiternamecode);
                    finish();
                } else if (s.contains("No item")) {
                    Toast.makeText(getApplicationContext(), "No item is ordered-->" + s, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Order not placed-->" + s + "  " + jsonObject, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void showPopUp() {

       /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();*/
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PlaceOrder_new.this);
        alertDialog.setMessage("Credit Limit Exceeds"+"\n Total amount :"+ Totalamount +"\n Credit Limit :"+C);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                return;
            }
        });
        alertDialog.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        ArrayList<String> list=null;
        list = db.getOTItemList();
        if(!list.isEmpty()){
            try{
                AlertDialog.Builder alertBulider=new AlertDialog.Builder(this);
                alertBulider.setMessage("Do you Want to Cancel the Order ?");
                alertBulider.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(Itemview.itemViewInstance!=null){
                            try {
                                Itemview.itemViewInstance.finish();
                            } catch (Exception e) {}
                        }

                        Intent i=new Intent(PlaceOrder_new.this, Accountlist.class);
                        i.putExtra("waiterid",waiterid);
                        i.putExtra("position",position);
                        startActivity(i);
                        finish();
                    }
                });
                alertBulider.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog=alertBulider.create();
                //  alertBulider.create();
                dialog.show();
            }
            catch(Exception e){
                String r=e.getMessage().toString();}
        }
        else{
            Intent i=new Intent(PlaceOrder_new.this, Accountlist.class);
            i.putExtra("tableNo",table);
            i.putExtra("tableId",tableId);
            i.putExtra("waiterid",waiterid);
            i.putExtra("position",position);
            i.putExtra("waiternamecode",waiternamecode);
            startActivity(i);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {

            case android.R.id.home:
                ArrayList<String> list=null;
                list = db.getOTItemList();
                if(!list.isEmpty()){
                    try{
                        AlertDialog.Builder alertBulider=new AlertDialog.Builder(this);
                        alertBulider.setMessage("Do you Want to Cancel the Order ?");
                        alertBulider.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(Itemview.itemViewInstance!=null){
                                    try {
                                        Itemview.itemViewInstance.finish();
                                    } catch (Exception e) {}
                                }

                                Intent i=new Intent(PlaceOrder_new.this, Accountlist.class);
                                i.putExtra("waiterid",waiterid);
                                i.putExtra("position",position);
                                startActivity(i);
                                finish();
                            }
                        });
                        alertBulider.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog dialog=alertBulider.create();
                        //  alertBulider.create();
                        dialog.show();
                    }
                    catch(Exception e){
                        String r=e.getMessage().toString();}
                }
                else{
                    Intent i=new Intent(PlaceOrder_new.this, Accountlist.class);
                    i.putExtra("tableNo",table);
                    i.putExtra("tableId",tableId);
                    i.putExtra("waiterid",waiterid);
                    i.putExtra("position",position);
                    i.putExtra("waiternamecode",waiternamecode);
                    startActivity(i);
                    finish();
                   // finish();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void alertTableNO() {

        AlertDialog.Builder alertBulider=new AlertDialog.Builder(this);
        alertBulider.setMessage("Table Number  ");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_table_no, null);
        alertBulider.setView(dialogView);

        editText = (EditText) dialogView.findViewById(R.id.tableno);

        alertBulider.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // finish();
                tableNo.setText("Table "+editText.getText().toString());
                newTableNo=editText.getText().toString();
                // newTableNo=editText.getText().toString();
            }
        });
        alertBulider.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog=alertBulider.create();
        dialog.show();

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle(""+pos + "  - "+bill);
            ab.setElevation(0);
            ab.setDisplayHomeAsUpEnabled(true);

        }
    }

    public void additems(int value)
    {
        ArrayList<String> list=null;
        noOfCounts=0;

        if(value==1)
        {
            list = db.getItemList(groupId);
        }
        else
        {
            // itemView.removeAllViews();
            itemView=null;
            view=null;
            itemName=null;
            itemCount=null;
            add=null;
            subtract=null;
            rate=null;
           // happyHour=null;
            list = db.getOTItemList();
            // TODO reo=move the comments
            if(!list.isEmpty()){
                placeOrder.setVisibility(View.VISIBLE);
            }else{
                placeOrder.setVisibility(View.GONE);
            }
        }

        for (int i = 0; i < list.size(); i++)
        {
            noOfCounts=0;
            if (itemView == null)
            {
                itemView = (LinearLayout) findViewById(R.id.itemView);
                view = getLayoutInflater().inflate(R.layout.itemlist_kot, itemView, false);
                itemName = (TextView) view.findViewById(R.id.kot_itemname);
                itemCount = (TextView) view.findViewById(R.id.kot_itemCount);
               // happyHour=(CheckBox)view.findViewById(R.id.happyhour_checbox);
               // happyHour.setVisibility(View.GONE);
                add = (ImageView) view.findViewById(R.id.kot_add);
                subtract = (ImageView) view.findViewById(R.id.kot_minus);
                rate=(TextView) view.findViewById(R.id.kot_rate);

               /* if(!list.isEmpty()){
                    txt_itemcount.setText(list.size()+"");
                }else{
                    txt_itemcount.setText("0");
                }*/



            } else {
                view = getLayoutInflater().inflate(R.layout.itemlist_kot, itemView, false);
                itemName = (TextView) view.findViewById(R.id.kot_itemname);
                itemCount = (TextView) view.findViewById(R.id.kot_itemCount);
                //happyHour=(CheckBox)view.findViewById(R.id.happyhour_checbox);
                //happyHour.setVisibility(View.GONE);
                add = (ImageView) view.findViewById(R.id.kot_add);
                subtract = (ImageView) view.findViewById(R.id.kot_minus);
                itemView = (LinearLayout) findViewById(R.id.itemView);
                rate=(TextView) view.findViewById(R.id.kot_rate);
              /*  if(!list.isEmpty() )
                {
                    txt_itemcount.setText(list.size()+"");
                }else{
                    txt_itemcount.setText("0");
                }*/



            }

//            happyHour.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    ViewGroup parent = (ViewGroup) v.getParent();
//                    TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
//                    TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);
//                    CheckBox happy=(CheckBox)parent.findViewById(R.id.happyhour_checbox);
//
//                    String _itemname = t2.getText().toString();
//                    String count = t1.getText().toString();
//
//
//                    if (_itemname.contains("-"))
//                    {
//                        String array[] = _itemname.split("-");
//                        if(happy.isChecked()){
//                            Long rvalur = db.insertOT_tempHappyHour(array[0], array[1], "1");
//                            if (rvalur < 0) {
//                                Toast.makeText(getApplicationContext(), "happyHour not set", Toast.LENGTH_LONG).show();
//                            }
//                        }else {
//                            Long rvalur = db.insertOT_tempHappyHour(array[0], array[1], "0");
//                            if (rvalur < 0) {
//                                Toast.makeText(getApplicationContext(), "happyHour not set", Toast.LENGTH_LONG).show();
//                            }
//                        }
//
//                    }
//                }
//            });



            itemName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int id = v.getId();
                    ViewGroup parent = (ViewGroup) v.getParent();
                    TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                    TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);

                    String _itemname = t2.getText().toString();
                    if (!_itemname.equals("")|| _itemname != null ){
                        String[] ss=_itemname.split("-");
                        String ii=ss[0];

                        String _itemcode = t1.getText().toString();
                        Intent i=new Intent(PlaceOrder_new.this,Alert_ItemDescription.class);
                        i.putExtra("itemName",_itemname);
                        i.putExtra("itemCode",ii);
                        startActivity(i);
                    }else {
                        Toast.makeText(PlaceOrder_new.this, "No Item", Toast.LENGTH_SHORT).show();
                    }

                }
            });



            add.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           int id = v.getId();
                                           ViewGroup parent = (ViewGroup) v.getParent();
                                           TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                                           TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);

                                           String _itemname = t2.getText().toString();
                                           String _itemcode = t1.getText().toString();
                                           String count = t1.getText().toString();

                                           if(!(count.contains(".") || count.equals("")))
                                           {
                                               int c = Integer.parseInt(count);
                                               t1.setText(c + 1 + "");
                                               int co=c+1;
                                               noOfCounts = noOfCounts + 1;

                                               if (_itemname.contains("-")) {
                                                   String array[] = _itemname.split("-");
                                                   Long rvalur = db.insertOT(array[0], array[1], Integer.toString(co));
                                                   if (rvalur < 0) {
                                                       Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                   }else{

                                                   }

                                               }
                                           }
                                           else if(count.contains("."))
                                           {
                                               double c = Double.parseDouble(count);
                                               t1.setText(c + 0.5 + "");
                                               double co=c+1;
                                               noOfCounts = noOfCounts + 0.5;
                                               if (_itemname.contains("-")) {
                                                   String array[] = _itemname.split("-");
                                                   Long rvalur = db.insertOT(array[0], array[1], Double.toString(co));
                                                   if (rvalur < 0) {
                                                       Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                   }else{

                                                   }

                                               }
                                           }
                                           else if(count.equals(""))
                                           {
                                               t1.setText("1");
                                               noOfCounts = noOfCounts + 1;
                                               // txt_itemcount.setText(noOfCounts + "");
                                               if (_itemname.contains("-"))
                                               {
                                                   String array[] = _itemname.split("-");
                                                   Long rvalur = db.insertOT(array[0], array[1], "1");
                                                   if (rvalur < 0) {
                                                       Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                   }else{

                                                   }

                                               }
                                           }
//                                           if (count.equals(""))
//                                           {
//                                               t1.setText("1");
//                                               noOfCounts = noOfCounts + 1;
//                                               // txt_itemcount.setText(noOfCounts + "");
//
//                                               if (_itemname.contains("-"))
//                                               {
//                                                   String array[] = _itemname.split("-");
//                                                   Long rvalur = db.insertOT(array[0], array[1], "1");
//                                                   if (rvalur < 0) {
//                                                       Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
//                                                   }else{
//
//                                                   }
//
//                                               }
//
//                                           } else {
//                                               int c = Integer.parseInt(count);
//                                               t1.setText(c + 1 + "");
//                                               int co=c+1;
//                                               noOfCounts = noOfCounts + 1;
//
//                                               if (_itemname.contains("-")) {
//                                                   String array[] = _itemname.split("-");
//                                                   Long rvalur = db.insertOT(array[0], array[1], Integer.toString(co));
//                                                   if (rvalur < 0) {
//                                                       Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
//                                                   }else{
//
//                                                   }
//
//                                               }
//                                               //  txt_itemcount.setText(noOfCounts + "");
//                                           }


                                       }
                                   }

            );
            subtract.setOnClickListener(new View.OnClickListener()

                                        {
                                            @Override
                                            public void onClick (View v)
                                            {
                                                try {
                                                    ViewGroup parent = (ViewGroup) v.getParent();
                                                    TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                                                    TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);

                                                    String _itemname = t2.getText().toString();
                                                    String _itemcode = t1.getText().toString();
                                                    String count = t1.getText().toString();

                                                    if(!(count.contains(".")|| count.equals("")))
                                                    {
                                                        int c = Integer.parseInt(count);
                                                        if (c != 0) {
                                                            t1.setText(c - 1 + "");
                                                            noOfCounts = noOfCounts - 1;
                                                            int co=c - 1;

                                                            if (_itemname.contains("-"))
                                                            {
                                                                String array[] = _itemname.split("-");
                                                                Long rvalur = db.insertOT(array[0], array[1], Integer.toString(co));
                                                                if (rvalur < 0)
                                                                {
                                                                    Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                                }

                                                            }

                                                            // txt_itemcount.setText(noOfCounts + "");
                                                        }
                                                    }
                                                    else if(count.contains("."))
                                                    {
                                                        double c = Double.parseDouble(count);
                                                        if (c != 0) {
                                                            t1.setText(c - 0.5 + "");
                                                            noOfCounts = noOfCounts - 0.5;
                                                            double co=c - 0.5;
                                                            if (_itemname.contains("-"))
                                                            {
                                                                String array[] = _itemname.split("-");
                                                                Long rvalur = db.insertOT(array[0], array[1], Double.toString(co));
                                                                if (rvalur < 0)
                                                                {
                                                                    Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                                }
                                                                else
                                                                {

                                                                }

                                                            }

                                                            // txt_itemcount.setText(noOfCounts + "");
                                                        }
                                                    }
                                                    else if(count.equals(""))
                                                    {
                                                        Toast.makeText(getApplicationContext(), "Add item first", Toast.LENGTH_LONG).show();
                                                    }

//                                                    if (count.equals("")) {
//                                                        Toast.makeText(getApplicationContext(), "Add item first", Toast.LENGTH_LONG).show();
//                                                    } else {
//                                                        int c = Integer.parseInt(count);
//                                                        if (c != 0) {
//                                                            t1.setText(c - 1 + "");
//                                                            noOfCounts = noOfCounts - 1;
//                                                            int co=c - 1;
//
//                                                            if (_itemname.contains("-"))
//                                                            {
//                                                                String array[] = _itemname.split("-");
//                                                                Long rvalur = db.insertOT(array[0], array[1], Integer.toString(co));
//                                                                if (rvalur < 0)
//                                                                {
//                                                                    Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
//                                                                }
//                                                                else
//                                                                    {
//
//                                                                }
//
//                                                            }
//
//                                                            // txt_itemcount.setText(noOfCounts + "");
//                                                        }
//                                                    }



                                                } catch (Exception e) {
                                                    String hh = e.getMessage().toString();
                                                }

                                            }
                                        }

            );
            //itemName.setId(i);
            // itemCount.setId(i);
            //add.setId(i);
            //subtract.setId(i);
            String ll[] = list.get(i).toString().split("\\$");
            if(ll.length>1)
            {
                itemName.setText(ll[0]);
                itemCount.setText(ll[1]);
                noOfCounts = noOfCounts + Double.parseDouble(ll[1]);
               // Double r=Double.parseDouble(ll[2]);
                rate.setText(ll[2]+"₹");

                /*if (!list.isEmpty()) {
                    txt_itemcount.setText(list.size() + "");
                } else {
                    txt_itemcount.setText("0");
                }*/

            }
            else
            {
                itemName.setText(ll[0]);
                rate.setText(ll[2]+"₹");
            }

            itemView.addView(view);
        }

    }

    private void AlertPayment(final String s, String s1,String cardType,String isCashCardFilled) {
        paymentMode=new ArrayList<>();
        if(bill.equals("Regular"))
        {
            switch(cardType)
            {

                case "MEMBER CARD":

                    paymentMode.add("Account");
                    paymentMode.add("Cash");
                    paymentMode.add("Credit");

                    break;

                case "CASH CARD":

                    if(MainActivity.cashCard.equals("true"))
                    {
                        paymentMode.add("Account");
                    }
                    else
                    {
                        paymentMode.add("Cash");
                        paymentMode.add("Credit");
                    }

                    break;

                case "CLUB CARD":

                    paymentMode.add("Account");

                    break;


                case "ROOM CARD":

                    paymentMode.add("Account");
                    //   paymentMode.add("Complimentary");

                    break;

                case "DEPENDENT CARD":

                    paymentMode.add("Account");
                    paymentMode.add("Cash");
                    paymentMode.add("Credit");

                    break;
                case "MEMBER DUPL CARD":

                    paymentMode.add("Account");
                    paymentMode.add("Cash");
                    paymentMode.add("Credit");

                    break;

                case "SMART CARD":

                    paymentMode.add("Account");

                    break;
            }



            if(!paymentMode.isEmpty())
            {

                payments = new String[paymentMode.size()];
                payments = paymentMode.toArray(payments);

            }

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Select the Payment Mode");
            builder.setItems(payments, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int _paymentMode) {

                    // mode=payments[paymentMode];
                    mode= paymentMode.get(_paymentMode);
                    //new AsyncCloseOrder().execute(s, "0",mode);
                    new PlaceOrder_new.AsyncPlaceOrder2().execute();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();


        }

//        else if(bill.equals("Party Billing"))
//        {
//            if(cardType.equals("PARTY CARD"))
//            {
//                new AsyncCloseOrder().execute(s,"0","Account");
//            }
//            else
//            {
//                Toast.makeText(PlaceOrder_new.this, "This card is Not applicable for Party Billing ", Toast.LENGTH_SHORT).show();
//            }
//        }
//        else if(bill.equals("Direct Party Billing"))
//        {
//            if(cardType.equals("DIRECTPARTY CARD"))
//            {
//                new AsyncCloseOrder().execute(s,"0","Account");
//            }
//            else
//            {
//                Toast.makeText(PlaceOrder_new.this, "This card is Not applicable for Direct Party Billing ", Toast.LENGTH_SHORT).show();
//            }
//        }
//        else if(bill.equals("Compliment"))
//        {
//            if(cardType.equals("MEMBER CARD") ||cardType.equals("ROOM CARD"))
//            {
//                new AsyncCloseOrder().execute(s, "0","Complimentary");
//            }
//            else
//            {
//                Toast.makeText(PlaceOrder_new.this, "This card is Not applicable for Compliment Billing ", Toast.LENGTH_SHORT).show();
//            }
//
//        }
//        else if(bill.equals("coupon"))
//        {
//            if(cardType.equals("MEMBER CARD"))
//            {
//                new AsyncCloseOrder().execute(s, "0","Coupon");
//
//            }
//            else
//            {
//                Toast.makeText(this, "This card is not applicable for coupon ", Toast.LENGTH_SHORT).show();
//            }
//
//
//        }
        else
        {
            Toast.makeText(this, "This card is not applicable ", Toast.LENGTH_SHORT).show();
        }
    }

    protected class AsyncCloseOrder extends AsyncTask<String, Void, String> {
        String status = "";
        String storeId = sharedpreferences.getString("storeId", "");
        String deviceid = sharedpreferences.getString("deviceId", "");
        String userId = sharedpreferences.getString("userId", "");
        String pageType = "";
        String mstoreid=sharedpreferences.getString("mstoreId","");
        String billtype=sharedpreferences.getString("BillMode","");
        JSONObject jsonObject;
        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(PlaceOrder_new.this);
            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                closeorderItemsArrayList.clear();
                if(bill.equals("coupon")){
                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, "COUPON", pos,params[2],contractorsId,mstoreid,billtype,0,"P",waiterid);
                }else if(bill.equals("Party Billing")){
                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, "PARTY CARD", pos,params[2],contractorsId,mstoreid,billtype,0,"P",waiterid);
                }
                else if(bill.equals("Direct Party Billing")){
                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, "DIRECTPARTY CARD", pos,params[2],contractorsId,mstoreid,billtype,0,"P",waiterid);
                }
                else {

                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos,params[2],contractorsId,mstoreid,billtype,0,"P",waiterid);
                }

                JSONArray array = jsonObject.getJSONArray("Value");
                CloseorderItems cl = null;
                pageType = params[1];
                for (int i = 0; i < array.length(); i++) {
                    cl = new CloseorderItems();
                    JSONObject object = array.getJSONObject(i);

                    String ItemID = object.getString("ItemID");
                    String ItemCode = object.getString("ItemCode");
                    String Quantity = object.getString("Quantity");
                    String Amount = object.getString("Amount");
                    String BillID = object.getString("BillID");
                    String taxDescription =object.optString("taxDescription");
                    String mAcc = object.getString("memberAcc");
                    String taxValue=object.getString("taxValue");
                    String ava_balance = object.optString("avBalance");
                    String billNo = object.optString("billnumber");
                    String taxAmount =object.optString("taxAmount");
                    String Rate = object.getString("rate");
                    String itemname = object.getString("itemname");
                    String mName = object.getString("membername");
                    String memberId = object.getString("memberId");
                    String BillDate = object.getString("BillDate");
                    String ACCharge = object.getString("ACCharge");

                    String otNo=object.getString("otno");
                    String opBalance=db.getOpeningBalance(mAcc);

                    cl.setItemID(ItemID);
                    cl.setItemCode(ItemCode);
                    cl.setQuantity(Quantity);
                    cl.setAmount(Amount);
                    cl.setBillID(BillID);
                    cl.setTaxDescription(taxDescription);
                    cl.setmAcc(mAcc);
                    cl.setTaxValue(taxValue);
                    cl.setAva_balance(ava_balance);
                    cl.setBillno(billNo);
                    cl.setTaxAmount(taxAmount);
                    cl.setmName(mName);
                    cl.setMemberID(memberId);
                    cl.setBillDate(BillDate);
                    cl.setRate(Rate);
                    cl.setItemname(itemname);
                    cl.setOtno(otNo);
                    cl.setCardType(params[2]);
                    cl.setACCharge(ACCharge);
                    cl.setOpeningBalance(opBalance);

                    closeorderItemsArrayList.add(cl);
                }
                status = jsonObject.toString();

            } catch (Exception e) {
                e.printStackTrace();
                status = e.toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(PlaceOrder_new.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (pageType.equals("0"))  //close order
            {
                if (s.contains("true")) {
                    if (closeorderItemsArrayList.size() == 0 || closeorderItemsArrayList == null) {
                        // db.deleteAccount(member,posid,userId);
                        String ot=sharedpreferences.getString("ot_close","");
                        db.deleteAccount_new(ot,storeId,userId,bill);
                        //readCard();
                        Toast.makeText(getApplicationContext(), "Order not taken for the member", Toast.LENGTH_LONG).show();
                    } else {
                        //db.deleteAccount(member,posid,userId);
                        String ot=sharedpreferences.getString("ot_close","");
                        db.deleteAccount_new(ot,storeId,userId,bill);
                        Toast.makeText(getApplicationContext(), "Order closed ", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(PlaceOrder_new.this, CloseOrderdetails.class);

                        i.putExtra("pageValue", 11);
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("creditName", creditName);
                        i.putExtra("tableNo", table);
                        i.putExtra("tableId", tableId);
                        i.putExtra("waiterid",waiterid);
                        i.putExtra("cardType", cardType);
                        i.putExtra("waiternamecode",waiternamecode);
                        startActivity(i);
                        finish();
                    }

                } else {

                    Toast.makeText(getApplicationContext(), "Order not closed-->" + s, Toast.LENGTH_LONG).show();
                }
            }
//            else if (pageType.equals("1"))  //cancel order
//            {
//                if (s.contains("true")) {
//                    if (closeorderItemsArrayList.size() == 0 || closeorderItemsArrayList == null) {
//                        Toast.makeText(getApplicationContext(), "Order not taken for the member", Toast.LENGTH_LONG).show();
//                    }
//                    else {
//                       // String memeberAnumber = memberAn.getText().toString();
//                        Intent i = new Intent(PlaceOrder_new.this, CloseOrderdetails.class);
//                        i.putExtra("pageValue", 1);
//                        i.putExtra("memeberAnumber", creditAccountno);
//                        i.putExtra("creditAno", creditAccountno);
//                        i.putExtra("creditName", creditName);
//                        i.putExtra("tableNo", table);
//                        i.putExtra("cardType", cardType);
//                        i.putExtra("tableId", tableId);
//                        i.putExtra("waiterid",waiterid);
//                        i.putExtra("waiternamecode",waiternamecode);
//                        //   i.putExtra("position",position);
//                        //  i.putExtra("waiternamecode",waiternamecode);
//                        startActivity(i);
//                        finish();
//                    }
//
//
//                } else {
//
//                    Toast.makeText(getApplicationContext(), "No Item to Cancel-->" + s, Toast.LENGTH_LONG).show();
//
//                }
//            }


        }
    }

}
