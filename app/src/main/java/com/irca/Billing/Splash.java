package com.irca.Billing;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.ImageCropping.CircularImage;
import com.irca.cosmo.R;

import java.util.Calendar;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by Manoch Richard on 10/19/2015.
 */
public class Splash extends Activity {
    Calendar rightNow = Calendar.getInstance();
    ImageView imageView;
    Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Fabric.with(this, new Crashlytics());
        try{
            setContentView(R.layout.splash);


            imageView=(ImageView)findViewById(R.id.imageView_logo);

            try{
                //  bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
                bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.kot);
                imageView.setImageBitmap(CircularImage.getCircleBitmap(bitmap));

            }catch(Exception e){
                String rr=e.getMessage().toString() ;
            }


            //if (rightNow.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(Splash.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }
            }, 2000);
        }
        catch (Exception e){
            Toast.makeText(Splash.this,"Excep::"+e,Toast.LENGTH_LONG).show();
        }

    }


}
