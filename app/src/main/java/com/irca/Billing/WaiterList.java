package com.irca.Billing;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

//import com.crashlytics.android.Crashlytics;
import com.irca.CustomAdapters.WaiterAdapter;
import com.irca.cosmo.R;
import com.irca.db.Dbase;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

//import io.fabric.sdk.android.Fabric;


public class WaiterList extends AppCompatActivity {
    ListView listView;
    Dbase db;
    FloatingActionButton addButton;
    EditText inputSearch;
    WaiterAdapter adapter = null;
    List<String> list;
    List<String> temp = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.store_list);
        db = new Dbase(WaiterList.this);
        list = db.getWaiterList1();
        listView = findViewById(R.id.list_store);
        adapter = new WaiterAdapter(WaiterList.this, list);
        listView.setAdapter(adapter);
        addButton = findViewById(R.id.add);
        addButton.setVisibility(View.GONE);
        inputSearch = findViewById(R.id.waiterSearch);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //if (i > 0) {
                if (temp.size() > 0) {
                    BillingProfile.waiternamecode = temp.get(i);
                } else {
                    BillingProfile.waiternamecode = list.get(i);
                }
                String array[] = BillingProfile.waiternamecode.split("_");
                String waiterid = db.getWaiterId(array[0].trim());
                Intent intent = new Intent();
                intent.putExtra("waiterid", waiterid);
                intent.putExtra("waiternamecode", BillingProfile.waiternamecode);
                setResult(2, intent);
                finish();//finishing ac
                // } else {
                //  Toast.makeText(getApplicationContext(), "Select Steward", Toast.LENGTH_LONG).show();
                //}

            }
        });
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    void filter(String text) {
        temp = new ArrayList<>();
        for (String d : list) {
            if (d.toLowerCase().contains(text.toLowerCase())) {
                temp.add(d);
            }
        }

        //update recyclerview
        adapter.updateList(temp);
    }

}
