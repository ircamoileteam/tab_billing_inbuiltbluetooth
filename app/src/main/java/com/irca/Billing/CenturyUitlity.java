package com.irca.Billing;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;

import org.json.JSONObject;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by Manoch Richard on 06-Jul-17.
 */

public class CenturyUitlity extends AppCompatActivity {
    TextView txt_membername, txt_accno, txt_balance;
    Button submit;
    EditText amount;
    String acno = "", membername = "", balance = "";
    SharedPreferences sharedpreferences;
    ProgressDialog pd;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Fabric.with(this, new Crashlytics());
        setContentView(R.layout.century_utility);
        txt_membername = (TextView) findViewById(R.id.txt_name);
        txt_accno = (TextView) findViewById(R.id.txt_accno);
        txt_balance = (TextView) findViewById(R.id.txt_balance);
        amount = (EditText) findViewById(R.id.editText_amount);
        submit = (Button) findViewById(R.id.button_placeutility);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        cd = new ConnectionDetector(getApplicationContext());
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String AMOUNT = amount.getText().toString();
                if (!AMOUNT.equals("")) {
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new AsyncUtility().execute(AMOUNT);

                    } else
                    {
                        showMessage("No internet connection", CenturyUitlity.this);
                    }

                } else {
                    showMessage("Enter Amount", CenturyUitlity.this);
                }
            }
        });
        Bundle b = getIntent().getExtras();
        if (b != null)
        {
            acno = b.getString("creditAno");
            membername = b.getString("creditName");
            balance = b.getString("creditLimit");
            txt_membername.setText(membername);
            txt_accno.setText(acno);
            txt_balance.setText(balance);
        }

    }

    protected class AsyncUtility extends AsyncTask<String, Void, String> {
        String storeId = sharedpreferences.getString("storeId", "");
        String userId = sharedpreferences.getString("userId", "");
        RestAPI api = new RestAPI(CenturyUitlity.this);
        String status = "";
        String mstoreid=sharedpreferences.getString("mstoreId","");

        @Override
        protected String doInBackground(String... params) {
            try {
                JSONObject jsonObject = api.cms_Utility(acno, params[0], Build.SERIAL + "", storeId, userId,mstoreid);
                if (jsonObject.toString().contains("true")) {
                    status = jsonObject.getString("Value");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(CenturyUitlity.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (!s.equals("")) {
                showMessage("Order place Successfully Referenceid is:" + s, CenturyUitlity.this);
                Intent intent=new Intent();
                intent.putExtra("MESSAGE","Successfully");
                setResult(2,intent);
                finish();
            } else {
                showMessage("Error pls check report", CenturyUitlity.this);
            }

        }
    }

    private void showMessage(String text, Context c) {

        Toast toast = Toast.makeText(c, "message", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 10, 50);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setText(text);
        toast.show();

    }
}
