package com.irca.Billing;

import android.annotation.TargetApi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.CustomAdapters.ListAdapter;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.db.Dbase;
import com.irca.fields.MasterStore;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by Manoch Richard on 10/22/2015.
 */
public class StoreList extends AppCompatActivity {
    ListView listView;
    ProgressDialog pd;
    Dbase db;
    SharedPreferences sharedpreferences;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    LinearLayout dummy;
    FloatingActionButton add;
    String pos = "";
    //ImageView logo;
    // Bitmap bitmap;
    EditText inputSearch;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Fabric.with(this, new Crashlytics());
        setContentView(R.layout.store_list);
        actionBarSetup();
        db = new Dbase(StoreList.this);
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        userId = sharedpreferences.getString("userId", "");
        dummy = (LinearLayout) findViewById(R.id.dummy);
        add = (FloatingActionButton) findViewById(R.id.add);
        inputSearch = (EditText) findViewById(R.id.waiterSearch);
        inputSearch.setVisibility(View.GONE);
        dummy.setVisibility(View.GONE);
        add.setVisibility(View.GONE);
        listView = (ListView) findViewById(R.id.list_store);
        //  logo=(ImageView)findViewById(R.id.imageView_logo);
        //  bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
        // logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));

        pos = sharedpreferences.getString("StoreName", "");
        final ArrayList<MasterStore> list = db.getStoreList();

        if (list.size() == 0) {
            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new AsyncStore().execute();
            } else {
                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
            }
        } else {
            ListAdapter adapter = new ListAdapter(StoreList.this, list);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    try {
                        String oldPosid = sharedpreferences.getString("storeId", "");
                        db.deleteItemGroup(oldPosid);
                        db.deleteItem(oldPosid);
                        int itemPosition = position;
                        //  Intent i = new Intent(StoreList.this, Dashboard.class);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("storeId", list.get(position).getID() + "");
                        editor.putString("StoreName", list.get(position).getMobilestorename() + "");
                        editor.putString("itemCategoryId", list.get(position).getItemCategoryID() + "");
                        editor.putString("billType", list.get(position).getOTType() + "");
                        editor.putString("cash", list.get(position).getIsCashSalesRequire() + "");
                        editor.putString("credit", list.get(position).getIsCreditSalesRequire() + "");
                        editor.putString("account", list.get(position).getIsDebitSalesRequired() + "");
                        editor.putString("party", list.get(position).getIsPartyBillApplicable() + "");
                        editor.putString("Dashboard", list.get(position).getDashboard() + "");
                        editor.putString("IsOTRequired", list.get(position).getIsOTRequired() + "");
                        editor.putString("mstoreId", list.get(position).getMid() + "");
                        editor.putString("misReader", list.get(position).getIsReader() + "");
                        editor.putString("misguest", list.get(position).getIsGuest() + "");
                        editor.putString("clubAddress", list.get(position).getAddress() + "");
                        editor.putString("mstoreGST", list.get(position).getGSTNumber() + "");
                        editor.putString("StewardBilling", list.get(position).getStewardwiseBilling() + "");
                        editor.putString("decimal", list.get(position).getIsdecimalval() + "");
                        editor.putString("isPax", list.get(position).getIsPaxCountMandatory() + "");
                        editor.putString("isTable", list.get(position).getIstableNoManadatory() + "");
                        editor.putString("IsCashC", list.get(position).getIsCashSalesRequire1() + "");
                        editor.putString("IsCreditC", list.get(position).getIsCreditSalesRequire1() + "");
                        editor.putString("IsDebitC", list.get(position).getIsDebitSalesRequired1() + "");
                        editor.putString("IsChequeC", list.get(position).getIsChequeSalesRequire() + "");
                        editor.putString("voidOT", list.get(position).getVoidOT() + "");
                        editor.putString("PrinterType", list.get(position).getPrinterType() + "");

                        editor.apply();
                        Class c = Class.forName("com.irca.Billing." + list.get(position).getDashboard());
                        Intent i = new Intent(StoreList.this, c);
                        i.putExtra("Storeid", list.get(position).getID());
                        i.putExtra("position", position);
                        startActivity(i);
                        finish();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }


                }

            });

        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("Store List");
            // ab.setElevation(0);

        }
    }

    protected class AsyncStore extends AsyncTask<String, Void, ArrayList<MasterStore>> {

        ArrayList<MasterStore> masterStoreArrayList = null;
        Long storeInsertCount = null;

        @Override
        protected ArrayList<MasterStore> doInBackground(String... params) {
            RestAPI api = new RestAPI(StoreList.this);
            try {
                //  JSONObject jsonObject=api.cms_getPOS();
                JSONObject jsonObject = api.CmsGetPosNew(userId);
                JSONObject jsonObject2 = jsonObject.getJSONObject("Value");
                JSONArray jsonArray = jsonObject2.getJSONArray("Table");
                MasterStore masterStore = null;
                String id = "0";
                masterStoreArrayList = new ArrayList<MasterStore>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    masterStore = new MasterStore();
                    id = jsonObject1.get("ID").toString();//store id
                    String StoreCode = jsonObject1.get("StoreCode").toString();
                    String storeNmae = jsonObject1.get("StoreName").toString();
                    String itemCategoryId = jsonObject1.get("ItemCategoryID").toString();//itemcategoryid
                    String billtype = jsonObject1.get("OTType").toString();
                    String cash = jsonObject1.get("IsCashSalesRequire").toString();
                    String credit = jsonObject1.get("IsCreditSalesRequire").toString();
                    String account = jsonObject1.get("IsDebitSalesRequired").toString();
                    String party = jsonObject1.get("IsPartyBillApplicable").toString();
                    String Dashboard = jsonObject1.get("Dashboard").toString();
                    String IsOTRequired = jsonObject1.get("IsOTRequired").toString();
                    String MobileStoreId = jsonObject1.get("Mid").toString();
                    String MobileStoreName = jsonObject1.get("Mobilestorename").toString();
                    String isReader = jsonObject1.get("isReader").toString();
                    String isGuest = jsonObject1.get("isGuest").toString();
                    String Address = jsonObject1.get("Address").toString();
                    String GSTNumber = jsonObject1.get("GSTNumber").toString();
                    String decimal = jsonObject1.optString("isdecimalval", "");
                    String isPax = jsonObject1.optString("IsPaxCount", "");
                    String isTable = jsonObject1.optString("IsTableNoMandatory", "");
                    String st = jsonObject1.optString("StewardwiseBilling", "");
                    String isCashC = jsonObject1.optString("IsCashSalesRequire1", "");
                    String isCreditC = jsonObject1.optString("IsCreditSalesRequire1", "");
                    String isDebitC = jsonObject1.optString("IsDebitSalesRequired1", "");
                    String isChequeC = jsonObject1.optString("IsChequeSalesRequire", "");
                    String voidOT = jsonObject1.optString("voidOT", "");
                    String PrinterType = jsonObject1.optString("PrinterType", "");
                    String OTCreditLimitNotRequired = jsonObject1.optString("OTCreditLimitNotRequired", "");

                    masterStore.setIsCashSalesRequire1(isCashC);
                    masterStore.setIsCreditSalesRequire1(isCreditC);
                    masterStore.setIsDebitSalesRequired1(isDebitC);
                    masterStore.setIsChequeSalesRequire(isChequeC);
                    masterStore.setID(id);
                    masterStore.setIsPaxCountMandatory(isPax);
                    masterStore.setIstableNoManadatory(isTable);
                    masterStore.setStoreCode(StoreCode);
                    masterStore.setStoreName(storeNmae);
                    masterStore.setItemCategoryID(itemCategoryId);
                    masterStore.setOTType(billtype);
                    masterStore.setIsCashSalesRequire(cash);
                    masterStore.setIsCreditSalesRequire(credit);
                    masterStore.setIsDebitSalesRequired(account);
                    masterStore.setIsPartyBillApplicable(party);
                    masterStore.setDashboard(Dashboard);
                    masterStore.setIsOTRequired(IsOTRequired);
                    masterStore.setMid(MobileStoreId);
                    masterStore.setMobilestorename(MobileStoreName);
                    masterStore.setIsReader(isReader);
                    masterStore.setIsGuest(isGuest);
                    masterStore.setAddress(Address);
                    masterStore.setGSTNumber(GSTNumber);
                    masterStore.setIsdecimalval(decimal);
                    masterStore.setStewardwiseBilling(st);
                    masterStore.setVoidOT(voidOT);
                    masterStore.setPrinterType(PrinterType);
                    masterStore.setOTCreditLimitNotRequired(OTCreditLimitNotRequired);
                    masterStoreArrayList.add(masterStore);

                }
                if (masterStoreArrayList != null) {
                    storeInsertCount = db.insertStore(masterStoreArrayList);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return masterStoreArrayList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(StoreList.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(ArrayList<MasterStore> s) {
            super.onPostExecute(s);
            try {

                pd.dismiss();
                ListAdapter adapter = new ListAdapter(StoreList.this, s);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        try {
                            String oldPosid = sharedpreferences.getString("storeId", "");
                            db.deleteItemGroup(oldPosid);
                            db.deleteItem(oldPosid);
                            int itemPosition = position;
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString("storeId", masterStoreArrayList.get(position).getID() + "");
                            editor.putString("StoreName", masterStoreArrayList.get(position).getMobilestorename() + "");
                            editor.putString("itemCategoryId", masterStoreArrayList.get(position).getItemCategoryID());
                            editor.putString("billType", masterStoreArrayList.get(position).getOTType() + "");
                            editor.putString("cash", masterStoreArrayList.get(position).getIsCashSalesRequire() + "");
                            editor.putString("credit", masterStoreArrayList.get(position).getIsCreditSalesRequire() + "");
                            editor.putString("account", masterStoreArrayList.get(position).getIsDebitSalesRequired() + "");
                            editor.putString("party", masterStoreArrayList.get(position).getIsPartyBillApplicable() + "");
                            editor.putString("Dashboard", masterStoreArrayList.get(position).getDashboard() + "");
                            editor.putString("IsOTRequired", masterStoreArrayList.get(position).getIsOTRequired() + "");
                            editor.putString("mstoreId", masterStoreArrayList.get(position).getMid() + "");
                            editor.putString("misReader", masterStoreArrayList.get(position).getIsReader() + "");
                            editor.putString("misguest", masterStoreArrayList.get(position).getIsGuest() + "");
                            editor.putString("clubAddress", masterStoreArrayList.get(position).getAddress() + "");
                            editor.putString("mstoreGST", masterStoreArrayList.get(position).getGSTNumber() + "");
                            editor.putString("decimal", masterStoreArrayList.get(position).getIsdecimalval() + "");
                            editor.putString("isPax", masterStoreArrayList.get(position).getIsPaxCountMandatory() + "");
                            editor.putString("isTable", masterStoreArrayList.get(position).getIstableNoManadatory() + "");
                            editor.putString("StewardBilling", masterStoreArrayList.get(position).getStewardwiseBilling() + "");
                            editor.putString("IsCashC", masterStoreArrayList.get(position).getIsCashSalesRequire1() + "");
                            editor.putString("IsCreditC", masterStoreArrayList.get(position).getIsCreditSalesRequire1() + "");
                            editor.putString("IsDebitC", masterStoreArrayList.get(position).getIsDebitSalesRequired1() + "");
                            editor.putString("IsChequeC", masterStoreArrayList.get(position).getIsChequeSalesRequire() + "");
                            editor.putString("voidOT", masterStoreArrayList.get(position).getVoidOT() + "");
                            editor.putString("PrinterType", masterStoreArrayList.get(position).getPrinterType() + "");
                            editor.putString("OTCreditLimitNotRequired", masterStoreArrayList.get(position).getOTCreditLimitNotRequired() + "");
                            editor.apply();
                            Intent i = null;
                            Class c = Class.forName("com.irca.Billing." + masterStoreArrayList.get(position).getDashboard());
                            i = new Intent(StoreList.this, c);
                            i.putExtra("Storeid", masterStoreArrayList.get(position).getID());
                            startActivity(i);
                            finish();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }

                    }

                });
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    public void alertMultipleChoiceItems() {

        // where we will store or remove selected items
        final ArrayList<Integer> mSelectedItems = new ArrayList<Integer>();

        AlertDialog.Builder builder = new AlertDialog.Builder(StoreList.this);

        // set the dialog title
        builder.setTitle("Choose One or More")

                // specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive call backs when items are selected
                // R.array.choices were set in the resources res/values/strings.xml
                .setMultiChoiceItems(R.array.choices, null, new DialogInterface.OnMultiChoiceClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                        if (isChecked) {
                            // if the user checked the item, add it to the selected items
                            mSelectedItems.add(which);
                        } else if (mSelectedItems.contains(which)) {
                            // else if the item is already in the array, remove it
                            mSelectedItems.remove(Integer.valueOf(which));
                        }

                        // you can also add other codes here,
                        // for example a tool tip that gives user an idea of what he is selecting
                        // showToast("Just an example description.");
                    }

                })

                // Set the action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        // user clicked OK, so save the mSelectedItems results somewhere
                        // here we are trying to retrieve the selected items indices
                        String selectedIndex = "";
                        for (Integer i : mSelectedItems) {
                            selectedIndex += i + ", ";
                        }


                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // removes the AlertDialog in the screen
                    }
                })

                .show();

    }
}