package com.irca.Billing;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.activity.ItemOTTransferActivity;
import com.irca.activity.NewMenuActivity;
import com.irca.activity.PendingBillsActivity;
import com.irca.activity.PromotionsNewActivity;
import com.irca.activity.RunningOrdersActivity;
import com.irca.activity.TableTransferActivity;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;
import com.irca.cosmo.TakeOrder;
import com.irca.cosmo.TodaysAccount;
import com.irca.db.Dbase;
import com.irca.fields.DashboardAdapter;
import com.irca.fields.DashboardDto;
import com.irca.fields.GraphDto;
import com.irca.fields.Item;
import com.irca.fields.ItemGroup;
import com.irca.fields.ItemModifier;
import com.irca.fields.OnRecyclerViewItemClickListener;
import com.irca.fields.Table;
import com.irca.fields.WaiterDetails;
import com.irca.tax.StoreClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by Manoch Richard on 24-Mar-18.
 */

public class Dashboard_NSCI extends AppCompatActivity {
    Dbase db;
    int tablecount = 0;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;

    SharedPreferences sharedpreferences;

    String posId = "";
    String serialNo = "";
    SharedPreferences bluetooth_prefrence;
    String target = "", Printername = "", billType = "";
    String[] NameList = new String[]{};
    int[] Images = new int[]{};
    int[] colours = new int[]{};
    int[] colours2 = new int[]{};
    RecyclerView gridView;
    CustomGridAdapter gridAdapter;
    String StoreName = "", isOT = "", isParty = "";
    Typeface tf;
    public static ArrayList<WaiterDetails> waiterlist = null;
    WaiterDetails waiterDetails = null;
    public static String pid, pcode, pname;
    public static String ItemCategoryID = "";
    String[] party;
    String _party = "";
    DashboardAdapter dashboardAdapter;
    public static ArrayList<ItemGroup> itemGrouplist = null;
    ItemGroup itemGroup = null;
    List<DashboardDto> dashboardList = new ArrayList<>();
    PieChart pieChart;
    ArrayList<GraphDto> grapthDtoArrayList = new ArrayList<>();
    String userId="";
    String stewardBilling="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Fabric.with(this, new Crashlytics());
        setContentView(R.layout.dahboard_neww);
        initView();
        initGridView();
        actionBarSetup();
        loadTable();
        if(stewardBilling.equalsIgnoreCase("true")){
            db.deleteSteward();
            new AsyncStewardMaster().execute();
        }


    }



    private void initView() {
        bluetooth_prefrence = getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        target = bluetooth_prefrence.getString("Target", "");
        Printername = bluetooth_prefrence.getString("Printer", "");
        String name = sharedpreferences.getString("loginName", "");
        billType = sharedpreferences.getString("billType", "");
        StoreName = sharedpreferences.getString("StoreName", "");
        isOT = sharedpreferences.getString("IsOTRequired", "");
        isParty = sharedpreferences.getString("IsPartyBillApplicable", "");
        posId = sharedpreferences.getString("storeId", "");

         userId = sharedpreferences.getString("userId", "");
        stewardBilling = sharedpreferences.getString("StewardBilling", "");

        db = new Dbase(Dashboard_NSCI.this);
    }

    private void initGridView() {
        serialNo = Build.SERIAL;
        NameList = new String[]
                {"Regular",//1
                        "Party Billing",//2
                        "Compliment",//3
                        "Offer",//4
                        "My Order",//5
                        "Menu"//6
                };
        colours = new int[]{
                R.color.g_mint1,
                R.color.g_pink1,
                R.color.g_or1,
                R.color.g_gr1,
                R.color.g_lav1,
                R.color.g_yel1
        };

        colours2 = new int[]
                {
                        R.color.g_mint2,
                        R.color.g_pink2,
                        R.color.g_or2,
                        R.color.g_gr2,
                        R.color.g_lav2,
                        R.color.g_yel12

                };


        Images = new int[]{
                R.drawable.grid_kot_bot, //regular
                R.drawable.grid_partyhall_booking,//party hall
                R.drawable.grid_room_booking,// compliment
                R.drawable.grid_coupon,// coupon
                R.drawable.grid_kitchen,// my order
                R.drawable.grid_kitchen// my order
        };// bluish green
        getData();
        gridView = findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(Dashboard_NSCI.this, 2);
        gridView.setLayoutManager(gridLayoutManager);
        dashboardAdapter = new DashboardAdapter(dashboardList, Dashboard_NSCI.this);
        gridView.setAdapter(dashboardAdapter);
        dashboardAdapter.notifyDataSetChanged();

        dashboardAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener<DashboardAdapter.MyViewHolder, DashboardDto>() {
            @Override
            public void onRecyclerViewItemClick(@NonNull DashboardAdapter.MyViewHolder myViewHolder, @NonNull View view, @NonNull DashboardDto dashboardDto, int position) {
                cd = new ConnectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                switch (dashboardDto.getId()) {

                    case "1":  //Billing
                        if (db.getTaxCount() <= 0) {
                            new Itemtax().execute();
                        }
                        loadItem("RR", NameList[position]);
                        break;

                    case "2":  // Running Orders
                        //AlertParty();
                        Intent abc = new Intent(Dashboard_NSCI.this, RunningOrdersActivity.class);
                        startActivity(abc);
                        break;

                    case "3": //Order Transfer
                        //loadItem( "RR",NameList[position] );
                        Intent a = new Intent(Dashboard_NSCI.this, ItemOTTransferActivity.class);
                        startActivity(a);
//                        Toast.makeText(Dashboard_NSCI.this, "Under Construction", Toast.LENGTH_SHORT).show();
                        break;
                    case "10": //Table Transfer
                        //loadItem( "RR",NameList[position] );
                        Intent aa = new Intent(Dashboard_NSCI.this, TableTransferActivity.class);
                        startActivity(aa);
                        break;

                    case "4": //Promotions
                        //loadItem( "RR",NameList[position] );
                        Intent intent = new Intent(Dashboard_NSCI.this, PromotionsNewActivity.class);
                        startActivity(intent);
                        break;
                    case "5": //Pending Bills
                        Intent intent1 = new Intent(Dashboard_NSCI.this, PendingBillsActivity.class);
                        startActivity(intent1);
                        //loadItem( "RR",NameList[position] );
//                        Toast.makeText(Dashboard_NSCI.this, "Under Construction ", Toast.LENGTH_SHORT).show();
                        break;

                    case "6":  // Billing List
                        Intent i = new Intent(Dashboard_NSCI.this, TodaysAccount.class);
                        i.putExtra("mpage", "0");
                        startActivity(i);
                        break;

                    case "7": //Party Billing
                        //loadItem( "RR",NameList[position] );
                        Toast.makeText(Dashboard_NSCI.this, "Under Construction ", Toast.LENGTH_SHORT).show();
                        break;

                    case "8": //Change POS
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            db.deleteStore();
                            Intent ac = new Intent(Dashboard_NSCI.this, StoreList.class);
                            ac.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(ac);
                            finish();
                        } else {
                            Intent ac = new Intent(Dashboard_NSCI.this, StoreList.class);
                            ac.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(ac);
                            finish();
                        }
                        break;
                    case "9": // menu
                        startActivity(new Intent(Dashboard_NSCI.this, NewMenuActivity.class));
//                        final ArrayList<Table> tablename = db.getTablename();
//                        int itemcount = db.getItemCount(posId);
//                        if (itemcount == 0) {
//                            new Dashboard_NSCI.AsyncMenuMaster().execute();
//                        } else {
//                            Intent i1 = new Intent(Dashboard_NSCI.this, GroupView.class);
//                            startActivity(i1);
//                        }
                        break;

                }
            }
        });

        if (!posId.isEmpty()) {
            new GetData().execute();
        } else {
            Toast.makeText(this, "Please select POS", Toast.LENGTH_SHORT).show();
        }
    }


    private void AlertParty() {
        party = new String[]{"Party Billing", "Direct Party Billing"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select the Payment Mode");
        builder.setItems(party, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int _paymentMode) {
                _party = party[_paymentMode];
                if (_party.equals("Party Billing")) {
                    loadItem("PR", _party);
                } else {
                    loadItem("BR", _party);
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void loadItem(String billmode, String billing) {
       /* monica if (posId.equals("4")) {
            billing = "coupon";
        }*/
        String storeId = sharedpreferences.getString("storeId", "");
        final ArrayList<Table> tablename = db.getTablename();
        int itemGroupCount = db.getItemGroupCount();
        int waiterCount = db.getWaiterCount();
        int itemcount = db.getItemCount(posId);
        int modifiercount = db.getModifierCount();
        int modifierdetailscount = db.getModifierDetailsCount();
    //  12-04-2021  int offerCount = db.getOfferCount();
        int offerCount =0;
        if (modifiercount == 0) {
            new MasterItemModifier().execute("1");
        }
        if (modifierdetailscount == 0) {
            new MasterItemModifier().execute("2");
        }
        if (waiterCount == 0) {
            if (tablename.size() > 0) {
                new AsyncMaster().execute(0 + 1 + "", tablename.get(0).getTableId() + "", billmode, billing);
            } else {
                new AsyncMaster().execute(0 + 1 + "", "0" + "", billmode, billing);

            }
        }

        if (itemcount == 0) {
            if (tablename.size() > 0) {
                new AsyncItemMaster().execute(0 + 1 + "", tablename.get(0).getTableId() + "", "0", billmode, billing);
            } else {
                new AsyncItemMaster().execute(0 + 1 + "", "0" + "", "0", billmode, billing);
            }
        }
        if (offerCount == 0 && !storeId.equals("")) {
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("BillMode", billmode);
            editor.putString("Bill", billing);
            editor.apply();
            new MasterItemModifier().execute("3", storeId);
        } else {
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("BillMode", billmode);
            editor.putString("Bill", billing);
            editor.apply();

            Intent i = new Intent(Dashboard_NSCI.this, Accountlist.class);
            i.putExtra("tableNo", 0 + 1);
            i.putExtra("tableId", tablename.get(0).getTableId() + "");
            i.putExtra("from", TakeOrder.class.getName());
            startActivity(i);
            finish();
        }

    }

    protected class AsyncStewardMaster extends AsyncTask<String, Void, Void> {
        boolean itemgroupbool = false;

        String itemCategoryId = sharedpreferences.getString("itemCategoryId", "");
        String storeid = sharedpreferences.getString("storeId", "");
        ProgressDialog pd;

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard_NSCI.this);
            try {
                String gid = "0";
                pid = "0";
                int _waiterCount = db.getWaiterCount();
                //-------waiter-----------------------

                if (_waiterCount == 0) {
                    JSONObject jsonObject = api.cms_getSteward(userId,posId);
                    JSONArray jsonArray = jsonObject.getJSONArray("Value");
                    waiterlist = new ArrayList<WaiterDetails>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        waiterDetails = new WaiterDetails();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        pid = jsonObject1.get("personid").toString();
                        pcode = jsonObject1.get("personcode").toString();
                        pname = jsonObject1.get("FirstName").toString();
                        waiterDetails.setpersonid(pid);
                        waiterDetails.setpersoncode(pcode);
                        waiterDetails.setFirstName(pname);
                        waiterlist.add(waiterDetails);

                    }
                    if (waiterlist != null) {
                        //  db.deleteItemGroup();
                        Long mm = db.insertWaiterGroup(waiterlist);

                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_NSCI.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            pd.dismiss();

        }
    }


    protected class AsyncMaster extends AsyncTask<String, Void, Void> {
        ArrayList<Item> itemList = null;
        Item item = null;
        boolean itembool = false;
        boolean itemgroupbool = false;
        int position = 0;
        int tableId;
        String billMode = "";
        String bill = "";
        String itemCategoryId = sharedpreferences.getString("itemCategoryId", "");
        String storeid = sharedpreferences.getString("storeId", "");
        ProgressDialog pd;

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard_NSCI.this);
            try {
                String gid = "0";
                pid = "0";
                position = Integer.parseInt(params[0]);
                tableId = Integer.parseInt(params[1]);
                billMode = params[2];
                bill = params[3];
                int _itemGroupCount = db.getItemGroupCount(posId);
                int _waiterCount = db.getWaiterCount();
                int _itemcount = db.getItemCount(posId);
                //-------waiter-----------------------

                if (_waiterCount == 0) {
                    JSONObject jsonObject = api.cms_getSteward(userId,posId);
                    JSONArray jsonArray = jsonObject.getJSONArray("Value");
                    waiterlist = new ArrayList<WaiterDetails>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        waiterDetails = new WaiterDetails();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        pid = jsonObject1.get("personid").toString();
                        pcode = jsonObject1.get("personcode").toString();
                        pname = jsonObject1.get("FirstName").toString();
                        waiterDetails.setpersonid(pid);
                        waiterDetails.setpersoncode(pcode);
                        waiterDetails.setFirstName(pname);
                        waiterlist.add(waiterDetails);

                    }
                    if (waiterlist != null) {
                        //  db.deleteItemGroup();
                        Long mm = db.insertWaiterGroup(waiterlist);
                        if (mm > 0) {
                            itemgroupbool = true;
                        }

                    }
                }

                //item group

                if (_itemGroupCount == 0) {

                    //JSONObject jsonObjectg=api.cms_getItemGroup(itemCategoryId); nsci
                    JSONObject jsonObjectg = api.cms_getItemGroup(storeid);
                    JSONArray jsonArrayg = jsonObjectg.getJSONArray("Value");
                    itemGrouplist = new ArrayList<ItemGroup>();
                    for (int i = 0; i < jsonArrayg.length(); i++) {
                        itemGroup = new ItemGroup();
                        JSONObject jsonObject1 = jsonArrayg.getJSONObject(i);
                        gid = jsonObject1.get("ID").toString();
                        String gcode = jsonObject1.get("GroupCode").toString();
                        String gname = jsonObject1.get("GroupName").toString();
                        itemGroup.setGroupid(gid);
                        itemGroup.setGroupcode(gcode);
                        itemGroup.setGroupname(gname);
                        itemGrouplist.add(itemGroup);

                    }
                    if (itemGrouplist != null) {
                        db.deleteItemGroup(posId);
                        Long mm = db.insertItemGroup(itemGrouplist, posId);
                        if (mm > 0) {
                            itemgroupbool = true;
                        }

                    }
                }


                if (_itemcount == 0) {
                    JSONObject jsonObject1 = api.cms_getItemlist(itemCategoryId, posId);
                    JSONArray jsonArray1 = jsonObject1.getJSONArray("Value");
                    itemList = new ArrayList<Item>();
                    for (int i = 0; i < jsonArray1.length(); i++) {
                        item = new Item();
                        JSONObject jsonObjectchild = jsonArray1.getJSONObject(i);
                        if (jsonObjectchild.has("ItemGroupID")) {
                            String ItemCategoryID = jsonObjectchild.optString("ItemCategoryID");
                            String ItemCode = jsonObjectchild.optString("ItemCode");
                            // String ItemIdentityID = jsonObjectchild.optString("ItemIdentityID");
                            String ItemIdentityID = jsonObjectchild.optString("ID");
                            String ItemGroupID = jsonObjectchild.optString("ItemGroupID");
                            String ItemName = jsonObjectchild.optString("ItemName");
                            String StoreID = jsonObjectchild.optString("StoreID");
                            String TaxID = jsonObjectchild.optString("TaxID");
                            String Tax = jsonObjectchild.optString("Tax");
                            String unitId = jsonObjectchild.optString("UnitID");
                            String Rate = jsonObjectchild.optString("Rate");
                            String description = jsonObjectchild.optString("description");
                            String itemImage = jsonObjectchild.optString("ItemImage");


                            item.setItemCategoryID(ItemCategoryID);
                            item.setItemcode(ItemCode);
                            item.setItemIdentityID(ItemIdentityID);
                            item.setItemGroupID(ItemGroupID);
                            item.setItemName(ItemName);
                            item.setStoreID(StoreID);
                            item.setTaxID(TaxID);
                            item.setTax(Tax);
                            item.setUnitID(unitId);
                            item.setRate(Rate);
                            item.setDescription(description);
                            item.setItemImage(itemImage);
                            itemList.add(item);
                        } else {
                            int id = i;
                        }

                    }
                    if (itemList != null) {
                        //   db.deleteItem();
                        Long mm = db.insertItem(itemList);
                        if (mm > 0) {
                            itembool = true;
                        }
                    }
                } else {
                    itembool = true;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_NSCI.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            pd.dismiss();

            if (itembool) {

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("BillMode", billMode);
                editor.putString("Bill", bill);
                editor.apply();

                Intent i = new Intent(Dashboard_NSCI.this, Accountlist.class);

                i.putExtra("tableNo", position);
                i.putExtra("tableId", tableId);
                i.putExtra("from", TakeOrder.class.getName());
                //   i.putExtra("position",positionn);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(Dashboard_NSCI.this, "Error loading items or item group", Toast.LENGTH_LONG).show();
            }
        }
    }

    protected class AsyncMenuMaster extends AsyncTask<String, Void, Void> {

        ArrayList<Item> itemList = null;
        Item item = null;
        boolean itembool = false;
        boolean itemgroupbool = false;
        int position = 0;
        String itemCategoryId = sharedpreferences.getString("itemCategoryId", "");
        ProgressDialog pd;

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard_NSCI.this);
            try {


                String gid = "0";


                //item group
                String storeId = sharedpreferences.getString("storeId", "");
                JSONObject jsonObjectg = api.cms_getItemGroup(storeId);
                JSONArray jsonArrayg = jsonObjectg.getJSONArray("Value");
                itemGrouplist = new ArrayList<ItemGroup>();
                for (int i = 0; i < jsonArrayg.length(); i++) {
                    itemGroup = new ItemGroup();
                    JSONObject jsonObject1 = jsonArrayg.getJSONObject(i);
                    gid = jsonObject1.get("ID").toString();
                    String gcode = jsonObject1.get("GroupCode").toString();
                    String gname = jsonObject1.get("GroupName").toString();
                    itemGroup.setGroupid(gid);
                    itemGroup.setGroupcode(gcode);
                    itemGroup.setGroupname(gname);
                    itemGrouplist.add(itemGroup);

                }
                if (itemGrouplist != null) {
                    db.deleteItemGroup(posId);
                    Long mm = db.insertItemGroup(itemGrouplist, posId);
                    if (mm > 0) {
                        itemgroupbool = true;
                    }

                }


                //item list
                JSONObject jsonObject1 = api.cms_getItemlist(itemCategoryId, posId);
                JSONArray jsonArray1 = jsonObject1.getJSONArray("Value");
                itemList = new ArrayList<Item>();
                for (int i = 0; i < jsonArray1.length(); i++) {
                    item = new Item();
                    JSONObject jsonObjectchild = jsonArray1.getJSONObject(i);
                    if (jsonObjectchild.has("ItemGroupID")) {

                        String ItemCategoryID = jsonObjectchild.optString("ItemCategoryID");
                        String ItemCode = jsonObjectchild.optString("ItemCode");
                        //  String ItemIdentityID = jsonObjectchild.optString("ItemIdentityID");
                        String ItemIdentityID = jsonObjectchild.optString("ID");
                        String ItemGroupID = jsonObjectchild.optString("ItemGroupID");
                        String ItemName = jsonObjectchild.optString("ItemName");
                        String StoreID = jsonObjectchild.optString("StoreID");
                        String TaxID = jsonObjectchild.optString("TaxID");
                        String Tax = jsonObjectchild.optString("Tax");
                        String unitId = jsonObjectchild.optString("UnitID");
                        String Rate = jsonObjectchild.optString("Rate");
                        String description = jsonObjectchild.optString("description");
                        String itemImage = jsonObjectchild.optString("ItemImage");


                        item.setItemCategoryID(ItemCategoryID);
                        item.setItemcode(ItemCode);
                        item.setItemIdentityID(ItemIdentityID);
                        item.setItemGroupID(ItemGroupID);
                        item.setItemName(ItemName);
                        item.setStoreID(StoreID);
                        item.setTaxID(TaxID);
                        item.setTax(Tax);
                        item.setUnitID(unitId);
                        item.setRate(Rate);
                        item.setDescription(description);
                        item.setItemImage(itemImage);
                        itemList.add(item);
                    } else {
                        int id = i;
                    }

                }
                if (itemList != null) {
                    // db.deleteItem();
                    Long mm = db.insertItem(itemList);
                    if (mm > 0) {
                        itembool = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_NSCI.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (itembool == true) {
                Intent i1 = new Intent(Dashboard_NSCI.this, GroupView.class);
                startActivity(i1);
            } else {
                Toast.makeText(Dashboard_NSCI.this, "Error", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class MasterItemModifier extends AsyncTask<String, Void, Void> {
        //ArrayList<ItemModifier> itemmodifier;
        RestAPI api = new RestAPI(Dashboard_NSCI.this);
        String page = "";
        ProgressDialog pd;

        @Override
        protected Void doInBackground(String... strings) {
            JSONObject jsonObject = null;
            ItemModifier itemModifier = null;
            try {
                //itemmodifier=new ArrayList<ItemModifier>();
                page = strings[0];
                if (strings[0].equals("1")) {
                    jsonObject = api.cms_getModifier();
                    JSONArray jsonArray = jsonObject.getJSONArray("Value");
                    if (jsonArray != null) {
                        db = new Dbase(Dashboard_NSCI.this);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            //itemModifier=new ItemModifier();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String modifierid = jsonObject1.getString("ID");
                            String modifierName = jsonObject1.getString("Name");
                            db.insertModifier(modifierName, modifierid);
                            //itemModifier.setDetailname(modifierName);
                            //itemModifier.setId(modifierid);
                            //itemmodifier.add(itemModifier);
                        }
                    }
                } else if (strings[0].equals("2")) {
                    jsonObject = null;
                    jsonObject = api.cms_getModifierDetails();
                    JSONArray jsonArray = jsonObject.getJSONArray("Value");
                    if (jsonArray != null) {
                        db = new Dbase(Dashboard_NSCI.this);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            //itemModifier=new ItemModifier();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String modifierid = jsonObject1.getString("ID");
                            String modifierName = jsonObject1.getString("Name");
                            db.insertModifierDetails(modifierName, modifierid);
                            //itemModifier.setDetailname(modifierName);
                            //itemModifier.setId(modifierid);
                            //itemmodifier.add(itemModifier);
                        }
                    }
                } else if (strings[0].equals("3") || strings[0].equals("4")) {
                    jsonObject = null;
                    jsonObject = api.cms_getItemPromotion(Integer.parseInt(strings[1]));
                    JSONArray jsonArray = jsonObject.getJSONArray("Value");
                    if (jsonArray != null) {
                        db = new Dbase(Dashboard_NSCI.this);
                        db.clearPromotion();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            //itemModifier=new ItemModifier();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String PromotionId = jsonObject1.optString("PromotionId");
                            String PromotionName = jsonObject1.optString("PromotionName");
                            String ItemID = jsonObject1.optString("ItemID");
                            String Quantity = jsonObject1.optString("Quantity");
                            String Rate = jsonObject1.optString("Rate");
                            String FromDate = jsonObject1.optString("FromDate");
                            String ToDate = jsonObject1.optString("ToDate");
                            String CreatedDate = jsonObject1.optString("CreatedDate", "");
                            String ModifiedDate = jsonObject1.optString("ModifiedDate");
                            String OfferPromotionId = jsonObject1.optString("OfferPromotionId");
                            String FreeItemId = jsonObject1.optString("FreeItemId");
                            String FreeQty = jsonObject1.optString("FreeQty");
                            String FreeItemName = jsonObject1.optString("FreeItemName");
                            String FreeItemCode = jsonObject1.optString("FreeItemCode");
                            String ItemCode = jsonObject1.optString("ItemCode");
                            db.insertItemPromotion(PromotionId, PromotionName, ItemID, Quantity, Rate, FromDate, ToDate, CreatedDate, ModifiedDate, OfferPromotionId, FreeItemId, FreeQty, FreeItemName, FreeItemCode, ItemCode);
                            //itemModifier.setDetailname(modifierName);
                            //itemModifier.setId(modifierid);
                            //itemmodifier.add(itemModifier);
                        }
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_NSCI.this);
            pd.show();
            pd.setMessage("Please wait loading ....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                pd.dismiss();
                if (page.equals("3")) {
                    Intent i = new Intent(Dashboard_NSCI.this, Accountlist.class);
                    i.putExtra("tableNo", 0 + 1);
                    i.putExtra("tableId", 0 + "");
                    i.putExtra("from", TakeOrder.class.getName());
                    startActivity(i);
                    finish();
                }
                if (page.equals("4")) {
                    Toast.makeText(getApplicationContext(), "Item Promotion Refreshed", Toast.LENGTH_SHORT).show();
                }
            } catch (Throwable e) {
                e.printStackTrace();
                Toast.makeText(Dashboard_NSCI.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    protected class AsyncItemMaster extends AsyncTask<String, Void, Void> {
        ArrayList<Item> itemList = null;
        Item item = null;
        boolean itembool = false;
        boolean itemgroupbool = false;
        int position = 0;
        int tableId;
        String billMode = "";
        String bill = "";
        String itemCategoryId = sharedpreferences.getString("itemCategoryId", "");
        String storeId = sharedpreferences.getString("storeId", "");
        ProgressDialog pd;

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard_NSCI.this);
            try {
                position = Integer.parseInt(params[0]);
                tableId = Integer.parseInt(params[1]);
                String gid = "0";
                billMode = params[3];
                bill = params[4];
                int _itemGroupCount = db.getItemGroupCount(posId);
                if (_itemGroupCount == 0) {
                    JSONObject jsonObjectg = api.cms_getItemGroup(storeId);
                    JSONArray jsonArrayg = jsonObjectg.getJSONArray("Value");
                    itemGrouplist = new ArrayList<ItemGroup>();
                    for (int i = 0; i < jsonArrayg.length(); i++) {
                        itemGroup = new ItemGroup();
                        JSONObject jsonObject1 = jsonArrayg.getJSONObject(i);
                        gid = jsonObject1.get("ID").toString();
                        String gcode = jsonObject1.get("GroupCode").toString();
                        String gname = jsonObject1.get("GroupName").toString();
                        itemGroup.setGroupid(gid);
                        itemGroup.setGroupcode(gcode);
                        itemGroup.setGroupname(gname);
                        itemGrouplist.add(itemGroup);
                    }
                    if (itemGrouplist != null) {
                        db.deleteItemGroup(posId);
                        Long mm = db.insertItemGroup(itemGrouplist, posId);
                        if (mm > 0) {
                            itemgroupbool = true;
                        }

                    }
                }

                //item list
                JSONObject jsonObject1 = api.cms_getItemlist(itemCategoryId, posId);
                JSONArray jsonArray1 = jsonObject1.getJSONArray("Value");
                itemList = new ArrayList<Item>();
                for (int i = 0; i < jsonArray1.length(); i++) {
                    //String storeId = sharedpreferences.getString("storeId", "");
                    JSONObject jsonObjectchild = jsonArray1.getJSONObject(i);
                    if (jsonObjectchild.has("ItemGroupID")) {
                        item = new Item();
                        String ItemCategoryID = jsonObjectchild.optString("ItemCategoryID");
                        String ItemCode = jsonObjectchild.optString("ItemCode");
                        //  String ItemIdentityID = jsonObjectchild.optString("ItemIdentityID");
                        String ItemIdentityID = jsonObjectchild.optString("ID");
                        String ItemGroupID = jsonObjectchild.optString("ItemGroupID");
                        String ItemName = jsonObjectchild.optString("ItemName");
                        String StoreID = jsonObjectchild.optString("StoreID");
                        String TaxID = jsonObjectchild.optString("TaxID");
                        String Tax = jsonObjectchild.optString("Tax");
                        String unitId = jsonObjectchild.optString("UnitID");
                        String Rate = jsonObjectchild.optString("Rate");
                        String description = jsonObjectchild.optString("description");
                        String itemImage = jsonObjectchild.optString("ItemImage");


                        item.setItemCategoryID(ItemCategoryID);
                        item.setItemcode(ItemCode);
                        item.setItemIdentityID(ItemIdentityID);
                        item.setItemGroupID(ItemGroupID);
                        item.setItemName(ItemName);
                        item.setStoreID(StoreID);
                        item.setTaxID(TaxID);
                        item.setTax(Tax);
                        item.setUnitID(unitId);
                        item.setRate(Rate);
                        item.setDescription(description);
                        item.setItemImage(itemImage);
                        itemList.add(item);
                    } else {
                        int id = i;
                    }

                }
                if (itemList != null) {
                    // db.deleteItem();
                    Long mm = db.insertItem(itemList);
                    if (mm > 0) {
                        itembool = true;
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_NSCI.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);

            if (itembool == true) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("BillMode", billMode);
                editor.putString("Bill", bill);
                editor.apply();
                Intent i = new Intent(Dashboard_NSCI.this, Accountlist.class);
                i.putExtra("tableNo", position);
                i.putExtra("tableId", tableId);
                i.putExtra("from", TakeOrder.class.getName());
                startActivity(i);
                finish();
            } else {

                Toast.makeText(Dashboard_NSCI.this, "Error", Toast.LENGTH_LONG).show();
            }
            //pd.dismiss();

        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("" + StoreName);
            ab.setElevation(0);
        }
    }

    private void loadTable() {
        try {
            db.deleteTable();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        db = new Dbase(Dashboard_NSCI.this);
        cd = new ConnectionDetector(Dashboard_NSCI.this);
        tablecount = db.getTableCount();
        if (tablecount == 0) {
            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent || !isInternetPresent) {
                new AsyncTablename().execute();

            } else {
                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
            }

        }

    }

    protected class AsyncTablename extends AsyncTask<String, Void, ArrayList<Table>> {

        ArrayList<Table> tablename = null;
        String storeId = sharedpreferences.getString("storeId", "");
        ProgressDialog pd;

        @Override
        protected ArrayList<Table> doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard_NSCI.this);
            try {

                String[] narr_kitchen = new String[]{"Salt", "Sugar", "More Salt", "Less Salt", "More Sugar", "Less Sugar", "More Spice", "Medium Spice", "Less Spice"};
                //   String[] narr_bar=new String[]{"Salt","Sugar","More Salt","Less Salt","More Sugar","Less Sugar","More Spice","Medium Spice","Less Spice"};
                //  Bundle b=getIntent().getExtras();
                //   positionn=b.getInt("position");
                //  String storeId=b.getString("Storeid");
                JSONObject jsonObject = api.cms_getTablebystore(posId);//1150511156
                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                tablename = new ArrayList<>();
                Table table = null;
                for (int i = 0; i < jsonArray.length(); i++) {
                    table = new Table();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    String tableN = jsonObject1.get("TableNumber").toString();
                    String tableid = jsonObject1.get("TableId").toString();
                    table.setTableId(Integer.parseInt(tableid));
                    table.setTableNmae("" + tableN);
                    tablename.add(table);
                }


                for (int j = 0; j < narr_kitchen.length; j++) {
                    long i = db.insertNarration(narr_kitchen[j], "KOT");
                }

                if (tablename != null) {
//                    db.deleteTable();
                    db.insertTables(tablename);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return tablename;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_NSCI.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(final ArrayList<Table> s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s != null) {
                Toast.makeText(Dashboard_NSCI.this, "table loaded ", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("DefaultLocale")
    public void setPieChart(ArrayList<GraphDto> grapthArrayList) {
        try {
            pieChart = findViewById(R.id.piechart);
            // pieChart.setUsePercentValues(true);
            pieChart.getDescription().setEnabled(false);
            //pieChart.setDescription("");
            pieChart.setExtraOffsets(5, 10, 5, 5);
            pieChart.setDragDecelerationFrictionCoef(0.9f);
            pieChart.setTransparentCircleRadius(61f);
            pieChart.setHoleColor(Color.WHITE);
            // pieChart.setHoleRadius(80f);
            pieChart.animateY(1000, Easing.EasingOption.EaseInOutCubic);
            Double tot = 0.0;
            for (int i = 0; i < grapthArrayList.size(); i++) {
                tot = tot + Double.parseDouble(grapthArrayList.get(i).getTotalAmount());
            }
            DecimalFormat precision = new DecimalFormat("0.00");

            pieChart.setCenterText("Total \n" + precision.format(tot));
            pieChart.setDrawSliceText(false);

            ArrayList<PieEntry> yValues = new ArrayList<>();
            for (int i = 0; i < grapthArrayList.size(); i++) {
                yValues.add(new PieEntry(Float.parseFloat(grapthArrayList.get(i).getTotalAmount()), grapthArrayList.get(i).getSettlement()));
            }

            PieDataSet dataSet = new PieDataSet(yValues, "");
            dataSet.setSliceSpace(0f);
            dataSet.setSelectionShift(5f);
            dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
            PieData pieData = new PieData((dataSet));
            pieData.setValueTextSize(15f);
            pieData.setValueTextColor(Color.WHITE);
            Legend l = pieChart.getLegend();
            l.setTextSize(15f);
            l.setTextColor(Color.BLACK);
            pieChart.setData(pieData);
            //PieChart Ends Here
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void getData() {

        try {

            dashboardList.clear();
            DashboardDto dashboardDealer;

            dashboardDealer = new DashboardDto("1", "Billing", R.drawable.d_billing, R.color.d1, R.color.d11);
            dashboardList.add(dashboardDealer);
            if (isOT.toLowerCase().equals("true")) {
                dashboardDealer = new DashboardDto("2", "Running Orders", R.drawable.d_running_order, R.color.d3, R.color.d33);
                dashboardList.add(dashboardDealer);
                dashboardDealer = new DashboardDto("3", "Order Transfer", R.drawable.d_ot, R.color.d5, R.color.d55);
                dashboardList.add(dashboardDealer);
            }
            dashboardDealer = new DashboardDto("10", "Table Transfer", R.drawable.chair, R.color.d2, R.color.d22);
            dashboardList.add(dashboardDealer);
            dashboardDealer = new DashboardDto("4", "Promotions", R.drawable.d_promotions, R.color.d4, R.color.d44);
            dashboardList.add(dashboardDealer);
            if (isOT.toLowerCase().equals("true")) {
                dashboardDealer = new DashboardDto("5", "Pending Bills", R.drawable.d_pending, R.color.d6, R.color.d66);
                dashboardList.add(dashboardDealer);
            }
            dashboardDealer = new DashboardDto("6", "Billing List", R.drawable.d_billing_list, R.color.d10, R.color.d101);
            dashboardList.add(dashboardDealer);
            if (isParty.toLowerCase().equals("true")) {
                dashboardDealer = new DashboardDto("7", "Party Billing", R.drawable.d_party, R.color.d8, R.color.d88);
                dashboardList.add(dashboardDealer);
            }
            dashboardDealer = new DashboardDto("8", "Change POS", R.drawable.d_pos, R.color.d9, R.color.d99);
            dashboardList.add(dashboardDealer);
            dashboardDealer = new DashboardDto("9", "Menu Card", R.drawable.p_menu, R.color.d7, R.color.d77);
            dashboardList.add(dashboardDealer);


//            dashboardAdapter.notifyDataSetChanged();
//
//
//            dashboardAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener<DashboardNewAdapter.MyViewHolder, DashboardDto>() {
//                @Override
//                public void onRecyclerViewItemClick(@NonNull DashboardNewAdapter.MyViewHolder myViewHolder, @NonNull View view, @NonNull DashboardDto dashboardDto, int position) {
//                    onClicks(dashboardDto.getId());
//                }
//            });

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private class AsyncPrinterTest extends AsyncTask<String, Void, Void> {
        String status = "";
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_NSCI.this);
            pd.show();
            pd.setMessage("Please wait loading....");

        }

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard_NSCI.this);
            try {

                JSONObject jsonObject1 = api.printTest(params[0], params[1], params[2]);
                // JSONArray jsonArray1=jsonObject1.getJSONArray("Value");
                status = jsonObject1.toString();

            } catch (Exception e) {
                String r = e.getMessage().toString();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            if (status.contains("true")) {
                Toast.makeText(getApplication(), "Printer connected ", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplication(), "Printer  not  connected ", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void Refresh() {
        new AlertDialog.Builder(Dashboard_NSCI.this)
                .setTitle("Refresh POS")
                .setMessage("Do you want to Refresh?")
                .setPositiveButton("Refresh",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                int count = db.getAccountCount(posId);
                                if (count > 0) {
                                    Toast.makeText(getApplicationContext(), "Please close the Member order in POS", Toast.LENGTH_LONG).show();
                                } else {
                                    try {
                                        db.deleteTable();
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                    }
                                    db.deleteItem(posId);
                                    db.deleteItemGroup(posId);

                                    Intent back = new Intent(Dashboard_NSCI.this, MainActivity.class);
                                    back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(back);
                                    finish();
                                }

                            }
                        })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    private void StewardRefresh() {
        new AlertDialog.Builder(Dashboard_NSCI.this)
                .setTitle("Steward Refresh")
                //  .setMessage("Do you want to Logout?")
                .setMessage("Do you want to Refresh?")
                //  .setIcon(R.drawable.ninja)
                .setPositiveButton("Steward Refresh",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                int count = db.getAccountCount(posId);
                                if (count > 0) {
                                    Toast.makeText(getApplicationContext(), "Please close the Member order in POS", Toast.LENGTH_LONG).show();
                                } else {
                                    db.deleteSteward();
                                    new AsyncStewardMaster().execute();

                                }

                            }
                        })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        Logout();
    }

    private void Logout() {
        new AlertDialog.Builder(Dashboard_NSCI.this)
                .setMessage("Do you want to Logout?")
                .setPositiveButton("Logout",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                Intent back = new Intent(Dashboard_NSCI.this, MainActivity.class);
                                back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(back);
                                finish();
                            }
                        })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menu_settings:

                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    db.deleteStore();
                    Intent i = new Intent(Dashboard_NSCI.this, StoreList.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();

                } else {
                    Intent i = new Intent(Dashboard_NSCI.this, StoreList.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }

                return true;

            case R.id.menu_printer:

                new Dashboard_NSCI.AsyncPrinterTest().execute(posId, "", serialNo);
                return true;

            case R.id.menu_refresh:
                Refresh();
                return true;
            case R.id.menu_refresh_steward:
                StewardRefresh();
                return true;

            case R.id.menu_refresh_promotion:
                PromotionRefresh();
                return true;

            case R.id.menu_refresh_item:
                itemRefresh();
                return true;
            case R.id.menu_refresh_tax:
                taxRefresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void taxRefresh() {
//        new Dashboard_NSCI.MasterItemModifier().execute("4", storeId);
        new Itemtax().execute();
    }

    public class GetData extends AsyncTask<Void, JSONObject, ArrayList<GraphDto>> {
        RestAPI api = new RestAPI(Dashboard_NSCI.this);
        String stauts = "";
        String error = "";
        ProgressDialog pd;

        @Override
        protected ArrayList<GraphDto> doInBackground(Void... params) {
            try {
                JSONObject jsonObj = api.CmsGetDashboardData("Monthly", posId);
                JSONObject jsonObject = jsonObj.getJSONObject("Value");
                JSONArray jsonArray = jsonObject.getJSONArray("Table");
                try {
                    Gson gson = new Gson();
                    grapthDtoArrayList = gson.fromJson(String.valueOf(jsonArray), new TypeToken<List<GraphDto>>() {
                    }.getType());

                } catch (Throwable e) {
                    e.getMessage();
                    error = e.getMessage();
                }
            } catch (Throwable e) {
                error = e.getMessage();

            }
            return grapthDtoArrayList;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_NSCI.this);
            pd.setMessage("Loading Data...");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.show();
            pd.setCancelable(true);
        }

        @Override
        protected void onPostExecute(ArrayList<GraphDto> result) {
            //super.onPostExecute(result);
            //Dashboard.session=1;
            if (stauts.equals("No internet connection")) {
                Toast.makeText(getApplicationContext(), "No internet connection-Dealer not loaded please referesh ", Toast.LENGTH_LONG).show();
                // pd.dismiss();
                if (result != null) {
                    Toast.makeText(getApplicationContext(), "Data not Loaded", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            } else if (!error.equals("")) {
                Toast.makeText(Dashboard_NSCI.this, "failed -- >" + error, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else {
                if (result.size() != 0) {
                    setPieChart(result);

//                    BarChart(result);
                } else {
                    Toast.makeText(Dashboard_NSCI.this, "Data not found", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }
            new AsyncStewardMaster().execute();


        }
    }


    private void PromotionRefresh() {
        String storeId = sharedpreferences.getString("storeId", "");
        new Dashboard_NSCI.MasterItemModifier().execute("4", storeId);
    }

    private void itemRefresh() {
        String storeId = sharedpreferences.getString("storeId", "");
        new AsyncItemMaster().execute(0 + 1 + "", "0", "0", "RR", "Regular");

    }


    public class Itemtax extends AsyncTask<Void, String, String> {

        RestAPI api = null;
        Dbase dbcontext = null;
        ProgressDialog pd = null;
        ArrayList<StoreClass> arrayList;
        int i = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard_NSCI.this);
            pd.show();
            pd.setMessage("Tax loading....");
            pd.setCancelable(false);
        }

        @Override
        protected String doInBackground(Void... voids) {

            api = new RestAPI(Dashboard_NSCI.this);
            dbcontext = new Dbase(Dashboard_NSCI.this);
            try {
                i = addItemsTax();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return i + "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (Integer.parseInt(s) > 0) {
                Toast.makeText(Dashboard_NSCI.this, "Tax details loaded", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(Dashboard_NSCI.this, "failed to load tax details", Toast.LENGTH_SHORT).show();

            }
        }

        private int addItemsTax() {
            long _iid = -1;
            JSONObject jsonObject = null;
            try {
                pd.setProgress(5);

                try {
                    jsonObject = api.getItemsTAX();
                    JSONArray jsonArray = jsonObject.getJSONArray("Value");
                    int length = getLengeth(jsonArray, dbcontext, "ITEMS_TAX");
                    arrayList = new ArrayList<>();
                    for (int i = 0; i < length; i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String ItemID = jsonObject1.optString("ItemID");
                        String TaxID = jsonObject1.optString("TaxID");
                        String TaxCode = jsonObject1.optString("TaxCode");
                        String Description = jsonObject1.optString("Description");
                        String Value = jsonObject1.optString("Value");
                        String Type = jsonObject1.optString("Type");
                        String CreatedDate = jsonObject1.optString("CreatedDate");
                        String Modifieddate = jsonObject1.optString("Modifieddate");
                        String IsTaxApplicable = jsonObject1.optString("IsTaxApplicable");
                        String issubtax = jsonObject1.optString("issubtax");
                        String Parentid = jsonObject1.optString("Parentid");
                        String taxtype = jsonObject1.optString("TaxType");

                        _iid = dbcontext.InsertItemtax(ItemID, TaxID, TaxCode, Description, Value, Type, CreatedDate, Modifieddate, IsTaxApplicable, issubtax, Parentid,taxtype);
                        pd.setProgress(i);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return (int) _iid;
        }


    }

    public static int getLengeth(JSONArray jsonArray, Dbase bmsHelper, String tableName) {
        int l = 0;
        if (jsonArray != null) {
            l = jsonArray.length();
            if (l > 0) {
                bmsHelper.clearTable(tableName);
            }
        }

        return l;
    }
}
