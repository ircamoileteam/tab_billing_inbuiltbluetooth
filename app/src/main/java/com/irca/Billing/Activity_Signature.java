package com.irca.Billing;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.irca.cosmo.MultiPrintView;
import com.irca.cosmo.R;
import com.irca.cosmo.RestAPI;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class Activity_Signature extends Activity {
    //signature
    LinearLayout mContent;
    View mView;
    signature mSignature;
    private Bitmap mBitmap;
    Button mGetSign;
    Button cancel;
    File mypath;
    Bundle bundle;
    byte[] mybyte = null;
    String memberid = "", base64String = "";
    Bitmap bitmap = null;
    ProgressDialog pd;
    int page;
    String memName = "";
    String memAccNo = "";
    String signimg = "";

    String waiternamecode;
    String cardType = "";
    String ava_balance = "", _isdebitcard = "";
    ;
    String billid = "";
    int tableNo = 0, tableId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__signature);
        mContent = (LinearLayout) findViewById(R.id.linearLayout);
        mView = mContent;
        mSignature = new signature(this, null);
        mSignature.setBackgroundResource(R.drawable.border);
        mContent.addView(mSignature, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
        mGetSign = (Button) findViewById(R.id.getsign);
        cancel = (Button) findViewById(R.id.cancel);
        mGetSign.setEnabled(false);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            billid = bundle.getString("billid");
            memberid = bundle.getString("accno");
            page = bundle.getInt("pageValue");
            memName = bundle.getString("creditName");
            memAccNo = bundle.getString("creditAno");
            tableNo = bundle.getInt("tableNo");
            tableId = bundle.getInt("tableId");
            waiternamecode = bundle.getString("waiternamecode");
            _isdebitcard = bundle.getString("isdebitcard");
            cardType = bundle.getString("cardType");//CASH CARD
            signimg = bundle.getString("signimg");//CASH CARD
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignature.clear();
                mGetSign.setEnabled(false);
                finish();
            }
        });


        mGetSign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                android.util.Log.v("log_tag", "Panel Saved");
                boolean error = captureSignature();
                if (!error) {
                    mView.setDrawingCacheEnabled(true);
                    mSignature.save(mView);
                    ByteArrayOutputStream byaos = new ByteArrayOutputStream();
                    mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byaos);
                    byte[] byt = byaos.toByteArray();
                    mybyte = byaos.toByteArray();
                    base64String = Base64.encodeToString(byt, Base64.DEFAULT);


                    if (!base64String.equalsIgnoreCase("")) {
                        new AsyncSavesign().execute(billid, memberid, base64String);

                    } else {

                        Toast.makeText(Activity_Signature.this, "Please Sign", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });







/*
        mGetSign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                android.util.Log.v("log_tag", "Panel Saved");
                boolean error = captureSignature();
                if (!error) {
                    mView.setDrawingCacheEnabled(true);
                    bitmap=  mSignature.save(mView);

                    if (bitmap.getWidth() > bitmap.getHeight()) {
                        bitmap = Bitmap.createScaledBitmap(bitmap, 640, 480, true);
                    } else {
                        bitmap = Bitmap.createScaledBitmap(bitmap, 480, 640, true);
                    }
                    ByteArrayOutputStream byaos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 25, byaos);
                    byte[] byt = byaos.toByteArray();
                    mybyte = byaos.toByteArray();
                    base64String = Base64.encodeToString(byt, Base64.DEFAULT);


if (!base64String.equalsIgnoreCase("")){
    new AsyncSavesign().execute(billid,memberid,base64String);
    finish();
}
else {

    Toast.makeText(Activity_Signature.this, "Please Sign", Toast.LENGTH_SHORT).show();
}




                }
            }
        });*/
    }

    protected class AsyncSavesign extends AsyncTask<String, Void, String> {

        String exp = "";

        @Override
        protected String doInBackground(String... params) {
            RestAPI restAPI = new RestAPI(Activity_Signature.this);
            try {
                JSONObject jsonObject1 = restAPI.cms_postSign(params[0], params[2], params[1]);
                exp = jsonObject1.optString("Value");

            } catch (Exception e) {
                e.printStackTrace();
                exp = e.toString();
            }


            return exp;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            if ((pd != null) && pd.isShowing()) {
                pd.dismiss();
            }
            try {
                // checking valid integer using parseInt() method
                Integer.parseInt(aVoid);
                //     Toast.makeText(Activity_Signature.this, aVoid+"", Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(Activity_Signature.this, MultiPrintView.class);
                intent.putExtra("billid", billid);
                intent.putExtra("accno", memberid);
                intent.putExtra("pageValue", 0);
                intent.putExtra("isdebitcard", _isdebitcard);
                intent.putExtra("creditAno", memAccNo);
                intent.putExtra("creditName", memName);
                intent.putExtra("tableNo", tableNo);
                intent.putExtra("tableId", tableId);
                //  i.putExtra("waiterid", waiterid);
                intent.putExtra("cardType", cardType);
                intent.putExtra("waiternamecode", waiternamecode);
                intent.putExtra("signimg", "signimg");
//                Bundle extras = new Bundle();
//                extras.putParcelable("Bitmap", mBitmap);
//                intent.putExtras(extras);
                startActivity(intent);
                finish();


            } catch (Throwable e) {
                Toast.makeText(Activity_Signature.this, aVoid + "", Toast.LENGTH_SHORT).show();

            }
//            pd.dismiss();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Activity_Signature.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }
    }

    private boolean captureSignature() {

        boolean error = false;
        String errorMessage = "";


        if (error) {
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 105, 50);
            toast.show();
        }

        return error;
    }

    public class signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public void save(View v) {
            View u = mContent;
            LinearLayout z = mContent;
            //  HorizontalScrollView z = (HorizontalScrollView) ((Activity) this).findViewById(R.id.hscr);
            int totalHeight = mContent.getHeight();
            int totalWidth = mContent.getWidth();

            // Bitmap b = getBitmapFromView(u, totalHeight, totalWidth);

            android.util.Log.v("log_tag", "Width: " + v.getWidth());
            android.util.Log.v("log_tag", "Height: " + v.getHeight());
            if (mBitmap == null) {
                //   mBitmap =  Bitmap.createBitmap (mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
                mBitmap = getBitmapFromView(u, totalHeight, totalWidth);


            }
            Canvas canvas = new Canvas(mBitmap);
            try {
                FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                v.draw(canvas);


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);


                byte[] byteArray = stream.toByteArray();
                mBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                mBitmap = getResizedBitmap(mBitmap, 350);
                // mBitmap= Bitmap.createScaledBitmap(mBitmap, 685, totalHeight, false);
                //    mBitmap= getResizedBitmap(mBitmap,685,totalHeight);
                mFileOutStream.flush();
                mFileOutStream.close();
                //   String url = MediaStore.Images.Media.insertImage(getContentResolver(), mBitmap, "title", null);
                MediaStore.Images.Media.insertImage(getContentResolver(), mBitmap, "Screen", "screen");
                //    android.util.Log.v("log_tag", "url: " + url);
                //In case you want to delete the file
                //boolean deleted = mypath.delete();
                //Log.v("log_tag","deleted: " + mypath.toString() + deleted);
                //If you want to convert the image to string use base64 converter

            } catch (Exception e) {
                android.util.Log.v("log_tag", e.toString());
            }
        }



        /*    public void save(View v) {
            View u = mContent;
            LinearLayout z = mContent;
            //  HorizontalScrollView z = (HorizontalScrollView) ((Activity) this).findViewById(R.id.hscr);
            int totalHeight = mContent.getHeight();
            int totalWidth = mContent.getWidth();

            // Bitmap b = getBitmapFromView(u, totalHeight, totalWidth);

            android.util.Log.v("log_tag", "Width: " + v.getWidth());
            android.util.Log.v("log_tag", "Height: " + v.getHeight());
            if (mBitmap == null) {
                //   mBitmap =  Bitmap.createBitmap (mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
                mBitmap = getBitmapFromView(u, totalHeight, totalWidth);


            }
            Canvas canvas = new Canvas(mBitmap);
            try {
                FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                v.draw(canvas);


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);


                byte[] byteArray = stream.toByteArray();
                mBitmap = BitmapFactory.decodeByteArray(byteArray,0,byteArray.length);
                mBitmap = getResizedBitmap(mBitmap, 350);
                // mBitmap= Bitmap.createScaledBitmap(mBitmap, 685, totalHeight, false);
                //    mBitmap= getResizedBitmap(mBitmap,685,totalHeight);
                mFileOutStream.flush();
                mFileOutStream.close();
                //   String url = MediaStore.Images.Media.insertImage(getContentResolver(), mBitmap, "title", null);
                MediaStore.Images.Media.insertImage(getContentResolver(), mBitmap, "Screen", "screen");
                //    android.util.Log.v("log_tag", "url: " + url);
                //In case you want to delete the file
                //boolean deleted = mypath.delete();
                //Log.v("log_tag","deleted: " + mypath.toString() + deleted);
                //If you want to convert the image to string use base64 converter

            }
            catch (Exception e) {
                android.util.Log.v("log_tag", e.toString());
            }
        }*/

        public void clear() {
            path.reset();
            invalidate();
        }

        public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
            int width = image.getWidth();
            int height = image.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }
            return Bitmap.createScaledBitmap(image, width, height, true);
        }/*
        public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
            int width = bm.getWidth();
            int height = bm.getHeight();
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;
            // CREATE A MATRIX FOR THE MANIPULATION
            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight);

            // "RECREATE" THE NEW BITMAP
            Bitmap resizedBitmap = Bitmap.createBitmap(
                    bm, 0, 0, width, height, matrix, false);
            bm.recycle();
            return resizedBitmap;
        }*/

        public Bitmap getBitmapFromView(View view, int totalHeight, int totalWidth) {
      /*  BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = false;
        opts.inPreferredConfig = Bitmap.Config.RGB_565;
        opts.inDither = true;*/


            Bitmap returnedBitmap = Bitmap.createBitmap((totalWidth), (totalHeight), Bitmap.Config.RGB_565);

            int heightofBitMap = returnedBitmap.getHeight();

            int widthofBitMap = returnedBitmap.getWidth();

            //       Bitmap returnedBitmap1 = Bitmap.createScaledBitmap(returnedBitmap, totalWidth,200,true);
            float ratio = Math.min(
                    (float) 1000 / returnedBitmap.getWidth(),
                    (float) returnedBitmap.getHeight() / returnedBitmap.getHeight());
            int width = Math.round((float) ratio * returnedBitmap.getWidth());
            int height = Math.round((float) ratio * returnedBitmap.getHeight());
            returnedBitmap = Bitmap.createScaledBitmap(returnedBitmap, width,
                    height, true);
            Canvas canvas = new Canvas(returnedBitmap);
            Drawable bgDrawable = view.getBackground();
            if (bgDrawable != null)
                bgDrawable.draw(canvas);
            else
                canvas.drawColor(Color.WHITE);
            view.draw(canvas);
            return returnedBitmap;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);
            mGetSign.setText("Save Signature");
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }


}
