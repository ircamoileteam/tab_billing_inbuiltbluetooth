package com.irca.Billing;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irca.cosmo.R;

/**
 * Created by Archanna on 9/23/2016.
 */
public class CustomGridAdapter extends BaseAdapter {

    private Context myContext1;
    private String[] nameId;
    private int [] imageId;
    private int [] background_colour;
    private int [] background_colour2;
    private LayoutInflater mInflater;

    Typeface tf;
    public CustomGridAdapter(Context myContext, String[] prgmNameList, int[] prgmImages,int[] color,int[] color2) {

        nameId=prgmNameList;
        myContext1=myContext;
        imageId=prgmImages;
        background_colour=color;
        background_colour2=color2;
        mInflater = LayoutInflater.from(myContext);

    }

    @Override
    public int getCount() {
        return imageId.length;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        try {
            view = mInflater.inflate(R.layout.dashboard_item_layout, null);
            tf = Typeface.createFromAsset(myContext1.getAssets(), "font/Kabel Book BT_0.ttf");
            final ImageView moduleicon = (ImageView) view.findViewById(R.id.imageView);
            final TextView modulename = (TextView) view.findViewById(R.id.textView);
            final LinearLayout background =(LinearLayout)view.findViewById(R.id.all);
            final LinearLayout background2 =(LinearLayout)view.findViewById(R.id.inner);
            modulename.setText(nameId[position]);
            modulename.setTypeface(tf,Typeface.BOLD);
            moduleicon.setImageResource(imageId[position]);
            background.setBackgroundResource(background_colour[position]);
            background2.setBackgroundResource(background_colour2[position]);
        }catch (Exception e) {
            e.printStackTrace();

        }
        return view;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

}