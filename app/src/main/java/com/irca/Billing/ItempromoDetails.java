package com.irca.Billing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

//import com.crashlytics.android.Crashlytics;
import com.irca.cosmo.R;
import com.irca.db.Dbase;
import com.irca.fields.ItemPromo;

import java.util.ArrayList;

//import io.fabric.sdk.android.Fabric;

public class ItempromoDetails extends AppCompatActivity {
    Dbase dbase;   TextInputEditText promotionName;TextView fromdate,todate;TextView fromtime,totime;
    AutoCompleteTextView e_buyitem,e_posname,e_getItem; EditText e_buyqty,e_getqty;  LinearLayout l_addItem;
    SharedPreferences sharedpreferences;
    String posname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_itempromo_details);
        promotionName=(TextInputEditText)findViewById(R.id.promotionName);
        fromdate=(TextView)findViewById(R.id.t_fromDate);
        todate=(TextView)findViewById(R.id.t_toDate);
        fromtime=(TextView)findViewById(R.id.t_startTime);
        l_addItem=(LinearLayout)findViewById(R.id.dy_add);
        e_buyitem=(AutoCompleteTextView)findViewById(R.id.e_buyitem);
        e_posname=(AutoCompleteTextView)findViewById(R.id.e_posname);
        e_getItem=(AutoCompleteTextView)findViewById(R.id.getItemName);
        e_buyqty=(EditText)findViewById(R.id.e_buyqty);
        e_getqty=(EditText)findViewById(R.id.e_getQuantity);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        posname = sharedpreferences.getString("StoreName", "");
        e_posname.setText(posname);
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        totime=(TextView)findViewById(R.id.t_endTime);
        dbase=new Dbase(ItempromoDetails.this);
        Bundle bundle=getIntent().getExtras();
        String promoid=bundle.getString("Promoid");
        ArrayList<ItemPromo> arrayList=dbase.getItemPromotiondetails(promoid);
        for (int i=0;i<arrayList.size();i++){
            String itemid=arrayList.get(i).getItemID();
            String Promoname=arrayList.get(i).getPromotionName();
            String BuyQty=arrayList.get(i).getQuantity();
            String itemname=  dbase.getItemname(itemid);
            promotionName.setText(Promoname);
            String Sfromdate=arrayList.get(i).getFromDate();
            String Stodate=arrayList.get(i).getToDate();
            String[] fdateary=Sfromdate.split(" ");
            String[] todateary=Stodate.split(" ");
            fromdate.setText(fdateary[0]);
            try{

                fromtime.setText(fdateary[1]+ " "+fdateary[2]);
                totime.setText(todateary[1]+ " "+todateary[2]);
            }
            catch (Exception e){

                fromtime.setText(fdateary[1]);
                totime.setText(todateary[1]);
            }
            todate.setText(todateary[0]);
            e_buyitem.setText(itemname);
            e_buyqty.setText(BuyQty);
            LayoutInflater inflater=(LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View add_newview=inflater.inflate(R.layout.add_extraitemrow,null);
            final AutoCompleteTextView item=(AutoCompleteTextView) add_newview.findViewById(R.id.getItemName);
            final EditText quantity =(EditText)add_newview.findViewById(R.id.e_getQuantity);
         //   final ImageView cimage =(ImageView)add_newview.findViewById(R.id.img_cancel1);
            item.setText(arrayList.get(i).getFreeItemCode()+"-"+arrayList.get(i).getFreeItemName());
            quantity.setText(arrayList.get(i).getFreeQty());
        //    cimage.setVisibility(View.GONE);
            l_addItem.addView(add_newview);
        }

    }
}
