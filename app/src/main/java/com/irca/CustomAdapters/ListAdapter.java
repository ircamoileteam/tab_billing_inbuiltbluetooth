package com.irca.CustomAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.irca.cosmo.R;
import com.irca.fields.MasterStore;

import java.util.ArrayList;

/**
 * Created by Manoch Richard on 10/22/2015.
 */
public class ListAdapter extends BaseAdapter {

    ArrayList<MasterStore> masterStore;
    Context context;
    public ListAdapter(Context c,ArrayList<MasterStore> masterStoreArrayAdapter)
    {
        this.masterStore=masterStoreArrayAdapter;
        context=c;
    }
    @Override
    public int getCount()
    {
        return masterStore.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_storelist1, parent,false);

        }


            TextView storeName = (TextView)convertView.findViewById(R.id.storeText);
            storeName.setText(masterStore.get(position).getMobilestorename());



        return convertView;
    }
}
