package com.irca.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.irca.dto.ModifiersDto;
import com.irca.dto.SendModifiers;
import com.irca.fields.CloseorderItems;
import com.irca.fields.Item;
import com.irca.fields.ItemGroup;
import com.irca.fields.ItemPromo;
import com.irca.fields.Itemadapter;
import com.irca.fields.MasterStore;
import com.irca.fields.Table;
import com.irca.fields.Card;
import com.irca.fields.WaiterDetails;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

/**
 * Created by Manoch Richard on 10/22/2015.
 */
public class Dbase extends SQLiteOpenHelper {
    private final static int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "KBOT";

    // TABLES
    private static final String TABLE_STORE = "mstores";
    private static final String ID = "sId";
    private static final String CODE = "sCode";
    private static final String NAME = "sName";
    private static final String ICID = "icategoryid";
    private static final String TYPE = "type";
    private static final String CASH = "cash";
    private static final String CREDIT = "credit";
    private static final String ACCOUNT = "account";
    private static final String PARTY = "party";
    private static final String PAGE = "page";
    private static final String ISOT = "isOT";
    private static final String MID = "msid";
    private static final String MSNAME = "msname";
    private static final String ISREADER = "isreader";
    private static final String ISGUEST = "isguest";
    private static final String ADDRESS = "address";
    private static final String GST = "gst";
    private static final String decimal = "decimal";


    private static final String TABLE_TABLENAME = "mtable";
    private static final String TID = "tableid";
    private static final String TABLENUMBER = "tableNumber";


    private static final String TABLE_ITEMGROUP = "mitemgroup";
    private static final String GID = "gid";
    private static final String GCODE = "gcode";
    private static final String GNAME = "gname";
    private static final String PoID = "posid";


    private static final String TABLE_WAITER = "waiterlist";
    private static final String PID = "pid";
    private static final String PCODE = "pcode";
    private static final String PNAME = "pname";


    private static final String TABLE_ITEM = "mitem";
    private static final String ICATEGORYID = "itemcategoryid";
    private static final String ICODE = "icode";
    private static final String IIDENTITYID = "itemid";
    private static final String DESCRIPTION = "description";
    private static final String IGID = "itemgid";
    private static final String INAME = "iname";
    private static final String SID = "itemstoreid";
    private static final String TAX = "tax";
    private static final String UNITID = "unitId";
    private static final String TaxID = "TaxID";
    private static final String RATE = "rate";
    private static final String ITEMIMAGE = "image";
    private static final String ITEMSTOCK = "itemstock";

    private static final String TABLE_ACCOUNT = "maccount";
    private static final String LID = "lid";
    private static final String POSID = "posid";
    private static final String ACCNUM = "accountnum";
    private static final String ACC_NAME = "account_name";
    private static final String TODAYSDATE = "todaysdate";
    private static final String CARDTYPE = "cardtype";
    private static final String OPENING_BALANCE = "opBalance";
    private static final String DEBIT_BALANCE = "debitBalance";
    private static final String PaCODE = "pcode";
    private static final String PaNAME = "pname";
    private static final String PaID = "paid";
    private static final String Pcodename = "pcn";//for display purpose
    private static final String BILLMODE = "bill_mode";//for billmode
    private static final String BILLMODE_TYPE = "bill_mode_type";//for billmode
    private static final String TABLENO = "tableNO";//for table no
    private static final String AC_NO = "accountNumber";//for table no
    private static final String ISOTD = "isot";
    private static final String PAX_CNT = "pax_count";
    private static final String STEWARD = "steward";


    private static final String TABLE_OT = "OT";
    private static final String ITEMCODE_OT = "itemcode_ot";
    private static final String ITEMNAME_OT = "itemname_ot";
    private static final String QTY_OT = "qty_ot";
    private static final String DISCOUNT = "discount";
    private static final String NARRATION = "narration";
    private static final String TEMP_HAPPYHOUR = "happyhour";
    private static final String TEMP_RATE = "rate_ot";
    private static final String NARRATION_ID = "narration_id";
    private static final String OID = "id";
    private static final String OCODE = "ocode";
    private static final String OFFERTYPE = "offertype";
    private static final String PARENTITEMID= "parentitemid";
    private static final String OFFERQTY= "offerqty";
    private static final String FREEQTY= "freeqty";

    private static final String TABLE_NARRATION = "table_narration";
    private static final String ITEM_NARRATION = "item_narrition";
    private static final String ITEM_POS = "item_pos";

    private static final String TABLE_MODIFIER = "table_modifier";
    private static final String MODIFIER_ID = "modifier_ID";
    private static final String MODIFIER_NAME = "modifier_name";

    private static final String TABLE_MODIFIER_DETAILS = "table_modifier_details";
    private static final String MODIFIER_DETAIL_ID = "modifier_detail_ID";
    private static final String MODIFIER_DETAIL_NAME = "modifier_detail_name";


    // QUERYS
    private static final String CREATE_TABLE_STORE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_STORE + "(" + ID + " TEXT ," + CODE + " TEXT," + ICID + " TEXT," + TYPE + " TEXT," + NAME + " TEXT," + CASH + " TEXT," + CREDIT + " TEXT," + ACCOUNT + " TEXT," + PAGE + " TEXT," + ISOT + " TEXT," + MID + " TEXT," + MSNAME + " TEXT," + ISREADER + " TEXT," + ISGUEST + " TEXT," + ADDRESS + " TEXT," + GST + " TEXT," + decimal + " TEXT," + PARTY + " TEXT" + ")";

    private static final String CREATE_TABLE_TABLENUMBER = "CREATE TABLE IF NOT EXISTS "
            + TABLE_TABLENAME + "(" + TID + " INTEGER ," + TABLENUMBER + " TEXT" + ")";

    private static final String CREATE_TABLE_ITEMGROUP = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ITEMGROUP + "(" + GID + " INTEGER ," + GCODE + " TEXT," + GNAME + " TEXT," + PoID + " TEXT" + ")";

    private static final String CREATE_TABLE_WAITER = "CREATE TABLE IF NOT EXISTS "
            + TABLE_WAITER + "(" + PID + " INTEGER ," + PCODE + " TEXT," + PNAME + " TEXT" + ")";


    private static final String CREATE_TABLE_ITEM = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ITEM + "(" + ICATEGORYID + " TEXT," + ICODE + " TEXT," + IIDENTITYID + " TEXT," + DESCRIPTION + " TEXT," + IGID + " TEXT," + INAME + " TEXT," + SID + " INTEGER," + TAX + " TEXT," + TaxID + " TEXT," + UNITID + " TEXT," + RATE + " TEXT," + ITEMIMAGE + " TEXT," + ITEMSTOCK + " TEXT" + ")";

    //  private static final String CREATE_TABLE_ACCOUNT = "CREATE TABLE IF NOT EXISTS "
    //           + TABLE_ACCOUNT + "("+ LID + " TEXT ," + POSID + " TEXT,"+ ACCNUM + " TEXT PRIMARY KEY,"+ CARDTYPE + " TEXT," + TODAYSDATE + " TEXT," + OPENING_BALANCE +" TEXT" +")";
    private static final String CREATE_TABLE_ACCOUNT = "CREATE TABLE "
            + TABLE_ACCOUNT + "(" + LID + " TEXT," + POSID + " TEXT," + ACCNUM + " TEXT," + ACC_NAME + " TEXT," + CARDTYPE + " TEXT," + TODAYSDATE + " TEXT," + OPENING_BALANCE + " TEXT," + PaCODE + " TEXT," + PaNAME + " TEXT," + PaID + " TEXT," + Pcodename + " TEXT," + BILLMODE + " TEXT," + BILLMODE_TYPE + " TEXT," + TABLENO + " TEXT," + AC_NO + " TEXT," + PAX_CNT + " TEXT," + STEWARD + " TEXT," + ISOTD + " INTEGER," + DEBIT_BALANCE + " TEXT" + ")";

    private static final String CREATE_TABLE_TABLE_OT = "CREATE TABLE IF NOT EXISTS "
            + TABLE_OT + "(" + ITEMCODE_OT + " TEXT PRIMARY KEY ," + ITEMNAME_OT + " TEXT," + QTY_OT + " TEXT," + DISCOUNT + " TEXT," + NARRATION + " TEXT," + TEMP_RATE + " TEXT," + NARRATION_ID + " TEXT," + TEMP_HAPPYHOUR + " TEXT," + OID + " INTEGER," + OCODE + " TEXT," +OFFERTYPE + " TEXT," +PARENTITEMID + " TEXT," +OFFERQTY + " TEXT," + FREEQTY + " TEXT" + ")";

    private static final String CREATE_TABLE_NARR = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NARRATION + "(" + ITEM_NARRATION + " TEXT," + ITEM_POS + " TEXT" + ")";

    private static final String CREATE_TABLE_NARRV2 = "CREATE TABLE IF NOT EXISTS "
            + TABLE_MODIFIER + "(" + MODIFIER_ID + " TEXT," + MODIFIER_NAME + " TEXT" + ")";

    private static final String CREATE_TABLE_NARR_DETAILS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_MODIFIER_DETAILS + "(" + MODIFIER_DETAIL_ID + " TEXT," + MODIFIER_DETAIL_NAME + " TEXT" + ")";

    //NSCI


    public Dbase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_STORE);
        db.execSQL(CREATE_TABLE_TABLENUMBER);
        db.execSQL(CREATE_TABLE_ITEMGROUP);
        db.execSQL(CREATE_TABLE_WAITER);
        db.execSQL(CREATE_TABLE_ITEM);
        db.execSQL(CREATE_TABLE_ACCOUNT);
        db.execSQL(CREATE_TABLE_TABLE_OT);
        db.execSQL(CREATE_TABLE_NARR);
        db.execSQL(CREATE_TABLE_NARRV2);
        db.execSQL(CREATE_TABLE_NARR_DETAILS);
        db.execSQL(
                "Create Table IF NOT EXISTS  OT_TRANSFER " +
                        "(BillID integer ,ItemID text,Otno text,Qty text,Amount text,Rate text,ItemSerialno text)"
        );
        db.execSQL(
                "Create Table IF NOT EXISTS  MODIFIER " +
                        "(Id text ,ItemId text,ItemName text,dId text,Name text,dName text,FreeFlow text)"
        );
        db.execSQL(
                "Create Table IF NOT EXISTS  ALLERGEN " +
                        "(ItemId text ,Name text)"
        );

        db.execSQL(
                "Create Table  IF NOT EXISTS BILLS " +
                        "(ItemID integer ,ItemCode text,Quantity text,Amount text,BillID text,mAcc text,ava_balance text,billNo text,Rate text,itemname text,mName text,memberId text,BillDate text,ACCharge text,BillDetNumber text,otNo text,ItemCategoryID  integer,opBalance text,cardType text,billdiscount text,GSTNO text)"
        );

        db.execSQL(
                "Create Table IF NOT EXISTS  BILL_TAX " +
                        "(ItemCategoryID integer,taxDescription text,Amount text,cardType text,discountamt text)"
        );

        db.execSQL(
                "Create Table  IF NOT EXISTS ITEM_PROMOTION " +
                        "(PromotionId integer,PromotionName text,ItemID text,Quantity text,Rate text,FromDate text,ToDate text,CreatedDate text,ModifiedDate text,OfferPromotionId text,FreeItemId text,FreeQty text,FreeItemName text,FreeItemCode text,ItemCode text)"
        );


        db.execSQL(
                "Create Table  IF NOT EXISTS OTREPRINT " +
                        "(otItemID integer ,otItemCode text,otQuantity text,otAmount text,otBillID text,otmAcc text,otava_balance text,otbillNo text,otRate text,otitemname text,otmName text,otmemberId text,otBillDate text,ototNo text,otItemCategoryID  integer)"
        );


        db.execSQL(
                "Create Table  IF NOT EXISTS NARRATION_TAX " +
                        "(NarrationDescription text,Narrationid text)"
        );
        db.execSQL(
                "Create Table  IF NOT EXISTS ITEMS_TAX " +
                        "(ItemID integer ,TaxID text,TaxCode text,Description text,Value text,Type text,CreatedDate text,Modifieddate text,IsTaxApplicable text,issubtax text,Parentid text,taxtype text)"

        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STORE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNT);
        db.execSQL("DROP TABLE IF EXISTS ITEMS_TAX");
        db.execSQL("DROP TABLE IF EXISTS MODIFIER");
        db.execSQL("DROP TABLE IF EXISTS ALLERGEN");
        onCreate(db);
    }

    public List<String> getWaiterFilter(String waiter) {
        List<String> labels = new ArrayList<String>();
        //String p=param;
        String p1 = "'" + waiter + "%'";
        //  String p1=pId;
        String selectQuery = "", selectQuery1 = "";
        // Select All Query

//        selectQuery = "SELECT icode,iname   FROM " + TABLE_ITEM +" where itemgid='"+gip+"'"+" and  icode like"+"'"+p+"'";
//        selectQuery1 = "SELECT icode,iname   FROM " + TABLE_ITEM +" where itemgid='"+gip+"'"+" and  iname like"+"'"+p+"'";
        //   selectQuery = "SELECT DISTINCT icode,iname   FROM " + TABLE_ITEM +" where  icode like" + "'" + p + "'" + "and itemgid like " + "'" + p1 + "'";
        //   selectQuery1 = "SELECT  DISTINCT icode,iname   FROM " + TABLE_ITEM +" where iname like" + "'" + p +"'" + "and itemgid like"+ "'" + p1 + "'";
        //selectQuery = "SELECT DISTINCT icode,iname   FROM " + TABLE_ITEM +" where  itemcategoryid like "  + p1 ;

        // selectQuery1 = "SELECT  DISTINCT icode,iname   FROM " + TABLE_ITEM +" where  itemcategoryid like" + p1 + "'";


        selectQuery = "SELECT  pcode,pname   FROM " + TABLE_WAITER + " where pname like " + p1 + " order by pname asc";
        selectQuery1 = "SELECT  pcode,pname   FROM " + TABLE_WAITER + " where pcode like " + p1 + " order by pcode asc";

        // itemstoreid
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Cursor cursor2 = db.rawQuery(selectQuery1, null);
        // looping through all rows and adding to list
        if (cursor.getCount() != 0) {

            if (cursor.moveToFirst()) {
                do {

                    labels.add(cursor.getString(0) + " " + "_" + " " + cursor.getString(1));
                    // labels.add(cursor.getString(0) + "_" + cursor.getString(1));
                } while (cursor.moveToNext());
            }
        } else {
            if (cursor2.moveToFirst()) {
                do {
                    labels.add(cursor2.getString(0) + "_" + cursor2.getString(1));
                } while (cursor2.moveToNext());
            }
        }

        // closing connection
        cursor.close();
        cursor2.close();
        db.close();

        return labels;

    }

    public String getItemname(String itemcode) {
        String iname = "";
        // String selectQuery = "SELECT rate FROM " + TABLE_ITEM +" where icode="+"'"+itemcode+"'" +"and itemcategoryid="+itemcategoryid;//century
        String selectQuery = "SELECT iname FROM " + TABLE_ITEM + " where itemid=" + "'" + itemcode + "'";//nsci
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    iname = cursor.getString(0);
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return iname;
    }

    public ArrayList<ItemPromo> getItemPromotiondetails(String promoid) {
        ArrayList<ItemPromo> promotionlist = new ArrayList<ItemPromo>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM ITEM_PROMOTION where PromotionId=" + promoid + " ";
        Cursor cursor = db.rawQuery(selectQuery, null);
        ItemPromo itemPromo = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    itemPromo = new ItemPromo();
                    itemPromo.setPromotionName(cursor.getString(1));
                    itemPromo.setItemID(cursor.getString(2));
                    itemPromo.setQuantity(cursor.getString(3));
                    itemPromo.setRate(cursor.getString(4));
                    itemPromo.setFromDate(cursor.getString(5));
                    itemPromo.setToDate(cursor.getString(6));
                    itemPromo.setCreatedDate(cursor.getString(7));
                    itemPromo.setModifiedDate(cursor.getString(8));
                    itemPromo.setOfferPromotionId(cursor.getString(9));
                    itemPromo.setFreeItemId(cursor.getString(10));
                    itemPromo.setFreeQty(cursor.getString(11));
                    itemPromo.setFreeItemName(cursor.getString(12));
                    itemPromo.setFreeItemCode(cursor.getString(13));
                    promotionlist.add(itemPromo);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        db.close();
        return promotionlist;
    }

    public String getRate(String itemcode, String itemcategoryid) {
        String rate = "";
        // String selectQuery = "SELECT rate FROM " + TABLE_ITEM +" where icode="+"'"+itemcode+"'" +"and itemcategoryid="+itemcategoryid;//century
        String selectQuery = "SELECT rate FROM " + TABLE_ITEM + " where icode=" + "'" + itemcode + "'" + "and itemstoreid=" + itemcategoryid;//nsci
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    rate = cursor.getString(0);
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return rate;
    }

    public int updateTableNo(String accountNumber, String tableNO, String POS, String userId, String todaydate, String paxcount, String waiternamecode) {
        SQLiteDatabase db = this.getWritableDatabase();
        int insetcount = 0;
        // String date = todaydate.replace("/", "-");
        ContentValues values = new ContentValues();
        values.put(TABLENO, tableNO);
        values.put(ISOTD, 1);
        values.put(ISOTD, 1);
        values.put(LID, userId);
        values.put(TODAYSDATE, todaydate);
        values.put(PAX_CNT, paxcount);
        values.put(STEWARD, waiternamecode);

        insetcount = db.update(TABLE_ACCOUNT, values, AC_NO + "= ? AND " + POSID + "= ?", new String[]{accountNumber, POS});
        db.close();
        return insetcount;
    }

    public int updateTableNo(String accountNumber, String tableNO, String POS, String userId, String todaydate, String paxcount, String waiternamecode, String isOT) {
        SQLiteDatabase db = this.getWritableDatabase();
        int insetcount = 0;
        // String date = todaydate.replace("/", "-");
        ContentValues values = new ContentValues();
        values.put(TABLENO, tableNO);
        values.put(ISOTD, isOT);
        values.put(LID, userId);
        values.put(TODAYSDATE, todaydate);
        values.put(PAX_CNT, paxcount);
      values.put(STEWARD, waiternamecode);

        insetcount = db.update(TABLE_ACCOUNT, values, AC_NO + "= ? AND " + POSID + "= ?", new String[]{accountNumber, POS});
        db.close();
        return insetcount;
    }
    public int updateTableNoWaiterwise(String accountNumber, String tableNO, String POS, String userId, String todaydate, String paxcount, String waiternamecode, String isOT) {
        int insetcount = 0;
        try {
        SQLiteDatabase db = this.getWritableDatabase();

        String _waiter="'"+waiternamecode+"'";
        // String date = todaydate.replace("/", "-");
        ContentValues values = new ContentValues();
        values.put(TABLENO, tableNO);
        values.put(ISOTD, isOT);
        values.put(LID, userId);
        values.put(TODAYSDATE, todaydate);
        values.put(PAX_CNT, paxcount);
        values.put(STEWARD, waiternamecode);

        insetcount = db.update(TABLE_ACCOUNT, values, AC_NO + "= ? AND " + POSID + "= ?"+ "= ? AND " + PaID + "= ?", new String[]{accountNumber, POS,_waiter});
        db.close();
    }
    catch (Exception e){
        Log.d("exp",""+e);
    }

        return insetcount;
    }
 public int updateTableNoWaiterWise(String accountNumber, String tableNO, String POS, String userId, String todaydate, String paxcount, String waiternamecode, String isOT) {
        SQLiteDatabase db = this.getWritableDatabase();
        int insetcount = 0;
        // String date = todaydate.replace("/", "-");
        ContentValues values = new ContentValues();
        values.put(TABLENO, tableNO);
        values.put(ISOTD, isOT);
        values.put(LID, userId);
        values.put(TODAYSDATE, todaydate);
        values.put(PAX_CNT, paxcount);
        values.put(PaID, waiternamecode);

        insetcount = db.update(TABLE_ACCOUNT, values, AC_NO + "= ? AND " + POSID + "= ?"+ "= ? AND " + PaID + "= ?", new String[]{accountNumber, POS,waiternamecode});
        db.close();
        return insetcount;
    }

    public int updateIsot(String accountNumber, String tableNO, String POS) {
        SQLiteDatabase db = this.getWritableDatabase();
        int insetcount = 0;

        ContentValues values = new ContentValues();
        values.put(ISOTD, 1);

        insetcount = db.update(TABLE_ACCOUNT, values, AC_NO + "= ? AND " + POSID + "= ?", new String[]{accountNumber, POS});
        db.close();
        return insetcount;
    }

    public long clearBills() {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete("BILLS", null, null);
        long delete1 = db.delete("BILL_TAX", null, null);
        db.close();
        return delete;
    }

    public long clearMods() {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete("MODIFIER", null, null);
        db.close();
        return delete;
    }

    public long clearAllen() {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete("ALLERGEN", null, null);
        db.close();
        return delete;
    }

    public long clearotreprint() {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete("OTREPRINT", null, null);

        db.close();
        return delete;
    }

    public long clearPromotion() {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete("ITEM_PROMOTION", null, null);
        db.close();
        return delete;
    }


    public Long insertItemPromotion(String PromotionId, String PromotionName, String ItemID, String Quantity, String Rate, String FromDate, String ToDate, String CreatedDate, String ModifiedDate, String OfferPromotionId, String FreeItemId, String FreeQty, String FreeItemName, String FreeItemCode, String ItemCode) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;

        ContentValues values1 = new ContentValues();
        values1.put("PromotionId", PromotionId);
        values1.put("PromotionName", PromotionName);
        values1.put("Quantity", Quantity);
        values1.put("ItemID", ItemID);
        values1.put("Quantity", Quantity);
        values1.put("Rate", Rate);
        values1.put("FromDate", FromDate);
        values1.put("ToDate", ToDate);
        values1.put("Rate", Rate);
        values1.put("CreatedDate", CreatedDate);
        values1.put("ModifiedDate", ModifiedDate);
        values1.put("OfferPromotionId", OfferPromotionId);
        values1.put("FreeItemId", FreeItemId);
        values1.put("FreeQty", FreeQty);
        values1.put("FreeItemName", FreeItemName);
        values1.put("FreeItemCode", FreeItemCode);
        values1.put("ItemCode", ItemCode);

        insetcount = db.insert("ITEM_PROMOTION", null, values1);
        db.close();
        return insetcount;
    }

    public ArrayList<ItemPromo> getItemPromotion() {
        ArrayList<ItemPromo> promotionlist = new ArrayList<ItemPromo>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  distinct PromotionName FROM ITEM_PROMOTION ";
        Cursor cursor = db.rawQuery(selectQuery, null);
        ItemPromo itemPromo = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    itemPromo = new ItemPromo();
                    itemPromo.setPromotionName(cursor.getString(0));
                    promotionlist.add(itemPromo);
                } while (cursor.moveToNext());

            }
        }
        cursor.close();
        db.close();
        return promotionlist;

    }

    public Long insertBills(String ItemID, String ItemCode, String Quantity, String Amount, String BillID, String mAcc, String ava_balance, String billNo, String Rate, String itemname, String mName, String memberId, String BillDate, String ACCharge, String BillDetNumber, String otNo, String ItemCategoryID, String opBalance, String cardType, String discountamt, String GSTNO) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;
        ContentValues values1 = new ContentValues();
        values1.put("ItemID", ItemID);
        values1.put("ItemCode", ItemCode);
        values1.put("Quantity", Quantity);
        values1.put("Amount", Amount);
        values1.put("BillID", BillID);
        values1.put("mAcc", mAcc);
        values1.put("ava_balance", ava_balance);
        values1.put("billNo", billNo);
        values1.put("Rate", Rate);
        values1.put("itemname", itemname);
        values1.put("mName", mName);
        values1.put("memberId", memberId);
        values1.put("BillDate", BillDate);
        values1.put("ACCharge", ACCharge);
        values1.put("BillDetNumber", BillDetNumber);
        values1.put("otNo", otNo);
        values1.put("ItemCategoryID", ItemCategoryID);
        values1.put("opBalance", opBalance);
        values1.put("cardType", cardType);
        values1.put("billdiscount", discountamt);
        values1.put("GSTNO", GSTNO);
        insetcount = db.insert("BILLS", null, values1);
        db.close();
        return insetcount;
    }


    public Long insertotreprint(String ItemID, String ItemCode, String Quantity, String Amount, String BillID, String mAcc, String ava_balance, String billNo, String Rate, String itemname, String mName, String memberId, String BillDate, String otNo, String ItemCategoryID) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;
        ContentValues values1 = new ContentValues();
        values1.put("otItemID", ItemID);
        values1.put("otItemCode", ItemCode);
        values1.put("otQuantity", Quantity);
        values1.put("otAmount", Amount);
        values1.put("otBillID", BillID);
        values1.put("otmAcc", mAcc);
        values1.put("otava_balance", ava_balance);
        values1.put("otbillNo", billNo);
        values1.put("otRate", Rate);
        values1.put("otitemname", itemname);
        values1.put("otmName", mName);
        values1.put("otmemberId", memberId);
        values1.put("otBillDate", BillDate);
        values1.put("otItemCategoryID", ItemCategoryID);
        values1.put("ototNo", otNo);
        insetcount = db.insert("OTREPRINT", null, values1);
        db.close();
        return insetcount;
    }

    public Long insertBillTax(String ItemCategoryID, String taxDescription, String Amount, String cardType, String dicountamt) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;
        ContentValues values1 = new ContentValues();
        values1.put("ItemCategoryID", ItemCategoryID);
        values1.put("taxDescription", taxDescription);
        values1.put("Amount", Amount);
        values1.put("cardType", cardType);
        values1.put("discountamt", dicountamt);
        insetcount = db.insert("BILL_TAX", null, values1);
        db.close();
        return insetcount;
    }

    public Long insertNarrationTax(String Narrationid, String NarrationDescription) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;
        ContentValues values1 = new ContentValues();
        values1.put("Narrationid", Narrationid);
        values1.put("NarrationDescription", NarrationDescription);
        insetcount = db.insert("NARRATION_TAX", null, values1);
        db.close();
        return insetcount;
    }

    public ArrayList<String> getOt() {
        ArrayList<String> billlist = new ArrayList<>();
        String selectQuery = "SELECT distinct ItemCategoryID FROM BILLS where BillDetNumber='Y' ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    billlist.add(cursor.getString(0));
                } while (cursor.moveToNext());

            }
        }
        cursor.close();
        db.close();
        return billlist;
    }

    public ArrayList<String> getOtreprint() {
        ArrayList<String> billlist = new ArrayList<>();
        String selectQuery = "SELECT distinct otItemCategoryID FROM OTREPRINT";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    billlist.add(cursor.getString(0));
                } while (cursor.moveToNext());

            }
        }
        cursor.close();
        db.close();
        return billlist;
    }

    public ArrayList<String> getBills() {
        ArrayList<String> billlist = new ArrayList<>();
//        String selectQuery = "SELECT distinct ItemCategoryID FROM BILLS";
        String selectQuery = "SELECT distinct BillID FROM BILLS";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    billlist.add(cursor.getString(0));
                } while (cursor.moveToNext());

            }
        }
        cursor.close();
        db.close();
        return billlist;
    }

    public ArrayList<CloseorderItems> getotDetails(String ItemCategoryID) {
        ArrayList<CloseorderItems> billlist = new ArrayList<>();
        String selectQuery = "SELECT ItemCode,itemname,Quantity,Amount,Rate,mAcc,mName,memberId,ava_balance,opBalance,ItemCategoryID,billNo,BillDetNumber,ACCharge,BillDate,cardType FROM BILLS where ItemCategoryID=" + ItemCategoryID + " and BillDetNumber='Y' ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        CloseorderItems closeorderItems = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    closeorderItems = new CloseorderItems();
                    closeorderItems.setItemCode(cursor.getString(0));
                    closeorderItems.setItemname(cursor.getString(1));
                    closeorderItems.setQuantity(cursor.getString(2));
                    closeorderItems.setAmount(cursor.getString(3));
                    closeorderItems.setRate(cursor.getString(4));
                    closeorderItems.setmAcc(cursor.getString(5));
                    closeorderItems.setmName(cursor.getString(6));
                    closeorderItems.setMemberID(cursor.getString(7));
                    closeorderItems.setAva_balance(cursor.getString(8));
                    closeorderItems.setOpeningBalance(cursor.getString(9));
                    closeorderItems.setItemCategoryID(cursor.getString(10));
                    closeorderItems.setBillno(cursor.getString(11));
                    closeorderItems.setBillDetNumber(cursor.getString(12));
                    closeorderItems.setACCharge(cursor.getString(13));
                    closeorderItems.setBillDate(cursor.getString(14));
                    closeorderItems.setCardType(cursor.getString(15));
                    billlist.add(closeorderItems);
                } while (cursor.moveToNext());

            }
        }
        cursor.close();
        db.close();
        return billlist;
    }

    public ArrayList<CloseorderItems> getotreprintDetails(String ItemCategoryID) {
        ArrayList<CloseorderItems> billlist = new ArrayList<>();
        String selectQuery = "SELECT otItemCode,otitemname,sum(otQuantity),sum(otAmount),otRate,otmAcc,otmName,otmemberId FROM OTREPRINT where otItemCategoryID=" + ItemCategoryID + " Group by otItemCode";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        CloseorderItems closeorderItems = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    closeorderItems = new CloseorderItems();
                    closeorderItems.setItemCode(cursor.getString(0));
                    closeorderItems.setItemname(cursor.getString(1));
                    closeorderItems.setQuantity(cursor.getString(2));
                    closeorderItems.setAmount(cursor.getString(3));
                    closeorderItems.setRate(cursor.getString(4));
                    closeorderItems.setmAcc(cursor.getString(5));
                    closeorderItems.setmName(cursor.getString(6));
                    closeorderItems.setMemberID(cursor.getString(7));
                    closeorderItems.setAva_balance("");
                    closeorderItems.setOpeningBalance("");
                    closeorderItems.setItemCategoryID("");
                    closeorderItems.setBillno("");
                    closeorderItems.setBillDetNumber("");
                    closeorderItems.setACCharge("");
                    closeorderItems.setBillDate("");
                    closeorderItems.setCardType("");
                    billlist.add(closeorderItems);
                } while (cursor.moveToNext());

            }
        }
        cursor.close();
        db.close();
        return billlist;
    }

 /*     public ArrayList<CloseorderItems> getBillDetails(String ItemCategoryID) {
          ArrayList<CloseorderItems> billlist = new ArrayList<>();
          String selectQuery = "SELECT ItemCode,itemname,Quantity,Amount,Rate,mAcc,mName,memberId,ava_balance,opBalance,ItemCategoryID,billNo,BillDetNumber,ACCharge,BillDate,cardType,billdiscount FROM BILLS where ItemCategoryID=" + ItemCategoryID;

  // String selectQuery1 = "SELECT DISTINCT ItemCode,itemname,sum(Quantity),sum(Amount),Rate,mAcc,mName,memberId,ava_balance,opBalance,ItemCategoryID,billNo,BillDetNumber,ACCharge,BillDate,cardType,billdiscount FROM BILLS where ItemCategoryID=" + ItemCategoryID+ " GROUP BY Quantity, Amount";
       //   String selectQuery1 = "SELECT DISTINCT ItemCode,itemname,sum(Quantity),sum(Amount),Rate,mAcc,mName,memberId,ava_balance,opBalance,ItemCategoryID,billNo,BillDetNumber,ACCharge,BillDate,cardType,billdiscount FROM BILLS where ItemCategoryID=" + ItemCategoryID+ " GROUP BY Quantity, Amount";


          SQLiteDatabase db = this.getReadableDatabase();
          Cursor cursor = db.rawQuery(selectQuery, null);
          CloseorderItems closeorderItems = null;
          if (cursor != null) {
              if (cursor.moveToFirst()) {
                  do {
                      closeorderItems = new CloseorderItems();
                      closeorderItems.setItemCode(cursor.getString(0));
                      closeorderItems.setItemname(cursor.getString(1));
                      closeorderItems.setQuantity(cursor.getString(2));
                      closeorderItems.setAmount(cursor.getString(3));
                      closeorderItems.setRate(cursor.getString(4));
                      closeorderItems.setmAcc(cursor.getString(5));
                      closeorderItems.setmName(cursor.getString(6));
                      closeorderItems.setMemberID(cursor.getString(7));
                      closeorderItems.setAva_balance(cursor.getString(8));
                      closeorderItems.setOpeningBalance(cursor.getString(9));
                      closeorderItems.setItemCategoryID(cursor.getString(10));
                      closeorderItems.setBillno(cursor.getString(11));
                      closeorderItems.setBillDetNumber(cursor.getString(12));
                      closeorderItems.setACCharge(cursor.getString(13));
                      closeorderItems.setBillDate(cursor.getString(14));
                      closeorderItems.setCardType(cursor.getString(15));
                      closeorderItems.setDiscountAmt(cursor.getString(16));
                      billlist.add(closeorderItems);
                  } while (cursor.moveToNext());

              }
          }
          cursor.close();
          db.close();
          return billlist;
      }
*/

/*    public ArrayList<CloseorderItems> getBillDetails(String ItemCategoryID) {
        ArrayList<CloseorderItems> billlist = new ArrayList<>();
        String selectQuery = "SELECT ItemCode,itemname,Quantity,Amount,Rate,mAcc,mName,memberId,ava_balance,opBalance,ItemCategoryID,billNo,BillDetNumber,ACCharge,BillDate,cardType,billdiscount FROM BILLS where ItemCategoryID=" + ItemCategoryID;
        String itemcode = "";
// String selectQuery1 = "SELECT DISTINCT ItemCode,itemname,sum(Quantity),sum(Amount),Rate,mAcc,mName,memberId,ava_balance,opBalance,ItemCategoryID,billNo,BillDetNumber,ACCharge,BillDate,cardType,billdiscount FROM BILLS where ItemCategoryID=" + ItemCategoryID+ " GROUP BY Quantity, Amount";
        String selectQuery1 = "SELECT billNo,BillDetNumber,BillDate,DISTINCT ItemCode,itemname,sum(Quantity),sum(Amount),Rate,mAcc,mName,memberId,ava_balance,opBalance,ItemCategoryID,ACCharge,cardType,billdiscount FROM BILLS where ItemCategoryID=" + ItemCategoryID + " GROUP BY Quantity, Amount";

        ArrayList<String> araylist = new ArrayList<>();
        ArrayList<String> araylistitem = new ArrayList<>();

        //SELECT DataId, COUNT(*) c FROM DataTab GROUP BY DataId HAVING c > 1;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        CloseorderItems closeorderItems = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    itemcode = cursor.getString(0);
                    String selectQueryduplicate = "SELECT ItemCode,COUNT(*) c FROM BILLS  where ItemCode='" + itemcode + "' GROUP BY ItemCode HAVING c > 1";
                    Cursor cursor1 = db.rawQuery(selectQueryduplicate, null);
                    if (cursor1 != null) {
                        if (cursor1.moveToFirst()) {
                            do {
                                String selectQuerysum = "SELECT sum(Quantity),sum(Amount) FROM BILLS where ItemCode='" + itemcode + "' GROUP BY Quantity,Amount";
                                Cursor cursor2 = db.rawQuery(selectQuerysum, null);
                                if (cursor2 != null) {
                                    araylist.add(itemcode);
                                    if (cursor2.moveToFirst()) {
                                        do {
                                            if (araylist.size() > 0) {
                                                if (!araylistitem.contains(itemcode)) {
                                                    araylistitem.add(cursor.getString(0));
                                                    String amt = cursor2.getString(1);
                                                    String qty = cursor2.getString(0);
                                                    closeorderItems = new CloseorderItems();
                                                    closeorderItems.setItemCode(cursor.getString(0));
                                                    closeorderItems.setItemname(cursor.getString(1));
                                                    closeorderItems.setQuantity(qty);
                                                    closeorderItems.setAmount(amt);
                                                    closeorderItems.setRate(cursor.getString(4));
                                                    closeorderItems.setmAcc(cursor.getString(5));
                                                    closeorderItems.setmName(cursor.getString(6));
                                                    closeorderItems.setMemberID(cursor.getString(7));
                                                    closeorderItems.setAva_balance(cursor.getString(8));
                                                    closeorderItems.setOpeningBalance(cursor.getString(9));
                                                    closeorderItems.setItemCategoryID(cursor.getString(10));
                                                    closeorderItems.setBillno(cursor.getString(11));
                                                    closeorderItems.setBillDetNumber(cursor.getString(12));
                                                    closeorderItems.setACCharge(cursor.getString(13));
                                                    closeorderItems.setBillDate(cursor.getString(14));
                                                    closeorderItems.setCardType(cursor.getString(15));
                                                    closeorderItems.setDiscountAmt(cursor.getString(16));
                                                    billlist.add(closeorderItems);
                                                }




                                            //    for (int k = 0; k < araylist.size(); k++) {

                                              //  }
                                            }
                                            else {
                                                closeorderItems = new CloseorderItems();
                                                closeorderItems.setItemCode(cursor.getString(0));
                                                closeorderItems.setItemname(cursor.getString(1));
                                                closeorderItems.setQuantity(cursor.getString(2));
                                                closeorderItems.setAmount(cursor.getString(3));
                                                closeorderItems.setRate(cursor.getString(4));
                                                closeorderItems.setmAcc(cursor.getString(5));
                                                closeorderItems.setmName(cursor.getString(6));
                                                closeorderItems.setMemberID(cursor.getString(7));
                                                closeorderItems.setAva_balance(cursor.getString(8));
                                                closeorderItems.setOpeningBalance(cursor.getString(9));
                                                closeorderItems.setItemCategoryID(cursor.getString(10));
                                                closeorderItems.setBillno(cursor.getString(11));
                                                closeorderItems.setBillDetNumber(cursor.getString(12));
                                                closeorderItems.setACCharge(cursor.getString(13));
                                                closeorderItems.setBillDate(cursor.getString(14));
                                                closeorderItems.setCardType(cursor.getString(15));
                                                closeorderItems.setDiscountAmt(cursor.getString(16));
                                                araylistitem.add(cursor.getString(0));
                                                billlist.add(closeorderItems);
                                            }
                                        }
                                        while (cursor2.moveToNext());
                                    }
                                }

                            }
                            while (cursor1.moveToNext());
                        }
                        else {
                            closeorderItems = new CloseorderItems();
                            closeorderItems.setItemCode(cursor.getString(0));
                            closeorderItems.setItemname(cursor.getString(1));
                            closeorderItems.setQuantity(cursor.getString(2));
                            closeorderItems.setAmount(cursor.getString(3));
                            closeorderItems.setRate(cursor.getString(4));
                            closeorderItems.setmAcc(cursor.getString(5));
                            closeorderItems.setmName(cursor.getString(6));
                            closeorderItems.setMemberID(cursor.getString(7));
                            closeorderItems.setAva_balance(cursor.getString(8));
                            closeorderItems.setOpeningBalance(cursor.getString(9));
                            closeorderItems.setItemCategoryID(cursor.getString(10));
                            closeorderItems.setBillno(cursor.getString(11));
                            closeorderItems.setBillDetNumber(cursor.getString(12));
                            closeorderItems.setACCharge(cursor.getString(13));
                            closeorderItems.setBillDate(cursor.getString(14));
                            closeorderItems.setCardType(cursor.getString(15));
                            closeorderItems.setDiscountAmt(cursor.getString(16));
                            araylistitem.add(cursor.getString(0));
                            billlist.add(closeorderItems);
                        }
                    } else {
*//*
                        if (araylistitem.size() > 0) {

                            for (int k = 0; k < araylistitem.size(); k++) {

                                if (!araylistitem.get(k).equalsIgnoreCase(cursor.getString(0))) {
                                    araylistitem.add(cursor.getString(0));
                                    closeorderItems = new CloseorderItems();
                                    closeorderItems.setItemCode(cursor.getString(0));
                                    closeorderItems.setItemname(cursor.getString(1));
                                    closeorderItems.setQuantity(cursor.getString(2));
                                    closeorderItems.setAmount(cursor.getString(3));
                                    closeorderItems.setRate(cursor.getString(4));
                                    closeorderItems.setmAcc(cursor.getString(5));
                                    closeorderItems.setmName(cursor.getString(6));
                                    closeorderItems.setMemberID(cursor.getString(7));
                                    closeorderItems.setAva_balance(cursor.getString(8));
                                    closeorderItems.setOpeningBalance(cursor.getString(9));
                                    closeorderItems.setItemCategoryID(cursor.getString(10));
                                    closeorderItems.setBillno(cursor.getString(11));
                                    closeorderItems.setBillDetNumber(cursor.getString(12));
                                    closeorderItems.setACCharge(cursor.getString(13));
                                    closeorderItems.setBillDate(cursor.getString(14));
                                    closeorderItems.setCardType(cursor.getString(15));
                                    closeorderItems.setDiscountAmt(cursor.getString(16));
                                    billlist.add(closeorderItems);
                                }
                            }
                        } else {*//*
                            closeorderItems = new CloseorderItems();
                            closeorderItems.setItemCode(cursor.getString(0));
                            closeorderItems.setItemname(cursor.getString(1));
                            closeorderItems.setQuantity(cursor.getString(2));
                            closeorderItems.setAmount(cursor.getString(3));
                            closeorderItems.setRate(cursor.getString(4));
                            closeorderItems.setmAcc(cursor.getString(5));
                            closeorderItems.setmName(cursor.getString(6));
                            closeorderItems.setMemberID(cursor.getString(7));
                            closeorderItems.setAva_balance(cursor.getString(8));
                            closeorderItems.setOpeningBalance(cursor.getString(9));
                            closeorderItems.setItemCategoryID(cursor.getString(10));
                            closeorderItems.setBillno(cursor.getString(11));
                            closeorderItems.setBillDetNumber(cursor.getString(12));
                            closeorderItems.setACCharge(cursor.getString(13));
                            closeorderItems.setBillDate(cursor.getString(14));
                            closeorderItems.setCardType(cursor.getString(15));
                            closeorderItems.setDiscountAmt(cursor.getString(16));
                            araylistitem.add(cursor.getString(0));
                            billlist.add(closeorderItems);
                       // }
                    }
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        db.close();
        return billlist;
    }*/

    public ArrayList<CloseorderItems> getBillDetails(String ItemCategoryID) {
        ArrayList<CloseorderItems> billlist = new ArrayList<>();
//        String selectQuery = "SELECT distinct ItemCode,itemname,sum(Quantity),sum(Amount),Rate,mAcc,mName,memberId,ava_balance,opBalance,ItemCategoryID,billNo,BillDetNumber,ACCharge,cardType,billdiscount FROM BILLS where ItemCategoryID=" + ItemCategoryID+ " Group by ItemCode";
        //  String selectQuery = "SELECT distinct ItemCode,itemname,sum(Quantity),sum(Amount),Rate,mAcc,mName,memberId,ava_balance,opBalance,ItemCategoryID,billNo,BillDetNumber,ACCharge,cardType,billdiscount FROM BILLS where BillID=" + ItemCategoryID + " Group by ItemCode";
        String selectQuery = "SELECT   ItemCode,itemname, Quantity , Amount ,Rate,mAcc,mName,memberId,ava_balance,opBalance,ItemCategoryID,billNo,BillDetNumber,ACCharge,cardType,billdiscount FROM BILLS where BillID=" + ItemCategoryID;

        ArrayList<String> itemcode = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        CloseorderItems closeorderItems = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    closeorderItems = new CloseorderItems();
                    closeorderItems.setItemCode(cursor.getString(0));
                    closeorderItems.setItemname(cursor.getString(1));
                    closeorderItems.setQuantity(cursor.getString(2));
                    closeorderItems.setAmount(cursor.getString(3));
                    closeorderItems.setRate(cursor.getString(4));
                    closeorderItems.setmAcc(cursor.getString(5));
                    closeorderItems.setmName(cursor.getString(6));
                    closeorderItems.setMemberID(cursor.getString(7));
                    closeorderItems.setAva_balance(cursor.getString(8));
                    closeorderItems.setOpeningBalance(cursor.getString(9));
                    closeorderItems.setItemCategoryID(cursor.getString(10));
                    closeorderItems.setBillno(cursor.getString(11));
                    closeorderItems.setBillDetNumber(cursor.getString(12));
                    closeorderItems.setACCharge(cursor.getString(13));
                    //  closeorderItems.setBillDate(cursor.getString(14));
                    closeorderItems.setCardType(cursor.getString(14));
                    closeorderItems.setDiscountAmt(cursor.getString(15));
                    billlist.add(closeorderItems);
                    itemcode.add(cursor.getString(0));
                }
                while (cursor.moveToNext());

            }
        }
        cursor.close();
        db.close();
        return billlist;
    }


    public ArrayList<CloseorderItems> getBilltax(String ItemCategoryID) {
        ArrayList<CloseorderItems> taxlist = new ArrayList<>();
        String selectQuery = "SELECT Amount,taxDescription,ItemCategoryID,cardType,discountamt FROM BILL_TAX";
        //where ItemCategoryID=" + ItemCategoryID;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        CloseorderItems closeorderItems = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    closeorderItems = new CloseorderItems();
                    closeorderItems.setAmount(cursor.getString(0));
                    closeorderItems.setTaxDescription(cursor.getString(1));
                    closeorderItems.setItemCategoryID(cursor.getString(2));
                    closeorderItems.setCardType(cursor.getString(3));
                    closeorderItems.setDiscountAmt(cursor.getString(4));
                    taxlist.add(closeorderItems);
                } while (cursor.moveToNext());

            }
        }
        cursor.close();
        db.close();
        return taxlist;
    }

    public Long insertModifierDetails(String description, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;

        ContentValues values1 = new ContentValues();
        values1.put(MODIFIER_DETAIL_ID, id);
        values1.put(MODIFIER_DETAIL_NAME, description);
        insetcount = db.insert(TABLE_MODIFIER_DETAILS, null, values1);
        db.close();
        return insetcount;
    }

    //    public Long insertOtTransfer(String BillID  ,String ItemID ,String Otno ,String Qty ,String Amount ,String Rate )
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//        Long insetcount = null;
//
//        ContentValues values1 = new ContentValues();
//        values1.put("BillID",BillID);
//        values1.put("ItemID",ItemID);
//        values1.put("Otno",Otno);
//        values1.put("Qty",Qty);
//        values1.put("Amount",Amount);
//        values1.put("Rate",Rate);
//        //values1.put("ItemSerialno",ItemSerialno);
//        insetcount= db.insert("OT_TRANSFER", null, values1);
//        db.close();
//        return insetcount;
//    }
    public Long insertModifier(String description, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;

        ContentValues values1 = new ContentValues();
        values1.put(MODIFIER_ID, id);
        values1.put(MODIFIER_NAME, description);

        insetcount = db.insert(TABLE_MODIFIER, null, values1);


        db.close();
        return insetcount;
    }

    public Long insertNarration(String description, String pos) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;

        ContentValues values1 = new ContentValues();
        values1.put(ITEM_NARRATION, description);
        values1.put(ITEM_POS, pos);

        insetcount = db.insert(TABLE_NARRATION, null, values1);


        db.close();
        return insetcount;
    }

    public ArrayList<String> getNarration() {

        ArrayList<String> list = new ArrayList<>();
        String selectQuery = "SELECT item_narrition FROM " + TABLE_NARRATION;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String compare = "";
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    list.add(cursor.getString(0));
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return list;
    }

    public List<ModifiersDto> getModifier() {

        List<ModifiersDto> list = new ArrayList<>();
        String selectQuery = "SELECT modifier_name,modifier_ID FROM " + TABLE_MODIFIER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String compare = "";
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    ModifiersDto modifiersDto = new ModifiersDto();
                    modifiersDto.setModifierName(cursor.getString(0));
                    modifiersDto.setModifierId(cursor.getString(1));
                    list.add(modifiersDto);
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return list;
    }

    public List<ModifiersDto> getModifierDetails() {

        List<ModifiersDto> list = new ArrayList<>();
        String selectQuery = "SELECT modifier_detail_name,modifier_detail_ID FROM " + TABLE_MODIFIER_DETAILS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    ModifiersDto modifiersDto = new ModifiersDto();
                    modifiersDto.setModifierName(cursor.getString(0));
                    modifiersDto.setModifierId(cursor.getString(1));
                    list.add(modifiersDto);
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return list;
    }

    //count
    public int getStoreCount() {
        String selectQuery = "SELECT sId  FROM " + TABLE_STORE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        db.close();
        return count;

    }

    public int getTableCount() {
        String selectQuery = "SELECT tableid  FROM " + TABLE_TABLENAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        db.close();
        return count;

    }

    public int getNarrationCount() {
        String selectQuery = "SELECT Narrationid FROM NARRATION_TAX";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        db.close();
        return count;

    }

    public int getItemGroupCount() {
        String selectQuery = "SELECT gid  FROM " + TABLE_ITEMGROUP;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        db.close();
        return count;
    }

    public int getItemGroupCount(String posid) {
        String selectQuery = "SELECT gid  FROM " + TABLE_ITEMGROUP + " where  posid ='" + posid + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        db.close();
        return count;
    }


    public int getWaiterCount() {
        String selectQuery = "SELECT pid  FROM " + TABLE_WAITER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        db.close();
        return count;

    }

    public int getItemCount(String posId) {
        String posid = "'%" + posId + "%'";
        int count = 0;
        try {

            String selectQuery = "SELECT itemid  FROM " + TABLE_ITEM + " where itemstoreid= " + posId;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            count = cursor.getCount();
            db.close();
        } catch (Exception e) {
            String r = e.getMessage().toString();
        }
        return count;
    }

    public int getModifierCount() {
        String countQuery = "SELECT  * FROM " + TABLE_MODIFIER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        // return count
        return count;
    }

    public int getModifierDetailsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_MODIFIER_DETAILS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count;
    }

    public int getOfferCount() {
        String countQuery = "SELECT  * FROM ITEM_PROMOTION ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count;
    }

    public int getAccountCount(String posId) {
        String posid = "'%" + posId + "%'";
        int count = 0;
        try {

            String selectQuery = "SELECT accountnum  FROM " + TABLE_ACCOUNT + " where itemstoreid like " + posid;
            //  String selectQuery = "SELECT accountnum  FROM " + TABLE_ACCOUNT ;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            count = cursor.getCount();
            db.close();
        } catch (Exception e) {
            String r = e.getMessage().toString();
        }
        return count;
    }

    public Long updateWaiterId(String accountNum, String posId, String waitercode) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PaID, waitercode);
        // int u=db.update(TABLE_ACCOUNT, values, ACCNUM + "=" +"'"+accountNum+"'", null);
        int u = db.update(TABLE_ACCOUNT, values, ACCNUM + "= ? AND " + POSID + "= ?", new String[]{accountNum, posId});
        db.close();
        return (long) u;
    }

    public String getItemPromoid(String param) {

        String Promoid = "";
        String p = param + "%";
        String selectQuery = "";
        selectQuery = "SELECT PromotionId FROM ITEM_PROMOTION where PromotionName like " + "'" + p + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    Promoid = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        }

        // closing connection
        cursor.close();

        db.close();

        return Promoid;
    }

    public Long insertAccount(String loginId, String posId, String accountNum, String tdate, String ctype, String opBalance, String waiternamecode, String accountName, String bill, String billModeType, String acc, String debitbal, String stewardBilling)
    //,String waiterid
    {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db1 = this.getReadableDatabase();
        Long insetcount = null;
        int id = 0;
        String g = "'" + accountNum + "'";
        String pos = "'" + posId + "'";
        String b = "'" + bill + "'";
           String  waiter="'"+waiternamecode+"%'";


        try {
            String selectQuery ="";
            //String selectQuery = "SELECT accountnum FROM " + TABLE_ACCOUNT  +"  where accountnum like "+ g + "and posid like "+pos;
            if (stewardBilling.equalsIgnoreCase("true")){
                  selectQuery = "SELECT accountnum FROM " + TABLE_ACCOUNT + "  where accountnum = " + g + " and posid = " + pos + " and bill_mode = " + b + "and paid = " + waiter;

            }
            else {
                  selectQuery = "SELECT accountnum FROM " + TABLE_ACCOUNT + "  where accountnum = " + g + " and posid = " + pos + "and bill_mode = " + b;

            }
            // String selectQuery = "SELECT accountNumber FROM " + TABLE_ACCOUNT + "  where accountNumber = " + acc + " and posid = " + pos + "and bill_mode = " + b;
            Cursor cursor = db1.rawQuery(selectQuery, null);
            id = cursor.getCount();


            if (id > 0) {
                ContentValues values = new ContentValues();
                values.put(OPENING_BALANCE, opBalance);
                values.put(DEBIT_BALANCE, debitbal);
                if (!stewardBilling.equalsIgnoreCase("true")){
                    values.put(PaID, waiternamecode);

                }

                // int u=db.update(TABLE_ACCOUNT, values, ACCNUM + "=" +"'"+accountNum+"'", null);
                int u = db.update(TABLE_ACCOUNT, values, ACCNUM + "= ? AND " + POSID + "= ?", new String[]{accountNum, posId});
                insetcount = (long) u;
            } else {
                ContentValues values1 = new ContentValues();
                values1.put(LID, loginId);
                values1.put(POSID, posId);
                values1.put(ACCNUM, accountNum);
                values1.put(TODAYSDATE, tdate);
                values1.put(CARDTYPE, ctype);
                values1.put(OPENING_BALANCE, opBalance);
                values1.put(DEBIT_BALANCE, debitbal);
                values1.put(PaID, waiternamecode);
                values1.put(ACC_NAME, accountName);
                values1.put(BILLMODE, bill);
                values1.put(BILLMODE_TYPE, billModeType);
                values1.put(TABLENO, "");
                values1.put(AC_NO, acc);
                values1.put(ISOTD, 0);
                //  values1.put(PaID, waiterid);

                insetcount = db.insert(TABLE_ACCOUNT, null, values1);
            }
            db1.close();
            db.close();
        } catch (Exception e) {
            String r = e.getMessage().toString();
        }
        return insetcount;
    }


    public Long insertAccountnew(String loginId, String posId, String accountNum, String tdate, String ctype, String opBalance, String waiternamecode, String accountName, String bill, String billModeType, String acc, String debitbal)
    //,String waiterid
    {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db1 = this.getReadableDatabase();
        Long insetcount = null;
        int id = 0;
        String g = "'" + accountNum + "'";
        String pos = "'" + posId + "'";
        String b = "'" + bill + "'";
        //  String  waiter=""+waiterid+"%'";


        try {
            //String selectQuery = "SELECT accountnum FROM " + TABLE_ACCOUNT  +"  where accountnum like "+ g + "and posid like "+pos;
            String selectQuery = "SELECT accountnum FROM " + TABLE_ACCOUNT + "  where accountnum = " + g + " and posid = " + pos + "and bill_mode = " + b;
            // String selectQuery = "SELECT accountNumber FROM " + TABLE_ACCOUNT + "  where accountNumber = " + acc + " and posid = " + pos + "and bill_mode = " + b;
            Cursor cursor = db1.rawQuery(selectQuery, null);
            id = cursor.getCount();


            if (id > 0) {
                ContentValues values = new ContentValues();
                values.put(OPENING_BALANCE, opBalance);
                values.put(DEBIT_BALANCE, debitbal);
                values.put(PaID, waiternamecode);

                // int u=db.update(TABLE_ACCOUNT, values, ACCNUM + "=" +"'"+accountNum+"'", null);
                int u = db.update(TABLE_ACCOUNT, values, ACCNUM + "= ? AND " + POSID + "= ?", new String[]{accountNum, posId});
                insetcount = (long) u;
            } else {
                ContentValues values1 = new ContentValues();
                values1.put(LID, loginId);
                values1.put(POSID, posId);
                values1.put(ACCNUM, accountNum);
                values1.put(TODAYSDATE, tdate);
                values1.put(CARDTYPE, ctype);
                values1.put(OPENING_BALANCE, opBalance);
                values1.put(DEBIT_BALANCE, debitbal);
                values1.put(PaID, waiternamecode);
                values1.put(ACC_NAME, accountName);
                values1.put(BILLMODE, bill);
                values1.put(BILLMODE_TYPE, billModeType);
                values1.put(TABLENO, "");
                values1.put(AC_NO, acc);
                values1.put(ISOTD, 0);
                //  values1.put(PaID, waiterid);

                insetcount = db.insert(TABLE_ACCOUNT, null, values1);
            }
            db1.close();
            db.close();
        } catch (Exception e) {
            String r = e.getMessage().toString();
        }
        return insetcount;
    }









   /* public Long insertAccount(String loginId,String posId,String accountNum,String tdate,String ctype,String opBalance)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db1 = this.getReadableDatabase();
        Long insetcount = null;

            ContentValues values1 = new ContentValues();
            values1.put(LID, loginId);
            values1.put(POSID, posId);
            values1.put(ACCNUM, accountNum);
            values1.put(TODAYSDATE, tdate);
            values1.put(CARDTYPE, ctype);
            values1.put(OPENING_BALANCE, opBalance);

            insetcount= db.insert(TABLE_ACCOUNT, null, values1);


        if(insetcount<0)
       { ContentValues values = new ContentValues();
            values.put(OPENING_BALANCE, opBalance);

            int u=db.update(TABLE_ACCOUNT, values, ACCNUM + "=" +"'"+accountNum+"'", null);
            insetcount=(long)u;
        }

            db.close();
        return insetcount;
    }*/

    public ArrayList<Item> getSearchItemlistAllFilter(String itemId) {
        ArrayList<Item> labels = new ArrayList<Item>();
        Item it;


        String p1 = itemId;
        String selectQuery = "", selectQuery1 = "";

        selectQuery = "SELECT DISTINCT icode,iname ,image ,rate,itemstock FROM " + TABLE_ITEM + " where  itemgid like " + p1;

        // selectQuery = "SELECT DISTINCT icode,iname ,ItemImage ,rate FROM " + TABLE_ITEM +" where  itemgid like "  + p1+" ORDER BY iname ASC";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() != 0) {
            // labels.add("Select Items");
            if (cursor.moveToFirst()) {
                do {
                    it = new Item();
                    it.setItemcode(cursor.getString(0));
                    it.setItemName(cursor.getString(1));
                    it.setItemImage(cursor.getString(2));
                    it.setRate(cursor.getString(3));
                    it.setStock(cursor.getString(4));

                    //labels.add(cursor.getString(1) +" " +"_" +" "+ cursor.getString(0)+" " +"_" +" "+ cursor.getString(2));
                    labels.add(it);
                    // labels.add(cursor.getString(0) + "_" + cursor.getString(1));
                } while (cursor.moveToNext());
            }
        } else {
           /* if (cursor2.moveToFirst())
            {
                do
                {
                    labels.add(cursor2.getString(1) + "_" + cursor2.getString(0));
                } while (cursor2.moveToNext());
            }*/
        }

        // closing connection
        cursor.close();
        //  cursor2.close();
        db.close();

        return labels;
    }

    public String getOpeningBalance(String accNo) {
        String g = "'%" + accNo + "%'";
        String gid = "0";
        String selectQuery = "SELECT opBalance  FROM " + TABLE_ACCOUNT + " where accountnum like " + g;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    gid = cursor.getString(0);
                } while (cursor.moveToNext());

            }
        }
        db.close();


        return gid;
    }


    public String getdebitBalance(String accNo) {
        String g = "'%" + accNo + "%'";
        String gid = "0";
        String selectQuery = "SELECT debitBalance  FROM " + TABLE_ACCOUNT + " where accountnum like " + g;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    gid = cursor.getString(0);
                } while (cursor.moveToNext());

            }
        }
        db.close();


        return gid;
    }

    public Long insertStore(ArrayList<MasterStore> s) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;
        for (int i = 0; i < s.size(); i++) {
            ContentValues values1 = new ContentValues();
            values1.put(ID, s.get(i).getID());
            values1.put(CODE, s.get(i).getStoreCode());
            values1.put(NAME, s.get(i).getStoreName());
            values1.put(ICID, s.get(i).getItemCategoryID());
            values1.put(TYPE, s.get(i).getOTType());
            values1.put(CASH, s.get(i).getIsCashSalesRequire());
            values1.put(CREDIT, s.get(i).getIsCreditSalesRequire());
            values1.put(ACCOUNT, s.get(i).getIsDebitSalesRequired());
            values1.put(PARTY, s.get(i).getIsPartyBillApplicable());
            values1.put(PAGE, s.get(i).getDashboard());
            values1.put(ISOT, s.get(i).getIsOTRequired());
            values1.put(MID, s.get(i).getMid());
            values1.put(MSNAME, s.get(i).getMobilestorename());
            values1.put(ISREADER, s.get(i).getIsReader());
            values1.put(ISGUEST, s.get(i).getIsGuest());
            values1.put(ADDRESS, s.get(i).getAddress());
            values1.put(GST, s.get(i).getGSTNumber());
            values1.put(decimal, s.get(i).getIsdecimalval());
            insetcount = db.insert(TABLE_STORE, null, values1);
        }

        db.close();
        return insetcount;
    }

    public Long insertTables(ArrayList<Table> tablename) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;
        for (int i = 0; i < tablename.size(); i++) {
            ContentValues values1 = new ContentValues();
            values1.put(TID, tablename.get(i).getTableId());
            values1.put(TABLENUMBER, tablename.get(i).getTableNmae());
            insetcount = db.insert(TABLE_TABLENAME, null, values1);
        }

        db.close();
        return insetcount;
    }


    public Long insertWaiterGroup(ArrayList<WaiterDetails> waiter) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;
        for (int i = 0; i < waiter.size(); i++) {
            ContentValues values1 = new ContentValues();
            values1.put(PID, waiter.get(i).getpersonid());
            values1.put(PCODE, waiter.get(i).getpersoncode());
            values1.put(PNAME, waiter.get(i).getFirstName());
            insetcount = db.insert(TABLE_WAITER, null, values1);
        }

        db.close();
        return insetcount;
    }


    public Long insertItemGroup(ArrayList<ItemGroup> tablename) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;
        for (int i = 0; i < tablename.size(); i++) {
            ContentValues values1 = new ContentValues();
            values1.put(GID, tablename.get(i).getGroupid());
            values1.put(GCODE, tablename.get(i).getGroupcode());
            values1.put(GNAME, tablename.get(i).getGroupname());
            insetcount = db.insert(TABLE_ITEMGROUP, null, values1);
        }

        db.close();
        return insetcount;
    }

    public Long insertItemGroup(ArrayList<ItemGroup> tablename, String posId) {
        SQLiteDatabase db = this.getWritableDatabase();
        long insetcount = 0;
        for (int i = 0; i < tablename.size(); i++) {
            ContentValues values1 = new ContentValues();
            values1.put(GID, tablename.get(i).getGroupid());
            values1.put(GCODE, tablename.get(i).getGroupcode());
            values1.put(GNAME, tablename.get(i).getGroupname());
            values1.put(PoID, posId);
            // values1.put(GNAME, Item.get(i).getStoreID());

            insetcount = db.insert(TABLE_ITEMGROUP, null, values1);
        }

        db.close();
        return insetcount;
    }

    public Long insertOT(String itemcode, String itemname, String qty) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;

        ContentValues values1 = new ContentValues();
        values1.put(ITEMCODE_OT, itemcode);
        values1.put(ITEMNAME_OT, itemname);
        values1.put(QTY_OT, qty);

        boolean isoffer = checkOffertestone(itemcode, qty, itemname, "");
        insetcount = db.insert(TABLE_OT, null, values1);
        if (insetcount < 0) {
            ContentValues values = new ContentValues();
            values.put(ITEMNAME_OT, itemname);
            values.put(QTY_OT, qty);

            int u = db.update(TABLE_OT, values1, ITEMCODE_OT + "=" + "'" + itemcode + "'", null);
            insetcount = (long) u;
        }
        db.close();
        return insetcount;
    }
    public String checkOffermethodforcancelloption(String itemcode, String Quantity, String itemname, String rate) {
        int i = 0;
        int j = 0;
        String isofferqty = "0";
        //String q = "select FreeItemCode,FreeItemName,FreeQty,PromotionId,Quantity from ITEM_PROMOTION where ItemCode='" + itemcode + "'" + " and Quantity=" + Quantity;
        String q = "select FreeItemCode,FreeItemName,FreeQty,PromotionId,Quantity,ItemID from ITEM_PROMOTION where ItemCode='" + itemcode + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cu = db.rawQuery(q, null);
        if (cu != null) {
            if (cu.moveToFirst()) {
                do {
                    if (j == 0) {
                        if (Integer.parseInt(Quantity) == Integer.parseInt(cu.getString(4))) {

                            isofferqty = cu.getString(2);
                        }


                        else {
                            int p1 = (Integer.parseInt(cu.getString(2)));
                            int p2 = (Integer.parseInt(cu.getString(4)));
                            int p3 = (Integer.parseInt(Quantity));
                            float p4 = p1 * p3 / p2;
                            if (p3 < p2) {

                            } else {
                                if (p4 % 1 == 0) {

                                    isofferqty =  Math.round(p4)+"";

                                }
                            }
                        }
                    }


                } while (cu.moveToNext());
            }

        }
        return isofferqty;
    }


    private boolean checkOffermethod(String itemcode, String Quantity, String itemname, String rate) {
        int i = 0;
        int j = 0;
        boolean isoffer = false;
        //String q = "select FreeItemCode,FreeItemName,FreeQty,PromotionId,Quantity from ITEM_PROMOTION where ItemCode='" + itemcode + "'" + " and Quantity=" + Quantity;
        String q = "select FreeItemCode,FreeItemName,FreeQty,PromotionId,Quantity,ItemID from ITEM_PROMOTION where ItemCode='" + itemcode + "'";
        String query1 = "select " + QTY_OT + " from " + TABLE_OT + " where " + ITEMCODE_OT + " =  '" + itemcode + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cu = db.rawQuery(q, null);
        Cursor cu1 = db.rawQuery(query1, null);


        if (cu1 != null) {
            if (cu1.moveToFirst()) {
                do {
                    String str = (cu1.getString(0));

                    if (str != null) {
                        j = Integer.parseInt((cu1.getString(0)));
                    } else {
                        j = 0;
                    }
                } while (cu1.moveToNext());
            }
        }

        if (cu != null) {
            if (cu.moveToFirst()) {
                do {
                    if (j == 0) {
                        if (Integer.parseInt(Quantity) == Integer.parseInt(cu.getString(4))) {
                            if (i == 0) {
                                db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{itemcode});
                                ContentValues values1 = new ContentValues();
                                values1.put(ITEMCODE_OT, itemcode);
                                values1.put(ITEMNAME_OT, itemname);
                                values1.put(QTY_OT, Quantity);
                                values1.put(TEMP_RATE, rate);
                                values1.put(OID, cu.getString(3));
                                values1.put(OFFERTYPE, "PO");
                                values1.put(PARENTITEMID, cu.getString(5));
                                values1.put(OFFERQTY, cu.getString(4));
                                values1.put(FREEQTY, cu.getString(2));
                                long i1 = db.insert(TABLE_OT, null, values1);

                                db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{("F*" + cu.getString(0))});

                                ContentValues values2 = new ContentValues();
                                values2.put(ITEMCODE_OT, ("F*" + cu.getString(0)));
                                values2.put(ITEMNAME_OT, cu.getString(1));
                                values2.put(QTY_OT, cu.getString(2));
                                values2.put(TEMP_RATE, "0");
                                values2.put(OID, cu.getString(3));
                                values2.put(OCODE, itemcode);
                                values2.put(OFFERTYPE, "CO");
                                values2.put(PARENTITEMID, cu.getString(5));
                                values2.put(OFFERQTY, cu.getString(4));
                                values2.put(FREEQTY, cu.getString(2));
                                long i2 = db.insert(TABLE_OT, null, values2);
                                long i13 = i2;
                            } else {
                                db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{("F*" + (cu.getString(0)))});
                                ContentValues values1 = new ContentValues();
                                values1.put(ITEMCODE_OT, ("F*" + cu.getString(0)));
                                values1.put(ITEMNAME_OT, cu.getString(1));
                                values1.put(QTY_OT, cu.getString(2));
                                values1.put(TEMP_RATE, "0");
                                values1.put(OID, cu.getString(3));
                                values1.put(OCODE, itemcode);
                                values1.put(OFFERTYPE, "CO");
                                values1.put(PARENTITEMID, cu.getString(5));
                                values1.put(OFFERQTY, cu.getString(4));
                                values1.put(FREEQTY, cu.getString(2));
                                long i3 = db.insert(TABLE_OT, null, values1);
                                long i11 = i3;
                            }
                            isoffer = true;
                        }

                   /* else if(j==0) {
                        int p1 = (Integer.parseInt(cu.getString(2)));
                        int p2 = (Integer.parseInt(cu.getString(4)));
                        int p3=(Integer.parseInt(Quantity));
                        float p4=p1*p3/p2;
                      if (p4 % 1 == 0){
                          ContentValues values1 = new ContentValues();
                          values1.put(ITEMCODE_OT, cu.getString(0));
                          values1.put(ITEMNAME_OT, cu.getString(1));
                          values1.put(QTY_OT, p4);
                          values1.put(TEMP_RATE, "0");
                          values1.put(OID, cu.getString(3));
                          values1.put(OCODE, itemcode);
                          values1.put(OFFERTYPE, "PO");
                          db.insert(TABLE_OT, null, values1);
                      }
                    }*/
                        else {
                            int p1 = (Integer.parseInt(cu.getString(2)));
                            int p2 = (Integer.parseInt(cu.getString(4)));
                            int p3 = (Integer.parseInt(Quantity));
                            float p4 = p1 * p3 / p2;
                            if (p3 < p2) {

                            } else {
                                if (p4 % 1 == 0) {
                                    ContentValues values1 = new ContentValues();
                                    values1.put(ITEMCODE_OT, ("F*" + cu.getString(0)));
                                    values1.put(ITEMNAME_OT, cu.getString(1));
                                    //   values1.put(QTY_OT, Integer.parseInt((p4+"")));

                                    values1.put(QTY_OT, Math.round(p4));
                                    values1.put(TEMP_RATE, "0");
                                    values1.put(OID, cu.getString(3));
                                    values1.put(OCODE, itemcode);
                                    values1.put(OFFERTYPE, "CO");
                                    values1.put(PARENTITEMID, cu.getString(5));
                                    values1.put(OFFERQTY, cu.getString(4));
                                    values1.put(FREEQTY, cu.getString(2));
                                    long x = db.insert(TABLE_OT, null, values1);
                                    if (x < 0) {
                                        ContentValues values = new ContentValues();
                                        values.put(ITEMNAME_OT, cu.getString(1));
                                        values.put(QTY_OT, Math.round(p4));
                                        values.put(OFFERTYPE, "CO");
                                        values.put(PARENTITEMID, cu.getString(5));
                                        int u = db.update(TABLE_OT, values, ITEMCODE_OT + "=" + "'" + ("F*" + cu.getString(0)) + "'", null);
                                        x = (long) u;
                                    }
                                }
                            }










               /*         db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{itemcode});
                        ContentValues values1 = new ContentValues();
                        values1.put(ITEMCODE_OT, itemcode);
                        values1.put(ITEMNAME_OT, itemname);
                        values1.put(QTY_OT, Quantity);
                        values1.put(TEMP_RATE, rate);
                        values1.put(OID, cu.getString(3));
                        values1.put(OFFERTYPE, "PO");
                        db.insert(TABLE_OT, null, values1);*/
                        }
                    } else {


                        int p1 = (Integer.parseInt(cu.getString(2)));
                        int p2 = (Integer.parseInt(cu.getString(4)));
                        int p3 = (Integer.parseInt(Quantity) + j);
                        float p4 = p1 * p3 / p2;


                        if (p3 < p2) {

                        } else {
                            if (p4 % 1 == 0) {
                                ContentValues values1 = new ContentValues();
                                values1.put(ITEMCODE_OT, ("F*" + cu.getString(0)));
                                values1.put(ITEMNAME_OT, cu.getString(1));
                                //   values1.put(QTY_OT, Integer.parseInt((p4+"")));

                                values1.put(QTY_OT, Math.round(p4));
                                values1.put(TEMP_RATE, "0");
                                values1.put(OID, cu.getString(3));
                                values1.put(OCODE, itemcode);
                                values1.put(OFFERTYPE, "CO");
                                values1.put(PARENTITEMID, cu.getString(5));
                                values1.put(OFFERQTY, cu.getString(4));
                                values1.put(FREEQTY, cu.getString(2));

                                long x = db.insert(TABLE_OT, null, values1);
                                if (x < 0) {
                                    ContentValues values = new ContentValues();
                                    values.put(ITEMNAME_OT, cu.getString(1));
                                    values.put(QTY_OT, Math.round(p4));
                                    values.put(OFFERTYPE, "CO");
                                    values.put(PARENTITEMID, cu.getString(5));
                                    int u = db.update(TABLE_OT, values, ITEMCODE_OT + "=" + "'" + ("F*" + cu.getString(0)) + "'", null);
                                    x = (long) u;
                                }
                            }
                        }


                    }


                } while (cu.moveToNext());
            }

        }
        return isoffer;
    }

      public String  checkOffermethodCancelOrder(String itemcode, String Quantity, String itemname, String rate) {
        int i = 0;
        int j = 0;
        boolean isoffer = false;
        String isofferqty = "0";
        //String q = "select FreeItemCode,FreeItemName,FreeQty,PromotionId,Quantity from ITEM_PROMOTION where ItemCode='" + itemcode + "'" + " and Quantity=" + Quantity;
        String q = "select FreeItemCode,FreeItemName,FreeQty,PromotionId,Quantity,ItemID from ITEM_PROMOTION where ItemCode='" + itemcode + "'";
        String query1 = "select " + QTY_OT + " from " + TABLE_OT + " where " + ITEMCODE_OT + " =  '" + itemcode + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cu = db.rawQuery(q, null);
        Cursor cu1 = db.rawQuery(query1, null);


        if (cu1 != null) {
            if (cu1.moveToFirst()) {
                do {
                    String str = (cu1.getString(0));

                    if (str != null) {
                        j = Integer.parseInt((cu1.getString(0)));
                    } else {
                        j = 0;
                    }
                } while (cu1.moveToNext());
            }
        }

        if (cu != null) {
            if (cu.moveToFirst()) {
                do {
                    if (j == 0) {
                        if (Integer.parseInt(Quantity) == Integer.parseInt(cu.getString(4))) {
                            if (i == 0) {
                                isofferqty=cu.getString(2);

                            } else {
                                isofferqty=cu.getString(2);

                            }
                         }
                        else {
                            int p1 = (Integer.parseInt(cu.getString(2)));
                            int p2 = (Integer.parseInt(cu.getString(4)));
                            int p3 = (Integer.parseInt(Quantity));
                            float p4 = p1 * p3 / p2;
                            if (p3 < p2) {

                            } else {
                                if (p4 % 1 == 0) {

                                    isofferqty= Math.round(p4)+"" ;

                                }
                            }
                        }
                    } else {
                        int p1 = (Integer.parseInt(cu.getString(2)));
                        int p2 = (Integer.parseInt(cu.getString(4)));
                        int p3 = (Integer.parseInt(Quantity) + j);
                        float p4 = p1 * p3 / p2;


                        if (p3 < p2) {

                        } else {
                            if (p4 % 1 == 0) {

                                isofferqty=Math.round(p4)+"";

                            }
                        }


                    }


                } while (cu.moveToNext());
            }

        }
        return isofferqty;
    }
    public Long insertOTtrail(String itemcode, String itemname, String qty, String rate) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;

        ContentValues values1 = new ContentValues();
        values1.put(ITEMCODE_OT, itemcode);
        values1.put(TEMP_RATE, rate);
        values1.put(ITEMNAME_OT, itemname);
        values1.put(QTY_OT, qty);

        boolean isoffer = checkOffertestone(itemcode, qty, itemname, rate);

        insetcount = db.insert(TABLE_OT, null, values1);
        if (insetcount < 0) {
            ContentValues values = new ContentValues();
            values.put(ITEMNAME_OT, itemname);
            values.put(QTY_OT, qty);

            int u = db.update(TABLE_OT, values1, ITEMCODE_OT + "=" + "'" + itemcode + "'", null);
            insetcount = (long) u;
        }
        db.close();
        return insetcount;
    }

    public Long insertOT_tempHappyHour(String itemcode, String itemname, String happyHourstaus) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;

        ContentValues values1 = new ContentValues();
        values1.put(ITEMCODE_OT, itemcode);
        values1.put(ITEMNAME_OT, itemname);
        values1.put(TEMP_HAPPYHOUR, happyHourstaus);


        insetcount = db.insert(TABLE_OT, null, values1);
        if (insetcount < 0) {
            ContentValues values = new ContentValues();
            values.put(TEMP_HAPPYHOUR, happyHourstaus);

            int u = db.update(TABLE_OT, values, ITEMCODE_OT + "=" + "'" + itemcode + "'", null);
            insetcount = (long) u;
        }
        db.close();
        return insetcount;
    }

    public Long insertOT_narration(String itemcode, String itemname, String narration, String narrationid) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;

        ContentValues values1 = new ContentValues();
        values1.put(ITEMCODE_OT, itemcode);
        values1.put(ITEMNAME_OT, itemname);
        values1.put(NARRATION, narration);
        values1.put(NARRATION_ID, narrationid);

        insetcount = db.insert(TABLE_OT, null, values1);
        if (insetcount < 0) {
            ContentValues values = new ContentValues();
            // values.put(ITEMNAME_OT, itemname);
            values.put(NARRATION, narration);
            values.put(NARRATION_ID, narrationid);

            int u = db.update(TABLE_OT, values1, ITEMCODE_OT + "=" + "'" + itemcode + "'", null);
            insetcount = (long) u;
        }
        db.close();
        return insetcount;
    }

    public List<String> getTempOrderlist() {
        List<String> orderlist = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String q = "select itemcode_ot,itemname_ot,qty_ot,rate_ot from OT ";
        Cursor cu = db.rawQuery(q, null);
        if (cu != null) {
            if (cu.moveToFirst()) {
                do {
                    orderlist.add(cu.getString(0) + "#" + cu.getString(1) + "#" + cu.getString(2) + "#" + cu.getString(3));
                } while (cu.moveToNext());

            }
        }
        cu.close();
        db.close();
        return orderlist;
    }

    public ArrayList<String> getNarrationlist() {
        ArrayList<String> orderlist = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String q = "select NarrationDescription from NARRATION_TAX ";
        Cursor cu = db.rawQuery(q, null);
        if (cu != null) {
            if (cu.moveToFirst()) {
                do {
                    orderlist.add(cu.getString(0));
                } while (cu.moveToNext());

            }
        }
        cu.close();
        db.close();
        return orderlist;
    }

    public long insertOT1(String itemcode, String itemname, String qty, String rate) {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db1 = this.getReadableDatabase();
        long insetcount = 0l;
        String iCode = "'" + itemcode + "'";
        String qt = "";
        String qq = "";
        boolean isoffer = checkOffermethod(itemcode, qty, itemname, rate);// IF OFFER IS THERE EXISTING ITEM AND OFFER WILLL DELETE
        if (!isoffer)//IF OFFER NOT THERE EXISTING OFFER WILL DELETE AND EXISTING ITEM WILL UPDATE
        {
            ContentValues values1 = new ContentValues();
            values1.put(ITEMCODE_OT, itemcode);
            values1.put(ITEMNAME_OT, itemname);
            values1.put(QTY_OT, qty);
            values1.put(TEMP_RATE, rate);
            values1.put(OID, 0);
            values1.put(OFFERTYPE, "NO");
            insetcount = db.insert(TABLE_OT, null, values1);
            if (insetcount < 0) {
                String q = "select qty_ot from OT where itemcode_ot like " + iCode;
                Cursor cu = db1.rawQuery(q, null);
                if (cu != null) {
                    if (cu.moveToFirst()) {
                        do {
                            qq = cu.getString(0);
                        } while (cu.moveToNext());
                    }

                }
                qt = Integer.toString(Integer.parseInt(qq) + Integer.parseInt(qty));
                ContentValues values = new ContentValues();
                values.put(ITEMNAME_OT, itemname);
                if (!qt.equals("")) {
                    values.put(QTY_OT, qt);
                } else {
                    values.put(QTY_OT, qty);
                }
                int u = db.update(TABLE_OT, values, ITEMCODE_OT + "=" + "'" + itemcode + "'", null);
                insetcount = (long) u;

            }
            //monica
           /* else {
                db.delete(TABLE_OT, OCODE + "= ?", new String[]{itemcode});
            }*/
        } else {
            insetcount = 1;
        }
        db.close();
        return insetcount;
    }


    private boolean checkOffer(String itemcode, String Quantity, String itemname, String rate) {
        int i = 0;
        boolean isoffer = false;
        //String q = "select FreeItemCode,FreeItemName,FreeQty,PromotionId,Quantity from ITEM_PROMOTION where ItemCode='" + itemcode + "'" + " and Quantity=" + Quantity;
        String q = "select FreeItemCode,FreeItemName,FreeQty,PromotionId,Quantity from ITEM_PROMOTION where ItemCode='" + itemcode + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cu = db.rawQuery(q, null);
        if (cu != null) {
            if (cu.moveToFirst()) {
                do {
                    if (Integer.parseInt(Quantity) == Integer.parseInt(cu.getString(4))) {
                        if (i == 0) {
                            db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{itemcode});
                            ContentValues values1 = new ContentValues();
                            values1.put(ITEMCODE_OT, itemcode);
                            values1.put(ITEMNAME_OT, itemname);
                            values1.put(QTY_OT, Quantity);
                            values1.put(TEMP_RATE, rate);
                            values1.put(OID, cu.getString(3));
                            values1.put(OFFERTYPE, "PO");
                            db.insert(TABLE_OT, null, values1);

                            db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{cu.getString(0)});
                            ContentValues values2 = new ContentValues();
                            values2.put(ITEMCODE_OT, cu.getString(0));
                            values2.put(ITEMNAME_OT, cu.getString(1));
                            values2.put(QTY_OT, cu.getString(2));
                            values2.put(TEMP_RATE, "0");
                            values2.put(OID, cu.getString(3));
                            values2.put(OCODE, itemcode);
                            values2.put(OFFERTYPE, "CO");
                            db.insert(TABLE_OT, null, values2);
                        } else {
                            db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{cu.getString(0)});
                            ContentValues values1 = new ContentValues();
                            values1.put(ITEMCODE_OT, cu.getString(0));
                            values1.put(ITEMNAME_OT, cu.getString(1));
                            values1.put(QTY_OT, cu.getString(2));
                            values1.put(TEMP_RATE, "0");
                            values1.put(OID, cu.getString(3));
                            values1.put(OCODE, itemcode);
                            values1.put(OFFERTYPE, "CO");
                            db.insert(TABLE_OT, null, values1);
                        }
                        isoffer = true;
                    }
                  /*  else
                    {

                        db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{itemcode});
                        ContentValues values1 = new ContentValues();
                        values1.put(ITEMCODE_OT, itemcode);
                        values1.put(ITEMNAME_OT, itemname);
                        values1.put(QTY_OT, Quantity);
                        values1.put(TEMP_RATE, rate);
                        values1.put(OID, cu.getString(3));
                        values1.put(OFFERTYPE, "PO");
                        db.insert(TABLE_OT, null, values1);
                    }*/

                } while (cu.moveToNext());
            }

        }
        return isoffer;
    }

    public Long insertOTTest(String itemcode, String itemname, String qty, String rate) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;

        ContentValues values1 = new ContentValues();
        values1.put(ITEMCODE_OT, itemcode);
        values1.put(ITEMNAME_OT, itemname);
        values1.put(QTY_OT, qty);
        boolean isoffer = checkOffertestone(itemcode, qty, itemname, rate);

        insetcount = db.insert(TABLE_OT, null, values1);
        if (insetcount < 0) {
            ContentValues values = new ContentValues();
            values.put(ITEMNAME_OT, itemname);
            values.put(QTY_OT, qty);
            int u = db.update(TABLE_OT, values1, ITEMCODE_OT + "=" + "'" + itemcode + "'", null);
            insetcount = (long) u;
        }
        db.close();
        return insetcount;
    }


    private boolean checkOffertestone(String itemcode, String Quantity, String itemname, String rate) {
        int i = 0;
        int j = 0;
        boolean isoffer = false;
        //String q = "select FreeItemCode,FreeItemName,FreeQty,PromotionId,Quantity from ITEM_PROMOTION where ItemCode='" + itemcode + "'" + " and Quantity=" + Quantity;
        String q = "select FreeItemCode,FreeItemName,FreeQty,PromotionId,Quantity from ITEM_PROMOTION where ItemCode='" + itemcode + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cu = db.rawQuery(q, null);


        if (cu != null) {
            if (cu.moveToFirst()) {
                do {
                    if (Integer.parseInt(Quantity) == Integer.parseInt(cu.getString(4))) {
                        if (i == 0) {
                            db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{itemcode});
                            ContentValues values1 = new ContentValues();
                            values1.put(ITEMCODE_OT, itemcode);
                            values1.put(ITEMNAME_OT, itemname);
                            values1.put(QTY_OT, Quantity);
                            values1.put(TEMP_RATE, rate);
                            values1.put(OID, cu.getString(3));
                            values1.put(OFFERTYPE, "PO");
                            long i1 = db.insert(TABLE_OT, null, values1);

                            db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{("F*" + cu.getString(0))});

                            ContentValues values2 = new ContentValues();
                            values2.put(ITEMCODE_OT, ("F*" + cu.getString(0)));
                            values2.put(ITEMNAME_OT, cu.getString(1));
                            values2.put(QTY_OT, cu.getString(2));
                            values2.put(TEMP_RATE, "0");
                            values2.put(OID, cu.getString(3));
                            values2.put(OCODE, itemcode);
                            values2.put(OFFERTYPE, "CO");
                            long i2 = db.insert(TABLE_OT, null, values2);
                            long i13 = i2;
                        } else {
                            db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{("F*" + (cu.getString(0)))});
                            ContentValues values1 = new ContentValues();
                            values1.put(ITEMCODE_OT, ("F*" + cu.getString(0)));
                            values1.put(ITEMNAME_OT, cu.getString(1));
                            values1.put(QTY_OT, cu.getString(2));
                            values1.put(TEMP_RATE, "0");
                            values1.put(OID, cu.getString(3));
                            values1.put(OCODE, itemcode);
                            values1.put(OFFERTYPE, "CO");
                            long i3 = db.insert(TABLE_OT, null, values1);
                            long i11 = i3;
                        }
                        isoffer = true;
                    }

                   /* else if(j==0) {
                        int p1 = (Integer.parseInt(cu.getString(2)));
                        int p2 = (Integer.parseInt(cu.getString(4)));
                        int p3=(Integer.parseInt(Quantity));
                        float p4=p1*p3/p2;
                      if (p4 % 1 == 0){
                          ContentValues values1 = new ContentValues();
                          values1.put(ITEMCODE_OT, cu.getString(0));
                          values1.put(ITEMNAME_OT, cu.getString(1));
                          values1.put(QTY_OT, p4);
                          values1.put(TEMP_RATE, "0");
                          values1.put(OID, cu.getString(3));
                          values1.put(OCODE, itemcode);
                          values1.put(OFFERTYPE, "PO");
                          db.insert(TABLE_OT, null, values1);
                      }
                    }*/
                    else {

                        int p1 = (Integer.parseInt(cu.getString(2)));
                        int p2 = (Integer.parseInt(cu.getString(4)));
                        int p3 = (Integer.parseInt(Quantity));
                        float p4 = p1 * p3 / p2;

                        if (p3 < p2) {
                            if (p4 % 1 == 0) {
                                ContentValues values1 = new ContentValues();
                                values1.put(ITEMCODE_OT, ("F*" + cu.getString(0)));
                                values1.put(ITEMNAME_OT, cu.getString(1));
                                //   values1.put(QTY_OT, Integer.parseInt((p4+"")));

                                values1.put(QTY_OT, "0");
                                values1.put(TEMP_RATE, "0");
                                values1.put(OID, cu.getString(3));
                                values1.put(OCODE, itemcode);
                                values1.put(OFFERTYPE, "CO");
                                long x = db.insert(TABLE_OT, null, values1);
                                if (x < 0) {
                                    ContentValues values = new ContentValues();
                                    values.put(ITEMNAME_OT, cu.getString(1));
                                    values.put(QTY_OT, "0");
                                    values.put(OFFERTYPE, "CO");
                                    int u = db.update(TABLE_OT, values, ITEMCODE_OT + "=" + "'" + ("F*" + cu.getString(0)) + "'", null);
                                    x = (long) u;
                                }
                            }

                        } else if (p3 % p2 != 0) {


                        } else {
                            if (p4 % 1 == 0) {
                                ContentValues values1 = new ContentValues();
                                values1.put(ITEMCODE_OT, ("F*" + cu.getString(0)));
                                values1.put(ITEMNAME_OT, cu.getString(1));
                                //   values1.put(QTY_OT, Integer.parseInt((p4+"")));

                                values1.put(QTY_OT, Math.round(p4));
                                values1.put(TEMP_RATE, "0");
                                values1.put(OID, cu.getString(3));
                                values1.put(OCODE, itemcode);
                                values1.put(OFFERTYPE, "CO");
                                long x = db.insert(TABLE_OT, null, values1);
                                if (x < 0) {
                                    ContentValues values = new ContentValues();
                                    values.put(ITEMNAME_OT, cu.getString(1));
                                    values.put(QTY_OT, Math.round(p4));
                                    values.put(OFFERTYPE, "CO");
                                    int u = db.update(TABLE_OT, values, ITEMCODE_OT + "=" + "'" + ("F*" + cu.getString(0)) + "'", null);
                                    x = (long) u;
                                }
                            }
                        }





               /*         db.delete(TABLE_OT, ITEMCODE_OT + "= ?", new String[]{itemcode});
                        ContentValues values1 = new ContentValues();
                        values1.put(ITEMCODE_OT, itemcode);
                        values1.put(ITEMNAME_OT, itemname);
                        values1.put(QTY_OT, Quantity);
                        values1.put(TEMP_RATE, rate);
                        values1.put(OID, cu.getString(3));
                        values1.put(OFFERTYPE, "PO");
                        db.insert(TABLE_OT, null, values1);*/
                    }
                } while (cu.moveToNext());
            }

        }
        return isoffer;
    }


    public Long insertOT_bak(String itemcode, String itemname, String qty, String discount) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;

        ContentValues values1 = new ContentValues();
        values1.put(ITEMCODE_OT, itemcode);
        values1.put(ITEMNAME_OT, itemname);
        values1.put(QTY_OT, qty);
        values1.put(DISCOUNT, discount);


        insetcount = db.insert(TABLE_OT, null, values1);
        if (insetcount < 0) {
            ContentValues values = new ContentValues();
            values.put(ITEMNAME_OT, itemname);
            values.put(QTY_OT, qty);
            values1.put(DISCOUNT, discount);

            int u = db.update(TABLE_OT, values1, ITEMCODE_OT + "=" + "'" + itemcode + "'", null);
            insetcount = (long) u;
        }
        db.close();
        return insetcount;
    }

    public long InsertItemtax(String ItemID, String TaxID, String TaxCode, String Description, String Value, String Type, String CreatedDate, String Modifieddate, String IsTaxApplicable, String issubtax, String Parentid, String taxtype) {
        long l = 0;
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("ItemID", ItemID);
        contentValues.put("TaxID", TaxID);
        contentValues.put("TaxCode", TaxCode);
        contentValues.put("Description", Description);
        contentValues.put("Value", Value);
        contentValues.put("Type", Type);
        contentValues.put("CreatedDate", CreatedDate);
        contentValues.put("Modifieddate", Modifieddate);
        contentValues.put("IsTaxApplicable", IsTaxApplicable);
        contentValues.put("issubtax", issubtax);
        contentValues.put("Parentid", Parentid);
        contentValues.put("taxtype", taxtype);
        l = database.insert("ITEMS_TAX", null, contentValues);
        database.close();
        return l;
    }

    public void deleteModsId(String ID) {
        String where = "ItemId=?";
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("MODIFIER", where, new String[]{ID});
    }

    public void deleteAllenId(String ID) {
        String where = "ItemId=?";
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("ALLERGEN", where, new String[]{ID});
    }

    public int insertMods(ModifiersDto modifiersDto, String id, String name) {

        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Id", modifiersDto.getModifierId());
        contentValues.put("dId", modifiersDto.getModifierDetailId());
        contentValues.put("ItemId", id);
        contentValues.put("ItemName", name);
        contentValues.put("Name", modifiersDto.getModifierName());
        contentValues.put("dName", modifiersDto.getModifierDetailName());
        contentValues.put("FreeFlow", modifiersDto.getFreeFlow());

        long l = database.insert("MODIFIER", null, contentValues);
        database.close();
        return (int) l;
    }

    public int insertAllen(String id, String name) {

        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("ItemId", id);
        contentValues.put("Name", name);

        long l = database.insert("ALLERGEN", null, contentValues);
        database.close();
        return (int) l;
    }

/*    public float getItemTax(String itemid, String rate, String qty, String discount, boolean isIGST) {

        String selectQuery = "";
        //String selectQuery = "SELECT  (sum(Value)*"+rate+")/100 as Taxamount FROM ITEMS_TAX where ItemID="+itemid;

        selectQuery = "SELECT  (((" + qty + "*" + rate + ")-" + discount + ")*" + "sum(Value))/100 as Taxamount FROM ITEMS_TAX where ItemID=" + itemid + " and Type='S'" + " and issubtax='0'";
        float price = 0;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        //float percentage =Float.parseFloat(cursor.getString(0));
                        // price+=(percentage*Float.parseFloat(rate))/100;
                        String taxamount = cursor.getString(0);
                        if (taxamount != null) {
                            price = Float.parseFloat(taxamount);
                        }

                    } while (cursor.moveToNext());
                }
            }
            // closing connection
            cursor.close();
            db.close();
        } catch (Exception e) {
            price = 0;
        }
        return price;

    }
    */

    public float getItemTax(String itemid, String rate, String qty, String discount, boolean isIGST) {

        String selectQuery = "";
        //String selectQuery = "SELECT  (sum(Value)*"+rate+")/100 as Taxamount FROM ITEMS_TAX where ItemID="+itemid;

        selectQuery = "SELECT  (((" + qty + "*" + rate + ")-" + discount + ")*" + "sum(Value))/100 as Taxamount FROM ITEMS_TAX where ItemID=" + itemid + " and Type='S'" + " and issubtax='0'" + " and taxtype='P'";
        float price = 0;
        float pricef = 0;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        //float percentage =Float.parseFloat(cursor.getString(0));
                        // price+=(percentage*Float.parseFloat(rate))/100;
                        String taxamount = cursor.getString(0);
                        if (taxamount != null) {
                            price = Float.parseFloat(taxamount);
                        }

                    } while (cursor.moveToNext());
                }
            }
            // closing connection
            cursor.close();

            String selectQueryf = "SELECT  (" + qty + "*" + " sum(Value) ) as Taxamount FROM ITEMS_TAX where ItemID=" + itemid + " and Type='S'" + " and issubtax='0'" + " and taxtype='F'";


            SQLiteDatabase dbf = this.getReadableDatabase();
            Cursor cursorf = db.rawQuery(selectQueryf, null);

            if (cursorf.getCount() != 0) {
                if (cursorf.moveToFirst()) {
                    do {
                        //float percentage =Float.parseFloat(cursor.getString(0));
                        // price+=(percentage*Float.parseFloat(rate))/100;
                        String taxamountf = cursorf.getString(0);
                        if (taxamountf != null) {
                            pricef = Float.parseFloat(taxamountf);
                        }

                    } while (cursorf.moveToNext());
                }
            }
            // closing connection
            cursorf.close();
            db.close();

        } catch (Exception e) {
            pricef = 0;
            price = 0;
        }
        return (price + pricef);

    }

    public float getItemSubTax(String itemid, String rate, String qty, String discount, boolean isIGST) {

        String selectQuery = "";
        String selectQuery1 = "";
        //String selectQuery = "SELECT  (sum(Value)*"+rate+")/100 as Taxamount FROM ITEMS_TAX where ItemID="+itemid;
        float price = 0;
        float price1 = 0;
        String taxId = "0";
        selectQuery = "SELECT  (((" + qty + "*" + rate + ")-" + discount + ")*" + "sum(Value))/100 as Taxamount,TaxID FROM ITEMS_TAX where ItemID=" + itemid + " and Type='S'" + " and IsTaxApplicable='True'";
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        //float percentage =Float.parseFloat(cursor.getString(0));
                        // price+=(percentage*Float.parseFloat(rate))/100;
                        String taxamount = cursor.getString(0);
                        taxId = cursor.getString(1);
                        if (taxamount != null) {
                            price = Float.parseFloat(taxamount);
                        }
                    } while (cursor.moveToNext());
                }
            }
            // closing connection
            cursor.close();
            db.close();
        } catch (Throwable e) {
            price = 0;
        }
        selectQuery1 = "SELECT  " + price + "*" + "sum(Value)/100 as Taxamount FROM ITEMS_TAX where ItemID=" + itemid + " and Type='S'" + " and issubtax='1'" + " and Parentid= '" + taxId + "'";
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery1, null);

            if (cursor.getCount() != 0) {
                if (cursor.moveToFirst()) {
                    do {
                        //float percentage =Float.parseFloat(cursor.getString(0));
                        // price+=(percentage*Float.parseFloat(rate))/100;
                        String taxamount = cursor.getString(0);
                        if (taxamount != null) {
                            price1 = Float.parseFloat(taxamount);
                        }
                    } while (cursor.moveToNext());
                }
            }
            // closing connection
            cursor.close();
            db.close();
        } catch (Throwable e) {
            price1 = 0;
        }
        return price1;

    }

    public long getTaxCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, "ITEMS_TAX");
        db.close();
        return count;
    }

    public long getModCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, "MODIFIER");
        db.close();
        return count;
    }

    public long getAllenCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, "ALLERGEN");
        db.close();
        return count;
    }

    public boolean clearTable(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        int did = db.delete(tableName, null, null);
        db.close();
        return (did > 0) ? true : false;
        //return db.delete("TEMP_ORDER","id = ? ",new String[] { Integer.toString(id) });
    }

    public ArrayList<String> getOTItemList() {

        ArrayList<String> list = new ArrayList<>();
        //String selectQuery = "SELECT itemcode_ot,itemname_ot,qty_ot,rate_ot  FROM " + TABLE_OT + " ORDER BY itemname_ot ASC";
        String selectQuery = "SELECT itemcode_ot,itemname_ot,qty_ot,rate_ot,id,offertype  FROM " + TABLE_OT + " ORDER BY id ASC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String compare = "";
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
//                    String alpabet=cursor.getString(1).toUpperCase().substring(0,1);
//                    if(compare.equals(""))
//                    {   compare=alpabet;
//                        list.add(alpabet);
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
//                    else if(compare.equals(alpabet))
//                    {
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
//                    else
//                    {
//                        alpabet=cursor.getString(1).toUpperCase().substring(0,1);
//                        compare=alpabet;
//                        list.add(alpabet);
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
                    list.add(cursor.getString(0) + "-" + cursor.getString(1) + "$" + cursor.getString(2) + "$" + cursor.getString(3) + "$" + cursor.getString(4) + "$" + cursor.getString(5));
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return list;
    }

    public ArrayList<String> getOTItemListNew() {

        ArrayList<String> list = new ArrayList<>();

        //String selectQuery = "SELECT itemcode_ot,itemname_ot,qty_ot,rate_ot  FROM " + TABLE_OT + " ORDER BY itemname_ot ASC";
        String selectQuery = "SELECT itemcode_ot,itemname_ot,qty_ot,rate_ot,id,offertype,parentitemid,offerqty,freeqty  FROM " + TABLE_OT + " ORDER BY id ASC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String compare = "";
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    list.add(cursor.getString(0) + "#" + cursor.getString(1) + "$" + cursor.getString(2) + "$" + cursor.getString(3) + "$" + cursor.getString(4) + "$" + cursor.getString(5)+ "$" + cursor.getString(6)+ "$" + cursor.getString(7)+ "$" + cursor.getString(8));

              /*      String selectmodQuery = "SELECT dId,Name,dName,FreeFlow   FROM MODIFIER" + " where ItemId='" + cursor.getString(0) + "'";
                    SQLiteDatabase db1 = this.getReadableDatabase();
                    Cursor cursor1 = db1.rawQuery(selectmodQuery, null);

                    if (cursor1 != null) {
                        if (cursor1.moveToFirst()) {
                            do {
                                if (cursor1.getString(0).equals("0")) {
                                    list.add(cursor1.getString(3)+ "$" + "0" + "$" + "Modifier" );
                                    Log.d("Cursor Values : ", cursor1.getString(3));
                                } else {
                                    list.add(  cursor1.getString(1) + " " + cursor1.getString(2)+ "$" + "0" + "$" + "Modifier");
                                    Log.d("Cursor Values : ", cursor1.getString(0) + cursor1.getString(1));

                                }

                            } while (cursor1.moveToNext());
                        }
                    }
*/
//                    String alpabet=cursor.getString(1).toUpperCase().substring(0,1);
//                    if(compare.equals(""))
//                    {   compare=alpabet;
//                        list.add(alpabet);
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
//                    else if(compare.equals(alpabet))
//                    {
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
//                    else
//                    {
//                        alpabet=cursor.getString(1).toUpperCase().substring(0,1);
//                        compare=alpabet;
//                        list.add(alpabet);
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return list;
    }

    public ArrayList<String> getOTItemList_bak() {

        ArrayList<String> list = new ArrayList<>();

        String selectQuery = "SELECT itemcode_ot,itemname_ot,qty_ot,discount  FROM " + TABLE_OT + " ORDER BY itemname_ot ASC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String compare = "";
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
//                    String alpabet=cursor.getString(1).toUpperCase().substring(0,1);
//                    if(compare.equals(""))
//                    {   compare=alpabet;
//                        list.add(alpabet);
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
//                    else if(compare.equals(alpabet))
//                    {
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
//                    else
//                    {
//                        alpabet=cursor.getString(1).toUpperCase().substring(0,1);
//                        compare=alpabet;
//                        list.add(alpabet);
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
                    list.add(cursor.getString(0) + "-" + cursor.getString(1) + "$" + cursor.getString(2) + "$" + cursor.getString(3));
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return list;
    }

    public Long insertItem(ArrayList<Item> tablename) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;
        for (int i = 0; i < tablename.size(); i++) {
            ContentValues values1 = new ContentValues();
            values1.put(ICATEGORYID, tablename.get(i).getItemCategoryID());
            values1.put(ICODE, tablename.get(i).getItemcode());
            values1.put(IIDENTITYID, tablename.get(i).getItemIdentityID());
            //  values1.put(DESCRIPTION  , tablename.get(i).getDescription());
            values1.put(IGID, tablename.get(i).getItemGroupID());
            values1.put(INAME, tablename.get(i).getItemName());
            values1.put(SID, tablename.get(i).getStoreID());
            //  values1.put(TAX  , tablename.get(i).getTax());
            values1.put(UNITID, tablename.get(i).getUnitID());
            //  values1.put(TaxID  , tablename.get(i).getTaxID());
            values1.put(RATE, tablename.get(i).getRate());
            values1.put(ITEMIMAGE, tablename.get(i).getItemImage());

            insetcount = db.insert(TABLE_ITEM, null, values1);
        }

        db.close();
        return insetcount;
    }

    public Long insertItemstock(ArrayList<Item> tablename) {
        SQLiteDatabase db = this.getWritableDatabase();
        Long insetcount = null;
        for (int i = 0; i < tablename.size(); i++) {
            ContentValues values1 = new ContentValues();
            values1.put(ICATEGORYID, tablename.get(i).getItemCategoryID());
            values1.put(ICODE, tablename.get(i).getItemcode());
            values1.put(IIDENTITYID, tablename.get(i).getItemIdentityID());
            //  values1.put(DESCRIPTION  , tablename.get(i).getDescription());
            values1.put(IGID, tablename.get(i).getItemGroupID());
            values1.put(INAME, tablename.get(i).getItemName());
            values1.put(SID, tablename.get(i).getStoreID());
            //  values1.put(TAX  , tablename.get(i).getTax());
            values1.put(UNITID, tablename.get(i).getUnitID());
            //  values1.put(TaxID  , tablename.get(i).getTaxID());
            values1.put(RATE, tablename.get(i).getRate());
            values1.put(ITEMIMAGE, tablename.get(i).getItemImage());
            values1.put(ITEMSTOCK, tablename.get(i).getStock());

            insetcount = db.insert(TABLE_ITEM, null, values1);
        }

        db.close();
        return insetcount;
    }

    //select

    public ArrayList<MasterStore> getStoreList() {
        String selectQuery = "SELECT sId,sName,icategoryid,type,cash,credit,account,party,page,isOT,msid,msname,isreader,isguest,address,gst  FROM " + TABLE_STORE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<MasterStore> tableList = new ArrayList<>();
        MasterStore table = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    table = new MasterStore();
                    table.setID(cursor.getString(0));
                    table.setStoreName(cursor.getString(1));
                    table.setItemCategoryID(cursor.getString(2));
                    table.setOTType(cursor.getString(3));

                    table.setIsCashSalesRequire(cursor.getString(4));
                    table.setIsCreditSalesRequire(cursor.getString(5));
                    table.setIsDebitSalesRequired(cursor.getString(6));
                    table.setIsPartyBillApplicable(cursor.getString(7));
                    table.setDashboard(cursor.getString(8));
                    table.setIsOTRequired(cursor.getString(9));
                    table.setMid(cursor.getString(10));
                    table.setMobilestorename(cursor.getString(11));
                    table.setIsReader(cursor.getString(12));
                    table.setIsGuest(cursor.getString(13));
                    table.setAddress(cursor.getString(14));
                    table.setGSTNumber(cursor.getString(15));
                    tableList.add(table);
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return tableList;
    }


    public List<String> getSearchItemlist(String param, String itemId) {
        List<String> labels = new ArrayList<String>();
        //  String p="%"+param+"%";
        String p = "%" + param + "%";
        String p1 = "%" + itemId + "%";
        String selectQuery = "", selectQuery1 = "";
        // Select All Query

//        selectQuery = "SELECT icode,iname   FROM " + TABLE_ITEM +" where itemgid='"+gip+"'"+" and  icode like"+"'"+p+"'";
//        selectQuery1 = "SELECT icode,iname   FROM " + TABLE_ITEM +" where itemgid='"+gip+"'"+" and  iname like"+"'"+p+"'";
       /* selectQuery = "SELECT DISTINCT icode,iname   FROM " + TABLE_ITEM +" where  icode like" + "'" + p + "'" + "and itemcategoryid like " + "'" + p1 + "'";
        selectQuery1 = "SELECT  DISTINCT icode,iname   FROM " + TABLE_ITEM +" where iname like" + "'" + p +"'" + "and itemcategoryid like"+ "'" + p1 + "'";
*/

        //  selectQuery = "SELECT DISTINCT icode,iname    FROM " + TABLE_ITEM +" where  iname like" + "'" + p + "'" + "and itemcategoryid like " + "'" + p1 + "'";
        //   selectQuery1 = "SELECT  DISTINCT icode,iname    FROM " + TABLE_ITEM +" where iname like" + "'" + p +"'" + "and itemcategoryid like"+ "'" + p1 + "'";

       /* selectQuery = "SELECT DISTINCT icode,iname    FROM " + TABLE_ITEM +" where itemcategoryid like " + "'" + p1 + "'";
        selectQuery1 = "SELECT  DISTINCT icode,iname    FROM " + TABLE_ITEM +" where itemcategoryid like"+ "'" + p1 + "'";
*/
        //selectQuery = "SELECT DISTINCT icode,iname    FROM " + TABLE_ITEM +" where  icode like" + "'" + p + "'" + "and itemcategoryid like " + "'" + p1 + "'"+" ORDER BY iname ASC";
        //selectQuery1 = "SELECT  DISTINCT icode,iname    FROM " + TABLE_ITEM +" where iname like" + "'" + p +"'" + "and itemcategoryid like"+ "'" + p1 + "'"+" ORDER BY iname ASC";
        selectQuery = "SELECT DISTINCT icode,iname    FROM " + TABLE_ITEM + " where  icode like" + "'" + p + "'" + "and itemstoreid like " + "'" + p1 + "'" + " ORDER BY icode ASC";
        selectQuery1 = "SELECT  DISTINCT icode,iname    FROM " + TABLE_ITEM + " where iname like" + "'" + p + "'" + "and itemstoreid like" + "'" + p1 + "'" + " ORDER BY iname ASC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Cursor cursor2 = db.rawQuery(selectQuery1, null);
        // looping through all rows and adding to list
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    labels.add(cursor.getString(1) + "_" + cursor.getString(0));
                } while (cursor.moveToNext());
            }
        } else {
            if (cursor2.moveToFirst()) {
                do {
                    labels.add(cursor2.getString(1) + "_" + cursor2.getString(0));
                } while (cursor2.moveToNext());
            }
        }

        // closing connection
        cursor.close();
        cursor2.close();
        db.close();

        return labels;
    }


    public ArrayList<Item> getSearchItemlist_new(String param, String itemId, String grpId) {
        //   List<String> labels = new ArrayList<String>();
        ArrayList<Item> labels = new ArrayList<>();

        String p = param + "%";
        String p1 = itemId + "%";
        String p2 = grpId + "%";
        String selectQuery = "", selectQuery1 = "";
        Item it;

        if (grpId.equals("ALL")) {
            selectQuery = "SELECT DISTINCT icode,iname,image   FROM " + TABLE_ITEM + " where itemcategoryid like " + "'" + p1 + "'";
            selectQuery1 = "SELECT  DISTINCT icode,iname,image  FROM " + TABLE_ITEM + " where itemcategoryid like " + "'" + p1 + "'";
        } else {
            selectQuery = "SELECT DISTINCT icode,iname,image    FROM " + TABLE_ITEM + " where itemcategoryid like " + "'" + p1 + "' and  itemgid like " + "'" + p2 + "'";
            selectQuery1 = "SELECT  DISTINCT icode,iname,image    FROM " + TABLE_ITEM + " where itemcategoryid like " + "'" + p1 + "'  and  itemgid like " + "'" + p2 + "'";

        }


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Cursor cursor2 = db.rawQuery(selectQuery1, null);
        // looping through all rows and adding to list

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    it = new Item();
                    it.setItemName(cursor.getString(1) + "_" + cursor.getString(0));
                    it.setItemImage(cursor.getString(2));

                    labels.add(it);
                } while (cursor.moveToNext());
            }
        } else {
            if (cursor2.moveToFirst()) {
                do {
                    //labels.add(cursor2.getString(1) + "_" + cursor2.getString(0));
                    it = new Item();
                    it.setItemName(cursor.getString(1) + "_" + cursor.getString(0));
                    it.setItemImage(cursor.getString(2));

                    labels.add(it);
                } while (cursor2.moveToNext());
            }
        }

        // closing connection
        cursor.close();
        cursor2.close();
        db.close();

        return labels;
    }


    public List<String> getSearchItemlist(String param, String itemId, String grpId) {
        List<String> labels = new ArrayList<String>();

        String p = param + "%";
        String p1 = itemId + "%";
        String p2 = grpId + "%";
        String selectQuery = "", selectQuery1 = "";

        if (grpId.equals("ALL")) {
            selectQuery = "SELECT DISTINCT icode,iname   FROM " + TABLE_ITEM + " where itemcategoryid like " + "'" + p1 + "'";
            selectQuery1 = "SELECT  DISTINCT icode,iname  FROM " + TABLE_ITEM + " where itemcategoryid like " + "'" + p1 + "'";
        } else {
            selectQuery = "SELECT DISTINCT icode,iname    FROM " + TABLE_ITEM + " where itemcategoryid like " + "'" + p1 + "' and  itemgid like " + "'" + p2 + "'";
            selectQuery1 = "SELECT  DISTINCT icode,iname    FROM " + TABLE_ITEM + " where itemcategoryid like " + "'" + p1 + "'  and  itemgid like " + "'" + p2 + "'";

        }


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Cursor cursor2 = db.rawQuery(selectQuery1, null);
        // looping through all rows and adding to list

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    labels.add(cursor.getString(1) + "_" + cursor.getString(0));
                } while (cursor.moveToNext());
            }
        } else {
            if (cursor2.moveToFirst()) {
                do {
                    labels.add(cursor2.getString(1) + "_" + cursor2.getString(0));
                } while (cursor2.moveToNext());
            }
        }

        // closing connection
        cursor.close();
        cursor2.close();
        db.close();

        return labels;
    }

    public ArrayList<Itemadapter> getOTItem() {

        ArrayList<String> list = new ArrayList<>();

        String selectQuery = "SELECT itemcode_ot,itemname_ot,qty_ot  FROM " + TABLE_OT + " ORDER BY itemname_ot ASC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<Itemadapter> tableList = new ArrayList<>();
        Itemadapter table = null;


        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    table = new Itemadapter();
                    table.setItemCode(cursor.getString(0));
                    table.setItemName(cursor.getString(1));
                    table.setQuantity(cursor.getString(2));
                    //  table.setType(cursor.getString(3));
                    tableList.add(table);
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return tableList;
    }

    public String getItemPrice(String param, String data) {

        String price = "";
        String p = param + "%";
        String selectQuery = "";
        selectQuery = "SELECT DISTINCT rate FROM " + TABLE_ITEM + " where icode like " + "'" + p + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {
                    price = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        }


        // closing connection
        cursor.close();

        db.close();

        return price;
    }

    public ArrayList<Card> getAccountList(String posid, String userId) {
        String _posid = "'" + posid + "'";
        String _userId = "'" + userId + "'";
        //  String _waiterid="'"+waiterid+"'";


        //  String selectQuery = "SELECT  accountnum,cardtype  FROM " + TABLE_ACCOUNT+" WHERE posid="+_posid;
        String selectQuery = "SELECT  accountnum,cardtype,paid,account_name  FROM " + TABLE_ACCOUNT + " WHERE posid=" + _posid + "and lid=" + _userId;
        //   String selectQuery = "SELECT  accountnum,cardtype  FROM " + TABLE_ACCOUNT+" WHERE posid="+posid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<Card> tableList = new ArrayList<>();

        Card table = table = new Card();
       /* table.setAccount("New Card");
        table.setCardtype("");
        table.setDummy_account("");
        tableList.add(table);*/
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    table = new Card();
                    table.setAccount(cursor.getString(0));
                    table.setCardtype(cursor.getString(1));
                    table.setFirstName(cursor.getString(2));
                    table.setDummy_account(cursor.getString(3));
                    //table.setDummy_account(cursor.getString(2));
                    //table.setItemCategoryId(cursor.getString(2));
                    //tableList.add(table);
                    tableList.add(table);
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return tableList;
    }

    public ArrayList<Card> getAccountList(String posid, String userId, String bill) {
        String _posid = "'" + posid + "'";
        String _userId = "'" + userId + "'";
        String _bill = "'" + bill + "'";
        //  String _waiterid="'"+waiterid+"'";

        //  String selectQuery = "SELECT  accountnum,cardtype  FROM " + TABLE_ACCOUNT+" WHERE posid="+_posid;
        String selectQuery = "SELECT  accountnum,cardtype,paid,account_name,bill_mode_type,tableNO,isot,todaysdate, pax_count,steward FROM " + TABLE_ACCOUNT + " WHERE posid=" + _posid + "and lid=" + _userId + "and bill_mode=" + _bill;
        //   String selectQuery = "SELECT  accountnum,cardtype  FROM " + TABLE_ACCOUNT+" WHERE posid="+posid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<Card> tableList = new ArrayList<>();

        Card table = table = new Card();
       /* table.setAccount("New Card");
        table.setCardtype("");
        table.setDummy_account("");
        tableList.add(table);*/
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    int isorderTaken = cursor.getInt(6);
                    if (isorderTaken != 0) {
                        table = new Card();
                        table.setAccount(cursor.getString(0));
                        table.setCardtype(cursor.getString(1));
                        table.setFirstName(cursor.getString(2));
                        table.setDummy_account(cursor.getString(3));
                        table.setBillmode(cursor.getString(4));
                        table.setTableNO(cursor.getString(5));
                        table.setOtdate(cursor.getString(7));
                        table.setPaxcount(cursor.getString(8));
                        table.setSteward(cursor.getString(9));
                        //table.setDummy_account(cursor.getString(2));
                        //table.setItemCategoryId(cursor.getString(2));
                        //tableList.add(table);
                        tableList.add(table);
                    }

                } while (cursor.moveToNext());

            }
        }
        db.close();
        return tableList;
    }

    public Card getAccountList_newcheck(String posid, String userId, String bill, String accountno) {
        String _posid = "'" + posid + "'";
        String _userId = "'" + userId + "'";
        String _bill = "'" + bill + "'";
        String _accountno = "'%" + accountno + "%'";

        //  String selectQuery = "SELECT  accountnum,cardtype  FROM " + TABLE_ACCOUNT+" WHERE posid="+_posid;
        String selectQuery = "SELECT  accountnum,cardtype,paid,account_name,bill_mode_type,tableNO,isot,todaysdate, pax_count,paid FROM " + TABLE_ACCOUNT + " WHERE posid=" + _posid + "and lid=" + _userId + "and bill_mode=" + _bill + "and accountnum like " + _accountno;
        //   String selectQuery = "SELECT  accountnum,cardtype  FROM " + TABLE_ACCOUNT+" WHERE posid="+posid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<Card> tableList = new ArrayList<>();

        Card table = table = new Card();
       /* table.setAccount("New Card");
        table.setCardtype("");
        table.setDummy_account("");
        tableList.add(table);*/
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    int isorderTaken = cursor.getInt(6);
                    if (isorderTaken != 0) {
                        table = new Card();
                        table.setAccount(cursor.getString(0));
                        table.setCardtype(cursor.getString(1));
                        table.setFirstName(cursor.getString(2));
                        table.setDummy_account(cursor.getString(3));
                        table.setBillmode(cursor.getString(4));
                        table.setTableNO(cursor.getString(5));
                        table.setOtdate(cursor.getString(7));
                        table.setPaxcount(cursor.getString(8));
                        table.setSteward(cursor.getString(9));
                        //table.setDummy_account(cursor.getString(2));
                        //table.setItemCategoryId(cursor.getString(2));
                        //tableList.add(table);
                    }

                } while (cursor.moveToNext());

            }
        }
        db.close();
        return table;
    }
    public Card getAccountList_newcheckwaiterwise(String posid, String userId, String bill, String accountno, String waitername) {
        String _posid = "'" + posid + "'";
        String _userId = "'" + userId + "'";
        String _bill = "'" + bill + "'";
        String _accountno = "'%" + accountno + "%'";
        String _waitername = "'" + waitername + "'";

        //  String selectQuery = "SELECT  accountnum,cardtype  FROM " + TABLE_ACCOUNT+" WHERE posid="+_posid;
        String selectQuery = "SELECT  accountnum,cardtype,paid,account_name,bill_mode_type,tableNO,isot,todaysdate, pax_count,steward FROM " + TABLE_ACCOUNT + " WHERE posid=" + _posid + "and lid=" + _userId + "and bill_mode=" + _bill +"and paid=" + _waitername + "and accountnum like " + _accountno;
        //   String selectQuery = "SELECT  accountnum,cardtype  FROM " + TABLE_ACCOUNT+" WHERE posid="+posid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<Card> tableList = new ArrayList<>();

        Card table = table = new Card();
       /* table.setAccount("New Card");
        table.setCardtype("");
        table.setDummy_account("");
        tableList.add(table);*/
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    int isorderTaken = cursor.getInt(6);
                    if (isorderTaken != 0) {
                        table = new Card();
                        table.setAccount(cursor.getString(0));
                        table.setCardtype(cursor.getString(1));
                        table.setFirstName(cursor.getString(2));
                        table.setDummy_account(cursor.getString(3));
                        table.setBillmode(cursor.getString(4));
                        table.setTableNO(cursor.getString(5));
                        table.setOtdate(cursor.getString(7));
                        table.setPaxcount(cursor.getString(8));
                        table.setSteward(cursor.getString(9));
                        //table.setDummy_account(cursor.getString(2));
                        //table.setItemCategoryId(cursor.getString(2));
                        //tableList.add(table);
                    }

                } while (cursor.moveToNext());

            }
        }
        db.close();
        return table;
    }

    public ArrayList<Card> getAccountList1(String posid) {

        String _posid = "'" + posid + "'";
        ArrayList<Card> tableList = new ArrayList<>();
        try {
            String selectQuery = "SELECT  accountnum,cardtype  FROM " + TABLE_ACCOUNT + " WHERE posid=" + _posid;
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            Card table;
       /* table.setAccount("New Card");
        table.setCardtype("");
        tableList.add(table);*/
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        table = new Card();
                        table.setAccount(cursor.getString(0));
                        table.setCardtype(cursor.getString(1));
                        //table.setItemCategoryId(cursor.getString(2));
                        //tableList.add(table);
                        tableList.add(table);
                    } while (cursor.moveToNext());

                }
            }
            db.close();
        } catch (Exception e) {
            String r = e.getMessage().toString();
        }

        return tableList;


    }

    public List<String> getItemid(String itemcode) {
        String itemId = "0";
        String code = "'" + itemcode + "'";
        List<String> listValues = new ArrayList<>();
        //   String selectQuery = "SELECT itemid,rate,TaxID,unitId,iname,tax,description FROM " + TABLE_ITEM +" where icode="+code;
        String selectQuery = "SELECT itemid,rate,unitId,iname,itemcategoryid FROM " + TABLE_ITEM + " where icode=" + code;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    listValues.add(cursor.getString(0));
                    listValues.add(cursor.getString(1));
                    listValues.add(cursor.getString(2));
                    listValues.add(cursor.getString(3));
                    listValues.add(cursor.getString(4));
                    //  listValues.add(cursor.getString(5));
                    //   listValues.add(cursor.getString(6));

                } while (cursor.moveToNext());

            }
        }
        db.close();
        return listValues;
    }


   /* public ArrayList<String>getwList(String id)
    {
        String code="'"+id+"'";
        ArrayList<String> list=new ArrayList<>();

        String selectQuery = "SELECT pcode,pname  FROM " + TABLE_WAITER +" where pid="+code;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String compare="";
        if(cursor!=null)
        {
            if (cursor.moveToFirst()) {
                do {
//                    String alpabet=cursor.getString(1).toUpperCase().substring(0,1);
//                    if(compare.equals(""))
//                    {   compare=alpabet;
//                        list.add(alpabet);
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
//                    else if(compare.equals(alpabet))
//                    {
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
//                    else
//                    {
//                        alpabet=cursor.getString(1).toUpperCase().substring(0,1);
//                        compare=alpabet;
//                        list.add(alpabet);
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
                    list.add(cursor.getString(0)+"-"+cursor.getString(1));
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return  list;
    }
*/


    public ArrayList<String> getItemList(String gid) {
        String code = "'" + gid + "'";
        ArrayList<String> list = new ArrayList<>();

        String selectQuery = "SELECT icode,iname,rate,tax  FROM " + TABLE_ITEM + " where itemgid=" + code + " ORDER BY iname ASC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String compare = "";
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
//                    String alpabet=cursor.getString(1).toUpperCase().substring(0,1);
//                    if(compare.equals(""))
//                    {   compare=alpabet;
//                        list.add(alpabet);
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
//                    else if(compare.equals(alpabet))
//                    {
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
//                    else
//                    {
//                        alpabet=cursor.getString(1).toUpperCase().substring(0,1);
//                        compare=alpabet;
//                        list.add(alpabet);
//                        list.add(cursor.getString(0)+"-"+cursor.getString(1));
//                    }
                    list.add(cursor.getString(0) + "-" + cursor.getString(1) + "-" + cursor.getString(2) + "-" + cursor.getString(3));
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return list;
    }


    public int getItemList1(String gid) {
        String code = "'" + gid + "'";
        int count = 0;

        String selectQuery = "SELECT itemcode_ot,itemname_ot,qty_ot  FROM " + TABLE_OT + " ORDER BY itemname_ot ASC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        String compare = "";
        if (cursor != null) {
            count = cursor.getCount();
        }
        db.close();
        return count;
    }

    public String getGroupId(String gname) {
        String g = "'" + gname + "'";
        String gid = "";
        String selectQuery = "SELECT gid  FROM " + TABLE_ITEMGROUP + " where gname=" + g;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    gid = cursor.getString(0);
                } while (cursor.moveToNext());

            }
        }
        db.close();


        return gid;
    }


    public String getWaiterId(String name) {
        // String g="'"+gname+"'";
        String g = "'" + name + "'";


        String gid = "0";
        String selectQuery = "SELECT pid  FROM " + TABLE_WAITER + " where pcode=" + g;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    gid = cursor.getString(0);
                } while (cursor.moveToNext());

            }
        }
        db.close();


        return gid;
    }
//---------------

    public String getNarration(String itemcode) {
        String narration = "";
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select " + NARRATION + " from " + TABLE_OT + " where " + ITEMCODE_OT + " =  '" + itemcode + "'";
        Cursor c = db.rawQuery(query, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    narration = c.getString(0);
                    if (narration == null) {
                        narration = "";
                    }
                } while (c.moveToNext());
            }
        }
        db.close();
        return narration;
    }

    public List<SendModifiers> getModDetails(String itemcode) {
        List<SendModifiers> modifiers = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select Id,dId,FreeFlow,ItemId from MODIFIER where ItemId =  '" + itemcode + "'";
        Cursor c = db.rawQuery(query, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    SendModifiers narration = new SendModifiers();
                    narration.setModifierId(c.getString(0));
                    narration.setModifierDetailid(c.getString(1));
                    narration.setModifier(c.getString(2));
                    narration.setItemId(c.getString(3));
                    modifiers.add(narration);
                } while (c.moveToNext());
            }
        }
        db.close();
        return modifiers;
    }

    public String getAllen(String itemcode) {
        String narration = "";
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select Name from ALLERGEN where ItemId =  '" + itemcode + "'";
        Cursor c = db.rawQuery(query, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    narration = c.getString(0);
                    if (narration == null) {
                        narration = "";
                    }
                } while (c.moveToNext());
            }
        }
        db.close();
        return narration;
    }

    public String getNarrationId(String itemcode) {
        String narration = "";
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select " + NARRATION_ID + " from " + TABLE_OT + " where " + ITEMCODE_OT + " =  '" + itemcode + "'";
        Cursor c = db.rawQuery(query, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    narration = c.getString(0);
                    if (narration == null) {
                        narration = "";
                    }
                } while (c.moveToNext());
            }
        }
        db.close();
        return narration;
    }

    public String getWaiterName(String id) {
        // String g="'"+gname+"'";
        String g = "'" + id + "'";


        String gid = "0";
        String selectQuery = "SELECT  pname  FROM " + TABLE_WAITER + " where pid=" + g;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    gid = cursor.getString(0);
                } while (cursor.moveToNext());

            }
        }
        db.close();


        return gid;
    }


    public ArrayList<Table> getTablename() {
        String selectQuery = "SELECT tableid,tableNumber  FROM " + TABLE_TABLENAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<Table> tableList = new ArrayList<>();
        Table table = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    table = new Table();
                    table.setTableId(cursor.getInt(0));
                    table.setTableNmae(cursor.getString(1));
                    tableList.add(table);
                } while (cursor.moveToNext());

            }
        }
        db.close();
        return tableList;
    }

    public ArrayList<String> getGroupList_new(String posid) {
        String pos_id = posid;

        String selectQuery = "SELECT gname  FROM " + TABLE_ITEMGROUP;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<String> tableList = new ArrayList<>();
        String compare = "";
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    String alpabet = cursor.getString(0).toUpperCase().substring(0, 1);
                    if (compare.equals("")) {
                        compare = alpabet;
                        tableList.add(cursor.getString(0));
                    } else if (compare.equals(alpabet)) {
                        tableList.add(cursor.getString(0));
                    } else {
                        alpabet = cursor.getString(0).toUpperCase().substring(0, 1);
                        compare = alpabet;
                        tableList.add(cursor.getString(0));
                    }

                } while (cursor.moveToNext());

            }
        }
        db.close();
        return tableList;
    }


    public ArrayList<String> getGroupList(String posid) {
        String pos_id = posid;

        String selectQuery = "SELECT gname  FROM " + TABLE_ITEMGROUP;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<String> tableList = new ArrayList<>();
        String compare = "";
        if (cursor != null) {
            tableList.add("ALL");
            if (cursor.moveToFirst()) {
                do {

                    String alpabet = cursor.getString(0).toUpperCase().substring(0, 1);
                    if (compare.equals("")) {
                        compare = alpabet;
                        // tableList.add(alpabet);
                        //  tableList.add("ALL");
                        tableList.add(cursor.getString(0));
                    } else if (compare.equals(alpabet)) {
                        tableList.add(cursor.getString(0));
                    } else {
                        alpabet = cursor.getString(0).toUpperCase().substring(0, 1);
                        compare = alpabet;
                        // tableList.add(alpabet);
                        tableList.add(cursor.getString(0));
                    }

                } while (cursor.moveToNext());

            }
        }
        db.close();
        return tableList;
    }

    public ArrayList<String> getGroupList() {
        // String pos_id=posid;

        String selectQuery = "SELECT gname  FROM " + TABLE_ITEMGROUP;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<String> tableList = new ArrayList<>();
        String compare = "";
        if (cursor != null) {
            tableList.add("ALL");
            if (cursor.moveToFirst()) {
                do {

                    String alpabet = cursor.getString(0).toUpperCase().substring(0, 1);
                    if (compare.equals("")) {
                        compare = alpabet;
                        // tableList.add(alpabet);
                        //  tableList.add("ALL");
                        tableList.add(cursor.getString(0));
                    } else if (compare.equals(alpabet)) {
                        tableList.add(cursor.getString(0));
                    } else {
                        alpabet = cursor.getString(0).toUpperCase().substring(0, 1);
                        compare = alpabet;
                        // tableList.add(alpabet);
                        tableList.add(cursor.getString(0));
                    }

                } while (cursor.moveToNext());

            }
        }
        db.close();
        return tableList;
    }

    public List<String> getWaiterList1() {
        List<String> labels = new ArrayList<String>();
        String selectQuery = "", selectQuery1 = "";
        selectQuery = "SELECT  pcode,pname   FROM " + TABLE_WAITER + " order by pname asc";
        // itemstoreid
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        //  Cursor cursor2 = db.rawQuery(selectQuery1, null);
        // looping through all rows and adding to list
        if (cursor.getCount() != 0) {
            if (cursor.moveToFirst()) {
                do {

                    labels.add(cursor.getString(0) + " " + "_" + " " + cursor.getString(1));
                    // labels.add(cursor.getString(0) + "_" + cursor.getString(1));
                } while (cursor.moveToNext());
            }
        } else {
           /* if (cursor2.moveToFirst())
            {
                do
                {
                    labels.add(cursor2.getString(1) + "_" + cursor2.getString(0));
                } while (cursor2.moveToNext());
            }*/
        }

        // closing connection
        cursor.close();
        //  cursor2.close();
        db.close();

        return labels;
    }

    public List<String> getWaiterList() {
        List<String> labels = new ArrayList<String>();
        //String p=param;
        //   String p1=itemId+"%";
        //  String p1=pId;
        String selectQuery = "", selectQuery1 = "";
        // Select All Query

//        selectQuery = "SELECT icode,iname   FROM " + TABLE_ITEM +" where itemgid='"+gip+"'"+" and  icode like"+"'"+p+"'";
//        selectQuery1 = "SELECT icode,iname   FROM " + TABLE_ITEM +" where itemgid='"+gip+"'"+" and  iname like"+"'"+p+"'";
        //   selectQuery = "SELECT DISTINCT icode,iname   FROM " + TABLE_ITEM +" where  icode like" + "'" + p + "'" + "and itemgid like " + "'" + p1 + "'";
        //   selectQuery1 = "SELECT  DISTINCT icode,iname   FROM " + TABLE_ITEM +" where iname like" + "'" + p +"'" + "and itemgid like"+ "'" + p1 + "'";
        //selectQuery = "SELECT DISTINCT icode,iname   FROM " + TABLE_ITEM +" where  itemcategoryid like "  + p1 ;

        // selectQuery1 = "SELECT  DISTINCT icode,iname   FROM " + TABLE_ITEM +" where  itemcategoryid like" + p1 + "'";


        selectQuery = "SELECT  pcode,pname   FROM " + TABLE_WAITER + " order by pname asc";
        // selectQuery = "SELECT DISTINCT icode,iname   FROM " + TABLE_ITEM +" where  itemgid like "  + p1  + "and itemstoreid like" + p ;

        // itemstoreid
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        //  Cursor cursor2 = db.rawQuery(selectQuery1, null);
        // looping through all rows and adding to list
        if (cursor.getCount() != 0) {
            labels.add("Select Steward");
            if (cursor.moveToFirst()) {
                do {

                    labels.add(cursor.getString(0) + " " + "_" + " " + cursor.getString(1));
                    // labels.add(cursor.getString(0) + "_" + cursor.getString(1));
                } while (cursor.moveToNext());
            }
        } else {
           /* if (cursor2.moveToFirst())
            {
                do
                {
                    labels.add(cursor2.getString(1) + "_" + cursor2.getString(0));
                } while (cursor2.moveToNext());
            }*/
        }

        // closing connection
        cursor.close();
        //  cursor2.close();
        db.close();

        return labels;
    }



   /* public ArrayList<String>getWaiterList()
    {
        String selectQuery = "SELECT pname  FROM " + TABLE_WAITER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<String> tableList=new ArrayList<>();
        String compare="";
        if(cursor!=null)
        {
            if (cursor.moveToFirst()) {
                do {

                    String alpabet=cursor.getString(0).toUpperCase().substring(0,1);
                    if(compare.equals(""))
                    {   compare=alpabet;
                        tableList.add(alpabet);
                        tableList.add( cursor.getString(0));
                    }
                    else if(compare.equals(alpabet))
                    {
                        tableList.add( cursor.getString(0));
                    }
                    else
                    {
                        alpabet=cursor.getString(0).toUpperCase().substring(0,1);
                        compare=alpabet;
                        tableList.add(alpabet);
                        tableList.add( cursor.getString(0));
                    }

                } while (cursor.moveToNext());

            }
        }
        db.close();
        return tableList;
    }*/
    //delete


    public Long deleteAccount(String accountnumber, String posid, String userId) {
        long delete = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            // delete=db.delete(TABLE_ACCOUNT,"accountnum = ?"+"'"+accountnumber+"'", null);
            delete = db.delete(TABLE_ACCOUNT, ACCNUM + "= ? AND " + POSID + "= ? AND " + LID + "= ?", new String[]{accountnumber, posid, userId});
            db.close();
        } catch (Exception e) {
            String mm = e.getMessage().toString();
        }

        return delete;
    }

    public Long deleteAccountlist(String posid, String userId) {
        long delete = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            // delete=db.delete(TABLE_ACCOUNT,"accountnum = ?"+"'"+accountnumber+"'", null);
            delete = db.delete(TABLE_ACCOUNT, POSID + "= ? AND " + LID + "= ?", new String[]{posid, userId});
            db.close();
        } catch (Exception e) {
            String mm = e.getMessage().toString();
        }

        return delete;
    }

    public Long deleteAccount_new(String accountnumber, String posid, String userId, String
            billmode) {
        long delete = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            // delete=db.delete(TABLE_ACCOUNT,"accountnum = ?"+"'"+accountnumber+"'", null);
            delete = db.delete(TABLE_ACCOUNT, ACCNUM + "= ? AND " + POSID + "= ? AND " + LID + "= ? AND " + BILLMODE + "= ?", new String[]{accountnumber, posid, userId, billmode});
            db.close();
        } catch (Exception e) {
            String mm = e.getMessage().toString();
        }

        return delete;
    }

    public Long deleteitemgrp(String gid, String posid) {
        long delete = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            // delete=db.delete(TABLE_ACCOUNT,"accountnum = ?"+"'"+accountnumber+"'", null);
            delete = db.delete(TABLE_ITEM, IGID + "= ? AND " + SID + "= ?", new String[]{gid, posid});
            db.close();
        } catch (Exception e) {
            String mm = e.getMessage().toString();
        }

        return delete;
    }

    public Long deleteAccountInbakery(String accountnumber) {
        long delete = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            delete = db.delete(TABLE_ACCOUNT, "dummy_account=" + "'" + accountnumber + "'", null);
            db.close();
        } catch (Exception e) {
            String mm = e.getMessage().toString();
        }

        return delete;
    }

    public Long deleteTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete(TABLE_TABLENAME, null, null);
        db.close();
        return delete;
    }


    public Long deleteSteward() {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete(TABLE_WAITER, null, null);
        db.close();
        return delete;
    }

    public Long deleteItemGroup(String posid) {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete(TABLE_ITEMGROUP, null, null);
        db.close();
        return delete;
    }


    public Long deleteWaiter(String posid) {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete(TABLE_WAITER, null, null);
        db.close();
        return delete;
    }

    public Long deleteItem(String posid) {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete(TABLE_ITEM, "itemstoreid=" + "'" + posid + "'", null);
        db.close();
        return delete;
    }

    public Long deleteOT() {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete(TABLE_OT, null, null);
        db.close();
        return delete;
    }


    public Long deleteStore() {
        SQLiteDatabase db = this.getWritableDatabase();
        long delete = db.delete(TABLE_STORE, null, null);
        db.close();
        return delete;
    }

    public boolean deleteInvalidOffer(String todaysDate) {
        SQLiteDatabase db = this.getWritableDatabase();
        //int did = db.delete("ITEM_PROMOTION", "ToDate < ? ", new String[]{todaysDate.toString()});
        int did = db.delete("ITEM_PROMOTION", null, null);
        //int did1 = db.delete("ITEMS_OFFER", "ToDate < ? ", new String[]{"2017-09-09"});
        db.close();
        return (did > 0) ? true : false;
        //return db.delete("TEMP_ORDER","id = ? ",new String[] { Integer.toString(id) });
    }

}
