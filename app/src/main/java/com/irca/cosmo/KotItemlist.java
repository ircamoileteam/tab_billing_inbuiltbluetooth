package com.irca.cosmo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.ImageCropping.CircularImage;
import com.irca.MaterialFloatLabel.MaterialAutoCompleteTextView;
import com.irca.MaterialFloatLabel.MaterialEditText;
import com.irca.Printer.PlaceOrder_bill;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.db.Dbase;
import com.irca.fields.AndroidTempbilldetails;
import com.irca.fields.AndroidTempbilling;
import com.irca.fields.CloseorderItems;
import com.irca.fields.Item;
import com.irca.fields.ItemList;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

//import io.fabric.sdk.android.Fabric;

import static com.irca.cosmo.TakeOrder.isCashCardFilled;

/**
 * Created by Manoch Richard  10/20/2015.
 */
public class  KotItemlist extends Activity {
    String waiternamecode;
    LinearLayout itemView;
    TextView itemName, itemCount;
    CheckBox happyHour;
    ImageView add, subtract,search;
    View view = null;
    TextView steward,  tableName;
    int noOfCounts = 0;
    TextView txt_itemcount;
    Dbase db;
    public static ArrayList<CloseorderItems> closeorderItemsArrayList = new ArrayList<CloseorderItems>();
    Button placeOrder;
    ProgressDialog pd;
    float Totalamount = 0;
    String memberId = "0";
    int tableId = 0;
    String creditLimit ="0";
    String C = "0";
    SharedPreferences sharedpreferences;
    String accountNo = "";
    String imei = "";
    TelephonyManager tel;
    String mode="";
    String cardType = "";
    String[] payments;
    String Nmae,waiterid="";
    int position;
    String acc;
    public static ArrayList<ItemList>  placeOrder_list;
    String billType = "";
    int tableNo=0;
    String groupId = "";
    MaterialAutoCompleteTextView searchItemname;
    View searchView = null;
    LayoutInflater inflater=null;
    MaterialEditText searchCount;
    Context c;
    AlertDialog builder;
    String referenceNo="";
    ImageView find;
    String dummy="";
    ItemnameSearch myadapter;
    TextView _title;
    String pos="";
    TextView memberdetails,mem_accNo,openingBalance ;
    String cLimit="";
    ConnectionDetector cd;
    Boolean isInternetPresent=false;
    ImageView logo ;
    Bitmap bitmap;

    String serialNo="";
    EditText editText;
    String newTableNo="";
    public static ArrayList<CloseorderItems> closeorderItemsArrayList_Others = new ArrayList<CloseorderItems>();
    ArrayList<String> description ;

    @Override
    protected void onResume()
    {
        super.onResume();
        itemView= (LinearLayout) findViewById(R.id.itemView);
        itemView.removeAllViews();
        placeOrder.setVisibility(View.VISIBLE);
        additems(2);

    }
    @Override
    public void onBackPressed()
    {
        //super.onBackPressed();
        ArrayList<String> list=null;
        list = db.getOTItemList();

        if(!list.isEmpty())
        {
            try{
                AlertDialog.Builder alertBulider=new AlertDialog.Builder(this);
                alertBulider.setMessage("Do you Want to Cancel the Order ?");
                alertBulider.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertBulider.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog=alertBulider.create();
                //  alertBulider.create();
                dialog.show();
            }
            catch(Exception e){
                String r=e.getMessage().toString();}
        }
        else
        {
            finish();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.kot_itemlist);

        /*gName = (TextView) findViewById(R.id.group);*/
        _title=(TextView)findViewById(R.id.Item_title);
        txt_itemcount = (TextView) findViewById(R.id.txtCount);

        tableName = (TextView) findViewById(R.id.tablename);

        steward= (TextView) findViewById(R.id.steward);
        placeOrder = (Button) findViewById(R.id.button_placeOrder);
        placeOrder.setVisibility(View.GONE);

        find=(ImageView)findViewById(R.id.search);
        itemView= (LinearLayout) findViewById(R.id.itemView);

        logo=(ImageView)findViewById(R.id.imageView_logo);
        bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
        logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));

        memberdetails = (TextView) findViewById(R.id.memberDetails);
        mem_accNo = (TextView) findViewById(R.id.accno);
        openingBalance = (TextView) findViewById(R.id.op_bal);



        c=KotItemlist.this;
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        pos=sharedpreferences.getString("StoreName", "");

       // cLimit=sharedpreferences.getString("creditLimit","");
        cLimit=TakeOrder.c_Limit;
        _title.setText(pos+"-  Item List");



        Bundle b = getIntent().getExtras();

        Nmae = b.getString("creditName");
        acc = b.getString("creditAno");
        creditLimit = b.getString("creditLimit");
        tableId = b.getInt("tableId");
        memberId = b.getString("memberId");
        cardType = b.getString("cardType");
        tableNo = b.getInt("tableNo");

        //referenceNo=(cardType.equals("SMART CARD")) ? "0":b.getString("referenceNo");
        referenceNo=b.getString("referenceNo");

        waiterid=b.getString("waiterid");
        position=b.getInt("position");
        waiternamecode=b.getString("waiternamecode");

        newTableNo=Integer.toString(tableNo);




        C = creditLimit;
        tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        //imei = tel.getDeviceId().toString();

        serialNo=Build.SERIAL;

        accountNo = creditLimit;
        tableName.setText("Table" + tableNo);
        steward.setText("Steward Name: " + waiternamecode);
        steward.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        txt_itemcount.setText(noOfCounts + "");
       // memberdetails.setText("Name:" + Nmae + "\n Account No.:" + acc + "\n Opening Balance:" + creditLimit + "Rs");
        memberdetails.setText( Nmae+"" );
        mem_accNo.setText("Account No :" + acc);
        openingBalance.setText(" Opening Balance: " + creditLimit + "Rs");

        db = new Dbase(KotItemlist.this);

        billType = sharedpreferences.getString("billType", "");
        inflater = KotItemlist.this.getLayoutInflater();


        tableName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertTableNO();
            }
        });

        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(c, Search_Item.class);
                i.putExtra("tableNo", tableNo);
                i.putExtra("creditLimit", creditLimit);
                i.putExtra("creditAno", acc);
                i.putExtra("creditName", Nmae);
                i.putExtra("memberId", memberId);
                i.putExtra("tableId", tableId);
                i.putExtra("cardType", cardType);
                i.putExtra("referenceNo", referenceNo);
                i.putExtra("position",position);
                i.putExtra("waiternamecode",waiternamecode);
                i.putExtra("waiterid",waiterid);
                startActivity(i);


                finish();
            }
        });

     //   db.deleteOT();
        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!txt_itemcount.getText().equals("0")) {
                    if (billType.equals("BOT")) {
                        new AsyncPlaceOrder2().execute();
                    }
                    else if(billType.equals("KOT"))
                    {
                       // AlertPayment(memberId, "0",cardType,isCashCardFilled);
                        new AsyncPlaceOrder().execute();
                    }
                    //monica
                    else if(billType.equals("OT"))
                    {
                        //new AsyncPlaceOrder().execute();
                        AlertPayment(memberId, "0",cardType,isCashCardFilled);

                    }
                    //**
                    else if(billType.equals("NOT")) {
                        new AsyncPlaceOrder3().execute();
                    }else {
                        Toast.makeText(getApplicationContext(), "No Such POS", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Order any items", Toast.LENGTH_LONG).show();
                }
            }
        });

        itemView= (LinearLayout) findViewById(R.id.itemView);
        itemView.removeAllViews();

        additems(2);

    }
    private void AlertPayment(final String s, String s1,String cardType,String isCashCardFilled) {

        if(cardType.equals("ROOM CARD")){
            payments  = new String[]{
                    "Account"
            };
        } else if (cardType.equals("CASH CARD")&& isCashCardFilled.equals("true") ){
            payments  = new String[]{
                    "Account"};
        }
        else if (cardType.equals("CASH CARD")&& isCashCardFilled.equals("false"))
        {
            payments  = new String[]{"Cash", "Credit"};}
        else
        {
            payments  = new String[]{
                    "Cash", "Credit", "Account"
            };
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select the Payment Mode");
        builder.setItems(payments, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int paymentMode) {

                mode=payments[paymentMode];
                new AsyncPlaceOrder3().execute(mode);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    //*****
    private void alertTableNO() {

        AlertDialog.Builder alertBulider=new AlertDialog.Builder(this);
        alertBulider.setMessage("Table Number  ");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_table_no, null);
        alertBulider.setView(dialogView);

        editText = (EditText) dialogView.findViewById(R.id.tableno);

        alertBulider.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               // finish();
                tableName.setText("Table "+editText.getText().toString());
                newTableNo=editText.getText().toString();
            }
        });
        alertBulider.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog=alertBulider.create();
        dialog.show();

    }

    private void alertNarration(){
        description=db.getNarration();


        AlertDialog.Builder alertBulider=new AlertDialog.Builder(this);
        alertBulider.setMessage("Narration ");

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.temp_alertview, null);
        alertBulider.setView(dialogView);

        editText = (EditText) dialogView.findViewById(R.id.tableno);

        alertBulider.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // finish();
                tableName.setText("Table "+editText.getText().toString());
                newTableNo=editText.getText().toString();
            }
        });
        alertBulider.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog=alertBulider.create();
        dialog.show();

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        //super.onCreateContextMenu(menu, v, menuInfo);
        menu.clear();
        menu.clearHeader();

    }

    protected class AsyncPlaceOrder extends AsyncTask<String, Void, String> {
        ArrayList<String> credit = null;
        String status = "";
        String creditAccountno = "";
        String creditName = "";
        String storeId = sharedpreferences.getString("storeId", "");
        String loginname = sharedpreferences.getString("loginName", "");
        String userId = sharedpreferences.getString("userId", "");
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        String result = "";
        String narration="";



        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(KotItemlist.this);
            try {
                Totalamount=0;

                AndroidTempbilling obj = new AndroidTempbilling();
                //ArrayList<AndroidTempbilldetails> itemList=new ArrayList<>();
                ArrayList<Object> itemList_main = new ArrayList<>();
                DecimalFormat _format=new DecimalFormat("#.##");
                placeOrder_list = new ArrayList<>();
                AndroidTempbilldetails ob = null;
                ItemList li = null;
                if (itemView != null)
                {
                    //itemView.bringChildToFront(v);
                    int childRow = itemView.getChildCount();
                    for (int i = 0; i < childRow; i++) {
                        View ItemRow = itemView.getChildAt(i);
                        TextView t = (TextView) ItemRow.findViewById(R.id.kot_itemCount);
                        TextView t1 = (TextView) ItemRow.findViewById(R.id.kot_itemname);
                        String Itemcount = t.getText().toString();

                        if (!((Itemcount.equals("")) || (Itemcount.equals("0")))) {

                            String itemname = t1.getText().toString();
                            String[] spl = itemname.split("-");
                            String _itemCode = spl[0];
                            ArrayList<String> dummy=new ArrayList<String>();
                            List<String> itemId = db.getItemid(_itemCode);

                            narration=db.getNarration(_itemCode);

                            // String  itemRate=db.getItemRate(_itemCode);
                            ob = new   AndroidTempbilldetails();
                            li = new ItemList();

                            float salesTax = 0;
                            float serviceTax = 0;
                            float cessTax = 0;

                            ob.setSalestax(0);  // unwanted
                            ob.setServicetax(0);
                            ob.setCesstax(0);
                            ob.setOtno("");
                            ob.setTaxId(0);
                            ob.setTaxRate(0);
                            ob.setAlltax(dummy);

                            ob.setSalesUintid(Integer.parseInt(itemId.get(2)));
                            ob.setQuantity(Integer.parseInt(Itemcount));
                            ob.setRate(itemId.get(1));
                            String _itemName = itemId.get(3);
                            ob.setItemcode(_itemCode);
                            ob.setItemname(_itemName);
                            ob.setItemId(itemId.get(0));
                            ob.setItemCategoryid(itemId.get(4));
                            ob.setNarration(narration);

                            li.setItemCode(_itemCode);
                            li.setItemName(_itemName);
                            li.setQuantity(Itemcount);

                            float v1 = Integer.parseInt(Itemcount);
                            float v2 = Float.parseFloat(_format.format(Double.parseDouble(itemId.get(1))));
                            float v3 = v1 * v2;

                            ob.setAmount(v3);

                            Totalamount = Totalamount + ob.getAmount();
                            itemList_main.add(ob);
                            placeOrder_list.add(li);

                            obj.setMemberId(memberId);
                            obj.setStoreId(storeId);
                            obj.setTableId(newTableNo + "");
                            obj.setTamount(Float.valueOf(_format.format(Totalamount)));
                            obj.setNarration(narration);
                        }

                    }
                }

                if (itemList_main.size() != 0)
                {

                     if(TakeOrder.check.equals(""))
                     {

                         if(cardType.equals("CASH CARD") )
                         {
                             if(isCashCardFilled.equalsIgnoreCase("true"))
                             {

                                 if(Float.parseFloat(C)-Totalamount >0)
                                 {
                                     // JSONObject jsonObject = api.cms_placeOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo);
                                     JSONObject jsonObject = api.cms_placeOrder_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo,waiterid);
                                     status = jsonObject.toString();
                                     result = jsonObject.optString("Value");
                                 }else
                                 {
                                     status = "Credit exceeds";
                                 }

                             }else if(isCashCardFilled.equalsIgnoreCase("false"))
                             {

                                 JSONObject jsonObject = api.cms_placeOrder_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo,waiterid);
                                 status = jsonObject.toString();
                                 result = jsonObject.optString("Value");

                             }else
                             {
                                 status="cardCheck";
                             }
                         }
                         else if (cardType.equals("SMART CARD"))
                         {
                             if(Float.parseFloat(C)-Totalamount >0)
                             {
                                 // JSONObject jsonObject = api.cms_placeOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo);
                                 JSONObject jsonObject = api.cms_placeOrder_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo,waiterid);
                                 status = jsonObject.toString();
                                 result = jsonObject.optString("Value");
                             }else
                             {
                                 status = "Credit exceeds";
                             }
                         }
                         else
                         {

                             //   String  limit = (cLimit.equals(""))?"0":cLimit;
                             String  limit = (TakeOrder.c_Limit.equals(""))?"0":TakeOrder.c_Limit;



                             if (C.equals("-0")||C.equals("-0.0"))
                             {
                                    // JSONObject jsonObject = api.cms_placeOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                                     JSONObject jsonObject = api.cms_placeOrder_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                     status = jsonObject.toString();
                                     result = jsonObject.optString("Value");
                             }
                             else if (Totalamount <= (Double.parseDouble(limit) - Float.parseFloat(C)))
                             {
                                 // JSONObject jsonObject = api.cms_placeOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                                 JSONObject jsonObject = api.cms_placeOrder_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                 status = jsonObject.toString();
                                 result = jsonObject.optString("Value");
                             }
                             else
                             {
                                 status = "Credit exceeds";
                             }

                         }



                     }
                     else
                     {
                            if(TakeOrder.check.equals("False"))
                            {
                               // JSONObject jsonObject = api.cms_placeOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                                JSONObject jsonObject = api.cms_placeOrder_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                status = jsonObject.toString();
                                result = jsonObject.optString("Value");


                            }
                            else
                            {

                                if(cardType.equals("CASH CARD"))
                                {
                                    if(isCashCardFilled.equalsIgnoreCase("true"))
                                    {

                                        if(Float.parseFloat(C)-Totalamount >0)
                                        {
                                            // JSONObject jsonObject = api.cms_placeOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo);
                                            JSONObject jsonObject = api.cms_placeOrder_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo,waiterid);
                                            status = jsonObject.toString();
                                            result = jsonObject.optString("Value");
                                        }else
                                        {
                                            status = "Credit exceeds";
                                        }

                                    }else if(isCashCardFilled.equalsIgnoreCase("false"))
                                    {

                                        JSONObject jsonObject = api.cms_placeOrder_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo,waiterid);
                                        status = jsonObject.toString();
                                        result = jsonObject.optString("Value");

                                    }else
                                    {
                                        status="cardCheck";
                                    }

                                }
                                else if (cardType.equals("SMART CARD"))
                                {
                                    if(Float.parseFloat(C)-Totalamount >0)
                                    {
                                        // JSONObject jsonObject = api.cms_placeOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo);
                                        JSONObject jsonObject = api.cms_placeOrder_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model,referenceNo,waiterid);
                                        status = jsonObject.toString();
                                        result = jsonObject.optString("Value");
                                    }else
                                    {
                                        status = "Credit exceeds";
                                    }

                                }
                                else
                                {
                                    String  limit = (cLimit.equals(""))?"0":cLimit;

                                    if (C.equals("-0")||C.equals("-0.0")) {
                                        // JSONObject jsonObject = api.cms_placeOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                                        JSONObject jsonObject = api.cms_placeOrder_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                        status = jsonObject.toString();
                                        result = jsonObject.optString("Value");
                                    }
                                    else if (Totalamount <= (Double.parseDouble(limit) - Float.parseFloat(C))) {
                                        //JSONObject jsonObject = api.cms_placeOrder(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                                        JSONObject jsonObject = api.cms_placeOrder_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                        status = jsonObject.toString();
                                        result = jsonObject.optString("Value");
                                    } else {
                                        status = "Credit exceeds";
                                    }
                                }

                            }
                     }
                }
                else
                {
                    status = "No item";
                }

            } catch (Exception e) {
                e.printStackTrace();
                status = e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(KotItemlist.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s.equals("Credit exceeds")) {

                if(cardType.equals("CASH CARD")&& isCashCardFilled.equalsIgnoreCase("True"))
                {
                    showPopUp();

                }else if(cardType.equals("SMART CARD")){
                    showPopUp();
                }else{
                    Toast.makeText(getApplicationContext(), "Credit limit exceeds : Total Amount"+Totalamount +"\n"+"Opening Balance:"+C, Toast.LENGTH_LONG).show();
                }

            }
            else if(s.contains("Unable to connect to the remote server")){
                Toast.makeText(getApplicationContext(), "Order placed -- > POS system is off", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(KotItemlist.this, PlaceOrder_bill.class);
                i.putExtra("mName", Nmae);
                i.putExtra("a/c", acc);
                i.putExtra("tableNo", tableNo);
                i.putExtra("tableId",tableId);
                i.putExtra("bill_id", result);
                // i.putExtra("Type", "k");
                i.putExtra("Type", "kk");
                i.putExtra("position",position);
                i.putExtra("waiternamecode",waiternamecode);

                startActivity(i);
                finish();
            } else if (s.contains("true") ) {
                Toast.makeText(getApplicationContext(), "Order placed", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(KotItemlist.this, PlaceOrder_bill.class);
                i.putExtra("mName", Nmae);
                i.putExtra("a/c", acc);
                i.putExtra("tableNo", tableNo);
                i.putExtra("tableId",tableId);
                i.putExtra("bill_id", result);
                i.putExtra("Type", "k");
                i.putExtra("position",position);
                i.putExtra("waiternamecode",waiternamecode);

                startActivity(i);
                finish();
          //  }else if(s.contains("{\"ErrorMessage\":\"Unable to connect to the remote server\",\"Successful\":false}")){
            }else if (s.contains("No item")) {
                Toast.makeText(getApplicationContext(), "No item is ordered-->" + s, Toast.LENGTH_LONG).show();
            } else if (status.equals("cardCheck")){
                Toast.makeText(getApplicationContext(), "something went wrong-->" + s + isCashCardFilled, Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(getApplicationContext(), "Order not placed-->" + s, Toast.LENGTH_LONG).show();
            }
        }
    }

    protected class AsyncPlaceOrder2 extends AsyncTask<String,Void,String> {
        ArrayList<String> credit=null;
        String status="";
        String creditAccountno="";
        String creditName="";
        String storeId = sharedpreferences.getString("storeId", "");
        String loginname=sharedpreferences.getString("loginName","");
        String userId=sharedpreferences.getString("userId","");
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        String result="";
        String narration="";

        @Override
        protected String doInBackground(String... params)
        {
            RestAPI api=new RestAPI(KotItemlist.this);
            try
            {
                Totalamount=0;
                AndroidTempbilling obj=new AndroidTempbilling();
                ArrayList<Object> itemList_main=new ArrayList<>();
                placeOrder_list=new ArrayList<>();
                AndroidTempbilldetails ob=null;
                ItemList li=null;
                if(itemView!=null)
                {
                    //itemView.bringChildToFront(v);
                    int childRow= itemView.getChildCount();
                    for(int i=0;i<childRow;i++)
                    {
                        View ItemRow=itemView.getChildAt(i);
                        TextView t= (TextView) ItemRow.findViewById(R.id.kot_itemCount);
                        TextView t1= (TextView) ItemRow.findViewById(R.id.kot_itemname);
                        String Itemcount=t.getText().toString();
                        if(!((Itemcount.equals("")) ||  (Itemcount.equals("0"))))
                        {
                            String itemname=t1.getText().toString();
                            String [] spl=itemname.split("-");
                            String _itemCode=spl[0];
                            ArrayList<String> dummy=new ArrayList<String>();
                            List<String>  itemId=db.getItemid(_itemCode);

                            narration=db.getNarration(_itemCode);

                            float salesTax = 0;
                            float serviceTax = 0;
                            float cessTax = 0;

                            ob=new AndroidTempbilldetails();
                            li=new ItemList();
                            String _itemName = itemId.get(3);

                            ob.setSalestax(0);  // unwanted
                            ob.setServicetax(0);
                            ob.setCesstax(0);
                            ob.setOtno("");
                            ob.setTaxId(0);
                            ob.setTaxRate(0);
                            ob.setAlltax(dummy);

                            ob.setSalesUintid(Integer.parseInt(itemId.get(2)));
                            ob.setQuantity(Integer.parseInt(Itemcount));
                            ob.setRate(itemId.get(1));
                            ob.setItemcode(_itemCode);
                            ob.setItemname(_itemName);
                            ob.setItemId(itemId.get(0));
                            ob.setItemCategoryid(itemId.get(4));
                            ob.setNarration(narration);


                            int v1=Integer.parseInt(Itemcount);
                            float v2=Float.parseFloat(itemId.get(1));
                            float  v3=v1 * v2;

                             //  ob.setAmount(Float.parseFloat(itemId.get(1)));
                            ob.setAmount(v3);

                           /* ArrayList<String> dummy=new ArrayList<String>();
                            if (!cardType.equals("CLUB CARD"))
                            {
                                if(itemId.size()>7)
                                {
                                    int k=5;
                                    int l=2;
                                    for(int j=0;j<(itemId.size()/7);j++)
                                    {
                                        dummy.add(itemId.get(k)+"-"+itemId.get(l));
                                        k=k+7;
                                        l=l+7;
                                    }
                                    ob.setAlltax(dummy);
                                    ob.setTaxId(Integer.parseInt(itemId.get(2)));
                                }
                                else
                                {
                                    dummy.add(itemId.get(5)+"-"+itemId.get(2));
                                    ob.setAlltax(dummy);
                                }
                            }
                            else
                            {
                                dummy.add("0");
                                ob.setAlltax(dummy);
                                ob.setTaxId(Integer.parseInt(itemId.get(2)));
                            }*/
                            Totalamount=Totalamount+v3+salesTax+serviceTax+cessTax;
                            itemList_main.add(ob);

                            obj.setMemberId(memberId);
                            obj.setStoreId(storeId);
                            obj.setTableId(newTableNo+"");
                            obj.setTamount(Totalamount);
                            obj.setNarration(narration);
                        }

                    }
                }

                if(itemList_main.size()!=0)
                {
                    //String  limit = (cLimit.equals(""))?"0":cLimit;
                    String  limit = (TakeOrder.c_Limit.equals(""))?"0":TakeOrder.c_Limit;

                    if(!cardType.equals("CASH CARD") && !cardType.equals("SMART CARD"))
                    {


                        if(C.equals("-0") || TakeOrder.check.equals("False")||C.equals("-0.0"))
                        {
                          //  JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                            JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                            status=jsonObject.toString();
                            result=jsonObject.optString("Value");

                            JSONArray array=jsonObject.getJSONArray("Value");
                            ItemList it=null;

                            for(int i=0;i<array.length();i++)
                            {
                                it=new ItemList();
                                JSONObject object=array.getJSONObject(i);

                                String status=object.getString("status");
                                String Quantity=object.getString("qty");
                                String stock=object.getString("stock");
                                String rate=object.optString("rate");
                                String itemcode=object.optString("itemcode");
                                String itemName=object.optString("itemname");
                                String billno=object.optString("billid");

                                it.setItemCode(itemcode);
                                it.setItemName(itemName);
                                it.setStatus(status);
                                it.setStock(stock);
                                it.setQuantity(Quantity);
                                it.setRate(rate);
                                it.setBillno(billno);
                                placeOrder_list.add(it);
                            }
                        }
                        else if(Totalamount<=(Double.parseDouble(limit)-Float.parseFloat(C)))
                        {
                          //  JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                            JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                            status=jsonObject.toString();
                            result=jsonObject.optString("Value");
                            JSONArray array=jsonObject.getJSONArray("Value");
                            ItemList it=null;

                            for(int i=0;i<array.length();i++)
                            {
                                it=new ItemList();
                                JSONObject object=array.getJSONObject(i);

                                String status=object.getString("status");
                                String Quantity=object.getString("qty");
                                String stock=object.getString("stock");
                                String rate=object.optString("rate");
                                String itemcode=object.optString("itemcode");
                                String itemName=object.optString("itemname");
                                String billno=object.optString("billid");

                                it.setItemCode(itemcode);
                                it.setItemName(itemName);
                                it.setStatus(status);
                                it.setStock(stock);
                                it.setQuantity(Quantity);
                                it.setRate(rate);
                                it.setBillno(billno);
                                placeOrder_list.add(it);
                            }

                        }
                        else
                        {
                            status="Credit exceeds";
                        }

                    }
                    else
                    {

                        if(cardType.equals("SMART CARD"))
                        {
                            if(Float.parseFloat(C)-Totalamount >0 )
                            {
                                // JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                                JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                status=jsonObject.toString();
                                result=jsonObject.optString("Value");
                                JSONArray array=jsonObject.getJSONArray("Value");
                                ItemList it=null;

                                for(int i=0;i<array.length();i++)
                                {
                                    it=new ItemList();
                                    JSONObject object=array.getJSONObject(i);

                                    String status=object.getString("status");

                                    String Quantity=object.getString("qty");
                                    String stock=object.getString("stock");
                                    String rate=object.optString("rate");
                                    String itemcode=object.optString("itemcode");
                                    String itemName=object.optString("itemname");

                                    String billno=object.optString("billid");

                                    it.setItemCode(itemcode);
                                    it.setItemName(itemName);
                                    it.setStatus(status);
                                    it.setStock(stock);
                                    it.setQuantity(Quantity);
                                    it.setRate(rate);
                                    it.setBillno(billno);
                                    placeOrder_list.add(it);
                                }

                            }else{
                                status="Credit exceeds";
                            }

                        }
                        else
                        {
                            // TODO need to validate for thr fillied cards ------

                            if(isCashCardFilled.equalsIgnoreCase("true"))
                            {


                                if(Float.parseFloat(C)-Totalamount >0 )
                                {
                                    // JSONObject jsonObject = api.cms_placeOrder2(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo);
                                    JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                    status=jsonObject.toString();
                                    result=jsonObject.optString("Value");
                                    JSONArray array=jsonObject.getJSONArray("Value");
                                    ItemList it=null;

                                    for(int i=0;i<array.length();i++)
                                    {
                                        it=new ItemList();
                                        JSONObject object=array.getJSONObject(i);

                                        String status=object.getString("status");

                                        String Quantity=object.getString("qty");
                                        String stock=object.getString("stock");
                                        String rate=object.optString("rate");
                                        String itemcode=object.optString("itemcode");
                                        String itemName=object.optString("itemname");

                                        String billno=object.optString("billid");

                                        it.setItemCode(itemcode);
                                        it.setItemName(itemName);
                                        it.setStatus(status);
                                        it.setStock(stock);
                                        it.setQuantity(Quantity);
                                        it.setRate(rate);
                                        it.setBillno(billno);
                                        placeOrder_list.add(it);
                                    }

                                }else{
                                    status="Credit exceeds";
                                }
                            }else if (isCashCardFilled.equalsIgnoreCase("false"))
                            {


                                JSONObject jsonObject = api.cms_placeOrder2_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,waiterid);
                                status=jsonObject.toString();
                                result=jsonObject.optString("Value");
                                JSONArray array=jsonObject.getJSONArray("Value");
                                ItemList it=null;

                                for(int i=0;i<array.length();i++)
                                {
                                    it=new ItemList();
                                    JSONObject object=array.getJSONObject(i);

                                    String status=object.getString("status");

                                    String Quantity=object.getString("qty");
                                    String stock=object.getString("stock");
                                    String rate=object.optString("rate");
                                    String itemcode=object.optString("itemcode");
                                    String itemName=object.optString("itemname");

                                    String billno=object.optString("billid");

                                    it.setItemCode(itemcode);
                                    it.setItemName(itemName);
                                    it.setStatus(status);
                                    it.setStock(stock);
                                    it.setQuantity(Quantity);
                                    it.setRate(rate);
                                    it.setBillno(billno);
                                    placeOrder_list.add(it);
                                }

                            }else {

                                status = "Check Card";
                            }

                        }


                    }

                }
                else
                {
                    status="No item";
                }

            } catch (Exception e)
            {
                e.printStackTrace();
                status=e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pd=new ProgressDialog(KotItemlist.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            pd.dismiss();
                if(s.equals("Credit exceeds"))
            {
                if(cardType.equals("CASH CARD")&& isCashCardFilled.equalsIgnoreCase("True"))
                {
                    showPopUp();

                }else if(cardType.equals("SMART CARD")){
                    showPopUp();
                }else{
                    Toast.makeText(getApplicationContext(),"Credit limit exceeds", Toast.LENGTH_LONG).show();
                }

            }
            else if(s.contains("true"))
            {
                Toast.makeText(getApplicationContext(),"Order placed", Toast.LENGTH_SHORT).show();
                Intent i=new Intent(KotItemlist.this, PlaceOrder_bill.class);
                i.putExtra("mName",Nmae);
                i.putExtra("a/c", acc);
                i.putExtra("tableNo",tableNo);
                i.putExtra("tableId",tableId);
                i.putExtra("bill_id","null");
                i.putExtra("Type","b");
                i.putExtra("position",position);
                i.putExtra("waiternamecode",waiternamecode);


                startActivity(i);
                finish();
            }else if(s.contains("Unable to connect to the remote server")){
                Toast.makeText(getApplicationContext(), "Order placed -- > POS system is off", Toast.LENGTH_SHORT).show();
                Intent i=new Intent(KotItemlist.this, PlaceOrder_bill.class);
                    i.putExtra("mName",Nmae);
                    i.putExtra("a/c", acc);
                    i.putExtra("tableNo",tableNo);
                    i.putExtra("tableId",tableId);
                    i.putExtra("bill_id","null");
                    i.putExtra("Type","b");
                    i.putExtra("position",position);
                    i.putExtra("waiternamecode",waiternamecode);
                    startActivity(i);
                finish();
            }
            else if(s.contains("No item"))
            {
                Toast.makeText(getApplicationContext(),"No item is ordered-->"+s, Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Order not placed-->"+s, Toast.LENGTH_LONG).show();
            }
        }
    }

    protected class AsyncPlaceOrder3 extends AsyncTask<String, Void, String> {
        ArrayList<String> credit = null;
        String status = "";
        String creditAccountno = "";
        String creditName = "";
        String storeId = sharedpreferences.getString("storeId", "");
        String loginname = sharedpreferences.getString("loginName", "");
        String userId = sharedpreferences.getString("userId", "");
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        String result = "";
        String pageType = "";

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(KotItemlist.this);
            try {
                Totalamount=0;
                AndroidTempbilling obj = new AndroidTempbilling();
                //ArrayList<AndroidTempbilldetails> itemList=new ArrayList<>();
                ArrayList<Object> itemList_main = new ArrayList<>();
                DecimalFormat _format=new DecimalFormat("#.##");
                placeOrder_list = new ArrayList<>();
                AndroidTempbilldetails ob = null;
                ItemList li = null;
                if (itemView != null)
                {
                    //itemView.bringChildToFront(v);
                    int childRow = itemView.getChildCount();
                    for (int i = 0; i < childRow; i++) {
                        View ItemRow = itemView.getChildAt(i);
                        TextView t = (TextView) ItemRow.findViewById(R.id.kot_itemCount);
                        TextView t1 = (TextView) ItemRow.findViewById(R.id.kot_itemname);
                        String Itemcount = t.getText().toString();
                        float salesTax1 = 0;
                        float serviceTax1 = 0;
                        float cessTax1= 0;
                        float vat1=0;

                        if (!((Itemcount.equals("")) || (Itemcount.equals("0")))) {

                            String itemname = t1.getText().toString();
                            String[] spl = itemname.split("-");
                            String _itemCode = spl[0];

                            List<String> itemId = db.getItemid(_itemCode);
                            // String  itemRate=db.getItemRate(_itemCode);
                            String _itemName = itemId.get(3);

                            ob = new   AndroidTempbilldetails();
                            li = new ItemList();
                            float salesTax = 0;
                            float serviceTax = 0;
                            float cessTax = 0;
                            float vat=0;
                            ArrayList<String> dummy=new ArrayList<String>();
                            ob.setSalestax(0);  // unwanted
                            ob.setServicetax(0);
                            ob.setCesstax(0);
                            ob.setOtno("");
                            ob.setTaxId(0);
                            ob.setTaxRate(0);
                            ob.setAlltax(dummy);
                            ob.setSalesUintid(Integer.parseInt(itemId.get(2)));
                            ob.setQuantity(Integer.parseInt(Itemcount));
                            ob.setRate(itemId.get(1));
                            ob.setItemcode(_itemCode);
                            ob.setItemname(_itemName);
                            ob.setItemId(itemId.get(0));
                            ob.setItemCategoryid(itemId.get(4));
                            int v1 = Integer.parseInt(Itemcount);
                            float v2 = Float.parseFloat(itemId.get(1));
                            float v3 = v1 * v2;
                            ob.setAmount(v3);
                            li.setAmount(v3 + "");
                            li.setServiceTax("0");
                            li.setVat("0");
                            li.setSalestTax("0");
                            li.setItemCode(_itemCode);
                            li.setItemName(_itemName);
                            li.setQuantity(Itemcount);
                            li.setAmount(Float.toString(v3));  //with discounted value
                            li.setCardType(cardType);
                            li.setRate(itemId.get(1));
                            li.setMemName(Nmae);
                            li.setMemCode(acc);
                            /*li.setServiceTax(Float.toString(0));
                            li.setSalestTax(Float.toString(0));
                            li.setCesstax(Float.toString(0));*/
                            Totalamount = Totalamount + ob.getAmount()+ob.getSalestax()+ob.getServicetax()+ob.getCesstax();
                            itemList_main.add(ob);
                            placeOrder_list.add(li);
                            obj.setMemberId(memberId);
                            obj.setStoreId(storeId);
                            obj.setTableId(tableId + "");
                            //   obj.setTamount(Float.parseFloat(_format.format(Totalamount)));
                            obj.setTamount(Float.valueOf(_format.format(Totalamount)));
                        }

                    }
                }

                if (itemList_main.size() != 0)
                {
                    //String  limit = (cLimit.equals(""))?"0":cLimit;
                    String  limit = (TakeOrder.c_Limit.equals(""))?"0":TakeOrder.c_Limit;
                    if(!cardType.equals("CASH CARD"))
                    {
                        if (C.equals("-0") || TakeOrder.check.equals("False")||C.equals("-0.0")) {
                            JSONObject jsonObject = api.cms_placeOrder3_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,params[0],waiterid);
                            status = jsonObject.toString();
                            result = jsonObject.optString("Value");

                            JSONArray array = jsonObject.getJSONArray("Value");
                            CloseorderItems cl = null;

                            for (int i = 0; i < array.length(); i++) {
                                cl = new CloseorderItems();
                                JSONObject object = array.getJSONObject(i);

                                String ItemID = object.getString("ItemID");
                                String ItemCode = object.getString("ItemCode");
                                String Quantity = object.getString("Quantity");
                                String Amount = object.getString("Amount");
                                String BillID = object.getString("BillID");
                                String taxDescription =object.optString("taxDescription");
                                String mAcc = object.getString("memberAcc");
                                String taxValue=object.getString("taxValue");
                                String ava_balance = object.optString("avBalance");
                                String billNo = object.optString("billnumber");
                                String taxAmount =object.optString("taxAmount");
                                String Rate = object.getString("rate");
                                String itemname = object.getString("itemname");
                                String mName = object.getString("membername");
                                String memberId = object.getString("memberId");
                                String BillDate = object.getString("BillDate");
                                String ACCharge = object.getString("ACCharge");
                                String otNo=object.getString("otno");
                                String avBalance = object.getString("avBalance");
                                String opBalance=db.getOpeningBalance(mAcc);
                                cl.setItemID(ItemID);
                                cl.setItemCode(ItemCode);
                                cl.setQuantity(Quantity);
                                cl.setAmount(Amount);
                                cl.setBillID(BillID);
                                cl.setTaxDescription(taxDescription);
                                cl.setmAcc(mAcc);
                                cl.setTaxValue(taxValue);
                                cl.setAva_balance(ava_balance);
                                cl.setBillno(billNo);
                                cl.setTaxAmount(taxAmount);
                                cl.setmName(mName);
                                cl.setMemberID(memberId);
                                cl.setBillDate(BillDate);
                                cl.setRate(Rate);
                                cl.setItemname(itemname);
                                cl.setOtno(otNo);
                                cl.setACCharge(ACCharge);
                                cl.setAva_balance(avBalance);
                                cl.setCardType(cardType);
                                cl.setOpeningBalance(opBalance);
                                closeorderItemsArrayList_Others.add(cl);
                            }


                        }

                        else if (Totalamount <= (Double.parseDouble(limit) - Float.parseFloat(C))) {
                            JSONObject jsonObject = api.cms_placeOrder3_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,params[0],waiterid);
                            status = jsonObject.toString();
                            result = jsonObject.optString("Value");


                            JSONArray array = jsonObject.getJSONArray("Value");
                            CloseorderItems cl = null;

                            for (int i = 0; i < array.length(); i++) {
                                cl = new CloseorderItems();
                                JSONObject object = array.getJSONObject(i);

                                String ItemID = object.getString("ItemID");
                                String ItemCode = object.getString("ItemCode");
                                String Quantity = object.getString("Quantity");
                                String Amount = object.getString("Amount");
                                String BillID = object.getString("BillID");
                                String taxDescription =object.optString("taxDescription");
                                String mAcc = object.getString("memberAcc");
                                String taxValue=object.getString("taxValue");
                                String ava_balance = object.optString("avBalance");
                                String billNo = object.optString("billnumber");
                                String taxAmount =object.optString("taxAmount");
                                String Rate = object.getString("rate");
                                String itemname = object.getString("itemname");
                                String mName = object.getString("membername");
                                String memberId = object.getString("memberId");
                                String BillDate = object.getString("BillDate");
                                String avBalance = object.getString("avBalance");
                                String otNo=object.getString("otno");
                                String ACCharge = object.getString("ACCharge");
                                cl.setItemID(ItemID);
                                cl.setItemCode(ItemCode);
                                cl.setQuantity(Quantity);
                                cl.setAmount(Amount);
                                cl.setBillID(BillID);
                                cl.setTaxDescription(taxDescription);
                                cl.setmAcc(mAcc);
                                cl.setTaxValue(taxValue);
                                cl.setAva_balance(ava_balance);
                                cl.setBillno(billNo);
                                cl.setTaxAmount(taxAmount);
                                cl.setmName(mName);
                                cl.setMemberID(memberId);
                                cl.setBillDate(BillDate);
                                cl.setRate(Rate);
                                cl.setAva_balance(avBalance);
                                cl.setACCharge(ACCharge);
                                cl.setItemname(itemname);

                                cl.setOtno(otNo);

                                cl.setCardType(cardType);

                                closeorderItemsArrayList_Others.add(cl);
                            }



                        } else {
                            status = "Credit exceeds";
                        }

                    }

                    else{
                        if(Float.parseFloat(C)-Totalamount >0){
                            JSONObject jsonObject = api.cms_placeOrder3_new(obj, itemList_main, serialNo, cardType, userId, storeId, manufacturer + model, referenceNo,params[0],waiterid);
                            status = jsonObject.toString();
                            result = jsonObject.optString("Value");

                            JSONArray array = jsonObject.getJSONArray("Value");
                            CloseorderItems cl = null;

                            for (int i = 0; i < array.length(); i++) {
                                cl = new CloseorderItems();
                                JSONObject object = array.getJSONObject(i);

                                String ItemID = object.getString("ItemID");
                                String ItemCode = object.getString("ItemCode");
                                String Quantity = object.getString("Quantity");
                                String Amount = object.getString("Amount");
                                String BillID = object.getString("BillID");
                                String taxDescription =object.optString("taxDescription");
                                String mAcc = object.getString("memberAcc");
                                String taxValue=object.getString("taxValue");
                                String ava_balance = object.optString("avBalance");
                                String billNo = object.optString("billnumber");
                                String taxAmount =object.optString("taxAmount");
                                String Rate = object.getString("rate");
                                String itemname = object.getString("itemname");
                                String mName = object.getString("membername");
                                String memberId = object.getString("memberId");
                                String BillDate = object.getString("BillDate");
                                String otNo=object.getString("otno");
                                String ACCharge = object.getString("ACCharge");
                                cl.setItemID(ItemID);
                                cl.setItemCode(ItemCode);
                                cl.setQuantity(Quantity);
                                cl.setAmount(Amount);
                                cl.setBillID(BillID);
                                cl.setTaxDescription(taxDescription);
                                cl.setmAcc(mAcc);
                                cl.setTaxValue(taxValue);
                                cl.setAva_balance(ava_balance);
                                cl.setBillno(billNo);
                                cl.setTaxAmount(taxAmount);
                                cl.setmName(mName);
                                cl.setMemberID(memberId);
                                cl.setBillDate(BillDate);
                                cl.setRate(Rate);
                                cl.setItemname(itemname);
                                cl.setACCharge(ACCharge);
                                cl.setOtno(otNo);
                                cl.setCardType(cardType);

                                closeorderItemsArrayList_Others.add(cl);
                            }


                        }else{
                            status = "Credit exceeds";
                        }

                    }
                } else {
                    status = "No item";
                }

            } catch (Exception e) {
                e.printStackTrace();
                status = e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(KotItemlist.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s.equals("Credit exceeds")) {

                if(cardType.equals("CASH CARD") && isCashCardFilled.equalsIgnoreCase("True"))
                {
                    showPopUp();

                }else{
                    Toast.makeText(getApplicationContext(), "Credit limit exceeds : Total Amount"+Totalamount +"\n"+"Opening Balance:"+C, Toast.LENGTH_LONG).show();
                }

            } else if (s.contains("true")) {
               String posid=sharedpreferences.getString("storeId", "");
                db.deleteAccount(memberId,"7",userId);

                Toast.makeText(getApplicationContext(),"Order placed", Toast.LENGTH_LONG).show();
              /*  Intent i=new Intent(KotItemlist.this, Placebill_bakery.class);
                i.putExtra("mName",Nmae);
                i.putExtra("a/c", acc);
                i.putExtra("tableNo",tableNo);
                i.putExtra("tableId",tableId);
                i.putExtra("bill_id", result);
                i.putExtra("Type", "k");
                startActivity(i);*/

                Intent i = new Intent(KotItemlist.this, CloseOrderdetails.class);
                i.putExtra("pageValue", 23);
                i.putExtra("creditAno", acc);
                i.putExtra("creditName", Nmae);
                i.putExtra("tableNo", tableNo);
                i.putExtra("tableId", tableId);
                i.putExtra("bill_id", result);
                i.putExtra("position",position);
                i.putExtra("cardType",cardType);
                i.putExtra("waiternamecode",waiternamecode);

                startActivity(i);
                finish();
            }else if(s.contains("Unable to connect to the remote server")){
                Toast.makeText(getApplicationContext(), "Order placed -- > POS system is off", Toast.LENGTH_LONG).show();
                String posid=sharedpreferences.getString("storeId", "");
                db.deleteAccount(memberId,"7",userId);
                Intent i = new Intent(KotItemlist.this, CloseOrderdetails.class);
                i.putExtra("pageValue", 23);
                i.putExtra("creditAno", acc);
                i.putExtra("creditName", Nmae);
                i.putExtra("tableNo", tableNo);
                i.putExtra("tableId", tableId);
                i.putExtra("bill_id", result);
                i.putExtra("position",position);
                i.putExtra("cardType",cardType);
                i.putExtra("waiternamecode",waiternamecode);

                startActivity(i);
                finish();
            }
            else if (s.contains("No item")) {
                Toast.makeText(getApplicationContext(), "No item is ordered-->" + s, Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(getApplicationContext(), "Order not placed-->" + s, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showPopUp() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(c);
        alertDialog.setMessage("Credit Limit Exceeds"+"\n Total amount :"+ Totalamount +"\n Credit Limit :"+C);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                return;
            }
        });
        alertDialog.create();
        alertDialog.show();
    }

    public void alertCustomizedLayout() {


        builder = new AlertDialog.Builder(KotItemlist.this).setView(searchView)
                .setTitle("")
                .setNegativeButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        additems(2);
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }

                })
                .show();

        builder.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {

                        String itemName = searchItemname.getText().toString();
                        String count = searchCount.getText().toString();
                            searchItemname.setLongClickable(false);
                            searchItemname.setTextIsSelectable(false);
                            searchItemname.setMovementMethod(null);
                         if (!(itemName.equals("") || count.equals("") || count.equals("0"))) {
                            if(itemName.contains("_"))
                            {
                                String array[] = itemName.split("_");
                               Long rvalur = db.insertOT(array[1], array[0], count);
                                //Long rvalur = db.insertOT1(array[1], array[0], count);
                                if (rvalur < 0) {
                                    Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                }else{
                                    Toast.makeText(KotItemlist.this, "Item  added", Toast.LENGTH_SHORT).show();
                                    searchItemname.setText("");
                                    searchCount.setText("");
                                    searchItemname.setEnabled(true);
                                    search.setVisibility(View.GONE);
                                    searchItemname.setFocusable(true);
                                    searchItemname.requestFocus();
                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.showSoftInput(searchItemname, InputMethodManager.SHOW_IMPLICIT);
                                }
                            }else{
                                Toast.makeText(KotItemlist.this, "Give Valid Item Name", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "Dont give empty values", Toast.LENGTH_LONG).show();
                        }

                /*additems(2);
                builder.cancel();*/
            }
        });

        builder.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                int count=db.getItemList1(groupId);
                if(count==0){
                    additems(1);
                    builder.cancel();
                }else{
                    additems(2);
                    builder.cancel();
                }


            }
        });
//        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface dialog) {
//                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
//                Button c = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
//                b.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View view) {
//                        // TODO Do something
//
//                        //Dismiss once everything is OK.
//                        additems(2);
//                        alertDialog.dismiss();
//                    }
//                });
//                c.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View view) {
//                        // TODO Do something
//
//                        //Dismiss once everything is OK.
//                        String itemName = searchItemname.getText().toString();
//                        String count = searchCount.getText().toString();
//                        if (!(itemName.equals("") || count.equals("") || count.equals("0"))) {
//                            String array[] = itemName.split("-");
//                            Long rvalur = db.insertOT(array[0], array[1], count);
//                            if (rvalur < 0) {
//                                Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
//                            }
//                        } else {
//                            Toast.makeText(getApplicationContext(), "Dont give empty values", Toast.LENGTH_LONG).show();
//                        }
//
//                    }
//                });
//            }
//        });

//                // action buttons
//                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//                        // your sign in code here
//                        additems(2);
//                        dialog.dismiss();
//                    }
//                })
//                .setNegativeButton("Save", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        // remove the dialog from the screen
//                        String itemName = searchItemname.getText().toString();
//                        String count = searchCount.getText().toString();
//                        if (!(itemName.equals("") || count.equals("") || count.equals("0"))) {
//                            String array[] = itemName.split("-");
//                            Long rvalur = db.insertOT(array[0], array[1], count);
//                            if (rvalur<0)
//                            {
//                                Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
//                            }
//                        } else {
//                            Toast.makeText(getApplicationContext(), "Dont give empty values", Toast.LENGTH_LONG).show();
//                        }
//
//
//                    }
//                })
               // .show();

    }

    public void onclick(View v) {
        searchItemname.setText("");
        search.setVisibility(View.GONE);
        searchItemname.setEnabled(true);
        searchItemname.requestFocus();
        InputMethodManager imm=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(searchItemname, InputMethodManager.SHOW_IMPLICIT);

    }

    public void additems(int value) {
        ArrayList<String> list=null;
        noOfCounts=0;

        if(value==1)
        {
            list = db.getItemList(groupId);
        }
        else
        {
            // itemView.removeAllViews();
            itemView=null;
            view=null;
            itemName=null;
            itemCount=null;
            add=null;
            subtract=null;
            happyHour=null;
            list = db.getOTItemList();
            if(!list.isEmpty()){
                placeOrder.setVisibility(View.VISIBLE);
            }else{
                placeOrder.setVisibility(View.GONE);
            }
        }

        for (int i = 0; i < list.size(); i++)
        {
            noOfCounts=0;
            if (itemView == null)
            {
                itemView = (LinearLayout) findViewById(R.id.itemView);
                view = getLayoutInflater().inflate(R.layout.itemlist_kot, itemView, false);
                itemName = (TextView) view.findViewById(R.id.kot_itemname);
                itemCount = (TextView) view.findViewById(R.id.kot_itemCount);
                //happyHour=(CheckBox)view.findViewById(R.id.happyhour_checbox);
               // happyHour.setVisibility(View.GONE);
                add = (ImageView) view.findViewById(R.id.kot_add);
                subtract = (ImageView) view.findViewById(R.id.kot_minus);

                if(!list.isEmpty()){
                    txt_itemcount.setText(list.size()+"");
                }else{
                    txt_itemcount.setText("0");
                }
            } else {
                view = getLayoutInflater().inflate(R.layout.itemlist_kot, itemView, false);
                itemName = (TextView) view.findViewById(R.id.kot_itemname);
                itemCount = (TextView) view.findViewById(R.id.kot_itemCount);
                //happyHour=(CheckBox)view.findViewById(R.id.happyhour_checbox);
                happyHour.setVisibility(View.GONE);
                add = (ImageView) view.findViewById(R.id.kot_add);
                subtract = (ImageView) view.findViewById(R.id.kot_minus);
                itemView = (LinearLayout) findViewById(R.id.itemView);

                if(!list.isEmpty() ) {
                    txt_itemcount.setText(list.size()+"");
                }else{
                    txt_itemcount.setText("0");
                }
            }

//            happyHour.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    ViewGroup parent = (ViewGroup) v.getParent();
//                    TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
//                    TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);
//                    //CheckBox happy=(CheckBox)parent.findViewById(R.id.happyhour_checbox);
//
//                    String _itemname = t2.getText().toString();
//                    String count = t1.getText().toString();
//
//
//                    if (_itemname.contains("-"))
//                    {
//                        String array[] = _itemname.split("-");
//                        if(happy.isChecked()){
//                            Long rvalur = db.insertOT_tempHappyHour(array[0], array[1], "1");
//                            if (rvalur < 0) {
//                                Toast.makeText(getApplicationContext(), "happyHour not set", Toast.LENGTH_LONG).show();
//                            }
//                        }else {
//                            Long rvalur = db.insertOT_tempHappyHour(array[0], array[1], "0");
//                            if (rvalur < 0) {
//                                Toast.makeText(getApplicationContext(), "happyHour not set", Toast.LENGTH_LONG).show();
//                            }
//                        }
//
//                    }
//                }
//            });



            itemName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int id = v.getId();
                    ViewGroup parent = (ViewGroup) v.getParent();
                    TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                    TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);

                    String _itemname = t2.getText().toString();
                    if (!_itemname.equals("")|| _itemname != null ){
                        String[] ss=_itemname.split("-");
                        String ii=ss[0];

                        String _itemcode = t1.getText().toString();
                        Intent i=new Intent(KotItemlist.this,Alert_ItemDescription.class);
                        i.putExtra("itemName",_itemname);
                        i.putExtra("itemCode",ii);
                        startActivity(i);
                    }else {
                        Toast.makeText(c, "No Item", Toast.LENGTH_SHORT).show();
                    }

                }
            });



            add.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {

                                           int id = v.getId();
                                           ViewGroup parent = (ViewGroup) v.getParent();
                                           TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                                           TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);

                                           String _itemname = t2.getText().toString();
                                           String _itemcode = t1.getText().toString();
                                           String count = t1.getText().toString();

                                           if (count.equals("")) {
                                               t1.setText("1");
                                               noOfCounts = noOfCounts + 1;
                                               // txt_itemcount.setText(noOfCounts + "");

                                               if (_itemname.contains("-"))
                                               {
                                                   String array[] = _itemname.split("-");
                                                   Long rvalur = db.insertOT(array[0], array[1], "1");
                                                   if (rvalur < 0) {
                                                       Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                   }else{

                                                   }

                                               }

                                           } else {
                                               int c = Integer.parseInt(count);
                                               t1.setText(c + 1 + "");
                                               int co=c+1;
                                               noOfCounts = noOfCounts + 1;

                                               if (_itemname.contains("-")) {
                                                   String array[] = _itemname.split("-");
                                                   Long rvalur = db.insertOT(array[0], array[1], Integer.toString(co));
                                                   if (rvalur < 0) {
                                                       Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                   }else{

                                                   }

                                               }
                                               //  txt_itemcount.setText(noOfCounts + "");
                                           }


                                       }
                                   }

            );
            subtract.setOnClickListener(new View.OnClickListener()

                                        {
                                            @Override
                                            public void onClick (View v)
                                            {
                                                try {
                                                    ViewGroup parent = (ViewGroup) v.getParent();
                                                    TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                                                    TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);

                                                    String _itemname = t2.getText().toString();
                                                    String _itemcode = t1.getText().toString();

                                                    String count = t1.getText().toString();
                                                    if (count.equals("")) {
                                                        Toast.makeText(getApplicationContext(), "Add item first", Toast.LENGTH_LONG).show();
                                                    } else {
                                                        int c = Integer.parseInt(count);
                                                        if (c != 0) {
                                                            t1.setText(c - 1 + "");
                                                            noOfCounts = noOfCounts - 1;
                                                            int co=c - 1;

                                                            if (_itemname.contains("-")) {
                                                                String array[] = _itemname.split("-");
                                                                Long rvalur = db.insertOT(array[0], array[1], Integer.toString(co));
                                                                if (rvalur < 0) {
                                                                    Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                                                }else{

                                                                }

                                                            }

                                                            // txt_itemcount.setText(noOfCounts + "");
                                                        }
                                                    }



                                                } catch (Exception e) {
                                                    String hh = e.getMessage().toString();
                                                }

                                            }
                                        }

            );
            //itemName.setId(i);
            // itemCount.setId(i);
            //add.setId(i);
            //subtract.setId(i);
            String ll[] = list.get(i).toString().split("\\$");
            if(ll.length>1)

            {
                itemName.setText(ll[0]);
                itemCount.setText(ll[1]);
                noOfCounts = noOfCounts + Integer.parseInt(ll[1]);
                if (!list.isEmpty()) {
                    txt_itemcount.setText(list.size() + "");
                } else {
                    txt_itemcount.setText("0");
                }

            } else {
                itemName.setText(ll[0]);
            }
                /*if(ll.length()==1)
                {
                    try
                    {
                        alpahabet=new TextView(KotItemlist.this);
                        alpahabet.setText(ll);
                        View  A=alpahabet;
                        itemView.addView(A);
                    }
                    catch (Exception e)
                    {
                        String mm=e.getMessage().toString();
                    }
                }
                else
                {
                    itemName.setText(ll);
                    itemView.addView(view);
                }*/
            itemView.addView(view);
        }
        //itemView.setVisibility(View.VISIBLE);
    }

    protected class AsyncSearch extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            try {

                if (itemView != null)
                {

                    //TODO remove the commented lines
                   /* int childRow = itemView.getChildCount();
                    for (int i = 0; i < childRow; i++)
                    {
                        View ItemRow = itemView.getChildAt(i);
                        TextView t = (TextView) ItemRow.findViewById(R.id.kot_itemCount);
                        TextView t1 = (TextView) ItemRow.findViewById(R.id.kot_itemname);
                        String Itemcount = t.getText().toString();
                        if (!((Itemcount.equals("")) || (Itemcount.equals("0"))))
                        {
                            String itemname = t1.getText().toString();
                            String[] spl = itemname.split("-");
                            String _itemCode = spl[0];
                            db.insertOT(_itemCode,spl[1],Itemcount);
                        }
                    }*/
                }



            } catch (Exception e) {
                e.printStackTrace();

            }
            return "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(KotItemlist.this);
            pd.show();
            pd.setMessage("Searching please wait ....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            itemView.removeAllViews();
            //additems(2);
            searchView = inflater.inflate(R.layout.search_dialog, null);
            searchItemname = (MaterialAutoCompleteTextView) searchView.findViewById(R.id.autoCompleteTextView_party);
            searchCount= (MaterialEditText) searchView.findViewById(R.id.eText_qty);
            myadapter=new ItemnameSearch(KotItemlist.this, groupId);
            searchItemname.setAdapter(myadapter);
            searchItemname.setThreshold(1);
            search= (ImageView) searchView.findViewById(R.id.imageView_close1);
            search.setVisibility(View.GONE);
            alertCustomizedLayout();
            searchItemname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    dummy = myadapter.getItem(position).toString();
                }
            });
                      /*  searchItemname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                dummy = myadapter.getItem(position).toString();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });*/

            searchItemname.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                }


                @Override
                public void afterTextChanged(Editable s) {
                    if (!dummy.equals("")) {
                        searchItemname.setEnabled(false);
                        search.setVisibility(View.VISIBLE);
                    } else {
                        searchItemname.setEnabled(true);
                        search.setVisibility(View.GONE);
                    }

                }
            });

            pd.dismiss();

        }
    }

    protected class AsyncCloseOrder extends AsyncTask<String, Void, String> {
        String status = "";
        String storeId = sharedpreferences.getString("storeId", "");
        String deviceid = sharedpreferences.getString("deviceId", "");
        String userId = sharedpreferences.getString("userId", "");
        String billtype=sharedpreferences.getString("BillMode","");
        String mstoreid=sharedpreferences.getString("mstoreId","");
        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(KotItemlist.this);
            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String formattedDate = df.format(c.getTime());

                JSONObject jsonObject = api.cms_closeOrder(accountNo, deviceid, storeId, formattedDate, Integer.parseInt(params[0]), userId, cardType,"","","",mstoreid,billtype,0,"P",waiterid);
                status = jsonObject.toString();

            } catch (Exception e) {
                e.printStackTrace();
                status = e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(KotItemlist.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s.contains("true")) {
                Toast.makeText(getApplicationContext(), "Order closed ", Toast.LENGTH_LONG).show();
                finish();
                Intent i = new Intent(KotItemlist.this, CloseOrderdetails.class);
                i.putExtra("pageValue", 0);
                i.putExtra("position",position);
                i.putExtra("waiternamecode",waiternamecode);

                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(), "Order not closed-->" + s, Toast.LENGTH_LONG).show();
            }
        }
    }

    protected class AsyncCheckstockAvalability extends AsyncTask<String, Void, String> {
        String status = "";
        String storeId = sharedpreferences.getString("storeId", "");
        //String deviceid=sharedpreferences.getString("deviceId", "");
        //String userId=sharedpreferences.getString("userId","");
        ArrayList<Item> list = new ArrayList<Item>();

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(KotItemlist.this);
            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());

                JSONObject jsonObject =null; //api.cms_checkStockavailabilty(storeId, params[0], formattedDate);
                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                Item ii = null;
                for (int i = 0; i < jsonArray.length(); i++) {
                    ii = new Item();
                    JSONObject object = jsonArray.getJSONObject(i);
                    String iname = object.getString("ItemName");
                    String iCode = object.getString("ItemCode");
                    String itemStock = object.getString("stoock");
                    ii.setItemcode(iCode);
                 //   ii.setItemNmae(iname);  //TODO check
                  //  ii.setStock(itemStock);
                    list.add(ii);
                }
                status = jsonObject.toString();

            } catch (Exception e) {
                e.printStackTrace();
                status = e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(KotItemlist.this);
            pd.show();
            pd.setMessage("Please wait loading....");
            pd.setCancelable(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s.contains("true")) {
                for (int i = 0; i < list.size(); i++) {
                    if (itemView == null) {
                        itemView = (LinearLayout) findViewById(R.id.itemView);
                        view = getLayoutInflater().inflate(R.layout.itemlist_kot, itemView, false);
                        //itemlist= (LinearLayout)view.findViewById(R.id.kotItemlist);
                        itemName = (TextView) view.findViewById(R.id.kot_itemname);
                        itemCount = (TextView) view.findViewById(R.id.kot_itemCount);
                        add = (ImageView) view.findViewById(R.id.kot_add);
                        add.setId(i);
                        subtract = (ImageView) view.findViewById(R.id.kot_minus);

                    } else {
                        view = getLayoutInflater().inflate(R.layout.itemlist_kot, itemView, false);
                        // itemlist= (LinearLayout)view.findViewById(R.id.kotItemlist);
                        itemName = (TextView) view.findViewById(R.id.kot_itemname);
                        itemCount = (TextView) view.findViewById(R.id.kot_itemCount);
                        add = (ImageView) view.findViewById(R.id.kot_add);
                        add.setId(i);
                        subtract = (ImageView) view.findViewById(R.id.kot_minus);
                        itemView = (LinearLayout) findViewById(R.id.itemView);
                    }
                    add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int id = v.getId();

                            ViewGroup parent = (ViewGroup) v.getParent();
                            TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                            String count = t1.getText().toString();
                            if (count.equals("")) {

                                t1.setText("1");
                                noOfCounts = noOfCounts + 1;
                                txt_itemcount.setText(noOfCounts + "");
                            } else {
                                int c = Integer.parseInt(count) + 1;
                             //   String stock = list.get(id).getStock();  //TODO check
                               /* if (c <= Integer.parseInt(stock)) {
                                    t1.setText(c + "");
                                    noOfCounts = noOfCounts + 1;
                                    txt_itemcount.setText(noOfCounts + "");
                                } else {
                                    Toast.makeText(getApplicationContext(), "Item stock exceed:" + stock, Toast.LENGTH_LONG).show();
                                }*/

                            }


                        }
                    });
                    subtract.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                ViewGroup parent = (ViewGroup) v.getParent();
                                TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                                String count = t1.getText().toString();
                                if (count.equals("")) {

                                    Toast.makeText(getApplicationContext(), "Add item first", Toast.LENGTH_LONG).show();
                                } else {
                                    int c = Integer.parseInt(count);
                                    if (c != 0) {
                                        t1.setText(c - 1 + "");
                                        noOfCounts = noOfCounts - 1;
                                        txt_itemcount.setText(noOfCounts + "");
                                    }

                                }
                            } catch (Exception e) {
                                String hh = e.getMessage().toString();
                            }

                        }
                    });

                    //TODO check
                  // itemName.setText(list.get(i).getItemcode() + "-" + list.get(i).getItemNmae());
                    itemView.addView(view);
                }
            } else {
                Toast.makeText(getApplicationContext(), "STOCK CHECK-->" + s, Toast.LENGTH_LONG).show();
            }
        }
    }


}
