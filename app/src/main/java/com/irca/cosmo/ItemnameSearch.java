package com.irca.cosmo;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.irca.db.Dbase;

import java.util.ArrayList;
import java.util.List;


public class ItemnameSearch extends BaseAdapter implements Filterable{
	
	Context contxt;	
	List<String> relablesList =new ArrayList<String>();
	List<String> relablesList1 =new ArrayList<String>();
	String ItemcatId="";
	public ItemnameSearch(Context c, String itemcatergoryId)
	{
		contxt=c;
		ItemcatId=itemcatergoryId;
	}

	@Override
	public int getCount() {
		return relablesList.size();
	}

	@Override
	public Object getItem(int position) {
		return relablesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{

		try{
			if (convertView == null)
			{
				LayoutInflater inflater = (LayoutInflater) contxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.autocomplete, parent, false);
			}
			TextView t=(TextView) convertView.findViewById(R.id.text1);
			t.setTextColor(Color.BLACK);

			if(!relablesList.isEmpty()){
				t.setText(relablesList.get(position));
			}else{
				t.setText("");
			}
			t.setEnabled(false);

		}catch (Exception e){
			String r=e.getMessage().toString();
		}
		return convertView;
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() 
		 {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) 
			{
				 FilterResults filterResults = new FilterResults();

                try
				{
                       if (constraint != null || constraint.length() == 0)
                       {

                        StringBuilder sb = new StringBuilder(constraint.length());
                        sb.append(constraint);
                        String userInput= sb.toString();
                        Dbase db=new Dbase(contxt);

                               relablesList=db.getSearchItemlist(userInput, ItemcatId);
                               filterResults.values = relablesList;
                               filterResults.count = relablesList.size();
                             //  notifyDataSetChanged();

                       }
                }catch(Exception e){
                   // Toast.makeText(contxt,""+e.getMessage(),Toast.LENGTH_LONG).show();
                    String r=e.getMessage().toString();
                }
               
               return filterResults;
			}
			@Override
			protected void publishResults(CharSequence constraint,FilterResults results) 
			{
				try {
                    relablesList1 = (ArrayList<String>) results.values;

					if (results != null && results.count > 0)
					{
                        notifyDataSetChanged();
					}
					else
					{
                        notifyDataSetInvalidated();
                    }

				}catch (Exception e){
                   // Toast.makeText(contxt,""+e.getMessage(),Toast.LENGTH_LONG).show();
					String r =e.getMessage().toString();
				}
				
			}
		
		 };
		return filter;

	}

}
