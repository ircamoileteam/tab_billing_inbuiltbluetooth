package com.irca.cosmo;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.Billing.GroupView;
import com.irca.Billing.Itemview;
import com.irca.Billing.MakeOrder;
import com.irca.MaterialFloatLabel.MaterialAutoCompleteTextView;
import com.irca.MaterialFloatLabel.MaterialEditText;
import com.irca.db.Dbase;

import java.util.List;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by Archanna on 5/26/2016.
 */
public class Search_Item extends AppCompatActivity {
    MaterialAutoCompleteTextView searchItemname;
    MaterialEditText searchCount;
    Button save, close;
    ItemnameSearch myadapter;
    ImageView search;
    Dbase db;
    Bundle bundle;
    int tableId;
    String table = "0";
    String creditLimit, debitbal, creditAccountno,accesstype, creditName, memberId, cardType, _params, waiterid, waiternamecode, paxcount;
    int position;
    //  TextView title;
    LinearLayout temView;
    SharedPreferences sharedpreferences;
    String itemCategoryId = "";
    String billmode = "";
    String StoreName = "";
    String idob = "";
    String idoa = "";
    String bill = "",TempBillAmount = "0.0";
    LinearLayout nameSearch, menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.search_item);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        bill = sharedpreferences.getString("Bill", "");
        searchItemname = (MaterialAutoCompleteTextView) findViewById(R.id.autoCompleteTextView_party);
        nameSearch = (LinearLayout) findViewById(R.id.image);
        nameSearch.setVisibility(View.GONE);
        searchCount = (MaterialEditText) findViewById(R.id.eText_qty);
        save = (Button) findViewById(R.id.save);
        close = (Button) findViewById(R.id.close);
        search = (ImageView) findViewById(R.id.imageView_close1);
        // title = (TextView)findViewById(R.id.logo);
        StoreName = sharedpreferences.getString("StoreName", "");
        itemCategoryId = sharedpreferences.getString("itemCategoryId", "");//century
        String storeid = sharedpreferences.getString("storeId", "");//nsci
        menu = (LinearLayout) findViewById(R.id.menu);
        actionBarSetup();
        // title.setText(StoreName + " - Item Search");
        // myadapter=new ItemnameSearch(Search_Item.this, itemCategoryId);//century
        myadapter = new ItemnameSearch(Search_Item.this, storeid);//nsci
        searchItemname.setAdapter(myadapter);
        searchItemname.setThreshold(1);
        search.setVisibility(View.GONE);
        db = new Dbase(Search_Item.this);
        temView = (LinearLayout) findViewById(R.id.dyn_tempView);
        temView.removeAllViews();
        bundle = getIntent().getExtras();
        table = bundle.getString("tableNo");
        creditLimit = bundle.getString("creditLimit", creditLimit);
        debitbal = bundle.getString("debitbal", debitbal);
        creditAccountno = bundle.getString("creditAno", creditAccountno);
        creditName = bundle.getString("creditName", creditName);
        memberId = bundle.getString("memberId", memberId);
        tableId = bundle.getInt("tableId", tableId);
        accesstype = bundle.getString("accessType");
        cardType = bundle.getString("cardType", cardType);
        _params = bundle.getString("referenceNo", _params);
        Log.d("_params contractor",_params);
        waiterid = bundle.getString("waiterid", waiterid);
        position = bundle.getInt("position", position);
        waiternamecode = bundle.getString("waiternamecode", waiternamecode);
        billmode = bundle.getString("billmode");
        idob = bundle.getString("dob");
        idoa = bundle.getString("doa");
        paxcount = bundle.getString("paxcnt");
        TempBillAmount = bundle.getString("TempBillAmount");
        if (idoa != null && idob != null) {
            if (!idob.equals("0") || !idoa.equals("0")) {
                blink();
            }
        }

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Search_Item.this, GroupView.class);
                startActivity(intent);
            }
        });
        searchItemname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                searchItemname.setEnabled(false);
                search.setVisibility(View.VISIBLE);
                searchCount.setFocusable(true);
                searchCount.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(searchCount, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String itemName = searchItemname.getText().toString();
                String count = searchCount.getText().toString();
                // searchItemname.setLongClickable(false);
                //  searchItemname.setTextIsSelectable(false);
                //  searchItemname.setMovementMethod(null);
                if (!(itemName.equals("") || count.equals("") || count.equals("0"))) {
                    if (!count.equals("")) {
                        try {
                            float cnt = Float.parseFloat(count);
                            if (cnt > 0) {
                                if (itemName.contains("_")) {
                                    String array[] = itemName.split("_");
                                    //  Long rvalur = db.insertOT(array[1], array[0], count);
                                    try {
                                        String itemCategoryId = sharedpreferences.getString("itemCategoryId", "");//century
                                        String storeId = sharedpreferences.getString("storeId", "");//century
                                        //String rate=db.getRate(array[1],itemCategoryId);//century
                                        String rate = db.getRate(array[1], storeId);//century
                                        long rvalur = db.insertOT1(array[1], array[0], count, rate);
                                        if (rvalur < 0) {
                                            Toast.makeText(getApplicationContext(), "Item not added", Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(Search_Item.this, "Item  added", Toast.LENGTH_SHORT).show();
                                            save.setText("Add Item");
                                            searchItemname.setText("");
                                            searchCount.setText("");
                                            searchItemname.setEnabled(true);
                                            search.setVisibility(View.GONE);
                                            searchItemname.setFocusable(true);
                                            searchItemname.requestFocus();
                                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                            imm.showSoftInput(searchItemname, InputMethodManager.SHOW_IMPLICIT);
                                            showTempView();
                                        }

                                    } catch (Exception e) {
                                        Toast.makeText(getApplicationContext(), "" + e.toString(), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(Search_Item.this, "Give Valid Item Name", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "Enter Proper values", Toast.LENGTH_LONG).show();
                        }


                    }
                } else {

                    Toast.makeText(getApplicationContext(), "Dont give empty values", Toast.LENGTH_LONG).show();
                }


            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent i=new Intent(Search_Item.this,PlaceOrder_new.class);
                Intent i = new Intent(Search_Item.this, MakeOrder.class);
                i.putExtra("tableNo", table);
                i.putExtra("creditLimit", creditLimit);
                i.putExtra("debitbal", debitbal);
                i.putExtra("creditAno", creditAccountno);
                i.putExtra("creditName", creditName);
                i.putExtra("memberId", memberId);
                i.putExtra("tableId", tableId);
                i.putExtra("cardType", cardType);
                i.putExtra("referenceNo", _params);
                i.putExtra("waiterid", waiterid);
                i.putExtra("position", position);
                i.putExtra("waiternamecode", waiternamecode);
                i.putExtra("billmode", billmode);
                i.putExtra("paxcnt", paxcount);
                i.putExtra("accessType", accesstype);
                i.putExtra("TempBillAmount", TempBillAmount);
                startActivity(i);
                finish();

            }
        });

        nameSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Search_Item.this, "Loading.....please wait", Toast.LENGTH_LONG).show();
                Intent i = new Intent(Search_Item.this, Itemview.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("tableNo", table);
                //i.putExtra("creditLimit", "-0");           // coupon no credit check in place order  so given -0to place order without validating for credit limit
                i.putExtra("creditLimit", creditLimit);
                i.putExtra("debitbal", debitbal);
                i.putExtra("creditAno", creditAccountno);
                i.putExtra("creditName", creditName);
                i.putExtra("memberId", memberId);
                i.putExtra("tableId", tableId);
                i.putExtra("cardType", cardType);
                i.putExtra("referenceNo", _params);
                i.putExtra("waiterid", waiterid);
                i.putExtra("position", position);
                i.putExtra("waiternamecode", waiternamecode);
                i.putExtra("billmode", billmode);
                i.putExtra("paxcnt", paxcount);
                i.putExtra("accessType", accesstype);
                i.putExtra("TempBillAmount", TempBillAmount);
                startActivity(i);
                finish();
            }
        });


    }

    public void onclick(View v) {
        searchItemname.setText("");
        search.setVisibility(View.GONE);
        searchItemname.setEnabled(true);
        searchItemname.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(searchItemname, InputMethodManager.SHOW_IMPLICIT);

    }

    private void showTempView() {
        temView.removeAllViews();
        List<String> labels = db.getTempOrderlist();
        for (int i = 0; i < labels.size(); i++) {
            String[] array = labels.get(i).split("#");
            LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View addview = inflater.inflate(R.layout.temp_view, null);
            TextView _itemName = (TextView) addview.findViewById(R.id.i_Name);
            TextView _itemcode = (TextView) addview.findViewById(R.id.i_Code);
            TextView _rate = (TextView) addview.findViewById(R.id.i_rate);
            _itemcode.setText("" + array[2]);
            _itemName.setText("" + array[1]);
            _rate.setText(array[3]);
            temView.addView(addview);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* Intent i=new Intent(Search_Item.this, Accountlist.class);
        i.putExtra("tableNo",table);
        i.putExtra("tableId",tableId);
        i.putExtra("waiterid",waiterid);
        i.putExtra("position",position);
        i.putExtra("waiternamecode",waiternamecode);
        startActivity(i);
        finish();*/

        Intent i = new Intent(Search_Item.this, MakeOrder.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("tableNo", table);
        i.putExtra("creditLimit", creditLimit);
        i.putExtra("debitbal", debitbal);
        i.putExtra("creditAno", creditAccountno);
        i.putExtra("creditName", creditName);
        i.putExtra("memberId", memberId);
        i.putExtra("tableId", tableId);
        i.putExtra("cardType", cardType);
        i.putExtra("referenceNo", _params);
        i.putExtra("waiterid", waiterid);
        i.putExtra("position", position);
        i.putExtra("waiternamecode", waiternamecode);
        i.putExtra("billmode", billmode);
        i.putExtra("paxcnt", paxcount);
        i.putExtra("accessType", accesstype);
        i.putExtra("TempBillAmount", TempBillAmount);
        startActivity(i);
        finish();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("" + StoreName + "  -  " + bill);
            ab.setElevation(0);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
//                Intent i = new Intent(Search_Item.this, PlaceOrder_new.class);
//                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                i.putExtra("tableNo", table);
//                i.putExtra("creditLimit", creditLimit);
//                i.putExtra("creditAno", creditAccountno);
//                i.putExtra("creditName", creditName);
//                i.putExtra("memberId", memberId);
//                i.putExtra("tableId", tableId);
//                i.putExtra("cardType", cardType);
//                i.putExtra("referenceNo", _params);
//                i.putExtra("waiterid", waiterid);
//                i.putExtra("position", position);
//                i.putExtra("waiternamecode", waiternamecode);
//                startActivity(i);
//                finish();
               /* Intent i=new Intent(Itemview.this, Accountlist.class);
                i.putExtra("tableNo",table);
                i.putExtra("tableId",tableId);
                i.putExtra("waiterid",waiterid);
                i.putExtra("position",position);
                i.putExtra("waiternamecode",waiternamecode);
                startActivity(i);
                finish();*/


                Intent i = new Intent(Search_Item.this, MakeOrder.class);
                i.putExtra("tableNo", table);
                i.putExtra("creditLimit", creditLimit);
                i.putExtra("creditAno", creditAccountno);
                i.putExtra("debitbal", debitbal);
                i.putExtra("creditName", creditName);
                i.putExtra("memberId", memberId);
                i.putExtra("tableId", tableId);
                i.putExtra("cardType", cardType);
                i.putExtra("referenceNo", _params);
                i.putExtra("waiterid", waiterid);
                i.putExtra("position", position);
                i.putExtra("waiternamecode", waiternamecode);
                i.putExtra("billmode", billmode);
                i.putExtra("dob", idob);
                i.putExtra("doa", idoa);
                i.putExtra("paxcnt", paxcount);
                i.putExtra("accessType", accesstype);
                i.putExtra("TempBillAmount", TempBillAmount);
                startActivity(i);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


    private void blink() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                //    int timeToBlink = 1000;    //in milissegunds
                int timeToBlink = 800;    //in milissegunds
                try {
                    Thread.sleep(timeToBlink);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        TextView txt = (TextView) findViewById(R.id.membday);
                        ImageView imgv = (ImageView) findViewById(R.id.bdayimg);
                        if (idob.equals("bday")) {


                            txt.setText("Today is " + creditName.toLowerCase() + " Birthday");
                        }
                        if (idoa.equals("aday")) {
                            txt.setText("Today is " + creditName.toLowerCase() + " Anniversary");
                        }
                        if (txt.getVisibility() == View.VISIBLE) {
                            txt.setVisibility(View.INVISIBLE);
                            imgv.setVisibility(View.INVISIBLE);
                        } else {
                            txt.setVisibility(View.VISIBLE);
                            imgv.setVisibility(View.VISIBLE);
                        }
                        blink();
                    }
                });
            }
        }).start();
    }
}
