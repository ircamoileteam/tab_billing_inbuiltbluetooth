package com.irca.cosmo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.Billing.Taxdataexcem;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.db.Dbase;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

//import io.fabric.sdk.android.Fabric;

public class TaxExcemption extends Activity {
    ProgressDialog pd;
    ArrayList<Taxdataexcem> taxdataexcems = new ArrayList<>();
    LinearLayout taxView;
    View view = null;
    Button save;
    Button cancel;
    TextView taxname, taxpercent;
    ConnectionDetector cd;
    CheckBox taxcheck;
    Dbase db;
    Spinner taxnaration;
    Boolean isInternetPresent = false;
    ArrayList<Integer> taxarray;
    ArrayList<String> taxstring;
    String billid = "";
    String mpage = "";
    ArrayList<String> sizeItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_tax_excemption);
        taxView = (LinearLayout) findViewById(R.id.taxView);
        save = (Button) findViewById(R.id.btn_save);
        cancel = (Button) findViewById(R.id.btn_cancel);
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        taxarray = new ArrayList();
        taxstring = new ArrayList();
        Bundle b = getIntent().getExtras();
        billid = b.getString("Billid");
        mpage = b.getString("mPage");
        db = new Dbase(TaxExcemption.this);

        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new AsyncTaxdetails().execute(billid);
        } else {
            Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
        }
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(TaxExcemption.this, Report.class);
                intent.putExtra("mPage", mpage);
                intent.putExtra("Billid", billid);
                intent.putExtra("billno", "");
                intent.putExtra("accNo", "" +
                        "");
                startActivity(intent);
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    if (taxarray.size() > 0) {
                        new AsyncTaxexcemtion().execute();

                    } else {
                        Toast.makeText(getApplicationContext(), "Please select tax", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }


    public class AsyncTaxdetails extends AsyncTask<String, Void, String> {

        String status = "";
        JSONObject jsonObject = null;

        int billidint = 0;

        @Override
        protected String doInBackground(String... params) {

            try {
                RestAPI api = new RestAPI(TaxExcemption.this);
                jsonObject = api.GetBillingGroupByTaxDetails(params[0]);
                JSONArray array = jsonObject.getJSONArray("Value");
                Taxdataexcem cl = null;
                for (int i = 0; i < array.length(); i++) {
                    cl = new Taxdataexcem();
                    JSONObject object = array.getJSONObject(i);
                    String BillID = object.optString("BillID");
                    String TaxID = object.optString("TaxID");
                    String TaxTypeName = object.optString("TaxTypeName");
                    String Value = object.optString("Value");
                    String TaxAmount = object.optString("TaxAmount");
                    String Rate = object.optString("Rate");
                    String Description = object.optString("Description");
                    String TaxSubTypeCode = object.optString("TaxSubTypeCode");
                    String IsSubTax = object.optString("IsSubTax");
                    cl.setBillID(BillID);
                    cl.setTaxID(TaxID);
                    cl.setTaxTypeName(TaxTypeName);
                    cl.setValue(Value);
                    cl.setTaxAmount(TaxAmount);
                    cl.setRate(Rate);
                    cl.setDescription(Description);
                    cl.setTaxSubTypeCode(TaxSubTypeCode);
                    cl.setIsSubTax(IsSubTax);
                    taxdataexcems.add(cl);

                }


            } catch (Exception e) {
                status = "error";
                // String mm=e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(TaxExcemption.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (status.equals("error")) {
                Toast.makeText(getApplicationContext(), "" + jsonObject, Toast.LENGTH_LONG).show();
                pd.dismiss();
            } else {
                try {
                    pd.dismiss();
                    if (taxdataexcems.size() > 0) {
                        for (int i = 0; i < taxdataexcems.size(); i++) {
                            if (taxView == null) {
                                taxView = (LinearLayout) findViewById(R.id.taxView);
                                view = getLayoutInflater().inflate(R.layout.taxexcemlayout, taxView, false);
                                taxname = (TextView) view.findViewById(R.id.kot_taxname);
                                taxpercent = (TextView) view.findViewById(R.id.kot_taxpercent);
                                taxcheck = (CheckBox) view.findViewById(R.id.taxexch);
                                taxnaration = (Spinner) view.findViewById(R.id.size_spin);
                                if (!(sizeItems.size() > 0)) {
                                    sizeItems = db.getNarrationlist();
                                }
                                final ArrayAdapter size_adapter = new ArrayAdapter(TaxExcemption.this, android.R.layout.simple_spinner_item, sizeItems);
                                size_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                taxnaration.setAdapter(size_adapter);
                            } else {
                                taxView = (LinearLayout) findViewById(R.id.taxView);
                                view = getLayoutInflater().inflate(R.layout.taxexcemlayout, taxView, false);
                                taxname = (TextView) view.findViewById(R.id.kot_taxname);
                                taxpercent = (TextView) view.findViewById(R.id.kot_taxpercent);
                                taxcheck = (CheckBox) view.findViewById(R.id.taxexch);
                                taxnaration = (Spinner) view.findViewById(R.id.size_spin);


                                if (!(sizeItems.size() > 0)) {
                                    sizeItems = db.getNarrationlist();
                                }
                                final ArrayAdapter size_adapter = new ArrayAdapter(TaxExcemption.this, android.R.layout.simple_spinner_item, sizeItems);
                                size_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                taxnaration.setAdapter(size_adapter);
                            }
                            taxname.setText(taxdataexcems.get(i).getDescription());
                            taxpercent.setText(taxdataexcems.get(i).getValue());
                            taxname.setTag(taxdataexcems.get(i).getTaxID());
                            taxcheck.setId(Integer.parseInt(taxdataexcems.get(i).getTaxID()));
                            taxcheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                    @Override
                                                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                                        if (isChecked) {
                                                                            int taxid = buttonView.getId();
                                                                            taxarray.add(taxid);
                                                                        }
                                                                        if (!isChecked) {
                                                                            int taxid = buttonView.getId();
                                                                            taxarray.remove(new Integer(taxid));
                                                                        }
                                                                    }
                                                                }
                            );




                            taxnaration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                            taxView.addView(view);
                        }
                    } else {
                        try {
                            taxView = (LinearLayout) findViewById(R.id.taxView);
                            view = getLayoutInflater().inflate(R.layout.taxexcemlayout, taxView, false);
                            taxname = (TextView) view.findViewById(R.id.kot_taxname);
                            taxpercent = (TextView) view.findViewById(R.id.kot_taxpercent);
                            taxcheck = (CheckBox) view.findViewById(R.id.taxexch);
                            taxname.setText("No Tax Available");
                            taxpercent.setVisibility(View.INVISIBLE);
                            taxcheck.setVisibility(View.INVISIBLE);
                            taxView.addView(view);
                            save.setVisibility(View.INVISIBLE);
                        } catch (Exception e) {
                            String r = e.getMessage().toString();
                        }


                    }
                } catch (Exception e) {
                    String gg = e.getMessage().toString();
                }

            }

        }
    }

    public class AsyncTaxexcemtion extends AsyncTask<String, Void, String> {
        String billId = "";
        String memberId = "";
        String status = "";
        JSONObject jsonObject = null;
        String memAccoNo = "";
        String memName = "";
        int billidint = 0;

        @Override
        protected String doInBackground(String... params) {

            try {
                RestAPI api = new RestAPI(TaxExcemption.this);
                jsonObject = api.cms_Taxexemption(billid, taxarray);
                status = jsonObject.getString("Value");

            } catch (Exception e) {
                String mm = e.getMessage().toString();
                status = "error";

            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(TaxExcemption.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (status.equals("")) {
                Toast.makeText(getApplicationContext(), "" + jsonObject, Toast.LENGTH_LONG).show();
                pd.dismiss();
            } else {

                try {
                    AlertDialog.Builder alertBulider = new AlertDialog.Builder(TaxExcemption.this);
                    alertBulider.setMessage("Saved");
                    alertBulider.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            Intent intent = new Intent(TaxExcemption.this, Report.class);
                            intent.putExtra("mPage", mpage);
                            intent.putExtra("Billid", billid);
                            intent.putExtra("billno", "");
                            intent.putExtra("accNo", "" +
                                    "");
                            startActivity(intent);
                        }
                    });

                    AlertDialog dialog = alertBulider.create();
                    //  alertBulider.create();
                    dialog.show();
                } catch (Exception e) {
                    String r = e.getMessage().toString();
                }


                pd.dismiss();

            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    finish();
    }
}
