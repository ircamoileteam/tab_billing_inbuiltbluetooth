package com.irca.cosmo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.epson.eposprint.Builder;
import com.irca.Billing.MainActivity;
import com.irca.Printer.DiscoveryActivity;
import com.irca.Utils.ShowMsg;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.db.Dbase;
import com.irca.fields.CloseorderItems;
import com.irca.fields.ItemList;
import com.irca.scan.PermissionsManager;
import com.zcs.sdk.DriverManager;
import com.zcs.sdk.SdkResult;
import com.zcs.sdk.Sys;
import com.zcs.sdk.print.PrnStrFormat;
import com.zcs.sdk.print.PrnTextFont;
import com.zcs.sdk.print.PrnTextStyle;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
/*

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
*/

/**
 * Created by Manoch Richard on 29-Apr-16.
 */
public class Report extends AppCompatActivity implements ReceiveListener {

    LinearLayout customList;
    View customView;
    View customTaxView;
    Dbase db;

    String waiterdetails;
    String name, code;

    TextView taxHeader, taxValue;
    TextView itemName, qty, amount, itemCount;
    ProgressDialog pd;
    Button cancelorder_butt, print_butt;
    public ArrayList<CloseorderItems> closeorderItemsArrayList = new ArrayList<CloseorderItems>();
    public ArrayList<Narrationgetter> Narrationgetterarray = new ArrayList<Narrationgetter>();


    ArrayList<ItemList> cancle_order;
    RelativeLayout printer;
    EditText printer_Name;
    Button bluetooth;
    String waitername;
    String waiter = "";
    String counter_name = "";
    String billType = "";
    SharedPreferences sharedpreferences;
    String memName = "";
    String memAccNo = "";
    int tableNo = 0;
    double servicetax = 0, vat = 0, GrantTotal = 0, billAmt = 0, cessTax = 0, tax = 0, discountamt = 0, taxexcemtedamt = 0;
    SharedPreferences pref;
    String _target = "", _printer = "";
    String print = "";
    private Printer mPrinter = null;
    private Context mContext = null;
    RelativeLayout eBox;
    LinearLayout buttons;
    String mPage = "";
    int count = 0;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    TextView tot_tax, ser_tax, vat_tax, c_tax, g_tot, a_bal;

    ArrayList<String> taxlist;
    String billid = "";
    LinearLayout customTaxList;

    String clubName = "", clubaddress = "";
    //ImageView logo;
    // Bitmap bitmap;
    Bitmap decodedByte = null;
    //newly added
    TextView _mName, _mCode, billNo, bNo;
    ActionBar ab;
    Button taxexcemtion,btnSampleReceiptnew;
    RelativeLayout rtax;
    String userId = "0";
    ImageView signimg;
    private DriverManager mDriverManager = DriverManager.getInstance();
    private com.zcs.sdk.Printer mPrinter1;

    private ExecutorService mSingleThreadExecutor;

    private PermissionsManager mPermissionsManager;
    private Sys mSys = mDriverManager.getBaseSysDevice();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Fabric.with(this, new Crashlytics());
        setContentView(R.layout.report);
        mContext = this;
        signimg = (ImageView) findViewById(R.id.imgview);
        // logo=(ImageView)findViewById(R.id.imageView_logo);
        // bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
        //  logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));

        Bundle b = getIntent().getExtras();
        billid = b.getString("Billid");
        String billno = b.getString("billno");
        String accNo = b.getString("accNo");
        mPage = b.getString("mPage");

        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();

        printer = (RelativeLayout) findViewById(R.id.pr);
        rtax = (RelativeLayout) findViewById(R.id.rtax);
        rtax.setVisibility(View.GONE);
        printer_Name = (EditText) findViewById(R.id.edtTarget1);
        bluetooth = (Button) findViewById(R.id.bluetooth);
        cancelorder_butt = (Button) findViewById(R.id.button_cancelOrder);
        //  title = (TextView) findViewById(R.id.title);
        eBox = (RelativeLayout) findViewById(R.id.pr);
        buttons = (LinearLayout) findViewById(R.id.butt);
        taxexcemtion = (Button) findViewById(R.id.taxexem);

        _mName = (TextView) findViewById(R.id.mName);
        _mCode = (TextView) findViewById(R.id.mCode);
        billNo = (TextView) findViewById(R.id.billNo);
        bNo = (TextView) findViewById(R.id.bNo);


        tot_tax = (TextView) findViewById(R.id.tamt);
        g_tot = (TextView) findViewById(R.id.g_tot);
              /*  ser_tax= (TextView) findViewById(R.id.ser_tax);
                vat_tax= (TextView) findViewById(R.id.vat);
                c_tax= (TextView) findViewById(R.id.c_tax)*/
        ;
        db = new Dbase(Report.this);
        // waitername=db.getWaiterName()


        // name=closeorderItemsArrayList`.get(1).FirstName;
        //  code=closeorderItemsArrayList.get(1).PersonCode;

        //   waiterdetails=code+"_"+name;

        pref = getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        waiter = sharedpreferences.getString("WaiterName", "");
        userId = sharedpreferences.getString("userId", "");
        _target = pref.getString("Target", "");
        _printer = pref.getString("Printer", "");

        clubName = sharedpreferences.getString("clubName", "");
        clubaddress = sharedpreferences.getString("clubAddress", "");

        counter_name = sharedpreferences.getString("StoreName", "");
        billType = sharedpreferences.getString("billType", "");
        taxlist = new ArrayList<>();

        taxexcemtion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int narrationcnt = db.getNarrationCount();
                if (narrationcnt > 0) {
                    Intent intent = new Intent(Report.this, TaxExcemption.class);
                    intent.putExtra("mPage", mPage);
                    intent.putExtra("Billid", billid);
                    startActivity(intent);
                } else {
                    new AsyncResonfortaxexempt().execute();
                }

            }
        });


        if (!mPage.equals("")) {


            if (mPage.equals("1")) {
                eBox.setVisibility(View.VISIBLE);
                buttons.setVisibility(View.VISIBLE);
                //  title.setText("Today's Report");

                actionBarSetup("Re-Print Bill");
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new AsyncCancelOrder().execute(billid);
                } else {
                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                }

                print_butt = (Button) findViewById(R.id.button_cancelprint);


                if (_printer.equalsIgnoreCase("") && _target.equalsIgnoreCase("")) {

//                    printer_Name.setText("TM-P20_011044" + "-" + "BT:00:01:90:AE:93:17");
                //    printer_Name.setText("TM-P80_002585" + "-" + "BT:00:01:90:88:DA:48");

//            printer_Name.setText("TM-P20_000452" + "-" + "BT:00:01:90:C2:AE:77");
                } else {

                    printer_Name.setText(_printer + "-" + _target);
                }


//

                cancelorder_butt.setVisibility(View.GONE);
                print_butt = (Button) findViewById(R.id.button_cancelprint);
                btnSampleReceiptnew = (Button) findViewById(R.id.btnSampleReceiptnew);


                print_butt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        print = printer_Name.getText().toString();
                        if (print.equals("")) {

                            Toast.makeText(Report.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                        } else if (print.equals("-")) {
                            Toast.makeText(Report.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                        } else {

                            try {
                                print_butt.setEnabled(false);
                                Activity mActivity = Report.this;

                                mActivity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        new Report.AsyncPrinting().execute();
                                    }
                                });
                                //  new AsyncPrinting().execute();
                            } catch (Exception e) {
                                ShowMsg.showException(e, "AsyncTask", mContext);
                            }
                        }
                    }
                });

                btnSampleReceiptnew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            try {
                                 Activity mActivity = Report.this;

                                mActivity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        new Report.AsyncPrintingpoona().execute();
                                    }
                                });
                                //  new AsyncPrinting().execute();
                            } catch (Exception e) {
                                ShowMsg.showException(e, "AsyncTask", mContext);
                            }

                    }
                });

                bluetooth.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //  Find bluetooth
                        Intent intent = new Intent(getApplication(), DiscoveryActivity.class);
                        startActivityForResult(intent, 0);

                    }
                });

            } else {
                eBox.setVisibility(View.VISIBLE);
                buttons.setVisibility(View.VISIBLE);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new AsyncCancelOrder().execute(billid);
                } else {
                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                }


                print_butt = (Button) findViewById(R.id.button_cancelprint);
                btnSampleReceiptnew = (Button) findViewById(R.id.btnSampleReceiptnew);

                if (_printer.equalsIgnoreCase("") && _target.equalsIgnoreCase("")) {


//                    printer_Name.setText("TM-P20_011044" + "-" + "BT:00:01:90:AE:93:17");
                    printer_Name.setText("TM-P80_002585" + "-" + "BT:00:01:90:88:DA:48");

//            printer_Name.setText("TM-P20_000452" + "-" + "BT:00:01:90:C2:AE:77");
                } else {

                    printer_Name.setText(_printer + "-" + _target);
                }


                actionBarSetup("Today's Report");

                cancelorder_butt.setVisibility(View.GONE);
                print_butt = (Button) findViewById(R.id.button_cancelprint);
                print_butt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        print = printer_Name.getText().toString();
                        if (print.equals("")) {

                            Toast.makeText(Report.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                        } else {
                            print_butt.setBackgroundColor(getResources().getColor(R.color.pink));
                            try {
                                print_butt.setEnabled(false);
                                Activity mActivity = Report.this;

                                mActivity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        new Report.AsyncPrinting().execute();
                                    }
                                });
                                //  new AsyncPrinting().execute();
                            } catch (Exception e) {
                                ShowMsg.showException(e, "AsyncTask", mContext);
                            }
                        }
                    }
                });
                btnSampleReceiptnew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            Activity mActivity = Report.this;

                            mActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    new Report.AsyncPrintingpoona().execute();
                                }
                            });
                            //  new AsyncPrinting().execute();
                        } catch (Exception e) {
                            ShowMsg.showException(e, "AsyncTask", mContext);
                        }

                    }
                });
                bluetooth.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //  Find bluetooth
                        Intent intent = new Intent(getApplication(), DiscoveryActivity.class);
                        startActivityForResult(intent, 0);

                    }
                });
            }


        } else {
            isInternetPresent = cd.isConnectingToInternet();

            if (isInternetPresent) {
                new AsyncCancelOrder().execute(billid);
            } else {
                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
            }
            print_butt = (Button) findViewById(R.id.button_cancelprint);
            btnSampleReceiptnew = (Button) findViewById(R.id.btnSampleReceiptnew);

            if (_printer.equalsIgnoreCase("") && _target.equalsIgnoreCase("")) {


//                printer_Name.setText("TM-P20_011044" + "-" + "BT:00:01:90:AE:93:17");
                printer_Name.setText("TM-P80_002585" + "-" + "BT:00:01:90:88:DA:48");

//            printer_Name.setText("TM-P20_000452" + "-" + "BT:00:01:90:C2:AE:77");
            } else {

                printer_Name.setText(_printer + "-" + _target);
            }


            actionBarSetup("Re-Print Bill");

            cancelorder_butt.setVisibility(View.GONE);
            print_butt = (Button) findViewById(R.id.button_cancelprint);
            print_butt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    print = printer_Name.getText().toString();
                    if (print.equals("")) {

                        Toast.makeText(Report.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            print_butt.setEnabled(false);
                            Activity mActivity = Report.this;

                            mActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    new Report.AsyncPrinting().execute();
                                }
                            });
                            //  new AsyncPrinting().execute();
                        } catch (Exception e) {
                            ShowMsg.showException(e, "AsyncTask", mContext);
                        }
                    }
                }
            });
            btnSampleReceiptnew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        Activity mActivity = Report.this;

                        mActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                new Report.AsyncPrintingpoona().execute();
                            }
                        });
                        //  new AsyncPrinting().execute();
                    } catch (Exception e) {
                        ShowMsg.showException(e, "AsyncTask", mContext);
                    }

                }
            });
            bluetooth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  Find bluetooth
                    Intent intent = new Intent(getApplication(), DiscoveryActivity.class);
                    startActivityForResult(intent, 0);

                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        if (data != null && resultCode == RESULT_OK) {
            String target = data.getStringExtra(getString(R.string.title_target));
            String printer = data.getStringExtra(getString(R.string.title_Printer));
            if (printer != null) {
                EditText mEdtTarget = (EditText) findViewById(R.id.edtTarget1);
                mEdtTarget.setText(printer + "-" + target);

            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup(String title) {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ab = getSupportActionBar();
            ab.setTitle("" + title);
            ab.setElevation(0);
            ab.setIcon(R.drawable.add_icon);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public class AsyncResonfortaxexempt extends AsyncTask<String, Void, String> {

        JSONObject jsonObject = null;
        String status = "";

        @Override
        protected String doInBackground(String... params) {

            try {
                RestAPI api = new RestAPI(Report.this);

                jsonObject = api.cms_getTaxexemptionReason();
                JSONArray array = jsonObject.getJSONArray("Value");
                Narrationgetter cl = null;
                //    db.clearotreprint();
                for (int i = 0; i < array.length(); i++) {
                    cl = new Narrationgetter();
                    JSONObject object = array.getJSONObject(i);
                    String ID = object.optString("ID");
                    String Narration = object.optString("Narration");
                    db.insertNarrationTax(ID, Narration);
                    cl.setNarrationid(ID);
                    cl.setNarration(Narration);
                    Narrationgetterarray.add(cl);
                }
            } catch (Exception e) {
                status = "error";
                // String mm=e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Report.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (status.equals("error")) {
                Toast.makeText(getApplicationContext(), "" + jsonObject, Toast.LENGTH_LONG).show();
                pd.dismiss();
            } else {
                try {
                    Intent intent = new Intent(Report.this, TaxExcemption.class);
                    intent.putExtra("mPage", mPage);
                    intent.putExtra("Billid", billid);
                    startActivity(intent);

                } catch (Exception e) {
                    String gg = e.getMessage().toString();
                }
                pd.dismiss();
            }

        }
    }


    public class AsyncCancelOrder extends AsyncTask<String, Void, String> {
        String billId = "";
        String memberId = "";
        String status = "";
        JSONObject jsonObject = null;
        String memAccoNo = "";
        String memName = "";

        @Override
        protected String doInBackground(String... params) {

            try {
                RestAPI api = new RestAPI(Report.this);
                Integer userid = Integer.valueOf(userId);
                jsonObject = api.cms_getReprint(params[0], userid);
                JSONArray array = jsonObject.getJSONArray("Value");
                CloseorderItems cl = null;
                db.clearotreprint();
                for (int i = 0; i < array.length(); i++) {
                    cl = new CloseorderItems();
                    JSONObject object = array.getJSONObject(i);
                    /*String ItemID = object.optString("ItemID");
                    String ItemCode = object.optString("ItemCode");
                    String Quantity = object.optString("Quantity");
                    String Amount = object.optString("Amount");
                    String BillID = object.optString("BillID");
                    String taxDescription =object.optString("taxDescription");
                    String mAcc = object.optString("memberAcc");
                    String taxValue=object.optString("taxValue");
                    String ava_balance = object.optString("avBalance");
                    String billNo = object.optString("billnumber");
                    String taxAmount =object.optString("taxAmount");
                    String Rate = object.optString("rate");
                    String itemname = object.optString("itemname");
                    String mName = object.optString("membername");
                    String memberId = object.optString("memberId");
                    String BillDate = object.optString("BillDate");
                    String otNo=object.optString("otno");

                    cl.setItemID(ItemID);
                    cl.setItemCode(ItemCode);
                    cl.setQuantity(Quantity);
                    cl.setAmount(Amount);
                    cl.setBillID(BillID);
                    cl.setTaxDescription(taxDescription);
                    cl.setmAcc(mAcc);
                    cl.setTaxValue(taxValue);
                    cl.setAva_balance(ava_balance);
                    cl.setBillno(billNo);
                    cl.setTaxAmount(taxAmount);
                    cl.setmName(mName);
                    cl.setMemberID(memberId);
                    cl.setBillDate(BillDate);
                    cl.setRate(Rate);
                    cl.setItemname(itemname);
                    cl.setOtno(otNo);
                   // cl.setCardType(cardType);
                    closeorderItemsArrayList.add(cl);*/

                    String ItemID = object.optString("ItemID");
                    String ItemCode = object.optString("ItemCode");
                    String Amount = object.optString("Amount");
                    String Rate = object.optString("rate");
                    String Quantity = object.optString("Quantity");
                    String itemname = object.optString("itemname");
                    String BillID = object.optString("BillID");
                    String mAcc = object.optString("memberAcc");
                    String mName = object.optString("membername");
                    String memberId = object.optString("memberId");
                    String BillDate = object.optString("BillDate");
                    String sales_tax = object.optString("salestax");
                    String service_tax = object.optString("servicetax");
                    String billType = object.optString("BillMode");
                    String closingBalance = object.optString("cbalance");
                    String billNo = object.optString("billnumber");  // not coming
                    String PersonCode = object.optString("PersonCode");
                    String ACCharge = object.getString("ACCharge");
                    String FirstName = object.optString("FirstName");
                    String IsTaxExemptionPermission = object.optString("IsTaxExemptionPermission");
                    String DiscountAmt = object.optString("DiscountAmt");
                    String Sign = object.optString("sign");
                    String gst = object.optString("GSTNO", "");

                    cl.setBillID(BillID);
                    cl.setItemID(ItemID);
                    cl.setQuantity(Quantity);
                    cl.setAmount(Amount);
                    cl.setBillDate(BillDate);
                    cl.setItemname(itemname);
                    cl.setMemberID(memberId);
                    cl.setItemCode(ItemCode);
                    cl.setRate(Rate);
                    cl.setService_tax(service_tax);
                    cl.setSales_tax(sales_tax);
                    cl.setmName(mName);
                    cl.setmAcc(mAcc);
                    cl.setBillno(billNo);
                    cl.setCardType(billType);
                    cl.setAva_balance(closingBalance);
                    cl.setPersonCode(PersonCode);
                    cl.setACCharge(ACCharge);
                    cl.setFirstName(FirstName);
                    cl.setIsTaxExemptionPermission(IsTaxExemptionPermission);
                    cl.setDiscountAmt(DiscountAmt);
                    cl.setMembersign(Sign);
                    cl.setGst(gst);
                    closeorderItemsArrayList.add(cl);

                }


            } catch (Exception e) {
                status = "error";
                // String mm=e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Report.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (status.equals("error")) {
                Toast.makeText(getApplicationContext(), "" + jsonObject, Toast.LENGTH_LONG).show();
                pd.dismiss();
            } else {
                try {
                    servicetax = 0;
                    vat = 0;
                    billAmt = 0;
                    GrantTotal = 0;
                    discountamt = 0;
                    taxlist = new ArrayList<>();
                    DecimalFormat format = new DecimalFormat("#.##");
                    for (int i = 0; i < closeorderItemsArrayList.size(); i++) {

                        memAccoNo = closeorderItemsArrayList.get(0).getmAcc();
                        memName = closeorderItemsArrayList.get(0).getmName();
                        billId = closeorderItemsArrayList.get(0).BillID;

                        if (i == closeorderItemsArrayList.size() - 1) {
                            if (closeorderItemsArrayList.get(i).getSales_tax().equals("")) {

                            } else {
                                try {
                                    if (closeorderItemsArrayList.get(i).getSales_tax().contains("-")) {
                                        String[] tax = closeorderItemsArrayList.get(i).getSales_tax().split("-");
                                        for (int j = 0; j < tax.length; j++) {
                                            String dummy[] = tax[j].split(",");

                                            if (dummy[1].equals("Notax")) {
                                                taxlist.add("Tax" + "-" + format.format(Double.parseDouble(dummy[0])));
                                            } else {
                                                if (dummy.length > 2) {
                                                    taxlist.add(dummy[1] + "-" + format.format(Double.parseDouble(dummy[0])) + "-" + dummy[2]);
                                                }

                                            }
                                            servicetax = servicetax + Double.parseDouble(dummy[0]);
                                        }
                                    } else {
                                        String[] tax = closeorderItemsArrayList.get(i).getSales_tax().split(",");
                                        for (int j = 0; j < tax.length; j++) {
                                            if (tax[1].equals("Service")) {
                                                servicetax = servicetax + Double.parseDouble(tax[0]);
                                            } else if (tax[1].equals("Vat")) {
                                                vat = vat + Double.parseDouble(tax[0]);
                                            } else {
                                                // salesTax=salesTax+Double.parseDouble(tax[0]);
                                            }
                                        }
                                    }

                                    if (closeorderItemsArrayList.get(i).getIsTaxExemptionPermission().equals("1")) {
                                        rtax.setVisibility(View.VISIBLE);

                                        taxexcemtion.setVisibility(View.VISIBLE);
                                    }


                                } catch (Exception e) {
                                    String r = e.getMessage().toString();
                                }
                            }
                        } else {

                            billAmt = billAmt + Double.parseDouble(closeorderItemsArrayList.get(i).getAmount());
                            //   ava_balance=_closeorderItemsArrayList.get(i).getAva_balance();

//ac charges
                            if (!closeorderItemsArrayList.get(0).getACCharge().equals("0") && !closeorderItemsArrayList.get(0).getACCharge().equals("")) {
                                taxlist.add("AC Charge" + "-" + format.format(Double.parseDouble(closeorderItemsArrayList.get(0).getACCharge())));
                            }


                            if (customList == null) {

                                customList = (LinearLayout) findViewById(R.id.closeItemView);
                                customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                                itemName = (TextView) customView.findViewById(R.id.itmName);
                                qty = (TextView) customView.findViewById(R.id.itmQty);
                                amount = (TextView) customView.findViewById(R.id.itmAmt);
                                itemCount = (TextView) customView.findViewById(R.id.itmrate);

                                itemName.setText(closeorderItemsArrayList.get(i).itemname);
                                qty.setText(closeorderItemsArrayList.get(i).Quantity);
                                amount.setText(format.format(Double.parseDouble(closeorderItemsArrayList.get(i).Amount)));
                                itemCount.setText(format.format(Double.parseDouble(closeorderItemsArrayList.get(i).rate)));
                                try {
                                    discountamt = discountamt + Double.parseDouble(closeorderItemsArrayList.get(i).DiscountAmt);
                                } catch (Exception e) {
                                    discountamt = 0;
                                }

                            } else {
                                customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                                itemName = (TextView) customView.findViewById(R.id.itmName);
                                qty = (TextView) customView.findViewById(R.id.itmQty);
                                amount = (TextView) customView.findViewById(R.id.itmAmt);
                                itemCount = (TextView) customView.findViewById(R.id.itmrate);

                                itemName.setText(closeorderItemsArrayList.get(i).itemname);
                                qty.setText(closeorderItemsArrayList.get(i).Quantity);
                                amount.setText(format.format(Double.parseDouble(closeorderItemsArrayList.get(i).Amount)));
                                itemCount.setText(format.format(Double.parseDouble(closeorderItemsArrayList.get(i).rate)));
                                try {
                                    discountamt = discountamt + Double.parseDouble(closeorderItemsArrayList.get(i).DiscountAmt);
                                } catch (Exception e) {
                                    discountamt = 0;
                                }
                            }

                            customList.addView(customView);

                        }


                    }

                    GrantTotal = servicetax + vat + billAmt + cessTax - discountamt;

                    tot_tax.setText(format.format(billAmt));
              /*  ser_tax.setText(format.format(servicetax));
                vat_tax.setText(format.format(vat));
                c_tax.setText(format.format(cessTax));*/


                    _mCode.setText("" + memAccoNo);
                    _mName.setText("" + memName);
                    //  billNo.setText(billId.substring(4));

                    if (billId.equals("") || billId == null) {
                        billNo.setText("" + billId);
                    } else {
                        billNo.setText("" + billId);
                    }


                    for (int k = 0; k < taxlist.size(); k++) {
                        if (customTaxList == null) {
                            customTaxList = (LinearLayout) findViewById(R.id.taxView_report);
                            customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);

                            taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                            taxValue = (TextView) customTaxView.findViewById(R.id.tax);

                            String dummy[] = taxlist.get(k).split("-");
                            if (dummy.length > 2) {
                                if (dummy[2].equalsIgnoreCase("true")) {
                                    taxHeader.setText(dummy[0] + "{Exempted}" + " :");
                                    taxexcemtedamt = Double.parseDouble(dummy[1]) + taxexcemtedamt;
                                } else {
                                    taxHeader.setText(dummy[0] + " :");
                                }
                            } else {
                                taxHeader.setText(dummy[0] + " :");
                            }

                            taxValue.setText(dummy[1]);


                        } else {
                            customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);

                            taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                            taxValue = (TextView) customTaxView.findViewById(R.id.tax);
                            String dummy[] = taxlist.get(k).split("-");
                            if (dummy.length > 2) {
                                if (dummy[2].equalsIgnoreCase("true")) {
                                    taxHeader.setText(dummy[0] + "{Exempted}" + " :");
                                    taxexcemtedamt = Double.parseDouble(dummy[1]) + taxexcemtedamt;
                                } else {
                                    taxHeader.setText(dummy[0] + " :");
                                }
                            } else {
                                taxHeader.setText(dummy[0] + " :");
                            }

                            taxValue.setText(dummy[1]);
                        }
                        customTaxList.addView(customTaxView);

                    }
                    if (discountamt > 0) {
                        if (customTaxList == null) {
                            customTaxList = (LinearLayout) findViewById(R.id.taxView_report);
                            customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);
                            taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                            taxValue = (TextView) customTaxView.findViewById(R.id.tax);
                            taxHeader.setText("Discount Amount" + " :");
                            taxValue.setText(format.format(discountamt) + "");
                        } else {
                            customTaxList = (LinearLayout) findViewById(R.id.taxView_report);
                            customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);
                            taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                            taxValue = (TextView) customTaxView.findViewById(R.id.tax);
                            taxHeader.setText("Discount Amount" + " :");
                            taxValue.setText(format.format(discountamt) + "");
                        }
                        customTaxList.addView(customTaxView);
                    }


                    if (closeorderItemsArrayList.get(0).getMembersign() != null) {
                        byte[] decodedString = Base64.decode(closeorderItemsArrayList.get(0).getMembersign(), Base64.DEFAULT);
                        decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        signimg.setImageBitmap(decodedByte);
                    }


                    g_tot.setText(format.format(GrantTotal - taxexcemtedamt));
                    taxexcemtedamt = 0;

                } catch (Exception e) {
                    String gg = e.getMessage().toString();
                }
                pd.dismiss();
            }

        }
    }

    //***************************** PRINTER  *********************************//

    public class AsyncPrinting extends AsyncTask<Void, Void, String> {

        String status = "";
        ProgressDialog pd;

        @Override
        protected String doInBackground(Void... params) {

            try {
              mPrinter = new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
                //       mPrinter = new Printer(Printer.TM_P80, Printer.MODEL_ANK, mContext);
                mPrinter.setReceiveEventListener(Report.this);
            } catch (Throwable e) {
                status = e.getMessage();
//                ShowMsg.showException(e, "Printer", mContext);
                e.printStackTrace();
            }
            try {

                if (!createReceiptData()) {
                    if (mPrinter == null) {
                        status = "false";
                    }
                    mPrinter.clearCommandBuffer();
                    mPrinter.setReceiveEventListener(null);
                    mPrinter = null;
                    status = "false";
                } else {
                    status = "true";
                }

            } catch (Throwable e) {
                //ShowMsg.showException(e, "other", mContext);
                status = e + "";
                e.printStackTrace();
            }

            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Report.this);
            pd.setMessage("Printing Please Wait .......");
            pd.setCancelable(false);
            pd.show();
        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            if (status.equals("true")) {

                try {

                    boolean print = printData();
                    if (!print) {
                        if (mPrinter == null) {
                            status = "false";
                        }

                        mPrinter.clearCommandBuffer();

                        mPrinter.setReceiveEventListener(null);

                        mPrinter = null;

                    } else {
                        count = count + 1;
                        status = "true";
                    }
                } catch (Exception e) {
                    ShowMsg.showException(e, "Post execute error", mContext);
                }
                // updateButtonState(true);
                Toast.makeText(Report.this, "Printing Success", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else if (status.equals("")) {
                Toast.makeText(Report.this, "Printing failed to enter", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else {
                Toast.makeText(Report.this, "Printing failed to enter" + status, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }
  public class AsyncPrintingpoona extends AsyncTask<Void, Void, String> {

        String status = "";
        ProgressDialog pd;

        @Override
        protected String doInBackground(Void... params) {
            try {
              createReceiptDatapoona() ;

            } catch (Throwable e) {
                //ShowMsg.showException(e, "other", mContext);
                status = e + "";
                e.printStackTrace();
            }

            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            if (status.equals("true")) {

                try {

                    boolean print = printData();
                    if (!print) {
                        if (mPrinter == null) {
                            status = "false";
                        }

                        mPrinter.clearCommandBuffer();

                        mPrinter.setReceiveEventListener(null);

                        mPrinter = null;

                    } else {
                        count = count + 1;
                        status = "true";
                    }
                } catch (Exception e) {
                    ShowMsg.showException(e, "Post execute error", mContext);
                }
                // updateButtonState(true);
                Toast.makeText(Report.this, "Printing Success", Toast.LENGTH_SHORT).show();
             //   pd.dismiss();
            } else if (status.equals("")) {
                Toast.makeText(Report.this, "Printing failed to enter", Toast.LENGTH_SHORT).show();
               // pd.dismiss();
            } else {
                Toast.makeText(Report.this, "Printing failed to enter" + status, Toast.LENGTH_SHORT).show();
              //  pd.dismiss();
            }
        }
    }

    public boolean runPrintReceiptSequence() {
        if (!initializeObject()) {
            return false;
        }


        if (!createReceiptData()) {
            finalizeObject();
            return false;
        }

        if (!printData()) {
            finalizeObject();
            return false;
        }
        count = count + 1;
        return true;
    }

    public boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            return false;
        }

        PrinterStatusInfo status = mPrinter.getStatus();

        dispPrinterWarnings(status);

        if (!isPrintable(status)) {
            ShowMsg.showMsg(makeErrorMessage(status), mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    public boolean initializeObject() {
        try {
               /* mPrinter = new Printer(((SpnModelsItem) mSpnSeries.getSelectedItem()).getModelConstant(),
                        ((SpnModelsItem) mSpnLang.getSelectedItem()).getModelConstant(),
                        mContext);*/
         mPrinter = new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
            //       mPrinter = new Printer(Printer.TM_P80, Printer.MODEL_ANK, mContext);
        } catch (Exception e) {
            ShowMsg.showException(e, "Printer", mContext);
            return false;
        }

        mPrinter.setReceiveEventListener(this);

        return true;
    }

    public void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    public boolean connectPrinter() {
        boolean isBeginTransaction = false;
        String ll = "";

        if (mPrinter == null) {
            return false;
        }

        try {
            ll = printer_Name.getText().toString();
            if (!ll.equals("")) {
                mPrinter.connect(printer_Name.getText().toString(), Printer.PARAM_DEFAULT);
            } else {
                String[] l = ll.split("-");
                mPrinter.connect(l[1], Printer.PARAM_DEFAULT);
            }
        } catch (Exception e) {
            ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        try {
            mPrinter.beginTransaction();
            isBeginTransaction = true;
        } catch (Exception e) {
            ShowMsg.showException(e, "beginTransaction", mContext);
        }

        if (isBeginTransaction == false) {
            try {
                mPrinter.disconnect();
            } catch (Epos2Exception e) {
                // Do nothing
                return false;
            }
        }

        return true;
    }

    public void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "endTransaction", mContext);
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "disconnect", mContext);
                }
            });
        }

        finalizeObject();
    }

    public boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            ;//print available
        }

        return true;
    }

    public String makeErrorMessage(PrinterStatusInfo status) {
        String msg = "";

        if (status.getOnline() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_offline);
        }
        if (status.getConnection() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_no_response);
        }
        if (status.getCoverOpen() == Printer.TRUE) {
            msg += getString(R.string.handlingmsg_err_cover_open);
        }
        if (status.getPaper() == Printer.PAPER_EMPTY) {
            msg += getString(R.string.handlingmsg_err_receipt_end);
        }
        if (status.getPaperFeed() == Printer.TRUE || status.getPanelSwitch() == Printer.SWITCH_ON) {
            msg += getString(R.string.handlingmsg_err_paper_feed);
        }
        if (status.getErrorStatus() == Printer.MECHANICAL_ERR || status.getErrorStatus() == Printer.AUTOCUTTER_ERR) {
            msg += getString(R.string.handlingmsg_err_autocutter);
            msg += getString(R.string.handlingmsg_err_need_recover);
        }
        if (status.getErrorStatus() == Printer.UNRECOVER_ERR) {
            msg += getString(R.string.handlingmsg_err_unrecover);
        }
        if (status.getErrorStatus() == Printer.AUTORECOVER_ERR) {
            if (status.getAutoRecoverError() == Printer.HEAD_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_head);
            }
            if (status.getAutoRecoverError() == Printer.MOTOR_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_motor);
            }
            if (status.getAutoRecoverError() == Printer.BATTERY_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_battery);
            }
            if (status.getAutoRecoverError() == Printer.WRONG_PAPER) {
                msg += getString(R.string.handlingmsg_err_wrong_paper);
            }
        }
        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_0) {
            msg += getString(R.string.handlingmsg_err_battery_real_end);
        }

        return msg;
    }

    public void dispPrinterWarnings(PrinterStatusInfo status) {
        EditText edtWarnings = (EditText) findViewById(R.id.edtWarnings1);

        String warningsMsg = "";

        if (status == null) {
            return;
        }

        if (status.getPaper() == Printer.PAPER_NEAR_END) {
            warningsMsg += getString(R.string.handlingmsg_warn_receipt_near_end);
        }

        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_1) {
            warningsMsg += getString(R.string.handlingmsg_warn_battery_near_end);
        }

        edtWarnings.setText(warningsMsg);
    }

    public void updateButtonState(boolean state) {
        // Button btnReceipt = (Button)findViewById(R.id.button_cancelprint);
        //Button btnCoupon = (Button)findViewById(R.id.btnSampleCoupon);
        print_butt.setBackgroundColor(getResources().getColor(R.color.control_focused));
        print_butt.setText("Connecting Printer...");
        if (state == true) {
            if (count == 0) {
                print_butt.setEnabled(state);
                print_butt.setBackgroundColor(getResources().getColor(R.color.pi));
                print_butt.setText("Print");

            } else {
                print_butt.setEnabled(state);
                print_butt.setBackgroundColor(getResources().getColor(R.color.control_focused));
                print_butt.setText("Re-Print");

            }

        } else {
            print_butt.setBackgroundColor(getResources().getColor(R.color.pink));
            print_butt.setEnabled(state);
            print_butt.setText("Printing ...");
        }
        //  btnCoupon.setEnabled(state);
    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
                ShowMsg.showResult(code, makeErrorMessage(status), mContext);

                dispPrinterWarnings(status);

                updateButtonState(true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        disconnectPrinter();
                    }
                }).start();
            }
        });
    }


    public boolean createReceiptData() {
        String method = "";
        StringBuilder textData = new StringBuilder();
        servicetax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        taxexcemtedamt = 0;
        tax = 0;
        DecimalFormat format = new DecimalFormat("#.##");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());
        //    String date = "30/09/2018 12:16:11.390";

        ArrayList<CloseorderItems> _closeorderItemsArrayList = closeorderItemsArrayList;

        if (mPrinter == null) {
            return false;
        }
        try {
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            //textData.append("BANGALORE GOLF CLUB\n");
            textData.append(clubName + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            // textData.append("No.8,Pampa Mahakavi Road," + "\n" + "Opp.Makkala Koota," + "\n" + "Basavanagudi,"+ "\n" + "Bangalore-560004" + "\n");
            //  textData.append("22nd Cross,3rd Block,Jayanagar" + "\n" + "Bangalore-560 011" + "\n");
            if (MainActivity.addressList != null) {
                for (int k = 0; k < MainActivity.addressList.size(); k++) {
                    textData.append(MainActivity.addressList.get(k) + "\n");
                }
            } else {
                textData.append("CLUB,KARNATAK6" + "\n");
            }

            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("RE-PRINT Bill No.:" + _closeorderItemsArrayList.get(0).BillID.substring(4) + "\n");

            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("Member  Id   " + ": " + _closeorderItemsArrayList.get(0).mAcc + "\n");
            textData.append("Member Name  " + ": " + _closeorderItemsArrayList.get(0).mName + "\n");
//            textData.append("GST NO  " + ": " + _closeorderItemsArrayList.get(1).gst + "\n");
            SharedPreferences sharedpreferences = getSharedPreferences("GST", Context.MODE_PRIVATE);
//                gst = sharedpreferences.getString("GST", "");
            String tin = sharedpreferences.getString("TIN", "");
            String gst = sharedpreferences.getString("GST", "");
            String cin = sharedpreferences.getString("CIN", "");
//            textData.append("GST NO:" + ":" + gst + "\n");
            textData.append("GST NO  " + ": " + gst + "\n");
            textData.append(tin + "\n");
            textData.append(cin + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("--------------------------------------\n");
            textData.append("Type       " + ":" + (_closeorderItemsArrayList.get(0).cardType) + "\n");
            // textData.append("Billed By  " + ":" + "           " + "\n");
            //     textData.append("Date Time  " + ":" + date + "\n");
            textData.append("Date Time  " + ":" + _closeorderItemsArrayList.get(0).getBillDate() + "\n");
            textData.append("Venue      " + ":" + counter_name + "\n\n");
            //textData.append("Bill Clerk " + ":" + waiter + "\n");
            textData.append("Steward " + ":" + waiter + "\n");
            //  textData.append("Waiter     " + ":" + TakeOrder.waiternamecode + "\n");waiterdetails
            textData.append("Waiter     " + ":" + _closeorderItemsArrayList.get(0).PersonCode + "_" + _closeorderItemsArrayList.get(0).FirstName + "\n\n");
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append(
                    "Item Name             "

                            + String.format("%4s", "Qty")
                            //  + " "
                            + String.format("%8s", "Rate")
                            //  + " "
                            + String.format("%8s", "Amount")
                            + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);

            for (int j = 0; j < _closeorderItemsArrayList.size() - 1; j++) {
                textData.append(
                        String.format("%18s", (_closeorderItemsArrayList.get(j).itemname + "                                                ").substring(0, 20))
                                // +" "
                                + String.format("%4s", (_closeorderItemsArrayList.get(j).Quantity))

                                + String.format("%8s", format.format
                                (Double.parseDouble((_closeorderItemsArrayList.get(j).rate))))
                                // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                + String.format("%10s", format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount))))
                                // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                + "\n");


                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                //TODO include in manoch 25-4

                // servicetax=servicetax+Double.parseDouble(_closeorderItemsArrayList.get(j).getService_tax());
                //  vat=vat+Double.parseDouble(_closeorderItemsArrayList.get(j).getSales_tax());
                billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
               /* if(_closeorderItemsArrayList.get(j).getCessTax()==null){
                    cessTax=cessTax+0;
                }else{
                    cessTax=cessTax+Double.parseDouble(_closeorderItemsArrayList.get(j).getCessTax());
                }
*/
            }

            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append(String.format("%20s", "Bill Amount" + " :  " + format.format(billAmt) + "\n"));

            for (int k = 0; k < taxlist.size(); k++) {
                String dummy[] = taxlist.get(k).split("-");
                tax = tax + Double.parseDouble(dummy[1]);
                if (dummy.length > 2) {
                    String taxexcemp = dummy[2];
                    if (taxexcemp.equalsIgnoreCase("true")) {
                        textData.append(String.format("%20s", dummy[0] + "{Exempted} :  " + format.format(Double.parseDouble(dummy[1])) + "\n"));
                        taxexcemtedamt = taxexcemtedamt + Double.parseDouble(dummy[1]);
                    } else {
                        textData.append(String.format("%20s", dummy[0] + " :  " + format.format(Double.parseDouble(dummy[1])) + "\n"));
                    }
                } else {
                    textData.append(String.format("%20s", dummy[0] + " :  " + format.format(Double.parseDouble(dummy[1])) + "\n"));
                }


            }
            if (discountamt > 0) {
                textData.append(String.format("%20s", "Discount Amt.:" + format.format(discountamt) + "\n"));
            }

            GrantTotal = billAmt + tax - discountamt - taxexcemtedamt;

            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal) + "\n");

            if (TakeOrder.cardType.equalsIgnoreCase("CASH CARD")) {

                if (TakeOrder.isCashCardFilled.equalsIgnoreCase("false")) {

                    textData.append("AVAIL.BAL  " + " :  " + "0" + "\n");
                } else {
                    textData.append("AVAIL.BAL  " + " :  " + (_closeorderItemsArrayList.get(0).ava_balance) + "\n");
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());
                }

            } else
                textData.append("AVAIL.BAL  " + " :  " + (_closeorderItemsArrayList.get(0).ava_balance) + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            textData.append("\n");
            textData.append("\n");
            textData.append("(Signature)" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            if (decodedByte != null) {
                decodedByte = getResizedBitmap(decodedByte, 350);
                //    bitmap = getResizedBitmap(bitmap, 200);
                method = "addImage";

                mPrinter.addPagePosition(0, 0);
                //   mPrinter.addImage(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), Builder.PARAM_DEFAULT, Builder.MODE_MONO, Builder.HALFTONE_DITHER, 1.0,Builder.PARAM_DEFAULT);
                Bitmap largeIcon = decodedByte;

                mPrinter.addImage(largeIcon, 0, 0, largeIcon.getWidth(), largeIcon.getHeight(), Builder.PARAM_DEFAULT, Builder.MODE_MONO, Builder.HALFTONE_DITHER, 1.0, Builder.PARAM_DEFAULT);

            }


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
            textData.append("* Thank you ! We Wish To Serve You Again * ");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);

        } catch (Exception e) {
            String olderror = sharedpreferences.getString("ReportClassError", "");
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("ReportClassError", "olderror:" + olderror + "NE:" + e + "");
            editor.commit();
            ShowMsg.showException(e, method, mContext);
            return false;
        }
        textData = null;
        return true;
    }
public boolean createReceiptDatapoona() {
        String method = "";
        final StringBuilder textData = new StringBuilder();
        servicetax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        taxexcemtedamt = 0;
        tax = 0;
        final DecimalFormat format = new DecimalFormat("#.##");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());
        final ArrayList<CloseorderItems> _closeorderItemsArrayList = closeorderItemsArrayList;

        try {
            mSingleThreadExecutor = mDriverManager.getSingleThreadExecutor();
            mDriverManager = DriverManager.getInstance();
            mPrinter1 = mDriverManager.getPrinter();

            mSingleThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    int printStatus = mPrinter1.getPrinterStatus();
                    if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                        Report.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(Report.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                            }
                        });
                    } else {
                        PrnStrFormat format1 = new PrnStrFormat();
                        format1.setTextSize(30);
                        format1.setAli(Layout.Alignment.ALIGN_CENTER);
                        format1.setFont(PrnTextFont.DEFAULT);
                        mPrinter1.setPrintAppendString(clubName, format1);

            // textData.append("No.8,Pampa Mahakavi Road," + "\n" + "Opp.Makkala Koota," + "\n" + "Basavanagudi,"+ "\n" + "Bangalore-560004" + "\n");
            //  textData.append("22nd Cross,3rd Block,Jayanagar" + "\n" + "Bangalore-560 011" + "\n");
            if (MainActivity.addressList != null) {
                for (int k = 0; k < MainActivity.addressList.size(); k++) {

                    format1.setTextSize(25);
                    format1.setAli(Layout.Alignment.ALIGN_CENTER);
                    format1.setFont(PrnTextFont.DEFAULT);
                    mPrinter1.setPrintAppendString(MainActivity.addressList.get(k) , format1);

                }
            }

                        mPrinter1.setPrintAppendString(" ", format1);
                        mPrinter1.setPrintAppendString("RE-PRINT Bill No.:" + _closeorderItemsArrayList.get(0).BillID.substring(4) , format1);

                        mPrinter1.setPrintAppendString("------------------------------------------------------", format1);



                        format1.setTextSize(30);
                        format1.setStyle(PrnTextStyle.BOLD);
                        mPrinter1.setPrintAppendString("Member  Id   " + ": " + _closeorderItemsArrayList.get(0).mAcc , format1);
                        mPrinter1.setPrintAppendString("Member Name  " + ": " + _closeorderItemsArrayList.get(0).mName, format1);
                        mPrinter1.setPrintAppendString("--------------------------------------", format1);

                        format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                        format1.setStyle(PrnTextStyle.NORMAL);
                        format1.setTextSize(25);
            SharedPreferences sharedpreferences = getSharedPreferences("GST", Context.MODE_PRIVATE);
//                gst = sharedpreferences.getString("GST", "");
            String tin = sharedpreferences.getString("TIN", "");
            String gst = sharedpreferences.getString("GST", "");
            String cin = sharedpreferences.getString("CIN", "");
//            textData.append("GST NO:" + ":" + gst + "\n");

                        mPrinter1.setPrintAppendString("GST NO  " + ": " + gst, format1);
                        mPrinter1.setPrintAppendString("------------------------------------------------------", format1);

  //          textData.append("Type       " + ":" + billtype(_closeorderItemsArrayList.get(0).cardType) + "\n");
                        mPrinter1.setPrintAppendString("Type       " + ":" + billtype(_closeorderItemsArrayList.get(0).cardType) , format1);

                        // textData.append("Billed By  " + ":" + "           " + "\n");
            //     textData.append("Date Time  " + ":" + date + "\n");
      //      textData.append("Date Time  " + ":" + _closeorderItemsArrayList.get(0).getBillDate() + "\n");

                        mPrinter1.setPrintAppendString("Date Time  " + ":" + _closeorderItemsArrayList.get(0).getBillDate(), format1);

                    //    textData.append("Venue      " + ":" + counter_name + "\n\n");
                        mPrinter1.setPrintAppendString("Venue      " + ":" + counter_name , format1);

                        //textData.append("Bill Clerk " + ":" + waiter + "\n");
      //      textData.append("Steward " + ":" + waiter + "\n");
                        mPrinter1.setPrintAppendString("Steward " + ":" + waiter , format1);

                        //  textData.append("Waiter     " + ":" + TakeOrder.waiternamecode + "\n");waiterdetails
     //       textData.append("Waiter     " + ":" + _closeorderItemsArrayList.get(0).PersonCode + "_" + _closeorderItemsArrayList.get(0).FirstName + "\n\n");
                        mPrinter1.setPrintAppendString("Waiter     " + ":" + _closeorderItemsArrayList.get(0).PersonCode + "_" + _closeorderItemsArrayList.get(0).FirstName, format1);


                        mPrinter1.setPrintAppendString("------------------------------------------------------", format1);

                        format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                        format1.setStyle(PrnTextStyle.NORMAL);
                        format1.setTextSize(23);
                        mPrinter1.setPrintAppendString(
                                String.format("%30s", ( "Item Name             "+ "                                                ").substring(0, 30))

                                        + String.format("%6s", "Qty")
                                        //  + " "
                                        //    + String.format("%8s", "Rate")
                                        //  + " "
                                        + String.format("%10s", "Amount"), format1);
                        mPrinter1.setPrintAppendString("------------------------------------------------------", format1);


            for (int j = 0; j < _closeorderItemsArrayList.size() - 1; j++) {
 /*               textData.append(
                        String.format("%18s", (_closeorderItemsArrayList.get(j).itemname + "                                                ").substring(0, 20))
                                // +" "
                                + String.format("%4s", (_closeorderItemsArrayList.get(j).Quantity))

                                + String.format("%8s", format.format
                                (Double.parseDouble((_closeorderItemsArrayList.get(j).rate))))
                                // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                + String.format("%10s", format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount))))
                                // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                + "\n");*/
                 format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                format1.setStyle(PrnTextStyle.NORMAL);
                format1.setTextSize(23);
                if(_closeorderItemsArrayList.get(j).itemname.length()>20){
                    String itemstr1=_closeorderItemsArrayList.get(j).itemname.substring(0,20);
                    String itemstr2="..."+_closeorderItemsArrayList.get(j).itemname.substring(20,_closeorderItemsArrayList.get(j).itemname.length());
                    String itemline =
                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                            String.format("%15s", (itemstr1 + "                                                ").substring(0, 20))

                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).getQuantity()))

                                    + String.format("%8s", format.format
                                    //     (Double.parseDouble((_closeorderbillList.get(m).getTRate())))) + String.format("%5s", format.format(Double.parseDouble((_closeorderbillList.get(m).getAmount()))))
                                            (Double.parseDouble((_closeorderItemsArrayList.get(j).getAmount()))))

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline, format1);

                    String itemline1 =
                            String.format("%15s", (itemstr2 + "                                                ").substring(0, 20))

                                    + String.format("%4s", (""))

                                    + String.format("%8s","")

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline1, format1);
                }
                else{
                    String itemline =
                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                            String.format("%15s", (_closeorderItemsArrayList.get(j).getItemname() + "                                                ").substring(0, 20))

                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).getQuantity()))

                                    + String.format("%8s", format.format
                                    //     (Double.parseDouble((_closeorderbillList.get(m).getTRate())))) + String.format("%5s", format.format(Double.parseDouble((_closeorderbillList.get(m).getAmount()))))
                                            (Double.parseDouble((_closeorderItemsArrayList.get(j).getAmount()))))

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline, format1);

                }
                billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());

            }

                mPrinter1.setPrintAppendString("------------------------------------------------------", format1);

           //     textData.append(String.format("%20s", "Bill Amount" + " :  " + format.format(billAmt) + "\n"));

                mPrinter1.setPrintAppendString(String.format("%20s", "Bill Amount" + " :  " + format.format(billAmt) ), format1);


                for (int k = 0; k < taxlist.size(); k++) {
                String dummy[] = taxlist.get(k).split("-");
                tax = tax + Double.parseDouble(dummy[1]);
                if (dummy.length > 2) {
                    String taxexcemp = dummy[2];
                    if (taxexcemp.equalsIgnoreCase("true")) {
                    //    textData.append(String.format("%20s", dummy[0] + "{Exempted} :  " + format.format(Double.parseDouble(dummy[1])) + "\n"));
                        mPrinter1.setPrintAppendString(String.format("%20s", dummy[0] + "{Exempted} :  " + format.format(Double.parseDouble(dummy[1]))), format1);


                        taxexcemtedamt = taxexcemtedamt + Double.parseDouble(dummy[1]);
                    } else {
                       // textData.append(String.format("%20s", dummy[0] + " :  " + format.format(Double.parseDouble(dummy[1])) + "\n"));
                        mPrinter1.setPrintAppendString(String.format("%20s", dummy[0] + " :  " + format.format(Double.parseDouble(dummy[1]))) , format1);

                    }
                } else {
                 //   textData.append(String.format("%20s", dummy[0] + " :  " + format.format(Double.parseDouble(dummy[1])) + "\n"));
                    mPrinter1.setPrintAppendString(String.format("%20s", dummy[0] + " :  " + format.format(Double.parseDouble(dummy[1]))), format1);
                }
            }
            if (discountamt > 0) {
              //  textData.append(String.format("%20s", "Discount Amt.:" + format.format(discountamt) + "\n"));
                mPrinter1.setPrintAppendString(String.format("%20s", "Discount Amt.:" + format.format(discountamt)) , format1);


            }

            GrantTotal = billAmt + tax - discountamt - taxexcemtedamt;
                format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                format1.setStyle(PrnTextStyle.NORMAL);
                format1.setTextSize(30);

       //      textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal) + "\n");
                mPrinter1.setPrintAppendString("GRAND TOTAL" + " :  " + format.format(GrantTotal)  , format1);
                format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                format1.setStyle(PrnTextStyle.NORMAL);
                format1.setTextSize(25);
            if (TakeOrder.cardType.equalsIgnoreCase("CASH CARD")) {

                if (TakeOrder.isCashCardFilled.equalsIgnoreCase("false")) {

                  //  textData.append("AVAIL.BAL  " + " :  " + "0" + "\n");

                    mPrinter1.setPrintAppendString("AVAIL.BAL  " + " :  " + "0" , format1);

                } else {
                //    textData.append("AVAIL.BAL  " + " :  " + (_closeorderItemsArrayList.get(0).ava_balance) + "\n");
                    mPrinter1.setPrintAppendString("AVAIL.BAL  " + " :  " + (_closeorderItemsArrayList.get(0).ava_balance), format1);

                }

            }
            else {

                mPrinter1.setPrintAppendString("AVAIL.BAL  " + " :  " + (_closeorderItemsArrayList.get(0).ava_balance), format1);


                mPrinter1.setPrintAppendString("------------------------------------------------------", format1);
                mPrinter1.setPrintAppendString(" ", format1);
                mPrinter1.setPrintAppendString(" ", format1);
                mPrinter1.setPrintAppendString("(Signature)" + "\n", format1);
                mPrinter1.setPrintAppendString("* Thank you ! We Wish To Serve You Again ", format1);
       /*
            if (decodedByte != null) {
                decodedByte = getResizedBitmap(decodedByte, 350);
                //    bitmap = getResizedBitmap(bitmap, 200);
                method = "addImage";

                mPrinter.addPagePosition(0, 0);
                //   mPrinter.addImage(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), Builder.PARAM_DEFAULT, Builder.MODE_MONO, Builder.HALFTONE_DITHER, 1.0,Builder.PARAM_DEFAULT);
                Bitmap largeIcon = decodedByte;

                mPrinter.addImage(largeIcon, 0, 0, largeIcon.getWidth(), largeIcon.getHeight(), Builder.PARAM_DEFAULT, Builder.MODE_MONO, Builder.HALFTONE_DITHER, 1.0, Builder.PARAM_DEFAULT);

            }
*/
            }
            }
              printStatus = mPrinter1.setPrintStart();
                    if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                        Report.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(Report.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                            }
                        });
                    }

                }});}
        catch (Exception e) {
            String olderror = sharedpreferences.getString("ReportClassError", "");
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("ReportClassError", "olderror:" + olderror + "NE:" + e + "");
            editor.commit();
            ShowMsg.showException(e, method, mContext);
            return false;
        }
         return true;
    }

    public String billtype(String cardType) {
        String Type = "";
        switch (cardType) {
            case "H":
                Type = "CASH BILL";
                break;
            case "G":
                Type = "CREDIT BILL";
                break;
            case "D":
                Type = "CASH BILL";
                break;
            case "S":
                Type = "CREDIT BILL";
                break;
            case "R":
                Type = "REGULAR BILL";
                break;
            case "C":
                Type = "REGULAR BILL";
                break;


        }
        return Type;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

}
