package com.irca.cosmo;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.db.Dbase;
import com.irca.fields.Taccount;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/*
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
*/

/**
 * Created by Manoch Richard on 29-Apr-16.
 */
public class TodaysAccount extends AppCompatActivity {
    ListView listView;
    ProgressDialog pd;
    Dbase db;
    SharedPreferences sharedpreferences;
    //TextView title,newCard;
    String mpage;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    FloatingActionButton add;
    // ImageView logo;
    // Bitmap bitmap;
    EditText inputSearch;
    ImageView fromdatebtn;
    ImageView todateimg;
    static EditText fromdatetxt;
    static EditText todatetxt;
    public static String roomcheckinSEND = "", roomcheckoutset = "", roomcheckoutSEND = "", roomcheckout = "", roomcheckin = "";
    public static SimpleDateFormat dateFormatsample, dateFormatsampleSEND;
    public static int YearO, MonthO, DayO;
    LinearLayout lin;
    public static SimpleDateFormat dateFormatsampleSET, dfDate;
    public static int Year, Month, Day;
    Button btnsubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.store_list);
        dateFormatsample = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        dateFormatsampleSEND = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        lin = (LinearLayout) findViewById(R.id.lin);
        lin.setVisibility(View.VISIBLE);
        roomcheckinSEND = "";
        roomcheckoutset = "";
        dateFormatsampleSET = new SimpleDateFormat("dd MMM yyyy");
        dfDate = new SimpleDateFormat("yyyy-MM-dd");
        btnsubmit = (Button) findViewById(R.id.submit);
        actionBarSetup();
        listView = (ListView) findViewById(R.id.list_store);
        add = (FloatingActionButton) findViewById(R.id.add);
        add.setVisibility(View.GONE);
        inputSearch = (EditText) findViewById(R.id.waiterSearch);
        inputSearch.setVisibility(View.GONE);
        fromdatebtn = (ImageView) findViewById(R.id.fromdateimg);
        todateimg = (ImageView) findViewById(R.id.todateimg);
        fromdatetxt = (EditText) findViewById(R.id.frmdatetxt);
        todatetxt = (EditText) findViewById(R.id.todatetxt);
        // logo=(ImageView)findViewById(R.id.imageView_logo);
        // bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
        // logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));

        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        //  title= (TextView) findViewById(R.id.txTitle);
        // newCard= (TextView) findViewById(R.id.newcard);
        //  newCard.setVisibility(View.GONE);
        //   title.setText("Member Account");
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        db = new Dbase(TodaysAccount.this);
        Bundle b = getIntent().getExtras();
        mpage = b.getString("mpage");


        fromdatetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                DialogFragment dialogfragment = new DatePickerDialogThemeIN();
//
//                dialogfragment.show(getFragmentManager(), "Theme 2");

                showFromDatepicker();

            }
        });
        todatetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatepicker();

//                DialogFragment dialogfragmentOut = new DatePickerDialogThemeOut();
//
//                dialogfragmentOut.show(getFragmentManager(), "Theme 2");
            }
        });
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!(roomcheckinSEND.equals("") || roomcheckoutSEND.equalsIgnoreCase(""))) {
                    if (isInternetPresent) {
                        //new AsyncStore().execute(mpage);
                        new AsyncStore().execute("1");

                    } else {
                        Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please Select from and to date", Toast.LENGTH_SHORT).show();

                }


            }
        });
    }

    private void showDatepicker() {

        DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int j, int k) {
                j = j + 1;
                Calendar c2 = Calendar.getInstance();
                c2.set(i, j - 1, k);
                Calendar c = Calendar.getInstance();
                Date time = c2.getTime();
                Date time2 = c.getTime();
                if (c2.getTime().after(time2)) {
                    Toast.makeText(TodaysAccount.this, "Please choose valid date", Toast.LENGTH_SHORT).show();
                } else {
                    todatetxt.setText(getFormatedDate(i + "-" + j + "-" + k));
                    roomcheckoutSEND = getFormatedDate(i + "-" + j + "-" + k);
                }
            }
        };

        String[] date_split = getTodysdate();
        DatePickerDialog datePickerDialog =
                //new DatePickerDialog(Billing.this,R.style.MyDialogTheme,myDateSetListener, 2017, 9, 13);
                new DatePickerDialog(TodaysAccount.this, myDateSetListener, Integer.parseInt(date_split[2]), Integer.parseInt(date_split[1]) - 1, Integer.parseInt(date_split[0]));
        datePickerDialog.setTitle("To Date");
        datePickerDialog.show();
    }


    private String[] getTodysdate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate.split("-");
    }

    private String getFormatedDate(String d1) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date d2 = null;
        try {
            d2 = df.parse(d1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return df.format(d2);
    }

    private void showFromDatepicker() {
        DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int j, int k) {
                j = j + 1;
                Calendar c2 = Calendar.getInstance();
                c2.set(i, j - 1, k);
                Calendar c = Calendar.getInstance();
                Date time = c2.getTime();
                Date time2 = c.getTime();
                if (c2.getTime().after(time2)) {
                    Toast.makeText(TodaysAccount.this, "Please choose valid date", Toast.LENGTH_SHORT).show();
                } else {
                    fromdatetxt.setText(getFormatedDate(i + "-" + j + "-" + k));
                    roomcheckinSEND = getFormatedDate(i + "-" + j + "-" + k);
                }
            }
        };

        String[] date_split = getTodysdate();
        DatePickerDialog datePickerDialog =
                //new DatePickerDialog(Billing.this,R.style.MyDialogTheme,myDateSetListener, 2017, 9, 13);
                new DatePickerDialog(TodaysAccount.this, myDateSetListener, Integer.parseInt(date_split[2]), Integer.parseInt(date_split[1]) - 1, Integer.parseInt(date_split[0]));
        datePickerDialog.setTitle("From Date");
        datePickerDialog.show();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("My order");
            ab.setElevation(0);
            ab.setDisplayHomeAsUpEnabled(true);

        }
    }

    public static class DatePickerDialogThemeIN extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            //  final long today = System.currentTimeMillis();
            final long today = System.currentTimeMillis();

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, this, year, month, day);

            //  datepickerdialog.getDatePicker().setMinDate(today);
            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar c2 = Calendar.getInstance();
            c2.set(year, monthOfYear, dayOfMonth);
            Year = year;
            Month = monthOfYear;
            Day = dayOfMonth;
            roomcheckin = dateFormatsample.format(c2.getTime());
            String roomcheckinset = dateFormatsampleSET.format(c2.getTime());
            roomcheckinSEND = dateFormatsampleSEND.format(c2.getTime());
            fromdatetxt.setText(roomcheckinset);
        }
    }

    public static class DatePickerDialogThemeOut extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            //  final long today = System.currentTimeMillis();
            final long today = System.currentTimeMillis();

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, this, year, month, day);

            //  datepickerdialog.getDatePicker().setMinDate(today);
            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar ca2 = Calendar.getInstance();
            ca2.set(year, monthOfYear, dayOfMonth);
            YearO = year;
            MonthO = monthOfYear;
            DayO = dayOfMonth;
            final long today = System.currentTimeMillis();

            if (ca2.getTimeInMillis() > today) {

                //  showDatePickerDialogcheckin();

            } else {
                roomcheckout = dateFormatsample.format(ca2.getTime());

                roomcheckoutset = dateFormatsampleSET.format(ca2.getTime());
                roomcheckoutSEND = dateFormatsampleSEND.format(ca2.getTime());
                todatetxt.setText(roomcheckoutset);


            }


        }
    }


    protected class AsyncStore extends AsyncTask<String, Void, ArrayList<Taccount>> {

        ArrayList<Taccount> masterStoreArrayList = null;

        String userId = sharedpreferences.getString("userId", "");
        String storeid = sharedpreferences.getString("storeId", "");

        @Override
        protected ArrayList<Taccount> doInBackground(String... params) {
            RestAPI api = new RestAPI(TodaysAccount.this);
            try {
                JSONObject jsonObject = null;
                if (params[0].equals("0")) {
                    jsonObject = api.cms_getAccountnumberTodays(storeid);  //todays report
                } else {
                    jsonObject = api.cms_getAccountnumberByLogin(userId, storeid, roomcheckinSEND, roomcheckoutSEND); //member bill report
                }

                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                Taccount masterStore = null;
                String billid = "0";
                String billNo = "0";
                masterStoreArrayList = new ArrayList<Taccount>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    masterStore = new Taccount();
                    billid = jsonObject1.optString("billnumber");//store id
                    String account = jsonObject1.optString("accountnumber");
                    billNo = jsonObject1.optString("billnumber");

                    masterStore.setBillid(billid);
                    masterStore.setAcc(account);
                    masterStore.setBillNo(billNo);
                    masterStoreArrayList.add(masterStore);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return masterStoreArrayList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(TodaysAccount.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(ArrayList<Taccount> s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s != null) {
                Tlistadapter adapter = new Tlistadapter(TodaysAccount.this, s, mpage);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // ListView Clicked item index
                        int itemPosition = position;
                        Intent i = new Intent(TodaysAccount.this, Report.class);
                        i.putExtra("Billid", masterStoreArrayList.get(position).getBillid());
                        i.putExtra("mPage", mpage);
                        i.putExtra("billno", masterStoreArrayList.get(position).getBillNo());
                        i.putExtra("accNo", masterStoreArrayList.get(position).getAcc());
                        startActivity(i);

                    }

                });
            } else {
                Toast.makeText(TodaysAccount.this, "No order placed", Toast.LENGTH_SHORT).show();
            }

        }
    }

    protected class AsyncgetBilllist extends AsyncTask<String, Void, ArrayList<Taccount>> {

        ArrayList<Taccount> masterStoreArrayList = null;

        String userId = sharedpreferences.getString("userId", "");
        String storeid = sharedpreferences.getString("storeId", "");

        @Override
        protected ArrayList<Taccount> doInBackground(String... params) {
            RestAPI api = new RestAPI(TodaysAccount.this);
            try {
                JSONObject jsonObject = null;
                if (params[0].equals("0")) {
                    jsonObject = api.cms_getAccountnumberTodays(storeid);  //todays report
                } else {
                    jsonObject = api.cms_getAccountnumberByLogin(userId, storeid, roomcheckinSEND, roomcheckoutSEND); //member bill report
                }

                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                Taccount masterStore = null;
                String billid = "0";
                String billNo = "0";
                masterStoreArrayList = new ArrayList<Taccount>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    masterStore = new Taccount();
                    billid = jsonObject1.optString("billnumber");//store id
                    String account = jsonObject1.optString("accountnumber");
                    billNo = jsonObject1.optString("billnumber");

                    masterStore.setBillid(billid);
                    masterStore.setAcc(account);
                    masterStore.setBillNo(billNo);
                    masterStoreArrayList.add(masterStore);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return masterStoreArrayList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(TodaysAccount.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(ArrayList<Taccount> s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s != null) {
                Tlistadapter adapter = new Tlistadapter(TodaysAccount.this, s, mpage);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // ListView Clicked item index
                        int itemPosition = position;
                        Intent i = new Intent(TodaysAccount.this, Report.class);
                        i.putExtra("Billid", masterStoreArrayList.get(position).getBillid());
                        i.putExtra("mPage", mpage);
                        i.putExtra("billno", masterStoreArrayList.get(position).getBillNo());
                        i.putExtra("accNo", masterStoreArrayList.get(position).getAcc());
                        startActivity(i);

                    }

                });
            } else {
                Toast.makeText(TodaysAccount.this, "No order placed", Toast.LENGTH_SHORT).show();
            }

        }
    }


}
