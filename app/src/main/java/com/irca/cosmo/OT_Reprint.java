package com.irca.cosmo;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.irca.Billing.BillingProfile;
import com.irca.Billing.MakeOrder;
import com.irca.Printer.DiscoveryActivity;
import com.irca.Utils.ShowMsg;
import com.irca.db.Dbase;
import com.irca.fields.CloseorderItems;
import com.irca.fields.ItemList;
import com.irca.scan.PermissionsManager;
import com.zcs.sdk.DriverManager;
import com.zcs.sdk.SdkResult;
import com.zcs.sdk.Sys;
import com.zcs.sdk.print.PrnStrFormat;
import com.zcs.sdk.print.PrnTextFont;
import com.zcs.sdk.print.PrnTextStyle;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
/*

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
*/

public class OT_Reprint extends AppCompatActivity implements ReceiveListener {
    LinearLayout customList;
    View customView;
    TextView itemName, qty, amount, title, itemCount, _itemRate;
    ImageView add, subtract;
    CheckBox happyhour;
    Dbase dbase;
    String method = "";
    LinearLayout removeButton;
    Button print_butt;
    Button print_buttnew;
    Dbase db;
    float Totalamount = 0;
    StringBuilder textData = new StringBuilder();
    SharedPreferences pref;
    String _target = "", _printer = "";
    String print = "";

    private Printer mPrinter = null;
    private Context mContext = null;

    ProgressDialog pd;

    String clubName = "", clubaddress = "";


    int page;

    ArrayList<ItemList> cancle_order;
    RelativeLayout printer;
    EditText printer_Name;
    ImageView bluetooth;

    String waiter = "";
    String counter_name = "";
    String billType = "";
    SharedPreferences sharedpreferences;
    String memName = "";
    String memAccNo = "";
    int tableNo = 0;
    double servicetax = 0, vat = 0, GrantTotal = 0, billAmt = 0, cessTax = 0;
    String cardType = "";
    TextView amt;
    String classtype = "";
    int count = 0;
    Bundle bundle;
    //ImageView logo ;
    //  Bitmap bitmap;


    private DriverManager mDriverManager = DriverManager.getInstance();
    private com.zcs.sdk.Printer mPrinter1;

    private ExecutorService mSingleThreadExecutor;

    private PermissionsManager mPermissionsManager;
    private Sys mSys = mDriverManager.getBaseSysDevice();


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @SuppressLint("RestrictedApi")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("" + "Ot Reprint List");
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setDefaultDisplayHomeAsUpEnabled(true);
            ab.setElevation(0);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_ot__reprint);
        mContext = this;

        // logo=(ImageView)findViewById(R.id.imageView_logo);
        //  bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
        //  logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));

        pref = getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        waiter = sharedpreferences.getString("WaiterName", "");
        _target = pref.getString("Target", "");
        _printer = pref.getString("Printer", "");
        counter_name = sharedpreferences.getString("StoreName", "");
        billType = sharedpreferences.getString("billType", "");

        printer = (RelativeLayout) findViewById(R.id.pr);
        printer_Name = (EditText) findViewById(R.id.edtTarget1);
        bluetooth = (ImageView) findViewById(R.id.bluetooth);
        amt = (TextView) findViewById(R.id.itmAmt);

        clubName = sharedpreferences.getString("clubName", "");
        clubaddress = sharedpreferences.getString("clubAddress", "");

        print_butt = (Button) findViewById(R.id.button_cancelprint);
        print_buttnew = (Button) findViewById(R.id.button_cancelprintnew);


        if (_printer.equalsIgnoreCase("") && _target.equalsIgnoreCase("")) {


//            printer_Name.setText("TM-P20_011044" + "-" + "BT:00:01:90:AE:93:17");
            printer_Name.setText("TM-P80_002585" + "-" + "BT:00:01:90:88:DA:48");

//            printer_Name.setText("TM-P20_000452" + "-" + "BT:00:01:90:C2:AE:77");
        } else {

            printer_Name.setText(_printer + "-" + _target);
        }

        bundle = getIntent().getExtras();
        classtype = bundle.getString("type");
        bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Find bluetooth
                Intent intent = new Intent(getApplication(), DiscoveryActivity.class);
                startActivityForResult(intent, 0);

            }
        });
        //amt.setVisibility(View.GONE);
        printer = (RelativeLayout) findViewById(R.id.pr);
        print_butt = (Button) findViewById(R.id.button_cancelprint);
        print_buttnew = (Button) findViewById(R.id.button_cancelprintnew);
        if (classtype.equalsIgnoreCase("BillingProfile")) {
            if (BillingProfile.closeorderItemsArrayList1.size() == 0 || BillingProfile.closeorderItemsArrayList1 == null) {

                print_buttnew.setVisibility(View.GONE);
                print_butt.setVisibility(View.GONE);
            } else {
                print_buttnew.setVisibility(View.VISIBLE);
                print_butt.setVisibility(View.VISIBLE);
            }
        } else {
            if (classtype.equalsIgnoreCase("makeorder")) {
                print_butt.setVisibility(View.GONE);
                print_buttnew.setVisibility(View.GONE);
            }
        }

        print_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                print = printer_Name.getText().toString();
                if (classtype.equalsIgnoreCase("BillingProfile")) {
                    if (BillingProfile.closeorderItemsArrayList1.size() == 0 || BillingProfile.closeorderItemsArrayList1 == null) {
                        //  Toast.makeText(getApplicationContext(), "Please ,Cancel the order ", Toast.LENGTH_SHORT).show();
                    }
                } else if (classtype.equalsIgnoreCase("makeorder")) {
                    if (MakeOrder.closeorderItemsArrayList1.size() == 0 || MakeOrder.closeorderItemsArrayList1 == null) {
                        //  Toast.makeText(getApplicationContext(), "Please ,Cancel the order ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (print.equals("")) {

                        Toast.makeText(OT_Reprint.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                    } else {
                        /*print_butt.setBackgroundColor(getResources().getColor(R.color.pink));
                        print_butt.setText("Connecting Printer...");
                        updateButtonState(false);
                        if (!runPrintReceiptSequence()) {
                            updateButtonState(true);
                        }*/
                        try {
                            print_butt.setEnabled(false);
                            Activity mActivity = OT_Reprint.this;

                            mActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    new OT_Reprint.AsyncPrinting().execute();
                                }
                            });
                        } catch (Exception e) {
                            ShowMsg.showException(e, "AsyncTask", mContext);
                        }
                    }
                }

            }
        });

        print_buttnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                print = printer_Name.getText().toString();
                if (classtype.equalsIgnoreCase("BillingProfile")) {
                    if (BillingProfile.closeorderItemsArrayList1.size() == 0 || BillingProfile.closeorderItemsArrayList1 == null) {
                         Toast.makeText(getApplicationContext(), "No items! ", Toast.LENGTH_SHORT).show();
                    }
                    else {

                        try {
                            print_buttnew.setEnabled(false);
                            Activity mActivity = OT_Reprint.this;

                            mActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    mSingleThreadExecutor = mDriverManager.getSingleThreadExecutor();
                                    mDriverManager = DriverManager.getInstance();
                                    mPrinter1 = mDriverManager.getPrinter();

                                    new OT_Reprint.AsyncPrintingPoona().execute();
                                }
                            });
                        } catch (Exception e) {
                            ShowMsg.showException(e, "AsyncTask", mContext);
                        }

                    }
                } else if (classtype.equalsIgnoreCase("makeorder")) {
                    if (MakeOrder.closeorderItemsArrayList1.size() == 0 || MakeOrder.closeorderItemsArrayList1 == null) {
                        //  Toast.makeText(getApplicationContext(), "Please ,Cancel the order ", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    try {
                        print_buttnew.setEnabled(false);
                        Activity mActivity = OT_Reprint.this;

                        mActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                mSingleThreadExecutor = mDriverManager.getSingleThreadExecutor();
                                mDriverManager = DriverManager.getInstance();
                                mPrinter1 = mDriverManager.getPrinter();

                                new OT_Reprint.AsyncPrintingPoona().execute();
                            }
                        });
                    } catch (Exception e) {
                        ShowMsg.showException(e, "AsyncTask", mContext);
                    }

                }

            }
        });


        cancelOrder();
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        if (data != null && resultCode == RESULT_OK) {
            String target = data.getStringExtra(getString(R.string.title_target));
            String printer = data.getStringExtra(getString(R.string.title_Printer));
            if (printer != null) {
                EditText mEdtTarget = (EditText) findViewById(R.id.edtTarget1);
                mEdtTarget.setText(printer + "-" + target);

            }
        }
    }

    public void cancelOrder() {
        ArrayList<CloseorderItems> _closeorderItemsArrayList = null;
        try {
            if (classtype.equalsIgnoreCase("BillingProfile")) {
                _closeorderItemsArrayList = BillingProfile.closeorderItemsArrayList1;
            } else if (classtype.equalsIgnoreCase("MakeOrder")) {
                _closeorderItemsArrayList = MakeOrder.closeorderItemsArrayList1;
            }

            for (int i = 0; i < _closeorderItemsArrayList.size(); i++) {
                if (customList == null) {
                    customList = (LinearLayout) findViewById(R.id.closeItemView);
                    customView = getLayoutInflater().inflate(R.layout.itemlist_kot, customList, false);
                    itemName = (TextView) customView.findViewById(R.id.kot_itemname);
                    itemCount = (TextView) customView.findViewById(R.id.kot_itemCount);
                    _itemRate = (TextView) customView.findViewById(R.id.kot_rate);
                    // happyhour = (CheckBox) customView.findViewById(R.id.happyhour_checbox);
                    // happyhour.setVisibility(View.GONE);
                    add = (ImageView) customView.findViewById(R.id.kot_add);
                    subtract = (ImageView) customView.findViewById(R.id.kot_minus);
                    add.setVisibility(View.GONE);
                    subtract.setVisibility(View.GONE);
                    // itemName.setText("Item"+i);
                    //itemCount.setText(""+i);
                    _itemRate.setText(_closeorderItemsArrayList.get(i).rate);
                    itemName.setText(_closeorderItemsArrayList.get(i).ItemCode + "-" + _closeorderItemsArrayList.get(i).itemname);
                    itemCount.setText(_closeorderItemsArrayList.get(i).Quantity);


                } else {
                    customView = getLayoutInflater().inflate(R.layout.itemlist_kot, customList, false);
                    itemName = (TextView) customView.findViewById(R.id.kot_itemname);
                    itemCount = (TextView) customView.findViewById(R.id.kot_itemCount);
                    _itemRate = (TextView) customView.findViewById(R.id.kot_rate);
                    //happyhour = (CheckBox) customView.findViewById(R.id.happyhour_checbox);
                    // happyhour.setVisibility(View.GONE);
                    add = (ImageView) customView.findViewById(R.id.kot_add);
                    add.setVisibility(View.GONE);

                    subtract = (ImageView) customView.findViewById(R.id.kot_minus);
                    subtract.setVisibility(View.GONE);
//                itemName.setText("Item"+i);
//                itemCount.setText(""+i);
                    _itemRate.setText(_closeorderItemsArrayList.get(i).rate);
                    itemName.setText(_closeorderItemsArrayList.get(i).ItemCode + "-" + _closeorderItemsArrayList.get(i).itemname);
                    itemCount.setText(_closeorderItemsArrayList.get(i).Quantity);
                }
                customList.addView(customView);


            }
        } catch (Exception e) {
            String mm = e.getMessage().toString();
        }
    }

    //***************************** PRINTER  *********************************//


    public class AsyncPrinting extends AsyncTask<Void, Void, String> {

        String status = "";

        @Override
        protected String doInBackground(Void... params) {

            try {
                mPrinter = new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
                //       mPrinter= new Printer(Printer.TM_P80, Printer.MODEL_ANK, mContext);
                mPrinter.setReceiveEventListener(OT_Reprint.this);
            } catch (Exception e) {
                ShowMsg.showException(e, "Printer", mContext);
            }
            try {
                // if (!createReceiptData1())
                if (!createReceiptData2()) {
                    if (mPrinter == null) {
                        status = "false";
                    }
                    mPrinter.clearCommandBuffer();
                    mPrinter.setReceiveEventListener(null);
                    mPrinter = null;
                    status = "false";
                } else {
                    status = "true";
                }
            } catch (Exception e) {
                ShowMsg.showException(e, "other", mContext);
            }

            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(OT_Reprint.this);
            pd.setMessage("Printing Please Wait .......");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            if (status.equals("true")) {

                try {

                    boolean print = printData();
                    if (!print) {
                        if (mPrinter == null) {
                            status = "false";
                        }

                        mPrinter.clearCommandBuffer();

                        mPrinter.setReceiveEventListener(null);

                        mPrinter = null;

                    } else {
                        count = count + 1;
                        status = "true";
                    }
                } catch (Exception e) {
                    ShowMsg.showException(e, "Post execute error", mContext);
                }
                // updateButtonState(true);
                Toast.makeText(OT_Reprint.this, "Printing Success", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else if (status.equals("")) {
                Toast.makeText(OT_Reprint.this, "Printing failed to enter", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else {
                Toast.makeText(OT_Reprint.this, "Printing failed to enter" + status, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }

    public class AsyncPrintingPoona extends AsyncTask<Void, Void, String> {

        String status = "";

        @Override
        protected String doInBackground(Void... params) {

            try {


                mSingleThreadExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        int printStatus = mPrinter1.getPrinterStatus();
                        if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                            OT_Reprint.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(OT_Reprint.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                                }
                            });
                        } else {
                            try {
                                createReceiptData2poona();
/*
                                    if (type.equals("k") || type.equals("kk")) {
                                        createReceiptDatapoona();
                                    } else {
                                        createReceiptData1poona();
                                    }*/
                                //   createReceiptDatapoona();
                            } catch (Exception e) {
                                Log.d("catch exp :", e + "");
                            }

                        }
                    }
                });


                // if (!createReceiptData1())

                status = "true";

            } catch (Exception e) {
                ShowMsg.showException(e, "other", mContext);
            }

            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(OT_Reprint.this);
            pd.setMessage("Printing Please Wait .......");
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            if (status.equals("true")) {

                try {

                        count = count + 1;
                        status = "true";

                } catch (Exception e) {
                    ShowMsg.showException(e, "Post execute error", mContext);
                }
                // updateButtonState(true);
                Toast.makeText(OT_Reprint.this, "Printing Success", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else if (status.equals("")) {
                Toast.makeText(OT_Reprint.this, "Printing failed to enter", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else {
                Toast.makeText(OT_Reprint.this, "Printing failed to enter" + status, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }

    public boolean runPrintReceiptSequence() {
        if (!initializeObject()) {
            return false;
        }

        if (!createReceiptData1()) {
            finalizeObject();
            return false;
        }


        if (!printData()) {
            finalizeObject();
            return false;
        }

        count = count + 1;
        return true;
    }

    public boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            return false;
        }

        PrinterStatusInfo status = mPrinter.getStatus();

        dispPrinterWarnings(status);

        if (!isPrintable(status)) {
            ShowMsg.showMsg(makeErrorMessage(status), mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    public boolean initializeObject() {
        try {
               /* mPrinter = new Printer(((SpnModelsItem) mSpnSeries.getSelectedItem()).getModelConstant(),
                        ((SpnModelsItem) mSpnLang.getSelectedItem()).getModelConstant(),
                        mContext);*/
            mPrinter = new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
            //   mPrinter= new Printer(Printer.TM_P80, Printer.MODEL_ANK, mContext);
        } catch (Exception e) {
            ShowMsg.showException(e, "Printer", mContext);
            return false;
        }

        mPrinter.setReceiveEventListener(this);

        return true;
    }

    public void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    public boolean connectPrinter() {
        boolean isBeginTransaction = false;
        String ll = "";

        if (mPrinter == null) {
            return false;
        }

        try {
            ll = printer_Name.getText().toString();
            if (!ll.equals("")) {
                mPrinter.connect(printer_Name.getText().toString(), Printer.PARAM_DEFAULT);
            } else {
                String[] l = ll.split("-");
                mPrinter.connect(l[1], Printer.PARAM_DEFAULT);
            }
        } catch (Exception e) {
            ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        try {
            mPrinter.beginTransaction();
            isBeginTransaction = true;
        } catch (Exception e) {
            ShowMsg.showException(e, "beginTransaction", mContext);
        }

        if (isBeginTransaction == false) {
            try {
                mPrinter.disconnect();
            } catch (Epos2Exception e) {
                // Do nothing
                return false;
            }
        }

        return true;
    }

    public void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "endTransaction", mContext);
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "disconnect", mContext);
                }
            });
        }

        finalizeObject();
    }

    public boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            ;//print available
        }

        return true;
    }

    public String makeErrorMessage(PrinterStatusInfo status) {
        String msg = "";

        if (status.getOnline() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_offline);
        }
        if (status.getConnection() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_no_response);
        }
        if (status.getCoverOpen() == Printer.TRUE) {
            msg += getString(R.string.handlingmsg_err_cover_open);
        }
        if (status.getPaper() == Printer.PAPER_EMPTY) {
            msg += getString(R.string.handlingmsg_err_receipt_end);
        }
        if (status.getPaperFeed() == Printer.TRUE || status.getPanelSwitch() == Printer.SWITCH_ON) {
            msg += getString(R.string.handlingmsg_err_paper_feed);
        }
        if (status.getErrorStatus() == Printer.MECHANICAL_ERR || status.getErrorStatus() == Printer.AUTOCUTTER_ERR) {
            msg += getString(R.string.handlingmsg_err_autocutter);
            msg += getString(R.string.handlingmsg_err_need_recover);
        }
        if (status.getErrorStatus() == Printer.UNRECOVER_ERR) {
            msg += getString(R.string.handlingmsg_err_unrecover);
        }
        if (status.getErrorStatus() == Printer.AUTORECOVER_ERR) {
            if (status.getAutoRecoverError() == Printer.HEAD_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_head);
            }
            if (status.getAutoRecoverError() == Printer.MOTOR_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_motor);
            }
            if (status.getAutoRecoverError() == Printer.BATTERY_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_battery);
            }
            if (status.getAutoRecoverError() == Printer.WRONG_PAPER) {
                msg += getString(R.string.handlingmsg_err_wrong_paper);
            }
        }
        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_0) {
            msg += getString(R.string.handlingmsg_err_battery_real_end);
        }

        return msg;
    }

    public void dispPrinterWarnings(PrinterStatusInfo status) {
        EditText edtWarnings = (EditText) findViewById(R.id.edtWarnings1);

        String warningsMsg = "";

        if (status == null) {
            return;
        }

        if (status.getPaper() == Printer.PAPER_NEAR_END) {
            warningsMsg += getString(R.string.handlingmsg_warn_receipt_near_end);
        }

        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_1) {
            warningsMsg += getString(R.string.handlingmsg_warn_battery_near_end);
        }

        edtWarnings.setText(warningsMsg);
    }

    public void updateButtonState(boolean state) {
        Button btnReceipt = (Button) findViewById(R.id.button_cancelprint);
        //Button btnCoupon = (Button)findViewById(R.id.btnSampleCoupon);

        if (state == true) {
            if (count == 0) {
                btnReceipt.setEnabled(state);
                btnReceipt.setBackgroundColor(getResources().getColor(R.color.violet));
                btnReceipt.setText("Print");

            } else {
                btnReceipt.setEnabled(state);
                btnReceipt.setBackgroundColor(getResources().getColor(R.color.violet));
                btnReceipt.setText("Re-Print");

            }
        } else {
            btnReceipt.setBackgroundColor(getResources().getColor(R.color.pink));
            btnReceipt.setEnabled(state);
            btnReceipt.setText("Printing ...");
        }
        //  btnCoupon.setEnabled(state);
    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
                ShowMsg.showResult(code, makeErrorMessage(status), mContext);

                dispPrinterWarnings(status);

                updateButtonState(true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        disconnectPrinter();
                    }
                }).start();
            }
        });
    }


    public boolean createReceiptData1() {
        ArrayList<CloseorderItems> _closeorderItemsArrayList1 = BillingProfile.closeorderItemsArrayList1;
        String method = "";
        StringBuilder textData = new StringBuilder();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());

        if (mPrinter == null) {
            return false;
        }
        try {
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            //   textData.append("BANGALORE GOLF CLUB\n");
            textData.append(clubName + "\n");
            textData.append("KOT /BOT  (RE-Print)\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            textData.append("--------------------------------------\n");
            //  textData.append("Table No.  " + tableNo + "\n");
            textData.append("Table No.  " + 0 + "\n");
            textData.append("--------------------------------------\n");

            if (billType.equals("KOT")) {
                textData.append("KOT No.    " + ": " + _closeorderItemsArrayList1.get(0).otno + "\n");
            } else if (billType.equals("BOT")) {
                textData.append("BOT No.    " + ": " + _closeorderItemsArrayList1.get(0).otno + "\n");
            }


            textData.append("Counter    " + ": " + counter_name + "\n");
            textData.append("Date Time  " + ": " + date + "\n");
            // textData.append("Bill Clerk "+": "+waiter+"\n");
            textData.append("Steward " + ": " + waiter + "\n");
            textData.append("Waiter     " + ": " + BillingProfile.waiternamecode + "\n");
            textData.append("--------------------------------------\n");
            textData.append("Member      " + ":" + _closeorderItemsArrayList1.get(0).mName + "\n");
            textData.append("Member Code " + ":" + _closeorderItemsArrayList1.get(0).mAcc + "\n");
            textData.append("--------------------------------------\n");
            textData.append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(0, 0, 0, Printer.COLOR_2);
           /* textData.append("Code  "
                           +" "
                           +"Item Name             "
                           +" "
                           +"Qty "
                           +" "
                           +"Rate "
                           +"\n"); */

            textData.append("Code  "
                    + " "
                    + "Item Name                   "
                           /*+" "
                           +"Qty "*/
                    + " "
                    + "Qty "
                    + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);

            for (int j = 0; j < _closeorderItemsArrayList1.size(); j++) {
                textData.append(
                        (_closeorderItemsArrayList1.get(j).ItemCode + "          ").substring(0, 6)
                                + " "
                                + String.format("%28s", (_closeorderItemsArrayList1.get(j).itemname + "                                   ").substring(0, 28))
                              /* + " "
                               +(KotItemlist.placeOrder_list.get(j).quantity+"      ").substring(0,4)*/
                                // +" "
                                + String.format("%4s", (_closeorderItemsArrayList1.get(j).Quantity))
                                + "\n");
                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }


            textData.append("------------------------------------------\n");

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);

        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }


    public boolean createReceiptData2() {
        if (mPrinter == null) {
            return false;
        }

        try {
            dbase = null;
            dbase = new Dbase(OT_Reprint.this);
            textData = new StringBuilder();
            ArrayList<String> otlist = dbase.getOtreprint();
            for (int i = 0; i < otlist.size(); i++) {
                String itemcategoryid = otlist.get(i).toString();
                printHeader();
                ArrayList<CloseorderItems> otdetsils = dbase.getotreprintDetails(itemcategoryid);
                printBody(otdetsils);
            }
            count++;

        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }

    public boolean createReceiptData2poona() {


        try {
            dbase = null;
            dbase = new Dbase(OT_Reprint.this);
            textData = new StringBuilder();
            ArrayList<String> otlist = dbase.getOtreprint();
            for (int i = 0; i < otlist.size(); i++) {
                String itemcategoryid = otlist.get(i).toString();
                printHeaderpoona();
                ArrayList<CloseorderItems> otdetsils = dbase.getotreprintDetails(itemcategoryid);
                printBodypoona(otdetsils);
            }
            count++;

        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }

    private void printBody(ArrayList<CloseorderItems> otdetsils) {

        try {
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);

            for (int j = 0; j < otdetsils.size(); j++) {

//                if (MakeOrder.placeOrder_list.get(j).status.equalsIgnoreCase("N") || MakeOrder.placeOrder_list.get(j).status.equalsIgnoreCase("CN")) {
//                    continue;
//                }
//                textData.append(
//
//                        //(otdetsils.get(j).getItemCode() + "          ").substring(0, 6)
//                        (otdetsils.get(j).getItemCode() + "          ")
//                                + " "
//                                + String.format("%28s", (otdetsils.get(j).getItemname() + "                                   ").substring(0, 28))
//                              /* + " "
//                               +(PlaceOrder_new.placeOrder_list.get(j).quantity+"      ").substring(0,4)*/
//                                // +" "
//                                + String.format("%4s", (otdetsils.get(j).getQuantity()))
//                                + "\n");

                textData.append(
                        String.format("%28s", (otdetsils.get(j).getItemCode() + "-" + otdetsils.get(j).getItemname() + "                                   ").substring(0, 28))
                                + String.format("%4s", (otdetsils.get(j).getQuantity()))
                                + "\n");

                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }

            //textData.append("------------------------------------------\n");

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);
        } catch (Exception e) {

        }
    }
  private void printBodypoona(ArrayList<CloseorderItems> otdetsils) {
      try {

          int printStatus = mPrinter1.getPrinterStatus();
          if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
              OT_Reprint.this.runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                      Toast.makeText(OT_Reprint.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                  }
              });
          } else {
              PrnStrFormat format = new PrnStrFormat();
              format.setAli(Layout.Alignment.ALIGN_NORMAL);
              format.setStyle(PrnTextStyle.NORMAL);
              format.setTextSize(25);
            for (int j = 0; j < otdetsils.size(); j++) {



                if( otdetsils.get(j).getItemname().length()>20){
                    String itemstr1= otdetsils.get(j).getItemname().substring(0,20);
                    String itemstr2="..."+ otdetsils.get(j).getItemname().substring(20,otdetsils.get(j).getItemname().length());

                    String code= (otdetsils.get(j).getItemCode()  + "********************").substring(0,6);
                    code=code.replace("*","   ");

                    mPrinter1.setPrintAppendString
                            (code+ " "
                                    + itemstr1 + " "
                                    +  otdetsils.get(j).getQuantity()
                            , format);
                    mPrinter1.setPrintAppendString(

                            (    "          ")

                                    + itemstr2
                            , format);
                }
                else if( otdetsils.get(j).getItemname().length()==19){
                    //    String itemname= (MakeOrder.placeOrder_list.get(j).itemName+"----------------------------------------------------------------------------").substring(0,30);
                    String itemname= ( otdetsils.get(j).getItemname()+"********************").substring(0,20);
                    itemname=itemname.replace("*","   ");
                    String code= (otdetsils.get(j).getItemCode()  + "********************").substring(0,6);
                    code=code.replace("*","   ");

                    String qty= ( otdetsils.get(j).getQuantity()  + "                                                                           ").substring(0,4);
                    mPrinter1.setPrintAppendString(


                            (code  + " "
                                    + itemname
                                    + " "
                                    +  qty ), format);
                }     else {
                    //    String itemname= (MakeOrder.placeOrder_list.get(j).itemName+"----------------------------------------------------------------------------").substring(0,30);
                    String itemname= ( otdetsils.get(j).getItemname()+"********************").substring(0,19);
                    itemname=itemname.replace("*","   ");
                    String code= (otdetsils.get(j).getItemCode() + "********************").substring(0,6);
                    code=code.replace("*","   ");

                    String qty= ( otdetsils.get(j).getQuantity() + "                                                                           ").substring(0,4);
                    mPrinter1.setPrintAppendString(


                            (code  + " "
                                    + itemname
                                    + " "
                                    +  qty ), format);
                }

            }
              mPrinter1.setPrintAppendString(" ", format);
              mPrinter1.setPrintAppendString(" ", format);
              mPrinter1.setPrintAppendString(" ", format);
              mPrinter1.setPrintAppendString(" ", format);
              printStatus = mPrinter1.setPrintStart();
              if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                  OT_Reprint.this.runOnUiThread(new Runnable() {
                      @Override
                      public void run() {
                          Toast.makeText(OT_Reprint.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                      }
                  });
              }

          }

      }catch (Exception e) {

      }
    }

    private void printHeader() {
        ArrayList<CloseorderItems> _closeorderItemsArrayList1 = BillingProfile.closeorderItemsArrayList1;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());


        try {
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);


            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            // textData.append("Cosmopolitan Club (Regd.)\n");
            textData.append(clubName + "\n");
            if (count == 0) {
                textData.append("KOT /BOT\n");
            } else {
                textData.append("KOT /BOT (RE-Print)\n");
            }
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            textData.append("--------------------------------------\n");
            textData.append("Table No.  " + tableNo + "\n");
            textData.append("--------------------------------------\n");
            if (billType.contains("KOT")) {
                textData.append("KOT No.    " + ": " + _closeorderItemsArrayList1.get(0).otno + "\n");
            } else {
                textData.append("BOT No.    " + ": " + _closeorderItemsArrayList1.get(0).otno + "\n");

            }

            textData.append("Counter    " + ": " + counter_name + "\n");
            textData.append("Date Time  " + ": " + date + "\n");
            //     textData.append("Bill Clerk " + ": " + waiter + "\n");
            textData.append("Steward " + ": " + waiter + "\n");
            textData.append("Waiter     " + ": " + TakeOrder.waiternamecode + "\n");
            textData.append("--------------------------------------\n");
            textData.append("Member      " + ":" + _closeorderItemsArrayList1.get(0).mName + "\n");
            textData.append("Member Code " + ":" + _closeorderItemsArrayList1.get(0).mAcc + "\n");
            textData.append("--------------------------------------\n");
            textData.append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            // mPrinter.addTextStyle(0, 0, 0, Printer.COLOR_2);
           /* textData.append("Code  "
                           +" "
                           +"Item Name             "
                           +" "
                           +"Qty "
                           +" "
                           +"Rate "
                           +"\n"); */

//            textData.append("Code  "
//                    + " "
//                    + "Item Name                   "
//                           /*+" "
//                           +"Qty "*/
//                    + " "
//                    + "Qty "
//                    + "\n");

            textData.append(
                    "Item Name                   "
                            + "Qty "
                            + "\n");

            method = "addText";
            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
        } catch (Epos2Exception e) {
            e.printStackTrace();
        }

    }

    private void printHeaderpoona() {
        ArrayList<CloseorderItems> _closeorderItemsArrayList1 = BillingProfile.closeorderItemsArrayList1;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());


        int printStatus = mPrinter1.getPrinterStatus();
        if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
            OT_Reprint.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(OT_Reprint.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                }
            });
        } else {
        }

        try {
            PrnStrFormat format = new PrnStrFormat();
            format.setTextSize(30);
            format.setAli(Layout.Alignment.ALIGN_CENTER);
            format.setFont(PrnTextFont.DEFAULT);

            mPrinter1.setPrintAppendString(clubName, format);

            format.setTextSize(25);
            format.setStyle(PrnTextStyle.NORMAL);
            format.setAli(Layout.Alignment.ALIGN_CENTER);


            if (count == 0) {
                //  textData.append("KOT /BOT\\n");
                mPrinter1.setPrintAppendString("KOT /BOT", format);

            } else {
                mPrinter1.setPrintAppendString("KOT /BOT (RE-Print)", format);
            }
            mPrinter1.setPrintAppendString("--------------------------------------", format);
            mPrinter1.setPrintAppendString("Table No.  " + tableNo, format);
            mPrinter1.setPrintAppendString("--------------------------------------", format);
            format.setAli(Layout.Alignment.ALIGN_CENTER);
            format.setTextSize(30);
            format.setStyle(PrnTextStyle.BOLD);
            if (billType.contains("KOT")) {
                mPrinter1.setPrintAppendString("KOT No.    " + ": " + _closeorderItemsArrayList1.get(0).otno, format);

            } else {
                mPrinter1.setPrintAppendString("BOT No.    " + ": " + _closeorderItemsArrayList1.get(0).otno, format);


            }

            mPrinter1.setPrintAppendString("--------------------------------------", format);



            format.setAli(Layout.Alignment.ALIGN_NORMAL);
            format.setStyle(PrnTextStyle.NORMAL);
            format.setTextSize(25);
            mPrinter1.setPrintAppendString("Counter    " + ": " + counter_name, format);
            mPrinter1.setPrintAppendString("Date Time  " + ": " + date, format);
            mPrinter1.setPrintAppendString("Steward " + ": " + waiter, format);
            mPrinter1.setPrintAppendString("Waiter     " + ": " + TakeOrder.waiternamecode, format);
            mPrinter1.setPrintAppendString("--------------------------------------", format);
            format.setTextSize(30);
            format.setStyle(PrnTextStyle.BOLD);
            mPrinter1.setPrintAppendString("Member Name " + ":" +  _closeorderItemsArrayList1.get(0).mName, format);
            mPrinter1.setPrintAppendString("Member Code " + ":" +  _closeorderItemsArrayList1.get(0).mAcc, format);
            mPrinter1.setPrintAppendString("--------------------------------------", format);

            format.setAli(Layout.Alignment.ALIGN_NORMAL);
            format.setStyle(PrnTextStyle.NORMAL);
            format.setTextSize(25);

            mPrinter1.setPrintAppendString("Code  "
                    + " "
                    + "Item Name                               "
                       /*+" "
                       +"Qty "*/
                    + " "
                    + "Qty ", format);
            printStatus = mPrinter1.setPrintStart();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                OT_Reprint.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(OT_Reprint.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                    }
                });
            }


    } catch (Exception e) {
        e.printStackTrace();
    }

    }
        public boolean createReceiptData () {
            String method = "";
            StringBuilder textData = new StringBuilder();
            servicetax = 0;
            vat = 0;
            billAmt = 0;
            GrantTotal = 0;
            cessTax = 0;
            DecimalFormat format = new DecimalFormat("#.##");

            ArrayList<CloseorderItems> _closeorderItemsArrayList = TakeOrder.closeorderItemsArrayList;

            if (mPrinter == null) {
                return false;
            }
            try {
                method = "addTextAlign";
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);

                method = "addFeedLine";
                mPrinter.addFeedLine(0);


                method = "addTextSize";
                mPrinter.addTextSize(2, 2);
                mPrinter.addTextFont(Printer.FONT_E);
                mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                textData.append("BANGALORE GOLF CLUB\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_D);
                mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                textData.append("2, Sankey Road, Near-Seshadiripuram," + "\n" + " High Grounds, Bengaluru," + "\n" + "Karnataka 560001" + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());


                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_D);
                mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                textData.append("Bill No.   :" + _closeorderItemsArrayList.get(0).billno + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());


                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_D);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                textData.append("--------------------------------------\n");
                textData.append("Member  Id   " + ": " + _closeorderItemsArrayList.get(0).mAcc + "\n");
                textData.append("Member Name  " + ": " + _closeorderItemsArrayList.get(0).mName + "\n");

                textData.append("--------------------------------------\n");
                textData.append("Type       " + ":" + _closeorderItemsArrayList.get(0).cardType + "\n");
                // textData.append("Billed By  " + ":" + "           " + "\n");
                textData.append("Date Time  " + ":" + _closeorderItemsArrayList.get(0).BillDate + "\n");
                textData.append("Waiter     " + ":" + waiter + "\n");
                textData.append("--------------------------------------\n");

                //

                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append(
                        "Item Name             "

                                + String.format("%4s", "Qty")
                                //  + " "
                                + String.format("%8s", "Rate")
                                //  + " "
                                + String.format("%8s", "Amount")
                                + "\n");

                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_D);
                mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
                textData.append("--------------------------------------\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());


                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);

                for (int j = 0; j < _closeorderItemsArrayList.size(); j++) {
                    textData.append(
                            String.format("%18s", (_closeorderItemsArrayList.get(j).itemname + "                                                ").substring(0, 20))
                                    // +" "
                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).Quantity))

                                    + String.format("%8s", format.format
                                    (Double.parseDouble((_closeorderItemsArrayList.get(j).rate))))
                                    // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                    // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                    + String.format("%10s", format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount))))
                                    // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                    // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                    + "\n");


                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                    //TODO include in manoch 25-4

                    servicetax = servicetax + Double.parseDouble(_closeorderItemsArrayList.get(j).getService_tax());
                    vat = vat + Double.parseDouble(_closeorderItemsArrayList.get(j).getSales_tax());
                    billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                    if (_closeorderItemsArrayList.get(j).getCessTax() == null) {
                        cessTax = cessTax + 0;
                    } else {
                        cessTax = cessTax + Double.parseDouble(_closeorderItemsArrayList.get(j).getCessTax());
                    }


                }

                textData.append("------------------------------------------\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                if (_closeorderItemsArrayList.get(0).cardType.equals("CLUB CARD")) {
                    GrantTotal = 0 + 0 + billAmt;
                    method = "addTextSize";
                    mPrinter.addTextSize(1, 1);
                    mPrinter.addTextFont(Printer.FONT_B);
                    mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                    mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                    textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");
                    textData.append("Service Tax" + " :  " + "0.0" + "\n");
                    textData.append("Vat        " + " :  " + "0.0" + "\n");
                    textData.append("Cess Tax   " + " :  " + "0.0" + "\n");
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());
                } else {
                    GrantTotal = servicetax + vat + billAmt;

                    method = "addTextSize";
                    mPrinter.addTextSize(1, 1);
                    mPrinter.addTextFont(Printer.FONT_B);
                    mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                    mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                    textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");
                    textData.append("Service Tax" + " :  " + format.format(servicetax) + "\n");
                    textData.append("Vat        " + " :  " + format.format(vat) + "\n");
                    textData.append("Cess Tax   " + " :  " + format.format(cessTax) + "\n");
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());
                }

                method = "addTextSize";
                mPrinter.addTextSize(1, 2);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal) + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());


                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("--------------------------------------\n");
                textData.append("\n");
                textData.append("\n");
                textData.append("(Signature)" + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_E);
                mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
                textData.append("* Thank you ! We Wish To Serve You Again ");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                method = "addFeedLine";
                mPrinter.addFeedLine(1);
                mPrinter.addCut(Printer.CUT_FEED);

            } catch (Exception e) {
                ShowMsg.showException(e, method, mContext);
                return false;
            }

            textData = null;

            return true;
        }


    }
