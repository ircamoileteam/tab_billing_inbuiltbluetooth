package com.irca.cosmo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.epson.eposprint.Builder;
import com.irca.Billing.Accountlist;
import com.irca.Billing.Activity_Signature;
import com.irca.Billing.BillingProfile;
import com.irca.Billing.MainActivity;
import com.irca.Printer.DiscoveryActivity;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.Utils.ShowMsg;
import com.irca.db.Dbase;
import com.irca.fields.CloseorderItems;
import com.irca.scan.PermissionsManager;
import com.zcs.sdk.DriverManager;
import com.zcs.sdk.SdkResult;
import com.zcs.sdk.Sys;
import com.zcs.sdk.print.PrnStrFormat;
import com.zcs.sdk.print.PrnTextFont;
import com.zcs.sdk.print.PrnTextStyle;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.ExecutorService;

/*
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
*/

/**
 * Created by Manoch Richard on 21-Mar-18.
 */

public class MultiPrintView extends AppCompatActivity implements ReceiveListener {
    SharedPreferences sharedpreferences;
    int tableNo = 0, tableId = 0;
    String table;
    ActionBar ab;
    TextView rate;
    LinearLayout removeButton, taxamount;
    TextView _mName, _mCode, billNo, bNo, steward;
    ArrayList<String> taxlist;
    private Context mContext = null;
    SharedPreferences pref;
    String waiter = "";
    String _target = "", _printer = "", _isdebitcard = "";
    String clubName = "", clubaddress = "";
    String counter_name = "";
    String billType = "";
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    TextView tot_tax, g_tot, a_bal;
    RelativeLayout printer;
    EditText printer_Name;
    Button bluetooth,btnSampleReceiptnew;
    TextView amt;
    Button cancelorder_butt, print_butt;
    int page;
    String memName = "";
    String memAccNo = "";
    String waiternamecode;
    String cardType = "";
    String ava_balance = "";
    String billid = "";
    String print = "",printertype;
    private Printer mPrinter = null;
    ProgressDialog pd;
    int count = 0;
    double servicetax = 0, vat = 0, GrantTotal = 0, billAmt = 0, discountAmt = 0, cessTax = 0, salesTax = 0, tax = 0, accharges;
    String _service = "0", _sales = "0", _vat = "0";
    LinearLayout customList;
    View customView;
    TextView itemName, qty, amount, itemCount,itmDis;
    //LinearLayout customTaxList;
    View customTaxView;
    TextView taxHeader, taxValue;
    //LinearLayout multi_printview;
    LinearLayout customTaxList;
    ArrayList<CloseorderItems> _closeorderItemsArrayList = null;
    DecimalFormat format = new DecimalFormat("#.##");
    String billnumer = "";
    //print
    StringBuilder textData = new StringBuilder();
    String method = "";
    ArrayList<String> list_bill = new ArrayList<>();
    ImageView img_signature;

    Bitmap bitmap = null;
    String signimg = "";

    private DriverManager mDriverManager = DriverManager.getInstance();
    private com.zcs.sdk.Printer mPrinter1;

    private ExecutorService mSingleThreadExecutor;

    private PermissionsManager mPermissionsManager;
    private Sys mSys = mDriverManager.getBaseSysDevice();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Fabric.with(this, new Crashlytics());
        setContentView(R.layout.order_multibill);


        initView();
        // logo=(ImageView)findViewById(R.id.imageView_logo);
        //   bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
        //  logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));
        taxlist = new ArrayList<>();
        mContext = this;
        final Bundle b = getIntent().getExtras();
        pref = getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        waiter = sharedpreferences.getString("WaiterName", "");
        _target = pref.getString("Target", "");
        _printer = pref.getString("Printer", "");
        clubName = sharedpreferences.getString("clubName", "");
        clubaddress = sharedpreferences.getString("clubAddress", "");
        counter_name = sharedpreferences.getString("StoreName", "");
        billType = sharedpreferences.getString("billType", "");
        printertype = sharedpreferences.getString("PrinterType", "");
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        EditText mEdtTarget = findViewById(R.id.edtTarget1);
        img_signature = findViewById(R.id.img_signature);
        img_signature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MultiPrintView.this, Activity_Signature.class);
                intent.putExtra("billid", BillingProfile.closeorderItemsArrayList.get(0).BillID);
                intent.putExtra("accno", BillingProfile.closeorderItemsArrayList.get(0).mAcc);
                intent.putExtra("pageValue", 0);
                intent.putExtra("isdebitcard", _isdebitcard);
                intent.putExtra("creditAno", memAccNo);
                intent.putExtra("creditName", memName);
                intent.putExtra("tableNo", tableNo);
                intent.putExtra("tableId", tableId);
                //  i.putExtra("waiterid", waiterid);
                intent.putExtra("cardType", cardType);
                intent.putExtra("waiternamecode", waiternamecode);
                startActivity(intent);
            }
        });

        mEdtTarget.setText(_printer + "-" + _target);

        if (_printer.equalsIgnoreCase("") && _target.equalsIgnoreCase("")) {


//            mEdtTarget.setText("TM-P20_011044" + "-" + "BT:00:01:90:AE:93:17");
      //      mEdtTarget.setText("TM-P80_002585" + "-" + "BT:00:01:90:88:DA:48");
        //    mEdtTarget.setText("TM-P20_012348-BT:00:01:90:B9:26:DB");

//            mEdtTarget.setText("TM-P20_000452" + "-" + "BT:00:01:90:C2:AE:77");
        } else {

            mEdtTarget.setText(_printer + "-" + _target);
        }


        bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Find bluetooth
                Intent intent = new Intent(getApplication(), DiscoveryActivity.class);
                startActivityForResult(intent, 0);

            }
        });
        btnSampleReceiptnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSingleThreadExecutor = mDriverManager.getSingleThreadExecutor();
                mDriverManager = DriverManager.getInstance();
                mPrinter1 = mDriverManager.getPrinter();

                if (BillingProfile.closeorderItemsArrayList.size() == 0 || BillingProfile.closeorderItemsArrayList == null) {
                        Toast.makeText(getApplicationContext(), "No Order placed ", Toast.LENGTH_SHORT).show();
                    } else {

                            try {
                                createReceiptDatapoona() ;
                    /*             Activity mActivity = MultiPrintView.this;
                                mActivity.runOnUiThread(new Runnable() {
                                    public void run() {
                                    //    new AsyncPrintingpoona().execute();
                                        createReceiptDatapoona() ;
                                    }
                                });*/

                            } catch (Throwable e) {
                                e.printStackTrace();
                                Toast.makeText(MultiPrintView.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                }
            }
        });
        print_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                print = printer_Name.getText().toString();
                if (!(print.equals("") || print.equals("-"))) {
                    if (BillingProfile.closeorderItemsArrayList.size() == 0 || BillingProfile.closeorderItemsArrayList == null) {
                        Toast.makeText(getApplicationContext(), "No Order placed ", Toast.LENGTH_SHORT).show();
                    } else {
                        if (print.equals("")) {
                            Toast.makeText(MultiPrintView.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                        } else {
                          /*  print_butt.setBackgroundColor(getResources().getColor(R.color.pink));
                            print_butt.setText("Connecting Printer...");
                            updateButtonState(false);
                            if (!runPrintReceiptSequence()) {
                                updateButtonState(true);
                            }*/
                            try {
                                print_butt.setEnabled(false);
                                Activity mActivity = MultiPrintView.this;
                                mActivity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        new AsyncPrinting().execute();
                                    }
                                });

                            } catch (Throwable e) {
                                e.printStackTrace();
                                Toast.makeText(MultiPrintView.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Connect Printer", Toast.LENGTH_SHORT).show();
                }

            }
        });
        page = b.getInt("pageValue");
        try {
            memName = b.getString("creditName");
            memAccNo = b.getString("creditAno");
            table = b.getString("tableNo");
            if (table.isEmpty()) {
                tableNo = 0;
            } else {
                tableNo = Integer.parseInt(table);
            }
            tableId = b.getInt("tableId");
            waiternamecode = b.getString("waiternamecode");
            _isdebitcard = b.getString("isdebitcard");
            cardType = b.getString("cardType");//CASH CARD
            signimg = b.getString("signimg");//CASH CARD

        } catch (Throwable e) {
            e.printStackTrace();
        }
        //       bitmap = (Bitmap) b.getParcelable("BitmapImage");
        if (cardType.equals("CASH CARD") && BillingProfile.isCashCardFilled.equalsIgnoreCase("false")) {
            a_bal.setText("0");

        } else if (cardType.equals("CASH CARD")) {
            a_bal.setText("-" + ava_balance);

        }

        if (page == 3) {
            billid = b.getString("bill_id");
        }


        if (page == 0) {
            actionBarSetup("Closed Order");
            //amt.setVisibility(View.VISIBLE);
            //taxamount.setVisibility(View.VISIBLE);
            //bNo.setVisibility(View.VISIBLE);
            //billNo.setVisibility(View.VISIBLE);
            if (BillingProfile.closeorderItemsArrayList.size() != 0) {
                _mName.setText(BillingProfile.closeorderItemsArrayList.get(0).mName);
                _mCode.setText(BillingProfile.closeorderItemsArrayList.get(0).mAcc);


                steward.setText("Waiter Name: " + waiternamecode);
                if (BillingProfile.closeorderItemsArrayList.get(0).billno.equals("") || BillingProfile.closeorderItemsArrayList.get(0).billno == null) {
                    if(waiternamecode.trim().contains("null") || waiternamecode==null){
                         steward.setText("Waiter Name: " + BillingProfile.closeorderItemsArrayList.get(0).waitername);
                    }
                    billNo.setText(BillingProfile.closeorderItemsArrayList.get(0).billno);
                } else {
                    billNo.setText(BillingProfile.closeorderItemsArrayList.get(0).billno.substring(4));
                }
            }
            cancelorder_butt.setVisibility(View.GONE);
            // removeButton.setVisibility(View.GONE);
            closeOrder();
        }

//        try {
//            //TODO remove commented
//            Log.setLogSettings(mContext, Log.PERIOD_TEMPORARY, Log.OUTPUT_STORAGE, null, 0, 1, Log.LOGLEVEL_LOW);
//        } catch (Exception e) {
//            ShowMsg.showException(e, "setLogSettings", mContext);
//        }


        try {
            String mstoreGST = BillingProfile.closeorderItemsArrayList.get(0).GSTNO;
            String gst = "", tin = "", cin = "";
            if (mstoreGST.contains("#")) {
                String[] separated = mstoreGST.split("#");
                gst = separated[0]; // this will contain "Fruit"
                tin = separated[1];
                cin = separated[2];
                SharedPreferences sharedPreferences = getSharedPreferences("GST", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("GST", gst);
                editor.putString("TIN", tin);
                editor.putString("CIN", cin);
                editor.apply();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        printer = findViewById(R.id.pr);
        printer_Name = findViewById(R.id.edtTarget1);
        bluetooth = findViewById(R.id.bluetooth);
        cancelorder_butt = findViewById(R.id.button_cancelOrder);
        print_butt = findViewById(R.id.button_cancelprint);
        btnSampleReceiptnew = (Button) findViewById(R.id.btnSampleReceiptnew);

        printer_Name.setText(_printer + "-" + _target);
        if (_printer.equalsIgnoreCase("") && _target.equalsIgnoreCase("")) {
//            printer_Name.setText("TM-P20_011044" + "-" + "BT:00:01:90:AE:93:17");
         //   printer_Name.setText("TM-P80_002585" + "-" + "BT:00:01:90:88:DA:48");
    //        printer_Name.setText("TM-P20_012348-BT:00:01:90:B9:26:DB");

//            printer_Name.setText("TM-P20_000452" + "-" + "BT:00:01:90:C2:AE:77");
        } else {

            printer_Name.setText(_printer + "-" + _target);
        }

        removeButton = findViewById(R.id.butt);
        print_butt = findViewById(R.id.button_cancelprint);

        a_bal = findViewById(R.id.cbalance_value);
        _mName = findViewById(R.id.mName);
        _mCode = findViewById(R.id.mCode);
        billNo = findViewById(R.id.billNo);
        bNo = findViewById(R.id.bNo);
        steward = findViewById(R.id.steward);
        tot_tax = findViewById(R.id.tamt);
        g_tot = findViewById(R.id.g_tot);
        // a_bal = (TextView) findViewById(R.id.a_bal);
        rate = findViewById(R.id.itmrate);
        taxamount = findViewById(R.id.tax_amts);
        amt = findViewById(R.id.itmAmt);
    }

    @Override
    public void onPtrReceive(Printer printer, int i, final PrinterStatusInfo printerStatusInfo, String s) {
        runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
//                ShowMsg.showResult(code, makeErrorMessage(status), mContext);

                dispPrinterWarnings(printerStatusInfo);

                updateButtonState(true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        disconnectPrinter();
                    }
                }).start();
            }
        });
    }

    public class AsyncPrinting extends AsyncTask<Void, Void, String> {

        String status = "";
        int printcode=2;

        @Override
        protected String doInBackground(Void... params) {

            try {
                printcode=    getPrinterCode(printertype);
         //  mPrinter = new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
      //     mPrinter = new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
            //    mPrinter = new Printer(printcode, 0, mContext);
                mPrinter = new Printer(5, 0, MultiPrintView.this);

                //       mPrinter = new Printer(Printer.TM_P80, Printer.MODEL_ANK, mContext);
                mPrinter.setReceiveEventListener(MultiPrintView.this);
            } catch (Throwable e) {
                e.printStackTrace();
            }
            try {
                if (page == 0)  // close order
                {

                    if (signimg != null &&  !signimg.equalsIgnoreCase("")) {
                       /* byte[] byteArray = getIntent().getByteArrayExtra("BitmapImage");
                        bitmap= BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
*/

                        Bundle extras = getIntent().getExtras();
                        Bitmap bmp = extras.getParcelable("Bitmap");
                        if (!createReceiptDataImg(bmp)) {
                            if (mPrinter == null) {
                                status = "false";
                            }
                            mPrinter.clearCommandBuffer();
                            mPrinter.setReceiveEventListener(null);
                            mPrinter = null;
                            status = "false";
                        } else {
                            status = "true";
                        }
                    } else {
                        if(printcode==5){
                            if (!createReceiptData_tmp80()) {
                                if (mPrinter == null) {
                                    status = "false";
                                }
                                mPrinter.clearCommandBuffer();
                                mPrinter.setReceiveEventListener(null);
                                mPrinter = null;
                                status = "false";
                            } else {
                                status = "true";
                            }
                        }
                        else {
                            if (!createReceiptData()) {
                                if (mPrinter == null) {
                                    status = "false";
                                }
                                mPrinter.clearCommandBuffer();
                                mPrinter.setReceiveEventListener(null);
                                mPrinter = null;
                                status = "false";
                            } else {
                                status = "true";
                            }
                        }


                    }
                }
                if (page == 11)  // close order
                {
                    if (!createReceiptData()) {
                        if (mPrinter == null) {
                            status = "false";
                        }
                        mPrinter.clearCommandBuffer();
                        mPrinter.setReceiveEventListener(null);
                        mPrinter = null;
                        status = "false";
                    } else {
                        status = "true";
                    }
                }
//                else if (page == 1) //cancel order (ot print )
//                {
//                    if (!createReceiptData1()) {
//                        if (mPrinter == null) {
//                            status = "false";
//                        }
//                        mPrinter.clearCommandBuffer();
//                        mPrinter.setReceiveEventListener(null);
//                        mPrinter = null;
//                        status = "false";
//                    } else {
//                        status = "true";
//                    }
//                } else if (page == 23)   //cancel order (ot print )
//
//                {
//                    if (!createReceiptData23()) {
//                        if (mPrinter == null) {
//                            status = "false";
//                        }
//                        mPrinter.clearCommandBuffer();
//                        mPrinter.setReceiveEventListener(null);
//                        mPrinter = null;
//                        status = "false";
//                    } else {
//                        status = "true";
//                    }
//                }
// else {
//                    if (!createReceiptData3()) {
//                        if (mPrinter == null) {
//                            status = "false";
//                        }
//                        mPrinter.clearCommandBuffer();
//                        mPrinter.setReceiveEventListener(null);
//                        mPrinter = null;
//                        status = "false";
//                    } else {
//                        status = "true";
//                    }
//                }


            } catch (final Throwable e) {
                e.printStackTrace();
            }

            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                pd = new ProgressDialog(MultiPrintView.this);
                pd.setMessage("Printing Please Wait .......");
                pd.setCancelable(false);
                pd.show();
            } catch (Throwable e) {
                e.printStackTrace();
                e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {
                Toast.makeText(MultiPrintView.this,"int printer code : "+printcode,Toast.LENGTH_LONG).show();

                if (status.equals("true")) {

                    try {

                        boolean print = printData();
                        if (!print) {
                            if (mPrinter == null) {
                                status = "false";
                            }
                            mPrinter.clearCommandBuffer();

                            mPrinter.setReceiveEventListener(null);

                            mPrinter = null;

                        } else {
                            count = count + 1;
                            status = "true";
                        }
                    } catch (Exception e) {
                        ShowMsg.showException(e, "Post execute error", mContext);
                    }
                    // updateButtonState(true);
                    Toast.makeText(MultiPrintView.this, "Printing Success", Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                } else if (status.equals("")) {
                    Toast.makeText(MultiPrintView.this, "Printing failed to enter", Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                } else {
                    Toast.makeText(MultiPrintView.this, "Printing failed to enter" + status, Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                }
            } catch (final Exception e) {
                MultiPrintView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        ShowMsg.showException(e, "other", mContext);
                    }
                });
            }
        }

    }
 public class AsyncPrintingpoona extends AsyncTask<Void, Void, String> {

        String status = "";
        int printcode=2;

        @Override
        protected String doInBackground(Void... params) {
            try {
                if (page == 0)  // close order
                {
                    if (signimg != null &&  !signimg.equalsIgnoreCase("")) {
                        Bundle extras = getIntent().getExtras();
                        Bitmap bmp = extras.getParcelable("Bitmap");
                        if (!createReceiptDataImg(bmp)) {
                            if (mPrinter == null) {
                                status = "false";
                            }
                            mPrinter.clearCommandBuffer();
                            mPrinter.setReceiveEventListener(null);
                            mPrinter = null;
                            status = "false";
                        } else {
                            status = "true";
                        }
                    } else {
                          createReceiptDatapoona() ;


                    }
                }
                if (page == 11)  // close order
                {
                    if (!createReceiptData()) {
                        if (mPrinter == null) {
                            status = "false";
                        }
                        mPrinter.clearCommandBuffer();
                        mPrinter.setReceiveEventListener(null);
                        mPrinter = null;
                        status = "false";
                    } else {
                        status = "true";
                    }
                }
//                else if (page == 1) //cancel order (ot print )
//                {
//                    if (!createReceiptData1()) {
//                        if (mPrinter == null) {
//                            status = "false";
//                        }
//                        mPrinter.clearCommandBuffer();
//                        mPrinter.setReceiveEventListener(null);
//                        mPrinter = null;
//                        status = "false";
//                    } else {
//                        status = "true";
//                    }
//                } else if (page == 23)   //cancel order (ot print )
//
//                {
//                    if (!createReceiptData23()) {
//                        if (mPrinter == null) {
//                            status = "false";
//                        }
//                        mPrinter.clearCommandBuffer();
//                        mPrinter.setReceiveEventListener(null);
//                        mPrinter = null;
//                        status = "false";
//                    } else {
//                        status = "true";
//                    }
//                }
// else {
//                    if (!createReceiptData3()) {
//                        if (mPrinter == null) {
//                            status = "false";
//                        }
//                        mPrinter.clearCommandBuffer();
//                        mPrinter.setReceiveEventListener(null);
//                        mPrinter = null;
//                        status = "false";
//                    } else {
//                        status = "true";
//                    }
//                }


            } catch (final Throwable e) {
                e.printStackTrace();
            }

            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
            /*    pd = new ProgressDialog(MultiPrintView.this);
                pd.setMessage("Printing Please Wait .......");
                pd.setCancelable(false);
                pd.show();*/
            } catch (Throwable e) {
                e.printStackTrace();
                e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {
                Toast.makeText(MultiPrintView.this,"int printer code : "+printcode,Toast.LENGTH_LONG).show();

                if (status.equals("true")) {

                    try {

                        boolean print = printData();
                        if (!print) {
                            if (mPrinter == null) {
                                status = "false";
                            }
                            mPrinter.clearCommandBuffer();

                            mPrinter.setReceiveEventListener(null);

                            mPrinter = null;

                        } else {
                            count = count + 1;
                            status = "true";
                        }
                    } catch (Exception e) {
                        ShowMsg.showException(e, "Post execute error", mContext);
                    }
                    // updateButtonState(true);
                    Toast.makeText(MultiPrintView.this, "Printing Success", Toast.LENGTH_SHORT).show();
                   // pd.dismiss();
                } else if (status.equals("")) {
                    Toast.makeText(MultiPrintView.this, "Printing failed to enter", Toast.LENGTH_SHORT).show();
                  //  pd.dismiss();
                } else {
                    Toast.makeText(MultiPrintView.this, "Printing failed to enter" + status, Toast.LENGTH_SHORT).show();
                  //  pd.dismiss();
                }
            } catch (final Exception e) {
                MultiPrintView.this.runOnUiThread(new Runnable() {
                    public void run() {
                        ShowMsg.showException(e, "other", mContext);
                    }
                });
            }
        }

    }

    public void closeOrder() {
        String ItemCategoryid = "";
        accharges = 0;
        servicetax = 0;
        vat = 0;
        billAmt = 0;
        discountAmt = 0;
        GrantTotal = 0;
        if (page == 0) {
            _closeorderItemsArrayList = BillingProfile.closeorderItemsArrayList;
        }
        taxlist = new ArrayList<>();

        for (int i = 0; i < _closeorderItemsArrayList.size(); i++) {
            if (i == _closeorderItemsArrayList.size() - 1) {
                addTax(ItemCategoryid, i);
            } else {
                ava_balance = _closeorderItemsArrayList.get(i).getAva_balance();
                a_bal.setText(ava_balance);
//ac charges
                if (!_closeorderItemsArrayList.get(0).getACCharge().equals("0")) {
                    taxlist.add("AC Charge" + "-" + format.format(Double.parseDouble(_closeorderItemsArrayList.get(0).getACCharge())));
                }

                String current_ItemCategoryid = _closeorderItemsArrayList.get(i).getItemCategoryID();
                if (customList == null) {
                    //customList = (LinearLayout) findViewById(R.id.closeItemView);
                    customList = findViewById(R.id.multi_printview);
                    customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                    itemName = customView.findViewById(R.id.itmName);
                    qty = customView.findViewById(R.id.itmQty);
                    amount = customView.findViewById(R.id.itmAmt);
                    itemCount = customView.findViewById(R.id.itmrate);
                    itmDis = customView.findViewById(R.id.itmDis);

                    itemName.setText(_closeorderItemsArrayList.get(i).itemname);
                    qty.setText(_closeorderItemsArrayList.get(i).Quantity);
                    amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).Amount)));
                    itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));
                    itmDis.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).DiscountAmt)));
                    ItemCategoryid = _closeorderItemsArrayList.get(i).getItemCategoryID();
                    billAmt = billAmt + (Double.parseDouble(_closeorderItemsArrayList.get(i).getQuantity()) * Double.parseDouble(_closeorderItemsArrayList.get(i).getRate()));
                    discountAmt = discountAmt + (Double.parseDouble(_closeorderItemsArrayList.get(i).DiscountAmt));
                } else if (current_ItemCategoryid.equals(ItemCategoryid)) {
                    customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                    itemName = customView.findViewById(R.id.itmName);
                    qty = customView.findViewById(R.id.itmQty);
                    amount = customView.findViewById(R.id.itmAmt);
                    itemCount = customView.findViewById(R.id.itmrate);
                    itmDis = customView.findViewById(R.id.itmDis);

                    itemName.setText(_closeorderItemsArrayList.get(i).itemname);
                    qty.setText(_closeorderItemsArrayList.get(i).Quantity);
                    amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).Amount)));
                    itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));
                    itmDis.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).DiscountAmt)));
                    discountAmt = discountAmt + (Double.parseDouble(_closeorderItemsArrayList.get(i).DiscountAmt));
                    billAmt = billAmt + (Double.parseDouble(_closeorderItemsArrayList.get(i).getQuantity()) * Double.parseDouble(_closeorderItemsArrayList.get(i).getRate()));
                } else {
                    addTax(ItemCategoryid, i);
                    customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                    itemName = customView.findViewById(R.id.itmName);
                    qty = customView.findViewById(R.id.itmQty);
                    amount = customView.findViewById(R.id.itmAmt);
                    itemCount = customView.findViewById(R.id.itmrate);
                    itmDis = customView.findViewById(R.id.itmDis);
                    discountAmt = discountAmt + (Double.parseDouble(_closeorderItemsArrayList.get(i).DiscountAmt));
                    itemName.setText(_closeorderItemsArrayList.get(i).itemname);
                    itmDis.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).DiscountAmt)));
                    qty.setText(_closeorderItemsArrayList.get(i).Quantity);
                    amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).Amount)));
                    itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));
                    ItemCategoryid = _closeorderItemsArrayList.get(i).getItemCategoryID();
                    billAmt = billAmt + (Double.parseDouble(_closeorderItemsArrayList.get(i).getQuantity()) * Double.parseDouble(_closeorderItemsArrayList.get(i).getRate()));
                }
                customList.addView(customView);

            }

        }
//monica
        //GrantTotal=servicetax+vat+billAmt+cessTax+salesTax;
//        GrantTotal = servicetax + billAmt + Double.parseDouble(_closeorderItemsArrayList.get(0).getACCharge());
//        tot_tax.setText(format.format(billAmt));
//        g_tot.setText(format.format(GrantTotal));
//        a_bal.setText(ava_balance);
//        for (int k = 0; k < taxlist.size(); k++)
//        {
//            if (customTaxList == null)
//            {
//                customTaxList = (LinearLayout) findViewById(R.id.taxView);
//                customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);
//
//                taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
//                taxValue = (TextView) customTaxView.findViewById(R.id.tax);
//
//                String dummy[] = taxlist.get(k).split("-");
//                taxHeader.setText(dummy[0] + " :");
//                taxValue.setText(dummy[1]);
//
//
//            } else {
//                customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);
//
//                taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
//                taxValue = (TextView) customTaxView.findViewById(R.id.tax);
//
//                String dummy[] = taxlist.get(k).split("-");
//                taxHeader.setText(dummy[0]);
//                taxValue.setText(dummy[1]);
//            }
//            customTaxList.addView(customTaxView);
//
//        }


    }

    private void addTax(String itemcategoryid, int iteration) {
        //total
        customView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);
        taxHeader = customView.findViewById(R.id.h_tax);
        taxValue = customView.findViewById(R.id.tax);
        taxHeader.setText("Total Amount");
        taxValue.setText(  format.format(billAmt));
        customList.addView(customView);
        billnumer = _closeorderItemsArrayList.get(iteration - 1).getBillDetNumber();
        //billNo.setText(billnumer);
        //tax
        int t = _closeorderItemsArrayList.size() - 1;
        if (!_closeorderItemsArrayList.get(t).getTaxAmount().equals("")) {
            try {
                if (_closeorderItemsArrayList.get(t).getTaxAmount().contains("-")) {
                    String[] tax = _closeorderItemsArrayList.get(t).getTaxAmount().split("-");
                    for (int j = 0; j < tax.length; j++) {
                        String dummy[] = tax[j].split(",");
                        if (dummy[1].equals("Notax")) {
                            taxlist.add("Tax" + "-" + format.format(Double.parseDouble(dummy[0])));
                        } else if (dummy[1].equalsIgnoreCase("Tax")) {
                            taxlist.add("Tax" + "-" + format.format(Double.parseDouble(dummy[0])));
                            customView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);
                            taxHeader = customView.findViewById(R.id.h_tax);
                            taxValue = customView.findViewById(R.id.tax);
                            taxHeader.setText(dummy[1]);
                            taxValue.setText(dummy[0]);
                            customList.addView(customView);
                            servicetax = servicetax + Double.parseDouble(dummy[0]);
                            _service = format.format(Double.parseDouble(dummy[0]));
                            GrantTotal = billAmt + Double.parseDouble(dummy[0]);
                        } else if (itemcategoryid.equals(dummy[2])) {
                            taxlist.add(dummy[1] + "-" + format.format(Double.parseDouble(dummy[0])));
                            customView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);
                            taxHeader = customView.findViewById(R.id.h_tax);
                            taxValue = customView.findViewById(R.id.tax);
                            taxHeader.setText(dummy[1]);
                            taxValue.setText(dummy[0]);
                            customList.addView(customView);
                            servicetax = servicetax + Double.parseDouble(dummy[0]);
                            _service = format.format(Double.parseDouble(dummy[0]));
                            GrantTotal = billAmt + Double.parseDouble(dummy[0]);
                        }
                        // add tax desription
                    }

                }
            } catch (Exception e) {
                String r = e.getMessage();
            }
        }
        GrantTotal = billAmt + servicetax - discountAmt;
        servicetax = 0;
        if (discountAmt > 0) {
            customView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);
            taxHeader = customView.findViewById(R.id.h_tax);
            taxValue = customView.findViewById(R.id.tax);
            taxHeader.setText("Discount Amount");
            taxValue.setText(format.format(discountAmt) + "");
            customList.addView(customView);
            discountAmt = 0;
        }

        customView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);
        taxHeader = customView.findViewById(R.id.h_tax);
        taxValue = customView.findViewById(R.id.tax);
        taxHeader.setText("Grand Total");
        taxValue.setText(format.format(GrantTotal) + "");
        customList.addView(customView);
        billAmt = 0;
        discountAmt = 0;
        customView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);
        taxHeader = customView.findViewById(R.id.h_tax);
        taxValue = customView.findViewById(R.id.tax);
        taxHeader.setText("BillNumber");
        taxValue.setText(billnumer);
        customList.addView(customView);
        GrantTotal = 0;
    }


    public boolean createReceiptDataImg(Bitmap bitmap) {
        String itemcategoryid = "";
        ArrayList<CloseorderItems> _closeorderbillList = new ArrayList<CloseorderItems>();
        ArrayList<CloseorderItems> taxlist = new ArrayList<CloseorderItems>();
        String opBalance = "";
        servicetax = 0;
        salesTax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        discountAmt = 0;
        cessTax = 0;
        tax = 0;
        String AB = "0";
        if (mPrinter == null) {
            return false;
        } else {
            Dbase dbase = new Dbase(MultiPrintView.this);
            list_bill = dbase.getBills();
            for (int m = 0; m < list_bill.size(); m++) {
                billAmt = 0;
                GrantTotal = 0;
                discountAmt = 0;
                tax = 0;
                itemcategoryid = list_bill.get(m);
                taxlist.clear();
                _closeorderbillList.clear();
                _closeorderbillList = dbase.getBillDetails(itemcategoryid);
                for (int item = 0; item < _closeorderbillList.size(); item++) {
                    if (item == 0) {
                        printHeader(item, _closeorderbillList);
                        AB = _closeorderbillList.get(item).getAva_balance();
                    }
                    printContent(item, _closeorderbillList);
                }
                taxlist = dbase.getBilltax(itemcategoryid);

                bitmap = getResizedBitmap(bitmap, 350);
                printFoooterimg(0, taxlist, AB, bitmap);

            }
            textData = null;
        }

        return true;
    }

    public boolean createReceiptData() {
        String itemcategoryid = "";
        ArrayList<CloseorderItems> _closeorderbillList = new ArrayList<CloseorderItems>();
        ArrayList<CloseorderItems> taxlist = new ArrayList<CloseorderItems>();
        String opBalance = "";
        servicetax = 0;
        salesTax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        discountAmt = 0;
        cessTax = 0;
        tax = 0;
        String AB = "0";
        if (mPrinter == null) {
            return false;
        } else {
            Dbase dbase = new Dbase(MultiPrintView.this);
            list_bill = dbase.getBills();
            for (int m = 0; m < list_bill.size(); m++) {
                billAmt = 0;
                GrantTotal = 0;
                discountAmt = 0;
                tax = 0;
                itemcategoryid = list_bill.get(m);
                taxlist.clear();
                _closeorderbillList.clear();
                _closeorderbillList = dbase.getBillDetails(itemcategoryid);
                printHeader(0, _closeorderbillList);
                AB = _closeorderbillList.get(0).getAva_balance();
                //monica
                for (int item = 0; item < _closeorderbillList.size(); item++) {
                    if (item == 0) {

                    }
                    printContent(item, _closeorderbillList);
                }
                //monica

                taxlist = dbase.getBilltax(itemcategoryid);
                printFoooter(0, taxlist, AB);

            }
            textData = null;
        }

        return true;
    }
  public boolean createReceiptDatapoona() {
        String itemcategoryid = "";
        ArrayList<CloseorderItems> _closeorderbillList = new ArrayList<CloseorderItems>();
        ArrayList<CloseorderItems> taxlist = new ArrayList<CloseorderItems>();
        String opBalance = "";
        servicetax = 0;
        salesTax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        discountAmt = 0;
        cessTax = 0;
        tax = 0;
        String AB = "0";

            Dbase dbase = new Dbase(MultiPrintView.this);
            list_bill = dbase.getBills();
            for (int m = 0; m < list_bill.size(); m++) {
                billAmt = 0;
                GrantTotal = 0;
                discountAmt = 0;
                tax = 0;
                itemcategoryid = list_bill.get(m);
                taxlist.clear();
                _closeorderbillList.clear();
                _closeorderbillList = dbase.getBillDetails(itemcategoryid);
             printHeaderpoona(0, _closeorderbillList);
                AB = _closeorderbillList.get(0).getAva_balance();
                //monica
                for (int item = 0; item < _closeorderbillList.size(); item++) {
                    if (item == 0) {

                    }
                 printContentpoona(item, _closeorderbillList);
                }
                //monica

                taxlist = dbase.getBilltax(itemcategoryid);
           printFoooterpoona(0, taxlist, AB);

            }
            textData = null;


        return true;
    }
  public boolean createReceiptData_tmp80() {
        String itemcategoryid = "";
        ArrayList<CloseorderItems> _closeorderbillList = new ArrayList<CloseorderItems>();
        ArrayList<CloseorderItems> taxlist = new ArrayList<CloseorderItems>();
        String opBalance = "";
        servicetax = 0;
        salesTax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        discountAmt = 0;
        cessTax = 0;
        tax = 0;
        String AB = "0";
        if (mPrinter == null) {
            return false;
        } else {
            Dbase dbase = new Dbase(MultiPrintView.this);
            list_bill = dbase.getBills();
            for (int m = 0; m < list_bill.size(); m++) {
                billAmt = 0;
                GrantTotal = 0;
                discountAmt = 0;
                tax = 0;
                itemcategoryid = list_bill.get(m);
                taxlist.clear();
                _closeorderbillList.clear();
                _closeorderbillList = dbase.getBillDetails(itemcategoryid);
                printHeadertmp80(0, _closeorderbillList);
                AB = _closeorderbillList.get(0).getAva_balance();
                //monica
                for (int item = 0; item < _closeorderbillList.size(); item++) {
                    if (item == 0) {

                    }
                    printContenttmp80(item, _closeorderbillList);
                }
                //monica

                taxlist = dbase.getBilltax(itemcategoryid);
                printFoootertmp80(0, taxlist, AB);

            }
            textData = null;
        }

        return true;
    }
    private void printContent(int m, ArrayList<CloseorderItems> _closeorderbillList) {
        try {

            if (billType.equals("BOT")) {
                textData.append(
                        String.format("%18s", (_closeorderbillList.get(m).itemname + "                                                ").substring(0, 20))
                                // +" "
                                + String.format("%4s", (_closeorderbillList.get(m).Quantity))

                                + String.format("%8s", format.format
                                (Double.parseDouble((_closeorderbillList.get(m).rate))))
                                + String.format("%10s", format.format(Double.parseDouble((_closeorderbillList.get(m).Amount))))
                                + "\n");
                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
                billAmt = billAmt + Double.parseDouble(_closeorderbillList.get(m).getAmount());
                discountAmt = discountAmt + Double.parseDouble(_closeorderbillList.get(m).getDiscountAmt());

            } else {
                textData.append(
                        String.format("%18s", (_closeorderbillList.get(m).itemname + "                                                ").substring(0, 20))
                                // +" "
                                + String.format("%4s", (_closeorderbillList.get(m).Quantity))

                                + String.format("%8s", format.format
                                (Double.parseDouble((_closeorderbillList.get(m).rate))))
                                // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                + String.format("%10s", format.format(Double.parseDouble((_closeorderbillList.get(m).Amount))))
                                // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                + "\n");


                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
                billAmt = billAmt + Double.parseDouble(_closeorderbillList.get(m).getAmount());
                discountAmt = discountAmt + Double.parseDouble(_closeorderbillList.get(m).getDiscountAmt());
            }
            servicetax = servicetax + Double.parseDouble(_service);
            vat = vat + Double.parseDouble(_vat);
            salesTax = salesTax + Double.parseDouble(_sales);
        } catch (Exception e) {

        }
    }

    private void printContentpoona(final int m, final ArrayList<CloseorderItems> _closeorderbillList) {
        try {
            mSingleThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
            int printStatus = mPrinter1.getPrinterStatus();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                MultiPrintView.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MultiPrintView.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                    }
                });
            } else {
            if (billType.equals("BOT")) {
                PrnStrFormat format1 = new PrnStrFormat();
                format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                format1.setStyle(PrnTextStyle.NORMAL);
                format1.setTextSize(23);
                if(_closeorderbillList.get(m).itemname.length()>20){
                    String itemstr1=_closeorderbillList.get(m).itemname.substring(0,20);
                    String itemstr2="..."+_closeorderbillList.get(m).itemname.substring(20,_closeorderbillList.get(m).itemname.length());
                    String itemline =
                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                            String.format("%15s", (itemstr1 + "                                                ").substring(0, 20))

                                    + String.format("%4s", (_closeorderbillList.get(m).getQuantity()))

                                    + String.format("%8s", format.format
                                    //     (Double.parseDouble((_closeorderbillList.get(m).getTRate())))) + String.format("%5s", format.format(Double.parseDouble((_closeorderbillList.get(m).getAmount()))))
                                            (Double.parseDouble((_closeorderbillList.get(m).getAmount()))))

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline, format1);

                    String itemline1 =
                            String.format("%15s", (itemstr2 + "                                                ").substring(0, 20))

                                    + String.format("%4s", (""))

                                    + String.format("%8s","")

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline1, format1);
                }
                else{
                    String itemname= (_closeorderbillList.get(m).getItemname() +  "**************************************").substring(0,20);
                    itemname=itemname.replace("*","  ");

                    String itemline =
                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                            itemname
                                    + String.format("%4s", (_closeorderbillList.get(m).getQuantity()))

                                    + String.format("%8s", format.format
                                    //     (Double.parseDouble((_closeorderbillList.get(m).getTRate())))) + String.format("%5s", format.format(Double.parseDouble((_closeorderbillList.get(m).getAmount()))))
                                            (Double.parseDouble((_closeorderbillList.get(m).getAmount()))))

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline, format1);

                }


                billAmt = billAmt + Double.parseDouble(_closeorderbillList.get(m).getAmount());
                discountAmt = discountAmt + Double.parseDouble(_closeorderbillList.get(m).getDiscountAmt());

            } else {
                PrnStrFormat format1 = new PrnStrFormat();
                format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                format1.setStyle(PrnTextStyle.NORMAL);
                format1.setTextSize(23);
                if(_closeorderbillList.get(m).itemname.length()>20){
                    String itemstr1=_closeorderbillList.get(m).itemname.substring(0,20);
                    String itemstr2="..."+_closeorderbillList.get(m).itemname.substring(20,_closeorderbillList.get(m).itemname.length());
                    String itemline =
                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                            String.format("%15s", (itemstr1 + "                                                ").substring(0, 20))

                                    + String.format("%4s", (_closeorderbillList.get(m).getQuantity()))

                                    + String.format("%8s", format.format
                                    //     (Double.parseDouble((_closeorderbillList.get(m).getTRate())))) + String.format("%5s", format.format(Double.parseDouble((_closeorderbillList.get(m).getAmount()))))
                                            (Double.parseDouble((_closeorderbillList.get(m).getAmount()))))

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline, format1);

                    String itemline1 =
                            String.format("%15s", (itemstr2 + "                                                ").substring(0, 20))

                                    + String.format("%4s", (""))

                                    + String.format("%8s","")

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline1, format1);
                }
                else{
                    String itemname= (_closeorderbillList.get(m).getItemname() +  "**************************************").substring(0,20);
                    itemname=itemname.replace("*","  ");


                    String itemline =
                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                             itemname

                                    + String.format("%4s", (_closeorderbillList.get(m).getQuantity()))

                                    + String.format("%8s", format.format
                                    //     (Double.parseDouble((_closeorderbillList.get(m).getTRate())))) + String.format("%5s", format.format(Double.parseDouble((_closeorderbillList.get(m).getAmount()))))
                                            (Double.parseDouble((_closeorderbillList.get(m).getAmount()))))

                                    + "\n";
                    mPrinter1.setPrintAppendString(itemline, format1);

                }


                billAmt = billAmt + Double.parseDouble(_closeorderbillList.get(m).getAmount());
                discountAmt = discountAmt + Double.parseDouble(_closeorderbillList.get(m).getDiscountAmt());
            }
            servicetax = servicetax + Double.parseDouble(_service);
            vat = vat + Double.parseDouble(_vat);
            salesTax = salesTax + Double.parseDouble(_sales);
        }

            printStatus = mPrinter1.setPrintStart();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                MultiPrintView.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MultiPrintView.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                    }
                });


            }    }
            });}catch (Exception e) {

        }
    }
  private void printContenttmp80(int m, ArrayList<CloseorderItems> _closeorderbillList) {
        try {

            if (billType.equals("BOT")) {
                textData.append(
                        String.format("%21s", (_closeorderbillList.get(m).itemname + "                                                ").substring(0, 20))
                                // +" "
                                + String.format("%7s", (_closeorderbillList.get(m).Quantity))

                                + String.format("%11s", format.format
                                (Double.parseDouble((_closeorderbillList.get(m).rate))))
                                + String.format("%13s", format.format(Double.parseDouble((_closeorderbillList.get(m).Amount))))
                                + "\n");
                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());



                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_D);
                mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);


                billAmt = billAmt + Double.parseDouble(_closeorderbillList.get(m).getAmount());
                discountAmt = discountAmt + Double.parseDouble(_closeorderbillList.get(m).getDiscountAmt());

            } else {
                textData.append(
                        String.format("%21s", (_closeorderbillList.get(m).itemname + "                                                ").substring(0, 20))
                                // +" "
                                + String.format("%7s", (_closeorderbillList.get(m).Quantity))

                                + String.format("%11s", format.format
                                (Double.parseDouble((_closeorderbillList.get(m).rate))))
                                // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                + String.format("%13s", format.format(Double.parseDouble((_closeorderbillList.get(m).Amount))))
                                // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                + "\n");


                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_D);
                mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);

                billAmt = billAmt + Double.parseDouble(_closeorderbillList.get(m).getAmount());
                discountAmt = discountAmt + Double.parseDouble(_closeorderbillList.get(m).getDiscountAmt());
            }
            servicetax = servicetax + Double.parseDouble(_service);
            vat = vat + Double.parseDouble(_vat);
            salesTax = salesTax + Double.parseDouble(_sales);
        } catch (Exception e) {

        }
    }

   /* private void printFoooter(int m, ArrayList<CloseorderItems> taxlist, String ava_balance) {
        try {
            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            if (taxlist.get(0).cardType.equals("CLUB CARD")) {
                GrantTotal = 0 + 0 + billAmt;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append(String.format("%20s", "Bill Amount" + " :  " + format.format(billAmt) + "\n"));


                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                //   GrantTotal=servicetax+vat+billAmt+cessTax;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");
                for (int t = 0; t < taxlist.size(); t++) {
                    tax = tax + Double.parseDouble(taxlist.get(t).getAmount());
                    //textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                    textData.append(String.format("%20s", (taxlist.get(t).getTaxDescription() + " : " + format.format(Double.parseDouble(taxlist.get(t).getAmount()))) + "\n"));
                }

            }


            if (discountAmt > 0) {
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");
                textData.append("Discount Amount" + " :  " + String.format("%.2f", discountAmt) + "\n\n");
            }


            //}
            GrantTotal = billAmt + tax-discountAmt;
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");
            //textData.append("GRAND TOTAL" + " :  " + String.format("%.2f",format.format(GrantTotal+"") ) + "\n\n");
            textData.append("GRAND TOTAL" + " :  " + String.format("%.2f",GrantTotal) + "\n\n");

            if (cardType.equals("CASH CARD")) {
                textData.append("AVAIL.BAL  " + " :  " + ava_balance + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                textData.append("AVAIL.BAL  " + " :  " + ava_balance + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            textData.append("\n");
            textData.append("\n");
            textData.append("(Signature)" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
            textData.append("* Thank you ! We Wish To Serve You Again ");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);


        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            //return false;
        }
    }*/

    private void printFoooter(int m, ArrayList<CloseorderItems> taxlist, String ava_balance) {
        try {
            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            if (taxlist.get(0).cardType.equals("CLUB CARD")) {
                GrantTotal = 0 + 0 + billAmt;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append(String.format("%20s", "Bill Amount" + " :  " + format.format(billAmt) + "\n"));


                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                //   GrantTotal=servicetax+vat+billAmt+cessTax;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");
                for (int t = 0; t < taxlist.size(); t++) {

                    if(isDouble(taxlist.get(t).getAmount() )){
                        tax = tax + Double.parseDouble(taxlist.get(t).getAmount());
                        //textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                        textData.append(String.format("%20s", (taxlist.get(t).getTaxDescription() + " : " + format.format(Double.parseDouble(taxlist.get(t).getAmount()))) + "\n"));

                    }
                      }
            }

            //}
            GrantTotal = billAmt + tax - discountAmt;
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");


            if (discountAmt > 0) {
                //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");
                textData.append("Discount Amount" + " :  " + String.format("%.2f", discountAmt) + "\n\n");
            }
            textData.append("GRAND TOTAL" + " :  " + String.format("%.2f", GrantTotal) + "\n\n");

            if (cardType.equals("CASH CARD")) {
                textData.append("AVAIL.BAL  " + " :  " + ava_balance + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                if (_isdebitcard.equalsIgnoreCase("debit")) {
                    textData.append("DEBIT CARD.BAL  " + " :  " + ava_balance + "\n");
                } else {
                    //     textData.append("AVAIL.BAL  " + " :  " + ava_balance + "\n");
                    if (ava_balance.contains("-")) {
                        textData.append("BALANCE  " + " :  " + ava_balance + " Cr" + "\n");

                    } else {
                        textData.append("BALANCE  " + " :  " + ava_balance + " Dr" + "\n");

                    }
                }
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            textData.append("\n");
            textData.append("\n");
            textData.append("(Signature)" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
            textData.append("* Thank you ! We Wish To Serve You Again ");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);


        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            //return false;
        }
    }
    private void printFoooterpoona(int m, final ArrayList<CloseorderItems> taxlist, final String ava_balance) {
        try {
            mSingleThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
            int printStatus = mPrinter1.getPrinterStatus();
                if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                    MultiPrintView.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MultiPrintView.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                        }
                    });
                } else {
                    PrnStrFormat format1 = new PrnStrFormat();

                    format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                    format1.setStyle(PrnTextStyle.NORMAL);
                    format1.setTextSize(25);
                    mPrinter1.setPrintAppendString("------------------------------------------------------", format1);


                    if (taxlist.get(0).cardType.equals("CLUB CARD")) {
                        GrantTotal = 0 + 0 + billAmt;

                        mPrinter1.setPrintAppendString("Bill Amount" + " :  " + format.format(billAmt), format1);


                    } else {
                        //   GrantTotal=servicetax+vat+billAmt+cessTax;
                    //     textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");
                        mPrinter1.setPrintAppendString("Bill Amount" + " :  " + format.format(billAmt), format1);


                        for (int t = 0; t < taxlist.size(); t++) {

                            if (isDouble(taxlist.get(t).getAmount())) {
                                tax = tax + Double.parseDouble(taxlist.get(t).getAmount());
                                //textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                              //  textData.append(String.format("%20s", (taxlist.get(t).getTaxDescription() + " : " + format.format(Double.parseDouble(taxlist.get(t).getAmount()))) + "\n"));
                                mPrinter1.setPrintAppendString(String.format("%20s", (taxlist.get(t).getTaxDescription() + " : " + format.format(Double.parseDouble(taxlist.get(t).getAmount()))) ), format1);

                            }
                        }
                    }

                    //}
                    GrantTotal = billAmt + tax - discountAmt;
                 //   mPrinter.addText(textData.toString());
                  //  textData.delete(0, textData.length());


                  //  method = "addTextSize";
                  //  mPrinter.addTextSize(1, 2);
                  //  mPrinter.addTextFont(Printer.FONT_B);
                  //  mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                  //  mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                    //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");


                    if (discountAmt > 0) {
                        //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");
                     //   textData.append("Discount Amount" + " :  " + String.format("%.2f", discountAmt) + "\n\n");
                        mPrinter1.setPrintAppendString("Discount Amount" + " :  " + String.format("%.2f", discountAmt), format1);


                    }
                    format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                    format1.setStyle(PrnTextStyle.NORMAL);
                    format1.setTextSize(30);
                 //   textData.append("GRAND TOTAL" + " :  " + String.format("%.2f", GrantTotal) + "\n\n");
                    mPrinter1.setPrintAppendString("GRAND TOTAL" + " :  " + String.format("%.2f", GrantTotal), format1);

                    format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                    format1.setStyle(PrnTextStyle.NORMAL);
                    format1.setTextSize(25);
                    if (cardType.equals("CASH CARD")) {
                     //   textData.append("AVAIL.BAL  " + " :  " + ava_balance + "\n");
                        mPrinter1.setPrintAppendString("AVAIL.BAL  " + " :  " + ava_balance, format1);


                    } else {
                        if (_isdebitcard.equalsIgnoreCase("debit")) {
                         //   textData.append("DEBIT CARD.BAL  " + " :  " + ava_balance + "\n");
                            mPrinter1.setPrintAppendString("DEBIT CARD.BAL  " + " :  " + ava_balance, format1);

                        } else {
                            //     textData.append("AVAIL.BAL  " + " :  " + ava_balance + "\n");
                            if (ava_balance.contains("-")) {
                             //   textData.append("BALANCE  " + " :  " + ava_balance + " Cr" + "\n");
                                mPrinter1.setPrintAppendString("BALANCE  " + " :  " + ava_balance + " Cr" , format1);

                            } else {
                           //     textData.append("BALANCE  " + " :  " + ava_balance + " Dr" + "\n");
                                mPrinter1.setPrintAppendString("BALANCE  " + " :  " + ava_balance + " Dr" , format1);

                            }
                        }
                             }
                    mPrinter1.setPrintAppendString("------------------------------------------------------", format1);
                    mPrinter1.setPrintAppendString(" ", format1);
                    mPrinter1.setPrintAppendString(" ", format1);
                    mPrinter1.setPrintAppendString("(Signature)" + "\n", format1);
                    mPrinter1.setPrintAppendString("* Thank you ! We Wish To Serve You Again ", format1);
                   // mPrinter.addText(textData.toString());

                    mPrinter1.setPrintAppendString(" ", format1);
                    mPrinter1.setPrintAppendString(" ", format1);
                    mPrinter1.setPrintAppendString(" ", format1);
                    mPrinter1.setPrintAppendString(" ", format1);
                }

            printStatus = mPrinter1.setPrintStart();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                MultiPrintView.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MultiPrintView.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                    }
                });
            }
                }
            });  } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            //return false;
        }
    }
    private void printFoootertmp80(int m, ArrayList<CloseorderItems> taxlist, String ava_balance) {
        try {
         //   textData.append("------------------------------------------\n");
            textData.append("--------------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            if (taxlist.get(0).cardType.equals("CLUB CARD")) {
                GrantTotal = 0 + 0 + billAmt;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append(String.format("%20s", "Bill Amount" + " :  " + format.format(billAmt) + "\n"));


                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                //   GrantTotal=servicetax+vat+billAmt+cessTax;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");
                for (int t = 0; t < taxlist.size(); t++) {

                    if(isDouble(taxlist.get(t).getAmount() )){
                        tax = tax + Double.parseDouble(taxlist.get(t).getAmount());
                        //textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                        textData.append(String.format("%20s", (taxlist.get(t).getTaxDescription() + " : " + format.format(Double.parseDouble(taxlist.get(t).getAmount()))) + "\n"));

                    }
                      }
            }

            //}
            GrantTotal = billAmt + tax - discountAmt;
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");


            if (discountAmt > 0) {
                //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");
                textData.append("Discount Amount" + " :  " + String.format("%.2f", discountAmt) + "\n\n");
            }
            textData.append("GRAND TOTAL" + " :  " + String.format("%.2f", GrantTotal) + "\n\n");

            if (cardType.equals("CASH CARD")) {
                textData.append("AVAIL.BAL  " + " :  " + ava_balance + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                if (_isdebitcard.equalsIgnoreCase("debit")) {
                    textData.append("DEBIT CARD.BAL  " + " :  " + ava_balance + "\n");
                } else {
                    //     textData.append("AVAIL.BAL  " + " :  " + ava_balance + "\n");
                    if (ava_balance.contains("-")) {
                        textData.append("BALANCE  " + " :  " + ava_balance + " Cr" + "\n");

                    } else {
                        textData.append("BALANCE  " + " :  " + ava_balance + " Dr" + "\n");

                    }
                }
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("--------------------------------------------------\n");
            textData.append("\n");
            textData.append("\n");
            textData.append("(Signature)" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
            textData.append("  ");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);


        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            //return false;
        }
    }

    private void printFoooterimg(int m, ArrayList<CloseorderItems> taxlist, String ava_balance, Bitmap bitmap) {
        try {
            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            if (taxlist.get(0).cardType.equals("CLUB CARD")) {
                GrantTotal = 0 + 0 + billAmt;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append(String.format("%20s", "Bill Amount" + " :  " + format.format(billAmt) + "\n"));


                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                //   GrantTotal=servicetax+vat+billAmt+cessTax;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");
                for (int t = 0; t < taxlist.size(); t++) {
                    tax = tax + Double.parseDouble(taxlist.get(t).getAmount());
                    //textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                    textData.append(String.format("%20s", (taxlist.get(t).getTaxDescription() + " : " + format.format(Double.parseDouble(taxlist.get(t).getAmount()))) + "\n"));
                }

            }

            //}
            GrantTotal = billAmt + tax - discountAmt;
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");


            if (discountAmt > 0) {
                //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");
                textData.append("Discount Amount" + " :  " + String.format("%.2f", discountAmt) + "\n\n");
            }
            textData.append("GRAND TOTAL" + " :  " + String.format("%.2f", GrantTotal) + "\n\n");

            if (cardType.equals("CASH CARD")) {
                textData.append("AVAIL.BAL  " + " :  " + ava_balance + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                if (_isdebitcard.equalsIgnoreCase("debit")) {
                    textData.append("DEBIT CARD.BAL  " + " :  " + ava_balance + "\n");
                } else {
                    //   textData.append("AVAIL.BAL  " + " :  " + ava_balance + "\n");
                    if (ava_balance.contains("-")) {
                        textData.append("BALANCE  " + " :  " + ava_balance + " Cr" + "\n");
                    } else {
                        textData.append("BALANCE  " + " :  " + ava_balance + " Dr" + "\n");
                    }

                }
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }
            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            textData.append("\n");
            textData.append("\n");
            textData.append("(Signature)" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            if (bitmap != null) {

                //    bitmap = getResizedBitmap(bitmap, 200);
                method = "addImage";

                mPrinter.addPagePosition(0, 0);
                //   mPrinter.addImage(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), Builder.PARAM_DEFAULT, Builder.MODE_MONO, Builder.HALFTONE_DITHER, 1.0,Builder.PARAM_DEFAULT);
                Bitmap largeIcon = bitmap;

                mPrinter.addImage(largeIcon, 0, 0, largeIcon.getWidth(), largeIcon.getHeight(), Builder.PARAM_DEFAULT, Builder.MODE_MONO, Builder.HALFTONE_DITHER, 1.0, Builder.PARAM_DEFAULT);

            }


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
            textData.append("* Thank you ! We Wish To Serve You Again ");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);


        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            //return false;
        }
    }

    private void printHeader(int m, ArrayList<CloseorderItems> _closeorderbillList) {
        try {

            if (textData == null) {
                textData = new StringBuilder();
            }
            DecimalFormat format = new DecimalFormat("#.##");
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
            String date = format1.format(cal.getTime());

            String opBalance = _closeorderbillList.get(0).openingBalance;
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            //  textData.append("Cosmopolitan Club (Regd.)\n");
            textData.append(clubName + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);


            if (clubaddress != null && !clubaddress.equals("")) {
                textData.append(clubaddress + "\n");
            } else {
                for (int k = 0; k < MainActivity.addressList.size(); k++) {
//                    textData.append(MainActivity.addressList.get(k) + "\n");
                }
            }
            //    String mstoreGST = sharedpreferences.getString("mstoreGST", "");
         //   textData.append("179,club road,Ooty,Tamil Nadu,643001" + "\n");

            String mstoreGST = BillingProfile.closeorderItemsArrayList.get(0).GSTNO;
            String vocationalmember = sharedpreferences.getString("vocationalmember", "");
            String gst = "", tin = "", cin = "";
            if (mstoreGST.contains("#")) {
                String[] separated = mstoreGST.split("#");
                gst = separated[0]; // this will contain "Fruit"
                try {
                    tin = separated[1];
                    cin = separated[2];
                    SharedPreferences sharedPreferences = getSharedPreferences("GST", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("GST", gst);
                    editor.putString("TIN", tin);
                    editor.putString("CIN", cin);
                    editor.apply();
                } catch (Throwable e) {
                    e.printStackTrace();
                    SharedPreferences sharedpreferences = getSharedPreferences("GST", Context.MODE_PRIVATE);
                    gst = sharedpreferences.getString("GST", "");
                    tin = sharedpreferences.getString("TIN", "");
                    cin = sharedpreferences.getString("CIN", "");
                }
            } else {
                gst = mstoreGST;
                SharedPreferences sharedpreferences = getSharedPreferences("GST", Context.MODE_PRIVATE);
                gst = sharedpreferences.getString("GST", "");
                tin = sharedpreferences.getString("TIN", "");
                cin = sharedpreferences.getString("CIN", "");
            }

            textData.append("\n" + "GST NO." + "" + ": " + gst);
            textData.append("\n" + tin);
            textData.append("\n" + cin + "\n");
            textData.append("" + "" + "" + vocationalmember + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            if (count == 0) {
                textData.append("Bill No. :" + (_closeorderbillList.get(0).getBillDetNumber()).substring(4) + "\n");
            } else {
                textData.append("RE-Print Bill No. :" + (_closeorderbillList.get(0).getBillDetNumber()).substring(4) + "\n");

            }
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            //   mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("Member  Id   " + ": " + _closeorderbillList.get(0).mAcc + "\n");
            textData.append("Member Name  " + ": " + _closeorderbillList.get(0).mName + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");

            if (_closeorderbillList.get(0).cardType.equals("Cash")) {

                textData.append("Type       " + ":" + "CASH BILL" + "\n");

            } else if (_closeorderbillList.get(0).cardType.equals("Credit")) {

                textData.append("Type       " + ":" + "CREDIT BILL" + "\n");
            } else if (_closeorderbillList.get(0).cardType.equals("Complimentary")) {
                textData.append("Type       " + ":" + "Complimentary" + "\n");
            } else {

                textData.append("Type       " + ":" + "REGULAR BILL" + "\n");
            }

            textData.append("Date Time  " + ":" + date + "\n");
            textData.append("Venue      " + ":" + counter_name + "\n");
            // textData.append("Billed By  " + ":" + "           " + "\n");
            //     textData.append("Bill Clerk " + ":" + waiter + "\n");
            textData.append("Steward " + ":" + waiter + "\n");
            textData.append("Waiter     " + ":" + waiternamecode + "\n");
            if (!_isdebitcard.equalsIgnoreCase("debit")) {
                textData.append("Opening Balance" + ":" + opBalance + "\n");
            }
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append(
                    "Item Name             "

                            + String.format("%4s", "Qty")
                            //  + " "
                            + String.format("%8s", "Rate")
                            //  + " "
                            + String.format("%8s", "Amount")
                            + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);


        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Header part issue: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    private void printHeaderpoona(int m, final ArrayList<CloseorderItems> _closeorderbillList) {
        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
            final String date = format1.format(cal.getTime());

            final String opBalance = _closeorderbillList.get(0).openingBalance;
            mSingleThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    int printStatus = mPrinter1.getPrinterStatus();
                    if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                        MultiPrintView.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MultiPrintView.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                            }
                        });
                    } else {
                        PrnStrFormat format = new PrnStrFormat();
                        format.setTextSize(30);
                        format.setAli(Layout.Alignment.ALIGN_CENTER);
                        format.setFont(PrnTextFont.DEFAULT);
                        mPrinter1.setPrintAppendString(clubName, format);


            if (clubaddress != null && !clubaddress.equals("")) {
                format.setTextSize(25);
                format.setAli(Layout.Alignment.ALIGN_CENTER);
                format.setFont(PrnTextFont.DEFAULT);
                mPrinter1.setPrintAppendString(clubaddress, format);

            } else {
                for (int k = 0; k < MainActivity.addressList.size(); k++) {
//                    textData.append(MainActivity.addressList.get(k) + "\n");
                }
            }
            //    String mstoreGST = sharedpreferences.getString("mstoreGST", "");
         //   textData.append("179,club road,Ooty,Tamil Nadu,643001" + "\n");

            String mstoreGST = BillingProfile.closeorderItemsArrayList.get(0).GSTNO;
            String vocationalmember = sharedpreferences.getString("vocationalmember", "");
            String gst = "", tin = "", cin = "";
            if (mstoreGST.contains("#")) {
                String[] separated = mstoreGST.split("#");
                gst = separated[0]; // this will contain "Fruit"
                try {
                    tin = separated[1];
                    cin = separated[2];
                    SharedPreferences sharedPreferences = getSharedPreferences("GST", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("GST", gst);
                    editor.putString("TIN", tin);
                    editor.putString("CIN", cin);
                    editor.apply();
                } catch (Throwable e) {
                    e.printStackTrace();
                    SharedPreferences sharedpreferences = getSharedPreferences("GST", Context.MODE_PRIVATE);
                    gst = sharedpreferences.getString("GST", "");
                    tin = sharedpreferences.getString("TIN", "");
                    cin = sharedpreferences.getString("CIN", "");
                }
            } else {
                gst = mstoreGST;
                SharedPreferences sharedpreferences = getSharedPreferences("GST", Context.MODE_PRIVATE);
                gst = sharedpreferences.getString("GST", "");
                tin = sharedpreferences.getString("TIN", "");
                cin = sharedpreferences.getString("CIN", "");
            }

                         mPrinter1.setPrintAppendString("GST NO." + "" + ": " + gst, format);

                     //   textData.append("\n" + tin);
                      //  mPrinter1.setPrintAppendString(  tin, format);
                      //   mPrinter1.setPrintAppendString(  cin, format);
                      //   mPrinter1.setPrintAppendString(  "" + "" + "" + vocationalmember , format);

             if (count == 0) {
               // textData.append("Bill No. :" + (_closeorderbillList.get(0).getBillDetNumber()).substring(4) + "\n");
                 mPrinter1.setPrintAppendString(" ", format);
                 mPrinter1.setPrintAppendString("Bill No. :" + (_closeorderbillList.get(0).getBillDetNumber()).substring(4), format);

             } else {
             //   textData.append("RE-Print Bill No. :" + (_closeorderbillList.get(0).getBillDetNumber()).substring(4) + "\n");
                 mPrinter1.setPrintAppendString(" ", format);
                 mPrinter1.setPrintAppendString("RE-Print Bill No. :" + (_closeorderbillList.get(0).getBillDetNumber()).substring(4), format);

            }
                        mPrinter1.setPrintAppendString("------------------------------------------------------", format);


                        format.setTextSize(30);
                        format.setStyle(PrnTextStyle.BOLD);
                        mPrinter1.setPrintAppendString("Member  Id   " + ": " + _closeorderbillList.get(0).mAcc , format);
                        mPrinter1.setPrintAppendString("Member Name  " + ": " + _closeorderbillList.get(0).mName, format);
                        mPrinter1.setPrintAppendString("--------------------------------------", format);


                        format.setAli(Layout.Alignment.ALIGN_NORMAL);
                        format.setStyle(PrnTextStyle.NORMAL);
                        format.setTextSize(25);


            if (_closeorderbillList.get(0).cardType.equals("Cash")) {
              mPrinter1.setPrintAppendString("Type       " + ":" + "CASH BILL" , format);

            } else if (_closeorderbillList.get(0).cardType.equals("Credit")) {

             //   textData.append("Type       " + ":" + "CREDIT BILL" + "\n");
                mPrinter1.setPrintAppendString("Type       " + ":" + "CREDIT BILL"  , format);

            } else if (_closeorderbillList.get(0).cardType.equals("Complimentary")) {
             //   textData.append("Type       " + ":" + "Complimentary" + "\n");
                mPrinter1.setPrintAppendString("Type       " + ":" + "Complimentary" , format);

            } else {

              //  textData.append("Type       " + ":" + "REGULAR BILL" + "\n");
                mPrinter1.setPrintAppendString("Type       " + ":" + "REGULAR BILL"  , format);

            }

          //  textData.append("Date Time  " + ":" + date + "\n");
                        mPrinter1.setPrintAppendString("Date Time  " + ":" + date  , format);

                     //   textData.append("Venue      " + ":" + counter_name + "\n");
                        mPrinter1.setPrintAppendString("Venue      " + ":" + counter_name  , format);

                        // textData.append("Billed By  " + ":" + "           " + "\n");
            //     textData.append("Bill Clerk " + ":" + waiter + "\n");
         //   textData.append("Steward " + ":" + waiter + "\n");
                        mPrinter1.setPrintAppendString("Steward " + ":" + waiter, format);
                        mPrinter1.setPrintAppendString("Waiter     " + ":" + waiternamecode, format);

                        if (!_isdebitcard.equalsIgnoreCase("debit")) {
            //    textData.append("Opening Balance" + ":" + opBalance + "\n");
                            mPrinter1.setPrintAppendString("Opening Balance" + ":" + opBalance , format);

                        }
                        mPrinter1.setPrintAppendString("------------------------------------------------------", format);


                        format.setAli(Layout.Alignment.ALIGN_NORMAL);
                        format.setStyle(PrnTextStyle.NORMAL);
                        format.setTextSize(23);
                        mPrinter1.setPrintAppendString(
                                String.format("%30s", ( "Item Name             "+ "                                                ").substring(0, 30))

                                        + String.format("%6s", "Qty")
                                        //  + " "
                                        //    + String.format("%8s", "Rate")
                                        //  + " "
                                        + String.format("%10s", "Amount"), format);
                        mPrinter1.setPrintAppendString("------------------------------------------------------", format);
                    }
                    printStatus = mPrinter1.setPrintStart();
                    if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                        MultiPrintView.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MultiPrintView.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                            }
                        });
                }}});}
        catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Header part issue: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
   private void printHeadertmp80(int m, ArrayList<CloseorderItems> _closeorderbillList) {
        try {

            if (textData == null) {
                textData = new StringBuilder();
            }
            DecimalFormat format = new DecimalFormat("#.##");
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
            String date = format1.format(cal.getTime());

            String opBalance = _closeorderbillList.get(0).openingBalance;
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            //  textData.append("Cosmopolitan Club (Regd.)\n");
            textData.append(counter_name + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);


            if (clubaddress != null && !clubaddress.equals("")) {
                textData.append(clubaddress + "\n");
            } else {
                for (int k = 0; k < MainActivity.addressList.size(); k++) {
//                    textData.append(MainActivity.addressList.get(k) + "\n");
                }
            }
            //    String mstoreGST = sharedpreferences.getString("mstoreGST", "");
         //   textData.append("179,club road,Ooty,Tamil Nadu,643001" + "\n");

            String mstoreGST = BillingProfile.closeorderItemsArrayList.get(0).GSTNO;
            String vocationalmember = sharedpreferences.getString("vocationalmember", "");
            String gst = "", tin = "", cin = "";
            if (mstoreGST.contains("#")) {
                String[] separated = mstoreGST.split("#");
                gst = separated[0]; // this will contain "Fruit"
                try {
                    tin = separated[1];
                    cin = separated[2];
                    SharedPreferences sharedPreferences = getSharedPreferences("GST", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("GST", gst);
                    editor.putString("TIN", tin);
                    editor.putString("CIN", cin);
                    editor.apply();
                } catch (Throwable e) {
                    e.printStackTrace();
                    SharedPreferences sharedpreferences = getSharedPreferences("GST", Context.MODE_PRIVATE);
                    gst = sharedpreferences.getString("GST", "");
                    tin = sharedpreferences.getString("TIN", "");
                    cin = sharedpreferences.getString("CIN", "");
                }
            } else {
                gst = mstoreGST;
                SharedPreferences sharedpreferences = getSharedPreferences("GST", Context.MODE_PRIVATE);
                gst = sharedpreferences.getString("GST", "");
                tin = sharedpreferences.getString("TIN", "");
                cin = sharedpreferences.getString("CIN", "");
            }

         /*   textData.append("\n" + "GST NO." + "" + ": " + gst);
            textData.append("\n" + tin);
            textData.append("\n" + cin + "\n");*/
            textData.append("" + "" + "" + vocationalmember + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            if (count == 0) {
                textData.append("Bill No. :" + (_closeorderbillList.get(0).getBillDetNumber()).substring(4) + "\n");
            } else {
                textData.append("RE-Print Bill No. :" + (_closeorderbillList.get(0).getBillDetNumber()).substring(4) + "\n");

            }
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
        //    textData.append("--------------------------------------\n");
            textData.append("------------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            //   mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("Member  Id   " + ": " + _closeorderbillList.get(0).mAcc + "\n");
            textData.append("Member Name  " + ": " + _closeorderbillList.get(0).mName + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("------------------------------------------------\n");

            if (_closeorderbillList.get(0).cardType.equals("Cash")) {

                textData.append("Type          " + ":" + "CASH BILL" + "\n");

            } else if (_closeorderbillList.get(0).cardType.equals("Credit")) {

                textData.append("Type          " + ":" + "CREDIT BILL" + "\n");
            } else if (_closeorderbillList.get(0).cardType.equals("Complimentary")) {
                textData.append("Type          " + ":" + "Complimentary" + "\n");
            } else {

                textData.append("Type          " + ":" + "REGULAR BILL" + "\n");
            }

            textData.append("Date Time     " + ":" + date + "\n");
            textData.append("Venue         " + ":" + counter_name + "\n");
            // textData.append("Billed By  " + ":" + "           " + "\n");
            //     textData.append("Bill Clerk " + ":" + waiter + "\n");
            textData.append("Steward       " + ":" + waiter + "\n");
            textData.append("Waiter        " + ":" + waiternamecode + "\n");
            if (!_isdebitcard.equalsIgnoreCase("debit")) {
                textData.append("Opening Balance" + ":" + opBalance + "\n");
            }
            textData.append("------------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append(
                    "Item Name                "

                            + String.format("%7s", "Qty")
                            //  + " "
                            + String.format("%11s", "Rate")
                            //  + " "
                            + String.format("%11s", "Amount")
                            + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("------------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);


        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Header part issue: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            return false;
        }

        PrinterStatusInfo status = mPrinter.getStatus();

        dispPrinterWarnings(status);

        if (!isPrintable(status)) {
            ShowMsg.showMsg(makeErrorMessage(status), mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    public boolean connectPrinter() {
        boolean isBeginTransaction = false;
        String ll = "";

        if (mPrinter == null) {
            return false;
        }

        try {
            ll = printer_Name.getText().toString();
            if (!ll.equals("")) {
                mPrinter.connect(printer_Name.getText().toString(), Printer.PARAM_DEFAULT);
            } else {
                String[] l = ll.split("-");
                mPrinter.connect(l[1], Printer.PARAM_DEFAULT);
            }
        } catch (Exception e) {
            ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        try {
            mPrinter.beginTransaction();
            isBeginTransaction = true;
        } catch (Exception e) {
            ShowMsg.showException(e, "beginTransaction", mContext);
        }

        if (isBeginTransaction == false) {
            try {
                mPrinter.disconnect();
            } catch (Epos2Exception e) {
                // Do nothing
                return false;
            }
        }

        return true;
    }

    public void dispPrinterWarnings(PrinterStatusInfo status) {
        //EditText edtWarnings = (EditText) findViewById(R.id.edtWarnings1);

        String warningsMsg = "";

        if (status == null) {
            return;
        }

        if (status.getPaper() == Printer.PAPER_NEAR_END) {
            warningsMsg += getString(R.string.handlingmsg_warn_receipt_near_end);
        }

        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_1) {
            warningsMsg += getString(R.string.handlingmsg_warn_battery_near_end);
        }

        // edtWarnings.setText(warningsMsg);
        Toast.makeText(getApplicationContext(), warningsMsg, Toast.LENGTH_LONG).show();
    }

    public boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            //print available
        }

        return true;
    }

    public String makeErrorMessage(PrinterStatusInfo status) {
        String msg = "";

        if (status.getOnline() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_offline);
        }
        if (status.getConnection() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_no_response);
        }
        if (status.getCoverOpen() == Printer.TRUE) {
            msg += getString(R.string.handlingmsg_err_cover_open);
        }
        if (status.getPaper() == Printer.PAPER_EMPTY) {
            msg += getString(R.string.handlingmsg_err_receipt_end);
        }
        if (status.getPaperFeed() == Printer.TRUE || status.getPanelSwitch() == Printer.SWITCH_ON) {
            msg += getString(R.string.handlingmsg_err_paper_feed);
        }
        if (status.getErrorStatus() == Printer.MECHANICAL_ERR || status.getErrorStatus() == Printer.AUTOCUTTER_ERR) {
            msg += getString(R.string.handlingmsg_err_autocutter);
            msg += getString(R.string.handlingmsg_err_need_recover);
        }
        if (status.getErrorStatus() == Printer.UNRECOVER_ERR) {
            msg += getString(R.string.handlingmsg_err_unrecover);
        }
        if (status.getErrorStatus() == Printer.AUTORECOVER_ERR) {
            if (status.getAutoRecoverError() == Printer.HEAD_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_head);
            }
            if (status.getAutoRecoverError() == Printer.MOTOR_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_motor);
            }
            if (status.getAutoRecoverError() == Printer.BATTERY_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_battery);
            }
            if (status.getAutoRecoverError() == Printer.WRONG_PAPER) {
                msg += getString(R.string.handlingmsg_err_wrong_paper);
            }
        }
        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_0) {
            msg += getString(R.string.handlingmsg_err_battery_real_end);
        }

        return msg;
    }

    public void updateButtonState(boolean state) {
        Button btnReceipt = findViewById(R.id.button_cancelprint);
        //Button btnCoupon = (Button)findViewById(R.id.btnSampleCoupon);

        if (state == true) {
            if (count == 0) {
                btnReceipt.setEnabled(state);
                btnReceipt.setBackgroundColor(getResources().getColor(R.color.violet));
                btnReceipt.setText("Print");

            } else {
                btnReceipt.setEnabled(state);
                btnReceipt.setBackgroundColor(getResources().getColor(R.color.violet));
                btnReceipt.setText("Re-Print");

            }
        } else {
            btnReceipt.setBackgroundColor(getResources().getColor(R.color.black_semi_transparent));
            btnReceipt.setEnabled(state);
            btnReceipt.setText("Printing ...");
        }
        //  btnCoupon.setEnabled(state);
    }

    public void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "endTransaction", mContext);
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "disconnect", mContext);
                }
            });
        }

        finalizeObject();
    }

    public void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        clearbills();
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("vocationalmember", "");
        editor.commit();
        Intent i = new Intent(MultiPrintView.this, Accountlist.class);
        i.putExtra("tableNo", tableNo);
        i.putExtra("tableId", tableId);
        i.putExtra("from", "CloseOrderdetails");
        //   i.putExtra("position",position);
        startActivity(i);

    }

    private void clearbills() {
        Dbase dbase = new Dbase(MultiPrintView.this);
        dbase.clearBills();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                clearbills();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("vocationalmember", "");
                editor.commit();
                Intent i = new Intent(MultiPrintView.this, Accountlist.class);
                i.putExtra("tableNo", tableNo);
                i.putExtra("tableId", tableId);
                i.putExtra("from", "CloseOrderdetails");
                startActivity(i);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup(String title) {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ab = getSupportActionBar();
            ab.setTitle("" + title);
            ab.setElevation(0);

            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        if (data != null && resultCode == RESULT_OK) {
            String target = data.getStringExtra(getString(R.string.title_target));
            String printer = data.getStringExtra(getString(R.string.title_Printer));
            if (printer != null) {
                EditText mEdtTarget = findViewById(R.id.edtTarget1);
                mEdtTarget.setText(printer + "-" + target);

            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private int getPrinterCode(String printername) {
        int printerCode = 2;
        switch (printername) {
            case "TM_P20":
                printerCode = 2;
                break;
            case "TM_P80":
                printerCode = 5;
                break;
         /*   case 3:
                noOfDays = 31;
                break;
            case 4:
                noOfDays = 30;
                break;
            case 5:
                noOfDays = 31;
                break;
            case 6:
                noOfDays = 30;
                break;
            case 7:
                noOfDays = 31;
                break;
            case 8:
                noOfDays = 31;
                break;
            case 9:
                noOfDays = 30;
                break;
            case 10:
                noOfDays = 31;
                break;
            case 11:
                noOfDays = 30;
                break;
            case 12:
                noOfDays = 31;
                break;*/

        }
        return printerCode;

    }

    boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
