package com.irca.cosmo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.Billing.Accountlist;
import com.irca.Billing.MainActivity;
import com.irca.Billing.StoreList;
import com.irca.ImageCropping.CircularImage;
import com.irca.Printer.DiscoveryActivity;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.db.Dbase;
import com.irca.fields.Item;
import com.irca.fields.Table;
import com.irca.fields.WaiterDetails;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;
import com.irca.widgets.FloatingActionButton.FloatingActionsMenu;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by Manoch Richard on 10/19/2015.
 */
public class Dashboard extends Activity {
    GridView gridDashboard;
    GridAdapter gridAdapter;
    ImageView settings, logOut;
    TextView memberNamee, TITLE;
    SharedPreferences sharedpreferences;
    ProgressDialog pd;
    Dbase db;
    int tablecount = 0;
    FloatingActionsMenu reports;
    SharedPreferences bluetooth_prefrence;
    String target = "";
    String Printername = "";
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    String billType = "";
    WaiterDetails waiterDetails = null;
    public static ArrayList<WaiterDetails> waiterlist = null;
    ImageView logo;
    Bitmap bitmap;
    public static String pid, pcode, pname;
    public static String ItemCategoryID = "";
    String posId = "";
    String imei = "";
    // int positionn;
    String serialNo = "";
    TelephonyManager tel;

    @Override
    protected void onResume() {
        super.onResume();
        if (reports.isExpanded()) {

            reports.collapse();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Fabric.with(this, new Crashlytics());
        setContentView(R.layout.dashboard);
        tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        // imei=tel.getDeviceId().toString();
        serialNo = Build.SERIAL;

        gridDashboard = (GridView) findViewById(R.id.dashboard_gridlayout);
        memberNamee = (TextView) findViewById(R.id.textView_name);
        settings = (ImageView) findViewById(R.id.referesh);
        logOut = (ImageView) findViewById(R.id.textView_logout);
        reports = (FloatingActionsMenu) findViewById(R.id.fab_siginstatus);
        TITLE = (TextView) findViewById(R.id.pos);
        logo = (ImageView) findViewById(R.id.imageView_logo);
        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
        logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));

        if (reports.isExpanded()) {

            reports.collapse();
        } //TODO remove if created problem


        bluetooth_prefrence = getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);
        target = bluetooth_prefrence.getString("Target", "");
        Printername = bluetooth_prefrence.getString("Printer", "");
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();


        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String name = sharedpreferences.getString("loginName", "");
        billType = sharedpreferences.getString("billType", "");
        String StoreName = sharedpreferences.getString("StoreName", "");
        posId = sharedpreferences.getString("storeId", "");
        TITLE.setText(StoreName);


        if (target.equals("")) {
            Intent intent = new Intent(this, DiscoveryActivity.class);
            startActivityForResult(intent, 0);
        }

        memberNamee.setText("Welcome," + " " + name.toLowerCase());

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logout();
                // Refresh();
            }
        });

        db = new Dbase(Dashboard.this);
        tablecount = db.getTableCount();
        if (tablecount == 0) {
            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent || !isInternetPresent) {
                new AsyncTablename().execute();

            } else {
                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
            }

        } else {
            final ArrayList<Table> tablename = db.getTablename();
            gridAdapter = new GridAdapter(Dashboard.this, tablename);
            gridDashboard.setAdapter(gridAdapter);
            gridDashboard.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    reports.collapse();

                    int itemGroupCount = db.getItemGroupCount();
                    int waiterCount = db.getWaiterCount();
                    int itemcount = db.getItemCount(posId);

                    //  if (itemGroupCount == 0) {
                   /* if (waiterCount == 0) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if(isInternetPresent){
                            new AsyncMaster().execute(position + 1 + "", tablename.get(position).getTableId() + "");
                        }else {
                            Toast.makeText(getApplicationContext(),"Internet Connection Lost",Toast.LENGTH_SHORT).show();
                        }

                    } else*/

                    if (itemcount == 0) {
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new AsyncItemMaster().execute(position + 1 + "", tablename.get(position).getTableId() + "", "0");
                        } else {
                            Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Intent i = new Intent(Dashboard.this, Accountlist.class);
                        i.putExtra("tableNo", position + 1);
                        i.putExtra("tableId", tablename.get(position).getTableId() + "");
                        startActivity(i);
                    }

                }
            });
        }
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Dashboard.this, StoreList.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });


        FloatingActionButton memberReport = new FloatingActionButton(this);
        memberReport.setIcon(R.drawable.report1);
        memberReport.setTitle("Todays Order");
        memberReport.setColorNormal(Color.parseColor("#e75b1e"));
        reports.addButton(memberReport);

        memberReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reports.collapse();
                Intent i = new Intent(Dashboard.this, TodaysAccount.class);
                i.putExtra("mpage", "0");
                startActivity(i);
            }
        });


        FloatingActionButton TodaysReport = new FloatingActionButton(this);
        TodaysReport.setIcon(R.drawable.report2);
        TodaysReport.setTitle("Steward's Order");
        TodaysReport.setColorNormal(Color.parseColor("#e75b1e"));
        reports.addButton(TodaysReport);

        TodaysReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reports.collapse();
                Intent i = new Intent(Dashboard.this, TodaysAccount.class);
                i.putExtra("mpage", "0");
                startActivity(i);
            }
        });


        FloatingActionButton printTest = new FloatingActionButton(this);
        printTest.setIcon(R.drawable.report1);
        printTest.setTitle("Printer Test");
        printTest.setColorNormal(Color.parseColor("#e75b1e"));
        reports.addButton(printTest);

        printTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reports.collapse();
                new AsyncPrinterTest().execute(posId, "", serialNo);


            }
        });

        FloatingActionButton _refresh = new FloatingActionButton(this);
        _refresh.setIcon(R.drawable.refresh);
        _refresh.setTitle("Refresh");
        _refresh.setColorNormal(Color.parseColor("#e75b1e"));
        reports.addButton(_refresh);

        _refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reports.collapse();
                Refresh();

            }
        });




      /*  FloatingActionButton Steward_refresh = new FloatingActionButton(this);
        Steward_refresh.setIcon(R.drawable.refresh);
        Steward_refresh.setTitle("Steward Refresh");
        Steward_refresh.setColorNormal(Color.parseColor("#e75b1e"));
        reports.addButton(Steward_refresh);

        Steward_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(Dashboard.this, "Steward Refresh Start", Toast.LENGTH_SHORT).show();
                reports.collapse();
                StewardRefresh();
               // Refresh();

            }
        });*/

       /* if(!billType.equals("BAK") || billType.equals("")){


            FloatingActionButton otReprint = new FloatingActionButton(this);
            otReprint.setIcon(R.drawable.report2);
            otReprint.setTitle("OT Reprint");
            otReprint.setColorNormal(Color.parseColor("#e75b1e"));
            reports.addButton(otReprint);

            otReprint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    reports.collapse();
                    Intent i = new Intent(Dashboard.this, Ot_Acc_List.class);
                    i.putExtra("mpage","1");
                    startActivity(i);
                }
            });
        }
*/


    }

    private void Logout() {
        new AlertDialog.Builder(Dashboard.this)
                //.setMessage("Do you want to Logout?")
                .setMessage("Do you want to Logout?")
                //  .setMessage("Do you want to Refresh?")
                //  .setIcon(R.drawable.ninja)
                .setPositiveButton("Logout",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                Intent back = new Intent(Dashboard.this, MainActivity.class);
                                back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                //       back.putExtra("position",positionn);
                                startActivity(back);
                                finish();
                            }
                        })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }


    private void Refresh() {
        new AlertDialog.Builder(Dashboard.this)
                .setTitle("Refresh POS")
                //  .setMessage("Do you want to Logout?")
                .setMessage("Do you want to Refresh?")
                //  .setIcon(R.drawable.ninja)
                .setPositiveButton("Refresh",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                int count = db.getAccountCount(posId);
                                if (count > 0) {
                                    Toast.makeText(getApplicationContext(), "Please close the Member order in POS", Toast.LENGTH_LONG).show();
                                } else {
                                    db.deleteTable();
                                    db.deleteItem(posId);
                                    db.deleteItemGroup(posId);

                                    Intent back = new Intent(Dashboard.this, MainActivity.class);
                                    back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    //   back.putExtra("position",positionn);
                                    startActivity(back);
                                    finish();

                                }

                            }
                        })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    private void StewardRefresh() {
        new AlertDialog.Builder(Dashboard.this)
                .setTitle("Steward Refresh")
                //  .setMessage("Do you want to Logout?")
                .setMessage("Do you want to Refresh?")
                //  .setIcon(R.drawable.ninja)
                .setPositiveButton("Steward Refresh",
                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {

                                int count = db.getAccountCount(posId);
                                if (count > 0) {
                                    Toast.makeText(getApplicationContext(), "Please close the Member order in POS", Toast.LENGTH_LONG).show();
                                } else {

                                    //   db.deleteTable();
                                    // db.deleteItem(posId);
                                    //  db.deleteItemGroup(posId);
                                    db.deleteSteward();

                                    Intent back = new Intent(Dashboard.this, MainActivity.class);
                                    back.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    //   back.putExtra("position",positionn);
                                    startActivity(back);
                                    finish();

                                }

                            }
                        })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    protected class AsyncTablename extends AsyncTask<String, Void, ArrayList<Table>> {

        ArrayList<Table> tablename = null;
        String storeId = sharedpreferences.getString("storeId", "");

        @Override
        protected ArrayList<Table> doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard.this);
            try {

                String[] narr_kitchen = new String[]{"Salt", "Sugar", "More Salt", "Less Salt", "More Sugar", "Less Sugar", "More Spice", "Medium Spice", "Less Spice"};
                //   String[] narr_bar=new String[]{"Salt","Sugar","More Salt","Less Salt","More Sugar","Less Sugar","More Spice","Medium Spice","Less Spice"};
                //  Bundle b=getIntent().getExtras();
                //   positionn=b.getInt("position");
                //int storeId=b.getInt("Storeid");
                // JSONObject jsonObject=api.cms_getTable(storeId);//1150511156
                // JSONArray jsonArray=jsonObject.getJSONArray("Value");
                tablename = new ArrayList<>();
                Table table = null;
                for (int i = 1; i < 21; i++) {
                    table = new Table();
                    // JSONObject jsonObject1=jsonArray.getJSONObject(i);
                    // String tableN=jsonObject1.get("TableNumber").toString();
                    //  String tableid=jsonObject1.get("TableId").toString();
                    table.setTableId(i);
                    table.setTableNmae("" + i);
                    tablename.add(table);
                }


                for (int j = 0; j < narr_kitchen.length; j++) {
                    long i = db.insertNarration(narr_kitchen[j], "KOT");
                }

                if (tablename != null) {
                    db.deleteTable();
                    db.insertTables(tablename);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return tablename;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(final ArrayList<Table> s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s != null) {
                gridAdapter = new GridAdapter(Dashboard.this, s);
                gridDashboard.setAdapter(gridAdapter);
                gridDashboard.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        int itemGroupCount = db.getItemGroupCount();
                        int waiterCount = db.getWaiterCount();
                        int itemcount = db.getItemCount(posId);
                        // if(itemGroupCount==0)
                        if (waiterCount == 0) {
                            new AsyncMaster().execute(position + 1 + "", s.get(position).getTableId() + "");
                        } else if (itemcount == 0) {
                            new AsyncItemMaster().execute(position + 1 + "", tablename.get(position).getTableId() + "", "0");
                        } else {
                            Intent i = new Intent(Dashboard.this, Accountlist.class);
                            i.putExtra("tableNo", position + 1);
                            i.putExtra("tableId", s.get(position).getTableId() + "");
                            //    Bundle b=getIntent().getExtras();
                            //  positionn=b.getInt("position");
                            //     i.putExtra("position",positionn);
                            startActivity(i);
                        }

                    }
                });

            }

        }
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Logout();
    }


    protected class AsyncMaster extends AsyncTask<String, Void, Void> {
        // ArrayList<ItemGroup> itemGrouplist=null;
        // ArrayList<WaiterDetails> waiterlist=null;
        ArrayList<Item> itemList = null;
        //   WaiterDetails waiterDetails=null;
        Item item = null;
        boolean itembool = false;
        boolean itemgroupbool = false;
        int position = 0;
        int tableId;
        String itemCategoryId = sharedpreferences.getString("itemCategoryId", "");

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard.this);
            try {

                String gid = "0";
                pid = "0";
                position = Integer.parseInt(params[0]);
                tableId = Integer.parseInt(params[1]);


                //-------waiter-----------------------

                JSONObject jsonObject = api.cms_getSteward("","");
                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                waiterlist = new ArrayList<WaiterDetails>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    waiterDetails = new WaiterDetails();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    pid = jsonObject1.get("personid").toString();
                    pcode = jsonObject1.get("personcode").toString();
                    pname = jsonObject1.get("FirstName").toString();
                    waiterDetails.setpersonid(pid);
                    waiterDetails.setpersoncode(pcode);
                    waiterDetails.setFirstName(pname);
                    waiterlist.add(waiterDetails);

                }
                if (waiterlist != null) {
                    //  db.deleteItemGroup();
                    Long mm = db.insertWaiterGroup(waiterlist);
                    if (mm > 0) {
                        itemgroupbool = true;
                    }

                }


                //----------------------


                //item group
              /*  JSONObject jsonObject=api.cms_getItemGroup(itemCategoryId);
                JSONArray jsonArray=jsonObject.getJSONArray("Value");
                itemGrouplist=new ArrayList<ItemGroup>();
                for(int i=0;i<jsonArray.length();i++)
                {
                    itemGroup=new ItemGroup();
                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                     gid=jsonObject1.get("ID").toString();
                    String gcode=jsonObject1.get("GroupCode").toString();
                    String gname=jsonObject1.get("GroupName").toString();
                    itemGroup.setGroupid(gid);
                    itemGroup.setGroupcode(gcode);
                    itemGroup.setGroupname(gname);
                    itemGrouplist.add(itemGroup);

                }
                if(itemGrouplist!=null)
                {
                    db.deleteItemGroup();
                   Long mm=db.insertItemGroup(itemGrouplist);
                    if(mm>0)
                    {
                        itemgroupbool=true;
                    }

                }*/
                //item list

                JSONObject jsonObject1 = api.cms_getItemlist(itemCategoryId, posId);
                JSONArray jsonArray1 = jsonObject1.getJSONArray("Value");
                itemList = new ArrayList<Item>();
                for (int i = 0; i < jsonArray1.length(); i++) {
                    item = new Item();
                    JSONObject jsonObjectchild = jsonArray1.getJSONObject(i);
                    if (jsonObjectchild.has("ItemGroupID")) {

                        String ItemCategoryID = jsonObjectchild.optString("ItemCategoryID");
                        String ItemCode = jsonObjectchild.optString("ItemCode");
                        String ItemIdentityID = jsonObjectchild.optString("ItemIdentityID");
                        String ItemGroupID = jsonObjectchild.optString("ItemGroupID");
                        String ItemName = jsonObjectchild.optString("ItemName");
                        String StoreID = jsonObjectchild.optString("StoreID");
                        String TaxID = jsonObjectchild.optString("TaxID");
                        String Tax = jsonObjectchild.optString("Tax");
                        String unitId = jsonObjectchild.optString("UnitID");
                        String Rate = jsonObjectchild.optString("Rate");
                        String description = jsonObjectchild.optString("description");

                        item.setItemCategoryID(ItemCategoryID);
                        item.setItemcode(ItemCode);
                        item.setItemIdentityID(ItemIdentityID);
                        item.setItemGroupID(ItemGroupID);
                        item.setItemName(ItemName);
                        item.setStoreID(StoreID);
                        item.setTaxID(TaxID);
                        item.setTax(Tax);
                        item.setUnitID(unitId);
                        item.setRate(Rate);
                        item.setDescription(description);
                        itemList.add(item);
                    } else {
                        int id = i;
                    }

                }
                if (itemList != null) {
                    //   db.deleteItem();
                    Long mm = db.insertItem(itemList);
                    if (mm > 0) {
                        itembool = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            pd.dismiss();

            if (itembool) {
                Intent i = new Intent(Dashboard.this, Accountlist.class);
                i.putExtra("tableNo", position);
                i.putExtra("tableId", tableId);
                //   i.putExtra("position",positionn);
                startActivity(i);
            } else {
                Toast.makeText(Dashboard.this, "Error", Toast.LENGTH_LONG).show();
            }
        }
    }


    protected class AsyncItemMaster extends AsyncTask<String, Void, Void> {

        ArrayList<Item> itemList = null;
        Item item = null;
        boolean itembool = false;
        int position = 0;
        int tableId;
        String itemCategoryId = sharedpreferences.getString("itemCategoryId", "");

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard.this);
            try {
                position = Integer.parseInt(params[0]);
                tableId = Integer.parseInt(params[1]);

                //item list
                JSONObject jsonObject1 = api.cms_getItemlist(itemCategoryId, posId);
                JSONArray jsonArray1 = jsonObject1.getJSONArray("Value");
                itemList = new ArrayList<Item>();
                for (int i = 0; i < jsonArray1.length(); i++) {
                    item = new Item();
                    JSONObject jsonObjectchild = jsonArray1.getJSONObject(i);
                    if (jsonObjectchild.has("ItemGroupID")) {

                        String ItemIdentityID = jsonObjectchild.optString("ItemIdentityID");
                        String ItemGroupID = jsonObjectchild.optString("ItemGroupID");
                        String StoreID = jsonObjectchild.optString("StoreID");
                        String ItemCode = jsonObjectchild.optString("ItemCode");
                        String ItemName = jsonObjectchild.optString("ItemName");
                        String Rate = jsonObjectchild.optString("Rate");
                        ItemCategoryID = jsonObjectchild.optString("ItemCategoryID");
                        String unitId = jsonObjectchild.optString("UnitID");
                        //String TaxID=jsonObjectchild.optString("TaxID");
                        //String Tax=jsonObjectchild.optString("Tax");
                        //String description=jsonObjectchild.optString("description");
                        item.setItemCategoryID(ItemCategoryID);
                        item.setItemcode(ItemCode);
                        item.setItemIdentityID(ItemIdentityID);
                        item.setItemGroupID(ItemGroupID);
                        item.setItemName(ItemName);
                        item.setStoreID(StoreID);
                        item.setUnitID(unitId);
                        item.setRate(Rate);
                        //item.setDescription(description);
                        // item.setTaxID(TaxID);
                        // item.setTax(Tax);
                        itemList.add(item);
                    } else {
                        int id = i;
                    }

                }
                if (itemList != null) {
                    // db.deleteItem();
                    Long mm = db.insertItem(itemList);
                    if (mm > 0) {
                        itembool = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(Void s) {
            super.onPostExecute(s);
            pd.dismiss();

            if (itembool) {
                Intent i = new Intent(Dashboard.this, Accountlist.class);
                i.putExtra("tableNo", position);
                i.putExtra("tableId", tableId);
                startActivity(i);
            } else {
                Toast.makeText(Dashboard.this, "Error", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class AsyncPrinterTest extends AsyncTask<String, Void, Void> {
        String status = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(Dashboard.this);
            pd.show();
            pd.setMessage("Please wait loading....");

        }

        @Override
        protected Void doInBackground(String... params) {
            RestAPI api = new RestAPI(Dashboard.this);
            try {

                JSONObject jsonObject1 = api.printTest(params[0], params[1], params[2]);
                // JSONArray jsonArray1=jsonObject1.getJSONArray("Value");
                status = jsonObject1.toString();

            } catch (Exception e) {
                String r = e.getMessage().toString();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            if (status.contains("true")) {
                Toast.makeText(getApplication(), "Printer connected ", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplication(), "Printer  not  connected ", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
