package com.irca.cosmo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;

//import com.crashlytics.android.Crashlytics;
import com.irca.db.Dbase;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by Manoch Richard on 10/19/2015.
 */
public class Kot extends Activity
{
    ListView listView ;
    TextView tableNo,memberDetails;
    String name="",accountNo="",creditLimit="",memberId="0";
    int tableId=0;
    Dbase db;
    FloatingActionButton newTable;
    String cardType="",referenceNo="";
    int table;
    SharedPreferences sharedpreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.kot_group);
        listView = (ListView) findViewById(R.id.list_kotgroup);
        tableNo= (TextView) findViewById(R.id.tablename);
        memberDetails=(TextView) findViewById(R.id.memberDetails);
        newTable = (FloatingActionButton) findViewById(R.id.fab_newtable);
        db=new Dbase(Kot.this);

        ArrayList<String> list=db.getGroupList();

//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, android.R.id.text1, list);
//            listView.setAdapter(adapter);
        listView.setAdapter(new MyListAdaptor(this, list));
         Bundle b=getIntent().getExtras();
         table=b.getInt("tableNo");
         tableNo.setText("Table" + table);
         name=b.getString("creditName");
         accountNo=b.getString("creditAno");
         creditLimit=b.getString("creditLimit");
         memberId=b.getString("memberId");
        memberDetails.setText("Name:  "+name+"\n Account No.:  "+accountNo+"\n Opening Balance:  "+creditLimit+" Rs");
        tableId=b.getInt("tableId");
        cardType=b.getString("cardType");
        referenceNo=b.getString("referenceNo");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // ListView Clicked item index
                int itemPosition = position;
                // ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);
                // Show Alert
//                Toast.makeText(getApplicationContext(),
//                        "Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_LONG)
//                        .show();
                String groupId=db.getGroupId(itemValue);
                if(!groupId.equals("0"))
                {
                    sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                    String billType=sharedpreferences.getString("billType", "");
                    if(billType.equals("BAK"))
                    {
                        Intent i = new Intent(Kot.this, Bakery.class);
                        i.putExtra("GroupName", itemValue);
                        i.putExtra("name", name);
                        i.putExtra("accountNo", accountNo);
                        i.putExtra("credit", creditLimit);
                        i.putExtra("groupId", groupId);
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableNo",table);
                        i.putExtra("referenceNo",referenceNo);
                        startActivity(i);
                    }
                    else
                    {
                        Intent i = new Intent(Kot.this, KotItemlist.class);
                        i.putExtra("GroupName", itemValue);
                        i.putExtra("name", name);
                        i.putExtra("accountNo", accountNo);
                        i.putExtra("credit", creditLimit);
                        i.putExtra("groupId", groupId);
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableNo",table);
                        i.putExtra("referenceNo",referenceNo);
                        startActivity(i);
                    }

                }

            }

        });
        newTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Kot.this, Dashboard.class);
                startActivity(i);
                finish();
            }
        });
    }
    public class MyListAdaptor extends ArrayAdapter<String> implements SectionIndexer
    {

        HashMap<String, Integer> alphaIndexer;
        String[] sections;

        public MyListAdaptor(Context context, ArrayList<String> items)
        {
            super(context, R.layout.list_item, items);

            alphaIndexer = new HashMap<String, Integer>();
            int size = items.size();

            for (int x = 0; x < size; x++)
            {
                String s = items.get(x);

                // get the first letter of the store
                String ch = s.substring(0, 1);
                // convert to uppercase otherwise lowercase a -z will be sorted
                // after upper A-Z
                ch = ch.toUpperCase();
                if (!alphaIndexer.containsKey(ch))
                {
                    alphaIndexer.put(ch, x);

                }

            }
            Set<String> sectionLetters = alphaIndexer.keySet();

            // create a list from the set to sort
            ArrayList<String> sectionList = new ArrayList<String>(sectionLetters);

            Collections.sort(sectionList);

            sections = new String[sectionList.size()];

            sectionList.toArray(sections);


        }

        @Override
        public Object[] getSections()
        {
            return sections;
        }

        @Override
        public int getPositionForSection(int sectionIndex)
        {
            return alphaIndexer.get(sections[sectionIndex]);
        }

        @Override
        public int getSectionForPosition(int position)
        {
            return 0;
        }
    }
}
