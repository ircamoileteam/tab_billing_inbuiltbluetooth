package com.irca.cosmo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.ImageCropping.CircularImage;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.db.Dbase;
import com.irca.fields.Card;
import com.irca.fields.CloseorderItems;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

//import io.fabric.sdk.android.Fabric;

public class Ot_Acc_List extends Activity {

    ListView listView ;
    Dbase db;
    TextView title;
    SharedPreferences sharedpreferences;
    String cardType="",member="";
    public static ArrayList<CloseorderItems> closeorderItemsArrayList=new ArrayList<CloseorderItems>();
    ProgressDialog pd;
    ConnectionDetector cd;
    Boolean isInternetPresent=false;
    ImageView backbutton;
    ImageView logo;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_ot__acc__list);

        listView = (ListView) findViewById(R.id.list_store1);
        title= (TextView) findViewById(R.id.txTitle1);
        backbutton=(ImageView)findViewById(R.id.back);

        logo=(ImageView)findViewById(R.id.imageView_logo);
        bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
        logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));


        db=new Dbase(Ot_Acc_List.this);
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        String storeid=sharedpreferences.getString("storeId", "");
        final ArrayList<Card> list=db.getAccountList1(storeid);
        AccountAdapter adapter = new AccountAdapter(Ot_Acc_List.this,list);
        listView.setAdapter(adapter);
        title.setText("Member Account");


        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Ot_Acc_List.this,Dashboard.class);
                startActivity(i);
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                isInternetPresent = cd.isConnectingToInternet();
                member= list.get(position).getAccount().toString();

                if(member.contains("#"))
                {
                    String ss1[]=member.split("#");

                    if(isInternetPresent){
                        new AsyncCloseOrder().execute(ss1[1],"1");
                    }else {
                        Toast.makeText(getApplicationContext(),"Internet Connection Lost",Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {

                   // new AsyncCloseOrder().execute(ss[0],"1");
                }


                /*Bundle b = getIntent().getExtras();
                int tableNo = b.getInt("tableNo");
                String tableId = b.getString("tableId");
*/
              /*  Intent i = new Intent(Ot_Acc_List.this, TakeOrder.class);
                i.putExtra("tableNo", tableNo);
                i.putExtra("tableId", tableId);
                i.putExtra("accountType", list.get(position).getAccount().toString());
                i.putExtra("cardType", list.get(position).getCardtype().toString());
                startActivity(i);
                finish();*/
            }

        });
    }

    protected class AsyncCloseOrder extends AsyncTask<String,Void,String>
    {
        String status="";
        String storeId = sharedpreferences.getString("storeId", "");
        String deviceid=sharedpreferences.getString("deviceId", "");
        String userId=sharedpreferences.getString("userId", "");
        String pageType="";
        @Override
        protected String doInBackground(String... params)
        {
            RestAPI api=new RestAPI(Ot_Acc_List.this);
            try
            {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                closeorderItemsArrayList.clear();
                //JSONObject jsonObject=api.cms_closeOrder(params[0], deviceid, storeId, formattedDate,Integer.parseInt(params[1]),userId,"","","");
                JSONObject jsonObject=api.cms_getOTReprint(params[0], deviceid, storeId, formattedDate, userId, "", "","");
                JSONArray array=jsonObject.getJSONArray("Value");
                CloseorderItems cl=null;
                pageType=params[1];
                for(int i=0;i<array.length();i++)
                {
                    cl=new CloseorderItems();
                    JSONObject object=array.getJSONObject(i);
                    String ItemID = object.optString("ItemID");
                    String ItemCode = object.optString("ItemCode");
                    String Quantity = object.optString("Quantity");
                    String Amount = object.optString("Amount");
                    String BillID = object.optString("BillID");
                    String mAcc = object.getString("memberAcc");
                    String billNo = object.optString("billnumber");
                    String Rate = object.getString("rate");
                    String itemname = object.getString("itemname");
                    String mName = object.getString("membername");
                    String memberId = object.getString("memberId");
                    String BillDate = object.getString("BillDate");
                    String otNo=object.getString("otno");

                    cl.setItemID(ItemID);
                    cl.setItemCode(ItemCode);
                    cl.setQuantity(Quantity);
                    cl.setAmount(Amount);
                    cl.setBillID(BillID);
                    cl.setmAcc(mAcc);
                    cl.setBillno(billNo);
                    cl.setmName(mName);
                    cl.setMemberID(memberId);
                    cl.setBillDate(BillDate);
                    cl.setRate(Rate);
                    cl.setItemname(itemname);
                    cl.setOtno(otNo);
                    cl.setCardType(cardType);

                    closeorderItemsArrayList.add(cl);
                }
                status=jsonObject.toString();

            } catch (Exception e)
            {
                e.printStackTrace();
                status=e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pd=new ProgressDialog(Ot_Acc_List.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            pd.dismiss();
            Intent i = new Intent(Ot_Acc_List.this, OT_Reprint.class);
            i.putExtra("type","ot_acc_list");
           /* i.putExtra("tableNo", tableNo);
            i.putExtra("tableId", tableId);
            i.putExtra("accountType", list.get(position).getAccount().toString());
            i.putExtra("cardType", list.get(position).getCardtype().toString());*/
            startActivity(i);
         //   finish();


         /*   if(pageType.equals("0"))
            {
                if (s.contains("true"))
                {
                    db.deleteAccount(member);
                    Toast.makeText(getApplicationContext(), "Order closed ", Toast.LENGTH_LONG).show();
                    finish();
                    Intent i = new Intent(Ot_Acc_List.this, CloseOrderdetails.class);
                    i.putExtra("pageValue", 0);
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("tableNo",table);
                    startActivity(i);
                }
                else
                {
                    //newwd to remove
                    Intent i = new Intent(TakeOrder.this, CloseOrderdetails.class);
                    i.putExtra("pageValue", 0);
                    startActivity(i);
                    Toast.makeText(getApplicationContext(),"Order not closed-->"+s, Toast.LENGTH_LONG).show();
                }
            }
            else if(pageType.equals("1"))
            {
                if (s.contains("true"))
                {
                    String memeberAnumber = memberAn.getText().toString();
                    finish();
                    Intent i = new Intent(TakeOrder.this, CloseOrderdetails.class);
                    i.putExtra("pageValue", 1);
                    i.putExtra("memeberAnumber", memeberAnumber);
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("tableNo",table);
                    i.putExtra("cardType",cardType);
                    startActivity(i);
                }
                else
                {
                    //newwd to remove
                    Intent i = new Intent(TakeOrder.this, CloseOrderdetails.class);
                    i.putExtra("pageValue", 1);
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("tableNo",table);
                    i.putExtra("cardType",cardType);
                    startActivity(i);

                }
            }*/


        }
    }
}
