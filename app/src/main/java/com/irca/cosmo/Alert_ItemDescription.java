package com.irca.cosmo;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.irca.adapter.ModifiersAdapter;
import com.irca.adapter.ModifiersSelectedAdapter;
import com.irca.db.Dbase;
import com.irca.dto.ModifiersDto;
import com.irca.fields.OnRecyclerViewItemClickListener;


import java.util.ArrayList;
import java.util.List;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by Archanna on 11/28/2016.
 */
public class Alert_ItemDescription extends AppCompatActivity {
    Dbase db;
    List<ModifiersDto> description = new ArrayList<>();
    List<ModifiersDto> description_details = new ArrayList<>();
    List<ModifiersDto> modifiersSelectedList = new ArrayList<>();
    Button add, button, button1;

    String dummy = "";
    Bundle b;
    String itemName = "", itemCode = "";
    String text = "";

    EditText etxt_free_narrateion;
    RecyclerView recyclerView, recyclerView1, recyclerView2;
    ModifiersAdapter modifiersAdapter;
    ModifiersAdapter modifierDetailsAdapter;
    ModifiersSelectedAdapter modifiersSelectedAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   Fabric.with(this, new Crashlytics());
        setContentView(R.layout.alert_item_description);
        db = new Dbase(this);
        b = getIntent().getExtras();
        itemName = b.getString("itemName");
        itemCode = b.getString("itemCode");
        add = findViewById(R.id.add);
        button = findViewById(R.id.button);
        button1 = findViewById(R.id.button1);
//        mTagGroup = findViewById(R.id.tag_group_beauty);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView1 = findViewById(R.id.recycler_view1);
        recyclerView2 = findViewById(R.id.recycler_view2);
        etxt_free_narrateion = findViewById(R.id.narr);
        description = db.getModifier();
        description_details = db.getModifierDetails();

        if (description.size() == 0) {
            Toast.makeText(this, "Modifiers not available", Toast.LENGTH_SHORT).show();
        } else {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(linearLayoutManager);
            modifiersAdapter = new ModifiersAdapter(description, this);
            recyclerView.setAdapter(modifiersAdapter);
            modifiersAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener<ModifiersAdapter.MultiSelectDialogViewHolder, ModifiersDto>() {
                @Override
                public void onRecyclerViewItemClick(@NonNull ModifiersAdapter.MultiSelectDialogViewHolder multiSelectDialogViewHolder, @NonNull View view, @NonNull ModifiersDto modifiersDto, int position) {
                    try {
                        for (int i = 0; i < description.size(); i++) {
                            if (modifiersDto.getModifierId().equals(description.get(i).getModifierId())) {
                                description.get(i).setSelected(true);
                            } else {
                                description.get(i).setSelected(false);
                            }
                        }
                        modifiersAdapter.notifyDataSetChanged();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        if (description_details.size() == 0) {
            Toast.makeText(this, "Modifier details not available", Toast.LENGTH_SHORT).show();
        } else {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            recyclerView1.setLayoutManager(linearLayoutManager);
            modifierDetailsAdapter = new ModifiersAdapter(description_details, this);
            recyclerView1.setAdapter(modifierDetailsAdapter);
            modifierDetailsAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener<ModifiersAdapter.MultiSelectDialogViewHolder, ModifiersDto>() {
                @Override
                public void onRecyclerViewItemClick(@NonNull ModifiersAdapter.MultiSelectDialogViewHolder multiSelectDialogViewHolder, @NonNull View view, @NonNull ModifiersDto modifiersDto, int position) {
                    try {
                        for (int i = 0; i < description_details.size(); i++) {
                            if (modifiersDto.getModifierId().equals(description_details.get(i).getModifierId())) {
                                description_details.get(i).setSelected(true);
                            } else {
                                description_details.get(i).setSelected(false);
                            }
                        }
                        modifierDetailsAdapter.notifyDataSetChanged();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView2.setLayoutManager(linearLayoutManager);
        modifiersSelectedAdapter = new ModifiersSelectedAdapter(modifiersSelectedList, this);
        recyclerView2.setAdapter(modifiersSelectedAdapter);
        modifiersSelectedAdapter.notifyDataSetChanged();


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etxt_free_narrateion.getText().toString().trim().isEmpty()) {
                    Toast.makeText(Alert_ItemDescription.this, "Please enter Free Flow", Toast.LENGTH_SHORT).show();
                } else {
                    for (int i = 0; i < modifiersSelectedList.size(); i++) {
                        if (modifiersSelectedList.get(i).getFreeFlow().equalsIgnoreCase(etxt_free_narrateion.getText().toString().trim())) {
                            Toast.makeText(Alert_ItemDescription.this, "Free Flow already added.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    ModifiersDto modifiersDto = new ModifiersDto();
                    modifiersDto.setModifierId("0");
                    modifiersDto.setModifierDetailId("0");
                    modifiersDto.setModifierName("");
                    modifiersDto.setModifierDetailName("");
                    modifiersDto.setFreeFlow(etxt_free_narrateion.getText().toString().trim());
                    modifiersSelectedList.add(modifiersDto);
                    modifiersSelectedAdapter.notifyDataSetChanged();
                    etxt_free_narrateion.setText("");
                    Toast.makeText(Alert_ItemDescription.this, "Modifiers Added", Toast.LENGTH_SHORT).show();

                }
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ModifiersDto modifiersDto1 = null;
                ModifiersDto modifierDetailsDto = null;
                for (int i = 0; i < description.size(); i++) {
                    if (description.get(i).isSelected()) {
                        modifiersDto1 = description.get(i);
                    }
                }
                for (int i = 0; i < description_details.size(); i++) {
                    if (description_details.get(i).isSelected()) {
                        modifierDetailsDto = description_details.get(i);
                    }
                }
                if (modifiersDto1 == null) {
                    Toast.makeText(Alert_ItemDescription.this, "Please select Modifier.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (modifierDetailsDto == null) {
                    Toast.makeText(Alert_ItemDescription.this, "Please select Modifier Details", Toast.LENGTH_SHORT).show();
                    return;
                }
                for (int i = 0; i < modifiersSelectedList.size(); i++) {
                    if (modifiersSelectedList.get(i).getModifierId().equals(modifiersDto1.getModifierId()) && modifiersSelectedList.get(i).getModifierDetailId().equals(modifierDetailsDto.getModifierId())) {
                        Toast.makeText(Alert_ItemDescription.this, "Modifier already added.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                ModifiersDto modifiersDto = new ModifiersDto();
                modifiersDto.setModifierId(modifiersDto1.getModifierId());
                modifiersDto.setModifierName(modifiersDto1.getModifierName());
                modifiersDto.setModifierDetailId(modifierDetailsDto.getModifierId());
                modifiersDto.setModifierDetailName(modifierDetailsDto.getModifierName());
                modifiersDto.setFreeFlow("");
                modifiersSelectedList.add(modifiersDto);
                modifiersSelectedAdapter.notifyDataSetChanged();
                Toast.makeText(Alert_ItemDescription.this, "Modifiers Added", Toast.LENGTH_SHORT).show();

            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (modifiersSelectedList.size() > 0) {
                    db.deleteModsId(itemCode);
                    int count = 0;
                    for (int i = 0; i < modifiersSelectedList.size(); i++) {
                        count = count + db.insertMods(modifiersSelectedList.get(i), itemCode, itemName);
                    }
                    if (count > 0) {
                        Toast.makeText(Alert_ItemDescription.this, "Modifiers updated", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Alert_ItemDescription.this, "Modifiers not updated", Toast.LENGTH_SHORT).show();
                    }
                    finish();
                } else {
                    Toast.makeText(Alert_ItemDescription.this, "Please add Modifiers", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}
