package com.irca.cosmo;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.acs.audiojack.AudioJackReader;
import com.acs.audiojack.Result;
//import com.crashlytics.android.Crashlytics;
import com.irca.Billing.Accountlist;
import com.irca.Billing.BillingProfile;
import com.irca.Billing.MainActivity;
import com.irca.MaterialFloatLabel.SearchableSpinner;
import com.irca.Utils.ShowMsg;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.db.Dbase;
import com.irca.fields.CloseorderItems;
import com.irca.fields.WaiterDetails;
import com.irca.widgets.FloatingActionButton.FloatingActionButton;
import com.irca.widgets.FloatingActionButton.FloatingActionsMenu;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/*
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
*/

/**
 * Created by Manoch Richard on 10/19/2015.
 */
public class
TakeOrder extends AppCompatActivity {

    SearchableSpinner searchItemname;
    public static String isCashCardFilled = "";
    // TextView tableNO;
    FloatingActionButton bot;
    EditText memberAn;
    ImageView takeOrder;
    Button corder;
    FloatingActionButton cancelOrder;
    Button order;
    ImageView backround;
    ArrayAdapter arrayAdapterr;
    SharedPreferences sharedpreferences;
    public static String c_Limit = "";
    List<String> Blist;

    // int table = 0;
    String table = "0";
    ProgressDialog pd;

    int tableId = 0;
    //ImageView takeOrder,closeOrder;
    FloatingActionsMenu fabmenu;

    //reader
    // private boolean mPiccAtrReady;//poer on method

    private boolean mResultReady;//power
    private AudioManager mAudioManager;
    private AudioJackReader mReader;
    private Context mContext = this;
    private ProgressDialog mProgress;

    private boolean mPiccResponseApduReady;// transmit to check apdu return values
    private Object mResponseEvent = null;

    public String finalValue = "";
    public String waiterid;
    public static String waiternamecode;
    public int manoch = 0;
    public int overAll = 0;
    public int k = 0;
    public int position;
    public int positionb;
    public String itemValue;
    //    public String daniel = "FF 82 00 00 06 FF FF FF FF FF FF," +
//                           "FF 86 00 00 05 01 00 04 60 00," +
//                           "FF B0 00 04 10," +
//                           "FF 86 00 00 05 01 00 05 60 00," +
//                           "FF B0 00 05 10,"+
//                           "FF 86 00 00 05 01 00 06 60 00," +
//                           "FF B0 00 06 10 ";
    //public  String daniel1="FF 86 00 00 05 01 00 05 60 00,FF B0 00 05 10 ";
    //public  String daniel2="FF 86 00 00 05 01 00 05 60 00,FF B0 00 06 10 ";
    public String seriadaniel = "FF CA 00 00 00,";
    private int mPiccTimeout = 1;
    private int mPiccCardType = 143;
    private byte[] mPiccCommandApdu;// transmit

    private byte[] mPiccResponseApdu;
    private byte[] mPiccRfConfig = new byte[19];
    private Result mResult;
    public static String cardType = "", member = "", Steward = "";
    public static ArrayList<CloseorderItems> closeorderItemsArrayList = new ArrayList<CloseorderItems>();
    public static ArrayList<WaiterDetails> waiterlist1 = new ArrayList<WaiterDetails>();
    public static ArrayList<CloseorderItems> closeorderItemsArrayList1 = new ArrayList<CloseorderItems>();
    Dbase db;

    String creditAccountno = "";

    String creditName = "";
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    //TextView title;
    String storeName;
    String pos;

    String cLimit = "";
    String mode = "";
    String bMode = "";
    String[] payments;
    String[] _BillMode;
    ArrayList<String> paymentMode;
    ArrayList<String> _billMode;

    //  ImageView logo;
    // Bitmap bitmap;

    Spinner sp_cardType;
    TextView colon;
    ArrayAdapter<String> spinnerAdapter;
    // String[] _cards =new String[ ]{"Select card Type","MEMBER CARD","MEMBER DUPL CARD","TEMP CARD","ROOM CARD","NEW ROOM CARD","CASH CARD","CLUB CARD","DEPENDENT CARD"};
    String[] _cards = new String[]{"MEMBER CARD"};
    Boolean isReadCard = false;
    String selectedCard = "";
    String posid = "";
    String billType = "";
    RelativeLayout obstrucuterView;
    AudioManager audio;
    String contractorsId = "";
    String bill = "";
    String billMode = "";
    String _new = "";
    String bMod = "";
    public static String check = "";
    //public  String pakistan="30 24 31 36 31 36 24 31 30 33 31 33 31 34 32 36 90 00 39 24 41 2D 30 30 30 33 24 68 48 51 47 2B 7A 46 90 00";
    private final BroadcastReceiver mHeadsetPlugReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {

                boolean plugged = (intent.getIntExtra("state", 0) == 1);

                /* Mute the audio output if the reader is unplugged. */
                mReader.setMute(!plugged);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   Fabric.with(this, new Crashlytics());
        setContentView(R.layout.take_order_new);
        initPrefrence();
        actionBarSetup();
        // object creation
        db = new Dbase(TakeOrder.this);
        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audio.setStreamVolume(AudioManager.STREAM_MUSIC, audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        obstrucuterView = (RelativeLayout) findViewById(R.id.obstructor);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mReader = new AudioJackReader(mAudioManager, true);
        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();

        fabmenu = (FloatingActionsMenu) findViewById(R.id.fab_menu);
        colon = (TextView) findViewById(R.id.colon);
        sp_cardType = (Spinner) findViewById(R.id.cardtype);
        searchItemname = (SearchableSpinner) findViewById(R.id.spinnerfiler);
        backround = (ImageView) findViewById(R.id.bg_image);
        corder = (Button) findViewById(R.id.corder);    // Button to close the order
        memberAn = (EditText) findViewById(R.id.editText_m1);   // to display the reader card

        order = (Button) findViewById(R.id.order);   // button lead to take order
        // tableNO = (TextView) findViewById(R.id.tableNo);   //display table No
        bot = (FloatingActionButton) findViewById(R.id.bot);


        bot.setEnabled(true);
        memberAn.setEnabled(false);
        sp_cardType.setVisibility(View.GONE);
        colon.setVisibility(View.GONE);

        spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, _cards);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_cardType.setAdapter(spinnerAdapter);

        Blist = db.getWaiterList();
        arrayAdapterr = new ArrayAdapter<String>(TakeOrder.this, R.layout.support_simple_spinner_dropdown_item, Blist);
        searchItemname.setAdapter(arrayAdapterr);
        searchItemname.setLongClickable(false);
        // searchItemname.setOnSearchTextChangedListener();
        searchItemname.setEnabled(true);

        try {
            searchItemname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    //  searchItemname.setEnabled(false);
                    position = i;
                    waiternamecode = adapterView.getItemAtPosition(i).toString();
                    String array[] = waiternamecode.split("_");
                    waiterid = db.getWaiterId(array[0].trim());
                    View v = TakeOrder.this.getCurrentFocus();
                    if (view != null && position != 0) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    searchItemname.setEnabled(true);
                }
            });
        } catch (Exception e) {
            ShowMsg.showException(e, "setLogSettings", getApplicationContext());
        }



        /* Register the headset plug receiver. */
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(mHeadsetPlugReceiver, filter);

        mProgress = new ProgressDialog(mContext);
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);


        String piccRfConfigString = "";
        if ((piccRfConfigString == null) || piccRfConfigString.equals("")
                || (toByteArray(piccRfConfigString, mPiccRfConfig) != 19)) {

            piccRfConfigString = "07 85 85 85 85 85 85 85 85 69 69 69 69 69 69 69 69 3F 3F";
            toByteArray(piccRfConfigString, mPiccRfConfig);
        }
        piccRfConfigString = toHexString(mPiccRfConfig);

         /* Set the PICC response APDU callback. */
        mReader.setOnPiccResponseApduAvailableListener(new OnPiccResponseApduAvailableListener());


        Bundle b = getIntent().getExtras();
        table = b.getString("tableNo");
        //  tableNO.setText("Table  " + table);
        tableId = b.getInt("tableId");
        cardType = b.getString("cardType");
        member = b.getString("accountType");
        Steward = b.getString("Steward");
        positionb = b.getInt("position");
        bMod = b.getString("billMode");
        if (member.contains("#")) {
            _new = "old";

            String[] mm = member.split("#");
            contractorsId = (cardType.equals("DEPENDENT CARD")) ? mm[2] : "";
            memberAn.setText(mm[0] + ":" + cardType);

            order.setVisibility(View.VISIBLE);
            corder.setVisibility(View.VISIBLE);  //TODO newly added
            sp_cardType.setVisibility(View.GONE);
            colon.setVisibility(View.GONE);
            searchItemname.setSelection(positionb);
            searchItemname.setSelection(arrayAdapterr.getPosition(Steward));
            searchItemname.setTitle(Steward);


            memberAn.setEnabled(false);
            memberAn.setFocusable(false);
            memberAn.setInputType(InputType.TYPE_NULL);

            //isReadCard=MainActivity.isSmartCard;
            isReadCard = sharedpreferences.getBoolean("CardRead", false);
            String isreader = sharedpreferences.getString("misReader", "");
            fabmenu.setVisibility(View.VISIBLE);

            if (isReadCard) {
                bot.setEnabled(true);
            } else {
                bot.setEnabled(false);
            }

            bot.setEnabled(false);
            //Toast.makeText(TakeOrder.this,"member:#",Toast.LENGTH_LONG).show();

        } else {
            // Toast.makeText(TakeOrder.this,"member:nohash",Toast.LENGTH_LONG).show();
            _new = "new";
            memberAn.setText("");
            fabmenu.setVisibility(View.GONE);
            if (memberAn.getText().toString().equals("")) {
                order.setVisibility(View.GONE);
            }
            isReadCard = MainActivity.isSmartCard;
            if (isReadCard) {
                String isreader = sharedpreferences.getString("misReader", "");
                if (isreader.equals("False")) {
                    noCardReader();
                    isReadCard = false;
                } else {
                    readCard();
                }

            } else {
                noCardReader();
            }

        }

        try {

            if (pos.equals("KH-KITCHEN")) {
                backround.setImageResource(R.drawable.hotel2);
            } else if (pos.equals("KITCHEN")) {
                backround.setImageResource(R.drawable.hotel2);
            } else if (pos.equals("BAR")) {
                backround.setImageResource(R.drawable.glasstable);
            } else if (pos.equals("ASHWIN ENTERPRISE")) {
                backround.setImageResource(R.drawable.hotel);
            } else if (pos.equals("BAKERY")) {
                backround.setImageResource(R.drawable.hotel5);
            } else if (pos.equals("KH Bar")) {
                backround.setImageResource(R.drawable.bar_bg);
            }

        } catch (Exception e) {
            String r = e.getMessage().toString();
        }


        // memberAn.setText("5942:DEPENDENT CARD");
        // cardType = "DEPENDENT CARD";
        //order.setVisibility(View.VISIBLE);
        //memberAn.setText("SR70:MEMBER CARD");
        //  memberAn.setText("PS44:MEMBER CARD");
        //   cardType="MEMBER CARD";
        //   order.setVisibility(View.VISIBLE);
        //   _new="new";

        // memberAn.setText("1269:ROOM CARD");
        //  cardType = "ROOM CARD";
        // order.setVisibility(View.VISIBLE);

        //  memberAn.setText("0043:MEMBER DUPL CARD");
        // cardType="MEMBER DUPL CARD";
        //order.setVisibility(View.VISIBLE);

        //  memberAn.setText("51199:CASH CARD");
        //  cardType="CASH CARD";
        //  order.setVisibility(View.VISIBLE);

        //   memberAn.setText("1483684397:CLUB CARD");
        //   cardType="CLUB CARD";
        //  order.setVisibility(View.VISIBLE);

//
//           memberAn.setText("2510:SMART CARD");
//           cardType="SMART CARD";
//           order.setVisibility(View.VISIBLE);//


        obstrucuterView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (obstrucuterView.getVisibility() == View.VISIBLE) {
                    fabmenu.collapse();
                    return true;
                }
                return false;
            }
        });

        fabmenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                if (obstrucuterView.getVisibility() == View.GONE)
                    obstrucuterView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuCollapsed() {
                if (obstrucuterView.getVisibility() == View.VISIBLE)
                    obstrucuterView.setVisibility(View.GONE);
            }
        });


        // *****************************  TAKE OREDER   BUTTON ******************************//


        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String memeberAnumber = memberAn.getText().toString();
                isInternetPresent = cd.isConnectingToInternet();
                String waiter = searchItemname.getSelectedItem().toString();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                if (_new.equals("new")) {
                    // new card need to choose the billmode
                    if (waiter.equals("Select Steward")) {
                        Toast.makeText(getApplicationContext(), "Please select Steward", Toast.LENGTH_SHORT).show();
                    } else if (isReadCard) {
                        if (memeberAnumber.equals("")) {
                            Toast.makeText(TakeOrder.this, "Read Member Card", Toast.LENGTH_LONG).show();
                        } else {
                            String ss[] = memeberAnumber.split(":");
                            editor.putString("ct", ss[1]);
                            editor.commit();
                            if (member.contains("#"))
                            {
                                String ss1[] = member.split("#");
                                if (isInternetPresent) {
                                    // new AsyncCreditLimit().execute(ss1[2]);
                                    AlertBillmode(ss1[2], ss[1]);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                if (isInternetPresent) {
                                      new AsyncCreditLimit().execute(ss[0]);
                                    //AlertBillmode(ss[0], ss[1]);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }

                            }

                        }
                    } else {

                        if (memeberAnumber.equals("")) {
                            Toast.makeText(TakeOrder.this, "Enter Member account Number !", Toast.LENGTH_LONG).show();
                        } else if (selectedCard.contains("Select")) {
                            Toast.makeText(TakeOrder.this, "Please choose the cardtype !", Toast.LENGTH_LONG).show();
                        } else {
                            String mem = memeberAnumber + ":" + selectedCard;

                            String ss[] = mem.split(":");
                            editor.putString("ct", ss[1]);
                            editor.commit();
                            String ss2[];

                            cardType = ss[1];

                            if (member.contains("#")) {

                                String ss1[] = member.split("#");

                                if (isInternetPresent) {
                                    // new AsyncCreditLimit().execute(ss1[2]);
                                    AlertBillmode(ss1[2], selectedCard);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                if (isInternetPresent) {
                                    //new AsyncCreditLimit().execute(ss[0]);
                                    AlertBillmode(ss[0], selectedCard);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }

                            }

                        }
                    }

                } else {
                    // old Card so direct method run
                    if (waiter.equals("Select Steward")) {
                        Toast.makeText(getApplicationContext(), "Please select Steward", Toast.LENGTH_SHORT).show();
                    } else if (isReadCard) {
                        if (memeberAnumber.equals("")) {
                            Toast.makeText(TakeOrder.this, "Read Member Card", Toast.LENGTH_LONG).show();
                        } else {

                            String ss[] = memeberAnumber.split(":");

                            editor.putString("ct", ss[1]);
                            editor.commit();
                            if (member.contains("#")) {
                                String ss1[] = member.split("#");
                                if (isInternetPresent) {
                                    new AsyncCreditLimit().execute(ss1[2], bMod);
                                    //  AlertBillmode(ss1[2],ss[1]);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                if (isInternetPresent) {
                                    new AsyncCreditLimit().execute(ss[0], bMod);
                                    // AlertBillmode(ss[0],ss[1]);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }

                            }

                        }
                    } else {

                        if (memeberAnumber.equals("")) {
                            Toast.makeText(TakeOrder.this, "Enter Member account Number !", Toast.LENGTH_LONG).show();
                        } else if (selectedCard.contains("Select")) {
                            Toast.makeText(TakeOrder.this, "Please choose the cardtype !", Toast.LENGTH_LONG).show();
                        } else {
                            String mem = memeberAnumber + ":" + selectedCard;

                            String ss[] = mem.split(":");
                            editor.putString("ct", ss[1]);
                            editor.commit();
                            String ss2[];

                            cardType = ss[1];

                            if (member.contains("#")) {

                                String ss1[] = member.split("#");

                                if (isInternetPresent) {
                                    new AsyncCreditLimit().execute(ss1[2], bMod);
                                    // AlertBillmode(ss1[2],selectedCard);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                if (isInternetPresent) {
                                    new AsyncCreditLimit().execute(ss[0], bMod);
                                    // AlertBillmode(ss[0],selectedCard);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                                }

                            }

                        }
                    }

                }


            }
        });


        //**************************** CARD READER BUTTON  **********************************//

        //  takeOrder.setOnClickListener(new View.OnClickListener() {
        bot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audio.setStreamVolume(
                        AudioManager.STREAM_MUSIC,
                        audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                        0);
                mReader.piccPowerOff();
                mReader.sleep();
                finalValue = "";
                manoch = 0;
                k = 0;
                memberAn.setText("");
                mPiccResponseApdu = null;
                mResponseEvent = null;
                mReader.start();
                mResponseEvent = new Object();

                overAll = 0;
                if (!checkResetVolume()) {
                } else {
                    mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    mProgress.setMessage("Reading Card please wait.. ");
                    mProgress.show();
                    new Thread(new Transmit()).start();
                }
            }
        });


        //********************************CLOSE ORDER BUTTON **********************************//
        corder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String memeberAnumber = memberAn.getText().toString();
                isInternetPresent = cd.isConnectingToInternet();
                if (memeberAnumber.equals("")) {
                    Toast.makeText(TakeOrder.this, "Enter member account Number", Toast.LENGTH_LONG).show();
                } else {
                    String ss[] = memeberAnumber.split(":");
                    if (member.contains("#")) {
                        String ss1[] = member.split("#");
                        // Toast.makeText(TakeOrder.this,""+ss1[1],Toast.LENGTH_LONG).show();
                        if (isInternetPresent) {

                            AlertPayment(ss1[1], "0", ss[1], isCashCardFilled);

                            //  new AsyncCloseOrder().execute(ss1[1], "0");
                        } else {
                            Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        // Toast.makeText(TakeOrder.this,""+ss[0],Toast.LENGTH_LONG).show();
                        if (isInternetPresent) {
                            AlertPayment(ss[0], "0", ss[1], isCashCardFilled);

                            //new AsyncCloseOrder().execute(ss[0], "0");
                        } else {
                            Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                        }

                    }

                }
                //bottomSheet.setVisibility(View.VISIBLE);
                // bottomSheet.setMinimumHeight(100);
                // bottomSheet.showWithSheetView(LayoutInflater.from(TakeOrder.this).inflate(R.layout.sheet, bottomSheet, false));
            }
        });

        // *******************************  CANCEL ORDRE (FAB BUTTON )***************************//

      /*  FloatingActionButton otReprint = new FloatingActionButton(this);
        otReprint.setIcon(R.drawable.report2);
        otReprint.setTitle("OT Reprint");

        otReprint.setColorNormal(Color.parseColor("#e75b1e"));
        fabmenu.addButton(otReprint);

        otReprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                fabmenu.collapse();
                Intent i = new Intent(TakeOrder.this, Ot_Acc_List.class);
                i.putExtra("mpage","1");
                startActivity(i);
            }
        });
*/
        if (!billType.equals("BAK") || billType.equals("")) {
            FloatingActionButton otReprint = new FloatingActionButton(this);
            otReprint.setIcon(R.drawable.report2);
            otReprint.setTitle("OT Reprint");
            otReprint.setColorNormal(Color.parseColor("#e75b1e"));
            fabmenu.addButton(otReprint);

            otReprint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fabmenu.collapse();
                   /* Intent i = new Intent(TakeOrder.this, Ot_Acc_List.class);
                    i.putExtra("mpage","1");
                    startActivity(i);*/

                    String memeberAnumber = memberAn.getText().toString();
                    isInternetPresent = cd.isConnectingToInternet();
                    if (memeberAnumber.equals("")) {
                        Toast.makeText(TakeOrder.this, "Enter member account Number", Toast.LENGTH_LONG).show();
                    } else {
                        String ss[] = memeberAnumber.split(":");
                        if (member.contains("#")) {
                            String ss1[] = member.split("#");
                            if (isInternetPresent) {
                                new AsyncCloseOrder1().execute(ss1[1], "1", "");
                            } else {
                                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            // Toast.makeText(TakeOrder.this,""+ss[0],Toast.LENGTH_LONG).show();
                            if (isInternetPresent) {
                                new AsyncCloseOrder1().execute(ss[0], "1", "");
                            } else {
                                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            });
        }


//        if (cardType.equals("")) {
//
//        }
//        else if (billType.equals("BOT")){
        FloatingActionButton cancelOrder = new FloatingActionButton(this);
        cancelOrder.setIcon(R.drawable.return_purchase);
        cancelOrder.setTitle("Cancel Order");
        cancelOrder.setColorNormal(Color.parseColor("#e75b1e"));
        fabmenu.addButton(cancelOrder);

//            cancelOrder.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String memeberAnumber = memberAn.getText().toString();
//                    isInternetPresent = cd.isConnectingToInternet();
//                    if (memeberAnumber.equals("")) {
//                        Toast.makeText(TakeOrder.this, "Enter member account Number", Toast.LENGTH_LONG).show();
//                    } else {
//                        String ss[] = memeberAnumber.split(":");
//                        if (member.contains("#")) {
//                            String ss1[] = member.split("#");
//                            //  Toast.makeText(TakeOrder.this,""+ss1[1],Toast.LENGTH_LONG).show();
//                            if (isInternetPresent) {
//                                new AsyncCloseOrder().execute(ss1[1], "1", bMod);
//                            } else {
//                                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
//                            }
//
//                        } else {
//                            // Toast.makeText(TakeOrder.this,""+ss[0],Toast.LENGTH_LONG).show();
//                            if (isInternetPresent) {
//                                new AsyncCloseOrder().execute(ss[0], "1", bMod);
//                            } else {
//                                Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//                }
//            });
        // }

        //order.setVisibility(View.VISIBLE);
        //memberAn.setText("B291AF01"+":READ CARD");

    }

    private void initPrefrence() {

        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        pos = sharedpreferences.getString("StoreName", "");
        billType = sharedpreferences.getString("billType", "");
        posid = sharedpreferences.getString("storeId", "");
        cLimit = sharedpreferences.getString("creditLimit", "");
        bill = sharedpreferences.getString("Bill", "");
        billMode = sharedpreferences.getString("BillMode", "");

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup() {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar ab = getSupportActionBar();
            ab.setTitle("" + pos + "  -  " + bill);
            ab.setElevation(0);
            ab.setDisplayHomeAsUpEnabled(true);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        String Dashboard = sharedpreferences.getString("Dashboard", "");
        if (Dashboard.equals("")) {
            Dashboard = "Dashboard_new";
        }
        Intent i = new Intent(TakeOrder.this, Accountlist.class);
        i.putExtra("waiterid", waiterid);
        i.putExtra("position", position);
        // i.putExtra("from","Dashboard_new");
        i.putExtra("from", Dashboard);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                String Dashboard = sharedpreferences.getString("Dashboard", "");
                if (Dashboard.equals("")) {
                    Dashboard = "Dashboard_new";
                }
                Intent i = new Intent(TakeOrder.this, Accountlist.class);
                i.putExtra("waiterid", waiterid);
                i.putExtra("position", position);
                //i.putExtra("from","Dashboard_new");
                i.putExtra("from", Dashboard);
                startActivity(i);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void noCardReader() {
        sp_cardType.setVisibility(View.VISIBLE);
        colon.setVisibility(View.VISIBLE);
        bot.setEnabled(false);
        memberAn.setEnabled(true);
        // memberAn.setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL);
        order.setVisibility(View.VISIBLE);
        corder.setVisibility(View.GONE);

        sp_cardType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCard = spinnerAdapter.getItem(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void readCard() {
        sp_cardType.setVisibility(View.GONE);
        colon.setVisibility(View.GONE);
        bot.setEnabled(true);
        memberAn.setEnabled(false);
        memberAn.setInputType(InputType.TYPE_NULL);
        corder.setVisibility(View.GONE);
    }
    private String AlertBillmode1(final String Account, String cardType) {
        _billMode = new ArrayList<String>();
        if (bill.equals("Regular")) {

            if (cardType.equals("MEMBER CARD") || cardType.equals("CLUB CARD") || cardType.equals("ROOM CARD") || cardType.equals("DEPENDENT CARD") || cardType.equals("MEMBER DUPL CARD") || cardType.equals("SMART CARD")) {

                //new AsyncCreditLimit().execute(Account,"Account");
                return "Account";

            } else if (cardType.equals("CASH CARD")) {

                if (MainActivity.cashCard.equals("true")) {
                    // new AsyncCreditLimit().execute(Account,"Account");
                    return "Account";
                } else {
                    //new AsyncCreditLimit().execute(Account,"Cash");
                    return "Cash";
                }
            }
        } else if (bill.equals("Party Billing")) {
            if (cardType.equals("MEMBER CARD")) {
                //new AsyncCreditLimit().execute(Account,"Account");
                return "Account";
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Party Billing ", Toast.LENGTH_SHORT).show();
            }
        } else if (bill.equals("Direct Party Billing")) {
            if (cardType.equals("MEMBER CARD")) {
                //new AsyncCreditLimit().execute(Account,"Account");
                return "Account";
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Direct Party Billing ", Toast.LENGTH_SHORT).show();
            }
        } else if (bill.equals("Compliment")) {
            if (cardType.equals("MEMBER CARD") || cardType.equals("ROOM CARD")) {
                //new AsyncCreditLimit().execute(Account,"Complimentary");
                return "Complimentary";
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Compliment Billing ", Toast.LENGTH_SHORT).show();
            }

        } else if (bill.equals("coupon")) {
            if (cardType.equals("MEMBER CARD")) {
                //new AsyncCreditLimit().execute(Account,"Coupon");
                return "Coupon";
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Coupon Billing ", Toast.LENGTH_SHORT).show();
            }
        }

        return "";

    }

    private void AlertBillmode(final String Account, String cardType) {
        _billMode = new ArrayList<String>();
        if (bill.equals("Regular")) {
            if (cardType.equals("MEMBER CARD") || cardType.equals("CLUB CARD") || cardType.equals("ROOM CARD") || cardType.equals("DEPENDENT CARD") || cardType.equals("MEMBER DUPL CARD") || cardType.equals("SMART CARD")) {
                new AsyncCreditLimit().execute(Account, "Account");
            } else if (cardType.equals("CASH CARD")) {

                if (MainActivity.cashCard.equals("true")) {
                    new AsyncCreditLimit().execute(Account, "Account");
                } else {
                    new AsyncCreditLimit().execute(Account, "Cash");
                }
            }
        } else if (bill.equals("Party Billing")) {
            if (cardType.equals("MEMBER CARD")) {
                new AsyncCreditLimit().execute(Account, "Account");
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Party Billing ", Toast.LENGTH_SHORT).show();
            }
        } else if (bill.equals("Direct Party Billing")) {
            if (cardType.equals("MEMBER CARD")) {
                new AsyncCreditLimit().execute(Account, "Account");
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Direct Party Billing ", Toast.LENGTH_SHORT).show();
            }
        } else if (bill.equals("Compliment")) {
            if (cardType.equals("MEMBER CARD") || cardType.equals("ROOM CARD")) {
                new AsyncCreditLimit().execute(Account, "Complimentary");
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Compliment Billing ", Toast.LENGTH_SHORT).show();
            }

        } else if (bill.equals("coupon")) {
            if (cardType.equals("MEMBER CARD")) {
                new AsyncCreditLimit().execute(Account, "Coupon");
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Coupon Billing ", Toast.LENGTH_SHORT).show();
            }
        }


    }

    // payment mode for close order
    private void AlertPayment(final String s, String s1, String cardType, String isCashCardFilled) {

        paymentMode = new ArrayList<>();
        if (bill.equals("Regular")) {
            switch (cardType) {

                case "MEMBER CARD":

                    paymentMode.add("Account");
                    paymentMode.add("Cash");
                    paymentMode.add("Credit");

                    break;

                case "CASH CARD":

                    if (MainActivity.cashCard.equals("true")) {
                        paymentMode.add("Account");
                    } else {
                        paymentMode.add("Cash");
                        paymentMode.add("Credit");
                    }

                    break;

                case "CLUB CARD":

                    paymentMode.add("Account");

                    break;


                case "ROOM CARD":

                    paymentMode.add("Account");
                    //   paymentMode.add("Complimentary");

                    break;

                case "DEPENDENT CARD":

                    paymentMode.add("Account");
                    paymentMode.add("Cash");
                    paymentMode.add("Credit");

                    break;
                case "MEMBER DUPL CARD":

                    paymentMode.add("Account");
                    paymentMode.add("Cash");
                    paymentMode.add("Credit");

                    break;

                case "SMART CARD":

                    paymentMode.add("Account");

                    break;
            }


            if (!paymentMode.isEmpty()) {

                payments = new String[paymentMode.size()];
                payments = paymentMode.toArray(payments);

            }

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Select the Payment Mode");
            builder.setItems(payments, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int _paymentMode) {

                    // mode=payments[paymentMode];
                    mode = paymentMode.get(_paymentMode);

                    new AsyncCloseOrder().execute(s, "0", mode);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();


        } else if (bill.equals("Party Billing")) {
            if (cardType.equals("PARTY CARD")) {
                new AsyncCloseOrder().execute(s, "0", "Account");
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Party Billing ", Toast.LENGTH_SHORT).show();
            }
        } else if (bill.equals("Direct Party Billing")) {
            if (cardType.equals("DIRECTPARTY CARD")) {
                new AsyncCloseOrder().execute(s, "0", "Account");
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Direct Party Billing ", Toast.LENGTH_SHORT).show();
            }
        } else if (bill.equals("Compliment")) {
            if (cardType.equals("MEMBER CARD") || cardType.equals("ROOM CARD")) {
                new AsyncCloseOrder().execute(s, "0", "Complimentary");
            } else {
                Toast.makeText(mContext, "This card is Not applicable for Compliment Billing ", Toast.LENGTH_SHORT).show();
            }

        } else if (bill.equals("coupon")) {
            if (cardType.equals("MEMBER CARD")) {
                new AsyncCloseOrder().execute(s, "0", "Coupon");

            } else {
                Toast.makeText(this, "This card is not applicable for coupon ", Toast.LENGTH_SHORT).show();
            }


        }
    }

    // *****************  FOR CLOSE AND CANCEL ORDER ***************************//

    protected class AsyncCloseOrder extends AsyncTask<String, Void, String> {
        String status = "";
        String storeId = sharedpreferences.getString("storeId", "");
        String deviceid = sharedpreferences.getString("deviceId", "");
        String userId = sharedpreferences.getString("userId", "");
        String pageType = "";
        String mstoreid = sharedpreferences.getString("mstoreId", "");
        JSONObject jsonObject;
        String billtype = sharedpreferences.getString("BillMode", "");

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(TakeOrder.this);
            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                closeorderItemsArrayList.clear();

                if (bill.equals("coupon")) {
                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, "COUPON", pos, params[2], contractorsId, mstoreid, billtype,0,"P",waiterid);
                } else if (bill.equals("Party Billing")) {
                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, "PARTY CARD", pos, params[2], contractorsId, mstoreid, billtype,0,"P",waiterid);
                } else if (bill.equals("Direct Party Billing")) {
                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, "DIRECTPARTY CARD", pos, params[2], contractorsId, mstoreid, billtype,0,"P",waiterid);
                } else {
                    jsonObject = api.cms_closeOrder(params[0], deviceid, storeId, formattedDate, Integer.parseInt(params[1]), userId, cardType, pos, params[2], contractorsId, mstoreid, billtype,0,"P",waiterid);
                }

                JSONArray array = jsonObject.getJSONArray("Value");
                CloseorderItems cl = null;
                pageType = params[1];
                for (int i = 0; i < array.length(); i++) {
                    cl = new CloseorderItems();
                    JSONObject object = array.getJSONObject(i);

                    String ItemID = object.getString("ItemID");
                    String ItemCode = object.getString("ItemCode");
                    String Quantity = object.getString("Quantity");
                    String Amount = object.getString("Amount");
                    String BillID = object.getString("BillID");
                    String taxDescription = object.optString("taxDescription");
                    String mAcc = object.getString("memberAcc");
                    String taxValue = object.getString("taxValue");
                    String ava_balance = object.optString("avBalance");
                    String billNo = object.optString("billnumber");
                    String taxAmount = object.optString("taxAmount");
                    String Rate = object.getString("rate");
                    String itemname = object.getString("itemname");
                    String mName = object.getString("membername");
                    String memberId = object.getString("memberId");
                    String BillDate = object.getString("BillDate");
                    String ACCharge = object.getString("ACCharge");

                    String otNo = object.getString("otno");
                    String opBalance = db.getOpeningBalance(mAcc);

                    cl.setItemID(ItemID);
                    cl.setItemCode(ItemCode);
                    cl.setQuantity(Quantity);
                    cl.setAmount(Amount);
                    cl.setBillID(BillID);
                    cl.setTaxDescription(taxDescription);
                    cl.setmAcc(mAcc);
                    cl.setTaxValue(taxValue);
                    cl.setAva_balance(ava_balance);
                    cl.setBillno(billNo);
                    cl.setTaxAmount(taxAmount);
                    cl.setmName(mName);
                    cl.setMemberID(memberId);
                    cl.setBillDate(BillDate);
                    cl.setRate(Rate);
                    cl.setItemname(itemname);
                    cl.setOtno(otNo);
                    cl.setCardType(params[2]);
                    cl.setACCharge(ACCharge);
                    cl.setOpeningBalance(opBalance);

                    closeorderItemsArrayList.add(cl);
                }
                status = jsonObject.toString();

            } catch (Exception e) {
                e.printStackTrace();
                status = e.toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(TakeOrder.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            pd.dismiss();
            if (pageType.equals("0"))  //close order
            {
                if (s.contains("true")) {
                    if (closeorderItemsArrayList.size() == 0 || closeorderItemsArrayList == null) {
                        // db.deleteAccount(member,posid,userId);
                        db.deleteAccount_new(member, posid, userId, bill);
                        //noCardReader();
                        readCard();
                        Toast.makeText(getApplicationContext(), "Order not taken for the member", Toast.LENGTH_LONG).show();
                    } else {
                        //db.deleteAccount(member,posid,userId);
                        db.deleteAccount_new(member, posid, userId, bill);
                        Toast.makeText(getApplicationContext(), "Order closed ", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(TakeOrder.this, CloseOrderdetails.class);

                        i.putExtra("pageValue", 0);
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("creditName", creditName);
                        i.putExtra("tableNo", table);
                        i.putExtra("tableId", tableId);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("cardType", cardType);
                        i.putExtra("waiternamecode", waiternamecode);
                        startActivity(i);
                        finish();
                    }

                } else {

                    Toast.makeText(getApplicationContext(), "Order not closed-->" + s, Toast.LENGTH_LONG).show();
                }
            } else if (pageType.equals("1"))  //cancel order
            {
                if (s.contains("true")) {
                    if (closeorderItemsArrayList.size() == 0 || closeorderItemsArrayList == null) {
                        Toast.makeText(getApplicationContext(), "Order not taken for the member", Toast.LENGTH_LONG).show();
                    } else {
                        String memeberAnumber = memberAn.getText().toString();
                        Intent i = new Intent(TakeOrder.this, CloseOrderdetails.class);
                        i.putExtra("pageValue", 1);
                        i.putExtra("memeberAnumber", memeberAnumber);
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("creditName", creditName);
                        i.putExtra("tableNo", table);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableId", tableId);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("waiternamecode", waiternamecode);
                        //   i.putExtra("position",position);
                        //  i.putExtra("waiternamecode",waiternamecode);
                        startActivity(i);
                        finish();
                    }


                } else {

                    Toast.makeText(getApplicationContext(), "No Item to Cancel-->" + s, Toast.LENGTH_LONG).show();

                }
            }


        }
    }

    private void refreshViewofOt() {
        searchItemname.setAdapter(arrayAdapterr);
    }


    //**************************** for OT reprint *************************//

    protected class AsyncCloseOrder1 extends AsyncTask<String, Void, String> {
        String status = "";
        String storeId = sharedpreferences.getString("storeId", "");
        String deviceid = sharedpreferences.getString("deviceId", "");
        String userId = sharedpreferences.getString("userId", "");
        String pageType = "";
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(TakeOrder.this);
            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                closeorderItemsArrayList1.clear();
                //JSONObject jsonObject=api.cms_closeOrder(params[0], deviceid, storeId, formattedDate,Integer.parseInt(params[1]),userId,"","","");
                //JSONObject jsonObject=api.cms_getOTReprint(params[0], deviceid, storeId, formattedDate, userId, cardType, "",contractorsId);

                //if(bill.equals("coupon")){
                //     jsonObject=api.cms_getOTReprint(params[0], storeId, formattedDate, userId);
                //  }else {
                // jsonObject=api.cms_getOTReprint(params[0], storeId, formattedDate, userId);
                //  }
                if (bill.equals("coupon")) {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "COUPON", billMode);

                } else if (bill.equals("Party Billing")) {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "PARTY CARD", billMode);

                } else if (bill.equals("Direct Party Billing")) {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, "DIRECTPARTY CARD", billMode);

                } else {
                    jsonObject = api.cms_getOTReprint(params[0], storeId, formattedDate, userId, cardType, billMode);
                }


                JSONArray array = jsonObject.getJSONArray("Value");
                CloseorderItems cl = null;
                pageType = params[1];
                for (int i = 0; i < array.length(); i++) {
                    cl = new CloseorderItems();
                    JSONObject object = array.getJSONObject(i);
                    String ItemID = object.optString("ItemID");
                    String ItemCode = object.optString("ItemCode");
                    String Quantity = object.optString("Quantity");
                    String Amount = object.optString("Amount");
                    String BillID = object.optString("BillID");
                    String mAcc = object.getString("memberAcc");
                    String billNo = object.optString("billnumber");
                    String Rate = object.getString("rate");
                    String itemname = object.getString("itemname");
                    String mName = object.getString("membername");
                    String memberId = object.getString("memberId");
                    String BillDate = object.getString("BillDate");
                    String otNo = object.getString("otno");

                    cl.setItemID(ItemID);
                    cl.setItemCode(ItemCode);
                    cl.setQuantity(Quantity);
                    cl.setAmount(Amount);
                    cl.setBillID(BillID);
                    cl.setmAcc(mAcc);
                    cl.setBillno(billNo);
                    cl.setmName(mName);
                    cl.setMemberID(memberId);
                    cl.setBillDate(BillDate);
                    cl.setRate(Rate);
                    cl.setItemname(itemname);
                    cl.setOtno(otNo);
                    cl.setCardType(cardType);

                    closeorderItemsArrayList1.add(cl);
                }
                status = jsonObject.toString();

            } catch (Exception e) {
                e.printStackTrace();
                status = e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(TakeOrder.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (closeorderItemsArrayList1.size() > 0) {
                Intent i = new Intent(TakeOrder.this, OT_Reprint.class);
                i.putExtra("type","takeorder");
                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(), "Please Place OT for the Member", Toast.LENGTH_LONG).show();
            }


        }
    }


    // ******************************* TAKE ORDER ***************************//

    protected class AsyncCreditLimit extends AsyncTask<String, Void, String> {

        ArrayList<String> credit = null;
        String creditLimit = "";
        String memberId = "";
        String previousbal = "";

        String _params = "";
        String formattedDate = "";
        String userId = sharedpreferences.getString("userId", "");
        String storeId = sharedpreferences.getString("storeId", "");
        JSONObject jsonObject;

        String bmod = "";
        String bookingNumber = "";
        String bookingId = "";
        double ava_balance = 0;
        String cl = "0";
        String membertype = "";
        String TempBillAmount = "0.0";
        @Override
        protected String doInBackground(String... params) {
            RestAPI api = new RestAPI(TakeOrder.this);
            try {
                // _params=(cardType.equals("SMART CARD")) ? "0":params[0];
                _params = params[0];
                //bmod = params[1];
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                formattedDate = df.format(c.getTime());
                if (bill.equals("coupon")) {
                    jsonObject = api.cms_creditCheck(params[0], formattedDate, "COUPON",posid);
                } else if (bill.equals("Party Billing")) {
                    jsonObject = api.cms_creditCheck(params[0], formattedDate, "PARTY CARD",posid);
                    cardType = "PARTY CARD";
                } else if (bill.equals("Direct Party Billing")) {
                    jsonObject = api.cms_creditCheck(params[0], formattedDate, "DIRECTPARTY CARD",posid);
                    cardType = "DIRECTPARTY CARD";
                }
                else if(params.length==2)
                {
                    jsonObject = api.cms_creditCheck(params[0], formattedDate, cardType,posid);
                    //jsonObject = api.cms_creditCheckV2(params[0],bill);
                }
                else {
                    //jsonObject = api.cms_creditCheck(params[0], formattedDate, cardType);
                    jsonObject = api.cms_creditCheckV2(params[0],bill,posid);
                }


                JSONArray jsonArray = jsonObject.getJSONArray("Value");
                credit = new ArrayList<>();
                check = "";

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    cl = jsonObject1.optString("availablebalance").toString();
                    c_Limit = jsonObject1.optString("creditLimit");

                    creditAccountno = jsonObject1.optString("AccountNumber");
                    creditName = jsonObject1.optString("MemberName");
                    memberId = jsonObject1.optString("memberID");
                    previousbal = jsonObject1.optString("previousCreditLimit");
                    check = jsonObject1.optString("isCLRFTM");
                    BillingProfile.check=jsonObject1.optString("isCLRFTM");// nfc in make order required
                    isCashCardFilled = jsonObject1.optString("isCashCardFilled");
                    bookingNumber = jsonObject1.optString("BookingNumber");
                    bookingId = jsonObject1.optString("BookingID");
                    ava_balance = Double.parseDouble((cl.equals("") ? "0" : cl));
                    membertype = jsonObject1.optString("MemberType");
                    membertype = jsonObject1.optString("MemberType");
                    TempBillAmount = jsonObject1.optString("TempBillAmount");
                    if(params.length!=2)
                    {
                        cardType = jsonObject1.optString("cardtype");
                    }
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ct", cardType);
                    editor.commit();
                    //   String   usedbalance=jsonObject1.get("usedbalance").toString();
                    //  double usedB=Double.parseDouble(usedbalance);
                    String limit = (c_Limit.equals("")) ? "0" : c_Limit;
                    //String limit = (cLimit.equals("")) ? "0" : cLimit;
                    if (!previousbal.equals("")) {
                        creditLimit = "0";
                    } else if (memberId.equals("AA")) {
                        creditLimit = "";
                    } else if (cardType.equals("CLUB CARD")) {
                        creditLimit = ava_balance + "";
                    } else if (bill.equals("coupon")) {
                        creditLimit = "coupon";
                    } else {
                        if (check.equals("")) {


                            if (cardType.equals("CASH CARD")) {

                                //  TODo need to validate for the cash card filled  (if  filled allow below line to validate or allow directly  inside to take order )
                                if (isCashCardFilled.equalsIgnoreCase("false")) {
                                    creditLimit = ava_balance + "";
                                } else {
                                    if (ava_balance > 0) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        creditLimit = "0";
                                    }
                                }

                            } else if (cardType.equals("SMART CARD")) {
                                if (ava_balance > 0) {
                                    creditLimit = ava_balance + "";
                                } else {
                                    creditLimit = "0";
                                }
                            } else     //if (!cardType.equals("CASH CARD") || !cardType.equals("SMART CARD"))
                            {
                                // if (ava_balance == -0 || (25000 - ava_balance) > 0) {
                                if (ava_balance == -0 || (Double.parseDouble(limit) - ava_balance) > 0) {
                                    creditLimit = ava_balance + "";
                                } else {
                                    creditLimit = "0";
                                }
                            }

                        } else {
                            if (check.equalsIgnoreCase("False")) {
                                creditLimit = ava_balance + "";
                            } else {
                                if (cardType.equals("CASH CARD")) {

                                    //  TODo need to validate for the cash card filled  (if  filled allow below line to validate or allow directly  inside to take order )
                                    if (isCashCardFilled.equalsIgnoreCase("false")) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        if (ava_balance > 0) {
                                            creditLimit = ava_balance + "";
                                        } else {
                                            creditLimit = "0";
                                        }
                                    }
                                } else if (cardType.equals("SMART CARD")) {
                                    if (ava_balance > 0) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        creditLimit = "0";
                                    }
                                }
                                // else  if (!cardType.equals("CASH CARD") || !cardType.equals("SMART CARD"))
                                else {
                                    // if (ava_balance == -0 || (25000 - ava_balance) > 0) {
                                    if (ava_balance == -0 || (Double.parseDouble(limit) - ava_balance) > 0) {
                                        creditLimit = ava_balance + "";
                                    } else {
                                        creditLimit = "0";
                                    }
                                }
                            }
                        }

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return creditLimit;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(TakeOrder.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (!s.equals("")) {
                if (s.equals("0")) {
                    if (cardType.equals("CASH CARD") && isCashCardFilled.equals("true")) {
                        //Toast.makeText(getApplicationContext(), "  cash card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        showMessage("  cash card limit exceeded ,Can't do billing", getApplicationContext());
                    } else if (cardType.equals("MEMBER CARD")) {
                        // Toast.makeText(getApplicationContext(), "  Member card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        showMessage(" Member card limit exceeded ,Can't do billing", getApplicationContext());
                    } else if (cardType.equals("SMART CARD")) {
                        //Toast.makeText(getApplicationContext(), "  SMART card limit exceeded ,Can't do billing", Toast.LENGTH_LONG).show();
                        showMessage("  SMART card limit exceeded ,Can't do billing", getApplicationContext());
                    } else if (cardType.equals("DEPENDENT CARD")) {
                        showMessage("DEPENDENT card limit exceeded ,Can't do billing", getApplicationContext());
                    } else {
                        //Toast.makeText(getApplicationContext(), "No account found  >" + s + "  " + _params +jsonObject, Toast.LENGTH_LONG).show();
                        showMessage(" No account found  >" + s + "  " + _params + jsonObject, getApplicationContext());
                    }

                } else if (bill.equals("Party Billing")) {
                    bmod = AlertBillmode1(creditAccountno, cardType);
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params+ "#" + membertype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno,"0","");
                    member = creditAccountno + "#" + memberId + "#" + _params;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.commit();
                    Intent i = new Intent(TakeOrder.this, Search_Item.class);
                    //Intent i = new Intent(TakeOrder.this, Itemview.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("tableNo", table);
                    i.putExtra("creditLimit", bookingNumber);           // coupon no credit check in place order  so given -0to place order without validating for credit limit
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("memberId", memberId);
                    i.putExtra("tableId", tableId);
                    i.putExtra("cardType", cardType);
                    i.putExtra("referenceNo", bookingId);
                    i.putExtra("waiterid", waiterid);
                    i.putExtra("position", position);
                    i.putExtra("waiternamecode", waiternamecode);
                    i.putExtra("billmode", bmod);
                    i.putExtra("dob", "0");
                    i.putExtra("doa", "0");
                    i.putExtra("TempBillAmount", TempBillAmount);
                    startActivity(i);
                    db.deleteOT();
                    finish();

                } else if (s.equals("coupon")) {  //coupon billing no credit check required
                    bmod = AlertBillmode1(creditAccountno, cardType);
                    String billType = sharedpreferences.getString("billType", "");
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params+ "#" + membertype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno,"0","");
                    member = creditAccountno + "#" + memberId + "#" + _params;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.commit();

                    if (billType.equals("BAK")) {
                        Intent i = new Intent(TakeOrder.this, Bakery.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("GroupName", "");
                        i.putExtra("name", creditName);
                        i.putExtra("accountNo", creditAccountno);
                        i.putExtra("credit", creditLimit);
                        i.putExtra("groupId", "");
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableNo", table);
                        i.putExtra("referenceNo", _params);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);
                        startActivity(i);
                        finish();
                    } else {

                        Intent i = new Intent(TakeOrder.this, Search_Item.class);
                        //Intent i = new Intent(TakeOrder.this, Itemview.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("tableNo", table);
                        i.putExtra("creditLimit", "-0");           // coupon no credit check in place order  so given -0to place order without validating for credit limit
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("creditName", creditName);
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("referenceNo", _params);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);
                        i.putExtra("dob", "0");
                        i.putExtra("doa", "0");
                        i.putExtra("TempBillAmount", TempBillAmount);
                        startActivity(i);
                        db.deleteOT();
                        finish();

                    }

                } else {
                    bmod = AlertBillmode1(creditAccountno, cardType);
                    String billType = sharedpreferences.getString("billType", "");
                    db.insertAccount(userId, storeId, creditAccountno + "#" + memberId + "#" + _params+ "#" + membertype, formattedDate, cardType, Double.toString(ava_balance), waiternamecode, creditName, bill, bmod, creditAccountno,"","");
                    member = creditAccountno + "#" + memberId + "#" + _params;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ot_close", member);
                    editor.putString("isCashCardFilled", isCashCardFilled);
                    editor.commit();
                    if (billType.equals("BAK")) {
                        Intent i = new Intent(TakeOrder.this, Bakery.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("GroupName", "");
                        i.putExtra("name", creditName);
                        i.putExtra("accountNo", creditAccountno);
                        i.putExtra("credit", creditLimit);
                        i.putExtra("groupId", "");
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("tableNo", table);
                        i.putExtra("referenceNo", _params);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);
                        startActivity(i);
                        finish();
                    } else {

                        Intent i = new Intent(TakeOrder.this, Search_Item.class);
                        //Intent i = new Intent(TakeOrder.this, Itemview.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("tableNo", table);
                        i.putExtra("creditLimit", creditLimit);
                        i.putExtra("creditAno", creditAccountno);
                        i.putExtra("creditName", creditName);
                        i.putExtra("memberId", memberId);
                        i.putExtra("tableId", tableId);
                        i.putExtra("cardType", cardType);
                        i.putExtra("referenceNo", _params);
                        i.putExtra("waiterid", waiterid);
                        i.putExtra("position", position);
                        i.putExtra("waiternamecode", waiternamecode);
                        i.putExtra("billmode", bmod);
                        i.putExtra("doa", "0");
                        i.putExtra("dob", "0");
                        i.putExtra("TempBillAmount", TempBillAmount);
                        startActivity(i);
                        db.deleteOT();
                        finish();
                             /* Intent i = new Intent(TakeOrder.this, KotItemlist.class);
                              i.putExtra("tableNo", table);
                              i.putExtra("creditLimit", creditLimit);
                              i.putExtra("creditAno", creditAccountno);
                              i.putExtra("creditName", creditName);
                              i.putExtra("memberId", memberId);
                              i.putExtra("tableId", tableId);
                              i.putExtra("cardType", cardType);
                              i.putExtra("referenceNo", _params);
                              startActivity(i);
                              finish();*/
                    }
                }
            } else {
                if (memberId.equals("AA")) {
                    //Toast.makeText(getApplicationContext(), " Sorry Member is already in another POS ", Toast.LENGTH_LONG).show();
                    showMessage("Sorry Member is already in another POS" + jsonObject, getApplicationContext());
                } else if (cardType.equals("ROOM CARD")) {
                    //Toast.makeText(getApplicationContext(), " Room card is checked out ,Can't do billing", Toast.LENGTH_LONG).show();
                    showMessage("Room card is checked out ,Can't do billing" + jsonObject, getApplicationContext());
                } else if (cardType.equals("NEW ROOM CARD")) {
                    //Toast.makeText(getApplicationContext(), "New Room card is checked out ,Can't do billing", Toast.LENGTH_LONG).show();
                    showMessage("New Room card is checked out ,Can't do billing" + jsonObject, getApplicationContext());
                } else if (cardType.equals("CASH CARD")) {
                    //Toast.makeText(getApplicationContext(), "cash card expired ,Can't do billing", Toast.LENGTH_LONG).show();
                    showMessage("cash card expired ,Can't do billing" + jsonObject, getApplicationContext());

                } else if (cardType.equals("SMART CARD")) {
                    //Toast.makeText(getApplicationContext(), "Smart card expired ,Can't do billing", Toast.LENGTH_LONG).show();
                    showMessage("Smart card expired ,Can't do billing" + jsonObject, getApplicationContext());
                } else {
                    //Toast.makeText(getApplicationContext(), "No account found-->" + s + _params +jsonObject, Toast.LENGTH_LONG).show();
                    showMessage("No account found-->" + s + _params + jsonObject, getApplicationContext());
                    //TODO remove
                    /* Intent i = new Intent(TakeOrder.this, Search_Item.class);
                    i.putExtra("tableNo", table);
                    i.putExtra("creditLimit", creditLimit);
                    i.putExtra("creditAno", creditAccountno);
                    i.putExtra("creditName", creditName);
                    i.putExtra("memberId", memberId);
                    i.putExtra("tableId", tableId);
                    i.putExtra("cardType", cardType);
                    i.putExtra("referenceNo", _params);
                    startActivity(i);
                    db.deleteOT();
                    finish();*/
                }
            }

        }
    }

    public class Transmit implements Runnable {

        @Override
        public void run() {

            mReader.reset();
            if (!mReader.piccPowerOn(5, 143))
            {
                //showRequestQueueError();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "Reader is not available", Toast.LENGTH_LONG).show();
                        mProgress.dismiss();
                    }
                });


            }
            else
            {
                      /* Transmit the command APDU. */
                // while (manoch <= 4) {
                while (manoch <= 0)
                {
                    mPiccResponseApduReady = false;
                    mResultReady = false;
                    String cmd[] = null;
                    cmd = seriadaniel.split(",");
                    mPiccCommandApdu = toByteArray(cmd[k]);
                    if (!mReader.piccTransmit(mPiccTimeout, mPiccCommandApdu))
                    {
                                 /* Show the request queue error. */
                        //showRequestQueueError();
                        Toast.makeText(mContext, "Read again ", Toast.LENGTH_LONG).show();

                    }
                    else
                    {

                                /* Show the PICC response APDU. */
                        //showPiccResponseApdu();
                        synchronized (mResponseEvent) {
                                 /* Wait for the PICC response APDU. */
                            while (!mPiccResponseApduReady && !mResultReady)
                            {

                                try
                                {
                                    mResponseEvent.wait(500);
                                }
                                catch (InterruptedException e)
                                {
                                }
                                break;
                            }

                            if (mPiccResponseApduReady)
                            {
                                runOnUiThread(new Runnable()
                                {

                                    @Override
                                    public void run()
                                    {
                                                /* Show the PICC response APDU. */
                                        String current = toHexString(mPiccResponseApdu);
                                        if (current.contains("90 00"))
                                        {
                                            current=current.replaceAll("\\s+", "");
                                            memberAn.setText(current.substring(0,current.length()-4)+":CARD READ");
                                            manoch++;
                                            overAll++;
                                            order.setVisibility(View.VISIBLE);
                                            mProgress.dismiss();
                                        }
                                        else
                                        {
                                            mReader.piccPowerOn(5, 143);
                                            overAll++;

                                        }
                                    }
                                });

                            }
                            else if (mResultReady)
                            {

                                runOnUiThread(new Runnable()
                                {

                                    @Override
                                    public void run()
                                    {
                                                /* Show the result. */
                                        Toast.makeText(mContext,
                                                toErrorCodeString(mResult.getErrorCode()),
                                                Toast.LENGTH_LONG).show();
                                        mProgress.dismiss();
                                    }
                                });

                            }
                            else
                            {
                                runOnUiThread(new Runnable()
                                {

                                    @Override
                                    public void run()
                                    {
                                        // finalValue=finalValue+"-11";
                                                 /* Show the timeout. */
                                        overAll++;
                                        mReader.piccPowerOn(5, 143);
                                        if (overAll > 8)
                                        {
                                            Toast.makeText(mContext, "The operation timed out Read again ", Toast.LENGTH_LONG).show();
                                            mReader.piccPowerOff();
                                            mReader.sleep();
                                        }
                                    }
                                });
                            }

                            mPiccResponseApduReady = false;
                            mResultReady = false;
                        }
                    }

                    if (overAll > 8)
                    {
                        mReader.piccPowerOff();
                        mReader.sleep();
                        mProgress.dismiss();
                        break;
                    }
                }
                        /* Hide the progress. */
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mProgress.dismiss();
                    }

                });
            }
        }


    }

    private class OnPiccResponseApduAvailableListener implements AudioJackReader.OnPiccResponseApduAvailableListener {

        @Override
        public void onPiccResponseApduAvailable(AudioJackReader reader,
                                                byte[] responseApdu) {

            synchronized (mResponseEvent) {
                /* Store the PICC response APDU. */
                mPiccResponseApdu = new byte[responseApdu.length];
                System.arraycopy(responseApdu, 0, mPiccResponseApdu, 0,
                        responseApdu.length);
                /* Trigger the response event. */
                mPiccResponseApduReady = true;
                mResponseEvent.notifyAll();
            }
        }
    }

    @Override
    protected void onDestroy() {
        /* Unregister the headset plug receiver. */
        unregisterReceiver(mHeadsetPlugReceiver);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mReader.start();
        } catch (Exception e) {
//            String ss = e.getMessage().toString();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            mReader.start();
        } catch (Exception e) {

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            mReader.stop();
            mProgress.dismiss();
        } catch (Exception e) {

        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            mProgress.dismiss();
            mReader.stop();
        } catch (Exception e) {

        }

    }

    private String toHexString(byte[] buffer) {
        String bufferString = "";
        if (buffer != null) {

            for (int i = 0; i < buffer.length; i++) {
                String hexChar = Integer.toHexString(buffer[i] & 0xFF);
                if (hexChar.length() == 1) {
                    hexChar = "0" + hexChar;
                }
                bufferString += hexChar.toUpperCase(Locale.US) + " ";
            }
        }

        return bufferString;
    }

    private boolean checkResetVolume() {

        boolean ret = true;

        int currentVolume = mAudioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);

        int maxVolume = mAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        if (currentVolume < maxVolume) {

            showMessageDialog(R.string.info, R.string.message_reset_info_volume);
            ret = false;
        }

        return ret;
    }

    private void showMessageDialog(int titleId, int messageId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setMessage(messageId)
                .setTitle(titleId)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });

        builder.show();
    }

    private String toErrorCodeString(int errorCode) {

        String errorCodeString = null;

        switch (errorCode) {
            case Result.ERROR_SUCCESS:
                errorCodeString = "The operation completed successfully.";
                break;
            case Result.ERROR_INVALID_COMMAND:
                errorCodeString = "The command is invalid.";
                break;
            case Result.ERROR_INVALID_PARAMETER:
                errorCodeString = "The parameter is invalid.";
                break;
            case Result.ERROR_INVALID_CHECKSUM:
                errorCodeString = "The checksum is invalid.";
                break;
            case Result.ERROR_INVALID_START_BYTE:
                errorCodeString = "The start byte is invalid.";
                break;
            case Result.ERROR_UNKNOWN:
                errorCodeString = "The error is unknown.";
                break;
            case Result.ERROR_DUKPT_OPERATION_CEASED:
                errorCodeString = "The DUKPT operation is ceased.";
                break;
            case Result.ERROR_DUKPT_DATA_CORRUPTED:
                errorCodeString = "The DUKPT data is corrupted.";
                break;
            case Result.ERROR_FLASH_DATA_CORRUPTED:
                errorCodeString = "The flash data is corrupted.";
                break;
            case Result.ERROR_VERIFICATION_FAILED:
                errorCodeString = "The verification is failed.";
                break;
            case Result.ERROR_PICC_NO_CARD:
                errorCodeString = "No card in PICC slot.";
                break;
            default:
                errorCodeString = "Error communicating with reader.";
                break;
        }

        return errorCodeString;
    }

    private int toByteArray(String hexString, byte[] byteArray) {

        char c = 0;
        boolean first = true;
        int length = 0;
        int value = 0;
        int i = 0;

        for (i = 0; i < hexString.length(); i++) {

            c = hexString.charAt(i);
            if ((c >= '0') && (c <= '9')) {
                value = c - '0';
            } else if ((c >= 'A') && (c <= 'F')) {
                value = c - 'A' + 10;
            } else if ((c >= 'a') && (c <= 'f')) {
                value = c - 'a' + 10;
            } else {
                value = -1;
            }

            if (value >= 0) {

                if (first) {

                    byteArray[length] = (byte) (value << 4);

                } else {

                    byteArray[length] |= value;
                    length++;
                }

                first = !first;
            }

            if (length >= byteArray.length) {
                break;
            }
        }

        return length;
    }

    private byte[] toByteArray(String hexString) {

        byte[] byteArray = null;
        int count = 0;
        char c = 0;
        int i = 0;

        boolean first = true;
        int length = 0;
        int value = 0;

        // Count number of hex characters
        for (i = 0; i < hexString.length(); i++) {

            c = hexString.charAt(i);
            if (c >= '0' && c <= '9' || c >= 'A' && c <= 'F' || c >= 'a'
                    && c <= 'f') {
                count++;
            }
        }

        byteArray = new byte[(count + 1) / 2];
        for (i = 0; i < hexString.length(); i++) {

            c = hexString.charAt(i);
            if (c >= '0' && c <= '9') {
                value = c - '0';
            } else if (c >= 'A' && c <= 'F') {
                value = c - 'A' + 10;
            } else if (c >= 'a' && c <= 'f') {
                value = c - 'a' + 10;
            } else {
                value = -1;
            }

            if (value >= 0) {

                if (first) {

                    byteArray[length] = (byte) (value << 4);

                } else {

                    byteArray[length] |= value;
                    length++;
                }

                first = !first;
            }
        }

        return byteArray;
    }

    public String hexToString(String hex1) {
        StringBuilder sb = new StringBuilder();
        String hex2 = hex1.replaceAll("00", "");
        String hex = hex2.replaceAll("90", "");
        char[] hexData = hex.toCharArray();
        for (int count = 0; count < hexData.length - 1; count += 2) {
            int firstDigit = Character.digit(hexData[count], 16);
            int lastDigit = Character.digit(hexData[count + 1], 16);
            int decimal = firstDigit * 16 + lastDigit;
            sb.append((char) decimal);
        }
        return sb.toString();
    }

    private void showMessage(String text, Context c) {
        Toast toast = Toast.makeText(c, "message", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 10, 50);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setText(text);
        toast.show();
    }
}
