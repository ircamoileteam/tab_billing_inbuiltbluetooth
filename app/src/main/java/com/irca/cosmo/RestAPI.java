/* JSON API for android appliation */
package com.irca.cosmo;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.irca.Utils.Constants;
import com.irca.dto.SendModifiers;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

public class RestAPI {
    Context c;
    SharedPreferences sharedpreferences;

// bgc

    //private final String urlString = "http://192.168.1.11/MOBILE_BILLING/HANDLER1.ASHX";
    // private final String urlString = "http://122.166.127.10/MOBILE_BILLING/HANDLER1.ASHX";

    //  ************************      BCI   ****************************//
    //private final String urlString = "http://192.168.1.5/mobile_billing_BCI/handler1.ashx";
    //private final String urlString = "http://122.166.105.234/mobile_billing_BCI/handler1.ashx";
    //private final String urlString = "http://122.166.206.247/mobile_billing_BCI/handler1.ashx";
    // private final String urlString = "http://192.168.0.160/mobile_billing_BCI/handler1.ashx";
    // private final String urlString = "http://192.168.0.160/mobile_billing_bci_test/handler1.ashx";
    //   private final String urlString = "http://122.166.105.234/mobile_billing_bci/handler1.ashx";

    //*******************************  Mangalore club **************************//

    //   private final String urlString = "http://122.166.105.234/mobile_billing_mangalore/handler1.ashx";
    // private final String urlString = "http://122.166.105.234/mobile_billing_mangalore/handler1.ashx";
    //LIVE
    // private final String urlString = "http://202.12.80.43/mobile_billing_mangalore/handler1.ashx";

    //*******************************  CENTURY **************************//
    // private final String urlString = "http://122.166.105.234/mobile_billing_century/handler1.ashx";

    //private final String urlString = "http://192.168.1.11/mobile_billing_century/handler1.ashx";   // century ip  // not
    //private final String urlString = "http://192.168.10.5/mobile_billing_century/handler1.ashx";   // century ip wifi ip  // not

    //private final String urlString = "http://192.168.10.7/mobile_billing_century/handler1.ashx";   // century ip wifi ip   // Live ip    // live ip

    // private final String urlString = "http://192.168.1.13/mobile_billing_century/handler1.ashx";   // century ip wifi ip  // not

    //private final String urlString = "http://1.22.230.173/mobile_century_billing/handler1.ashx";   // century ip wifi ip  //not

    //private final String urlString = "http://117.198.96.156/mobile_billing_centuryv2/handler1.ashx";   // century ip static ip
//192.168.0.222
    //private final String urlString = "http://192.168.10.7/mobile_billing_centuryv2/handler1.ashx";   // century ip static ip // not
    //    private final String urlString = "http://1.22.230.173/mobile_billing_NSCI/handler1.ashx";
    //  private final String urlString ="http://1.22.230.173:82/mobile_tabbilling_nsci/handler1.ashx";

    // private final String urlString = "http://1.22.230.173:3455/mobile_tabbilling_ooty/handler1.ashx";
    // private final String urlString = "http://113.193.227.78:2956/mobile_billing_ooty/handler1.ashx";

    //   private final String urlString = "http://1.22.230.173:2956/mobile_billing_demo/handler1.ashx";

    //   private final String urlString = "http://110.172.174.236/mobile_billing_ooty/handler1.ashx";
    //testing     private final String urlString = "http://110.172.174.236/mobile_billing_ooty/handler1.ashx";
    //   private final String urlString = "http://192.168.0.238/mobile_billing_ooty/handler1.ashx";

    //      private final String urlString = "http://183.87.220.172/mobile_billing_pune/handler1.ashx";
    //      private final String urlString = "http://192.168.0.222/mobile_billing_pune/handler1.ashx";
    //  /  private final String urlString = "http://192.168.1.116/mobile_billing_NSCI_development/handler1.ashx";
    //    private final String urlString = "http://202.131.96.146:8081/mobile_billing_gujarat/handler1.ashx";172.16.1.250
    //  private final String urlString = "http://172.16.1.250/mobile_billing_gujarat/handler1.ashx";
    //   private final String urlString = "http://110.172.174.236/mobile_billing_ooty/handler1.ashx";

    //private final String urlString = "http://192.168.10.7/mobile_billing_century_test/handler1.ashx";   // century ip static ip // not
    //**********************    Indranagar    ****************************//
    //private final String urlString = "http://117.218.52.180/mobile_billing_inc/handler1.ashx";   // Indra nagar ip wifi ip   // Live ip
    // private final String urlString = "http://192.168.1.191/mobile_billing_inc/handler1.ashx";   // Indra nagar ip wifi ip   // Live ip

    //  private final String urlString = "http://192.168.0.238/mobile_billing_ooty/handler1.ashx";  // Ooty club live link (local)
    //  private final String urlString = "http://110.172.174.236/mobile_billing_ooty/handler1.ashx";
    // Ooty club live link (global) //todo is pending in BillingProfile
//   private final String urlString = "http://113.193.227.78:2956/mobile_billing_ooty/handler1.ashx";  // Ooty club 116 test instance (global)
//   private final String urlString = "http://122.166.204.10/mobile_billing_jpnagar/handler1.ashx";  // jp nagar
//   private final String urlString = "http://192.168.1.102/mobile_billing_jpnagar/handler1.ashx";  // jp nagar local

    // private final String urlString = "http://110.172.174.236/mobile_billing_30052019/handler1.ashx";  // OOTY CLUB test link (global)
    // private final String urlString = "http://122.166.223.240/mobile_tab_billing/handler1.ashx";  //
//    private final String urlString = "http://122.166.154.63/mobile_tab_billing/handler1.ashx";  //
//    private final String urlString = "http://110.172.174.236/mobile_billing_ooty/handler1.ashx";  //LIVE GLOBAL OOTY  5-2-2020
//    private final String urlString = "http://122.166.154.101/mobile_tab_recca/handler1.ashx";  //RECCA TEST
//    private final String urlString = "http://172.19.101.60/mobile_billing_recca/handler1.ashx";  //RECCA LIVE LOCAL NEW
//    private final String urlString = "http://beldihtabbilling.ircacms.in/handler1.ashx";  //live  beldhi


    // JP Nagar
    //  private final String urlString = "http://113.193.227.78:2956/mobile_billing_jpnagar/handler1.ashx";


    /////////////////////////////// 2020 URLS ///////////////////////////////////////////
//    private final String urlString = "http://110.172.174.236/mobile_test_ooty_2020/handler1.ashx";  //live global ooty
//    private final String urlString = "http://192.168.0.238/mobile_test_ooty_2020/handler1.ashx";  //live local ooty

//    private final String urlString = "http://110.172.174.236/mobile_billing_ooty_2020/handler1.ashx";  //live global ooty
//    private final String urlString = "http://192.168.0.238/mobile_billing_ooty_2020/handler1.ashx";  //live local ooty


    ///****************************  OOTY CLUB ************************////////////////
//    private final String urlString = "http://192.168.0.238/mobile_billing_vss_2020/handler1.ashx";  //ooty local live
//    private final String urlString = "http://110.172.174.236/mobile_billing_vss_2020/handler1.ashx";  //ooty global live
//       private final String urlString = "http://122.166.154.101/mobile_billing_ooty/handler1.ashx";  //TEST OOTY  5-2-2020

//  private final String urlString = "http://117.248.111.39:81/mobile_billing_boat/handler1.ashx";  //BOAT
//  private final String urlString = "http://bayclubtab.ircacms.in/handler1.ashx";  //BOAT
//      private final String urlString = "http://117.248.111.39:81/mobile_billing_boat/handler1.ashx";  //BOAT
    //   private final String urlString = "http://bayclubtab.ircacms.in/handler1.ashx";

    //      ****************************  EPGC CLUB ************************////////////////
//  private final String urlString = "http://103.140.155.67/mobile_epgc_tab_billing/handler1.ashx";  // EPGC TEST
// private final String urlString = "http://ctabbill.epgc.in/handler1.ashx";                       // EPGC LIVE
//    private static  String urlString = "http://122.166.154.101/mobile_epgc_billing/handler1.ashx";  // EPGC
    private String urlString;  // EPGC

    public RestAPI(Context context) {
        c = context;
        sharedpreferences = c.getSharedPreferences("URLS", Context.MODE_PRIVATE);
        String url = sharedpreferences.getString("url", "");
        urlString = url;//Live
    }

//    private final String urlString = "http://103.57.150.35/mobile_billing_tmc/handler1.ashx";    // Thirupur
//    private final String urlString = "http://103.57.150.35/mobile_billing_tmc_live/handler1.ashx";    // Thirupur
//    private final String urlString = "http://bayclubtab.ircacms.in/handler1.ashx";  //BAY
//    private final String urlString = "http://192.168.100.2/mobile_billing_boat/handler1.ashx";  //boat
//    private final String urlString = "http://117.248.111.39:81/mobile_billing_boat/handler1.ashx";  //boat
//      private final String urlString = "http://43.251.80.54/mobile_tab_billing/Handler1.ashx";
//      private final String urlString = "http://10.20.142.2/mobile_tab_billing/Handler1.ashx";

    //   ****************************  BAY CLUB ************************////////////////
//    private final String urlString = "http://bayclubtab.ircacms.in/handler1.ashx";  //Bay Club

//    private final String urlString = "http://43.251.80.54/mobile_tab_billing/handler1.ashx";  //Cosmo Club
//    private final String urlString = "http://43.251.80.54/mobile_tab_test/handler1.ashx";  //Cosmo TEST Club
//    private final String urlString = "http://10.20.142.2/mobile_tab_billing/handler1.ashx";  //Cosmo LOCAL Club
//    private final String urlString = "http://192.168.40.95/mobile_billing_tmc_live/handler1.ashx";  //Tirupur LOCAL Club
//    private final String urlString = "http://192.168.1.4/Mobile_TabBilling/handler1.ashx";
//    private final String urlString = "http://117.205.2.113/Mobile_TabBilling/handler1.ashx";
//    private final String urlString = "http://10.20.142.2/mobile_tab_billing/handler1.ashx";
//    private final String urlString = "http://103.57.150.35/mobile_billing_tmc/handler1.ashx";

    private static String convertStreamToUTF8String(InputStream stream) throws IOException {
        String result = "";
        StringBuilder sb = new StringBuilder();
        try {
            InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[4096];
            int readedChars = 0;
            while (readedChars != -1) {
                readedChars = reader.read(buffer);
                if (readedChars > 0)
                    sb.append(buffer, 0, readedChars);
            }
            result = sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String load(String contents) throws IOException {
        if (urlString.isEmpty()) {
            Toast.makeText(c, "Please configure URL in Configuration Module", Toast.LENGTH_SHORT).show();
        }
        String res = "";
        URL url = new URL(urlString);
        Log.d("URL", urlString);
        Log.d("Post Data", contents);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Connection", "close");
        conn.setConnectTimeout(70000);
        conn.setDoOutput(true);
        conn.setDoInput(true);
        OutputStreamWriter w = new OutputStreamWriter(conn.getOutputStream());
        w.write(contents);
        w.flush();
        try {
            InputStream istream = conn.getInputStream();
            res = convertStreamToUTF8String(istream);
        } catch (Exception e) {
            JSONObject o = new JSONObject();
            try {
                o.put("NetworkErr", "Network");
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            res = o.toString();
        }
        Log.d("Response Data", res);
        return res;
    }

    /*private String load1(String contents) throws IOException {
        URL url = new URL(urlString1);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setConnectTimeout(60000);
        conn.setDoOutput(true);
        conn.setDoInput(true);
        OutputStreamWriter w = new OutputStreamWriter(conn.getOutputStream());
        w.write(contents);
        w.flush();
        InputStream istream = conn.getInputStream();
        String result = convertStreamToUTF8String(istream);
        return result;
    }*/

    private Object mapObject(Object o) {
        Object finalValue = null;
        if (o.getClass() == String.class) {
            finalValue = o;
        } else if (Number.class.isInstance(o)) {
            finalValue = String.valueOf(o);
        } else if (Date.class.isInstance(o)) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", new Locale("en", "USA"));
            finalValue = sdf.format((Date) o);
        } else if (Collection.class.isInstance(o)) {
            Collection<?> col = (Collection<?>) o;
            JSONArray jarray = new JSONArray();
            for (Object item : col) {
                jarray.put(mapObject(item));
            }
            finalValue = jarray;
        } else {
            Map<String, Object> map = new HashMap<String, Object>();
            Method[] methods = o.getClass().getMethods();
            for (Method method : methods) {
                if (method.getDeclaringClass() == o.getClass()
                        && method.getModifiers() == Modifier.PUBLIC
                        && method.getName().startsWith("get")) {
                    String key = method.getName().substring(3);
                    try {
                        Object obj = method.invoke(o, null);
                        Object value = mapObject(obj);
                        map.put(key, value);
                        finalValue = new JSONObject(map);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        return finalValue;
    }

    public JSONObject getCommonUser_bgc(String userName, String passsword, String deviceId, String devicename) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "getCommonUser_bgc");
        p.put("userName", mapObject(userName));
        p.put("passsword", mapObject(passsword));
        p.put("deviceId", mapObject(deviceId));
        p.put("devicename", mapObject(devicename));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject getItemsTAX() throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "getItemsTAX");
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getPOS() throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getPOS");
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getTablebystore(String storeId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getTablebystore");
        p.put("storeId", mapObject(storeId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getItemGroup(String itemcategoryid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getItemGroup");
        p.put("itemcategoryid", mapObject(itemcategoryid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getItemlist(String itemCategoryId, String storetype) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getItemlist");
        p.put("itemCategoryId", mapObject(itemCategoryId));
        p.put("storetype", mapObject(storetype));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

/*    public JSONObject cms_creditCheck(String accountNo, String date, String cardtype) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_creditCheck");
        p.put("accountNo", mapObject(accountNo));
        p.put("date", mapObject(date));
        p.put("cardtype", mapObject(cardtype));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }*/
    public JSONObject cms_creditCheck(String accountNo,String date,String cardtype,String PosId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface","RestAPI");
        o.put("method", "cms_creditCheck");
        p.put("accountNo",mapObject(accountNo));
        p.put("date",mapObject(date));
        p.put("cardtype",mapObject(cardtype));
        p.put("PosId",mapObject(PosId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject ValidateOTP(String MobileNo, String OTPtext) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "ValidateOTP");
        p.put("MobileNo", mapObject(MobileNo));
        p.put("OTPtext", mapObject(OTPtext));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_placeOrder(Object param, ArrayList<Object> itemlist, String deviceid, String cardtype, String loginId, String posId, String deviceNodel, String referenceNo) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_placeOrder");
        p.put("param", mapObject(param));
        p.put("itemlist", mapObject(itemlist));
        p.put("deviceid", mapObject(deviceid));
        p.put("cardtype", mapObject(cardtype));
        p.put("loginId", mapObject(loginId));
        p.put("posId", mapObject(posId));
        p.put("deviceNodel", mapObject(deviceNodel));
        p.put("referenceNo", mapObject(referenceNo));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    //    public JSONObject cms_closeOrder(String accoutNo,String deviceid,String storeId,String billdate,int type,String personid,String cardtype,String storeName,String paymode,String contractorid) throws Exception {
//        JSONObject result = null;
//        JSONObject o = new JSONObject();
//        JSONObject p = new JSONObject();
//        o.put("interface","RestAPI");
//        o.put("method", "cms_closeOrder");
//        p.put("accoutNo",mapObject(accoutNo));
//        p.put("deviceid",mapObject(deviceid));
//        p.put("storeId",mapObject(storeId));
//        p.put("billdate",mapObject(billdate));
//        p.put("type",mapObject(type));
//        p.put("personid",mapObject(personid));
//        p.put("cardtype",mapObject(cardtype));
//        p.put("storeName",mapObject(storeName));
//        p.put("paymode",mapObject(paymode));
//        p.put("contractorid",mapObject(contractorid));
//        o.put("parameters", p);
//        String s = o.toString();
//        String r = load(s);
//        result = new JSONObject(r);
//        return result;
//    }
    public JSONObject cms_getCancelItems(String memberid, ArrayList<Object> itemlist, Object param, String billid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getCancelItems");
        p.put("memberid", mapObject(memberid));
        p.put("itemlist", mapObject(itemlist));
        p.put("param", mapObject(param));
        p.put("billid", mapObject(billid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }
/*    public JSONObject cms_getAccountnumberByLogin(String personid, String storeid) throws Exception
    {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getAccountnumberByLogin");
        p.put("personid", mapObject(personid));
        p.put("storeid", mapObject(storeid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }*/

    public JSONObject cms_getAccountnumberTodays(String storeid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getAccountnumberTodays");
        p.put("storeid", mapObject(storeid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject CmsGetTablesForTransfer(String storeId, String otNo) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "CmsGetTablesForTransfer");
        p.put("storeId", mapObject(storeId));
        p.put("otNo", mapObject(otNo));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject GetMemberDetailsForOTTransfer(String AccNo, String billDate) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetMemberDetailsForOTTransfer");
        p.put("AccNo", mapObject(AccNo));
        p.put("billDate", mapObject(billDate));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject GetOTSummaryForTransfer(String StoreID, String AccNo, String OtNo, String tableNumber) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetOTSummaryForTransfer");
        p.put("StoreID", mapObject(StoreID));
        p.put("AccNo", mapObject(AccNo));
        p.put("OtNo", mapObject(OtNo));
        p.put("tableNumber", mapObject(tableNumber));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject InsertOTTransferDetails(String FromMemberId, String ToMemberId, String BillID, String OTNo, String StoreID, String UserId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "InsertOTTransferDetails");
        p.put("FromMemberId", mapObject(FromMemberId));
        p.put("ToMemberId", mapObject(ToMemberId));
        p.put("BillID", mapObject(BillID));
        p.put("OTNo", mapObject(OTNo));
        p.put("StoreID", mapObject(StoreID));
        p.put("UserId", mapObject(UserId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject GetPendingOTList(String AccNo, String FromDate, String ToDate, String StoreID, String UserCode) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetPendingOTList");
        p.put("AccNo", mapObject(AccNo));
        p.put("FromDate", mapObject(FromDate));
        p.put("ToDate", mapObject(ToDate));
        p.put("StoreID", mapObject(StoreID));
        p.put("UserCode", mapObject(UserCode));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject InsertOTItemTransferDetails(String FromMemberId, String ToMemberId, String BillID, String OTNo, String ItemIds, String StoreID, String UserId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "InsertOTItemTransferDetails");
        p.put("FromMemberId", mapObject(FromMemberId));
        p.put("ToMemberId", mapObject(ToMemberId));
        p.put("BillID", mapObject(BillID));
        p.put("OTNo", mapObject(OTNo));
        p.put("ItemIds", mapObject(ItemIds));
        p.put("StoreID", mapObject(StoreID));
        p.put("UserId", mapObject(UserId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject GetOTSummaryForItemsTransfer(String StoreID, String AccNo, String OtNo, String tableNumber) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetOTSummaryForItemsTransfer");
        p.put("StoreID", mapObject(StoreID));
        p.put("AccNo", mapObject(AccNo));
        p.put("OtNo", mapObject(OtNo));
        p.put("tableNumber", mapObject(tableNumber));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject CmsGetRunningOrders(String storeId, String personID) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "CmsGetRunningOrders");
        p.put("storeId", mapObject(storeId));
        p.put("personID", mapObject(personID));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject GetProvisionalBill(String billId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetProvisionalBill");
        p.put("billId", mapObject(billId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject CmsUpdateTableTransfer(String tableId, String otNo) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "CmsUpdateTableTransfer");
        p.put("tableId", mapObject(tableId));
        p.put("otNo", mapObject(otNo));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject CmsGetItemPromotion(String storeid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "CmsGetItemPromotion");
        p.put("storeid", mapObject(storeid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getOTReprint(String accoutNo, String deviceid, String storeId, String billdate, String personid, String cardtype, String storeName, String contractorid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getOTReprint");
        p.put("accoutNo", mapObject(accoutNo));
        p.put("deviceid", mapObject(deviceid));
        p.put("storeId", mapObject(storeId));
        p.put("billdate", mapObject(billdate));
        p.put("personid", mapObject(personid));
        p.put("cardtype", mapObject(cardtype));
        p.put("storeName", mapObject(storeName));
        p.put("contractorid", mapObject(contractorid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getOTReprint(String memberid, String storeId, String billdate, String personid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getOTReprint");
        p.put("memberid", mapObject(memberid));
        p.put("storeId", mapObject(storeId));
        p.put("billdate", mapObject(billdate));
        p.put("personid", mapObject(personid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject GetMemberInformationforPOSonAccKeyIn(String AccNo,String CardSerialNo) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface","RestAPI");
        o.put("method", "GetMemberInformationforPOSonAccKeyIn");
        p.put("AccNo",mapObject(AccNo));
        p.put("CardSerialNo",mapObject(CardSerialNo));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getOTReprint(String memberid, String storeId, String billdate, String personid, String cardtype, String billtype) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getOTReprint");
        p.put("memberid", mapObject(memberid));
        p.put("storeId", mapObject(storeId));
        p.put("billdate", mapObject(billdate));
        p.put("personid", mapObject(personid));
        p.put("cardtype", mapObject(cardtype));
        p.put("billtype", mapObject(billtype));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject cms_placeOrder2(Object param, ArrayList<Object> itemlist, String deviceid, String cardtype, String loginId, String posId, String deviceNodel, String referenceNo) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_placeOrder2");
        p.put("param", mapObject(param));
        p.put("itemlist", mapObject(itemlist));
        p.put("deviceid", mapObject(deviceid));
        p.put("cardtype", mapObject(cardtype));
        p.put("loginId", mapObject(loginId));
        p.put("posId", mapObject(posId));
        p.put("deviceNodel", mapObject(deviceNodel));
        p.put("referenceNo", mapObject(referenceNo));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_placeOrder3(Object param, ArrayList<Object> itemlist, String deviceid, String cardtype, String loginId, String posId, String deviceNodel, String referenceNo) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_placeOrder3");
        p.put("param", mapObject(param));
        p.put("itemlist", mapObject(itemlist));
        p.put("deviceid", mapObject(deviceid));
        p.put("cardtype", mapObject(cardtype));
        p.put("loginId", mapObject(loginId));
        p.put("posId", mapObject(posId));
        p.put("deviceNodel", mapObject(deviceNodel));
        p.put("referenceNo", mapObject(referenceNo));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


//kitchen

    public JSONObject cms_placeOrder_new(Object param, ArrayList<Object> itemlist, String deviceid, String cardtype, String loginId, String posId, String deviceNodel, String referenceNo, String personId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_placeOrder_new");
        p.put("param", mapObject(param));
        p.put("itemlist", mapObject(itemlist));
        p.put("deviceid", mapObject(deviceid));
        p.put("cardtype", mapObject(cardtype));
        p.put("loginId", mapObject(loginId));
        p.put("posId", mapObject(posId));
        p.put("deviceNodel", mapObject(deviceNodel));
        p.put("referenceNo", mapObject(referenceNo));
        p.put("personId", mapObject(personId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    //bar
    public JSONObject cms_placeOrder2_new(Object param, ArrayList<Object> itemlist, String deviceid, String cardtype, String loginId, String posId, String deviceNodel, String referenceNo, String personId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_placeOrder2_new");
        p.put("param", mapObject(param));
        p.put("itemlist", mapObject(itemlist));
        p.put("deviceid", mapObject(deviceid));
        p.put("cardtype", mapObject(cardtype));
        p.put("loginId", mapObject(loginId));
        p.put("posId", mapObject(posId));
        p.put("deviceNodel", mapObject(deviceNodel));
        p.put("referenceNo", mapObject(referenceNo));
        p.put("personId", mapObject(personId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject GetAccountListData(String StoreId, String UserId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetAccountListData");
        p.put("StoreId", mapObject(StoreId));
        p.put("UserId", mapObject(UserId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    //events
    public JSONObject cms_placeOrder3_new(Object param, ArrayList<Object> itemlist, String deviceid, String cardtype, String loginId, String posId, String deviceNodel, String referenceNo, String paymode, String personId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_placeOrder3_new");
        p.put("param", mapObject(param));
        p.put("itemlist", mapObject(itemlist));
        p.put("deviceid", mapObject(deviceid));
        p.put("cardtype", mapObject(cardtype));
        p.put("loginId", mapObject(loginId));
        p.put("posId", mapObject(posId));
        p.put("deviceNodel", mapObject(deviceNodel));
        p.put("referenceNo", mapObject(referenceNo));
        p.put("paymode", mapObject(paymode));
        p.put("personId", mapObject(personId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject printTest(String storeid, String printername, String deviceid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "printTest");
        p.put("storeid", mapObject(storeid));
        p.put("printername", mapObject(printername));
        p.put("deviceid", mapObject(deviceid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject cms_getSteward(String loginid,String posid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface","RestAPI");
        o.put("method", "cms_getSteward");
        p.put("loginid",mapObject(loginid));
        p.put("posid",mapObject(posid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }



    //Rani methids new

    public JSONObject cms_getWebPOS() throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getWebPOS");
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject CmsGetDashboardData(String date, String storeId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "CmsGetDashboardData");
        p.put("date", mapObject(date));
        p.put("storeId", mapObject(storeId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject CmsGetOtTransferDetails(String storeId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "CmsGetOtTransferDetails");
        p.put("storeId", mapObject(storeId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject CmsGetPosNew() throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "CmsGetPosNew");
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject CmsGetPosNew(String UserID) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "CmsGetPosNew");
        p.put("UserID", mapObject(UserID));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    //    public JSONObject cms_WebplaceOrder(Object param,ArrayList<Object> itemlist,String deviceid,String cardtype,String loginId,String posId,String deviceNodel,String referenceNo,String personId,String BillType,String PaymentMode,String OTtype,String mobilestoreid) throws Exception {
//        JSONObject result = null;
//        JSONObject o = new JSONObject();
//        JSONObject p = new JSONObject();
//        o.put("interface","RestAPI");
//        o.put("method", "cms_WebplaceOrder");
//        p.put("param",mapObject(param));
//        p.put("itemlist",mapObject(itemlist));
//        p.put("deviceid",mapObject(deviceid));
//        p.put("cardtype",mapObject(cardtype));
//        p.put("loginId",mapObject(loginId));
//        p.put("posId",mapObject(posId));
//        p.put("deviceNodel",mapObject(deviceNodel));
//        p.put("referenceNo",mapObject(referenceNo));
//        p.put("personId",mapObject(personId));
//        p.put("BillType",mapObject(BillType));
//        p.put("PaymentMode",mapObject(PaymentMode));
//        p.put("OTtype",mapObject(OTtype));
//        p.put("mobilestoreid",mapObject(mobilestoreid));
//        o.put("parameters", p);
//        String s = o.toString();
//        String r = load(s);
//        result = new JSONObject(r);
//        return result;
//    }
 /*   public JSONObject cms_closeOrder(String accoutNo, String deviceid, String storeId, String billdate, int type, String personid, String cardtype, String storeName, String paymode, String contractorid, String mobilestoreid, String billtype) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_closeOrder");
        p.put("accoutNo", mapObject(accoutNo));
        p.put("deviceid", mapObject(deviceid));
        p.put("storeId", mapObject(storeId));
        p.put("billdate", mapObject(billdate));
        p.put("type", mapObject(type));
        p.put("personid", mapObject(personid));
        p.put("cardtype", mapObject(cardtype));
        p.put("storeName", mapObject(storeName));
        p.put("paymode", mapObject(paymode));
        p.put("contractorid", mapObject(contractorid));
        p.put("mobilestoreid", mapObject(mobilestoreid));
        p.put("billtype", mapObject(billtype));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }*/

   /* public JSONObject cms_closeOrder(String accoutNo, String deviceid, String storeId, String billdate, int type, String personid, String cardtype, String storeName, String paymode, String contractorid, String mobilestoreid, String billtype, float dp,String discountType) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_closeOrder");
        p.put("accoutNo", mapObject(accoutNo));
        p.put("deviceid", mapObject(deviceid));
        p.put("storeId", mapObject(storeId));
        p.put("billdate", mapObject(billdate));
        p.put("type", mapObject(type));
        p.put("personid", mapObject(personid));
        p.put("cardtype", mapObject(cardtype));
        p.put("storeName", mapObject(storeName));
        p.put("paymode", mapObject(paymode));
        p.put("contractorid", mapObject(contractorid));
        p.put("mobilestoreid", mapObject(mobilestoreid));
        p.put("billtype", mapObject(billtype));
        p.put("discountType", mapObject(discountType));
        p.put("dp", mapObject(dp));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }*/
   public JSONObject cms_closeOrder(String accoutNo,String deviceid,String storeId,String billdate,int type,String personid,String cardtype,String storeName,String paymode,String contractorid,String mobilestoreid,String billtype,float dp,String discountType,String stewardid) throws Exception {
       JSONObject result = null;
       JSONObject o = new JSONObject();
       JSONObject p = new JSONObject();
       o.put("interface","RestAPI");
       o.put("method", "cms_closeOrder");
       p.put("accoutNo",mapObject(accoutNo));
       p.put("deviceid",mapObject(deviceid));
       p.put("storeId",mapObject(storeId));
       p.put("billdate",mapObject(billdate));
       p.put("type",mapObject(type));
       p.put("personid",mapObject(personid));
       p.put("cardtype",mapObject(cardtype));
       p.put("storeName",mapObject(storeName));
       p.put("paymode",mapObject(paymode));
       p.put("contractorid",mapObject(contractorid));
       p.put("mobilestoreid",mapObject(mobilestoreid));
       p.put("billtype",mapObject(billtype));
       p.put("dp",mapObject(dp));
       p.put("discountType",mapObject(discountType));
       p.put("stewardid",mapObject(stewardid));
       o.put("parameters", p);
       String s = o.toString();
       String r = load(s);
       result = new JSONObject(r);
       return result;
   }


    public JSONObject cms_getUitilityBills(String posid, String mobilestoreid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getUitilityBills");
        p.put("posid", mapObject(posid));
        p.put("mobilestoreid", mapObject(mobilestoreid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_Utility(String accno, String amount, String deviceid, String posid, String userId, String mobilestoreid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_Utility");
        p.put("accno", mapObject(accno));
        p.put("amount", mapObject(amount));
        p.put("deviceid", mapObject(deviceid));
        p.put("posid", mapObject(posid));
        p.put("userId", mapObject(userId));
        p.put("mobilestoreid", mapObject(mobilestoreid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_WebplaceOrder(Object param, ArrayList<Object> itemlist, String deviceid, String cardtype, String loginId, String posId, String deviceNodel, String referenceNo, String personId, String BillType, String PaymentMode, String OTtype, String mobilestoreid, String guestname) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_WebplaceOrder");
        p.put("param", mapObject(param));
        p.put("itemlist", mapObject(itemlist));
        p.put("deviceid", mapObject(deviceid));
        p.put("cardtype", mapObject(cardtype));
        p.put("loginId", mapObject(loginId));
        p.put("posId", mapObject(posId));
        p.put("deviceNodel", mapObject(deviceNodel));
        p.put("referenceNo", mapObject(referenceNo));
        p.put("personId", mapObject(personId));
        p.put("BillType", mapObject(BillType));
        p.put("PaymentMode", mapObject(PaymentMode));
        p.put("OTtype", mapObject(OTtype));
        p.put("mobilestoreid", mapObject(mobilestoreid));
        p.put("guestname", mapObject(guestname));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_WebplaceOrder(Object param, ArrayList<Object> itemlist, String deviceid, String cardtype, String loginId, String posId, String deviceNodel, String referenceNo, String personId, String BillType, String PaymentMode, String OTtype, String mobilestoreid, String guestname, String OTNote, String IsSmartCardUsed, ArrayList<Object> modifierlist) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_WebplaceOrder");
        p.put("param", mapObject(param));
        p.put("itemlist", mapObject(itemlist));
        p.put("modifierlist", mapObject(modifierlist));
        p.put("deviceid", mapObject(deviceid));
        p.put("cardtype", mapObject(cardtype));
        p.put("loginId", mapObject(loginId));
        p.put("posId", mapObject(posId));
        p.put("deviceNodel", mapObject(deviceNodel));
        p.put("referenceNo", mapObject(referenceNo));
        p.put("personId", mapObject(personId));
        p.put("BillType", mapObject(BillType));
        p.put("PaymentMode", mapObject(PaymentMode));
        p.put("IsSmartCardUsed", mapObject(IsSmartCardUsed));
        p.put("OTtype", mapObject(OTtype));
        p.put("mobilestoreid", mapObject(mobilestoreid));
        p.put("guestname", mapObject(guestname));
        p.put("OTNote", mapObject(OTNote));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }



    public JSONObject cms_creditCheckV2(String cardnumber,String bilingtype,String PosId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface","RestAPI");
        o.put("method", "cms_creditCheckV2");
        p.put("cardnumber",mapObject(cardnumber));
        p.put("bilingtype",mapObject(bilingtype));
        p.put("PosId",mapObject(PosId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject GetMenuListForMenuCard(String QRCode, String POSId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetMenuListForMenuCard");
        p.put("QRCode", mapObject(QRCode));
        p.put("POSId", mapObject(POSId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject GetLatestApkReleaseVersion() throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetLatestApkReleaseVersion");

        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject UpdatePaxCount(String PaxCount, String BillID, String MemberID, String UserId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "UpdatePaxCount");
        p.put("PaxCount", mapObject(PaxCount));
        p.put("BillID", mapObject(BillID));
        p.put("MemberID", mapObject(MemberID));
        p.put("UserId", mapObject(UserId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject GetTabBillingApplicationURL(String clubCode) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetTabBillingApplicationURL");
        p.put("clubCode", mapObject(clubCode));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject OTPGeneration(String MobileNo, String UserId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "OTPGeneration");
        p.put("MobileNo", mapObject(MobileNo));
        p.put("UserId", mapObject(UserId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getModifier() throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getModifier");
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getModifierDetails() throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getModifierDetails");
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_updateOTtransfer(ArrayList<String> otno, String memberid, String acno) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_updateOTtransfer");
        p.put("otno", mapObject(otno));
        p.put("memberid", mapObject(memberid));
        p.put("acno", mapObject(acno));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getItemPromotion(int storeid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getItemPromotion");
        p.put("storeid", mapObject(storeid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject GetTempBillItemModifierdetails(String StoreId, String BillId) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetTempBillItemModifierdetails");
        p.put("StoreId", mapObject(StoreId));
        p.put("BillId", mapObject(BillId));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject cms_getdateDetails(String accno) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getdateDetails");
        p.put("accno", mapObject(accno));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject GetBillingGroupByTaxDetails(String billno) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "GetBillingGroupByTaxDetails");
        p.put("billno", mapObject(billno));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject cms_getReprint(String billid, int userid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getReprint");
        p.put("billid", mapObject(billid));
        p.put("userid", mapObject(userid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_Taxexemption(String billdetailno, ArrayList<Integer> taxid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_Taxexemption");
        p.put("billdetailno", mapObject(billdetailno));
        p.put("taxid", mapObject(taxid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getTaxexemptionReason() throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getTaxexemptionReason");
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }


    public JSONObject cms_getAccountnumberByLogin(String personid, String storeid, String fd, String td) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getAccountnumberByLogin");
        p.put("personid", mapObject(personid));
        p.put("storeid", mapObject(storeid));
        p.put("fd", mapObject(fd));
        p.put("td", mapObject(td));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_postSign(String billid, String signdata, String acno) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_postSign");
        p.put("billid", mapObject(billid));
        p.put("signdata", mapObject(signdata));
        p.put("acno", mapObject(acno));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getItemStockbyGroup(int storeid, int groupid) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getItemStockbyGroup");
        p.put("storeid", mapObject(storeid));
        p.put("groupid", mapObject(groupid));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }

    public JSONObject cms_getaccountlistot(String FromDate, String ToDate, String StoreID, String UserCode) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface", "RestAPI");
        o.put("method", "cms_getaccountlistot");
        p.put("FromDate", mapObject(FromDate));
        p.put("ToDate", mapObject(ToDate));
        p.put("StoreID", mapObject(StoreID));
        p.put("UserCode", mapObject(UserCode));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }






    public JSONObject getModifiersBluetooth(String otNo,String itemCode,String quantity) throws Exception {
        JSONObject result = null;
        JSONObject o = new JSONObject();
        JSONObject p = new JSONObject();
        o.put("interface","RestAPI");
        o.put("method", "getModifiersBluetooth");
        p.put("otNo",mapObject(otNo));
        p.put("itemCode",mapObject(itemCode));
        p.put("quantity",mapObject(quantity));
        o.put("parameters", p);
        String s = o.toString();
        String r = load(s);
        result = new JSONObject(r);
        return result;
    }
}