package com.irca.cosmo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
 import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.crashlytics.android.Crashlytics;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.irca.Billing.Accountlist;
import com.irca.Billing.BillingProfile;
import com.irca.Billing.MainActivity;
import com.irca.Billing.MakeOrder;
import com.irca.Printer.DiscoveryActivity;
import com.irca.Utils.ShowMsg;
import com.irca.ServerConnection.ConnectionDetector;
import com.irca.db.Dbase;
import com.irca.fields.AndroidTempbilldetails;
import com.irca.fields.AndroidTempbilling;
import com.irca.fields.CloseorderItems;
import com.irca.fields.ItemList;
import com.irca.scan.PermissionsManager;
import com.zcs.sdk.DriverManager;
import com.zcs.sdk.SdkResult;
import com.zcs.sdk.Sys;
import com.zcs.sdk.print.PrnStrFormat;
import com.zcs.sdk.print.PrnTextFont;
import com.zcs.sdk.print.PrnTextStyle;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ExecutorService;

/*
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
*/

import static java.lang.Float.parseFloat;

/**
 * Created by Manoch Richard on 11/13/2015.
 */
public class CloseOrderdetails extends AppCompatActivity implements ReceiveListener {
    LinearLayout customList;
    LinearLayout customTaxList;
    View customView;
    View customTaxView;
    TextView itemName, qty, amount, itemCount, kot_rate;
    CheckBox happyhour;
    TextView taxHeader, taxValue;
    ImageView add, subtract;

    LinearLayout removeButton, taxamount;
    Button cancelorder_butt, print_butt, print_buttnew;
    Dbase db;
    float Totalamount = 0;
    // String position;

    SharedPreferences pref;
    String _target = "", _printer = "";
    String print = "";

    private Printer mPrinter = null;
    private Context mContext = null;

    ProgressDialog pd;

    int page;

    ArrayList<ItemList> cancle_order;
    RelativeLayout printer;
    EditText printer_Name;
    Button bluetooth;

    String waiter = "";
    String counter_name = "";
    String billType = "";
    SharedPreferences shafreepreferences;
    String memName = "";
    String memAccNo = "";
    int   tableId = 0;
    String tableNo = "";
    String waiternamecode;
    double servicetax = 0, vat = 0, GrantTotal = 0, billAmt = 0, cessTax = 0, salesTax = 0, tax = 0, accharges;
    String ava_balance = "";
    String cardType = "";
    TextView amt;

    int count = 0;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    TextView rate;

    TextView tot_tax, g_tot, a_bal, billdetnum;
    TextView _mName, _mCode, billNo, bNo, steward;
    String billid = "";
    String _service = "0", _sales = "0", _vat = "0";

    String clubName = "", clubaddress = "";

    ArrayList<String> taxlist;
    // ImageView logo;
    Bitmap bitmap;
    ActionBar ab;

    private DriverManager mDriverManager = DriverManager.getInstance();
    private com.zcs.sdk.Printer mPrinter1;

    private ExecutorService mSingleThreadExecutor;

    private PermissionsManager mPermissionsManager;
    private Sys mSys = mDriverManager.getBaseSysDevice();
   // ArrayList<CloseorderItems> _closeorderItemsArrayList   = new ArrayList<>();
    ArrayList<CloseorderItems> _closeorderItemsArrayList = new ArrayList<>();

    int countclick=0;

    //TODO 29-07-2022
    CharSequence[] values;
    ImageView nextot,backot;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences.Editor editor = shafreepreferences.edit();
        editor.putString("vocationalmember", "");
        editor.commit();
        Intent i = new Intent(CloseOrderdetails.this, Accountlist.class);
        i.putExtra("tableNo", tableNo);
        i.putExtra("tableId", tableId);
        i.putExtra("from", "CloseOrderdetails");
        //   i.putExtra("position",position);
        startActivity(i);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.close_order);
        // title = (TextView) findViewById(R.id.title);
        rate = (TextView) findViewById(R.id.itmrate);
        taxamount = (LinearLayout) findViewById(R.id.tax_amts);

        _mName = (TextView) findViewById(R.id.mName);
        _mCode = (TextView) findViewById(R.id.mCode);
        billNo = (TextView) findViewById(R.id.billNo);
        bNo = (TextView) findViewById(R.id.bNo);
        steward = (TextView) findViewById(R.id.steward);
        billdetnum = (TextView) findViewById(R.id.billdetnum);
        backot = (ImageView) findViewById(R.id.backot);
        nextot = (ImageView) findViewById(R.id.nextot);

        // logo=(ImageView)findViewById(R.id.imageView_logo);
        //   bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.clublogo);
        //  logo.setImageBitmap(CircularImage.getCircleBitmap(bitmap));
        taxlist = new ArrayList<>();
        mContext = this;

        final Bundle b = getIntent().getExtras();

        pref = getSharedPreferences("Bluetooth", Context.MODE_PRIVATE);
        shafreepreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        waiter = shafreepreferences.getString("WaiterName", "");

        _target = pref.getString("Target", "");
        _printer = pref.getString("Printer", "");

        clubName = shafreepreferences.getString("clubName", "");
        clubaddress = shafreepreferences.getString("clubAddress", "");

        counter_name = shafreepreferences.getString("StoreName", "");
        billType = shafreepreferences.getString("billType", "");

        cd = new ConnectionDetector(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();

        tot_tax = (TextView) findViewById(R.id.tamt);
       /* ser_tax= (TextView) findViewById(R.id.ser_tax);
        sales_tax= (TextView) findViewById(R.id.sal_tax);
        vat_tax= (TextView) findViewById(R.id.vat);
        c_tax= (TextView) findViewById(R.id.c_tax);*/
        g_tot = (TextView) findViewById(R.id.g_tot);
        a_bal = (TextView) findViewById(R.id.a_bal);


        printer = (RelativeLayout) findViewById(R.id.pr);
        printer_Name = (EditText) findViewById(R.id.edtTarget1);
        bluetooth = (Button) findViewById(R.id.bluetooth);
        amt = (TextView) findViewById(R.id.itmAmt);


        cancelorder_butt = (Button) findViewById(R.id.button_cancelOrder);
        print_butt = (Button) findViewById(R.id.button_cancelprint);
        print_buttnew = (Button) findViewById(R.id.button_cancelprintnew);


        if (_printer.equalsIgnoreCase("") && _target.equalsIgnoreCase("")) {


//            printer_Name.setText("TM-P20_011044" + "-" + "BT:00:01:90:AE:93:17");
            //     printer_Name.setText("TM-P80_002585" + "-" + "BT:00:01:90:88:DA:48");

//            printer_Name.setText("TM-P20_000452" + "-" + "BT:00:01:90:C2:AE:77");
        } else {

            printer_Name.setText(_printer + "-" + _target);
        }


   /*     bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Find bluetooth
                Intent intent = new Intent(getApplication(), DiscoveryActivity.class);
                startActivityForResult(intent, 0);

            }
        });*/
        nextot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int totalentries=values.length;
                int selectedentry= (int) bluetooth.getTag(R.string.firstvalue);
                selectedentry=selectedentry+1;
                int minus= totalentries-(selectedentry);
                android.util.Log.d("minus : ",(minus)+"");
                if(minus>1){
                    bluetooth.setText((selectedentry+1) +" OT / "+totalentries);
                    android.util.Log.d("totalentries : ",(totalentries)+"");
                    android.util.Log.d("selectedentry : ",(selectedentry)+"");
                    cancelOrder(values[(selectedentry)],(selectedentry));
                    backot.setVisibility(View.VISIBLE);

                }
                else if((minus) == 1){
                    bluetooth.setText("Last OT :"+values[(selectedentry)]);
                    android.util.Log.d("totalentries : ",(totalentries)+"");
                    android.util.Log.d("selectedentry : ",(selectedentry)+"");
                    cancelOrder(values[(selectedentry)],(selectedentry));
                }
                else {
                    Toast.makeText(CloseOrderdetails.this,"Last Order",Toast.LENGTH_LONG).show();
                }
            }
        });


        backot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int totalentries=values.length;
                int selectedentry= (int) bluetooth.getTag(R.string.firstvalue);

                selectedentry=selectedentry-1;
                if(selectedentry>-1){
                    int minus= totalentries-(selectedentry);
                    android.util.Log.d("minus : ",(minus)+"");
                    bluetooth.setText((selectedentry +1)+" OT / "+totalentries);
                    if(minus>1){
                        android.util.Log.d("totalentries : ",(totalentries)+"");
                        android.util.Log.d("selectedentry : ",(selectedentry)+"");
                        cancelOrder(values[(selectedentry)],(selectedentry));

                    }
                    else if((minus) == 1){
                        bluetooth.setText("Last OT :"+values[(selectedentry)]);
                        android.util.Log.d("totalentries : ",(totalentries)+"");
                        Log.d("selectedentry : ",(selectedentry)+"");
                        cancelOrder(values[(selectedentry)],(selectedentry));
                    }
                    else {
                        Toast.makeText(CloseOrderdetails.this,"Last Order",Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(CloseOrderdetails.this,"Please click next order",Toast.LENGTH_LONG).show();

                }

            }
        });
        page = b.getInt("pageValue");
        memName = b.getString("cfreeitName");
        memAccNo = b.getString("cfreeitAno");
        tableNo = b.getString("tableNo");
        tableId = b.getInt("tableId");
        waiternamecode = b.getString("waiternamecode");
        cardType = b.getString("cardType");//CASH CARD


        if (cardType.equals("CASH CARD") && BillingProfile.isCashCardFilled.equalsIgnoreCase("false")) {
            a_bal.setText("0");

        } else if (cardType.equals("CASH CARD")) {
            a_bal.setText("-" + ava_balance);

        }


        if (page == 3) {
            billid = b.getString("bill_id");
        }


        if (page == 0) {
            actionBarSetup("Closed Order");
            //title.setText("Closed Order");
            amt.setVisibility(View.VISIBLE);
            taxamount.setVisibility(View.VISIBLE);
            bNo.setVisibility(View.VISIBLE);
            billNo.setVisibility(View.VISIBLE);
            if (BillingProfile.closeorderItemsArrayList.size() == 0) {

            } else {
                _mName.setText(BillingProfile.closeorderItemsArrayList.get(0).mName);
                _mCode.setText(BillingProfile.closeorderItemsArrayList.get(0).mAcc);
                // billNo.setText(TakeOrder.closeorderItemsArrayList.get(0).billno);
                steward.setText("Steward Name: " + waiternamecode);
                if (BillingProfile.closeorderItemsArrayList.get(0).billno.equals("") || BillingProfile.closeorderItemsArrayList.get(0).billno == null) {
                    billNo.setText(BillingProfile.closeorderItemsArrayList.get(0).billno);
                } else {
                    billNo.setText(BillingProfile.closeorderItemsArrayList.get(0).billno.substring(4));
                }

            }

            removeButton = (LinearLayout) findViewById(R.id.butt);
            cancelorder_butt.setVisibility(View.GONE);
            print_butt = (Button) findViewById(R.id.button_cancelprint);
            print_buttnew = (Button) findViewById(R.id.button_cancelprintnew);
            print_butt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    print = printer_Name.getText().toString();

                    if (BillingProfile.closeorderItemsArrayList.size() == 0 || BillingProfile.closeorderItemsArrayList == null) {
                        Toast.makeText(getApplicationContext(), "No Order placed ", Toast.LENGTH_SHORT).show();
                    } else {
                        if (print.equals("")) {

                            Toast.makeText(CloseOrderdetails.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                        } else {
                          /*  print_butt.setBackgroundColor(getResources().getColor(R.color.pink));
                            print_butt.setText("Connecting Printer...");
                            updateButtonState(false);
                            if (!runPrintReceiptSequence()) {
                                updateButtonState(true);
                            }*/

                            try {
                                print_butt.setEnabled(false);
                                Activity mActivity = CloseOrderdetails.this;

                                mActivity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        new CloseOrderdetails.AsyncPrinting().execute();
                                    }
                                });

                            } catch (Exception e) {
                                ShowMsg.showException(e, "AsyncTask", mContext);
                            }
                        }
                    }

                }
            });

            print_buttnew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (BillingProfile.closeorderItemsArrayList.size() == 0 || BillingProfile.closeorderItemsArrayList == null) {
                        Toast.makeText(getApplicationContext(), "No Order placed ", Toast.LENGTH_SHORT).show();
                    } else {


                        try {
                            //    print_buttnew.setEnabled(false);
                            Activity mActivity = CloseOrderdetails.this;

                            mActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    new CloseOrderdetails.AsyncPrintingpoona().execute();
                                }
                            });

                        } catch (Exception e) {
                            ShowMsg.showException(e, "AsyncTask", mContext);
                        }

                    }

                }
            });
            // removeButton.setVisibility(View.GONE);
            closeOrder();
        }

        if (page == 11) {
            actionBarSetup("Closed Order");
            //title.setText("Closed Order");
            amt.setVisibility(View.VISIBLE);
            taxamount.setVisibility(View.VISIBLE);
            bNo.setVisibility(View.VISIBLE);
            billNo.setVisibility(View.VISIBLE);
            if (MakeOrder.closeorderItemsArrayList.size() == 0) {

            } else {
                _mName.setText(MakeOrder.closeorderItemsArrayList.get(0).mName);
                _mCode.setText(MakeOrder.closeorderItemsArrayList.get(0).mAcc);
                // billNo.setText(TakeOrder.closeorderItemsArrayList.get(0).billno);
                steward.setText("Steward Name: " + waiternamecode);
                if (MakeOrder.closeorderItemsArrayList.get(0).billno.equals("") || MakeOrder.closeorderItemsArrayList.get(0).billno == null) {
                    billNo.setText(MakeOrder.closeorderItemsArrayList.get(0).billno);
                } else {
                    billNo.setText(MakeOrder.closeorderItemsArrayList.get(0).billno.substring(4));
                }

            }

            removeButton = (LinearLayout) findViewById(R.id.butt);
            cancelorder_butt.setVisibility(View.GONE);
            print_butt = (Button) findViewById(R.id.button_cancelprint);
            print_butt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    print = printer_Name.getText().toString();

                    if (MakeOrder.closeorderItemsArrayList.size() == 0 || MakeOrder.closeorderItemsArrayList == null) {
                        Toast.makeText(getApplicationContext(), "No Order placed ", Toast.LENGTH_SHORT).show();
                    } else {
                        if (print.equals("")) {

                            Toast.makeText(CloseOrderdetails.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                        } else {
                          /*  print_butt.setBackgroundColor(getResources().getColor(R.color.pink));
                            print_butt.setText("Connecting Printer...");
                            updateButtonState(false);
                            if (!runPrintReceiptSequence()) {
                                updateButtonState(true);
                            }*/

                            try {
                                print_butt.setEnabled(false);
                                Activity mActivity = CloseOrderdetails.this;

                                mActivity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        new CloseOrderdetails.AsyncPrinting().execute();
                                    }
                                });

                            } catch (Exception e) {
                                ShowMsg.showException(e, "AsyncTask", mContext);
                            }
                        }
                    }

                }
            });
            // removeButton.setVisibility(View.GONE);
            closeOrder();
        } else if (page == 23) {
            // title.setText("Closed Order");
            actionBarSetup("Closed Order");

            amt.setVisibility(View.VISIBLE);
            taxamount.setVisibility(View.VISIBLE);
            bNo.setVisibility(View.VISIBLE);
            billNo.setVisibility(View.VISIBLE);
            //  _mName.setText(memName);
            // _mCode.setText(memAccNo);
            // steward.setText("Steward Name: "+waiternamecode);
            if (KotItemlist.closeorderItemsArrayList_Others.size() == 0) {

            } else {
                _mName.setText(KotItemlist.closeorderItemsArrayList_Others.get(0).mName);
                _mCode.setText(KotItemlist.closeorderItemsArrayList_Others.get(0).mAcc);
                billNo.setText(KotItemlist.closeorderItemsArrayList_Others.get(0).billno);
                steward.setText("Steward Name: " + waiternamecode);
                if (KotItemlist.closeorderItemsArrayList_Others.get(0).billno.equals("") || KotItemlist.closeorderItemsArrayList_Others.get(0).billno == null) {
                    billNo.setText(KotItemlist.closeorderItemsArrayList_Others.get(0).billno);
                } else {
                    billNo.setText(KotItemlist.closeorderItemsArrayList_Others.get(0).billno.substring(4));
                }

            }

            removeButton = (LinearLayout) findViewById(R.id.butt);
            cancelorder_butt.setVisibility(View.GONE);
            print_butt = (Button) findViewById(R.id.button_cancelprint);
            print_butt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    print = printer_Name.getText().toString();

                    if (KotItemlist.closeorderItemsArrayList_Others.size() == 0 || KotItemlist.closeorderItemsArrayList_Others == null) {
                        Toast.makeText(getApplicationContext(), "No Order placed ", Toast.LENGTH_SHORT).show();
                    } else {
                        if (print.equals("")) {

                            Toast.makeText(CloseOrderdetails.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                        } else {
                           /* print_butt.setBackgroundColor(getResources().getColor(R.color.pink));
                            print_butt.setText("Connecting Printer...");
                            updateButtonState(false);
                            if (!runPrintReceiptSequence()) {
                                updateButtonState(true);
                            }*/

                            try {
                                print_butt.setEnabled(false);
                                Activity mActivity = CloseOrderdetails.this;

                                mActivity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        new CloseOrderdetails.AsyncPrinting().execute();
                                    }
                                });

                            } catch (Exception e) {
                                ShowMsg.showException(e, "AsyncTask", mContext);
                            }
                        }
                    }

                }
            });
            // removeButton.setVisibility(View.GONE);
            closeOrder1();
        } else if (page == 1) {
            //  title.setText("Cancel Order");
            actionBarSetup("Cancel Order");
            List<String> numbers = new ArrayList<>();
            for(int i=0;i<BillingProfile.closeorderItemsArrayList.size() ;i++){
                numbers.add(BillingProfile.closeorderItemsArrayList.get(i).billno);
            }
            System.out.println(numbers);
            Set<String> hashSet = new LinkedHashSet(numbers);
            ArrayList<String> removedDuplicates = new ArrayList(hashSet);
            //TODO 29-07-2022 CreateAlertDialogWithRadioButtonGroup(removedDuplicates) ;
            values = removedDuplicates.toArray(new CharSequence[removedDuplicates.size()]);
            countclick=values.length;
            cancelOrder(values[0],0);

            if(values.length>1){
                bluetooth.setText("1 OT /"+values.length);
                backot.setVisibility(View.VISIBLE);
                nextot.setVisibility(View.VISIBLE);
            }
            else {
                bluetooth.setText(values[0]);
                bluetooth.setTag(R.string.firstvalue,-1);
                backot.setVisibility(View.GONE);
                nextot.setVisibility(View.GONE);
            }
            amt.setVisibility(View.GONE);
            taxamount.setVisibility(View.GONE);
            bNo.setVisibility(View.GONE);
            billNo.setVisibility(View.GONE);
            if (BillingProfile.closeorderItemsArrayList.size() == 0) {
            } else {
                _mName.setText(BillingProfile.closeorderItemsArrayList.get(0).mName);
                _mCode.setText(BillingProfile.closeorderItemsArrayList.get(0).mAcc);
                steward.setText("Steward Name: " + waiternamecode);
            }
            _mName.setText(BillingProfile.closeorderItemsArrayList.get(0).mName);
            _mCode.setText(BillingProfile.closeorderItemsArrayList.get(0).mAcc);
            steward.setText("Steward Name: " + waiter);
            cancelorder_butt.setVisibility(View.VISIBLE);
            printer = (RelativeLayout) findViewById(R.id.pr);
            print_butt = (Button) findViewById(R.id.button_cancelprint);
            print_butt.setVisibility(View.GONE);
            print_buttnew = (Button) findViewById(R.id.button_cancelprintnew);
            print_buttnew.setVisibility(View.GONE);
            cancelorder_butt = (Button) findViewById(R.id.button_cancelOrder);

            //   position=b.getString("position");
            cancelorder_butt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isInternetPresent = cd.isConnectingToInternet();

                    if (isInternetPresent) {
                        new AsyncCancelOrder().execute();

                    } else {
                        Toast.makeText(getApplicationContext(), "Internet Connection Lost", Toast.LENGTH_SHORT).show();
                    }


                }
            });
            print_butt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    print = printer_Name.getText().toString();
                    if (BillingProfile.closeorderItemsArrayList.size() == 0 || BillingProfile.closeorderItemsArrayList == null) {
                        Toast.makeText(getApplicationContext(), "Please ,Cancel the order ", Toast.LENGTH_SHORT).show();
                    } else {
                        if (print.equals("")) {

                            Toast.makeText(CloseOrderdetails.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                        } else {
                            print_butt.setBackgroundColor(getResources().getColor(R.color.pink));
                            print_butt.setText("Connecting Printer...");
                            updateButtonState(false);
                            if (!runPrintReceiptSequence()) {
                                updateButtonState(true);
                            }
                        }
                    }

                }
            });
            print_buttnew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // print = printer_Name.getText().toString();
                    if (BillingProfile.closeorderItemsArrayList.size() == 0 || BillingProfile.closeorderItemsArrayList == null) {
                        Toast.makeText(getApplicationContext(), "Please ,Cancel the order ", Toast.LENGTH_SHORT).show();
                    } else {

                        print_buttnew.setBackgroundColor(getResources().getColor(R.color.pink));
                        print_buttnew.setText("Connecting Printer...");
                         /*   updateButtonState(false);
                            if (!runPrintReceiptSequence()) {
                                updateButtonState(true);
                            }*/
                        try {
                            createReceiptData1poona();

                        } catch (Exception e) {
                            Toast.makeText(CloseOrderdetails.this, e + "", Toast.LENGTH_LONG).show();
                        }
                    }


                }
            });
          //  cancelOrder();
        } else if (page == 3) {


            //  title.setText("Bill Print");
            actionBarSetup("Bill Print");


            amt.setVisibility(View.VISIBLE);
            taxamount.setVisibility(View.VISIBLE);

            _mName.setText(memName);
            _mCode.setText(memAccNo);
            steward.setText("Steward Name: " + waiternamecode);

            removeButton = (LinearLayout) findViewById(R.id.butt);
            cancelorder_butt.setVisibility(View.GONE);
            print_butt = (Button) findViewById(R.id.button_cancelprint);
            print_butt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    print = printer_Name.getText().toString();

                    if (KotItemlist.closeorderItemsArrayList_Others.size() == 0 || KotItemlist.closeorderItemsArrayList_Others == null) {
                        Toast.makeText(getApplicationContext(), "No Order placed ", Toast.LENGTH_SHORT).show();
                    } else {
                        if (print.equals("")) {

                            Toast.makeText(CloseOrderdetails.this, "Please choose the Bluetooth Device ", Toast.LENGTH_SHORT).show();
                        } else {
                            print_butt.setBackgroundColor(getResources().getColor(R.color.pink));
                            print_butt.setText("Connecting Printer...");
                            updateButtonState(false);
                            if (!runPrintReceiptSequence()) {
                                updateButtonState(true);
                            }
                        }
                    }

                }
            });
            // removeButton.setVisibility(View.GONE);
            // closeOrder_Others();
            closeOrder_OthersNew();

        }
    /*    try {
            //TODO remove commented
            Log.setLogSettings(mContext, Log.PERIOD_TEMPORARY, Log.OUTPUT_STORAGE, null, 0, 1, Log.LOGLEVEL_LOW);
        } catch (Exception e) {
            ShowMsg.showException(e, "setLogSettings", mContext);
        }*/


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                SharedPreferences.Editor editor = shafreepreferences.edit();
                editor.putString("vocationalmember", "");
                editor.commit();
                Intent i = new Intent(CloseOrderdetails.this, Accountlist.class);
                i.putExtra("tableNo", tableNo);
                i.putExtra("tableId", tableId);
                i.putExtra("from", "CloseOrderdetails");
                startActivity(i);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void actionBarSetup(String title) {
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/Kabel Book BT_0.ttf");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ab = getSupportActionBar();
            ab.setTitle("" + title);
            ab.setElevation(0);

            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    public class AsyncPrinting extends AsyncTask<Void, Void, String> {

        String status = "";

        @Override
        protected String doInBackground(Void... params) {

            try {
                mPrinter = new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
                //     mPrinter = new Printer(Printer.TM_P80, Printer.MODEL_ANK, mContext);
                mPrinter.setReceiveEventListener(CloseOrderdetails.this);
            } catch (Exception e) {
                ShowMsg.showException(e, "Printer", mContext);
            }
            try {
                if (page == 0)  // close order
                {
                    if (!createReceiptData()) {
                        if (mPrinter == null) {
                            status = "false";
                        }
                        mPrinter.clearCommandBuffer();
                        mPrinter.setReceiveEventListener(null);
                        mPrinter = null;
                        status = "false";
                    } else {
                        status = "true";
                    }
                }
                if (page == 11)  // close order
                {
                    if (!createReceiptData()) {
                        if (mPrinter == null) {
                            status = "false";
                        }
                        mPrinter.clearCommandBuffer();
                        mPrinter.setReceiveEventListener(null);
                        mPrinter = null;
                        status = "false";
                    } else {
                        status = "true";
                    }
                } else if (page == 1) //cancel order (ot print )
                {
                    if (!createReceiptData1()) {
                        if (mPrinter == null) {
                            status = "false";
                        }
                        mPrinter.clearCommandBuffer();
                        mPrinter.setReceiveEventListener(null);
                        mPrinter = null;
                        status = "false";
                    } else {
                        status = "true";
                    }
                } else if (page == 23)   //cancel order (ot print )

                {
                    if (!createReceiptData23()) {
                        if (mPrinter == null) {
                            status = "false";
                        }
                        mPrinter.clearCommandBuffer();
                        mPrinter.setReceiveEventListener(null);
                        mPrinter = null;
                        status = "false";
                    } else {
                        status = "true";
                    }
                }
// else {
//                    if (!createReceiptData3()) {
//                        if (mPrinter == null) {
//                            status = "false";
//                        }
//                        mPrinter.clearCommandBuffer();
//                        mPrinter.setReceiveEventListener(null);
//                        mPrinter = null;
//                        status = "false";
//                    } else {
//                        status = "true";
//                    }
//                }


            } catch (Exception e) {
                ShowMsg.showException(e, "other", mContext);
            }

            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(CloseOrderdetails.this);
            pd.setMessage("Printing Please Wait .......");
            pd.setCancelable(false);
            pd.show();
        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            if (status.equals("true")) {

                try {

                    boolean print = printData();
                    if (!print) {
                        if (mPrinter == null) {
                            status = "false";
                        }

                        mPrinter.clearCommandBuffer();

                        mPrinter.setReceiveEventListener(null);

                        mPrinter = null;

                    } else {
                        count = count + 1;
                        status = "true";
                    }
                } catch (Exception e) {
                    ShowMsg.showException(e, "Post execute error", mContext);
                }
                // updateButtonState(true);
                Toast.makeText(CloseOrderdetails.this, "Printing Success", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else if (status.equals("")) {
                Toast.makeText(CloseOrderdetails.this, "Printing failed to enter", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else {
                Toast.makeText(CloseOrderdetails.this, "Printing failed to enter" + status, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }

    public class AsyncPrintingpoona extends AsyncTask<Void, Void, String> {

        String status = "";

        @Override
        protected String doInBackground(Void... params) {
            try {
                if (page == 0)  // close order
                {
                    createReceiptDatapoona();

                    status = "true";

                }
                if (page == 11)  // close order
                {
                    createReceiptDatapoona();
                    status = "true";

                } else if (page == 1) //cancel order (ot print )
                {
                    createReceiptData1poona();

                    status = "true";

                } else if (page == 23)   //cancel order (ot print )

                {
                    createReceiptData23poona();
                    status = "true";

                }

            } catch (Exception e) {
                ShowMsg.showException(e, "other", mContext);
            }

            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(CloseOrderdetails.this);
            pd.setMessage("Printing Please Wait .......");
            pd.setCancelable(false);
            pd.show();
        }


        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);

            if (status.equals("true")) {

                try {

                    boolean print = printData();
                    if (!print) {
                        if (mPrinter == null) {
                            status = "false";
                        }

                        mPrinter.clearCommandBuffer();

                        mPrinter.setReceiveEventListener(null);

                        mPrinter = null;

                    } else {
                        count = count + 1;
                        status = "true";
                    }
                } catch (Exception e) {
                    ShowMsg.showException(e, "Post execute error", mContext);
                }
                // updateButtonState(true);
                Toast.makeText(CloseOrderdetails.this, "Printing Success", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else if (status.equals("")) {
                Toast.makeText(CloseOrderdetails.this, "Printing failed to enter", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            } else {
                Toast.makeText(CloseOrderdetails.this, "Printing failed to enter" + status, Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        }
    }


    private void closeOrder_Others() {
        try {
            servicetax = 0;
            accharges = 0;
            vat = 0;
            billAmt = 0;
            GrantTotal = 0;
            DecimalFormat format = new DecimalFormat("#.##");
            ArrayList<ItemList> _closeorderItemsArrayList = KotItemlist.placeOrder_list;

            for (int i = 0; i < _closeorderItemsArrayList.size(); i++) {
                //  ava_balance=_closeorderItemsArrayList.get(i).getAva_balance();
                servicetax = servicetax + parseFloat(_closeorderItemsArrayList.get(i).getServiceTax());
                vat = vat + parseFloat(_closeorderItemsArrayList.get(i).getVat());
                //  accharges=accharges+parseFloat(_closeorderItemsArrayList.get(i).g());
                salesTax = salesTax + parseFloat(_closeorderItemsArrayList.get(i).getSalestTax());
                billAmt = billAmt + parseFloat(_closeorderItemsArrayList.get(i).getAmount());

                if (_closeorderItemsArrayList.get(i).getCesstax() == null) {
                    cessTax = cessTax + 0;
                } else {
                    cessTax = cessTax + parseFloat(_closeorderItemsArrayList.get(i).getCesstax());
                }

                if (customList == null) {

                    customList = (LinearLayout) findViewById(R.id.closeItemView);
                    customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                    itemName = (TextView) customView.findViewById(R.id.itmName);
                    qty = (TextView) customView.findViewById(R.id.itmQty);
                    amount = (TextView) customView.findViewById(R.id.itmAmt);
                    itemCount = (TextView) customView.findViewById(R.id.itmrate);


                    itemName.setText(_closeorderItemsArrayList.get(i).itemName.trim());
                    qty.setText(_closeorderItemsArrayList.get(i).quantity);
                    amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).amount)));
                    itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));

                }
//            else if(i==10)
//            {
//                customView=getLayoutInflater().inflate(R.layout.close_order_itemlist,customList,false);
//                itemName= (TextView) customView.findViewById(R.id.itmName);
//                qty=(TextView) customView.findViewById(R.id.itmQty);
//                amount=(TextView)customView.findViewById(R.id.itmAmt);
//                itemName.setText("TOTAL");
//                qty.setText("");
//                amount.setText(10*100+"");
//            }
                else {
                    customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);
                    itemName = (TextView) customView.findViewById(R.id.itmName);
                    qty = (TextView) customView.findViewById(R.id.itmQty);
                    amount = (TextView) customView.findViewById(R.id.itmAmt);
                    itemCount = (TextView) customView.findViewById(R.id.itmrate);

                    itemName.setText(_closeorderItemsArrayList.get(i).itemName.trim());
                    qty.setText(_closeorderItemsArrayList.get(i).quantity);
                    amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).amount)));
                    itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));
                }

                customList.addView(customView);
            }
            GrantTotal = servicetax + vat + billAmt + cessTax + salesTax;

            tot_tax.setText(format.format(billAmt));
            /*ser_tax.setText(format.format(servicetax));
            sales_tax.setText(format.format(salesTax));
            vat_tax.setText(format.format(vat));
            c_tax.setText(format.format(cessTax));*/
            g_tot.setText(format.format(GrantTotal));


            if (cardType.equals("CASH CARD") && BillingProfile.isCashCardFilled.equalsIgnoreCase("false")) {
                a_bal.setText("0");

            } else if (cardType.equals("CASH CARD")) {
                a_bal.setText("-" + ava_balance);

            } else
                a_bal.setText(ava_balance);
        } catch (Exception e) {
            String r = e.getMessage().toString();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        if (data != null && resultCode == RESULT_OK) {
            String target = data.getStringExtra(getString(R.string.title_target));
            String printer = data.getStringExtra(getString(R.string.title_Printer));
            if (printer != null) {
                EditText mEdtTarget = (EditText) findViewById(R.id.edtTarget1);
                mEdtTarget.setText(printer + "-" + target);

            }
        }
    }

    public void closeOrder_OthersNew() {
        servicetax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        DecimalFormat format = new DecimalFormat("#.##");
        ArrayList<CloseorderItems> _closeorderItemsArrayList = KotItemlist.closeorderItemsArrayList_Others;
        taxlist = new ArrayList<>();

        for (int i = 0; i < _closeorderItemsArrayList.size(); i++) {
            // if(i== _closeorderItemsArrayList.size()-1 && billType.equals("BOT") ){
            if (i == _closeorderItemsArrayList.size() - 1) {
                if (_closeorderItemsArrayList.get(i).getTaxAmount().equals("")) {

                } else {
                    try {
                        if (_closeorderItemsArrayList.get(i).getTaxAmount().contains("-")) {
                            String[] tax = _closeorderItemsArrayList.get(i).getTaxAmount().split("-");
                            for (int j = 0; j < tax.length; j++) {
                                String dummy[] = tax[j].split(",");

                                if (dummy[1].equals("Notax")) {
                                    taxlist.add("Tax" + "-" + format.format(Double.parseDouble(dummy[0])));
                                } else {
                                    taxlist.add(dummy[1] + "-" + format.format(Double.parseDouble(dummy[0])));
                                }


                                servicetax = servicetax + Double.parseDouble(dummy[0]);
                                _service = format.format(Double.parseDouble(dummy[0]));
                            }
                        } else {
                            String[] tax = _closeorderItemsArrayList.get(i).getTaxAmount().split(",");
                            for (int j = 0; j < tax.length; j++) {
                                if (tax[1].equals("Service")) {
                                    servicetax = servicetax + Double.parseDouble(tax[0]);
                                    _service = format.format(Double.parseDouble(tax[0]));
                                } else if (tax[1].equals("Vat")) {
                                    vat = vat + Double.parseDouble(tax[0]);
                                    _vat = format.format(Double.parseDouble(tax[0]));
                                } else {
                                    salesTax = salesTax + Double.parseDouble(tax[0]);
                                    _sales = format.format(Double.parseDouble(tax[0]));
                                }
                            }
                        }
                    } catch (Exception e) {
                        String r = e.getMessage().toString();
                    }
                }


            } else {

                billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(i).getAmount());
                ava_balance = _closeorderItemsArrayList.get(i).getAva_balance();
                if (customList == null) {

                    customList = (LinearLayout) findViewById(R.id.closeItemView);
                    customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                    itemName = (TextView) customView.findViewById(R.id.itmName);
                    qty = (TextView) customView.findViewById(R.id.itmQty);
                    amount = (TextView) customView.findViewById(R.id.itmAmt);
                    itemCount = (TextView) customView.findViewById(R.id.itmrate);

                    itemName.setText(_closeorderItemsArrayList.get(i).itemname);
                    qty.setText(_closeorderItemsArrayList.get(i).Quantity);
                    amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).Amount)));
                    itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));

                } else {
                    customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                    itemName = (TextView) customView.findViewById(R.id.itmName);
                    qty = (TextView) customView.findViewById(R.id.itmQty);
                    amount = (TextView) customView.findViewById(R.id.itmAmt);
                    itemCount = (TextView) customView.findViewById(R.id.itmrate);

                    itemName.setText(_closeorderItemsArrayList.get(i).itemname);
                    qty.setText(_closeorderItemsArrayList.get(i).Quantity);
                    amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).Amount)));
                    itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));
                }

                customList.addView(customView);

            }

        }

        GrantTotal = servicetax + billAmt;
        billdetnum.setText((_closeorderItemsArrayList.get(0).getBillDetNumber()));
        tot_tax.setText(format.format(billAmt));
        g_tot.setText(format.format(GrantTotal));

        if (cardType.equals("CASH CARD") && BillingProfile.isCashCardFilled.equalsIgnoreCase("false")) {
            a_bal.setText("0");

        } else if (cardType.equals("CASH CARD")) {
            a_bal.setText("-" + ava_balance);

        } else
            a_bal.setText(ava_balance);
        for (int k = 0; k < taxlist.size(); k++) {
            if (customTaxList == null) {
                customTaxList = (LinearLayout) findViewById(R.id.taxView);
                customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);

                taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                taxValue = (TextView) customTaxView.findViewById(R.id.tax);

                String dummy[] = taxlist.get(k).split("-");
                taxHeader.setText(dummy[0] + " :");
                taxValue.setText(dummy[1]);

            } else {
                customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);

                taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                taxValue = (TextView) customTaxView.findViewById(R.id.tax);

                String dummy[] = taxlist.get(k).split("-");
                taxHeader.setText(dummy[0]);
                taxValue.setText(dummy[1]);
            }
            customTaxList.addView(customTaxView);
        }
    }

    //monica
       /* public void closeOrder() {
            accharges = 0;
            servicetax = 0;
            vat = 0;
            billAmt = 0;
            GrantTotal = 0;
            DecimalFormat format = new DecimalFormat("#.##");
            ArrayList<CloseorderItems> _closeorderItemsArrayList = null;
            if (page == 0) {
                _closeorderItemsArrayList = BillingProfile.closeorderItemsArrayList;
            } else {
                _closeorderItemsArrayList = MakeOrder.closeorderItemsArrayList;
            }
            taxlist = new ArrayList<>();
            for (int i = 0; i < _closeorderItemsArrayList.size(); i++) {
                // if(i== _closeorderItemsArrayList.size()-1 && billType.equals("BOT") ){
                if (i == _closeorderItemsArrayList.size() - 1) {
                    if (_closeorderItemsArrayList.get(i).getTaxAmount().equals("")) {

                    } else {
                        try {
                            if (_closeorderItemsArrayList.get(i).getTaxAmount().contains("-")) {

                      *//*  if(!_closeorderItemsArrayList.get(i).getACCharge().equals("0")){
                            taxlist.add("AC Charge"+"-"+format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).getACCharge())));

                        }
                        *//*
                                String[] tax = _closeorderItemsArrayList.get(i).getTaxAmount().split("-");
                                for (int j = 0; j < tax.length; j++) {
                                    String dummy[] = tax[j].split(",");

                                    // taxlist.add(dummy[1]+"-"+format.format(Double.parseDouble(dummy[0])));


                                    if (dummy[1].equals("Notax")) {
                                        taxlist.add("Tax" + "-" + format.format(Double.parseDouble(dummy[0])));
                                    } else {
                                        taxlist.add(dummy[1] + "-" + format.format(Double.parseDouble(dummy[0])));
                                    }


                                    servicetax = servicetax + Double.parseDouble(dummy[0]);
                                    _service = format.format(Double.parseDouble(dummy[0]));




                            *//* if(dummy[1].contains("Service")){
                                servicetax=servicetax+Double.parseDouble(dummy[0]);
                                _service= format.format(Double.parseDouble(dummy[0]));

                            }else if (dummy[1].contains("Vat")){
                                vat=vat+Double.parseDouble(dummy[0]);
                                _vat= format.format(Double.parseDouble(dummy[0]));

                            }else{
                                salesTax=salesTax+Double.parseDouble(dummy[0]);
                                _sales= format.format(Double.parseDouble(dummy[0]));
                            }*//*
                                }


                            } else {
                                String[] tax = _closeorderItemsArrayList.get(i).getTaxAmount().split(",");
                                for (int j = 0; j < tax.length; j++) {

                                    // taxlist.put();
                                    if (tax[1].equals("Service")) {
                                        servicetax = servicetax + Double.parseDouble(tax[0]);
                                        _service = format.format(Double.parseDouble(tax[0]));
                                    } else if (tax[1].equals("Vat")) {
                                        vat = vat + Double.parseDouble(tax[0]);
                                        _vat = format.format(Double.parseDouble(tax[0]));
                                    } else {
                                        salesTax = salesTax + Double.parseDouble(tax[0]);
                                        _sales = format.format(Double.parseDouble(tax[0]));
                                    }
                                }
                            }
                        } catch (Exception e) {
                            String r = e.getMessage().toString();
                        }
                    }


                } else {

                    billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(i).getAmount());
                    ava_balance = _closeorderItemsArrayList.get(i).getAva_balance();

//ac charges
                    if (!_closeorderItemsArrayList.get(0).getACCharge().equals("0")) {
                        taxlist.add("AC Charge" + "-" + format.format(Double.parseDouble(_closeorderItemsArrayList.get(0).getACCharge())));
                    }

                    if (customList == null) {

                        customList = (LinearLayout) findViewById(R.id.closeItemView);
                        customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                        itemName = (TextView) customView.findViewById(R.id.itmName);
                        qty = (TextView) customView.findViewById(R.id.itmQty);
                        amount = (TextView) customView.findViewById(R.id.itmAmt);
                        itemCount = (TextView) customView.findViewById(R.id.itmrate);

                        itemName.setText(_closeorderItemsArrayList.get(i).itemname);
                        qty.setText(_closeorderItemsArrayList.get(i).Quantity);
                        amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).Amount)));
                        itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));

                    }
//            else if(i==10)
//            {
//                customView=getLayoutInflater().inflate(R.layout.close_order_itemlist,customList,false);
//                itemName= (TextView) customView.findViewById(R.id.itmName);
//                qty=(TextView) customView.findViewById(R.id.itmQty);
//                amount=(TextView)customView.findViewById(R.id.itmAmt);
//                itemName.setText("TOTAL");
//                qty.setText("");
//                amount.setText(10*100+"");
//            }
                    else {
                        customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                        itemName = (TextView) customView.findViewById(R.id.itmName);
                        qty = (TextView) customView.findViewById(R.id.itmQty);
                        amount = (TextView) customView.findViewById(R.id.itmAmt);
                        itemCount = (TextView) customView.findViewById(R.id.itmrate);

                        itemName.setText(_closeorderItemsArrayList.get(i).itemname);
                        qty.setText(_closeorderItemsArrayList.get(i).Quantity);
                        amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).Amount)));
                        itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));
                    }

                    customList.addView(customView);

                }

            }
//monica
            //GrantTotal=servicetax+vat+billAmt+cessTax+salesTax;
            GrantTotal = servicetax + billAmt + Double.parseDouble(_closeorderItemsArrayList.get(0).getACCharge());
            billdetnum.setText((_closeorderItemsArrayList.get(0).getBillDetNumber()));
            tot_tax.setText(format.format(billAmt));
            g_tot.setText(format.format(GrantTotal));


       *//* if (cardType.equals("CASH CARD") && TakeOrder.isCashCardFilled.equalsIgnoreCase("false")){
            a_bal.setText("0");

        }else
        if (cardType.equals("CASH CARD")){
            a_bal.setText("-"+ava_balance);

        }else*//*
            a_bal.setText(ava_balance);

       *//* ser_tax.setText(format.format(servicetax));
        sales_tax.setText(format.format(salesTax));
        vat_tax.setText(format.format(vat));
        c_tax.setText(format.format(cessTax));
        c_tax.setVisibility(View.GONE);*//*


            for (int k = 0; k < taxlist.size(); k++) {
                if (customTaxList == null) {
                    customTaxList = (LinearLayout) findViewById(R.id.taxView);
                    customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);

                    taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                    taxValue = (TextView) customTaxView.findViewById(R.id.tax);

                    String dummy[] = taxlist.get(k).split("-");
                    taxHeader.setText(dummy[0] + " :");
                    taxValue.setText(dummy[1]);


                } else {
                    customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);

                    taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                    taxValue = (TextView) customTaxView.findViewById(R.id.tax);

                    String dummy[] = taxlist.get(k).split("-");
                    taxHeader.setText(dummy[0]);
                    taxValue.setText(dummy[1]);
                }
                customTaxList.addView(customTaxView);

            }


        }*/
    public void closeOrder() {
        accharges = 0;
        servicetax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        DecimalFormat format = new DecimalFormat("#.##");
        ArrayList<CloseorderItems> _closeorderItemsArrayList = null;
        if (page == 0) {
            _closeorderItemsArrayList = BillingProfile.closeorderItemsArrayList;
        } else {
            _closeorderItemsArrayList = MakeOrder.closeorderItemsArrayList;
        }
        taxlist = new ArrayList<>();
        for (int i = 0; i < _closeorderItemsArrayList.size(); i++) {
            // if(i== _closeorderItemsArrayList.size()-1 && billType.equals("BOT") ){
            if (i == _closeorderItemsArrayList.size() - 1) {
                if (_closeorderItemsArrayList.get(i).getTaxAmount().equals("")) {

                } else {
                    try {
                        if (_closeorderItemsArrayList.get(i).getTaxAmount().contains("-")) {

                      /*  if(!_closeorderItemsArrayList.get(i).getACCharge().equals("0")){
                            taxlist.add("AC Charge"+"-"+format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).getACCharge())));

                        }
                        */
                            String[] tax = _closeorderItemsArrayList.get(i).getTaxAmount().split("-");
                            for (int j = 0; j < tax.length; j++) {
                                String dummy[] = tax[j].split(",");

                                // taxlist.add(dummy[1]+"-"+format.format(Double.parseDouble(dummy[0])));


                                if (dummy[1].equals("Notax")) {
                                    taxlist.add("Tax" + "-" + format.format(Double.parseDouble(dummy[0])));
                                } else {
                                    taxlist.add(dummy[1] + "-" + format.format(Double.parseDouble(dummy[0])));
                                }


                                servicetax = servicetax + Double.parseDouble(dummy[0]);
                                _service = format.format(Double.parseDouble(dummy[0]));




                            /* if(dummy[1].contains("Service")){
                                servicetax=servicetax+Double.parseDouble(dummy[0]);
                                _service= format.format(Double.parseDouble(dummy[0]));

                            }else if (dummy[1].contains("Vat")){
                                vat=vat+Double.parseDouble(dummy[0]);
                                _vat= format.format(Double.parseDouble(dummy[0]));

                            }else{
                                salesTax=salesTax+Double.parseDouble(dummy[0]);
                                _sales= format.format(Double.parseDouble(dummy[0]));
                            }*/
                            }


                        } else {
                            String[] tax = _closeorderItemsArrayList.get(i).getTaxAmount().split(",");
                            for (int j = 0; j < tax.length; j++) {

                                // taxlist.put();
                                if (tax[1].equals("Service")) {
                                    servicetax = servicetax + Double.parseDouble(tax[0]);
                                    _service = format.format(Double.parseDouble(tax[0]));
                                } else if (tax[1].equals("Vat")) {
                                    vat = vat + Double.parseDouble(tax[0]);
                                    _vat = format.format(Double.parseDouble(tax[0]));
                                } else {
                                    salesTax = salesTax + Double.parseDouble(tax[0]);
                                    _sales = format.format(Double.parseDouble(tax[0]));
                                }
                            }
                        }
                    } catch (Exception e) {
                        String r = e.getMessage().toString();
                    }
                }


            } else {

                billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(i).getAmount());
                ava_balance = _closeorderItemsArrayList.get(i).getAva_balance();

//ac charges
                if (!_closeorderItemsArrayList.get(0).getACCharge().equals("0")) {
                    taxlist.add("AC Charge" + "-" + format.format(Double.parseDouble(_closeorderItemsArrayList.get(0).getACCharge())));
                }

                if (customList == null) {

                    customList = (LinearLayout) findViewById(R.id.closeItemView);
                    customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                    itemName = (TextView) customView.findViewById(R.id.itmName);
                    qty = (TextView) customView.findViewById(R.id.itmQty);
                    amount = (TextView) customView.findViewById(R.id.itmAmt);
                    itemCount = (TextView) customView.findViewById(R.id.itmrate);
                    //kot_rate= (TextView) findViewById(R.id.kot_rate);
                    //  kot_rate.setVisibility(View.INVISIBLE);
                    itemName.setText(_closeorderItemsArrayList.get(i).itemname);
                    qty.setText(_closeorderItemsArrayList.get(i).Quantity);
                    amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).Amount)));
                    itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));

                }
//            else if(i==10)
//            {
//                customView=getLayoutInflater().inflate(R.layout.close_order_itemlist,customList,false);
//                itemName= (TextView) customView.findViewById(R.id.itmName);
//                qty=(TextView) customView.findViewById(R.id.itmQty);
//                amount=(TextView)customView.findViewById(R.id.itmAmt);
//                itemName.setText("TOTAL");
//                qty.setText("");
//                amount.setText(10*100+"");
//            }
                else {
                    customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                    itemName = (TextView) customView.findViewById(R.id.itmName);
                    qty = (TextView) customView.findViewById(R.id.itmQty);
                    amount = (TextView) customView.findViewById(R.id.itmAmt);
                    itemCount = (TextView) customView.findViewById(R.id.itmrate);
                    // kot_rate= (TextView) findViewById(R.id.kot_rate);
                    //   kot_rate.setVisibility(View.INVISIBLE);
                    itemName.setText(_closeorderItemsArrayList.get(i).itemname);
                    qty.setText(_closeorderItemsArrayList.get(i).Quantity);
                    amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).Amount)));
                    itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));
                }

                customList.addView(customView);

            }

        }
//monica
        //GrantTotal=servicetax+vat+billAmt+cessTax+salesTax;
        GrantTotal = servicetax + billAmt + Double.parseDouble(_closeorderItemsArrayList.get(0).getACCharge());
        billdetnum.setText((_closeorderItemsArrayList.get(0).getBillDetNumber()));
        tot_tax.setText(format.format(billAmt));
        g_tot.setText(format.format(GrantTotal));


       /* if (cardType.equals("CASH CARD") && TakeOrder.isCashCardFilled.equalsIgnoreCase("false")){
            a_bal.setText("0");

        }else
        if (cardType.equals("CASH CARD")){
            a_bal.setText("-"+ava_balance);

        }else*/
        a_bal.setText(ava_balance);

       /* ser_tax.setText(format.format(servicetax));
        sales_tax.setText(format.format(salesTax));
        vat_tax.setText(format.format(vat));
        c_tax.setText(format.format(cessTax));
        c_tax.setVisibility(View.GONE); */


        for (int k = 0; k < taxlist.size(); k++) {
            if (customTaxList == null) {
                customTaxList = (LinearLayout) findViewById(R.id.taxView);
                customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);

                taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                taxValue = (TextView) customTaxView.findViewById(R.id.tax);

                String dummy[] = taxlist.get(k).split("-");
                taxHeader.setText(dummy[0] + " :");
                taxValue.setText(dummy[1]);


            } else {
                customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);

                taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                taxValue = (TextView) customTaxView.findViewById(R.id.tax);

                String dummy[] = taxlist.get(k).split("-");
                taxHeader.setText(dummy[0]);
                taxValue.setText(dummy[1]);
            }
            customTaxList.addView(customTaxView);

        }


    }

    public void closeOrder1() {
        servicetax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        DecimalFormat format = new DecimalFormat("#.##");
        ArrayList<CloseorderItems> _closeorderItemsArrayList = KotItemlist.closeorderItemsArrayList_Others;
        taxlist = new ArrayList<>();

        for (int i = 0; i < _closeorderItemsArrayList.size(); i++) {
            // if(i== _closeorderItemsArrayList.size()-1 && billType.equals("BOT") ){
            if (i == _closeorderItemsArrayList.size() - 1) {
                if (_closeorderItemsArrayList.get(i).getTaxAmount().equals("")) {

                } else {
                    try {
                        if (_closeorderItemsArrayList.get(i).getTaxAmount().contains("-")) {
                            String[] tax = _closeorderItemsArrayList.get(i).getTaxAmount().split("-");
                            for (int j = 0; j < tax.length; j++) {
                                String dummy[] = tax[j].split(",");

                                // taxlist.add(dummy[1]+"-"+format.format(Double.parseDouble(dummy[0])));


                                if (dummy[1].equals("Notax")) {
                                    taxlist.add("Tax" + "-" + format.format(Double.parseDouble(dummy[0])));
                                } else {
                                    taxlist.add(dummy[1] + "-" + format.format(Double.parseDouble(dummy[0])));
                                }


                                servicetax = servicetax + Double.parseDouble(dummy[0]);
                                _service = format.format(Double.parseDouble(dummy[0]));
                            /* if(dummy[1].contains("Service")){
                                servicetax=servicetax+Double.parseDouble(dummy[0]);
                                _service= format.format(Double.parseDouble(dummy[0]));

                            }else if (dummy[1].contains("Vat")){
                                vat=vat+Double.parseDouble(dummy[0]);
                                _vat= format.format(Double.parseDouble(dummy[0]));

                            }else{
                                salesTax=salesTax+Double.parseDouble(dummy[0]);
                                _sales= format.format(Double.parseDouble(dummy[0]));
                            }*/
                            }
                        } else {
                            String[] tax = _closeorderItemsArrayList.get(i).getTaxAmount().split(",");
                            for (int j = 0; j < tax.length; j++) {

                                // taxlist.put();
                                if (tax[1].equals("Service")) {
                                    servicetax = servicetax + Double.parseDouble(tax[0]);
                                    _service = format.format(Double.parseDouble(tax[0]));
                                } else if (tax[1].equals("Vat")) {
                                    vat = vat + Double.parseDouble(tax[0]);
                                    _vat = format.format(Double.parseDouble(tax[0]));
                                } else {
                                    salesTax = salesTax + Double.parseDouble(tax[0]);
                                    _sales = format.format(Double.parseDouble(tax[0]));
                                }
                            }
                        }
                    } catch (Exception e) {
                        String r = e.getMessage().toString();
                    }
                }


            } else {
                /*if(!_closeorderItemsArrayList.get(0).getACCharge().equals("0")){
                    taxlist.add("AC Charge"+"-"+format.format(Double.parseDouble(_closeorderItemsArrayList.get(0).getACCharge())));
                }*/
                billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(i).getAmount());
                // ava_balance=_closeorderItemsArrayList.get(i).getAva_balance();
                if (customList == null) {

                    customList = (LinearLayout) findViewById(R.id.closeItemView);
                    customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                    itemName = (TextView) customView.findViewById(R.id.itmName);
                    qty = (TextView) customView.findViewById(R.id.itmQty);
                    amount = (TextView) customView.findViewById(R.id.itmAmt);
                    itemCount = (TextView) customView.findViewById(R.id.itmrate);

                    itemName.setText(_closeorderItemsArrayList.get(i).itemname);
                    qty.setText(_closeorderItemsArrayList.get(i).Quantity);
                    amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).Amount)));
                    itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));

                }
//            else if(i==10)
//            {
//                customView=getLayoutInflater().inflate(R.layout.close_order_itemlist,customList,false);
//                itemName= (TextView) customView.findViewById(R.id.itmName);
//                qty=(TextView) customView.findViewById(R.id.itmQty);
//                amount=(TextView)customView.findViewById(R.id.itmAmt);
//                itemName.setText("TOTAL");
//                qty.setText("");
//                amount.setText(10*100+"");
//            }
                else {
                    customView = getLayoutInflater().inflate(R.layout.report_list, customList, false);

                    itemName = (TextView) customView.findViewById(R.id.itmName);
                    qty = (TextView) customView.findViewById(R.id.itmQty);
                    amount = (TextView) customView.findViewById(R.id.itmAmt);
                    itemCount = (TextView) customView.findViewById(R.id.itmrate);

                    itemName.setText(_closeorderItemsArrayList.get(i).itemname);
                    qty.setText(_closeorderItemsArrayList.get(i).Quantity);
                    amount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).Amount)));
                    itemCount.setText(format.format(Double.parseDouble(_closeorderItemsArrayList.get(i).rate)));
                }

                customList.addView(customView);

            }

        }

        //GrantTotal=servicetax+vat+billAmt+cessTax+salesTax;
        GrantTotal = servicetax + billAmt + Double.parseDouble(_closeorderItemsArrayList.get(0).getACCharge());
        tot_tax.setText(format.format(billAmt));
        g_tot.setText(format.format(GrantTotal));


        if (cardType.equals("CASH CARD") && BillingProfile.isCashCardFilled.equalsIgnoreCase("false")) {
            a_bal.setText("0");

        } else if (cardType.equals("CASH CARD")) {
            a_bal.setText("-" + ava_balance);

        } else
            a_bal.setText(ava_balance);

       /* ser_tax.setText(format.format(servicetax));
        sales_tax.setText(format.format(salesTax));
        vat_tax.setText(format.format(vat));
        c_tax.setText(format.format(cessTax));
        c_tax.setVisibility(View.GONE);*/


        for (int k = 0; k < taxlist.size(); k++) {
            if (customTaxList == null) {
                customTaxList = (LinearLayout) findViewById(R.id.taxView);
                customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);

                taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                taxValue = (TextView) customTaxView.findViewById(R.id.tax);

                String dummy[] = taxlist.get(k).split("-");
                taxHeader.setText(dummy[0] + " :");
                taxValue.setText(dummy[1]);


            } else {
                customTaxView = getLayoutInflater().inflate(R.layout.taxview, customTaxList, false);

                taxHeader = (TextView) customTaxView.findViewById(R.id.h_tax);
                taxValue = (TextView) customTaxView.findViewById(R.id.tax);

                String dummy[] = taxlist.get(k).split("-");
                taxHeader.setText(dummy[0]);
                taxValue.setText(dummy[1]);
            }
            customTaxList.addView(customTaxView);

        }
    }

    public void cancelOrder(CharSequence value,int position) {
        try {


            if(customList!=null){
                customList.removeAllViews();
            }
            bluetooth.setTag(R.string.firstvalue,position);

            //   ArrayList<CloseorderItems> _closeorderItemsArrayList = BillingProfile.closeorderItemsArrayList;
            _closeorderItemsArrayList = new ArrayList<>();
            for (int i=0;i<BillingProfile.closeorderItemsArrayList.size();i++)
            {
                if(BillingProfile.closeorderItemsArrayList.get(i).getBillno().equalsIgnoreCase(String.valueOf(value))){
                    _closeorderItemsArrayList.add(BillingProfile.closeorderItemsArrayList.get(i));
                    Log.d("Billing",BillingProfile.closeorderItemsArrayList.get(i).getOtno());
                }
            }
            for (int i = 0; i < _closeorderItemsArrayList.size(); i++) {
                if (customList == null) {
                    rate.setVisibility(View.GONE);
                    customList = (LinearLayout) findViewById(R.id.closeItemView);
                    customView = getLayoutInflater().inflate(R.layout.itemlist_kot, customList, false);
                    itemName = (TextView) customView.findViewById(R.id.kot_itemname);
                    itemCount = (TextView) customView.findViewById(R.id.kot_itemCount);
                    // happyhour = (CheckBox) customView.findViewById(R.id.happyhour_checbox);
                    // happyhour.setVisibility(View.GONE);
                    add = (ImageView) customView.findViewById(R.id.kot_add);
                    subtract = (ImageView) customView.findViewById(R.id.kot_minus);
                    add.setVisibility(View.GONE);
                    // itemName.setText("Item"+i);
                    //itemCount.setText(""+i);
                    itemName.setText(_closeorderItemsArrayList.get(i).ItemCode + "-" + _closeorderItemsArrayList.get(i).itemname);
                    itemCount.setText(_closeorderItemsArrayList.get(i).Quantity);
                    itemName.setTag(R.string.Itemid, _closeorderItemsArrayList.get(i).ItemID);
                    itemName.setTag(R.string.Itemserialno, _closeorderItemsArrayList.get(i).ItemSerialNo);
                    itemName.setTag(R.string.Itemcode, _closeorderItemsArrayList.get(i).ItemCode);
                    itemName.setTag(R.string.isFreeitem,"0");

                    subtract.setId(i);
                    if(_closeorderItemsArrayList.get(i).isFreeitem.equalsIgnoreCase("1")){
                        customView.setBackgroundColor(getResources().getColor(R.color.free));
                        itemName.setTag(R.string.isFreeitem,"1");

                    }

                } else {
                    rate.setVisibility(View.GONE);
                    customView = getLayoutInflater().inflate(R.layout.itemlist_kot, customList, false);
                    itemName = (TextView) customView.findViewById(R.id.kot_itemname);
                    itemCount = (TextView) customView.findViewById(R.id.kot_itemCount);
                    //happyhour = (CheckBox) customView.findViewById(R.id.happyhour_checbox);
                    //   happyhour.setVisibility(View.GONE);
                    add = (ImageView) customView.findViewById(R.id.kot_add);
                    add.setVisibility(View.GONE);
                    subtract = (ImageView) customView.findViewById(R.id.kot_minus);
//                itemName.setText("Item"+i);
//                itemCount.setText(""+i);
                    itemName.setText(_closeorderItemsArrayList.get(i).ItemCode + "-" + _closeorderItemsArrayList.get(i).itemname);
                    itemCount.setText(_closeorderItemsArrayList.get(i).Quantity);
                    itemName.setTag(R.string.Itemid, _closeorderItemsArrayList.get(i).ItemID);
                    itemName.setTag(R.string.Itemserialno, _closeorderItemsArrayList.get(i).ItemSerialNo);
                    itemName.setTag(R.string.Itemcode, _closeorderItemsArrayList.get(i).ItemCode);
                    itemName.setTag(R.string.isFreeitem,"0");

                    subtract.setId(i);
                    if(_closeorderItemsArrayList.get(i).isFreeitem.equalsIgnoreCase("1")){
                        customView.setBackgroundColor(getResources().getColor(R.color.free));
                        itemName.setTag(R.string.isFreeitem,"1");

                    }
                }
                if(_closeorderItemsArrayList.get(i).isFreeitem.equalsIgnoreCase("1")){
                    customView.setBackgroundColor(getResources().getColor(R.color.free));
                    itemName.setTag(R.string.isFreeitem,"1");

                }
                customList.addView(customView);
//            add.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int id = v.getId();
//                    ViewGroup parent = (ViewGroup) v.getParent();
//                    TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
//                    String count = t1.getText().toString();
//                    if (count.equals("")) {
//
//                        t1.setText("1");
//
//                    } else {
//                        int c = Integer.parseInt(count);
//                        t1.setText(c + 1 + "");
//
//                    }
//
//
//                }
//            });
                subtract.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewGroup parent = (ViewGroup) v.getParent();
                        TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                        TextView t2 = (TextView) parent.findViewById(R.id.kot_itemname);
                        String count = t1.getText().toString();
                        String itemid = t2.getTag(R.string.Itemid).toString();
                        String parentserialno = t2.getTag(R.string.Itemserialno).toString();
                        String itemcode = t2.getTag(R.string.Itemcode).toString();
                        if (count.equals("")) {


                        } else {
                            int c = Integer.parseInt(count);
                            if (c != 0) {
                                int qty =  (c - 1);
                              //  t1.setText(qty + "");
                                CloseorderItems closeorderItemsmain=new CloseorderItems();
                                closeorderItemsmain=_closeorderItemsArrayList.get(v.getId());
                                _closeorderItemsArrayList.remove(v.getId());
                                closeorderItemsmain.setIsFreeitem("0");
                                closeorderItemsmain.setQuantity(qty+"");
                                _closeorderItemsArrayList.add(v.getId(),closeorderItemsmain);
                                 for (int x = 0; x <_closeorderItemsArrayList.size(); x++) {
                                    if (_closeorderItemsArrayList.get(x).isFreeitem.equalsIgnoreCase("1") && _closeorderItemsArrayList.get(x).BuyItemId.equalsIgnoreCase(itemid) && _closeorderItemsArrayList.get(x).ParentItemSerialNo.equalsIgnoreCase(parentserialno)) {

                                        CloseorderItems closeorderItems=new CloseorderItems();
                                        closeorderItems=_closeorderItemsArrayList.get(x);
                                        _closeorderItemsArrayList.remove(x);
                                        Dbase dbase=new Dbase(CloseOrderdetails.this);
                                        //checkOffermethodCancelOrder(String itemcode, String Quantity, String itemname, String rate)
                                      int Mainqty= Integer.parseInt(t1.getText().toString());
                                     String freeqty=   dbase.checkOffermethodforcancelloption(itemcode,qty+"","","");
                                      if(freeqty!=null && !freeqty.equalsIgnoreCase("")){
                                             /// && !freeqty.equalsIgnoreCase("0")){
                                          closeorderItems.setIsFreeitem("1");
                                         closeorderItems.setQuantity(freeqty);
                                         _closeorderItemsArrayList.add(x,closeorderItems);
                                     }
                                    }

                                }
                            }
                        }
                        customList.removeAllViews();
                        cancelOrder(value,position);
                    }

                });

          /*      subtract.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {

                        ViewGroup parent = (ViewGroup) v.getParent();
                        TextView t1 = (TextView) parent.findViewById(R.id.kot_itemCount);
                        String count = t1.getText().toString();
                        if (count.equals("")) {


                        } else {
                            int c = Integer.parseInt(count);
                            if (c != 0) {
                                t1.setText(0 + "");

                            }
                        }
                        return true;
                    }
                });*/

            }
        } catch (Exception e) {
            String mm = e + "";
        }
    }

    public class AsyncCancelOrder extends AsyncTask<String, Void, String> {
        String billId = "";
        String memberId = "";
        String status = "";
        String storeId = shafreepreferences.getString("storeId", "");
        JSONObject jsonObject;

        @Override
        protected String doInBackground(String... params) {

            try {
                RestAPI api = new RestAPI(CloseOrderdetails.this);
                db = new Dbase(CloseOrderdetails.this);
            //    ArrayList<CloseorderItems> _closeorderItemsArrayList = BillingProfile.closeorderItemsArrayList;

                cancle_order = new ArrayList<ItemList>();

                AndroidTempbilling obj = new AndroidTempbilling();
                ArrayList<Object> itemList_main = new ArrayList<>();
                AndroidTempbilldetails ob = null;

                if (customList != null) {
                    //itemView.bringChildToFront(v);
                    int childRow = customList.getChildCount();
                    for (int i = 0; i < childRow; i++) {
                        billId = _closeorderItemsArrayList.get(0).BillID;
                        memberId = _closeorderItemsArrayList.get(0).memberID;
                        View ItemRow = customList.getChildAt(i);
                        TextView t = (TextView) ItemRow.findViewById(R.id.kot_itemCount);
                        TextView t1 = (TextView) ItemRow.findViewById(R.id.kot_itemname);
                        String Itemcount = t.getText().toString();
                        if (!Itemcount.equals("")) {
                            String itemname = t1.getText().toString();
                            String[] spl = itemname.split("-");
                            // String _itemName = spl[1];
                            String _itemCode = spl[0];
                            ArrayList<String> dummy = new ArrayList<String>();
                            List<String> itemId = db.getItemid(_itemCode);
                            // String  itemRate=db.getItemRate(_itemCode);
                            ob = new AndroidTempbilldetails();

                            float salesTax = 0;
                            float serviceTax = 0;
                            float cessTax = 0;

                            ob.setSalestax(0);  // unwanted
                            ob.setServicetax(0);
                            ob.setCesstax(0);
                            ob.setTaxId(0);
                            ob.setTaxRate(0);
                            ob.setAlltax(dummy);

                            ob.setSalesUintid(Integer.parseInt(itemId.get(2)));
                            ob.setQuantity(Integer.parseInt(Itemcount));
                            ob.setRate(itemId.get(1));
                            String _itemName = itemId.get(3);
                            ob.setItemcode(_itemCode);
                            ob.setItemname(_itemName);
                            ob.setItemId(itemId.get(0));
                            ob.setItemCategoryid(itemId.get(4));
                            ob.setIsFreeitem(t1.getTag(R.string.isFreeitem).toString());


                            int v1_qty = Integer.parseInt(Itemcount);
                            float v2_rate = parseFloat(itemId.get(1));
                            float v3_itemamount = v1_qty * v2_rate;

                            ob.setAmount(v3_itemamount);
                            ob.setOtno(_closeorderItemsArrayList.get(i).otno);

                           /* if(!cardType.equals("CLUB CARD"))
                            {
                                //salesTax=(v3_itemamount*Float.parseFloat(itemId.get(2)))/100;
                                //serviceTax=(v3_itemamount*Float.parseFloat(itemId.get(4)))/100;
                                //cessTax=(v3_itemamount*Float.parseFloat(itemId.get(3)))/100;
                                ob.setSalestax(salesTax);
                                ob.setServicetax(serviceTax);
                                ob.setCesstax(cessTax);

                                ArrayList<String> dummy=new ArrayList<String>();

                                if(itemId.size()>7)
                                {
                                    int k=5;
                                    for(int j=0;j<(itemId.size()/7);j++)
                                    {
                                        float tax=(v3_itemamount*Float.parseFloat(itemId.get(k)))/100;
                                        dummy.add(tax+"");
                                        k=k+7;
                                    }
                                    ob.setAlltax(dummy);
                                }
                                else
                                {

                                  if(itemId.get(5).equals("")){
                                      ob.setAlltax(dummy);
                                  }else{
                                      float tax=(v3_itemamount*Float.parseFloat(itemId.get(5)))/100;
                                      dummy.add(tax+"");
                                      ob.setAlltax(dummy);
                                  }

                                }
                            }
                            else
                            {
                                ob.setSalestax(0);
                                ob.setServicetax(0);
                                ob.setCesstax(0);
                            }*/

                            Totalamount = Totalamount + ob.getAmount();
                            itemList_main.add(ob);

                        }
                        obj.setMemberId(memberId);
                        obj.setStoreId(storeId);
                        obj.setTableId(tableId + "");
                        obj.setTamount(Totalamount);
                    }

                    if (itemList_main.size() > 0) {
                        jsonObject = api.cms_getCancelItems(memberId, itemList_main, obj, billId);
                        status = jsonObject.toString();
                        JSONArray array = jsonObject.getJSONArray("Value");
                        ItemList it = null;

                        for (int i = 0; i < array.length(); i++) {
                            it = new ItemList();
                            JSONObject object = array.getJSONObject(i);

                            String Quantity = object.getString("Quantity");

                            String BillDate = object.getString("BillDate");
                            String itemname = object.getString("itemname");
                            // String memberId=object.getString("memberId");
                            String ItemCode = object.optString("ItemCode");
                            String otNo = object.optString("OTNo");


                            it.setItemName(itemname);
                            it.setItemCode(ItemCode);
                            it.setQuantity(Quantity);
                            it.setOtNo(otNo);
                            cancle_order.add(it);
                        }
                        status = jsonObject + "";
                    }
                }

            } catch (Exception e) {
                status = e.getMessage().toString();
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(CloseOrderdetails.this);
            pd.show();
            pd.setMessage("Please wait loading....");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (s.contains("true")) {

                if (cancle_order == null || cancle_order.size() == 0) {

                    print_butt.setVisibility(View.GONE);
                    print_buttnew.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Choose Item to cancel", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(getApplicationContext(), "Item canceled ", Toast.LENGTH_LONG).show();

                    //  print_butt.setVisibility(View.VISIBLE);
                    print_buttnew.setVisibility(View.VISIBLE);
                    cancelorder_butt.setVisibility(View.GONE);

                }
                // print_butt.setVisibility(View.VISIBLE);
                //   finish();
            } else {
                Toast.makeText(getApplicationContext(), "Item not canceled: " + status + "  " + jsonObject, Toast.LENGTH_LONG).show();
                //print_butt.setVisibility(View.GONE);
            }
        }
    }

    //***************************** PRINTER  *********************************//

    public boolean runPrintReceiptSequence() {
        if (!initializeObject()) {
            return false;
        }

        if (page == 0) {    // close order

            if (!createReceiptData()) {
                finalizeObject();
                return false;
            }

        } else if (page == 1) {   //cancel order (ot print )

            if (!createReceiptData1()) {
                finalizeObject();
                return false;
            }

        } else if (page == 23) {   //cancel order (ot print )

            if (!createReceiptData23()) {
                finalizeObject();
                return false;
            }
        } else {
            if (!createReceiptData3()) {   //other modules final bill
                finalizeObject();
                return false;
            }
        }


        if (!printData()) {
            finalizeObject();
            return false;
        }

        count = count + 1;
        return true;
    }

    public boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            return false;
        }

        PrinterStatusInfo status = mPrinter.getStatus();

        dispPrinterWarnings(status);

        if (!isPrintable(status)) {
            ShowMsg.showMsg(makeErrorMessage(status), mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    public boolean initializeObject() {
        try {
               /* mPrinter = new Printer(((SpnModelsItem) mSpnSeries.getSelectedItem()).getModelConstant(),
                        ((SpnModelsItem) mSpnLang.getSelectedItem()).getModelConstant(),
                        mContext);*/
            mPrinter = new Printer(Printer.TM_P20, Printer.MODEL_ANK, mContext);
            //     mPrinter = new Printer(Printer.TM_P80, Printer.MODEL_ANK, mContext);
        } catch (Exception e) {
            ShowMsg.showException(e, "Printer", mContext);
            return false;
        }

        mPrinter.setReceiveEventListener(this);

        return true;
    }

    public void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    public boolean connectPrinter() {
        boolean isBeginTransaction = false;
        String ll = "";

        if (mPrinter == null) {
            return false;
        }

        try {
            ll = printer_Name.getText().toString();
            if (!ll.equals("")) {
                mPrinter.connect(printer_Name.getText().toString(), Printer.PARAM_DEFAULT);
            } else {
                String[] l = ll.split("-");
                mPrinter.connect(l[1], Printer.PARAM_DEFAULT);
            }
        } catch (Exception e) {
            ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        try {
            mPrinter.beginTransaction();
            isBeginTransaction = true;
        } catch (Exception e) {
            ShowMsg.showException(e, "beginTransaction", mContext);
        }

        if (isBeginTransaction == false) {
            try {
                mPrinter.disconnect();
            } catch (Epos2Exception e) {
                // Do nothing
                return false;
            }
        }

        return true;
    }

    public void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "endTransaction", mContext);
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    ShowMsg.showException(e, "disconnect", mContext);
                }
            });
        }

        finalizeObject();
    }

    public boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            ;//print available
        }

        return true;
    }

    public String makeErrorMessage(PrinterStatusInfo status) {
        String msg = "";

        if (status.getOnline() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_offline);
        }
        if (status.getConnection() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_no_response);
        }
        if (status.getCoverOpen() == Printer.TRUE) {
            msg += getString(R.string.handlingmsg_err_cover_open);
        }
        if (status.getPaper() == Printer.PAPER_EMPTY) {
            msg += getString(R.string.handlingmsg_err_receipt_end);
        }
        if (status.getPaperFeed() == Printer.TRUE || status.getPanelSwitch() == Printer.SWITCH_ON) {
            msg += getString(R.string.handlingmsg_err_paper_feed);
        }
        if (status.getErrorStatus() == Printer.MECHANICAL_ERR || status.getErrorStatus() == Printer.AUTOCUTTER_ERR) {
            msg += getString(R.string.handlingmsg_err_autocutter);
            msg += getString(R.string.handlingmsg_err_need_recover);
        }
        if (status.getErrorStatus() == Printer.UNRECOVER_ERR) {
            msg += getString(R.string.handlingmsg_err_unrecover);
        }
        if (status.getErrorStatus() == Printer.AUTORECOVER_ERR) {
            if (status.getAutoRecoverError() == Printer.HEAD_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_head);
            }
            if (status.getAutoRecoverError() == Printer.MOTOR_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_motor);
            }
            if (status.getAutoRecoverError() == Printer.BATTERY_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_battery);
            }
            if (status.getAutoRecoverError() == Printer.WRONG_PAPER) {
                msg += getString(R.string.handlingmsg_err_wrong_paper);
            }
        }
        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_0) {
            msg += getString(R.string.handlingmsg_err_battery_real_end);
        }

        return msg;
    }

    public void dispPrinterWarnings(PrinterStatusInfo status) {
        EditText edtWarnings = (EditText) findViewById(R.id.edtWarnings1);

        String warningsMsg = "";

        if (status == null) {
            return;
        }

        if (status.getPaper() == Printer.PAPER_NEAR_END) {
            warningsMsg += getString(R.string.handlingmsg_warn_receipt_near_end);
        }

        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_1) {
            warningsMsg += getString(R.string.handlingmsg_warn_battery_near_end);
        }

        edtWarnings.setText(warningsMsg);
    }

    public void updateButtonState(boolean state) {
        Button btnReceipt = (Button) findViewById(R.id.button_cancelprint);
        //Button btnCoupon = (Button)findViewById(R.id.btnSampleCoupon);

        if (state == true) {
            if (count == 0) {
                btnReceipt.setEnabled(state);
                btnReceipt.setBackgroundColor(getResources().getColor(R.color.violet));
                btnReceipt.setText("Print");

            } else {
                btnReceipt.setEnabled(state);
                btnReceipt.setBackgroundColor(getResources().getColor(R.color.violet));
                btnReceipt.setText("Re-Print");

            }
        } else {
            btnReceipt.setBackgroundColor(getResources().getColor(R.color.black_semi_transparent));
            btnReceipt.setEnabled(state);
            btnReceipt.setText("Printing ...");
        }
        //  btnCoupon.setEnabled(state);
    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
//                ShowMsg.showResult(code, makeErrorMessage(status), mContext);

                dispPrinterWarnings(status);

                updateButtonState(true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        disconnectPrinter();
                    }
                }).start();
            }
        });
    }

//Cancel Order

    public boolean createReceiptData1() {
        ArrayList<CloseorderItems> _closeorderItemsArrayList1 = BillingProfile.closeorderItemsArrayList;
        String method = "";
        StringBuilder textData = new StringBuilder();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());

        if (mPrinter == null) {
            return false;
        }
        try {
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            //   textData.append("Cosmopolitan Club (Regd.)\n");
            textData.append(clubName + "\n");
            if (count == 0) {
                textData.append("KOT /BOT\n");
            } else {
                textData.append("KOT /BOT (RE-Print)\n");
            }
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            textData.append("--------------------------------------\n");
            textData.append("Table No.  " + tableNo + "\n");
            textData.append("--------------------------------------\n");

            if (billType.equals("KOT")) {
                textData.append("KOT No.    " + ": " + cancle_order.get(0).otNo + "\n");
            } else if (billType.equals("BOT")) {
                textData.append("BOT No.    " + ": " + cancle_order.get(0).otNo + "\n");
            }


            textData.append("Counter    " + ": " + counter_name + "\n");
            textData.append("Date Time  " + ": " + date + "\n");
            //   textData.append("Bill Clerk " + ": " + waiter + "\n");
            textData.append("Steward " + ": " + waiter + "\n");
            textData.append("Waiter     " + ": " + waiternamecode + "\n");
            textData.append("--------------------------------------\n");
            textData.append("Member      " + ":" + _closeorderItemsArrayList1.get(0).mName + "\n");
            textData.append("Member Code " + ":" + _closeorderItemsArrayList1.get(0).mAcc + "\n");
            String gst = shafreepreferences.getString("mstoreGST", "");
            textData.append("GST NO:" + ":" + gst + "\n");


            textData.append("--------------------------------------\n");
            textData.append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(0, 0, 0, Printer.COLOR_2);
           /* textData.append("Code  "
                           +" "
                           +"Item Name             "
                           +" "
                           +"Qty "
                           +" "
                           +"Rate "
                           +"\n"); */

            textData.append("Code  "
                    + " "
                    + "Item Name                   "
                           /*+" "
                           +"Qty "*/
                    + " "
                    + "Qty "
                    + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);

            for (int j = 0; j < cancle_order.size(); j++) {
                /*textData.append(
                                 (cancle_order.get(j).itemCode+"                ").substring(0,5)
                                +" "
                                +(cancle_order.get(j).itemName+"                                                                     ").substring(0,27)
                        *//* + " "
                          +(KotItemlist.placeOrder_list.get(j).quantity+"      ").substring(0,4)*//*
                                +" "
                                +(cancle_order.get(j).quantity+"          ").substring(0,4)
                                +"\n");*/
                textData.append(
                        (cancle_order.get(j).itemCode + "          ").substring(0, 6)
                                + " "
                                + String.format("%28s", (cancle_order.get(j).itemName + "                                   ").substring(0, 28))
                              /* + " "
                               +(KotItemlist.placeOrder_list.get(j).quantity+"      ").substring(0,4)*/
                                // +" "
                                + String.format("%4s", (cancle_order.get(j).quantity))
                                + "\n");
                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }


            textData.append("------------------------------------------\n");

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);

        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }

    public boolean createReceiptData1poona() {
        mSingleThreadExecutor = mDriverManager.getSingleThreadExecutor();
        mDriverManager = DriverManager.getInstance();
        mPrinter1 = mDriverManager.getPrinter();

        final ArrayList<CloseorderItems> _closeorderItemsArrayList1 = BillingProfile.closeorderItemsArrayList;
        String method = "";
        StringBuilder textData = new StringBuilder();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        final String date = format1.format(cal.getTime());
        mSingleThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                int printStatus = mPrinter1.getPrinterStatus();
                if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                    CloseOrderdetails.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CloseOrderdetails.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                        }
                    });
                } else {


                    PrnStrFormat format = new PrnStrFormat();
                    format.setTextSize(30);
                    format.setAli(Layout.Alignment.ALIGN_CENTER);
                    format.setFont(PrnTextFont.DEFAULT);
                    mPrinter1.setPrintAppendString(clubName, format);


                    if (clubaddress != null && !clubaddress.equals("")) {
                        format.setTextSize(25);
                        format.setAli(Layout.Alignment.ALIGN_CENTER);
                        format.setFont(PrnTextFont.DEFAULT);
                        mPrinter1.setPrintAppendString(clubaddress, format);

                    } else {
                        for (int k = 0; k < MainActivity.addressList.size(); k++) {
//                    textData.append(MainActivity.addressList.get(k) + "\n");
                        }
                    }

                    format.setTextSize(25);
                    format.setStyle(PrnTextStyle.NORMAL);
                    format.setAli(Layout.Alignment.ALIGN_NORMAL);

                    if (count == 0) {
                        //  textData.append("KOT /BOT\\n");
                        mPrinter1.setPrintAppendString("KOT /BOT", format);

                    } else {
                        mPrinter1.setPrintAppendString(" ", format);

                    }


                    mPrinter1.setPrintAppendString("------------------------------------------------------", format);
                    mPrinter1.setPrintAppendString("Table No.  " + tableNo, format);
                    mPrinter1.setPrintAppendString("------------------------------------------------------", format);
                    format.setAli(Layout.Alignment.ALIGN_CENTER);
                    format.setTextSize(30);
                    format.setStyle(PrnTextStyle.BOLD);


                    if (billType.contains("KOT")) {
                        mPrinter1.setPrintAppendString("KOT No.    " + ": " + cancle_order.get(0).otNo, format);

                    } else {
                        mPrinter1.setPrintAppendString("BOT No.    " + ": " + cancle_order.get(0).otNo, format);


                    }

                    mPrinter1.setPrintAppendString("--------------------------------------", format);


                    format.setAli(Layout.Alignment.ALIGN_NORMAL);
                    format.setStyle(PrnTextStyle.NORMAL);
                    format.setTextSize(25);
                    mPrinter1.setPrintAppendString("Counter    " + ": " + counter_name, format);
                    mPrinter1.setPrintAppendString("Date Time  " + ": " + date, format);
                    mPrinter1.setPrintAppendString("Steward " + ": " + waiter, format);
                    mPrinter1.setPrintAppendString("Waiter     " + ": " + waiternamecode, format);
                    mPrinter1.setPrintAppendString("------------------------------------------------------", format);
                    format.setTextSize(30);
                    format.setStyle(PrnTextStyle.BOLD);
                    mPrinter1.setPrintAppendString("Member Name " + ":" + _closeorderItemsArrayList1.get(0).mName, format);
                    mPrinter1.setPrintAppendString("Member Code " + ":" + _closeorderItemsArrayList1.get(0).mAcc, format);
                    format.setAli(Layout.Alignment.ALIGN_NORMAL);
                    format.setStyle(PrnTextStyle.NORMAL);
                    format.setTextSize(25);


                    mPrinter1.setPrintAppendString("Code  "
                            + " "
                            + "Item Name                               "
                           /*+" "
                           +"Qty "*/
                            + " "
                            + "Qty ", format);

                    format.setAli(Layout.Alignment.ALIGN_NORMAL);
                    format.setStyle(PrnTextStyle.NORMAL);
                    format.setTextSize(25);

                    for (int j = 0; j < cancle_order.size(); j++) {


                        if (cancle_order.get(j).itemName.length() > 20) {
                            String itemstr1 = cancle_order.get(j).itemName.substring(0, 20);
                            String itemstr2 = "..." + cancle_order.get(j).itemName.substring(20, cancle_order.get(j).itemName.length());

                            String code = (cancle_order.get(j).itemCode + "********************").substring(0, 6);
                            code = code.replace("*", "   ");

                            mPrinter1.setPrintAppendString(

                                    (code + " "
                                            + itemstr1 + " "
                                            + String.format("%4s", (cancle_order.get(j).quantity)))
                                    , format);
                            mPrinter1.setPrintAppendString(

                                    ("          ")

                                            + itemstr2
                                    , format);
                        } else if (cancle_order.get(j).itemName.length() == 19) {
                            //    String itemname= (MakeOrder.placeOrder_list.get(j).itemName+"----------------------------------------------------------------------------").substring(0,30);
                            String itemname = (cancle_order.get(j).itemName + "********************").substring(0, 20);
                            itemname = itemname.replace("*", "   ");
                            String code = (cancle_order.get(j).itemCode + "********************").substring(0, 6);
                            code = code.replace("*", "   ");

                            String qty = (cancle_order.get(j).quantity + "                                                                           ").substring(0, 4);
                            mPrinter1.setPrintAppendString(


                                    (code + " "
                                            + itemname
                                            + " "
                                            + qty), format);
                        } else {
                            //    String itemname= (MakeOrder.placeOrder_list.get(j).itemName+"----------------------------------------------------------------------------").substring(0,30);
                            String itemname = (cancle_order.get(j).itemName + "********************").substring(0, 19);
                            itemname = itemname.replace("*", "   ");
                            String code = (cancle_order.get(j).itemCode + "********************").substring(0, 6);
                            code = code.replace("*", "   ");

                            String qty = (cancle_order.get(j).quantity + "                                                                           ").substring(0, 4);
                            mPrinter1.setPrintAppendString(


                                    (code + " "
                                            + itemname
                                            + " "
                                            + qty), format);
                        }


                    }

                    mPrinter1.setPrintAppendString("------------------------------------------------------", format);//7
                    mPrinter1.setPrintAppendString(" ", format);
                    mPrinter1.setPrintAppendString(" ", format);
                    mPrinter1.setPrintAppendString(" ", format);
                    mPrinter1.setPrintAppendString(" ", format);
                    printStatus = mPrinter1.setPrintStart();
                    if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                        CloseOrderdetails.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CloseOrderdetails.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                            }
                        });
                    }
                }

            }
        });
        return true;
    }

    //Close order
    public boolean createReceiptData() {
        String method = "";
        String opBalance = "";
        StringBuilder textData = new StringBuilder();
        servicetax = 0;
        salesTax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        cessTax = 0;
        tax = 0;

        DecimalFormat format = new DecimalFormat("#.##");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());


        ArrayList<CloseorderItems> _closeorderItemsArrayList = null;
        if (page == 11) {
            _closeorderItemsArrayList = MakeOrder.closeorderItemsArrayList;
        } else {
            _closeorderItemsArrayList = BillingProfile.closeorderItemsArrayList;
        }


        if (mPrinter == null) {
            return false;
        }
        try {


            opBalance = _closeorderItemsArrayList.get(0).openingBalance;

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            //  textData.append("Cosmopolitan Club (Regd.)\n");
            textData.append(clubName + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            //String address=shafreepreferences.getString("clubAddress","");
            // String[] _address=address.split(",");
            for (int k = 0; k < MainActivity.addressList.size(); k++) {
                textData.append(MainActivity.addressList.get(k) + "\n");
            }
//            for(int k=0;k<=_address.length;k++)
//            {
//                textData.append(_address[k] + "\n");
//            }
            //  textData.append("22nd Cross,3rd Block,Jayanagar" + "\n" + "Bangalore-560 011" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            if (count == 0) {
                textData.append("Bill No. :" + (_closeorderItemsArrayList.get(0).billno).substring(4) + "\n");
            } else {
                textData.append("RE-Print Bill No. :" + (_closeorderItemsArrayList.get(0).billno).substring(4) + "\n");

            }
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            //   mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("Member  Id   " + ": " + _closeorderItemsArrayList.get(0).mAcc + "\n");
            textData.append("Member Name  " + ": " + _closeorderItemsArrayList.get(0).mName + "\n");
            String mstoreGST = shafreepreferences.getString("mstoreGST", "");
            String vocationalmember = shafreepreferences.getString("vocationalmember", "");
            textData.append("GST NO." +
                    "" + ": " + mstoreGST + "\n");
            textData.append("" +
                    "" + "" + vocationalmember + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");

            if (_closeorderItemsArrayList.get(0).cardType.equals("Cash")) {

                textData.append("Type       " + ":" + "CASH BILL" + "\n");

            } else if (_closeorderItemsArrayList.get(0).cardType.equals("Cfreeit")) {

                textData.append("Type       " + ":" + "CfreeIT BILL" + "\n");
            } else {

                textData.append("Type       " + ":" + "REGULAR BILL" + "\n");
            }


            textData.append("Date Time  " + ":" + date + "\n");
            textData.append("Venue      " + ":" + counter_name + "\n");
            // textData.append("Billed By  " + ":" + "           " + "\n");
            //   textData.append("Bill Clerk " + ":" + waiter + "\n");
            textData.append("Steward " + ":" + waiter + "\n");
            textData.append("Waiter     " + ":" + waiternamecode + "\n");
            textData.append("Opening Balance" + ":" + opBalance + "\n");
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append(
                    "Item Name             "

                            + String.format("%4s", "Qty")
                            //  + " "
                            + String.format("%8s", "Rate")
                            //  + " "
                            + String.format("%8s", "Amount")
                            + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);

            if (billType.equals("BOT")) {
                for (int j = 0; j < _closeorderItemsArrayList.size() - 1; j++) {
                    textData.append(
                            String.format("%18s", (_closeorderItemsArrayList.get(j).itemname + "                                                ").substring(0, 20))
                                    // +" "
                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).Quantity))

                                    + String.format("%8s", format.format
                                    (Double.parseDouble((_closeorderItemsArrayList.get(j).rate))))
                                    // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                    // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                    + String.format("%10s", format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount))))
                                    // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                    // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                    + "\n");


                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                    //TODO include in manoch 25-4

                /*servicetax=servicetax+Double.parseDouble(_closeorderItemsArrayList.get(j).getService_tax());
                vat=vat+Double.parseDouble(_closeorderItemsArrayList.get(j).getSales_tax());
                billAmt=billAmt+Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                if(_closeorderItemsArrayList.get(j).getCessTax()==null){
                    cessTax=cessTax+0;
                }else{
                    cessTax=cessTax+Double.parseDouble(_closeorderItemsArrayList.get(j).getCessTax());
                }
*/
                    billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                }
            } else {
                for (int j = 0; j < _closeorderItemsArrayList.size() - 1; j++) {
                    textData.append(
                            String.format("%18s", (_closeorderItemsArrayList.get(j).itemname + "                                                ").substring(0, 20))
                                    // +" "
                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).Quantity))

                                    + String.format("%8s", format.format
                                    (Double.parseDouble((_closeorderItemsArrayList.get(j).rate))))
                                    // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                    // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                    + String.format("%10s", format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount))))
                                    // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                    // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                    + "\n");


                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                    //TODO include in manoch 25-4

                /*servicetax=servicetax+Double.parseDouble(_closeorderItemsArrayList.get(j).getService_tax());
                vat=vat+Double.parseDouble(_closeorderItemsArrayList.get(j).getSales_tax());
                billAmt=billAmt+Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                if(_closeorderItemsArrayList.get(j).getCessTax()==null){
                    cessTax=cessTax+0;
                }else{
                    cessTax=cessTax+Double.parseDouble(_closeorderItemsArrayList.get(j).getCessTax());
                }
*/
                    billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                }
            }


            servicetax = servicetax + Double.parseDouble(_service);
            vat = vat + Double.parseDouble(_vat);
            salesTax = salesTax + Double.parseDouble(_sales);

            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            if (_closeorderItemsArrayList.get(0).cardType.equals("CLUB CARD")) {
                GrantTotal = 0 + 0 + billAmt;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                //   textData.append(String .format("%20s",("Bill Amount" + " :  " + format.format(billAmt)+"                                  " ).substring(0,25)+ "\n"));
                textData.append(String.format("%20s", "Bill Amount" + " :  " + format.format(billAmt) + "\n"));

                //             textData.append("Service Tax" + " :  " +  "0.0" + "\n");
                //             textData.append("Vat        " + " :  " +  "0.0"+ "\n");
                //             textData.append("Cess Tax   " + " :  " +  "0.0"+ "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                //   GrantTotal=servicetax+vat+billAmt+cessTax;


                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");

                for (int k = 0; k < taxlist.size(); k++) {
                    String dummy[] = taxlist.get(k).split("-");
                    tax = tax + Double.parseDouble(dummy[1]);
                    //textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                    textData.append(String.format("%20s", (dummy[0] + " : " + format.format(Double.parseDouble(dummy[1]))) + "\n"));
                }
                /*if(servicetax!=0){
                    textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                }
                if(salesTax!=0){
                    textData.append("Sales Tax  " + " :  " +  format.format(cessTax)+ "\n");
                }
                if(vat!=0){
                    textData.append("Vat        " + " :  " +  format.format(vat)+ "\n");
                }*/

                GrantTotal = billAmt + tax;
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }

            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");
            textData.append("GRAND TOTAL" + " :  " + String.format("%.2f", GrantTotal) + "\n\n");

            if (cardType.equals("CASH CARD")) {

//                if(TakeOrder.isCashCardFilled.equalsIgnoreCase("false")){
//
//                    textData.append("AVAIL.BAL  " + " :  " + "0"+ "\n");
//                    mPrinter.addText(textData.toString());
//                    textData.delete(0, textData.length());
//                }else{
                textData.append("AVAIL.BAL  " + " :  " + _closeorderItemsArrayList.get(0).ava_balance + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
                //  }
            } else {

                textData.append("AVAIL.BAL  " + " :  " + _closeorderItemsArrayList.get(0).ava_balance + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

            }


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            textData.append("\n");
            textData.append("\n");
            textData.append("(Signature)" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
            textData.append("* Thank you ! We Wish To Serve You Again ");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);

        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }

    public boolean createReceiptDatapoona() {
        String method = "";
        final StringBuilder textData = new StringBuilder();
        servicetax = 0;
        salesTax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        cessTax = 0;
        tax = 0;

        final DecimalFormat format11 = new DecimalFormat("#.##");
        Calendar cal = Calendar.getInstance();
        final SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        final String date = format1.format(cal.getTime());


        try {
            mSingleThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    int printStatus = mPrinter1.getPrinterStatus();
                    if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                        CloseOrderdetails.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CloseOrderdetails.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();

                            }
                        });
                    } else {
                        String opBalance = "";

                        ArrayList<CloseorderItems> _closeorderItemsArrayList = null;
                        if (page == 11) {
                            _closeorderItemsArrayList = MakeOrder.closeorderItemsArrayList;
                        } else {
                            _closeorderItemsArrayList = BillingProfile.closeorderItemsArrayList;
                        }

                        opBalance = _closeorderItemsArrayList.get(0).openingBalance;

                        PrnStrFormat format = new PrnStrFormat();
                        format.setTextSize(30);
                        format.setAli(Layout.Alignment.ALIGN_CENTER);
                        format.setFont(PrnTextFont.DEFAULT);
                        mPrinter1.setPrintAppendString(clubName, format);


                        if (clubaddress != null && !clubaddress.equals("")) {
                            format.setTextSize(25);
                            format.setAli(Layout.Alignment.ALIGN_CENTER);
                            format.setFont(PrnTextFont.DEFAULT);
                            mPrinter1.setPrintAppendString(clubaddress, format);

                        } else {
                            for (int k = 0; k < MainActivity.addressList.size(); k++) {
//                    textData.append(MainActivity.addressList.get(k) + "\n");
                            }
                        }


                        if (count == 0) {
                            // textData.append("Bill No. :" + (_closeorderbillList.get(0).getBillDetNumber()).substring(4) + "\n");
                            mPrinter1.setPrintAppendString(" ", format);
                            mPrinter1.setPrintAppendString("Bill No. :" + (_closeorderItemsArrayList.get(0).billno).substring(4), format);

                        } else {
                            //   textData.append("RE-Print Bill No. :" + (_closeorderbillList.get(0).getBillDetNumber()).substring(4) + "\n");
                            mPrinter1.setPrintAppendString(" ", format);
                            mPrinter1.setPrintAppendString("RE-Print Bill No. :" + (_closeorderItemsArrayList.get(0).billno).substring(4), format);

                        }

                        mPrinter1.setPrintAppendString("------------------------------------------------------", format);

                        format.setTextSize(30);
                        format.setStyle(PrnTextStyle.BOLD);
                        mPrinter1.setPrintAppendString("Member  Id   " + ": " + _closeorderItemsArrayList.get(0).mAcc, format);
                        mPrinter1.setPrintAppendString("Member Name  " + ": " + _closeorderItemsArrayList.get(0).mName, format);
                        mPrinter1.setPrintAppendString("--------------------------------------", format);
                        String mstoreGST = shafreepreferences.getString("mstoreGST", "");
                        String vocationalmember = shafreepreferences.getString("vocationalmember", "");
                        textData.append("GST NO." +
                                "" + ": " + mstoreGST + "\n");
                        textData.append("" +
                                "" + "" + vocationalmember + "\n");

                        mPrinter1.setPrintAppendString("GST NO." +
                                "" + ": " + mstoreGST, format);
                        mPrinter1.setPrintAppendString("" +
                                "" + "" + vocationalmember, format);
                        mPrinter1.setPrintAppendString("--------------------------------------", format);


                        if (_closeorderItemsArrayList.get(0).cardType.equals("Cash")) {
                            mPrinter1.setPrintAppendString("Type       " + ":" + "CASH BILL", format);

                        } else if (_closeorderItemsArrayList.get(0).cardType.equals("Cfreeit")) {

                            //   textData.append("Type       " + ":" + "CfreeIT BILL" + "\n");
                            mPrinter1.setPrintAppendString("Type       " + ":" + "CfreeIT BILL", format);

                        } else if (_closeorderItemsArrayList.get(0).cardType.equals("Complimentary")) {
                            //   textData.append("Type       " + ":" + "Complimentary" + "\n");
                            mPrinter1.setPrintAppendString("Type       " + ":" + "Complimentary", format);

                        } else {

                            //  textData.append("Type       " + ":" + "REGULAR BILL" + "\n");
                            mPrinter1.setPrintAppendString("Type       " + ":" + "REGULAR BILL", format);

                        }


                        mPrinter1.setPrintAppendString("Date Time  " + ":" + date, format);

                        //   textData.append("Venue      " + ":" + counter_name + "\n");
                        mPrinter1.setPrintAppendString("Venue      " + ":" + counter_name, format);

                        // textData.append("Billed By  " + ":" + "           " + "\n");
                        //     textData.append("Bill Clerk " + ":" + waiter + "\n");
                        //   textData.append("Steward " + ":" + waiter + "\n");
                        mPrinter1.setPrintAppendString("Steward " + ":" + waiter, format);
                        mPrinter1.setPrintAppendString("Waiter     " + ":" + waiternamecode, format);

                        mPrinter1.setPrintAppendString("Opening Balance" + ":" + opBalance, format);


                        mPrinter1.setPrintAppendString("------------------------------------------------------", format);


                        format.setAli(Layout.Alignment.ALIGN_NORMAL);
                        format.setStyle(PrnTextStyle.NORMAL);
                        format.setTextSize(23);
                        mPrinter1.setPrintAppendString(
                                String.format("%30s", ("Item Name             " + "                                                ").substring(0, 30))

                                        + String.format("%6s", "Qty")
                                        //  + " "
                                        //    + String.format("%8s", "Rate")
                                        //  + " "
                                        + String.format("%10s", "Amount"), format);
                        mPrinter1.setPrintAppendString("------------------------------------------------------", format);


                        if (billType.equals("BOT")) {
                            PrnStrFormat format1 = new PrnStrFormat();
                            format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                            format1.setStyle(PrnTextStyle.NORMAL);
                            format1.setTextSize(23);

                            for (int j = 0; j < _closeorderItemsArrayList.size() - 1; j++) {


                                if (_closeorderItemsArrayList.get(j).itemname.length() > 20) {
                                    String itemstr1 = _closeorderItemsArrayList.get(j).itemname.substring(0, 20);
                                    String itemstr2 = "..." + _closeorderItemsArrayList.get(j).itemname.substring(20, _closeorderItemsArrayList.get(j).itemname.length());
                                    String itemline =
                                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                                            String.format("%15s", (itemstr1 + "                                                ").substring(0, 20))

                                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).Quantity))

                                                    + String.format("%8s",
                                                    (Double.parseDouble((_closeorderItemsArrayList.get(j).Amount))))

                                                    + "\n";
                                    mPrinter1.setPrintAppendString(itemline, format1);

                                    String itemline1 =
                                            String.format("%15s", (itemstr2 + "                                                ").substring(0, 20))

                                                    + String.format("%4s", (""))

                                                    + String.format("%8s", "")

                                                    + "\n";
                                    mPrinter1.setPrintAppendString(itemline1, format1);
                                } else {
                                    String itemname = (_closeorderItemsArrayList.get(j).itemname + "**************************************").substring(0, 20);
                                    itemname = itemname.replace("*", "  ");

                                    String itemline =
                                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                                            itemname
                                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).getQuantity()))

                                                    + String.format("%8s", (Double.parseDouble((_closeorderItemsArrayList.get(j).getAmount()))))

                                                    + "\n";
                                    mPrinter1.setPrintAppendString(itemline, format1);

                                }

                                billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                            }
                        } else {
                            for (int j = 0; j < _closeorderItemsArrayList.size() - 1; j++) {


                                PrnStrFormat format1 = new PrnStrFormat();
                                format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                                format1.setStyle(PrnTextStyle.NORMAL);
                                format1.setTextSize(23);

                                if (_closeorderItemsArrayList.get(j).itemname.length() > 20) {
                                    String itemstr1 = _closeorderItemsArrayList.get(j).itemname.substring(0, 20);
                                    String itemstr2 = "..." + _closeorderItemsArrayList.get(j).itemname.substring(20, _closeorderItemsArrayList.get(j).itemname.length());
                                    String itemline =
                                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                                            String.format("%15s", (itemstr1 + "                                                ").substring(0, 20))

                                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).getQuantity()))

                                                    + String.format("%8s", (Double.parseDouble((_closeorderItemsArrayList.get(j).getAmount()))))

                                                    + "\n";
                                    mPrinter1.setPrintAppendString(itemline, format1);

                                    String itemline1 =
                                            String.format("%15s", (itemstr2 + "                                                ").substring(0, 20))

                                                    + String.format("%4s", (""))

                                                    + String.format("%8s", "")

                                                    + "\n";
                                    mPrinter1.setPrintAppendString(itemline1, format1);
                                } else {
                                    String itemname = (_closeorderItemsArrayList.get(j).getItemname() + "**************************************").substring(0, 20);
                                    itemname = itemname.replace("*", "  ");


                                    String itemline =
                                            //   String.format("%18s", (_closeorderbillList.get(m).getItemName() + "                                                ").substring(0, 20))
                                            itemname

                                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).getQuantity()))

                                                    + String.format("%8s", (Double.parseDouble((_closeorderItemsArrayList.get(j).getAmount()))))

                                                    + "\n";
                                    mPrinter1.setPrintAppendString(itemline, format1);

                                }


                                billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                            }
                        }
                        PrnStrFormat format1 = new PrnStrFormat();

                        format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                        format1.setStyle(PrnTextStyle.NORMAL);
                        format1.setTextSize(25);

                        mPrinter1.setPrintAppendString("------------------------------------------------------", format1);


                        servicetax = servicetax + Double.parseDouble(_service);
                        vat = vat + Double.parseDouble(_vat);
                        salesTax = salesTax + Double.parseDouble(_sales);

                        if (_closeorderItemsArrayList.get(0).cardType.equals("CLUB CARD")) {
                            GrantTotal = 0 + 0 + billAmt;

                            mPrinter1.setPrintAppendString("Bill Amount" + " :  " + format11.format(billAmt), format1);


                        } else {
                            mPrinter1.setPrintAppendString("Bill Amount" + " :  " + format11.format(billAmt), format1);


                            for (int k = 0; k < taxlist.size(); k++) {
                                String dummy[] = taxlist.get(k).split("-");
                                tax = tax + Double.parseDouble(dummy[1]);
                                //textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                                // textData.append(String.format("%20s", (dummy[0] + " : " + format.format(Double.parseDouble(dummy[1]))) + "\n"));
                                mPrinter1.setPrintAppendString(String.format("%20s", (dummy[0] + " : " + format11.format(Double.parseDouble(dummy[1])))), format1);


                            }
                /*if(servicetax!=0){
                    textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                }
                if(salesTax!=0){
                    textData.append("Sales Tax  " + " :  " +  format.format(cessTax)+ "\n");
                }
                if(vat!=0){
                    textData.append("Vat        " + " :  " +  format.format(vat)+ "\n");
                }*/

                            GrantTotal = billAmt + tax;

                        }
                        //textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal)+ "\n\n");
                        mPrinter1.setPrintAppendString("GRAND TOTAL" + " :  " + String.format("%.2f", GrantTotal), format1);

                        format1.setAli(Layout.Alignment.ALIGN_NORMAL);
                        format1.setStyle(PrnTextStyle.NORMAL);
                        format1.setTextSize(25);
                        if (cardType.equals("CASH CARD")) {

//                if(TakeOrder.isCashCardFilled.equalsIgnoreCase("false")){
//
//                    textData.append("AVAIL.BAL  " + " :  " + "0"+ "\n");
//                    mPrinter.addText(textData.toString());
//                    textData.delete(0, textData.length());
//                }else{
                            mPrinter1.setPrintAppendString("AVAIL.BAL  " + " :  " + _closeorderItemsArrayList.get(0).ava_balance, format1);

                            //  }
                        } else {

                            mPrinter1.setPrintAppendString("AVAIL.BAL  " + " :  " + _closeorderItemsArrayList.get(0).ava_balance, format1);


                        }
                        mPrinter1.setPrintAppendString("------------------------------------------------------", format1);
                        mPrinter1.setPrintAppendString(" ", format1);
                        mPrinter1.setPrintAppendString(" ", format1);

                        mPrinter1.setPrintAppendString("(Signature)" + "\n", format1);
                        mPrinter1.setPrintAppendString("* Thank you ! We Wish To Serve You Again ", format1);

                        mPrinter1.setPrintAppendString(" ", format1);
                        mPrinter1.setPrintAppendString(" ", format1);
                        mPrinter1.setPrintAppendString(" ", format1);
                        mPrinter1.setPrintAppendString(" ", format1);
                    }

                    printStatus = mPrinter1.setPrintStart();
                    if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                        CloseOrderdetails.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CloseOrderdetails.this, "printer_out_of_paper", Toast.LENGTH_SHORT).show();// pd.dismiss();
                            }
                        });
                    }
                }
            });
        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            //return false;
        }
        return true;
    }

    //for direct
    public boolean createReceiptData23() {
        String method = "";
        String opBalance = "";
        StringBuilder textData = new StringBuilder();
        servicetax = 0;
        salesTax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        cessTax = 0;
        tax = 0;

        DecimalFormat format = new DecimalFormat("#.##");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());


        ArrayList<CloseorderItems> _closeorderItemsArrayList = KotItemlist.closeorderItemsArrayList_Others;


        if (mPrinter == null) {
            return false;
        }
        try {

            opBalance = _closeorderItemsArrayList.get(0).openingBalance;

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            //  textData.append("Cosmopolitan Club (Regd.)\n");
            textData.append(clubName + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            for (int k = 0; k < MainActivity.addressList.size(); k++) {
                textData.append(MainActivity.addressList.get(k) + "\n");
            }
            //  textData.append("22nd Cross,3rd Block,Jayanagar" + "\n" + "Bangalore-560 011" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            if (count == 0) {
                textData.append("Bill No. :" + (_closeorderItemsArrayList.get(0).billno).substring(4) + "\n");
            } else {
                textData.append("RE-Print Bill No. :" + (_closeorderItemsArrayList.get(0).billno).substring(4) + "\n");

            }
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            //   mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("Member  Id   " + ": " + _closeorderItemsArrayList.get(0).mAcc + "\n");
            textData.append("Member Name  " + ": " + _closeorderItemsArrayList.get(0).mName + "\n");
            String mstoreGST = shafreepreferences.getString("mstoreGST", "");
            textData.append("GST NO." +
                    "" + ": " + mstoreGST + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");

            if (_closeorderItemsArrayList.get(0).cardType.equals("Cash")) {

                textData.append("Type       " + ":" + "CASH BILL" + "\n");

            } else if (_closeorderItemsArrayList.get(0).cardType.equals("Cfreeit")) {

                textData.append("Type       " + ":" + "CfreeIT BILL" + "\n");
            } else {

                textData.append("Type       " + ":" + "REGULAR BILL" + "\n");
            }


            textData.append("Date Time  " + ":" + date + "\n");
            textData.append("Venue      " + ":" + counter_name + "\n");
            // textData.append("Billed By  " + ":" + "           " + "\n");
            //   textData.append("Bill Clerk " + ":" + waiter + "\n");
            textData.append("Steward " + ":" + waiter + "\n");
            textData.append("Waiter     " + ":" + waiternamecode + "\n");
            textData.append("Opening Balance" + ":" + opBalance + "\n");
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append(
                    "Item Name             "

                            + String.format("%4s", "Qty")
                            //  + " "
                            + String.format("%8s", "Rate")
                            //  + " "
                            + String.format("%8s", "Amount")
                            + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);

            if (billType.equals("BOT")) {
                for (int j = 0; j < _closeorderItemsArrayList.size() - 1; j++) {
                    textData.append(
                            String.format("%18s", (_closeorderItemsArrayList.get(j).itemname + "                                                ").substring(0, 20))
                                    // +" "
                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).Quantity))

                                    + String.format("%8s", format.format
                                    (Double.parseDouble((_closeorderItemsArrayList.get(j).rate))))
                                    // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                    // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                    + String.format("%10s", format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount))))
                                    // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                    // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                    + "\n");


                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                    //TODO include in manoch 25-4

                /*servicetax=servicetax+Double.parseDouble(_closeorderItemsArrayList.get(j).getService_tax());
                vat=vat+Double.parseDouble(_closeorderItemsArrayList.get(j).getSales_tax());
                billAmt=billAmt+Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                if(_closeorderItemsArrayList.get(j).getCessTax()==null){
                    cessTax=cessTax+0;
                }else{
                    cessTax=cessTax+Double.parseDouble(_closeorderItemsArrayList.get(j).getCessTax());
                }
*/
                    billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                }
            } else {
                for (int j = 0; j < _closeorderItemsArrayList.size() - 1; j++) {
                    textData.append(
                            String.format("%18s", (_closeorderItemsArrayList.get(j).itemname + "                                                ").substring(0, 20))
                                    // +" "
                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).Quantity))

                                    + String.format("%8s", format.format
                                    (Double.parseDouble((_closeorderItemsArrayList.get(j).rate))))
                                    // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                    // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                    + String.format("%10s", format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount))))
                                    // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                    // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                    + "\n");


                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                    //TODO include in manoch 25-4

                /*servicetax=servicetax+Double.parseDouble(_closeorderItemsArrayList.get(j).getService_tax());
                vat=vat+Double.parseDouble(_closeorderItemsArrayList.get(j).getSales_tax());
                billAmt=billAmt+Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                if(_closeorderItemsArrayList.get(j).getCessTax()==null){
                    cessTax=cessTax+0;
                }else{
                    cessTax=cessTax+Double.parseDouble(_closeorderItemsArrayList.get(j).getCessTax());
                }
*/
                    billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                }
            }


            servicetax = servicetax + Double.parseDouble(_service);
            vat = vat + Double.parseDouble(_vat);
            salesTax = salesTax + Double.parseDouble(_sales);

            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            if (_closeorderItemsArrayList.get(0).cardType.equals("CLUB CARD")) {
                GrantTotal = 0 + 0 + billAmt;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                //   textData.append(String .format("%20s",("Bill Amount" + " :  " + format.format(billAmt)+"                                  " ).substring(0,25)+ "\n"));
                textData.append(String.format("%20s", "Bill Amount" + " :  " + format.format(billAmt) + "\n"));

                //             textData.append("Service Tax" + " :  " +  "0.0" + "\n");
                //             textData.append("Vat        " + " :  " +  "0.0"+ "\n");
                //             textData.append("Cess Tax   " + " :  " +  "0.0"+ "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                //   GrantTotal=servicetax+vat+billAmt+cessTax;


                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");

                for (int k = 0; k < taxlist.size(); k++) {
                    String dummy[] = taxlist.get(k).split("-");
                    tax = tax + Double.parseDouble(dummy[1]);
                    //textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                    textData.append(String.format("%20s", (dummy[0] + " : " + format.format(Double.parseDouble(dummy[1]))) + "\n"));
                }
                /*if(servicetax!=0){
                    textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                }
                if(salesTax!=0){
                    textData.append("Sales Tax  " + " :  " +  format.format(cessTax)+ "\n");
                }
                if(vat!=0){
                    textData.append("Vat        " + " :  " +  format.format(vat)+ "\n");
                }*/

                GrantTotal = billAmt + tax;
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }

            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal) + "\n\n");

            if (cardType.equals("CASH CARD")) {

                if (BillingProfile.isCashCardFilled.equalsIgnoreCase("false")) {

                    textData.append("AVAIL.BAL  " + " :  " + "0" + "\n");
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());
                } else {
                    textData.append("AVAIL.BAL  " + " :  " + _closeorderItemsArrayList.get(0).ava_balance + "\n");
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());
                }
            } else {

                textData.append("AVAIL.BAL  " + " :  " + _closeorderItemsArrayList.get(0).ava_balance + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

            }


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            textData.append("\n");
            textData.append("\n");
            textData.append("(Signature)" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
            textData.append("* Thank you ! We Wish To Serve You Again ");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);

        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }

    public boolean createReceiptData23poona() {
        String method = "";
        String opBalance = "";
        StringBuilder textData = new StringBuilder();
        servicetax = 0;
        salesTax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        cessTax = 0;
        tax = 0;

        DecimalFormat format = new DecimalFormat("#.##");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());


        ArrayList<CloseorderItems> _closeorderItemsArrayList = KotItemlist.closeorderItemsArrayList_Others;


        if (mPrinter == null) {
            return false;
        }
        try {

            opBalance = _closeorderItemsArrayList.get(0).openingBalance;

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            //  textData.append("Cosmopolitan Club (Regd.)\n");
            textData.append(clubName + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            for (int k = 0; k < MainActivity.addressList.size(); k++) {
                textData.append(MainActivity.addressList.get(k) + "\n");
            }
            //  textData.append("22nd Cross,3rd Block,Jayanagar" + "\n" + "Bangalore-560 011" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            if (count == 0) {
                textData.append("Bill No. :" + (_closeorderItemsArrayList.get(0).billno).substring(4) + "\n");
            } else {
                textData.append("RE-Print Bill No. :" + (_closeorderItemsArrayList.get(0).billno).substring(4) + "\n");

            }
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            //   mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("Member  Id   " + ": " + _closeorderItemsArrayList.get(0).mAcc + "\n");
            textData.append("Member Name  " + ": " + _closeorderItemsArrayList.get(0).mName + "\n");
            String mstoreGST = shafreepreferences.getString("mstoreGST", "");
            textData.append("GST NO." +
                    "" + ": " + mstoreGST + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");

            if (_closeorderItemsArrayList.get(0).cardType.equals("Cash")) {

                textData.append("Type       " + ":" + "CASH BILL" + "\n");

            } else if (_closeorderItemsArrayList.get(0).cardType.equals("Cfreeit")) {

                textData.append("Type       " + ":" + "CfreeIT BILL" + "\n");
            } else {

                textData.append("Type       " + ":" + "REGULAR BILL" + "\n");
            }


            textData.append("Date Time  " + ":" + date + "\n");
            textData.append("Venue      " + ":" + counter_name + "\n");
            // textData.append("Billed By  " + ":" + "           " + "\n");
            //   textData.append("Bill Clerk " + ":" + waiter + "\n");
            textData.append("Steward " + ":" + waiter + "\n");
            textData.append("Waiter     " + ":" + waiternamecode + "\n");
            textData.append("Opening Balance" + ":" + opBalance + "\n");
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append(
                    "Item Name             "

                            + String.format("%4s", "Qty")
                            //  + " "
                            + String.format("%8s", "Rate")
                            //  + " "
                            + String.format("%8s", "Amount")
                            + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);

            if (billType.equals("BOT")) {
                for (int j = 0; j < _closeorderItemsArrayList.size() - 1; j++) {
                    textData.append(
                            String.format("%18s", (_closeorderItemsArrayList.get(j).itemname + "                                                ").substring(0, 20))
                                    // +" "
                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).Quantity))

                                    + String.format("%8s", format.format
                                    (Double.parseDouble((_closeorderItemsArrayList.get(j).rate))))
                                    // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                    // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                    + String.format("%10s", format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount))))
                                    // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                    // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                    + "\n");


                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                    //TODO include in manoch 25-4

                /*servicetax=servicetax+Double.parseDouble(_closeorderItemsArrayList.get(j).getService_tax());
                vat=vat+Double.parseDouble(_closeorderItemsArrayList.get(j).getSales_tax());
                billAmt=billAmt+Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                if(_closeorderItemsArrayList.get(j).getCessTax()==null){
                    cessTax=cessTax+0;
                }else{
                    cessTax=cessTax+Double.parseDouble(_closeorderItemsArrayList.get(j).getCessTax());
                }
*/
                    billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                }
            } else {
                for (int j = 0; j < _closeorderItemsArrayList.size() - 1; j++) {
                    textData.append(
                            String.format("%18s", (_closeorderItemsArrayList.get(j).itemname + "                                                ").substring(0, 20))
                                    // +" "
                                    + String.format("%4s", (_closeorderItemsArrayList.get(j).Quantity))

                                    + String.format("%8s", format.format
                                    (Double.parseDouble((_closeorderItemsArrayList.get(j).rate))))
                                    // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                    // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                    + String.format("%10s", format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount))))
                                    // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                    // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                    + "\n");


                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                    //TODO include in manoch 25-4

                /*servicetax=servicetax+Double.parseDouble(_closeorderItemsArrayList.get(j).getService_tax());
                vat=vat+Double.parseDouble(_closeorderItemsArrayList.get(j).getSales_tax());
                billAmt=billAmt+Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                if(_closeorderItemsArrayList.get(j).getCessTax()==null){
                    cessTax=cessTax+0;
                }else{
                    cessTax=cessTax+Double.parseDouble(_closeorderItemsArrayList.get(j).getCessTax());
                }
*/
                    billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());
                }
            }


            servicetax = servicetax + Double.parseDouble(_service);
            vat = vat + Double.parseDouble(_vat);
            salesTax = salesTax + Double.parseDouble(_sales);

            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            if (_closeorderItemsArrayList.get(0).cardType.equals("CLUB CARD")) {
                GrantTotal = 0 + 0 + billAmt;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                //   textData.append(String .format("%20s",("Bill Amount" + " :  " + format.format(billAmt)+"                                  " ).substring(0,25)+ "\n"));
                textData.append(String.format("%20s", "Bill Amount" + " :  " + format.format(billAmt) + "\n"));

                //             textData.append("Service Tax" + " :  " +  "0.0" + "\n");
                //             textData.append("Vat        " + " :  " +  "0.0"+ "\n");
                //             textData.append("Cess Tax   " + " :  " +  "0.0"+ "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                //   GrantTotal=servicetax+vat+billAmt+cessTax;


                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");

                for (int k = 0; k < taxlist.size(); k++) {
                    String dummy[] = taxlist.get(k).split("-");
                    tax = tax + Double.parseDouble(dummy[1]);
                    //textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                    textData.append(String.format("%20s", (dummy[0] + " : " + format.format(Double.parseDouble(dummy[1]))) + "\n"));
                }
                /*if(servicetax!=0){
                    textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                }
                if(salesTax!=0){
                    textData.append("Sales Tax  " + " :  " +  format.format(cessTax)+ "\n");
                }
                if(vat!=0){
                    textData.append("Vat        " + " :  " +  format.format(vat)+ "\n");
                }*/

                GrantTotal = billAmt + tax;
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }

            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal) + "\n\n");

            if (cardType.equals("CASH CARD")) {

                if (BillingProfile.isCashCardFilled.equalsIgnoreCase("false")) {

                    textData.append("AVAIL.BAL  " + " :  " + "0" + "\n");
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());
                } else {
                    textData.append("AVAIL.BAL  " + " :  " + _closeorderItemsArrayList.get(0).ava_balance + "\n");
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());
                }
            } else {

                textData.append("AVAIL.BAL  " + " :  " + _closeorderItemsArrayList.get(0).ava_balance + "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

            }


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            textData.append("\n");
            textData.append("\n");
            textData.append("(Signature)" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
            textData.append("* Thank you ! We Wish To Serve You Again ");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);

        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }

    //for direct
    // for other module
    public boolean createReceiptData3() {
        String method = "";
        String opBalance = "";
        StringBuilder textData = new StringBuilder();
        servicetax = 0;
        vat = 0;
        billAmt = 0;
        GrantTotal = 0;
        cessTax = 0;
        tax = 0;
        DecimalFormat format = new DecimalFormat("#.##");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.ENGLISH);
        String date = format1.format(cal.getTime());


        ArrayList<CloseorderItems> _closeorderItemsArrayList = KotItemlist.closeorderItemsArrayList_Others;
        if (mPrinter == null) {
            return false;
        }
        try {
            opBalance = _closeorderItemsArrayList.get(0).openingBalance;
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);

            method = "addFeedLine";
            mPrinter.addFeedLine(0);


            method = "addTextSize";
            mPrinter.addTextSize(2, 2);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            //  textData.append("Cosmopolitan Club (Regd.)\n");
            textData.append(clubName + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            for (int k = 0; k < MainActivity.addressList.size(); k++) {
                textData.append(MainActivity.addressList.get(k) + "\n");
            }
            //  textData.append("22nd Cross,3rd Block,Jayanagar" + "\n" + "Bangalore-560 011" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            if (count == 0) {
                textData.append("Bill No. :" + billid.substring(4) + "\n");
            } else {
                textData.append("RE-Print Bill No. :" + billid.substring(4) + "\n");
            }
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("Member  Id   " + ": " + _closeorderItemsArrayList.get(0).mAcc + "\n");
            textData.append("Member Name  " + ": " + _closeorderItemsArrayList.get(0).mAcc + "\n");
            String mstoreGST = shafreepreferences.getString("mstoreGST", "");
            textData.append("GST NO." +
                    "" + ": " + mstoreGST + "\n");
            if (_closeorderItemsArrayList.get(0).cardType.equals("Cash")) {

                textData.append("Type       " + ":" + "CASH BILL" + "\n");

            } else if (_closeorderItemsArrayList.get(0).cardType.equals("Cfreeit")) {

                textData.append("Type       " + ":" + "CfreeIT BILL" + "\n");
            } else {

                textData.append("Type       " + ":" + "REGULAR BILL" + "\n");
            }
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("--------------------------------------\n");
            textData.append("Date Time  " + ":" + date + "\n");
            textData.append("Venue      " + ":" + counter_name + "\n");
            // textData.append("Billed By  " + ":" + "           " + "\n");
            //   textData.append("Bill Clerk " + ":" + waiter + "\n");
            textData.append("Steward " + ":" + waiter + "\n");
            textData.append("Waiter     " + ":" + waiternamecode + "\n");
            textData.append("Opening Balance" + ":" + opBalance + "\n");
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append(
                    "Item Name             "

                            + String.format("%4s", "Qty")
                            //  + " "
                            + String.format("%8s", "Rate")
                            //  + " "
                            + String.format("%8s", "Amount")
                            + "\n");

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_D);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);

            for (int j = 0; j < _closeorderItemsArrayList.size(); j++) {
                textData.append(
                        String.format("%18s", (_closeorderItemsArrayList.get(j).itemname + "                                                ").substring(0, 20))
                                // +" "
                                + String.format("%4s", (_closeorderItemsArrayList.get(j).Quantity))

                                + String.format("%8s", format.format
                                (Double.parseDouble((_closeorderItemsArrayList.get(j).rate))))
                                // +(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9)

                                // +String.format("%8s",format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))))
                                + String.format("%10s", format.format(Double.parseDouble((_closeorderItemsArrayList.get(j).Amount))))
                                // +String.format("%8s",(_closeorderItemsArrayList.get(j).Amount + "                 ").substring(0, 9))
                                // +String.format("%8s",(deci[j] + "                 ").substring(0, 9))

                                + "\n");


                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                //TODO include in manoch 25-4

               /* servicetax=servicetax+Double.parseDouble(_closeorderItemsArrayList.get(j).getServiceTax());
                vat=vat+Double.parseDouble(_closeorderItemsArrayList.get(j).getVat());
                salesTax= salesTax+Double.parseDouble(_closeorderItemsArrayList.get(j).getSalestTax());
*/
                billAmt = billAmt + Double.parseDouble(_closeorderItemsArrayList.get(j).getAmount());

                /*if(_closeorderItemsArrayList.get(j).getCesstax()==null){
                    cessTax=cessTax+0;
                }else{
                    cessTax=cessTax+Double.parseDouble(_closeorderItemsArrayList.get(j).getCesstax());
                }
*/


            }

            textData.append("------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            if (_closeorderItemsArrayList.get(0).cardType.equals("CLUB CARD")) {
                GrantTotal = 0 + 0 + billAmt;
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");
                //   textData.append("Service Tax" + " :  " +  "0.0" + "\n");
                //   textData.append("Vat        " + " :  " +  "0.0"+ "\n");
                //         textData.append("Cess Tax   " + " :  " +  "0.0"+ "\n");
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            } else {
                method = "addTextSize";
                mPrinter.addTextSize(1, 1);
                mPrinter.addTextFont(Printer.FONT_B);
                mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
                textData.append("Bill Amount" + " :  " + format.format(billAmt) + "\n");

                for (int k = 0; k < taxlist.size(); k++) {
                    String dummy[] = taxlist.get(k).split("-");
                    tax = tax + Double.parseDouble(dummy[1]);
                    //  textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                    textData.append(String.format("%30s", dummy[0] + " :  " + format.format(Double.parseDouble(dummy[1])) + "\n"));
                }


               /* if(servicetax!=0){
                    textData.append("Service Tax" + " :  " +  format.format(servicetax) + "\n");
                }else if(vat!=0){
                    textData.append("Vat        " + " :  " +  format.format(vat)+ "\n");
                }else if (salesTax!=0){
                    textData.append("Sales Tax  " + " :  " +  format.format(cessTax)+ "\n");
                }*/
                GrantTotal = billAmt + tax;
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }

            method = "addTextSize";
            mPrinter.addTextSize(1, 2);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("GRAND TOTAL" + " :  " + format.format(GrantTotal) + "\n");
            textData.append("AVAIL.BAL  " + " :  " + _closeorderItemsArrayList.get(0).ava_balance + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_B);
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_1);
            textData.append("--------------------------------------\n");
            textData.append("\n");
            textData.append("\n");
            textData.append("(Signature)" + "\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextSize";
            mPrinter.addTextSize(1, 1);
            mPrinter.addTextFont(Printer.FONT_E);
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            mPrinter.addTextStyle(Printer.PARAM_UNSPECIFIED, Printer.PARAM_UNSPECIFIED, Printer.TRUE, Printer.COLOR_2);
            textData.append("* Thank you ! We Wish To Serve You Again ");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            mPrinter.addCut(Printer.CUT_FEED);

        } catch (Exception e) {
            ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }


}
