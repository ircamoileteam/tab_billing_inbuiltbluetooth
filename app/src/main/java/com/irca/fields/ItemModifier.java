package com.irca.fields;

/**
 * Created by Manoch Richard on 23-Feb-18.
 */

public class ItemModifier {
    String id;
    String name;
    String iddetails;
    String detailname;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getIddetails()
    {
        return iddetails;
    }

    public void setIddetails(String iddetails)
    {
        this.iddetails = iddetails;
    }

    public String getDetailname()
    {
        return detailname;
    }

    public void setDetailname(String detailname)
    {
        this.detailname = detailname;
    }

}
