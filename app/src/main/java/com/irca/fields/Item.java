package com.irca.fields;

/**
 * Created by Manoch Richard on 10/23/2015.
 */
public class    Item {

    public String ItemCategoryID;
    public String itemcode;
    public String ItemIdentityID;
    public String description;
    public String ItemGroupID;
    public String ItemName;
    public String StoreID;
    public String Tax;
    public String UnitID;
    public String TaxID;
    public String Rate;
    public String ItemImage;

    public String getStock() {
        return Stock;
    }

    public void setStock(String stock) {
        Stock = stock;
    }

    public String Stock;

    public String getItemImage() {
        return ItemImage;
    }

    public void setItemImage(String itemImage) {
        ItemImage = itemImage;
    }

    public String getItemCategoryID() {
        return ItemCategoryID;
    }

    public void setItemCategoryID(String itemCategoryID) {
        ItemCategoryID = itemCategoryID;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getItemIdentityID() {
        return ItemIdentityID;
    }

    public void setItemIdentityID(String itemIdentityID) {
        ItemIdentityID = itemIdentityID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemGroupID() {
        return ItemGroupID;
    }

    public void setItemGroupID(String itemGroupID) {
        ItemGroupID = itemGroupID;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public String getStoreID() {
        return StoreID;
    }

    public void setStoreID(String storeID) {
        StoreID = storeID;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getUnitID() {
        return UnitID;
    }

    public void setUnitID(String unitID) {
        UnitID = unitID;
    }

    public String getTaxID() {
        return TaxID;
    }

    public void setTaxID(String taxID) {
        TaxID = taxID;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

}
