package com.irca.fields;

/**
 * Created by Manoch Richard on 27-Mar-18.
 */

public class ItemPromo {

    public String getPromotionId() {
        return PromotionId;
    }

    public void setPromotionId(String promotionId) {
        PromotionId = promotionId;
    }

    public String getPromotionName() {
        return PromotionName;
    }

    public void setPromotionName(String promotionName) {
        PromotionName = promotionName;
    }

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String fromDate) {
        FromDate = fromDate;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String toDate) {
        ToDate = toDate;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getOfferPromotionId() {
        return OfferPromotionId;
    }

    public void setOfferPromotionId(String offerPromotionId) {
        OfferPromotionId = offerPromotionId;
    }

    public String getFreeItemId() {
        return FreeItemId;
    }

    public void setFreeItemId(String freeItemId) {
        FreeItemId = freeItemId;
    }

    public String getFreeQty() {
        return FreeQty;
    }

    public void setFreeQty(String freeQty) {
        FreeQty = freeQty;
    }

    public String getFreeItemName() {
        return FreeItemName;
    }

    public void setFreeItemName(String freeItemName) {
        FreeItemName = freeItemName;
    }

    public String getFreeItemCode() {
        return FreeItemCode;
    }

    public void setFreeItemCode(String freeItemCode) {
        FreeItemCode = freeItemCode;
    }

    String PromotionId,PromotionName,ItemID,Quantity,Rate,FromDate,ToDate,CreatedDate,ModifiedDate,OfferPromotionId,FreeItemId,FreeQty,FreeItemName,FreeItemCode;
}
