package com.irca.fields;

/**
 * Created by Manoch Richard on 10/27/2015.
 */
public class Table {
    int tableId;
    String tableNmae;

    public int getTableId() {
        return tableId;
    }

    public void setTableId(int tableId) {
        this.tableId = tableId;
    }

    public String getTableNmae() {
        return tableNmae;
    }

    public void setTableNmae(String tableNmae) {
        this.tableNmae = tableNmae;
    }
}
