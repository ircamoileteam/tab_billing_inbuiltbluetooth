package com.irca.fields;

/**
 * Created by Monica on 7/7/2018.
 */

public class MemberDob {
    public  String memdob,memdoa;

    public String getMemdob() {
        return memdob;
    }

    public void setMemdob(String memdob) {
        this.memdob = memdob;
    }

    public String getMemdoa() {

        return memdoa;
    }

    public void setMemdoa(String memdoa) {
        this.memdoa = memdoa;
    }
}
