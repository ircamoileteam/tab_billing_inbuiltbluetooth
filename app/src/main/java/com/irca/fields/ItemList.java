package com.irca.fields;

import javax.sql.StatementEvent;

/**
 * Created by archanna on 4/21/2016.
 */
public class ItemList {

    public String itemName;
    public String itemCode;
    public String quantity;
    public String rate;
    public String amount;  //with discounted value for bakery class
    public String total;
    public String tax;
    public String status;
    public  String stock;
    public String billno;
    public String vat;
    public String billAmount;  // without discount for bakery class
    public String discount;
    public String serviceTax;
    public String salestTax;

    public String cardType;
    public String cesstax;

    public  String memName;
    public  String memCode;

    public String otNo;
    public String accharges;

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String modifier;

    public String getTableno() {
        return Tableno;
    }

    public void setTableno(String tableno) {
        Tableno = tableno;
    }

    public String Tableno;

    public String getItemcategory() {
        return itemcategory;
    }

    public void setItemcategory(String itemcategory) {
        this.itemcategory = itemcategory;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String itemcategory;
    public String narration;

    public String getaccharges() {
        return accharges;
    }

    public void setaccharges(String accharges) {
        this.accharges = accharges;
    }



    public String getOtNo() {
        return otNo;
    }

    public void setOtNo(String otNo) {
        this.otNo = otNo;
    }



    public String getMemCode() {
        return memCode;
    }

    public void setMemCode(String memCode) {
        this.memCode = memCode;
    }

    public String getMemName() {
        return memName;
    }

    public void setMemName(String memName) {
        this.memName = memName;
    }



    public String getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    public String getSalestTax() {
        return salestTax;
    }

    public void setSalestTax(String salestTax) {
        this.salestTax = salestTax;
    }




    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }



    public String getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }



    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }



    public String getCesstax() {
        return cesstax;
    }

    public void setCesstax(String cesstax) {
        this.cesstax = cesstax;
    }




    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }



    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }




    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }


}
