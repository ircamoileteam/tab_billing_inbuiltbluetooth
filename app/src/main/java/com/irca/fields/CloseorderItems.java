package com.irca.fields;

/**
 * Created by Manoch Richard on 21-Apr-16.
 */
public class CloseorderItems {


    public   String BillID;
    public String ItemID;
    public String Quantity;
    public String Amount;
    public String BillDate;
    public String itemname;
    public String memberID;
    public String rate;
    public String  ItemCode;
    public String  cardType;
    public String  mName;
    public String mAcc;
    public String ava_balance ;
    public String taxDescription;
    public String service_tax;
    public String sales_tax;
    public String cessTax;
    public String taxValue;
    public String taxAmount;
    public String openingBalance;
    public String PersonCode;
    public String FirstName;
    public String ACCharge;
    public String gst;
    public String isFreeitem;
    public String BuyItemId;
    public String ItemSerialNo;
    public String ParentItemSerialNo;

    public String getIsFreeitem() {
        return isFreeitem;
    }

    public void setIsFreeitem(String isFreeitem) {
        this.isFreeitem = isFreeitem;
    }

    public String getBuyItemId() {
        return BuyItemId;
    }

    public void setBuyItemId(String buyItemId) {
        BuyItemId = buyItemId;
    }

    public String getItemSerialNo() {
        return ItemSerialNo;
    }

    public void setItemSerialNo(String itemSerialNo) {
        ItemSerialNo = itemSerialNo;
    }

    public String getParentItemSerialNo() {
        return ParentItemSerialNo;
    }

    public void setParentItemSerialNo(String parentItemSerialNo) {
        ParentItemSerialNo = parentItemSerialNo;
    }

    public String getWaitername() {
        return waitername;
    }

    public void setWaitername(String waitername) {
        this.waitername = waitername;
    }

    public String waitername;

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getMembersign() {
        return Membersign;
    }

    public void setMembersign(String membersign) {
        Membersign = membersign;
    }

    public String Membersign;

    public String getGSTNO() {
        return GSTNO;
    }

    public void setGSTNO(String GSTNO) {
        this.GSTNO = GSTNO;
    }

    public String GSTNO;

    public String getDiscountAmt() {
        return DiscountAmt;
    }

    public void setDiscountAmt(String discountAmt) {
        DiscountAmt = discountAmt;
    }

    public String DiscountAmt;

    public String getIsTaxExemptionPermission() {
        return IsTaxExemptionPermission;
    }

    public void setIsTaxExemptionPermission(String isTaxExemptionPermission) {
        IsTaxExemptionPermission = isTaxExemptionPermission;
    }

    public String IsTaxExemptionPermission;

    public String getItemCategoryID() {
        return ItemCategoryID;
    }

    public void setItemCategoryID(String itemCategoryID) {
        ItemCategoryID = itemCategoryID;
    }

    public String ItemCategoryID;
    public String getBillDetNumber() {
        return BillDetNumber;
    }

    public void setBillDetNumber(String billDetNumber) {
        BillDetNumber = billDetNumber;
    }

    public String BillDetNumber;

    public String getACCharge() {
        return ACCharge;
    }

    public void setACCharge(String ACCharge) {
        this.ACCharge = ACCharge;
    }

    public String getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(String openingBalance) {
        this.openingBalance = openingBalance;
    }




    public String getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(String taxValue) {
        this.taxValue = taxValue;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }



    public String getTaxDescription() {
        return taxDescription;
    }

    public void setTaxDescription(String taxDescription) {
        this.taxDescription = taxDescription;
    }


    public String getAva_balance() {
        return ava_balance;
    }

    public void setAva_balance(String ava_balance) {
        this.ava_balance = ava_balance;
    }



    public String getOtno() {
        return otno;
    }

    public void setOtno(String otno) {
        this.otno = otno;
    }

    public String otno;

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billNo) {
        this.billno = billNo;
    }

    public String billno;


    public String getCessTax() {
        return cessTax;
    }


    public void setCessTax(String cessTax) {
        this.cessTax = cessTax;
    }

    public String getmName() {     return mName;   }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmAcc() {
        return mAcc;
    }

    public void setmAcc(String mAcc) {
        this.mAcc = mAcc;
    }



    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }






    public String getBillID() {
        return BillID;
    }

    public void setBillID(String billID) {
        BillID = billID;
    }

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getBillDate() {
        return BillDate;
    }

    public void setBillDate(String billDate) {
        BillDate = billDate;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }


    public String getService_tax() {
        return service_tax;
    }

    public void setService_tax(String service_tax) {
        this.service_tax = service_tax;
    }

    public String getSales_tax() {
        return sales_tax;
    }

    public void setSales_tax(String sales_tax) {
        this.sales_tax = sales_tax;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }



    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }


    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }



    public String getPersonCode() {     return PersonCode;   }

    public void setPersonCode(String PersonCode) {
        this.PersonCode = PersonCode;
    }

    public String getFirstName() {     return FirstName;   }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }


    public CloseorderItems()
    {

    }

}
