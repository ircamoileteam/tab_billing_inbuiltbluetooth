package com.irca.fields;

/**
 * Created by Manoch Richard on 10/22/2015.
 */
public class MasterStore {


    public MasterStore() {

    }

    public String ID;
    public String StoreName;
    public String StoreCode;
    public String OTType;
    public String IsOTRequired;
    public String ItemCategoryID;
    public String IsCashSalesRequire;  //cash
    public String IsCreditSalesRequire;
    public String IsDebitSalesRequired;
    public String IsPartyBillApplicable;
    public String isdecimalval;
    public String isGuest;
    public String Address;
    public String GSTNumber;
    public String isReader;
    public String Mobilestorename;
    public String Mid;
    public String Dashboard;
    public String IstableNoManadatory;
    public String IsPaxCountMandatory ;
    public String StewardwiseBilling ;
    public String IsCashSalesRequire1 ;
    public String IsCreditSalesRequire1 ;
    public String IsDebitSalesRequired1 ;
    public String IsChequeSalesRequire ;
    public String voidOT ;
  public String PrinterType ;
    public String OTCreditLimitNotRequired ;
    public String getOTCreditLimitNotRequired() {
        return OTCreditLimitNotRequired;
    }

    public void setOTCreditLimitNotRequired(String OTCreditLimitNotRequired) {
        this.OTCreditLimitNotRequired = OTCreditLimitNotRequired;
    }





    public String getPrinterType() {
        return PrinterType;
    }

    public void setPrinterType(String printerType) {
        PrinterType = printerType;
    }

    public String getVoidOT() {
        return voidOT;
    }

    public void setVoidOT(String voidOT) {
        this.voidOT = voidOT;
    }

    public String getStewardwiseBilling() {
        return StewardwiseBilling;
    }

    public String getIsCashSalesRequire1() {
        return IsCashSalesRequire1;
    }

    public void setIsCashSalesRequire1(String isCashSalesRequire1) {
        IsCashSalesRequire1 = isCashSalesRequire1;
    }

    public String getIsCreditSalesRequire1() {
        return IsCreditSalesRequire1;
    }

    public void setIsCreditSalesRequire1(String isCreditSalesRequire1) {
        IsCreditSalesRequire1 = isCreditSalesRequire1;
    }

    public String getIsDebitSalesRequired1() {
        return IsDebitSalesRequired1;
    }

    public void setIsDebitSalesRequired1(String isDebitSalesRequired1) {
        IsDebitSalesRequired1 = isDebitSalesRequired1;
    }

    public String getIsChequeSalesRequire() {
        return IsChequeSalesRequire;
    }

    public void setIsChequeSalesRequire(String isChequeSalesRequire) {
        IsChequeSalesRequire = isChequeSalesRequire;
    }

    public void setStewardwiseBilling(String stewardwiseBilling) {
        StewardwiseBilling = stewardwiseBilling;
    }

    public String getIstableNoManadatory() {
        return IstableNoManadatory;
    }

    public void setIstableNoManadatory(String istableNoManadatory) {
        IstableNoManadatory = istableNoManadatory;
    }

    public String getIsPaxCountMandatory() {
        return IsPaxCountMandatory;
    }

    public void setIsPaxCountMandatory(String isPaxCountMandatory) {
        IsPaxCountMandatory = isPaxCountMandatory;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String storeName) {
        StoreName = storeName;
    }

    public String getStoreCode() {
        return StoreCode;
    }

    public void setStoreCode(String storeCode) {
        StoreCode = storeCode;
    }

    public String getOTType() {
        return OTType;
    }

    public void setOTType(String OTType) {
        this.OTType = OTType;
    }

    public String getIsOTRequired() {
        return IsOTRequired;
    }

    public void setIsOTRequired(String isOTRequired) {
        IsOTRequired = isOTRequired;
    }

    public String getItemCategoryID() {
        return ItemCategoryID;
    }

    public void setItemCategoryID(String itemCategoryID) {
        ItemCategoryID = itemCategoryID;
    }

    public String getIsCashSalesRequire() {
        return IsCashSalesRequire;
    }

    public void setIsCashSalesRequire(String isCashSalesRequire) {
        IsCashSalesRequire = isCashSalesRequire;
    }

    public String getIsCreditSalesRequire() {
        return IsCreditSalesRequire;
    }

    public void setIsCreditSalesRequire(String isCreditSalesRequire) {
        IsCreditSalesRequire = isCreditSalesRequire;
    }

    public String getIsDebitSalesRequired() {
        return IsDebitSalesRequired;
    }

    public void setIsDebitSalesRequired(String isDebitSalesRequired) {
        IsDebitSalesRequired = isDebitSalesRequired;
    }

    public String getIsPartyBillApplicable() {
        return IsPartyBillApplicable;
    }

    public void setIsPartyBillApplicable(String isPartyBillApplicable) {
        IsPartyBillApplicable = isPartyBillApplicable;
    }

    public String getIsdecimalval() {
        return isdecimalval;
    }

    public void setIsdecimalval(String isdecimalval) {
        this.isdecimalval = isdecimalval;
    }

    public String getIsGuest() {
        return isGuest;
    }

    public void setIsGuest(String isGuest) {
        this.isGuest = isGuest;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getGSTNumber() {
        return GSTNumber;
    }

    public void setGSTNumber(String GSTNumber) {
        this.GSTNumber = GSTNumber;
    }

    public String getIsReader() {
        return isReader;
    }

    public void setIsReader(String isReader) {
        this.isReader = isReader;
    }

    public String getMobilestorename() {
        return Mobilestorename;
    }

    public void setMobilestorename(String mobilestorename) {
        Mobilestorename = mobilestorename;
    }

    public String getMid() {
        return Mid;
    }

    public void setMid(String mid) {
        Mid = mid;
    }

    public String getDashboard() {
        return Dashboard;
    }

    public void setDashboard(String dashboard) {
        Dashboard = dashboard;
    }
}
