package com.irca.fields;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Manoch Richard on 10/26/2015.
 */
public class AndroidTempbilldetails {

    public int salesUintid,taxId;

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double quantity;
    public float amount , salestax, cesstax, servicetax, taxRate;
    public String rate;//
    public String otno;
    public String itemcode;//
    public String itemname;//
    public String itemId;//
    public ArrayList<String> alltax = null;
    public  String itemCategoryid;//
    public String narration;
    public String freeFlow;
    public String allergen;
    public String offerQty;
    public String freeQty;
    public String isFreeitem;

    public String getOfferQty() {
        return offerQty;
    }

    public void setOfferQty(String offerQty) {
        this.offerQty = offerQty;
    }

    public String getFreeQty() {
        return freeQty;
    }

    public void setFreeQty(String freeQty) {
        this.freeQty = freeQty;
    }

    public String getIsFreeitem() {
        return isFreeitem;
    }

    public void setIsFreeitem(String isFreeitem) {
        this.isFreeitem = isFreeitem;
    }

    public String getParentItemId() {
        return parentItemId;
    }

    public void setParentItemId(String parentItemId) {
        this.parentItemId = parentItemId;
    }

    public String parentItemId;





    public String getNarattionid() {
        return narattionid;
    }

    public void setNarattionid(String narattionid) {
        this.narattionid = narattionid;
    }

    public String narattionid;


    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }


    public String getItemCategoryid() {
        return itemCategoryid;
    }

    public void setItemCategoryid(String itemCategoryid) {
        this.itemCategoryid = itemCategoryid;
    }




    public ArrayList<String> getAlltax() {
        return alltax;
    }

    public void setAlltax(ArrayList<String> alltax) {
        this.alltax = alltax;
    }



    public int getSalesUintid() {
        return salesUintid;
    }

    public void setSalesUintid(int salesUintid) {
        this.salesUintid = salesUintid;
    }

    public String getFreeFlow() {
        return freeFlow;
    }

    public void setFreeFlow(String freeFlow) {
        this.freeFlow = freeFlow;
    }

    public String getAllergen() {
        return allergen;
    }

    public void setAllergen(String allergen) {
        this.allergen = allergen;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTaxId() {
        return taxId;
    }

    public void setTaxId(int taxId) {
        this.taxId = taxId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public float getSalestax() {
        return salestax;
    }

    public void setSalestax(float salestax) {
        this.salestax = salestax;
    }

    public float getCesstax() {
        return cesstax;
    }

    public void setCesstax(float cesstax) {
        this.cesstax = cesstax;
    }

    public float getServicetax() {
        return servicetax;
    }

    public void setServicetax(float servicetax) {
        this.servicetax = servicetax;
    }

    public float getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(float taxRate) {
        this.taxRate = taxRate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getOtno() {
        return otno;
    }

    public void setOtno(String otno) {
        this.otno = otno;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }


}
