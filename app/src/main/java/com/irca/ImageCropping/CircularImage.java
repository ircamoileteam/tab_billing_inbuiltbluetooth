package com.irca.ImageCropping;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

/**
 * Created by archanna on 16/11/15.
 */
public class CircularImage {
    public  static Bitmap getCircleBitmap(Bitmap bitmap)

    {
        /*

        * To draw a u need 4 components

        * 1.Bitmap  - to hold pixels
        * 2.canvas   -writing into bitmap
        *3.drawable primitive (e.g. Rect, Path, text, Bitmap)
        *4.paint   (to describe the colors and styles for the drawing).
        */


        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);

        final Canvas canvas = new Canvas(output);

        int w=bitmap.getWidth();
        int h=bitmap.getHeight();

        final int color = Color.RED;

        final Paint paint = new Paint();

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);

        canvas.drawARGB(0, 0, 0, 0);

        paint.setColor(color);

        canvas.drawOval(rectF,paint);
        //  canvas.drawCircle(bitmap.getWidth()/2,bitmap.getHeight()/2,bitmap.getWidth() /2, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        canvas.drawBitmap(bitmap, rect, rect, paint);

        // bitmap.recycle();

        return output;
    }
}
